package a07_objekte.a02_Bank01;

/************************************************************
 * Ausbau: Bankangestellter, Kapselung, statische Variablen ...
*********************************************************** */
public class Bank01Test {
    
    public static void main(String[] args) {
        Kunde k1 = new Kunde("Meier",30000,123);
        System.out.println(k1.gebeName());
        System.out.println(k1.geldstand());
    
        Kunde k2 = new Kunde("Mueller",40000,456);
        System.out.println(k2.gebeName());
        System.out.println(k2.geldstand());
    
        // usw...
    }
}
