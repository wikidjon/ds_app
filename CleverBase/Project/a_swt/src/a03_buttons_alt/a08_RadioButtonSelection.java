package a03_buttons_alt;
/****************************************************************
 * If you need to select a button programmatically, use the following method: 
		public void setSelection(boolean selected)
    
This method is applicable for checkbox buttons, radio buttons, and toggle buttons only. It has no effect on push buttons and arrow buttons. 
To check the selection status of a button, use the following method: 

		public boolean getSelection()

The above method is only applicable for check buttons, radio buttons, and toggle buttons. It always returns false if the button is a push button or an arrow button.
 ****************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a08_RadioButtonSelection {

  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setLayout(new RowLayout());

    for (int i = 0; i < 4; i++) {
      Button button = new Button(shell, SWT.RADIO);
      button.setText("Button " + i);

      button.setSelection(true);
    }
    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}