package a19_cursor;

// Sanduhrcursor w�hrend Aktivit�t
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a19_sanduhr_curosr {

	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new GridLayout());
		final Text text = new Text(shell, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		text.setLayoutData(new GridData(GridData.FILL_BOTH));
		final int[] nextId = new int[1];
		Button b = new Button(shell, SWT.PUSH);
		b.setText("starten einer aktivit�t");
		b.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Runnable prozess = new Runnable() {
					boolean fertig = false;
					int id;
					public void run() {
						Thread thread = new Thread(new Runnable() {
							public void run() {
								id = nextId[0]++;
								display.syncExec(new Runnable() {
									public void run() {
										if (text.isDisposed()) return;
										text.append("\nStart - Sanduhr- cursor "+id);
									}
								});
								for (int i = 0; i < 100000; i++) {
									if (display.isDisposed()) return;
									System.out.println("lange task in einem separaten Thread "+id);
								}
								if (display.isDisposed()) return;
								display.syncExec(new Runnable() {
									public void run() {
										if (text.isDisposed()) return;
										text.append("\ntask beendnet "+id);
									}
								});
								fertig = true;
								display.wake();
							}
						});
						thread.start();
						while (!fertig && !shell.isDisposed()) {
							if (!display.readAndDispatch())
								display.sleep();
						}
					}
				};
				// Cursorform aendern
				BusyIndicator.showWhile(display, prozess);
			}
		});
		shell.setSize(250, 150);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
