package icons;
//import org.eclipse.jface.resource.ImageDescriptor;

//import a_13_jface3.AddBookAction;

/***********************************************

//
    ir.put(ONE, ImageDescriptor.createFromFile(ImageRegistryTest.class,
        "/icons/one.gif"));
    
    checked = new Image(null, LibraryLabelProvider.class
            .getResourceAsStream("/icons/checked.gif"));


    super("&Add Book@Ctrl+B", ImageDescriptor.createFromFile(AddBookAction.class,
        "/icons/addBook.gif"));
    setDisabledImageDescriptor(ImageDescriptor.createFromFile(
        AddBookAction.class, "/icons/disabledAddBook.gif"));

**********************************************/