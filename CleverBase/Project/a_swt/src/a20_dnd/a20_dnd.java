package a20_dnd;


// Drag text zwischen textwidgets
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a20_dnd {
	static String string1 = "Da die Gesamtheit der Prozessfunktionalit�ten nur in Anwender-Dynpros implementiert" +
							"ist, werden diese �ber das Batch - Input - Verfahren f�r den externen Zugriff verf�gbar" +
							"gemacht. Dieses Verfahren wird im Abschnitt 2.1.5 ausf�hrlich beschrieben. Dabei bleibt " +
							"es f�r den Gutachter unklar, wie �Nutzereingaben ... automatisch erfolgen� k�nnen (Seite 15). " +
							"Offenbar werden diese Eingaben dann in einer Tabelle gespeichert, die im Folgenden beschrieben wird. " +
							"Auch handelt es sich wohl bei den FNAM und FVAL um Zeilen und nicht um Spalten (S. 16 oben)." ;

	static String string2 = "Da die Gesamtheit der Prozessfunktionalit�ten nur in Anwender-Dynpros implementiert" +
							"ist, werden diese �ber das Batch - Input - Verfahren f�r den externen Zugriff verf�gbar" +
							"gemacht. Dieses Verfahren wird im Abschnitt 2.1.5 ausf�hrlich beschrieben. Dabei bleibt " +
							"es f�r den Gutachter unklar, wie �Nutzereingaben ... automatisch erfolgen� k�nnen (Seite 15). "+
							"Offenbar werden diese Eingaben dann in einer Tabelle gespeichert, die im Folgenden beschrieben wird. " +
							"Auch handelt es sich wohl bei den FNAM und FVAL um Zeilen und nicht um Spalten (S. 16 oben)." ;
	
public static void main (String [] args) {
	final Display display = new Display ();
	Shell shell = new Shell (display);
	shell.setLayout(new FillLayout());
	int style = SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER;
	final StyledText text1 = new StyledText(shell, style);
	text1.setText(string1);
	DragSource source = new DragSource(text1, DND.DROP_COPY | DND.DROP_MOVE);
	source.setTransfer(new Transfer[] {TextTransfer.getInstance()});
	source.addDragListener(new DragSourceAdapter() {
		Point selection;
		public void dragStart(DragSourceEvent e) {
			selection = text1.getSelection();
			e.doit = selection.x != selection.y;
		}
		public void dragSetData(DragSourceEvent e) {
			e.data = text1.getText(selection.x, selection.y-1);
		}
		public void dragFinished(DragSourceEvent e) {
			if (e.detail == DND.DROP_MOVE) {
				text1.replaceTextRange(selection.x, selection.y - selection.x, "");
			}
			selection = null;
		}
	});
	
	final StyledText text2 = new StyledText(shell, style);
	text2.setText(string2);
	DropTarget target = new DropTarget(text2, DND.DROP_DEFAULT | DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_LINK);
	target.setTransfer(new Transfer[] {TextTransfer.getInstance()});
	target.addDropListener(new DropTargetAdapter() {
		public void dragEnter(DropTargetEvent e) {
			if (e.detail == DND.DROP_DEFAULT)
				e.detail = DND.DROP_COPY;
		}
		public void dragOperationChanged(DropTargetEvent e) {
			if (e.detail == DND.DROP_DEFAULT)
				e.detail = DND.DROP_COPY;
		}
		public void drop(DropTargetEvent e) {
			text2.insert((String)e.data);
		}
	});
	shell.open ();
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
}