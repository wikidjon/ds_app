package a99a_xml.SAX;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXPrimzahlHandler extends DefaultHandler {

  private boolean inInt = false, debug=false;
  
  public void startElement(String namespaceURI, String localName,
   String qualifiedName, Attributes atts) throws SAXException {
    if (debug) System.out.println("[startElement]localName="+localName+"; qualifiedName="+qualifiedName);
	if (localName.equals("int")||qualifiedName.equals("int")) inInt = true;
    
  }

  public void endElement(String namespaceURI, String localName,
   String qualifiedName) throws SAXException {
	if (debug) System.out.println("endElement : localName="+localName);
	if (localName.equals("int")||qualifiedName.equals("int")) inInt = false;
    
  }

  public void characters(char[] ch, int start, int length)
  throws SAXException {
	if (debug) System.out.println("[characters]start="+start+"; Length="+length);
	if (inInt) {
	  for (int i = start; i < start+length; i++) {
		System.out.print(ch[i]); 
	  }
	  System.out.println();
	}   
   
  }
  
}
