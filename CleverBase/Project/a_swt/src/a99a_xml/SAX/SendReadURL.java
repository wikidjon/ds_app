package a99a_xml.SAX;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class SendReadURL {

	public static void main(String[] args) {
		System.out.println("[SendToURL]main()");
		try {
			URL url = new URL("http://www.hs-merseburg.de");

//			URL url = new URL("http://localhost:5555/");
			URLConnection urlConnection = url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());

			String xml_rpc_body = 	"<?xml version=\"1.0\"?>\n"+
									"<methodCall>\n"+
									"<methodName>primeServer.getPrime</methodName>\n"+
									"<params>\n"+
									"<param>\n"+
									"<value><int>2345</int></value>\n"+
									"</param>\n"+
									"<param>\n"+
									"<value><boolean>true</boolean></value>\n"+
									"</param>\n"+
									"</params>\n"+
									"</methodCall>\n";
			wr.write(xml_rpc_body);
			wr.flush();

			// Lies die Antwort vom Server
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		
			// Schreibe die Antwort in den StringBuffer
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			wr.close();
			rd.close();
			
			// von Hand Analyse der Antwort
			String startTag="<value><int>", endTag="</int></value>";
			String antwort = sb.toString();
			int start = antwort.indexOf(startTag)+startTag.length();
			int end = antwort.indexOf(endTag);
			String ergebnis = antwort.substring(start,end);
			System.out.println("Eingabe : 2345");
			System.out.println("Primzahl : "+ergebnis);
			
		} catch(MalformedURLException urlexp) {
			urlexp.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}