package a99a_xml.SAX;

import java.io.File;
import java.io.FileWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * DOM Test : einlesen eines XML Dokuments und selektive Ausgabe von
 * DOM Information.  
 */
public class DOMCrimsonHelloWorld {
	private static String tab = "\t";
	public static void main(String[] args) throws Exception {
		FileWriter fw = new FileWriter("DOMTest.xml");

		fw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
		fw.write("<methodCall>\n");
		fw.write("	<methodName>methode.getName</methodName>\n");
		fw.write("	<params>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<int>123</int>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<boolean>1</boolean>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("	</params>\n");
		fw.write("</methodCall>\n");
		fw.flush();
		fw.close();

		// DOM
		// Document Builder
		System.out.println("[DocumentBuilder]");
		// holen eines dom parsers
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		System.out.println("\tDocumentBuilder ist validierend: " + db.isValidating());

		// Document
		Document doc = db.parse(new File("DOMTest.xml"));
		System.out.println("[Document]");
		System.out.println("[Document]Dokumnet-Information (Root Node)");
		System.out.println("[Document]localName : " + doc.getLocalName());
		System.out.println("[Document]NamespaceURI : " + doc.getNamespaceURI());
		System.out.println("[Document]NodeName : " + doc.getNodeName());
		System.out.println("[Document]Node Type : "	+ doc.getNodeType()	+ " = DOCUMENT_NODE "
				+ Document.DOCUMENT_NODE);
		System.out.println("[Document]NodeValue : " + doc.getNodeValue());
		System.out.println("[Document]NodePrefix : " + doc.getPrefix());

		// root
		System.out.println("[Document]rekursive Analyse");
		System.out.println("[Document]root");
		Element doc_element = doc.getDocumentElement();
		String str_doc_element = doc_element.getNodeName();
		System.out.println("[Document.Element]NodeName : " + str_doc_element);

		// Children
		// durch alle Child Knoten sausen		 
		System.out.println("\nRekursives Durchlaufen der Knoten\n");
		DOMCrimsonHelloWorld.visitNodes(doc); // ab root
		
		// gezielt suchen
		NodeList nl = doc.getElementsByTagName("int");
		System.out.println("Anzahl 'int' Knoten : " + nl.getLength());
		Node n = nl.item(0);
		System.out.println("Value: " + n.getNodeValue());
		System.out.println("Local Name: " + n.getLocalName());
		System.out.println("Node Name: " + n.getNodeName());
		NodeList nl2 = n.getChildNodes();
		System.out.println(
			"Anzahl Kinder des 'int' Knotens : " + nl2.getLength());
		Node n2 = nl2.item(0);
		System.out.println("Value: " + n2.getNodeValue());
		System.out.println("Local Name: " + n2.getLocalName());
		System.out.println("Node Name: " + n2.getNodeName());

		// Document aufbauen und in eine Datei schreiben
		Document newDoc = DOMCrimsonHelloWorld.buildTree();
		DOMCrimsonHelloWorld.writeXmlFile(newDoc, "DOMausHelloWorld.xml");
		DOMCrimsonHelloWorld.visitNodes(newDoc.getDocumentElement());
	}
	/**
	 * rekursive Methode, mit der pro Node alle Child Knoten bestimmte werden
	 * (NodeList) und pro Knoten in der NodeList wird jeweils (rekursiv)
	 * diese Methode gleich wieder aufgerufen.
	 * @param pStartNode	Start Knoten (dessen Node List wird bestimmt)
	 * @throws Exception	
	 */
	public static void visitNodes(Node pStartNode) throws Exception {
		if (pStartNode != null) {
			System.out.println(
				tab + "[StartNode]Node Name : " + pStartNode.getNodeName());
			Node node = pStartNode;
			NodeList nl = node.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				Node nodeTemp = nl.item(i);
				System.out.println(
					tab + "\tNode Name: " + nodeTemp.getNodeName());
				System.out.println(
					tab
						+ "\tNode Type: "
						+ DOMCrimsonHelloWorld.getNodeType(nodeTemp.getNodeType()));
				System.out.println(
					tab + "\tNode Value: " + nodeTemp.getNodeValue());
				// Rekusrion
				DOMCrimsonHelloWorld.visitNodes(nodeTemp);
			}
		}
	}
	/**
	 * Aufbau eines DOM Baumes (von Hand)
	 * @return DOM Baum als Document
	 */
	private static Document buildTree() {
		// Aufbau eines DOM Baumes und Serialisierung in eine Datei
		Document doc = null;
		try {
			System.out.println("[buildTree]Aufbau eines DOM Baumes");
			DocumentBuilder builder =
				DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = builder.newDocument();
			/*
			 *     <?xml version="1.0" encoding="UTF-8"?>
			 *		<!--dynamisch generiertes XML -->
			 * 		<Adresse>
			 * 			<Name>Huber<Name/>
			 * 			<Ort>Jona</Ort>
			 * 		</Adresse>
			 */
			// Kommentar einf�gen
			Comment comment = doc.createComment("dynamisch generiertes XML");
			doc.appendChild(comment);

			// Root Element Node
			Element adresse = doc.createElement("Adresse");
			doc.appendChild(adresse);
			Element name = doc.createElement("Name");
			adresse.appendChild(name);
			Text tName = doc.createTextNode("Huber");
			name.appendChild(tName);
			Element ort = doc.createElement("Ort");
			Text tOrt = doc.createTextNode("Jona");
			ort.appendChild(tOrt);
			adresse.appendChild(ort);

		} catch (ParserConfigurationException e) {
		}
		return doc;

	}
	/**
	 * einfache Umsetzung der Node Codes in eine textuelle Darstellung
	 * @param pNodeType Node Type als short
	 * @return Node Type als String
	 */
	private static String getNodeType(short pNodeType) {
		switch (pNodeType) {
			case Node.ATTRIBUTE_NODE :
				return "Attribute Node";
			case Node.CDATA_SECTION_NODE :
				return "CDATA Section Node";
			case Node.COMMENT_NODE :
				return "Comment Node";
			case Node.DOCUMENT_FRAGMENT_NODE :
				return "Document Fragment Node";
			case Node.DOCUMENT_NODE :
				return "Document Node";
			case Node.DOCUMENT_TYPE_NODE :
				return "Document Type Node";
			case Node.ELEMENT_NODE :
				return "Element Node";
			case Node.ENTITY_NODE :
				return "Entity Node";
			case Node.ENTITY_REFERENCE_NODE :
				return "Entity Reference Node";
			case Node.NOTATION_NODE :
				return "Notation Node";
			case Node.PROCESSING_INSTRUCTION_NODE :
				return "Processing Instruction Node";
			case Node.TEXT_NODE :
				return "Text Node";
			default :
				return "unbekannter Knotentyp";
		}
	}
	/**
	 * Serialisierung eines DOM Document in eine XML Datei
	 * Dies geschieht mit Hilfe der transform() Methode
	 * @param doc		DOM Document
	 * @param filename	Dateiname
	 */
	public static void writeXmlFile(Document doc, String filename) {
		try {
			System.out.println(
				"[writeXMLFile]Ausgabe eines Document in die Datei "
					+ filename);
			// DOM Document vorbereiten
			Source source = new DOMSource(doc);

			// Ausgabedatei vorbereiten
			File file = new File(filename);
			Result result = new StreamResult(file);

			// DOM in Datei speichern
			Transformer xformer =
				TransformerFactory.newInstance().newTransformer();
			xformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}
	}
}
