package a99a_xml.SAX;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Einfacher SOAP Client f�r SOAPPrimzahlServlet
 * Dieser verwendet URLConnection zum Senden der SOAP Message
 * und liest die Antwort in eine Zeichenkette
 * Diese wird dann "geparsed" / anaysiert.
 * Gesucht wird einfach der Antworttag.
 */
public class SOAPURLConnectionClient {
	private static final String DEFAULT_HOST_URL =
		"http://localhost:5555/JavaXML5/servlet/parsers.SOAPPrimzahlServlet";
	private static final int DEFAULT_STARTZAHL = 123;
	public static void main(String[] args) {
		System.out.println("[URLConnectionClient]Start");
		@SuppressWarnings("unused")
		int iStartzahl = DEFAULT_STARTZAHL;
		String server = DEFAULT_HOST_URL;

		/*
		 * mithilfe von URLConnection eine Verbindung zur URL aufbauen
		 */
		try {
			System.out.println("[URLConnectionClient]URL");
			URL url = new URL(server);
			System.out.println("[URLConnectionClient]URLConnection");
			URLConnection urlcon = url.openConnection();
			System.out.println("[URLConnectionClient]HttpURLConnection");
			HttpURLConnection hurlcon = (HttpURLConnection) urlcon;
			hurlcon.setDoOutput(true);
			hurlcon.setDoInput(true);
			hurlcon.setRequestMethod("POST");

			OutputStream os = hurlcon.getOutputStream();
			System.out.println("[URLConnectionClient]Writer");
			Writer wout = new OutputStreamWriter(os);

			wout.write("<?xml version=\"1.0\"  encoding=\"UTF-8\"?>\r\n");
			wout.write(
				"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
			wout.write("<SOAP-ENV:Header>");
			wout.write(
				"<javaxml:MessageHeader xmlns:javaxml=\"urn:javaxml\" SOAP-ENV:mustUnderstand=\"1\" xmlns:SOAP-ENV=\"urn:javaxml\"><From>From_Client</From><To>To_Client</To><MessageId>9999</MessageId></javaxml:MessageHeader>");
			wout.write("</SOAP-ENV:Header>");
			wout.write("<SOAP-ENV:Body>");
			wout.write("<methodCall>");
			wout.write("	<methodName>primeServer.getPrime</methodName>");
			wout.write("	<params>");
			wout.write("		<param>");
			wout.write("			<value>");
			wout.write("				<int>123</int>");
			wout.write("			</value>");
			wout.write("		</param>");
			wout.write("		<param>");
			wout.write("			<value>");
			wout.write("				<boolean>1</boolean>");
			wout.write("			</value>");
			wout.write("		</param>");
			wout.write("	</params>");
			wout.write("</methodCall>");
			wout.write("(</SOAP-ENV:Body>");
			wout.write("</SOAP-ENV:Envelope>");

		} catch (IOException ioe) {
			ioe.printStackTrace(System.err);
		}
	}
}
