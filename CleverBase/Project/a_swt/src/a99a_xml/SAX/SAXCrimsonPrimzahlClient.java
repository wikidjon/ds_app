
package a99a_xml.SAX;

/**
 * Der Client benutzt XMLRPCPrimeServer als Primzahl-Server
 * Der Server sendet eine XML Antwort
 * Diee wird im SAXPrimzahlHandler verarbeitet 
 * (und die Primzahl wird durch den Handler ausgegeben).
 * 
 * Zudem wird der Apache Tunnel verwendet
 * Dieser muss zuerst gestartet werden. 
 */

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXCrimsonPrimzahlClient {

	public static void main(String[] args) throws Exception {
		System.out.println("[SAXCrimsonPrimzahlClient]main()");
		try {
			URL url = new URL("http://localhost:5555/");
			URLConnection urlConnection = url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());

			String xml_rpc_body = 	"<?xml version=\"1.0\"?>\n"+
									"<methodCall>\n"+
									"<methodName>primeServer.getPrime</methodName>\n"+
									"<params>\n"+
									"<param>\n"+
									"<value><int>2345</int></value>\n"+
									"</param>\n"+
									"<param>\n"+
									"<value><boolean>true</boolean></value>\n"+
									"</param>\n"+
									"</params>\n"+
									"</methodCall>\n";
			wr.write(xml_rpc_body);
			wr.flush();
			wr.close();

			SAXParserFactory saxFactory = javax.xml.parsers.SAXParserFactory.newInstance();
			SAXParser sax = saxFactory.newSAXParser();

			// Ausgabe des Servers steht im InputStream der urlConnection
			System.out.println("Eingabe : 2345");
			System.out.print("Primzahl : ");
			sax.parse(urlConnection.getInputStream(),new SAXPrimzahlHandler());
			
		} catch(MalformedURLException urlexp) {
			urlexp.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}

	}
}
