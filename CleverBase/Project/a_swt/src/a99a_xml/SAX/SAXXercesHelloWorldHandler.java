package a99a_xml.SAX;

import java.io.IOException;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class SAXXercesHelloWorldHandler extends org.xml.sax.helpers.DefaultHandler {

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#characters(char[], int, int)
	 */
	public void characters(char[] ch, int start, int length)
		throws SAXException {
		super.characters(ch, start, length);
		System.out.println("characters");
		System.out.println("\tstart="+start);
		System.out.println("\tlength="+length);
		if (length > 0) {
			System.out.print("\tArray :");

			for (int i = 0; i < start + length; i++)
				System.out.print(ch[i]);
			System.out.println();
		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#endDocument()
	 */
	public void endDocument() throws SAXException {
		super.endDocument();
		System.out.println("endDocument");
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(String uri, String localName, String qName)
		throws SAXException {
		super.endElement(uri, localName, qName);
		System.out.println("endElement");
		System.out.println("\tURI : " + uri);
		System.out.println("\tlocal Name : " + localName);
		System.out.println("\tqName : " + qName);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#endPrefixMapping(java.lang.String)
	 */
	public void endPrefixMapping(String prefix) throws SAXException {
		super.endPrefixMapping(prefix);
		System.out.println("endPrefixMapping");
		System.out.println("\tprefix : " + prefix);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
	 */
	public void error(SAXParseException e) throws SAXException {
		super.error(e);
		System.out.println("error");
		System.out.println("\t" + e.getMessage());
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
	 */
	public void fatalError(SAXParseException e) throws SAXException {
		super.fatalError(e);
		System.out.println("fatalError");
		System.out.println("\tfatal " + e.getMessage());

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int, int)
	 */
	public void ignorableWhitespace(char[] ch, int start, int length)
		throws SAXException {
		super.ignorableWhitespace(ch, start, length);
		System.out.println("ignorableWhitespace");
		if (length > 0) {

			System.out.print("\tArray : ");
			for (int i = start; i < start + length; i++)
				System.out.print(ch[i]);
			System.out.println();
		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.DTDHandler#notationDecl(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void notationDecl(String name, String publicId, String systemId)
		throws SAXException {
		super.notationDecl(name, publicId, systemId);
		System.out.println("notationDecl:");
		System.out.println("\tname : " + name);
		System.out.println("\tpublicID : " + publicId);
		System.out.println("\tsystemId : " + systemId);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#processingInstruction(java.lang.String, java.lang.String)
	 */
	public void processingInstruction(String target, String data)
		throws SAXException {
		super.processingInstruction(target, data);
		System.out.println("processingInstruction");
		System.out.println("\ttarget : " + target);
		System.out.println("\tdata : " + data);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
	 */
	public InputSource resolveEntity(String publicId, String systemId)
		throws SAXException, IOException {
		System.out.println("resolveEntity");
		System.out.println("\tpublicId : " + publicId);
		System.out.println("\tsystemId : " + systemId);
		return super.resolveEntity(publicId, systemId);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#setDocumentLocator(org.xml.sax.Locator)
	 */
	public void setDocumentLocator(Locator locator) {
		super.setDocumentLocator(locator);
		System.out.println("setDocumentLocator");
		System.out.println(
			"\tLocator Line Number : " + locator.getLineNumber());
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
	 */
	public void skippedEntity(String name) throws SAXException {
		super.skippedEntity(name);
		System.out.println("skippedEntity");
		System.out.println("\tname : " + name);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#startDocument()
	 */
	public void startDocument() throws SAXException {
		super.startDocument();
		System.out.println("startDocument");
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(
		String uri,
		String localName,
		String qName,
		Attributes attributes)
		throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		System.out.println("startElement");
		System.out.println("\turi : " + uri);
		System.out.println("\tlocalName : " + localName);
		System.out.println("\tqName : " + localName);
		System.out.println("\tAttribute L�nge: " + attributes.getLength());
		if (attributes.getLength() > 0) {
			System.out.println("\tAttribute ");
			System.out.print("\t");
			for (int i = 0; i < attributes.getLength(); i++)
				System.out.print(
					"Local Name : "
						+ attributes.getLocalName(i)
						+ "; Type : "
						+ attributes.getType(i));

		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String, java.lang.String)
	 */
	public void startPrefixMapping(String prefix, String uri)
		throws SAXException {
		super.startPrefixMapping(prefix, uri);
		System.out.println("startPrefixMapping");
		System.out.println("\tprefix : " + prefix);
		System.out.println("\turi : " + uri);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.DTDHandler#unparsedEntityDecl(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void unparsedEntityDecl(
		String name,
		String publicId,
		String systemId,
		String notationName)
		throws SAXException {
		super.unparsedEntityDecl(name, publicId, systemId, notationName);
		System.out.println("unparsedEntity");
		System.out.println("\tname : " + name);
		System.out.println("\tpublicId : " + publicId);
		System.out.println("\tsystemId : " + systemId);
		System.out.println("\tnotationName : " + notationName);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
	 */
	public void warning(SAXParseException e) throws SAXException {
		super.warning(e);
		System.out.println("warning");
	}

}
