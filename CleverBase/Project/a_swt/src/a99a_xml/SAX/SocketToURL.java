package a99a_xml.SAX;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

public class SocketToURL {

	public static void main(String[] args) {
		// Construct data
		String xml_rpc_call=""; 
		try {
			BufferedReader br = new BufferedReader(new FileReader("XMLRPCCall.xml"));
			String line="";
			while ((line=br.readLine())!=null){
				xml_rpc_call=xml_rpc_call+line;
			}
		}catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		try {
			// Create a socket to the host
			String hostname = "localhost";
			int port = 9090;

			InetAddress addr = InetAddress.getByName(hostname);
			Socket socket = new Socket(addr, port);
    
			// Send header
			String path = "/computer";
			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
			
			String http_POST_header = 	"POST "+path+" HTTP/1.1\r\n"+
										"Content-Length: "+xml_rpc_call.length()+"\r\n"+
										"Content-Type: text/xml\r\n"+
										"\r\n";
			wr.write(http_POST_header);
			System.out.println(http_POST_header);
			System.out.println(xml_rpc_call);
    
			// Send data
			wr.write(xml_rpc_call);
			wr.flush();
    
			// Get response
			BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String line;
			
			BufferedWriter wb = new BufferedWriter(new FileWriter("XMLRPCSocket.txt"));
			while ((line = rd.readLine()) != null) {
				wb.write(line+"\n");
				System.out.println(line);
			}
			wb.close();
			wr.close();
			rd.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}
}
