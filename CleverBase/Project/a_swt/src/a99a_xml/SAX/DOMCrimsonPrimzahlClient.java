/*
 * Created on 02.11.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package a99a_xml.SAX;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


public class DOMCrimsonPrimzahlClient {
	private static final String DEFAULT_HOST_URL =
		"http://localhost:5555/JavaXML5/servlet/parsers.SOAPPrimzahlServlet";
	public static void main(String[] args) {
		System.out.println("[DOMCrimsonPrimzahlClient]main()");
		String pServer = DEFAULT_HOST_URL;
		DOMCrimsonPrimzahlClient dc = new DOMCrimsonPrimzahlClient();
		int iStart=2345; 
		//int iPrim=0;
		Document doc = dc.bildeDocument(iStart);

		// Server
		try {

			URL url = new URL(pServer);
			URLConnection uc = url.openConnection();
			HttpURLConnection connection = (HttpURLConnection) uc;
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			OutputStream out = connection.getOutputStream();

			// Primzahl abfragen
			dc.callServer(doc, out);

			out.flush();
			out.close();

			// Antwort lesen
			InputStream in = connection.getInputStream();
			int iPrime = dc.extrahierePrimzahl(in);

			// Abschluss
			in.close();
			connection.disconnect();
			System.out.println("Startzahl :\t"+iStart);
			System.out.println("Primzahl : \t"+iPrime);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Lesen der Antwort des Primzahl Servers aus dem InputStream
	 * der URLConnection zum Server
	 * @param pIn InputStream von der URLConnection
	 * @return naechste Primzahl zur gesendeten int Zahl
	 */
	private int extrahierePrimzahl(InputStream pIn) {
		System.out.println("[DOMCrimsonPrimzahlClient]extrahierePrimzahl()");
		int iPrime = 0;
		try {
			// DOM Document Builder
			DocumentBuilder db =
				DocumentBuilderFactory.newInstance().newDocumentBuilder();

			// Document
			Document doc = db.parse(pIn);
			NodeList doubles = doc.getElementsByTagName("int");
			Node prime = doubles.item(0);
			Text result = (Text) prime.getFirstChild();
			String strPrime = result.getNodeValue();
			iPrime = Integer.parseInt(strPrime);
		} catch (DOMException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return iPrime;
	}
	/**
	 * Kommunikation mit dem Primzahl Server
	 * @param pDoc DOM Document, in dem die XML Anfrage steckt
	 * @param pOut OutputStream zum Server
	 */
	private void callServer(Document pDoc, OutputStream pOut) {
		System.out.println("[DOMCrimsonPrimzahlClient]callServer()");		
		try {

			// DOM Document vorbereiten
			Source source = new DOMSource(pDoc);

			// Ausgabe vorbereiten
			Result result = new StreamResult(pOut);

			// DOM in Stream schreiben
			Transformer xformer =
				TransformerFactory.newInstance().newTransformer();
			xformer.transform(source, result);

			// Antwort

		} catch (TransformerConfigurationException e) {
		} catch (TransformerException e) {
		}
	}
	/**
	 * Aufbau eines DOM Baumes (von Hand)
	 * @return DOM Baum als Document
	 */
	private Document bildeDocument(int pStart) {
		System.out.println("[DOMCrimsonPrimzahlClient]bildeDocuemnt()");
		Document doc = null;
		try {
			/*
			 * 	<?xml version="1.0" encoding="UTF-8" ?> 
				<methodCall>
					<methodName>primeServer.getPrime</methodName>
					<params>
						<param>
							<value><int>2345</int></value>
						</param>
						<param>
							<value><boolean>true</boolean></value>
						</param>
					</params>
				</methodCall>
			 */
			System.out.println("[bildeDocument]Aufbau eines DOM Baumes");
			DocumentBuilder builder =
				DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = builder.newDocument();
			// Kommentar einf�gen
			Comment comment =
				doc.createComment(
					"Abfrage der naechst groesseren Primzahl zu einer int");
			doc.appendChild(comment);

			// Root Element Node
			Element methodCall = doc.createElement("methodCall");
			doc.appendChild(methodCall);
			Element methodName = doc.createElement("methodName");
			methodCall.appendChild(methodName);
			Text tName = doc.createTextNode("primeServer.getPrime");
			methodName.appendChild(tName);
			Element params = doc.createElement("params");
			Element param1 = doc.createElement("param");
			Element value1 = doc.createElement("value");
			Element intValue = doc.createElement("int");
			Text tInt = doc.createTextNode(""+pStart);
			intValue.appendChild(tInt);
			value1.appendChild(intValue);
			param1.appendChild(value1);
			params.appendChild(param1);
			methodCall.appendChild(params);

			Element param2 = doc.createElement("param");
			Element value2 = doc.createElement("value");
			Element booleanValue = doc.createElement("boolean");
			Text tBoole = doc.createTextNode("true");
			booleanValue.appendChild(tBoole);
			value2.appendChild(booleanValue);
			param2.appendChild(value2);
			params.appendChild(param2);
		} catch (ParserConfigurationException e) {
		}
		return doc;

	}
}
