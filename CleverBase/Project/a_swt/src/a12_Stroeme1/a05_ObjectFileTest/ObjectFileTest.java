package a12_Stroeme1.a05_ObjectFileTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

//*******************************************************************************************
/* Schreibt Objekte mit variabler Länge (Manager/Angestellter) )                            */
/* in eine Binärdatei (Angestellter.dat) - ist typischer Fall beim Speichern von OBjekten   */
/* zuerst den Typ speichern und dann die Daten                                              */
/* beim Lesen erst Typ ermitteln und dann Objekt erzeugen und Daten einlesen                */
//*******************************************************************************************

// ************************************************************************************************************
// ************************************************************************************************************
class Angestellter implements Serializable { // Serializable muss implementiert

	private static final long serialVersionUID = 1L;

	// Beim Serialisieren eines Objektes wird auch die serialVersionUID der zugehörigen
	// Klasse mit in die Ausgabedatei geschrieben. Soll das Objekt später deserialisiert
	// werden, so wird die in der Datei gespeicherte serialVersionUID mit der aktuellen
	// serialVersionUID des geladenen .class-Files verglichen. Stimmen beide nicht
	// überein, so gibt es eine Ausnahme des Typs InvalidClassException, und der
	// Deserialisierungsvorgang bricht ab.


	private String name;
	private double lohn;
	private Date GebTag;

	public Angestellter() {
	}

	public Angestellter(final String n, final double l, final int jahr, final int monat, final int tag) {
		this.name = n;
		this.lohn = l;
		final GregorianCalendar calendar = new GregorianCalendar(jahr, monat - 1, tag);
		// GregorianCalendar verwendet 0 für Januar
		this.GebTag = calendar.getTime();
	}

	public Date getGebtag() {
		return this.GebTag;
	}

	public double getLohn() {
		return this.lohn;
	}

	public String getName() {
		return this.name;
	}

	public void LohnZuschlag(final double Prozent) {
		final double zuschlag = this.lohn * Prozent / 100;
		this.lohn += zuschlag;
	}

	@Override
	public String toString() {
		return getClass().getName() + ": \nName = " + this.name + ", Lohn = " + this.lohn
				+ ", GebTag = " + this.GebTag;
		// return getClass().getName() + ": \nName = " + name + ", Lohn = " +
		// getLohn() + ", GebTag = " + GebTag;
		// Bonus wird berücksichtigt
	}

}

// ************************************************************************************************************
// ************************************************************************************************************
class Manager extends Angestellter {

	private static final long serialVersionUID = 1L;
	private double bonus;

	public Manager(final String n, final double s, final int jahr, final int monat, final int tag) {
		super(n, s, jahr, monat, tag);
		this.bonus = 0;
	}

	@Override
	public double getLohn() {
		final double grundlohn = super.getLohn();
		return grundlohn + this.bonus;
	}

	public void setBonus(final double b) {
		this.bonus = b;
	}

	@Override
	public String toString() {
		return super.toString() + " [bonus=" + this.bonus + "]";
	}
}

class ObjectFileTest {
	public static void main(final String[] args) {
		final Manager boss = new Manager("Angie Merkel", 80000, 1930, 12, 15);
		boss.setBonus(5000); // soll zeigen, dass die DS wieder richtig
		// ausgelesen und zugeordnet werden

		final Angestellter[] personal = new Angestellter[3];
		personal[0] = boss;
		personal[1] = new Angestellter("Jimi Hendrix", 50000, 1940, 10, 1);
		personal[2] = new Angestellter("Mick Jagger", 40000, 1970, 3, 15);

		try {
			// Alle Mitarbeiterdatensätze in Datei schreiben
			final ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream("C:/Ausgang/Angestellte.dat"));
			out.writeObject(personal); // Schreiben von Array - Objekten
			// !!!!!!!!!
			out.close();

			// Alle Datensätze in ein neues Array einlesen
			final ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					"C:/Ausgang/Angestellte.dat"));
			final Angestellter[] newpersonal = (Angestellter[]) in.readObject();
			// Lesen von Array-Objekten !!!!!!!!!
			in.close();

			// eingelesene Mitarbeiterdatensätze ausgeben
			for (int i = 0; i < newpersonal.length; i++) {
				System.out.println("\n" + (i + 1) + ". " + newpersonal[i]);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
