package a12_Stroeme1.a06_ObjectRefTest;

import java.io.*;
import java.util.*;

//**************************************************************************
/* Schreibt Datenarray (Angestellter) in eine Binärdatei(Angestellte.dat) */
/* Objektreferenzen werden gespeichert (Sekretaer vom Boss)        	  */
//**************************************************************************

class ObjectRefTest {
	public static void main(String[] args) {
		Angestellter pofi = new Angestellter("Pofalla Pofalla", 50000, 1989, 10, 1);
		Manager boss = new Manager("Angie Merkel", 80000, 1987, 12, 15);
		
		// ***** Objektreferenz !!!!!!!***********************************************
		boss.setSekretaer(pofi);

		Angestellter[] personal = new Angestellter[3];

		personal[0] = boss;
		personal[1] = pofi;
		personal[2] = new Angestellter("Guido Giudo", 40000, 1990, 3, 15);

		try {
			// Alle Mitarbeiterdatensätze in die Datei regierung.dat schreiben
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("regierung.dat"));
			out.writeObject(personal);
			out.close();

			// Alle Datensätze in ein neues Array einlesen
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("regierung.dat"));
			Angestellter[] newPersonal = (Angestellter[]) in.readObject();
			in.close();

			// Gehalt des Sekretärs erhöhen
			newPersonal[1].LohnZuschlag(10);

			System.out.println("Ausgabe (nach Lohnerhöhung)-------------------------------------------------------");
			// Die neu gelesenen Mitarbeiterdatensätze ausgeben
			for (int i = 0; i < newPersonal.length; i++)
				System.out.println(newPersonal[i]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// ************************************************************************************************************
// ************************************************************************************************************
class Angestellter implements Serializable {
	
	// Beim Serialisieren eines Objektes wird auch die serialVersionUID der zugehörigen
	// Klasse mit in die Ausgabedatei geschrieben. Soll das Objekt später deserialisiert
	// werden, so wird die in der Datei gespeicherte serialVersionUID mit der aktuellen
	// serialVersionUID des geladenen .class-Files verglichen. Stimmen beide nicht
	// überein, so gibt es eine Ausnahme des Typs InvalidClassException, und der
	// Deserialisierungsvorgang bricht ab.



	private static final long serialVersionUID = 1L;
	private String name;
	private double lohn;
	private Date GebTag;

	public Angestellter() {
	}

	public Angestellter(String n, double l, int jahr, int monat, int tag) {
		name = n;
		lohn = l;
		GregorianCalendar calendar = new GregorianCalendar(jahr, monat - 1, tag);
		// GregorianCalendar verwendet 0 für Januar
		GebTag = calendar.getTime();
	}

	public String getName() {
		return name;
	}

	public double getLohn() {
		return lohn;
	}

	public Date getGebTag() {
		return GebTag;
	}

	public void LohnZuschlag(double prozent) {
		double zuschlag = lohn * prozent / 100;
		lohn += zuschlag;
	}

	public String toString() {
		return getClass().getName() + "\nAngestellter [Name=" + name + ",Lohn=" + lohn
				+ ",GebTag=" + GebTag + "]\n";
	}
}

// ************************************************************************************************************
// ************************************************************************************************************
class Manager extends Angestellter {

	private static final long serialVersionUID = 1L;
	private Angestellter sekretaer;

	/******************************************************
	 * konstruiert einen Manager ohne Sekretär
	 *******************************************************/
	public Manager(String n, double l, int jahr, int monat, int tag) {
		super(n, l, jahr, monat, tag);
		sekretaer = null;
	}

	/*****************************************************
	 * weist dem Manager einen Sekretär zu.
	 ******************************************************/
	public void setSekretaer(Angestellter s) {
		sekretaer = s;
	}

	public String toString() {
		return super.toString() + "[Sekretaer = !!!!***!!!! " + "   " + sekretaer + "]";
	}
}
