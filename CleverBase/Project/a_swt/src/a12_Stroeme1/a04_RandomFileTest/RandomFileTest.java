package a12_Stroeme1.a04_RandomFileTest;

import java.io.*;
import java.util.*;

//**********************************************************************************
/* Schreibt Datenarray (Angestellter) in eine Binärdatei (Angestellte.dat)         */
/* damit ist z.B. auch Positionierung möglich                                      */
/* anwendbar beim Schreiben von Datensätzen gleichen Typs                          */
//**********************************************************************************

public class RandomFileTest {
	public static void main(String[] args) {
		Angestellter[] angest = new Angestellter[3];

		angest[0] = new Angestellter("Eric Clapton", 750000, 1950, 12, 15);
		angest[1] = new Angestellter("Mick Jagger", 50000, 1949, 10, 1);
		angest[2] = new Angestellter("Jimi Hendrix", 40000, 1952, 3, 15);
		// **************************************************************
		// Länge = 100 Byte - 2 * 40 Name + 8 Lohn (double) + 3* int (12)
		// **************************************************************

		try {
			
			// ****************************************************************************
			// Alle Mitarbeiterdatensätze in die Datei Angestellte.dat schreiben
			// ****************************************************************************
			DataOutputStream out = new DataOutputStream(new FileOutputStream("C:/Ausgang/Angestellte.dat"));
			for (int i = 0; i < angest.length; i++)
				angest[i].writeData(out); 			// Methode aus Angestellter
			out.close();

			
			
			// ****************************************************************************
			// Alle Datensätze in ein neues Array einlesen
			// ****************************************************************************
			RandomAccessFile in = new RandomAccessFile("C:/Ausgang/Angestellte.dat", "r");	
			// Arraygroesse berechnen **************
			int n = (int) (in.length() / Angestellter.RECORD_SIZE);  		//Array Komp Groesse = 100
			Angestellter[] angestArray = new Angestellter[n];
			// Mitarbeiter in umgekehrter Reihenfolge lesen
			for (int i = n - 1; i >= 0; i--) {
				angestArray[i] = new Angestellter();
				in.seek(i * Angestellter.RECORD_SIZE);  					// siehe Berechnung unten
				angestArray[i].readData(in); 								// Methode aus Angestellter
			}
			in.close();

			
			// ****************************************************************************
			// eingelesene Mitarbeiterdatensätze ausgeben
			for (int i = 0; i < angestArray.length; i++)
				System.out.println("\n" + (i + 1) + ". " + angestArray[i]);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}

// **********************************************************************************
// **********************************************************************************

class Angestellter {
	// **********************************************************************************
	public static final int NAME_SIZE = 40;
	public static final int RECORD_SIZE = 2 * NAME_SIZE + 8 + 4 + 4 + 4;

	private String name;
	private double lohn;
	private Date gebTag;

	public Angestellter() {
	}

	// ***********************************************************************************

	public Angestellter(String n, double l, int jahr, int monat, int day) {
		name = n;
		lohn = l;
		GregorianCalendar calendar = new GregorianCalendar(jahr, monat - 1, day);
		// GregorianCalendar verwendet 0 für Januar
		gebTag = calendar.getTime();
	}

	public String getName() {
		return name;
	}

	public double getLohn() {
		return lohn;
	}

	public Date getgebTag() {
		return gebTag;
	}

	public void LohnZuschlag(double Prozent) {
		double zuschlag = lohn * Prozent / 100;
		lohn += zuschlag;
	}

	public String toString() {
		return getClass().getName() + " \n[Name: " + name + ",  Lohn: " + lohn
				+ ",  GebTag: " + gebTag + "]";
	}

	/************************************************************************************
	 * Schreibt Mitarbeiterdaten in eine Datenausgabe
	 *************************************************************************************/
	public void writeData(DataOutput out) throws IOException // DataOutput ist Interface aus IO
	{
		DataIO.writeFString(name, NAME_SIZE, out);			// siehe unten
		out.writeDouble(lohn); 								// Binärspeicherung

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(gebTag);
		out.writeInt(calendar.get(Calendar.YEAR)); // Binärspeicherung
		out.writeInt(calendar.get(Calendar.MONTH) + 1);
		out.writeInt(calendar.get(Calendar.DAY_OF_MONTH));
	}

	/**************************************************************************************
	 * Liest Mitarbeiterdaten aus einer Dateneingabe
	 ***************************************************************************************/
	public void readData(DataInput in) throws IOException {
		name = DataIO.readFString(NAME_SIZE, in);
		lohn = in.readDouble(); // Binärlesen
		int y = in.readInt(); // Binärlesen
		int m = in.readInt();
		int d = in.readInt();
		GregorianCalendar calendar = new GregorianCalendar(y, m - 1, d);
		// GregorianCalendar verwendet 0 für Januar
		gebTag = calendar.getTime();
	}

}

// **************************************************************************************
// **************************************************************************************

class DataIO {
	/****************************************************************************************
	 * da die Grösse des Datensatzes konstant sein muss, werden die DatenSätze				*
	 * mit den F-Methoden auf die gleiche Länge gebracht 									*
	 ****************************************************************************************/
	public static  String readFString(int size, DataInput in) throws IOException {
		StringBuffer b = new StringBuffer(size);
		int i = 0;
		boolean mehr = true;
		while (mehr && i < size) {
			char ch = in.readChar();
			i++;
			if (ch == 0)
				mehr = false;
			else
				b.append(ch);
		}
		in.skipBytes(2 * (size - i));
		return b.toString();
	}

	// **************************************************************************************
	public static void writeFString(String s, int size, DataOutput out)
			throws IOException {
		int i;
		for (i = 0; i < size; i++) {
			char ch = 0;
			if (i < s.length())
				ch = s.charAt(i);
			out.writeChar(ch);
		}
	}
}
