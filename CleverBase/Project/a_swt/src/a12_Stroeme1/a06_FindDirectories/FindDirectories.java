package a12_Stroeme1.a06_FindDirectories;

import java.io.*;

public class FindDirectories
{
   public static void main(String[] args)
   {
      // Sind keine Argumente angegeben, beim �bergeordneten Verzeichnis beginnen
      if (args.length == 0) args = new String[] { "C:/Ausgang" };

      try
      {
         File pathName = new File(args[0]);
         String[] fileNames = pathName.list();

         // Alle Dateien im Verzeichnis aufz�hlen
         for (int i = 0; i < fileNames.length; i++)
         {
            File f = new File(pathName.getPath(), fileNames[i]);

            // Wenn die Datei wieder ein Verzeichnis ist, die Methode main rekursiv aufrufen
            if (f.isDirectory())
            {
               System.out.println(f.getCanonicalPath());
               main(new String [] { f.getPath() });
            }
         }
      }
      catch(IOException e)
      {
         e.printStackTrace();
      }
   }
}
