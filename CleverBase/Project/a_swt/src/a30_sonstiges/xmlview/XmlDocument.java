package a30_sonstiges.xmlview;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.w3c.dom.Document;

/**
 * This class wraps an XML file
 */
public class XmlDocument {
	private String filename;
	private Document document;

	/**
	 * Constructs an XmlDocument
	 * 
	 * @param filename the file name of the XML file
	 */
	public XmlDocument(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the underlying JDOM Document object
	 * 
	 * @return Document
	 */
	public Document getDocument() {
		return this.document;
	}

	/**
	 * Gets just the file name
	 * 
	 * @return String
	 */
	public String getFilename() {
		return this.filename.substring(this.filename.lastIndexOf(File.separator) + 1);
	}

	/**
	 * Opens and parses the file
	 * 
	 * @throws IOException if any problem opening or parsing
	 */
	public void open() throws IOException {
		SAXBuilder builder = new SAXBuilder();
		try {
			this.document = builder.build(new FileInputStream(this.filename));
		} catch (JDOMException e) {
			throw new IOException(e.getMessage());
		}
	}
}
