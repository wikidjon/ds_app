package a15_ole;

/*
 * example snippet: Embed Word in an applet (win32 only)
 *
 * For a list of all SWT example snippets see
 * http://www.eclipse.org/swt/snippets/
 * 
 * @since 3.0
 */

import java.applet.Applet;

public class a31_ole_word extends Applet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5543171198842058566L;
	org.eclipse.swt.widgets.Display display;
	org.eclipse.swt.widgets.Shell swtParent;
	java.awt.Canvas awtParent;

	@Override
	public void init() {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				setLayout(new java.awt.GridLayout(1, 1));
				a31_ole_word.this.awtParent = new java.awt.Canvas();
				add(a31_ole_word.this.awtParent);
				a31_ole_word.this.display = new org.eclipse.swt.widgets.Display();
				a31_ole_word.this.swtParent = org.eclipse.swt.awt.SWT_AWT.new_Shell(a31_ole_word.this.display,
						a31_ole_word.this.awtParent);
				a31_ole_word.this.swtParent.setLayout(new org.eclipse.swt.layout.FillLayout());
				org.eclipse.swt.ole.win32.OleFrame frame = new org.eclipse.swt.ole.win32.OleFrame(
						a31_ole_word.this.swtParent, org.eclipse.swt.SWT.NONE);
				org.eclipse.swt.ole.win32.OleClientSite site;
				try {
					site = new org.eclipse.swt.ole.win32.OleClientSite(frame,
							org.eclipse.swt.SWT.NONE, "Word.Document");
				} catch (org.eclipse.swt.SWTException e) {
					String str = "Create OleClientSite Error" + e.toString();
					System.out.println(str);
					return;
				}
				setSize(500, 500);
				validate();
				site.doVerb(org.eclipse.swt.ole.win32.OLE.OLEIVERB_SHOW);

				while (a31_ole_word.this.swtParent != null && !a31_ole_word.this.swtParent.isDisposed()) {
					if (!a31_ole_word.this.display.readAndDispatch()) {
						a31_ole_word.this.display.sleep();
					}
				}
			}
		});
		thread.start();
	}

	@Override
	public void stop() {
		if (this.display != null && !this.display.isDisposed()) {
			this.display.syncExec(new Runnable() {
				@Override
				public void run() {
					if (a31_ole_word.this.swtParent != null && !a31_ole_word.this.swtParent.isDisposed()) {
						a31_ole_word.this.swtParent.dispose();
					}
					a31_ole_word.this.swtParent = null;
					a31_ole_word.this.display.dispose();
					a31_ole_word.this.display = null;
				}
			});
			remove(this.awtParent);
			this.awtParent = null;
		}
	}
}