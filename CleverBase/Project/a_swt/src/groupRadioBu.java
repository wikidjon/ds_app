import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

public class groupRadioBu {

	public static void main(String[] args) {

		//new group_radio_bu();

		Display d;

		Shell s;

		d = new Display();
		s = new Shell(d);
		s.setSize(200, 200);

		s.setText("A Group Example");
		final Group g = new Group(s, SWT.SHADOW_ETCHED_IN);
		g.setSize(110, 75);
		g.setText("Options Group");
		final Button b1;
		final Button b2;
		final Button b3;
		b1 = new Button(g, SWT.RADIO);
		b1.setBounds(10, 20, 75, 15);
		b1.setText("Option One");
		b2 = new Button(g, SWT.RADIO);
		b2.setBounds(10, 35, 75, 15);
		b2.setText("Option Two");
		b3 = new Button(g, SWT.RADIO);
		b3.setBounds(10, 50, 80, 15);
		b3.setText("Option Three");
		g.pack();
		g.setLocation(20, 20);
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}

}


