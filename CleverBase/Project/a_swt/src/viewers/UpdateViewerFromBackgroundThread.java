/***************************
 * package viewers;


import java.awt.Button;
import java.awt.Color;
import java.awt.Image;

import javax.swing.table.TableColumn;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;

/**
 * Demonstrates how to update a viewer from a long running task (which is executed in a thread) and calls back to the UI-Thread using asyncExec

public class UpdateViewerFromBackgroundThread {
	private class MyContentProvider implements IStructuredContentProvider {


		public void dispose() {

		}


		public Object[] getElements(Object inputElement) {
			return (MyModel[])inputElement;
		}


		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

		}

	}
/************************************************************
	public class MyLabelProvider extends LabelProvider implements ITableLabelProvider {

		public Image getColumnImage(Object element, int columnIndex) {
			if( columnIndex == 0 )
				return images[((MyModel)element).finished?0:1];

			return null;
		}

		public String getColumnText(Object element, int columnIndex) {
			return "Column " + columnIndex + " => " + element.toString();
		}

	}

	public class MyModel {
		public int counter;
		public boolean finished;

		public MyModel(int counter) {
			this.counter = counter;
		}

		public String toString() {
			return "Item " + this.counter;
		}
	}

	private static Image[] images;

	private static Image createImage(Display display, int red, int green, int blue) {
		Color color = new Color(display,red,green,blue);
		Image image = new Image(display,10,10);
		GC gc = new GC(image);
		gc.setBackground(color);
		gc.fillRectangle(0, 0, 10, 10);
		gc.dispose();

		return image;
	}


	public static void main(String[] args) {
		Display display = new Display ();

		images = new Image[2];
		images[0] = createImage(display,0,255,0);
		images[1] = createImage(display,255,0,0);

		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		new UpdateViewerFromBackgroundThread(shell);
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}

		for( int i = 0; i < images.length; i++ ) {
			images[i].dispose();
		}

		display.dispose ();

	}

	public UpdateViewerFromBackgroundThread(Shell shell) {
		final TableViewer v = new TableViewer(shell,SWT.BORDER|SWT.FULL_SELECTION);
		v.setLabelProvider(new MyLabelProvider());
		v.setContentProvider(new MyContentProvider());

		TableColumn column = new TableColumn(v.getTable(),SWT.NONE);
		column.setWidth(200);
		column.setText("Column 1");

		column = new TableColumn(v.getTable(),SWT.NONE);
		column.setWidth(200);
		column.setText("Column 2");

		final MyModel[] model = createModel();
		v.setInput(model);
		v.getTable().setLinesVisible(true);
		v.getTable().setHeaderVisible(true);

		Button b = new Button(shell,SWT.PUSH);
		b.setText("Start Long Task");
		b.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				final Thread t = new Thread() {

					public void run() {
						for( int i = 0; i < model.length; i++ ) {
							if( v.getTable().isDisposed())
								return;
							final int j = i;
							v.getTable().getDisplay().asyncExec(new Runnable() {

								public void run() {
									model[j].finished = true;
									v.update(model[j], null);
								}

							});
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				};
				t.start();
			}
		});
	}

	private MyModel[] createModel() {
		MyModel[] elements = new MyModel[10];

		for( int i = 0; i < 10; i++ ) {
			elements[i] = new MyModel(i);
		}

		return elements;
	}

}

 ************************************/