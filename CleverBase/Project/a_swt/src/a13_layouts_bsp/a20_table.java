package a13_layouts_bsp;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public final class a20_table  {
	
	public void createTable(Composite com ){ //, Display d){

		Table table = new Table(com, SWT.NONE);
		table.setBounds(10,10,270,160);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		TableColumn a = new TableColumn(table,SWT.CENTER);
		a.setText("a");		a.setWidth(50);
		TableColumn b = new TableColumn(table,SWT.CENTER);
		b.setText("b");		b.setWidth(50);
		TableColumn c = new TableColumn(table,SWT.CENTER);
		c.setText("c");		c.setWidth(50);
		TableColumn d = new TableColumn(table,SWT.CENTER);
		d.setText("d");		d.setWidth(50);
	
		TableItem a1 = new TableItem(table,SWT.NONE);
		a1.setText(new String[] {"22","25","33","44"});
		TableItem a2 = new TableItem(table,SWT.NONE);
		a2.setText(new String[] {"5","66","26","109"});
	}
}
