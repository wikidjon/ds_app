package a06_combo2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class CompositeDemo {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300,300);
		shell.setText("Composite Demo");

		//ein Layout Manager sollte immer da sein, sonst sind einige Anweisungen "wirkungslos"
		GridLayout gl = new GridLayout();
		shell.setLayout(gl);
		//		shell.setImage(new Image(display, "src/icons/cut.gif"));
		shell.setText("Composite Beispiel");

		GridData gd = new GridData(GridData.FILL_BOTH);// .FILL_HORIZONTAL);

		Composite composite1 = new Composite(shell,SWT.BORDER);
		composite1.setBounds(10,10,270,250);
		composite1.setBackground(new Color(display,31,133,31));

		Label label = new Label(composite1,SWT.NONE);
		label.setText("Das ist ein gruenes Composite");
		label.setBounds(10,10,200,20);
		/****************************************/
		composite1.setLayoutData(gd);
		/****************************************/

		Composite composite2 = new Composite(composite1,SWT.NONE);
		composite2.setBounds(10,40,230,200);
		composite2.setBackground((display.getSystemColor(SWT.COLOR_RED)));

		Label label2 = new Label(composite2,SWT.NONE);
		label2.setText("Das ist ein rotes Composite");
		label2.setBounds(10,10,150,20);

		Composite composite3 = new Composite(composite2,SWT.NONE);
		composite3.setBounds(10,40,170,150);
		composite3.setBackground((display.getSystemColor(SWT.COLOR_YELLOW)));

		Label label3 = new Label(composite3,SWT.NONE);
		label3.setText("Das ist ein gelbes Composite");
		label3.setBounds(10,10,150,20);

		shell.open();

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
