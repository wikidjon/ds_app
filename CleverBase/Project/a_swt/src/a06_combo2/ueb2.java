package a06_combo2;

import java.io.Serializable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;



/***************************************************************************
 ***************************************************************************/

class StudData implements Serializable{
	private String name;
	private String vname;
	private String matrikel;
	private Text pnote;
	private Text mnote;
	private String f�cher;
}

public class ueb2{

	public static void main(String[] args){

		Display display = new Display();
		Shell shell = new Shell(display);

		GridLayout grid = new GridLayout();
		grid.numColumns = 3;

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;

		GridData gridData1 = new GridData();
		gridData1.horizontalAlignment = GridData.FILL;
		gridData1.grabExcessHorizontalSpace = true;
		gridData1.grabExcessVerticalSpace = true;

		//gridData1.verticalAlignment = GridData.FILL;
		//gridData1.heightHint = 40;

		/**********************
		GridLayout grid1 = new GridLayout();

		grid1.numColumns = 1;

		GridLayout grid2 = new GridLayout();

		grid2.numColumns = 2;

		RowLayout row = new RowLayout ();
		row.type = SWT.VERTICAL;
		row.justify = true;
		row.wrap = false;

		RowLayout row1 = new RowLayout ();
		row1.type = SWT.HORIZONTAL;
		row1.justify = true;
		row1.wrap = false;
		 */
		shell.setText("Eingabe");
		shell.setLayout(grid);
		shell.setBackground(display.getSystemColor(SWT.COLOR_YELLOW));
		/******************
		SelectionListener pselectionListener = new SelectionAdapter () {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button button = ((Button) event.widget);
				System.out.print(button.getText());
				System.out.println(" selected = " + button.getSelection());
				//note = button.getText();
			};
		};

		SelectionListener mselectionListener = new SelectionAdapter () {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button button = ((Button) event.widget);
				System.out.print(button.getText());
				System.out.println(" selected = " + button.getSelection());
				//note2 = button.getText();
			};
		};

		SelectionListener selectionListener = new SelectionAdapter () {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button button = ((Button) event.widget);
				System.out.print(button.getText());
				System.out.println(" selected = " + button.getSelection());
			};
		};


		/**
	    MouseAdapter mouse = new MouseAdapter() {

	    	public void mouseDown(MouseEvent e) {

	    		StudData dat = new StudData(text1.getText(), text2.getText(), text3.getText(), note1, note2);
	    		try{
	    			ObjectOutputStream out = new ObjectOutputStream(
	    					new FileOutputStream("H:/public_html/prog/dat"));
	    			out.writeObject(dat);
	    			out.close();
	    		}
	    		catch (Exception f) {
	    			f.printStackTrace();
	    		}
	    	}

	    };

		 ************************************		 */

		/******************************************************
		 * US - 3 Composites nebeneinander !!!*
		 ******************************************************/

		final Composite comp0 = new Composite(shell, SWT.NONE);
		comp0.setLayoutData(gridData);
		comp0.setBackground(display.getSystemColor(SWT.COLOR_DARK_RED));
		// US - Achtung Label an die Composite binden !!!
		Label label1 = new Label(comp0, SWT.NONE);
		label1.setText("comp0");
		label1.setLocation(10,10);
		label1.pack();

		final Composite comp1 = new Composite(shell, SWT.BORDER);
		comp1.setLayoutData(gridData);
		comp1.setBackground(display.getSystemColor(SWT.COLOR_DARK_YELLOW));
		Label label2 = new Label(comp1, SWT.NONE);
		label2.setText("comp1");
		label2.setLocation(10,10);
		label2.pack();

		final Composite comp2 = new Composite(shell, SWT.BORDER);
		comp2.setLayoutData(gridData);
		comp2.setBackground(display.getSystemColor(SWT.COLOR_DARK_GREEN));
		Label label3 = new Label(comp2, SWT.NONE);
		label3.setText("comp2");
		label3.setLocation(10,10);
		label3.pack();

		/******************************************************
		 * US - Ende --- 3 Composites nebeneinander !!!*
		 ******************************************************/

		GridLayout grid01 = new GridLayout();
		grid01.numColumns = 1;

		GridData gridData01 = new GridData();
		gridData01.grabExcessHorizontalSpace = true;
		gridData01.grabExcessVerticalSpace = true;
		gridData01.horizontalAlignment = GridData.FILL;
		gridData01.verticalAlignment = GridData.FILL;

		comp0.setLayout(grid01);


		//		shell.setBackground(display.getSystemColor(SWT.COLOR_YELLOW));
		final Composite comp01 = new Composite(comp0, SWT.NONE);
		comp01.setLayoutData(gridData);
		comp01.setBackground(display.getSystemColor(SWT.COLOR_CYAN));
		Label label01 = new Label(comp01, SWT.NONE);
		label01.setText("comp01");
		label01.setLocation(10,10);
		label01.pack();

		final Composite comp02 = new Composite(comp0, SWT.NONE);
		comp02.setLayoutData(gridData);
		comp02.setBackground(display.getSystemColor(SWT.COLOR_BLUE));
		Label label02 = new Label(comp02, SWT.NONE);
		label02.setText("comp02");
		label02.setLocation(10,10);
		label02.pack();

		final Composite comp03 = new Composite(comp0, SWT.NONE);
		comp03.setLayoutData(gridData);
		comp03.setBackground(display.getSystemColor(SWT.COLOR_DARK_GRAY));
		Label label03 = new Label(comp03, SWT.NONE);
		label03.setText("comp03");
		label03.setLocation(10,10);
		label03.pack();

		/************************************

		final Composite comp11 = new Composite(comp0, SWT.NONE);
		comp11.setLayoutData(gridData);

		comp11.setLayout(grid1);  ///row

		Label label11 = new Label(comp11, SWT.NONE);
		label11.setText("ojl�jkl�e");

		label11.pack();

		Text text1 = new Text(comp11, SWT.BORDER);

		final Composite comp12 = new Composite(comp1, SWT.NONE);
		comp12.setLayoutData(gridData);

		comp12.setLayout(row);

		Label label12 = new Label(comp12, SWT.NONE);
		label12.setText("Vorname");

		label12.pack();

		Text text2 = new Text(comp12, SWT.BORDER);

		final Composite comp13 = new Composite(comp1, SWT.NONE);
		comp13.setLayoutData(gridData);

		comp13.setLayout(row);

		Label label13 = new Label(comp13, SWT.NONE);
		label13.setText("Matrikel");

		label13.pack();

		Text text3 = new Text(comp13, SWT.BORDER);


		final Composite comp2 = new Composite(comp0, SWT.BORDER);
		comp2.setLayoutData(gridData);

		comp2.setLayout(grid1);

		comp2.setBackground(display.getSystemColor(SWT.COLOR_RED));

		final Composite comp21 = new Composite(comp2, SWT.BORDER);
		comp21.setLayoutData(gridData);

		comp21.setLayout(grid2);

		comp21.setBackground(display.getSystemColor(SWT.COLOR_GREEN));

		final Composite comp211 = new Composite(comp21, SWT.NONE);
		comp211.setLayoutData(gridData);

		comp211.setBackground(display.getSystemColor(SWT.COLOR_BLUE));

		Label label211 = new Label(comp211, SWT.NONE);
		label211.setText("prog");
		label211.setLocation(10,10);

		label211.pack();

		final Composite comp212 = new Composite(comp21, SWT.NONE);
		comp212.setLayoutData(gridData);

		comp212.setLayout(row);

		comp212.setBackground(display.getSystemColor(SWT.COLOR_CYAN));

		Button p1 = new Button(comp212, SWT.RADIO);
		p1.setText("p1");
		p1.addSelectionListener(pselectionListener);
		Button p2 = new Button(comp212, SWT.RADIO);
		p2.setText("p2");
		p2.addSelectionListener(pselectionListener);
		Button p3 = new Button(comp212, SWT.RADIO);
		p3.setText("p3");
		p3.addSelectionListener(pselectionListener);
		Button p4 = new Button(comp212, SWT.RADIO);
		p4.setText("p4");
		p4.addSelectionListener(pselectionListener);
		Button p5 = new Button(comp212, SWT.RADIO);
		p5.setText("p5");
		p5.addSelectionListener(pselectionListener);

		final Composite comp22 = new Composite(comp2, SWT.BORDER);
		comp22.setLayoutData(gridData);

		comp22.setLayout(grid2);

		comp22.setBackground(display.getSystemColor(SWT.COLOR_DARK_GREEN));

		final Composite comp221 = new Composite(comp22, SWT.NONE);
		comp221.setLayoutData(gridData);

		comp221.setBackground(display.getSystemColor(SWT.COLOR_DARK_BLUE));

		Label label221 = new Label(comp221, SWT.NONE);
		label221.setText("mathe");
		label221.setLocation(10,10);

		label221.pack();

		final Composite comp222 = new Composite(comp22, SWT.NONE);
		comp222.setLayoutData(gridData);

		comp222.setLayout(row);

		comp222.setBackground(display.getSystemColor(SWT.COLOR_DARK_CYAN));

		Button m1 = new Button(comp222, SWT.RADIO);
		m1.setText("m1");
		m1.addSelectionListener(mselectionListener);
		Button m2 = new Button(comp222, SWT.RADIO);
		m2.setText("m2");
		m2.addSelectionListener(mselectionListener);
		Button m3 = new Button(comp222, SWT.RADIO);
		m3.setText("m3");
		m3.addSelectionListener(mselectionListener);
		Button m4 = new Button(comp222, SWT.RADIO);
		m4.setText("m4");
		m4.addSelectionListener(mselectionListener);
		Button m5 = new Button(comp222, SWT.RADIO);
		m5.setText("m5");
		m5.addSelectionListener(mselectionListener);

		final Composite comp3 = new Composite(comp0, SWT.BORDER);
		comp3.setLayoutData(gridData);

		comp3.setLayout(row);

		Label label5 = new Label(comp3, SWT.NONE);
		label5.setText("F�cher");

		label5.pack();

		Button f1 = new Button(comp3, SWT.CHECK);
		f1.setText("FACH 1");
		f1.addSelectionListener(selectionListener);
		Button f2 = new Button(comp3, SWT.CHECK);
		f2.setText("FACH 2");
		f2.addSelectionListener(selectionListener);
		Button f3 = new Button(comp3, SWT.CHECK);
		f3.setText("FACH 3");
		f3.addSelectionListener(selectionListener);
		Button f4 = new Button(comp3, SWT.CHECK);
		f4.setText("FACH 4");
		f4.addSelectionListener(selectionListener);
		Button f5 = new Button(comp3, SWT.CHECK);
		f5.setText("FACH 5");
		f5.addSelectionListener(selectionListener);
		Button f6 = new Button(comp3, SWT.CHECK);
		f6.setText("FACH 6");
		f6.addSelectionListener(selectionListener);


		/**
		final Composite end = new Composite(shell, SWT.NONE);
			end.setLayoutData(gridData1);
			end.setLayout(grid1);

			new Button(end, SWT.CENTER).addMouseListener(mouse);
		 **/





		//shell.pack();

		shell.open();

		while (!shell.isDisposed()){
			if (!display.readAndDispatch()){
				display.sleep();
			}
		}

		shell.dispose();


	}
}