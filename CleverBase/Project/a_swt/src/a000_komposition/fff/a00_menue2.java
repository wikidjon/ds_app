package a000_komposition.fff;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

@SuppressWarnings("unused")
public class a00_menue2 {

	static SelectionListener l = new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			System.out.println(e.widget.getData());
			a_MenuShell_composite.setLabelText((String) e.widget.getData());
		}
	};

	public void menue2(Shell s, Display d, Composite c) {

		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;

		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);

		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		// ********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		item.setData("Textmodus");
		item.addSelectionListener(l);
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		item.setData("Formatieren");
		item.addSelectionListener(l);
		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);

		menu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);

		final CoolBar coolBar = new CoolBar(c, SWT.NONE);
		coolBar.setSize(395, 70);
		// coolBar.setLocation(0,0);

		// create images for toolbar buttons
		final Image saveIcon = new Image(d, "src/icons/filesave.png");
		final Image openIcon = new Image(d, "src/icons/open1.gif");
		final Image childIcon = new Image(d, "src/icons/hotel.gif");
		final Image cutIcon = new Image(d, "src/icons/reconnect.gif");
		final Image copyIcon = new Image(d, "src/icons/down.gif");
		final Image pasteIcon = new Image(d, "src/icons/up.gif");

		// create and add the button for performing an open operation
		final CoolItem openCoolItem = new CoolItem(coolBar, SWT.NONE);

		final ToolBar fileToolBar = new ToolBar(coolBar, SWT.HORIZONTAL);
		final ToolItem openToolItem = new ToolItem(fileToolBar, SWT.PUSH);
		openToolItem.setImage(openIcon);
		// openToolItem.setText("Open");
		openToolItem.setToolTipText("Oeffnen");

		final ToolItem saveToolItem = new ToolItem(fileToolBar, SWT.PUSH);
		saveToolItem.setImage(saveIcon);
		// saveToolItem.setText("Save");
		saveToolItem.setToolTipText("Speichern");

		fileToolBar.pack();
		Point size = fileToolBar.getSize();
		openCoolItem.setControl(fileToolBar);
		openCoolItem.setSize(openCoolItem.computeSize(size.x, size.y));

		final CoolItem editbarCoolItem = new CoolItem(coolBar, SWT.PUSH);
		final ToolBar editToolBar = new ToolBar(coolBar, SWT.HORIZONTAL);

		final ToolItem cutToolItem = new ToolItem(editToolBar, SWT.PUSH);
		cutToolItem.setImage(cutIcon);
		// cutToolItem.setText("Cut");
		cutToolItem.setToolTipText("Ausschneiden");

		// create and add the button for performing a copy operation
		final ToolItem copyToolItem = new ToolItem(editToolBar, SWT.PUSH);
		copyToolItem.setImage(copyIcon);
		// copyToolItem.setText("Copy");
		copyToolItem.setToolTipText("Kopieren");

		// create and add the button for performing a paste operation
		final ToolItem pasteToolItem = new ToolItem(editToolBar, SWT.PUSH);
		pasteToolItem.setImage(pasteIcon);
		// pasteToolItem.setText("Paste");
		pasteToolItem.setToolTipText("Einfuegen");
		editToolBar.pack();
		size = editToolBar.getSize();
		editbarCoolItem.setControl(editToolBar);
		editbarCoolItem.setSize(editbarCoolItem.computeSize(size.x, size.y));

		final CoolItem fontCoolItem = new CoolItem(coolBar, SWT.NONE);
		final Combo fontCombo = new Combo(coolBar, SWT.READ_ONLY | SWT.NONE);
		String[] items = { "Arial", "Courier", "Times New Roman" };

		fontCombo.setItems(items);
		fontCombo.pack();

		size = fontCombo.getSize();
		fontCoolItem.setControl(fontCombo);
		fontCoolItem.setSize(fontCoolItem.computeSize(size.x, size.y));
		fontCoolItem.setMinimumSize(size);
		fontCombo.select(0);

		//***************************************************************

		final CoolItem openCoolItem2 = new CoolItem(coolBar, SWT.NONE);

		final ToolBar fileToolBar2 = new ToolBar(coolBar, SWT.HORIZONTAL);
		final ToolItem openToolItem2 = new ToolItem(fileToolBar2, SWT.PUSH);
		openToolItem2.setImage(openIcon);
		// openToolItem.setText("Open");
		openToolItem2.setToolTipText("Oeffnen");


		final ToolItem saveToolItem2 = new ToolItem(fileToolBar2, SWT.PUSH);
		saveToolItem2.setImage(saveIcon);
		// saveToolItem.setText("Save");
		saveToolItem2.setToolTipText("Speichern");

		fileToolBar2.pack();
		Point size2 = fileToolBar2.getSize();
		openCoolItem2.setControl(fileToolBar2);
		openCoolItem2.setSize(openCoolItem.computeSize(size.x, size.y));


		//***********ende*************************
		//***/

		openToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("geoeffnet");

			}
		});

		saveToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("speichern");

			}
		});

		cutToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("auschneiden");

			}
		});

		copyToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("kopieren");

			}
		});

		pasteToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("einfuegen");

			}
		});

	}
}