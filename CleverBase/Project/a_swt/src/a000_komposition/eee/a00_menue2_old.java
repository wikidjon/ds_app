package a000_komposition.eee;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
//import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import a000_komposition.a04_Menue_toolbar;  //fuer icons -> verbessern!!
//import MenuShell_composite1;
import a13_layouts1.a_Table;

@SuppressWarnings("unused")
public class a00_menue2_old{

	static SelectionListener l = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
//			System.out.println(e.widget.getData());
			a_MenuShell_composite_old.setLabelText((String) e.widget.getData());			}
	};

	
	public void menue2(Shell s, Display d, Composite c){

		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;
	
		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
				a_MenuShell_composite_old.setLabelText((String) e.widget.getData());			
				}
		};

		FillLayout fillLayout = new FillLayout (SWT.HORIZONTAL);
	
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);
	
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		item.addSelectionListener(l);
		System.out.println("test2");

		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);
		
		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);
		
//***************************************************************************		
		final ToolBar toolbar = new ToolBar(c, SWT.FLAT);
		GridLayout gl = new GridLayout();
		gl.marginWidth = 0;
		gl.marginHeight = 0;
		gl.horizontalSpacing = 0;
		gl.verticalSpacing = 0;
		c.setLayout(fillLayout);
///***************************************************************************		
		
		toolbar.setBackground(d.getSystemColor(SWT.COLOR_WHITE));
		menubar.setData(toolbar);

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");
//****/
//********************************************************************
//		a_Folder af = new a_Folder();
//		af.folder(s, d);

		/***************************************************
		TabFolder tabFolder1 = new TabFolder(s,SWT.NONE);
		tabFolder1.setBounds(10,10,70,50);

		//Button Tab
		Composite buttonComp = new Composite(tabFolder1,SWT.NONE);
		Button button1 = new Button(buttonComp,SWT.PUSH);
		button1.setSize(80,20);
		button1.setText("Hallo");
		button1.setLocation(10,10);
		Button button2 = new Button(buttonComp,SWT.ARROW);
		button2.setBounds(100,0,20,20);

		TabItem item1 = new TabItem(tabFolder1,SWT.NONE);
		item1.setText("Buttons");
	//	item1.setData(new FillLayout());
		item1.setControl(buttonComp);

		//Label Tab
		Composite labelComp = new Composite(tabFolder1,SWT.NONE);
		Label label1 = new Label(labelComp,SWT.NONE);
		label1.setText("Label 1");
		label1.setBounds(10,10,250,20);
		Label label2 = new Label(labelComp,SWT.NONE);
		label2.setText("Label 2");
		label2.setBounds(10,40,200,20);

		TabItem item2 = new TabItem(tabFolder1,SWT.NONE);
		item2.setText("Labels");
		item2.setControl(labelComp);

		// ???? Tab
		Composite testComp = new Composite(tabFolder1,SWT.NONE);
		Label label3 = new Label(testComp,SWT.NONE);
		label3.setText("Hallo");
//		label3.setData
		label3.setBounds(10,10,250,20);
		
		TabItem item3 = new TabItem(tabFolder1,SWT.NONE);
		item3.setText("???");
		item3.setControl(testComp);
//*****************************************************************/

//********************************************************
//		createWidgets(s,d);	// auf groesse bringen und an Tabs binden
//		new a_Table().tabelle(s,d);
		}
	
	
	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static Image createImage(String name) {
		return new Image(
			Display.getCurrent(),
			a04_Menue_toolbar.class.getResourceAsStream("../icons/" + name + ".gif"));
	}
	
	//*******Textarea anlegen*****************************
	private void createWidgets(Shell s, Display d) {   
		final Text text;
//*		FillLayout fillLayout = new FillLayout ();
		// Standardfont (Windows-spezifisch) festlegen
		final Font font = new Font(d, "Lucida Console", 10, SWT.NORMAL);
		text = new Text(s, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		text.setSize(100, 300);
		text.pack();
//*		text.setLayoutData(fillLayout);
		
//		FormLayout layout = new FormLayout();
//		text.setLayoutData(layout);
		
		text.setFont(font);
		text.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				font.dispose();
			}
		});
	}
}