package a000_komposition.eee;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import a000_komposition.a04_Menue_toolbar;

@SuppressWarnings("unused")
public class a00_menue2{

	static SelectionListener l = new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			System.out.println(e.widget.getData());
			MenuShell_composite.setLabelText((String) e.widget.getData());			}
	};


	private static Image createImage(String name) {
		return new Image(
				Display.getCurrent(),
				a04_Menue_toolbar.class.getResourceAsStream("../icons/" + name + ".gif"));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	//*******Textarea anlegen*****************************
	private void createWidgets(Shell s, Display d) {
		final Text text;
		final Font font = new Font(d, "Lucida Console", 10, SWT.NORMAL);
		text = new Text(s, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		text.setSize(100, 300);
		text.pack();
		text.setFont(font);
		text.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				font.dispose();
			}
		});
	}

	public void menue2(Shell s, Display d, Composite c){

		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;

		FillLayout fillLayout = new FillLayout (SWT.HORIZONTAL);

		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		item.setData("Textmodus");
		item.addSelectionListener(l);
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		item.setData("Formatieren");
		item.addSelectionListener(l);
		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);

		menu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);

		//	/***************************************************************************
		final ToolBar toolbar = new ToolBar(c, SWT.FLAT);
		GridLayout gl = new GridLayout();
		gl.marginWidth = 0;
		gl.marginHeight = 0;
		gl.horizontalSpacing = 0;
		gl.verticalSpacing = 0;
		c.setLayout(fillLayout);
		///***************************************************************************

		toolbar.setBackground(d.getSystemColor(SWT.COLOR_WHITE));
		menubar.setData(toolbar);

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");
		// */
	}
}