package a000_komposition.ddd;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


public class CDVerwaltung {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new CDVerwaltung().createShell(display);

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	Image bild;
	Text bildText;
	Combo bildTextAuswahl;
	Canvas Bild;
	List kategorien;
	Text name;
	Text telefon;
	//	Text telefonText;



	public Shell createShell(final Display display) {
		final Shell shell = new Shell(display);

		FormLayout flayout = new FormLayout();
		flayout.marginWidth = 25;					// 225
		flayout.marginHeight = 15;					// 115
		shell.setLayout(flayout);
		shell.setText("CD Verwaltung");

		Group sonstigeDaten = new Group(shell, SWT.NONE);
		sonstigeDaten.setText("Daten");
		FormLayout fgroupLayout = new FormLayout();
		fgroupLayout.marginWidth = 5;
		fgroupLayout.marginHeight = 5;					// 25 !!!
		sonstigeDaten.setLayout(fgroupLayout);

		// ********************************************
		//Composite c = new Composite(shell,SWT.NONE);
		a20_menue men = new a20_menue();
		men.menue(shell, display, shell);

		// *******************************************

		Label titel = new Label(shell, SWT.NONE);
		titel.setText("Titel:     ");
		this.bildText = new Text(shell, SWT.SINGLE | SWT.BORDER);

		Label gruppe = new Label(shell, SWT.NONE);
		gruppe.setText("Gruppe: ");

		this.bildTextAuswahl = new Combo(shell, SWT.NONE);
		this.bildTextAuswahl.setItems(new String[] { "Jethro Tull", "Pink Floyd",
				"Emmerson, Lake & Palmer", "Herbert Roth", "Rolling Stones" });

		Label photo = new Label(shell, SWT.NONE);
		photo.setText("Foto:");
		this.Bild = new Canvas(shell, SWT.BORDER);

		Button browse = new Button(shell, SWT.PUSH);
		browse.setText("Laden...");

		Button delete = new Button(shell, SWT.PUSH);
		delete.setText("Loeschen");

		Label kateg = new Label(shell, SWT.NONE);
		kateg.setText("Kategorien");
		this.kategorien = new List(shell, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		this.kategorien.setItems(new String[] { "Heavy Metal", "Folk", "Rock",
				"Blues", "Jazz", "Swing", "Klassik", "Instrumental",
				"Kinderlieder", "Wanderlieder", "Trinklieder", "Opernchoere",
		"Operettensongs" });

		Button enter = new Button(shell, SWT.PUSH);
		enter.setText("Enter");

		//Label label1 = new Label(shell, SWT.BORDER);
		//label1.setText("hallo");

		FormData data = new FormData();
		data.top = new FormAttachment(this.bildText, 0, SWT.CENTER);
		titel.setLayoutData(data);

		data = new FormData();
		data.left = new FormAttachment(titel, 5);
		data.right = new FormAttachment(100, 0);
		this.bildText.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(this.bildTextAuswahl, 0, SWT.CENTER);
		gruppe.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(this.bildText, 5);
		data.left = new FormAttachment(this.bildText, 0, SWT.LEFT);
		data.right = new FormAttachment(this.kategorien, -5);
		this.bildTextAuswahl.setLayoutData(data);

		data = new FormData(80, 80);
		data.top = new FormAttachment(this.bildTextAuswahl, 5);
		data.left = new FormAttachment(this.bildText, 0, SWT.LEFT);
		data.right = new FormAttachment(this.kategorien, -5);
		data.bottom = new FormAttachment(sonstigeDaten, -5);
		this.Bild.setLayoutData(data);

		// !!!!!! neu Grafik !!!!!
		this.Bild.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(final PaintEvent event) {
				if (CDVerwaltung.this.bild != null) {
					event.gc.drawImage(CDVerwaltung.this.bild, 0, 0);
				}
			}
		});

		data = new FormData();
		data.top = new FormAttachment(this.Bild, 0, SWT.TOP);
		photo.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(photo, 5);
		data.right = new FormAttachment(this.Bild, -5);
		browse.setLayoutData(data);
		browse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String fileName = new FileDialog(shell).open();
				if (fileName != null) {
					CDVerwaltung.this.bild = new Image(display, fileName);
				}
			}
		});

		data = new FormData();
		data.left = new FormAttachment(browse, 0, SWT.LEFT);
		data.top = new FormAttachment(browse, 5);
		data.right = new FormAttachment(this.Bild, -5);
		delete.setLayoutData(data);
		delete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (CDVerwaltung.this.bild != null) {
					CDVerwaltung.this.bild.dispose();
					CDVerwaltung.this.bild = null;
					CDVerwaltung.this.Bild.redraw();
				}
			}
		});

		data = new FormData(90, 140);
		data.top = new FormAttachment(this.Bild, 0, SWT.TOP);
		data.right = new FormAttachment(100, 0);
		data.bottom = new FormAttachment(enter, -5);
		this.kategorien.setLayoutData(data);

		data = new FormData();
		data.bottom = new FormAttachment(this.kategorien, -5);
		data.left = new FormAttachment(this.kategorien, 0, SWT.CENTER);
		kateg.setLayoutData(data);

		data = new FormData();
		data.right = new FormAttachment(100, 0);
		data.bottom = new FormAttachment(100, 0);
		enter.setLayoutData(data);
		enter.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				System.out.println("\nTitel: \t\t" + CDVerwaltung.this.bildText.getText());
				System.out.println("Gruppe: \t" + CDVerwaltung.this.bildTextAuswahl.getText());
				System.out.println("Name: \t\t" + CDVerwaltung.this.name.getText());
				System.out.println("Telefon: \t" + CDVerwaltung.this.telefon.getText());
				System.out.print("Kategorie:");
				String kateg[] = CDVerwaltung.this.kategorien.getSelection();
				for (int i = 0; i < kateg.length; i++) {
					System.out.println("\t" + kateg[i]);
				}
			}
		});

		data = new FormData();
		data.bottom = new FormAttachment(enter, -5);
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(this.kategorien, -5);
		sonstigeDaten.setLayoutData(data);

		Label labelName = new Label(sonstigeDaten, SWT.NULL);
		labelName.setText("Name:");

		Label tellefon = new Label(sonstigeDaten, SWT.PUSH);
		tellefon.setText("Telefon:");

		this.name = new Text(sonstigeDaten, SWT.SINGLE | SWT.BORDER);
		this.telefon = new Text(sonstigeDaten, SWT.SINGLE | SWT.BORDER);

		data = new FormData();
		data.top = new FormAttachment(this.name, 0, SWT.CENTER);
		this.name.setLayoutData(data);

		data = new FormData();
		data.top = new FormAttachment(this.telefon, 0, SWT.CENTER);
		tellefon.setLayoutData(data);

		data = new FormData();
		data.left = new FormAttachment(tellefon, 5);
		data.right = new FormAttachment(100, 0);
		this.name.setLayoutData(data);

		data = new FormData();
		data.left = new FormAttachment(this.name, 0, SWT.LEFT);
		data.right = new FormAttachment(100, 0);
		data.top = new FormAttachment(55, 0);
		this.telefon.setLayoutData(data);

		shell.pack();

		return shell;
	}
}