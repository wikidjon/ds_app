package a000_komposition.ggg;

import org.eclipse.swt.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class ShowCTabFolder {
	private int insertMark = -1;
	private CTabFolder tabFolder;

	public void run(Display display, Shell shell, Composite c) {
		createContents(display, shell, c);
	}

	private void createContents(Display d, Shell shell, Composite composite) {
		composite.setLayout(new GridLayout(1, true));

		Composite composite2 = new Composite(composite, SWT.NONE);
		composite2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		composite2.setLayout(new RowLayout());
		createButtons(composite2);

		tabFolder = new CTabFolder(composite, SWT.BORDER);
		tabFolder.setBorderVisible(true);
		tabFolder.setLayoutData(new GridData(GridData.FILL_BOTH));

        Composite c1 = new Composite(tabFolder, SWT.BORDER);
        c1.setLayout(new FillLayout());

//*************
		
		// Add a listener to get the close button on each tab
		tabFolder.addCTabFolderListener(new CTabFolderAdapter() {
			public void itemClosed(CTabFolderEvent event) {
			}
		});
	}

	/**
	 * Creates the buttons for moving the insert mark and adding a tab
	 * 
	 * @param composite
	 *            the parent composite
	 */
	private void createButtons(Composite composite) {
		// Move mark left
		Button button = new Button(composite, SWT.PUSH);
		button.setText("<<");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (insertMark > -1) {
					--insertMark;
					resetInsertMark();
				}
			}
		});

		// Move mark right
		button = new Button(composite, SWT.PUSH);
		button.setText(">>");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (insertMark < tabFolder.getItemCount() - 1) {
					++insertMark;
					resetInsertMark();
				}
			}
		});

		// Add a tab
		button = new Button(composite, SWT.PUSH);
		button.setText("Add Tab");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				CTabItem a = new CTabItem(tabFolder, SWT.NONE, insertMark + 1);
						a.setText("das ist Tab (" + (insertMark + 1) + ")");

//				Text t = new Text( SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
//		        tabFolder.setControl(c1);
//				tabFolder.setData(t);
//				a.setControl(t);

				
				
			}
		});
	}

	/**
	 * Moves the insert mark to the new location
	 */
	private void resetInsertMark() {
		tabFolder.setInsertMark(insertMark, true);

		// Workaround for bug #32846
		if (insertMark == -1) {
			tabFolder.redraw();
		}
	}

}
