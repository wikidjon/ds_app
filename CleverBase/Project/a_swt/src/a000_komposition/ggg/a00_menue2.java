package a000_komposition.ggg;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
//import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import a000_komposition.a04_Menue_toolbar;  //fuer icons -> verbessern!!
import a000_komposition.fff.a_MenuShell_composite;
import a13_layouts1.a_Table;

@SuppressWarnings("unused")
public class a00_menue2{

	static Label label1;
		
	static SelectionListener l = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			System.out.println(e.widget.getData());
			setLabelText((String) e.widget.getData());			}
	};

	
	
	public void menue2(Shell s, Display d, Composite c){

		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;
	
		FillLayout fillLayout = new FillLayout (SWT.HORIZONTAL);
		
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);
	
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		item.setData("Textmodus");
		item.addSelectionListener(l);
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		item.setData("Formatieren");
		item.addSelectionListener(l);
		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);
		
		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);
		
//***************************************************************************		
//		final ToolBar toolbar = new ToolBar(c, SWT.FLAT);
//		GridLayout gl = new GridLayout();
//		gl.marginWidth = 0;
//		gl.marginHeight = 0;
//		gl.horizontalSpacing = 0;
//		gl.verticalSpacing = 0;
//		c.setLayout(fillLayout);

///***************************************************************************		

		final CoolBar coolBar = new CoolBar(s,SWT.NONE);
        coolBar.setSize(395,70);
        coolBar.setLocation(0,0);
		
//		toolbar.setBackground(d.getSystemColor(SWT.COLOR_WHITE));
		menubar.setData(coolBar);

		createToolItem(coolBar, "hotel");
		createToolItem(coolBar, "open1");
		createToolItem(coolBar, "open");
//		createSeparator(toolbar);
		createToolItem(coolBar, "reconnect");
//		createSeparator(toolbar);
		createToolItem(coolBar, "down");
		createToolItem(coolBar, "down");
		createToolItem(coolBar, "down");		
	}
	
	
	static void setLabelText(String object) {
	//	label1.setText("HAllo");
		System.out.println("Test" + object);
	}

	private static void createToolItem(CoolBar coolbar, String name) {
		CoolItem item = new CoolItem(coolbar, SWT.PUSH);  
		item.setImage(createImage(name));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static Image createImage(String name) {
		return new Image(
			Display.getCurrent(),
			a04_Menue_toolbar.class.getResourceAsStream("../icons/" + name + ".gif"));
			//a04_Menue_toolbar.class.getResourceAsStream(name));
	}
	
	//*******Textarea anlegen*****************************
	private void createWidgets(Shell s, Display d) {   
		final Text text;
		final Font font = new Font(d, "Lucida Console", 10, SWT.NORMAL);
		text = new Text(s, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		text.setSize(100, 300);
		text.pack();
		text.setFont(font);
		text.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				font.dispose();
			}
		});
	}
}
