package a000_komposition;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

//import a04_menue.BorderLayout;

/** inaktive/aktive Pulldown-Men�s... */
@SuppressWarnings("unused")
public class a03_MenuShell_gridlayout{

	public static void main(String[] arguments) {
		final Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(600, 400);
		shell.setImage(new Image(display, "src/icons/hotel.gif"));

		//		BorderLayout bl = new BorderLayout() ;
		//		BorderLayout.borderlayout(display, shell);
		//		shell.setLayout(bl);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;



		//		final Composite comp = new Composite(shell,SWT.BORDER);
		//		comp.setLayoutData(gridData);


		Menu menubar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menubar);

		Menu menu;
		menu = new Menu(menubar);

		//*		menu.setData(BorderLayout.NORDEN);

		final Composite composite1 = new Composite(shell,SWT.BORDER);
		composite1.setBackground(new Color(display,31,133,31));
		composite1.setLayoutData(gridData);


		final Label label2 = new Label(composite1,SWT.NONE);
		label2.setText("Das ist ein gr�nes Composite ");
		label2.setBounds(100,100,250,15);

		final Composite composite2 = new Composite(shell,SWT.BORDER);
		//		composite1.setBackground(new Color(display,31,133,31));
		//		composite2.setLayoutData(gridData);


		final Label label = new Label(composite2, SWT.BORDER);
		//*		label.setLayoutData(BorderLayout.SUEDEN);
		//		label.setLayoutData(gridData);


		Listener armListener = new Listener() {			//Arm, wenn widget fuer Selektion vorbereitet wird
			@Override
			public void handleEvent(Event event) {
				MenuItem item = (MenuItem) event.widget;
				label2.setText(item.getText());
				label2.update();
			}
		};
		Listener showListener = new Listener() {
			@Override
			public void handleEvent(Event event) {
				Menu menu = (Menu) event.widget;
				MenuItem item = menu.getParentItem();
				if (item != null) {
					label2.setText(item.getText());
					label2.update();
				}
			}
		};
		Listener hideListener = new Listener() {
			@Override
			public void handleEvent(Event event) {
				label2.setText("");
				label2.update();
			}
		};

		SelectionListener l = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
				label.setText((String)e.widget.getData());
				if(e.widget.getData().equals("oeffnen"))
				{
					composite1.setBackground(new Color(display,255,133,31));
				}else
				{
					composite1.setBackground(new Color(display,31,133,255));
				}
			}
		};

		//***********************************************
		//menue

		MenuItem item;

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);  //neues Menue an MB

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("Extras");
		item.setMenu(menu);

		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Textmodus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Formatieren");
		final MenuItem formatItem = item;

		//Textmodus und Formatieren haengen an menu
		menu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}