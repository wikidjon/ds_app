package a70_grafik;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 */
@SuppressWarnings("unused")
public class Animations {
	public static void main(String[] args) {
		new Animations();
	}
	Display display = new Display();

	Shell shell = new Shell(this.display);

	public Animations() {
		this.shell.setLayout(new FillLayout());

		ImageLoader imageLoader = new ImageLoader();
		final ImageData[] imageDatas = imageLoader.load("src/icons/eclipse-ani.gif");

		final Image image = new Image(this.display, imageDatas[0].width, imageDatas[0].height);
		final Canvas canvas = new Canvas(this.shell, SWT.NULL);

		canvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				e.gc.drawImage(image, 0, 0);
			}
		});

		final GC gc = new GC(image);

		final Thread thread = new Thread() {
			int frameIndex = 0;
			@Override
			public void run() {
				while (!isInterrupted()) {
					this.frameIndex %= imageDatas.length;

					final ImageData frameData = imageDatas[this.frameIndex];
					Animations.this.display.asyncExec(new Runnable() {
						@Override
						public void run() {
							Image frame =
									new Image(Animations.this.display, frameData);
							gc.drawImage(frame, frameData.x, frameData.y);
							frame.dispose();
							canvas.redraw();
						}
					});

					try {
						// delay
						Thread.sleep(imageDatas[this.frameIndex].delayTime * 10);
					} catch (InterruptedException e) {
						return;
					}

					this.frameIndex += 1;
				}

			}
		};

		this.shell.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				thread.interrupt();
			}
		});

		this.shell.setSize(400, 200);
		this.shell.open();

		thread.start();

		// Set up the event loop.
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				// If no more entries in event queue
				this.display.sleep();
			}
		}

		this.display.dispose();
	}

	private void init() {

	}
}
