package a05_list;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a05_liste_auswahl_aus {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		final List list = new List(shell, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		for (int i = 0; i < 128; i++) {
			list.add("Eintrag " + i);
		}
		list.setBounds(0, 0, 100, 100);			///    !!!!!

		list.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				String string = "";
				int[] selection = list.getSelectionIndices();
				for (int i = 0; i < selection.length; i++) {
					string += selection[i] + " ";
				}
				System.out.println("Auswahl={" + string + "}");
			}
		});

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
