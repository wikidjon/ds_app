package a05_list;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a05_liste_auswahl_alles {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setLayout(new GridLayout(2, false));
		(new Label(shell, SWT.NULL))
		.setText("Waehlen Sie eine geeignete Programmiersprache: ");

		final Combo combo = new Combo(shell, SWT.NULL);
		String[] progsprachen = new String[] { "Java", "C", "C++", "SmallTalk"}; // ""
		Arrays.sort(progsprachen);

		combo.setText(progsprachen[0]);			//	!!!!!!!!!
		//combo.select(0);

		for (int i = 0; i < progsprachen.length; i++) {
			combo.add(progsprachen[i]);
		}



		//combo.add("Perl", 5);   // aendern auf 4!!!
		//combo.setItem(5, "Perl");

		combo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				System.out.println("Default index: "
						+ combo.getSelectionIndex()
						+ ", gewaehlter Eintrag: "
						+ (combo.getSelectionIndex() == -1 ? "<null>" : combo
								.getItem(combo.getSelectionIndex()))
								+ ", Inhalt des Feldes: " + combo.getText());
				String text = combo.getText();
				if (combo.indexOf(text) < 0) { // nicht in der Liste
					combo.add(text);
					// sortierung
					String[] items = combo.getItems();
					Arrays.sort(items);
					combo.setItems(items);
				}
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Gewaehlter index: "
						+ combo.getSelectionIndex() + ", gewaehletr Eintrag: "
						+ combo.getItem(combo.getSelectionIndex())
						+ ", Inhalt des Feldes: " + combo.getText());
			}
		});

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

		display.dispose();
	}
}
