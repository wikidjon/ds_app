package eigenes;


import java.util.Timer;
import java.util.TimerTask;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;


public class SuchFenster
{ 
	final Shell dialog = new Shell(global.getShell(), SWT.TITLE| SWT.PRIMARY_MODAL);
	Display display=dialog.getDisplay();
	int status;//Wert f�r die Progressbar
	
public void run()
 {
	 	dialog.setLocation(200, 200);
		dialog.setText("Suche l�uft...");
		dialog.setSize(500, 160);
		dialog.open();
		final Timer timer = new Timer();
		final ProgressBar bar = new ProgressBar(dialog, SWT.SMOOTH);
		bar.setBounds(0, 80, 500, 50);
	
		Label verzeichnis = new Label(dialog, SWT.LEFT);
		verzeichnis.setText("Datei:");
		verzeichnis.setBounds(0,0,100,0);
		verzeichnis.pack();
	    
	    Label anzahl = new Label(dialog, SWT.LEFT);
	    anzahl.setText("Anzahl:");
	    anzahl.setBounds(0,30,100,0);
	    anzahl.pack();
	    
        final Label anzahl2 = new Label(dialog, SWT.LEFT);
    	final Label verzeichnis2 = new Label(dialog, SWT.LEFT); 
    	
		//Rgelm��ig die Anzeige erneuern
        timer.schedule(new TimerTask() {
            public void run() {

            	display.asyncExec(new Runnable() {
         		    public void run() {
         		        // UI Zugriff
         		    	String dateianzahl=String.valueOf((AlleDaten.dateiliste.size()));	
         		    	if(!(AlleDaten.dateiliste.size()<=0)&&global.SucheImGange){//falls noch keine Datei gefunden wurde nicht auf das Array zugreifen und die suche muss noch im gange sein da sonnst dieses Fenster beendet wird
         		    		verzeichnis2.setText(AlleDaten.dateiliste.get(AlleDaten.dateiliste.size()-1));//regelm��ig neues Verzeichnis anzeigen (immer das zuletzt gefundene)
         		    		verzeichnis2.setBounds(70,0,100,0);
         		    		verzeichnis2.pack();
         		    	}
         		   	anzahl2.setText(dateianzahl);
         		    anzahl2.setBounds(70,30,100,0);
         		    anzahl2.pack();
         		    //Progressbar
         		   int maximum = bar.getMaximum();
         		    if(status>=maximum){//Z�hler f�r Progressbar
         		    status=1;
         		    }
         		    status++;
         		   bar.setSelection(status);
         		   
         		if(!global.SucheImGange){//Timer abbrechen wenn die suche beendet ist	
            	timer.cancel();
            	dialog.close();
            	}}});} }, 50L, 50);	
 
	    
		
		while (!dialog.isDisposed ()) {
			if (!dialog.getDisplay().readAndDispatch ()) dialog.getDisplay().sleep ();
		}
		
 }
 }