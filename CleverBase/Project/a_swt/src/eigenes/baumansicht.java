package eigenes;


import java.util.ArrayList;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;


public class baumansicht
{
	//globale variablen
	Tree tree;
	TreeItem parentKategorien;

public void getTree(Shell shell)
{
	tree = new Tree(shell, SWT.BORDER);	 
	//Alle St�cke Anzeigen
	ListeF�llen();
	
	//Aktion beim Klicken 
	tree.addSelectionListener(new SelectionListener()
	{
		 public void widgetSelected(SelectionEvent e) 
         {
        	 TreeItem item 	= (TreeItem) e.item;
        	 TreeItem tmp	=(TreeItem)item.getParentItem();//�bergeordnete Kategorie
        	 //Wenn der Eintrag "Alle" gew�hlt wurde 
        	 if(item.toString().equals("TreeItem {Alle}")){
        	 //abfragen ob array leer!!!!
        		 	global.tabelle.showInterpret(item.toString(),"AlleDatenAnzeigen");//Tabelle neu zeichnen
        		 //Wenn der Eintrag "Playlists" gew�hlt wurde 	 
        	 }else  if(item.toString().equals("TreeItem {Playlists}"))
        	 {
     }
        	//Wenn der Eintrag "Kategorie" gew�hlt wurde 	 
        	 //Alle vorher festgelegten Namen im Tree ignorieren und nur w�rend der Laufzeit eingef�gten Namen verarbeiten
        	//�bergeben werden die gew�hlten Eintr�ge
        	 if(!(tmp==null))//falls kein �bergeordnetes existiert
        	{
        		 //item=ausgew�hlter Eintrag im Tree
        		if(tmp.toString().equals("TreeItem {Interpret}")){
        			global.tabelle.showInterpret(item.toString(),"Interpret");//Tabelle neu zeichnen
            	}else if(tmp.toString().equals("TreeItem {Album}")){
            		global.tabelle.showInterpret(item.toString(),"Album");//Tabelle neu zeichnen
            	}
            	else if(tmp.toString().equals("TreeItem {Erscheinungsjahr}")){
            		global.tabelle.showInterpret(item.toString(),"Jahr");//Tabelle neu zeichnen
            	}	
        	}	 
         }

         public void widgetDefaultSelected(SelectionEvent e) 
         {}
     }
);
};
	  
//Vorarbeit zum F�llen der Liste
public void ListeF�llen()
{
	tree.clearAll(true);
	tree.removeAll();

	//Icons einlesen
	Image picAlle = null,picKategorien=null,picPlaylist=null;
	String vereichnis=System.getProperty("user.dir");//Verzeichnis aus dem das Prohgramm gestartet wurde

	try {
		Display d=tree.getDisplay();//Icons auf Toolbar zeichen
		picAlle = new Image(d, vereichnis+"/mp3-icon.png");
		picKategorien = new Image(d, vereichnis+"/FolderMusic.png");
		picPlaylist = new Image(d, vereichnis+"/MyDocuments128.png");	
	} catch (Exception e1) {
		e1.printStackTrace();
	}

	TreeItem parentall = new TreeItem(tree, 0);
	parentall.setText("Alle");
	parentall.setImage(picAlle);
	
	parentKategorien = new TreeItem(tree, 0);
	parentKategorien.setText("Kategorien");
	parentKategorien.setImage(picKategorien);
	
	TreeItem parent2 = new TreeItem(tree, 0);
	parent2.setText("Playlists");
	parent2.setImage(picPlaylist);
	
	ListeF�llenEx(null);
}
	  
	  
//F�llen der Liste
//immer mit "null" aufrufen z�hlt automatisch!!
public  void ListeF�llenEx(String Typ)
{
	//Icons einlesen
	Image picFiles = null,picFolder=null;
	String vereichnis=System.getProperty("user.dir");//Verzeichnis aus dem das Prohgramm gestartet wurde
	try {
		Display d=tree.getDisplay();//Icons auf Toolbar zeichen
		picFiles = new Image(d, vereichnis+"/Music128.png");
		picFolder = new Image(d, vereichnis+"/FolderMusic.png");
	} catch (Exception e1) {
		e1.printStackTrace();
	}
	

	Boolean run=false;//zum �berpr�fen ob alles geschrieben wurde
	if(Typ==null){//f�r den ersten Aufruf der Funktion
		Typ="Interpret";
		run=true;
	}else if(Typ.equals("Interpret")){
		Typ="Album";
		run=true;
	}else if(Typ.equals("Album")){
		Typ="Erscheinungsjahr";
		run=true;
	}
	
	
if(run)//Endlosschleife verhindern
{
		  //jeweils nur einmal den selben Eintrag egal wieviele Eintr�ge doppelt auftreten
		  ArrayList<String> tmp = new ArrayList<String>();
		
		  Boolean doppelt=false;//hilfe zum Feststellen ob eine �bereinstimmung gefunden wurde
		  //Interpreten suchen
		  for (int i = 0; i < global.getDateilisteGr��e(); i++) //einzelne Dateien durchgehen
		  {
			  //unterscheiden wonach gesucht und ausgegeben wird 
			  String temp = "";
			  if(Typ.equalsIgnoreCase("Interpret")){
			  temp=global.getDateilisteInterpret(i);
			  }else if(Typ.equalsIgnoreCase("Album")){
			  temp=global.getDateilisteAlbum(i); 
			  }else if(Typ.equalsIgnoreCase("Erscheinungsjahr")){
			  temp=global.getDateilisteJahr(i);
			  }

			
			  doppelt=false;//initialiseren bei jedem Durchlauf
			  for (int j = 0; j <tmp.size(); j++){//mit Liste vergleichen(tmp) ob der Eintrag schon existiert
				if(temp.equalsIgnoreCase(tmp.get(j))){//wenn die Namen �bereinstimmen �berspringen
				   doppelt=true;//falls der Eintrag dopptelt vorkommt
				  }
			  }
			   if(!doppelt){//wenn der Eintrag noch nicht in der Liste ist:aufnehmen
			   	if(!temp.isEmpty())//Falls kein Eintrag vorhanden ist nicht aufnehmen da sonnst leeres Feld
				   tmp.add(temp);
				}
		  }
		  
		  //Kategorie Interpret
		  TreeItem parent = new TreeItem(parentKategorien, 0);//unter dem �bergeordneten Feld (z.B.Kategorien) schreiben
		  parent.setText(Typ);//Den Namen des Parent (Durchlauf aus Typ)
		  parent.setImage(picFolder);//bild einf�gen
		  
			for (int i = 0; i < tmp.size(); i++){ //Eintr�ge in die Liste einf�gen
				  TreeItem child = new TreeItem(parent, 0);
				  child.setText(tmp.get(i));//gefundene Eintr�ge einf�gen
				  child.setImage(picFiles);
			 }
		  
		  
		  
			if(!Typ.equals("Jahr")){//Der letzte Eintrag der in den Tree geschrieben werden muss, ansonnsten nicht mehr aufrufen (kein Wert mehr �brig f�r Abfrage oben)
				ListeF�llenEx(Typ);	//sich selbst aufrufen mit dem n�chsten
			}
}		
}



public void setSize(int posx,int posy,int sizex,int sizey)//�ndern des Baums (Position und Gr��e)
{
		  tree.setBounds(posx, posy, sizex, sizey);
}
		  
}