package eigenes;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.DecoderException;
import javazoom.jl.decoder.Equalizer;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.decoder.Obuffer;
import javazoom.jl.player.AudioDevice;
import javazoom.jl.player.FactoryRegistry;
import javazoom.jl.player.advanced.AdvancedPlayer;

// mp3 player
//NICHT AKTIV
//start by typing : .start();
class PlayerThread extends Thread
{
	FileInputStream fin = null;
	AdvancedPlayer p = null;
	String file;
	static Boolean run=true;//zum beenden des Threads
	
	PlayerThread(String file) 
	{           
		this.file=file;
	} 
	
@SuppressWarnings("unused")
public void Play()
{
	
 
			try {
				fin = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			 BufferedInputStream in = new BufferedInputStream(fin);
			 Bitstream bs = new Bitstream(in);

			 AudioDevice device = null;
			try {
				device = FactoryRegistry.systemRegistry().createAudioDevice ();
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}
			 Decoder decoder = new Decoder();

			 float[] esettings = new float[32];

			 for(int i = 0; i < 32; i++) {
			 esettings[i] = +1.0f;
			 }

			 Equalizer equalizer = new Equalizer(esettings);
			 decoder.setEqualizer(equalizer);

			
			try {
				p = new AdvancedPlayer(in, device);
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}

			 try {
				device.open(decoder);
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}
			 Header hd = null;
			try {
				hd = bs.readFrame();
			} catch (BitstreamException e) {
				e.printStackTrace();
			}
			 try {
				Obuffer ob = decoder.decodeFrame(hd, bs);
			} catch (DecoderException e) {
				e.printStackTrace();
			}

			 try {
				 
				
				 
				p.play();
				//currentThread().stop();
			} catch (JavaLayerException e) {
				e.printStackTrace();
			}	
}

 public void run()
 {
	Play();
 }
 public void stopThread()
 {
	 Thread.currentThread().interrupt();
//this.run=false;
//Thread.interrupted();
//stop();
 }
}