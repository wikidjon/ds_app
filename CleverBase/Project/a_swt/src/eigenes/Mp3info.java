package eigenes;


import java.io.IOException;
import org.farng.mp3.MP3File;
import org.farng.mp3.TagException;
import org.farng.mp3.id3.AbstractID3v2;
import org.farng.mp3.id3.ID3v1;



public class Mp3info 
{
	
private  MP3File InizialiserenMp3(String File)
{
	MP3File mp3 = null;
	
	try {
		mp3 = new MP3File(File);
	} catch (IOException e) {
	//	mp3=null;//setzen falls auf die Datei nicht zugegriffen werden kann
		e.printStackTrace();	
	} catch (TagException e) {
		e.printStackTrace();
	}
	
//notwendig f�r das Auslesen der Bitrate
	try {
		if(!(mp3==null))
		mp3.seekMP3Frame();
	} catch (IOException e) {
		e.printStackTrace();
	} 

	return mp3;
}
		
public String getArtist(String File) 
{       
		String Artist=""; 
		MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
	    if(!(mp3==null)){
		if (mp3.hasID3v1Tag()) 
		{
		ID3v1 v1tag = mp3.getID3v1Tag();
		Artist = v1tag.getArtist();
		}
	    }

		  return Artist; 
} 
				
public String getTitle(String File) 
{          			
		String Title="";
		MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
		if(!(mp3==null)){
		if (mp3.hasID3v1Tag()) 
		{
		ID3v1 v1tag = mp3.getID3v1Tag();
		Title = v1tag.getTitle();
		}

		if (mp3.hasID3v2Tag()) {
		AbstractID3v2 v2tag = mp3.getID3v2Tag();
		Title = v2tag.getSongTitle();
		} 
		    }
		return Title;     
		
} 
			
public String getAlbum(String File) 
{          
		String Album = new String();
		MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
	
		if(!(mp3==null)){ 
		if (mp3.hasID3v1Tag()) {
		ID3v1 v1tag = mp3.getID3v1Tag();
		Album = v1tag.getAlbum();
		}
		    }
		return Album;     
		
} 
		

public int getBitrate(String File) 
{          
		int intBitrate = 0;
		MP3File mp3 = InizialiserenMp3(File);			
		//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
		 if(!(mp3==null)){ 
			 intBitrate = mp3.getBitRate();
		 }
		return intBitrate;     
} 
				
public double getFrequency(String File) 
{          
	double dblFrequency = 0;
	MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
	
	 if(!(mp3==null)){
		dblFrequency = mp3.getFrequency();
	    }
	
		return dblFrequency;     	
} 
				
public  int getMode(String File) 
{          
		byte btMode = 0;
		MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
		
		if(!(mp3==null)){
			btMode = mp3.getMode();
	    }
		return btMode;     
		
} 
				
public  int getModeEx(String File) 
{          
		byte btModeEx = 0; 
		MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert
	 
		if(!(mp3==null)){
		btModeEx = mp3.getModeExtension();
	    }
		return btModeEx;     
} 
 

public String getYear(String File) 
{ 
	String Year="";
	MP3File mp3 = InizialiserenMp3(File);//mp3 Bibiliothek initialisieren und Datei �bergeben und R�ckwert

	if(!(mp3==null)){
	if (mp3.hasID3v1Tag()) {
	ID3v1 v1tag = mp3.getID3v1Tag();
	Year = v1tag.getYearReleased();
	}

	if (mp3.hasID3v2Tag()){ 
	AbstractID3v2 v2tag = mp3.getID3v2Tag();
	Year = v2tag.getYearReleased();
	} 

	//�berpr�fen ob ein g�ltiges Datum existiert, falls nicht "ka" zur�ckgeben.
	//falls andere Zeichen au�er Zahlen sich im String befinden abfangen und Wert auf 0000 setzen
	int temp;
	try{
	temp = Integer.valueOf( Year ).intValue();
	}
	catch ( NumberFormatException nfe ){
	//System.out.println("Keine g�ltige Zahl oder Buchstaben im String!");
	temp=0000;//Setzen, damit keine Buchstaben vorhanden sind und nich null
	}

	//wenn datum nicht in diesem bereich
	if(temp<1600||temp>2050){
		Year="ka";//keine angeben (kein Datum ausgelesen)
	}
}
	return Year;       
}
}
