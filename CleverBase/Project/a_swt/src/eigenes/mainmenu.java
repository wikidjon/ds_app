package eigenes;



import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;


public class mainmenu
{
	//globale Var
	static Shell shell;
	static FileList filelist;
	static Display display;
	
	 @SuppressWarnings("static-access")
	public void getMenu(final Shell shellEx,Display display)
	 {
		 this.display=display;
		 shell=shellEx;
		 
	SelectionListener l = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			SelectedMenuItem(e);//�berpr�fung welches Men�item gew�hlt wurde
		}};

	Menu menubar = new Menu(shell, SWT.BAR);
	Menu menu;
	MenuItem menuentry;
	menu = new Menu(menubar);
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Playlist �ffnen...");
	menuentry.setData("OpenPlaylist");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL + 'O');
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Playlist Speichern\tStrg+S");
	menuentry.setAccelerator(SWT.CTRL + 'S');
	menuentry.setData("savePlaylist");
	menuentry.addSelectionListener(l);
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Mitte Exportieren \tStrg+S");
	menuentry.setAccelerator(SWT.CTRL +SWT.ALT+ 'E');
	menuentry.setData("ExportMiddle");
	menuentry.addSelectionListener(l);
	
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Beenden");
	menuentry.setData("Beenden");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL +SWT.ALT+ 'X');
	
	menuentry = new MenuItem(menubar, SWT.CASCADE);
	menuentry.setText("Datei");
	menuentry.setMenu(menu);

	//********************************
	menu = new Menu(menubar);  //neues Menue an MB
	
	menuentry = new MenuItem(menubar, SWT.CASCADE);
	menuentry.setText("Suche");
	menuentry.setMenu(menu);
	

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Alles Durchsuchen");
	menuentry.setData("Gesammte System");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL +SWT.ALT+ 'F');

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Verzeichnis durchsuchen");
	menuentry.setData("Verzeichnis durchsuchen");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL + 'F');
	
	new MenuItem(menu, SWT.SEPARATOR);
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Suche �ffnen");
	menuentry.setData("OpenSuche");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL +SWT.ALT +'O');

	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Suche speichern");
	menuentry.setData("SaveSearch");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL +SWT.ALT+ 'S');
	
	menu = new Menu(menubar);  //neues Menue an MB
	
	menuentry = new MenuItem(menubar, SWT.CASCADE);
	menuentry.setText("Playlist");
	menuentry.setMenu(menu);

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Abspielen");
	menuentry.setData("Abspielen");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL + 'P');
	

	new MenuItem(menu, SWT.SEPARATOR);

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Neue Playlist");
	menuentry.setData("newPlaylist");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL +SWT.ALT+ 'N');
	
	new MenuItem(menu, SWT.SEPARATOR);

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Playliste �ffnen");
	menuentry.setData("OpenPlaylist");
	menuentry.addSelectionListener(l);
	
	

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Playliste speichern");
	menuentry.setData("savePlaylist");
	menuentry.addSelectionListener(l);
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Playliste exportieren");
	menuentry.setData("exportPlaylist");
	menuentry.addSelectionListener(l);
	menuentry.setAccelerator(SWT.CTRL + 'E');
	
	
	menu = new Menu(menubar);  //neues Menue an MB
	
	menuentry = new MenuItem(menubar, SWT.CASCADE);
	menuentry.setText("?");
	menuentry.setMenu(menu);

	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("�ber");
	menuentry.setData("�ber");
	menuentry.addSelectionListener(l);
	
	menuentry = new MenuItem(menu, SWT.PUSH);
	menuentry.setText("Hilfe");
	menuentry.setData("Hilfe");
	menuentry.addSelectionListener(l);
	
	shell.setMenuBar(menubar);//Men� einf�gen
	

	//Thread beenden wenn programm beendet wird
	 shell.addShellListener(new ShellAdapter(){
		public void shellClosed(ShellEvent arg0){
	     }});
}
	 
//�berpr�fung welches Men�item gew�hlt wurde
	 static private void SelectedMenuItem(SelectionEvent e) 
	 {
			Object object = ((MenuItem) e.getSource()).getData();
			if (object == "OpenSuche"){
				global.gem.openGespeicherteSuche();
			} 
			else if (object == "Beenden") {
				System.exit(0);
			}else if (object == "Verzeichnis durchsuchen") {
				global.gem.systemDurchsuchenVerzeichnis();	
			}else if (object == "Gesammte System") {
				global.gem.systemDurchsuchenAlles();
			}else if (object == "savePlaylist") {
				global.playlist.savePlaylist();
			}else if (object == "OpenPlaylist") {
				global.playlist.openPlaylist();
			}else if (object == "newPlaylist") {
				global.playlist.newPlaylist();
			}else if (object == "exportPlaylist") {
				global.gem.exportPlaylistMediaplayer();
			}else if (object == "Abspielen") {
				global.gem.PlaylistAbspielen();	
			}else if (object == "SaveSearch") {
				global.gem.saveGespeicherteSuche();	
			}else if (object == "ExportMiddle") {
				global.gem.exportPlaylistMediaplayerMitte();	
			}
	 } 
}