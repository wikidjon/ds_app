package eigenes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


public class entrywindow
{
	int x=0;
	public int run()
	{
		final Shell dialog = new Shell(global.getShell(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setLocation(100, 100);
		dialog.setText("Position eingeben");
		dialog.setSize(200, 50);
		dialog.open();
		
		final Text eingabe=new Text(dialog,SWT.FLAT);
		eingabe.setBounds(0,0,60,25);
		
		//Button erstellen
		Button ok=new Button(dialog, 0);
		ok.setBounds(70,0,60,25);
		ok.setText("Ok");
		
		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				//�berpr�fen ob die Eingabe g�ltig ist (keine Buchstaben, Sonderzeichen, Leer)
				try{
					x = Integer.valueOf(eingabe.getText()).intValue();
					dialog.close();//falls der Wert korrekt ist Fenster schlie�en
				}
				catch ( NumberFormatException nfe ){
					//Zahl auf definierten Wert setzen
					//System.out.println("Keine g�ltige Zahl!:"+x);
					x=0;
				}
				
			//Falls ein Wert gew�hlt wurde, der gr��er ist als die maximale Anzahl an Eintr�gen in der Playlist
			int gr��e=global.dateilistePlaylist.size();
			if(x>gr��e-1){
				x=-1;
			}
			if(x<-1){	//und falls ein negativer Wert eingegeben wurde "-1= nichts tun"
				x=-1;
			}}});


		//Button erstellen
		Button abbrechen=new Button(dialog, 0);
		abbrechen.setBounds(130,0,60,25);
		abbrechen.setText("Abbrechen");
		
		abbrechen.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				x=-1;//auf -1 setzen damit ein abbruch festgestellt werden kann
				dialog.close();//Fenster schlie�en
			}});

		
		while (!dialog.isDisposed ()) {
			if (!dialog.getDisplay().readAndDispatch ()) dialog.getDisplay().sleep ();
		}
		//dialog.getDisplay().dispose (); 
		
		return x;
		//gibt Eintrag zur�ck
	}
	
	
}