package eigenes;

import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;


public class dateien {
	

static Display display=global.display;
static	final Shell shell = global.shell;



public static void main (String [] args) {

	mainmenu menu=new mainmenu();//men� einf�gen
	menu.getMenu(shell,display);
	
	//Toolbar einrichten
	global.toolbarex.getToolbar(shell, display);
	//Playlist playlist=new Playlist();//Klasse der Playlist

	//Baumansicht einrichten(links)
	global.baum.getTree(shell);
	
	//Tabelle f�r Musikst�cke (Mitte)
	global.tabelle.getTabelle(shell);

	//Playlist einrichten(links)
	global.playlist.getPlaylist(shell);
	
	shell.open ();	//Fenster �ffnen
	refreshSize();  //Fenster zeichnen 
	

	 //�nderung der fenstergr��e
	 shell.addControlListener(new ControlListener() { 
         public void controlMoved(ControlEvent e) {
         }
         public void controlResized(ControlEvent e) {
        	 refreshSize();
         } });
      
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose (); 
}




private static void refreshSize()
{
	 //H�he und Breite der beiden linken Objekte
	 int breiteLinks= shell.getClientArea ().width/4;
	 int x=shell.getClientArea ().width;
	 int y=shell.getClientArea ().height-global.toolbarex.toolbar.getSize().y;
	
	 int temp=(int)Math.round(y/1.3);//float in int umwandeln da setSize kein float akzeptiert

	 global.baum.setSize(
			 0,
			 global.toolbarex.toolbar.getSize().y,
			 breiteLinks,
			 temp);

	 global.playlist.setSize(
			 0, 
			 (global.baum.tree.getSize().y+88), 
			 x,
			 y/4);

	 global.tabelle.setSize(
			 x/4, 
			 global.toolbarex.toolbar.getSize().y, 
			 x-breiteLinks, 
			 temp);
 
}}



