package eigenes;


import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class Tabelle
{ 
	//Globale variablen
	Table table;
	int Spalten=7;
	TableColumn[] column = new TableColumn[Spalten];
	Image picFiles = null;//bild
	
	//speichern ob die Sortierung absteigend oder aufsteigend erfolgt ist
	
	public void getTabelle(Shell shell)
	{
		 table = new Table(shell, SWT.BORDER);
	  	 table.setRedraw(false);
	  
	  	 table.setHeaderVisible(true);

		String vereichnis=System.getProperty("user.dir");//Verzeichnis aus dem das Prohgramm gestartet wurde
		try {
			Display d=table.getDisplay();//Icons auf Toolbar zeichen
			picFiles = new Image(d, vereichnis+"/mp3-icon.png");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		

	  column[0] = new TableColumn(table, SWT.BORDER);
	  column[0].setText("Dateiname");

	  column[1] = new TableColumn(table, SWT.BORDER);
	  column[1].setText("Titel");
	  
	  column[2] = new TableColumn(table, SWT.BORDER);
	  column[2].setText("Jahr");
	  
	  column[3] = new TableColumn(table, SWT.BORDER);
	  column[3].setText("Artist");
	  
	  column[4] = new TableColumn(table, SWT.BORDER);
	  column[4].setText("Album");
	  
	  column[5] = new TableColumn(table, SWT.BORDER);
	  column[5].setText("Bitrate");
	  
	  column[6] = new TableColumn(table, SWT.BORDER);
	  column[6].setText("Frequenz");
	  
	 

	 //rechtsklick abfangen
	 table.addListener(SWT.MouseDown, new Listener() {
		    public void handleEvent(Event event) {
		    	if(event.button==3){
		    		if(!(getSelected()<0)){//falls kein Eintrag ausgew�hlt wurde (z.B. in ein leeres Feld geklickt) Men� nicht aufrufen
		    		KontextMenu menu=new KontextMenu();	
		    		menu.konetxtMen�Tabelle();
		    }}}});		
		    	
	 //doppelklick Event
	 table.addListener(SWT.MouseDoubleClick, new Listener() {
		    public void handleEvent(Event event) {  
		    	addToPlaylist();
		    }});
		

	//einen Listener auf Die Spalten von Tabelle legen
	 TableColumn[] spalten = table.getColumns();
	 
     //Auf jede Tabellenspalte einen Listener legen
     for (int k = 0; k < spalten.length; k++) {
         final int f = k;
         spalten[k].addListener(SWT.Selection, new Listener() {//Jeder Spalte wird eine Nummer zugewiesen ab 0
             public void handleEvent(Event event) {
               global.sort.sortierenTabelle(f);
               redraw();//Tabelle neu zeichnen
           } });


	 for (int i = 0, n = column.length; i < n; i++) {
		  column[i].pack();
		  }

	  table.setRedraw(true);
	  }
}
	
	//Tabelle neu Zeichnen (nur momentaner Inhalt)
public void redraw()
{
	table.clearAll();//Tabelle komplett leeren	
	table.removeAll();//Alle Eintr�ge entfernen
	for (int i = 0; i < global.getDateilisteMitteGr��e(); i++){ //einzelne Dateien durchgehen
		EintragSchreiben(i);
	}	
}

	//ruft Alle Daten auf und vergleicht
	 //Zugeh�rigkeit der Dateien zu Eigenschaften und Ausgabe in Tabelle 2
	//string=dateiname
	//typ=Interpret Album Jahr Genre Alle
	//andere werden �bersprungen
public void showInterpret(String string,String Typ)
{
	//Typen:
	//
	//Mp3info mp3=new Mp3info();
	table.clearAll();//Tabelle komplett leeren	
	table.removeAll();//Alle Eintr�ge entfernen
	global.clearMitte();
	table.setRedraw(true);
	
	if(!Typ.equals("AlleDatenAnzeigen")){//Falls alles gew�hlt wurde ist eine Filterung nicht n�tig also �berspringen
	for (int i = 0; i < global.getDateilisteGr��e(); i++){ //einzelne Dateien durchgehen
			  if(global.FileExists(global.getDateilisteEintrag(i))){//Datei existiert noch
				  //unterscheiden wonach gesucht wird
				  //Typ enth�lt den String nach dem gefiltert wird
				  //temp enth�lt den Eintrag der gefunden wurde
				  String temp = null;
				  if(Typ.equalsIgnoreCase("Interpret")){//unterscheiden welche Kategorie gesucht wird
				  temp=global.getDateilisteInterpret(i);
				  }else if(Typ.equalsIgnoreCase("Album")){
				  temp=global.getDateilisteAlbum(i);  
				  }else if(Typ.equalsIgnoreCase("Jahr")){
				  temp=global.getDateilisteJahr(i);
				  }
					  

				  //string=gew�hlter Eintrag im Tree 
				  //alle Dateien vergleichen mit entsprechendem Merkmal
				  //wenn �bereinstimmung den Eintrag komplett ausgeben
				  if(string.equalsIgnoreCase("TreeItem {"+temp+"}")){//falls der �bergebene Wert dem ausgew�hlten Eintrag entspricht
						//Mittlere Liste f�llen und speichern
						global.copyArrayHauptZuMitte(i);//Eintrag in Array f�r mittlere Anzeige	
				  }
			  }
	}
	

	//Liste f�llen
	for (int i = 0; i < global.getDateilisteMitteGr��e(); i++) {
	EintragSchreiben(i);
	}

	
}
	else if(Typ.equals("AlleDatenAnzeigen")){
		for (int i = 0; i < global.getDateilisteGr��e(); i++){ //einzelne Dateien durchgehen
			if(global.FileExists(global.getDateilisteEintrag(i))){//Datei existiert noch
				  global.copyArrayHauptZuMitte(i);//Eintrag in Array f�r mittlere Anzeige
				  EintragSchreiben(i);	
					  }	  
		}	
	}
	setSpaltenbreite();//Spaltenbreite anpassen	
}


private void setSpaltenbreite()
{
 for (int i1 = 0, n = column.length; i1 < n; i1++) {
		 column[i1].pack();
		 }//Tabelle zusammenpacken
	 
	 column[0].setWidth(230);
	 column[1].setWidth(150);
	 column[3].setWidth(150);	
	 column[4].setWidth(150);	
}
//zur Playlist hinzuf�gen
public void addToPlaylist()
{
		//global.getDateilisteEintrag();
		int x=getSelected();
		
		if(!(x<0)){//falls nichts ausgew�hlt wurde in Tabelle "-1" abfangen sonnst Absturz
		Playlist.addEntry(x);//Ausgew�hlte Datei an Playlist �bergeben
		}
}

//Die Tabelle f�llen
public void EintragSchreiben(int i)
{
		TableItem item = new TableItem(table, SWT.NONE);
	  	int pos=0;//Position in der Tabelle hochz�hlen horizontal
	  	//dateiname
		item.setText(pos++,  global.getDateilisteMitteshortEintrag(i));//kurzer Dateiname ohne Verzeichnis
		item.setImage(picFiles);
		//Titel
		item.setText(pos++, global.getDateilisteMitteTitel(i));
		//Jahr
		item.setText(pos++, global.getDateilisteMitteJahr(i));
		//Artist
		item.setText(pos++, global.getDateilisteMitteInterpret(i));
		//Album
		item.setText(pos++, global.getDateilisteMitteAlbum(i));
		//bitrate
		item.setText(pos++, global.getDateilisteMitteBitrate(i));
		//Frequenz
		item.setText(pos++,  global.getDateilisteMitteFrequenz(i));	 	
}
	

public int getSelected()
{	
		return table.getSelectionIndex();
}
	
 public void setSize(int posx,int posy,int sizex,int sizey)//�ndern des Baums (Position und Gr��e)
{
		 table.setBounds(posx, posy, sizex, sizey);
}
	
}