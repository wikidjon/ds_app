package eigenes;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.events.KeyListener;

public class Playlist
{
	static //Globale variablen
	Table table;
	int Spalten=7;
	static TableColumn[] column ;
	
	public void getPlaylist(Shell shell)
	{
		column = new TableColumn[Spalten];
		table = new Table(shell, SWT.BORDER);
	  	table.setRedraw(false);
	  
	  table.setHeaderVisible(true);
	  column[0] = new TableColumn(table, SWT.BORDER);
	  column[0].setText("Dateiname");
	  
	  column[1] = new TableColumn(table, SWT.BORDER);
	  column[1].setText("Titel");
	  
	  column[2] = new TableColumn(table, SWT.BORDER);
	  column[2].setText("Jahr");
	  
	  column[3] = new TableColumn(table, SWT.BORDER);
	  column[3].setText("Artist");
	  
	  column[4] = new TableColumn(table, SWT.BORDER);
	  column[4].setText("Album");
	  
	  column[5] = new TableColumn(table, SWT.BORDER);
	  column[5].setText("Bitrate");
	  
	  column[6] = new TableColumn(table, SWT.BORDER);
	  column[6].setText("Frequenz");
	  
	  
	  	//Klick Event
		 table.addListener (SWT.Selection, new Listener () {
				public void handleEvent (Event event) {	
				}});	

		 //doppelklick Event
		 table.addListener(SWT.MouseDoubleClick, new Listener() {
			    @SuppressWarnings("unused")
				public void handleEvent(Event event) {
			    	if(!(getSelected()<0)){
			    	//Mediaplayer Starten und Datei abspielen
					String ausgew�hlteDatei=global.dateilistePlaylist.get(getSelected());
					ProcessBuilder builder = new ProcessBuilder( "C:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe",ausgew�hlteDatei);
					Process p = null;
					try {
						p = builder.start();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			  }}}); 

		 //rechtsklick abfangen
		 table.addListener(SWT.MouseDown, new Listener() {
			    public void handleEvent(Event event) {
			    	if(event.button==3){
			    		if(!(table.getSelectionIndex()<0)){//falls kein Eintrag ausgew�hlt wurde (z.B. in ein leeres Feld geklickt) Men� nicht aufrufen
			    		KontextMenu menu=new KontextMenu();	
			    		menu.konetxtMen�Playlist();
			    		}
			    	}}});

		 //Tastendruck abfangen
		 table.addKeyListener(new KeyListener() {
				public void keyPressed(KeyEvent e) {
					switch (e.character) {
						case 'p': global.gem.PlaylistAbspielen(); break;
						case 'i':  
						int s=global.playlist.getSelected();
						Infofenster info=new Infofenster();
						if(!(s<0)){info.run(s,0);}//Position an Fenster �bergeben "0=Playlist"
						break;
						case SWT.DEL:  
							global.playlist.remove(); break;
						case 'c':  
							global.playlist.changeEntry();//Position eines Eintrages in der Playlist �ndern
						break;
							}}
				public void keyReleased(KeyEvent e) 
				{}} );

		 
		//einen Listener auf Die Spalten von Tabelle legen
		 TableColumn[] spalten = table.getColumns();
	     //Auf jede Tabellenspalte einen Listener legen
	     for (int k = 0; k < spalten.length; k++) {
	         final int f = k;
	         spalten[k].addListener(SWT.Selection, new Listener() {//Jeder Spalte wird eine Nummer zugewiesen ab 0
	             public void handleEvent(Event event) {
	               global.sort.sortierenPlaylist(f);//Sortierfunktion aufrufen
	              redraw();//Tabelle neu zeichnen
	             }});
 
	  table.setRedraw(true);
	}}
	

	public void redraw()
	{
		showInterpret();
	}
	
	public static void showInterpret()
	{
		table.clearAll();//Tabelle komplett leeren	
		table.removeAll();
		table.setRedraw(true);
	 
		for (int i = 0; i < global.getDateilistePlaylistGr��e(); i++){ //einzelne Dateien durchgehen
		
				  if(global.FileExists(global.getDateilistePlaylistEintrag(i))){//Datei existiert noch
				  
					  //string=gew�hlter Eintrag im Tree 
					  //alle Dateien vergleichen mit entsprechendem Merkmal
					  //wenn �bereinstimmung den Eintrag komplett ausgeben
					
						  	TableItem item = new TableItem(table, SWT.NONE);
						  	int pos=0;//Position in der Tabelle hochz�hlen horizontal
						  //dateiname
							item.setText(pos++, global.dateilistePlaylistshort.get(i));
							//Titel
							String test=global.dateilistePlaylistTitel.get(i);
							item.setText(pos++, test);
							//Jahr
							item.setText(pos++, global.dateilistePlaylistJahr.get(i));
							//Artist
							item.setText(pos++,global.dateilistePlaylistInterpret.get(i));
							//Album
							item.setText(pos++, global.dateilistePlaylistAlbum.get(i));
							//bitrate
							item.setText(pos++, global.dateilistePlaylistBitrate.get(i));
							//Frequenz
							item.setText(pos++, global.dateilistePlaylistFrequenz.get(i));
					
				  }    
		  }
		setSpaltenbreite();
	}
	

private static void setSpaltenbreite()
{
	 for (int i1 = 0, n = column.length; i1 < n; i1++) {
			 column[i1].pack();
			 }//Tabelle zusammenpacken
		 column[0].setWidth(270);
		 column[1].setWidth(180);
		 column[3].setWidth(180);	
		 column[4].setWidth(180);	
}

	
	//Gew�hlten Eintrag aus Tabelle in Playlist
	public static void addEntry(int pos)
	{
		//Array Eintr�ge hinzuf�gen
		global.copyArrayMitteZuPlaylist(pos);
		showInterpret();
	}
	
	public void newPlaylist()
	{
		Boolean isSaved = false;
		if(global.dateilistePlaylist.size()>0){//falls die Playlist nicht leer ist vorher Speichern
			isSaved=savePlaylist();
		}
		//Tabelle komplett und Array komplett leeren wenn Playlist gespeichert wurde
		if(isSaved){
			clearPlaylist();	
		}
	}
	
	@SuppressWarnings("unchecked")
	public void openPlaylist()
	{
		String datei=global.gem.OpenDialogPlaylist();//R�ckgabewert vom Dialog

		if(!(datei==null)){	//falls keine Datei �bergeben wurde nichts tun

			 	 try {
			 		 FileInputStream f_out = new FileInputStream(datei);
			 		 ObjectInputStream obj_out = new ObjectInputStream (f_out);

			 	        	try {
			 					global.dateilistePlaylist= (java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}
			 				try {
			 					global.dateilistePlaylistshort=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}	
			 				try {
			 					global.dateilistePlaylistTitel=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e1) {
			 					e1.printStackTrace();
			 				}
			 				try {
			 					global.dateilistePlaylistAlbum=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}	
			 				try {
			 					global.dateilistePlaylistInterpret=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}
			 				try {
			 					global.dateilistePlaylistJahr=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}	
			 				try {
			 					global.dateilistePlaylistBitrate=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}
			 				try {
			 					global.dateilistePlaylistFrequenz=(java.util.ArrayList<String>) obj_out.readObject();
			 				} catch (ClassNotFoundException e) {
			 					e.printStackTrace();
			 				}	

			 		 		showInterpret();	//Liste neu zeichen			
			 	     } 
			 		 catch (IOException e) {
			 	         e.printStackTrace();
			 	     }
		}	
	}	
	
	
	
	
	public Boolean savePlaylist()
	{
		 String datei=global.gem.SaveDialogPlaylist();//R�ckgabewert vom Dialog
		 	
		 if(!(datei==null)){	//falls keine Datei �bergeben wurde nichts tun
		 	 try {
		 		 	FileOutputStream f_out = new FileOutputStream(datei);
		 		 	ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
		 	
		         	obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylist);
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistshort);	
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistTitel);
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistAlbum);	
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistInterpret);
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistJahr);	
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistBitrate);
		 			obj_out.writeObject((java.util.ArrayList<String>)global.dateilistePlaylistFrequenz);	
		 	 				
		 			obj_out.flush();
		 			f_out.close();	
		 	
		      } 
		 	 catch (IOException e) {
		          e.printStackTrace();
		      }
		 	 return true;
		 }
		//true falls datei gew�hlt wurde
		//false falls keine datei gew�hlt wurde
		 return false;
	}
	
	
	
	//Elemente in der Playlist tauchen
	public void changeEntry()
	{
		//neues Fenster �ffnen
		entrywindow window=new entrywindow();	 
		int r�ckgabewert=window.run();
		if(!(r�ckgabewert==-1)){//-1 wird zur�ckgegeben falls der Dialog abgebrochen wurde
			global.pos�ndernPlaylist(getSelected(),r�ckgabewert);
			showInterpret();	//Liste neu zeichen
		}
	}
	private void clearPlaylist()
	{
		//Tabelle komplett und Array komplett leeren
		table.clearAll();
		table.removeAll();
		global.clearPlaylist();	
	}
	
	//Ausgew�hlter Eintrag
	public int getSelected()
	{
		return table.getSelectionIndex();
	}
	
	//Eintrag aus Liste l�schen
	public void remove()
	{
		if(!(getSelected()<0)){//falls kein Eintrag gew�hlt wurde
		global.removeIndexPlaylist(getSelected());
		}
		redraw();
	}
	
	 public void setSize(int posx,int posy,int sizex,int sizey)//�ndern des Baums (Position und Gr��e)
	  {
		 table.setBounds(posx, posy, sizex, sizey);
	  }
	
}