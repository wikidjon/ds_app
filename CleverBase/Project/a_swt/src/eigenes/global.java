package eigenes;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

 @SuppressWarnings("serial")

class AlleDaten implements Serializable
{
	 //komplette Dateiliste
     public static ArrayList<String> dateiliste = new ArrayList<String>();
     public static ArrayList<String> dateilisteshort = new ArrayList<String>();//Dateinamen ohne Verzeichnis
     public static ArrayList<String> dateilisteTitel = new ArrayList<String>();//Titel Speichern
     public static ArrayList<String> dateilisteAlbum = new ArrayList<String>();
     public static ArrayList<String> dateilisteInterpret = new ArrayList<String>();
     public static ArrayList<String> dateilisteJahr = new ArrayList<String>();
     public static ArrayList<String> dateilisteBitrate = new ArrayList<String>();
     public static ArrayList<String> dateilisteFrequenz = new ArrayList<String>();	
}

public class global 
{
	
	 static AlleDaten oRead = new AlleDaten();
	 static Tabelle tabelle=new Tabelle();//mittlere Tabelle
	 static Playlist playlist=new Playlist();//Klasse der Playlist
	 static sortieren sort=new sortieren();//Klasse der Sortierfunktionen
	 static Gemeinsamgenutzt gem=new Gemeinsamgenutzt();//Zugriff auf gemenisam genutzte
	 static protected Display display = new Display ();
	 static protected Shell shell = new Shell(display);
	 static ToolbarEx toolbarex=new ToolbarEx();
	 static baumansicht baum=new baumansicht();//baumansicht einfügen	
	 
	 public static Shell getShell()
	 {
		return shell; 
	 }
	 
	 public static Display getDisplay()
	 {
		return display; 
	 }
	 
     public static int dateianzahl = 0;//Anzahl gefundener Dateien
     public static String s = "aaa";

     //Angezeigter Bereich Mitte
     public static ArrayList<String> dateilisteMitte = new ArrayList<String>();//sortiert wenn nicht alle dateien angezeigt werden(z.B nur einzelne in Playlist)
     public static ArrayList<String> dateilisteMitteshort = new ArrayList<String>();//sortiert wenn nicht alle dateien angezeigt werden(zugehöriger Pfad)
     public static ArrayList<String> dateilisteMitteTitel = new ArrayList<String>();//Titel Speichern
     public static ArrayList<String> dateilisteMitteAlbum = new ArrayList<String>();
     public static ArrayList<String> dateilisteMitteInterpret = new ArrayList<String>();
     public static ArrayList<String> dateilisteMitteJahr = new ArrayList<String>();
     public static ArrayList<String> dateilisteMitteBitrate = new ArrayList<String>();
     public static ArrayList<String> dateilisteMitteFrequenz = new ArrayList<String>();
     

     //Angezeigter Bereich Playlist
     public static ArrayList<String> dateilistePlaylist = new ArrayList<String>();//sortiert wenn nicht alle dateien angezeigt werden(z.B nur einzelne in Playlist)
     public static ArrayList<String> dateilistePlaylistshort = new ArrayList<String>();//sortiert wenn nicht alle dateien angezeigt werden(zugehöriger Pfad)
     public static ArrayList<String> dateilistePlaylistTitel = new ArrayList<String>();//Titel Speichern
     public static ArrayList<String> dateilistePlaylistAlbum = new ArrayList<String>();
     public static ArrayList<String> dateilistePlaylistInterpret = new ArrayList<String>();
     public static ArrayList<String> dateilistePlaylistJahr = new ArrayList<String>();
     public static ArrayList<String> dateilistePlaylistBitrate = new ArrayList<String>();
     public static ArrayList<String> dateilistePlaylistFrequenz = new ArrayList<String>();
     //

     public static Boolean SucheImGange=false;
     
     //Array abfragen 
     //global
     public static String getDateilisteEintrag(int pos)
     {
    	 return AlleDaten.dateiliste.get(pos);
     }
     
     public static int getDateilisteGröße()
     {
    	
    	 return AlleDaten.dateiliste.size();
     }
     
     public static String getDateilisteShortEintrag(int pos)
     {
    	 return AlleDaten.dateilisteshort.get(pos);
     }
     
     public static String getDateilisteInterpret(int pos)
     {
    	 return AlleDaten.dateilisteInterpret.get(pos);
     }
     
     public static String getDateilisteAlbum(int pos)
     {
    	 return AlleDaten.dateilisteAlbum.get(pos);
     }
     
     public static String getDateilisteJahr(int pos)
     {
    	 return AlleDaten.dateilisteJahr.get(pos);
     }
     public static String getDateilisteTitel(int pos)
     {
    	 return AlleDaten.dateilisteTitel.get(pos);
     }
     public static String getDateilisteBitrate(int pos)
     {
    	 return AlleDaten.dateilisteBitrate.get(pos);
     }
     public static String getDateilisteFrequenz(int pos)
     {
    	 return AlleDaten.dateilisteFrequenz.get(pos);
     }
     
     public static void addDateiliste(String Text)
     {
    	  AlleDaten.dateiliste.add(Text);
     }
     
     public static void addDateilisteShort(String Text)
     {
    	  AlleDaten.dateilisteshort.add(Text);
     }
     public static void addDateilisteAlbum(String Text)
     {
    	  AlleDaten.dateilisteAlbum.add(Text);
     }
     public static void addDateilisteTitel(String Text)
     {
    	  AlleDaten.dateilisteTitel.add(Text);
     }
     public static void addDateilisteInterpret(String Text)
     {
    	  AlleDaten.dateilisteInterpret.add(Text);
     }
     public static void addDateilisteBitrate(String Text)
     {
    	  AlleDaten.dateilisteBitrate.add(Text);
     }
     public static void addDateilisteFrequenz(String Text)
     {
    	  AlleDaten.dateilisteFrequenz.add(Text);
     }
     public static void addDateilisteJahr(String Text)
     {
    	  AlleDaten.dateilisteJahr.add(Text);
     }
     
     
     public static void clearMain()
     {
    	 AlleDaten.dateiliste.clear();
    	 AlleDaten.dateilisteshort.clear();
    	 AlleDaten.dateilisteTitel.clear();
    	 AlleDaten.dateilisteAlbum .clear();
    	 AlleDaten.dateilisteInterpret .clear();
    	 AlleDaten.dateilisteJahr.clear();
    	 AlleDaten.dateilisteBitrate.clear();
    	 AlleDaten.dateilisteFrequenz .clear();
     }
     
     
     //mitte
     
     public static String getDateilisteMitteEintrag(int pos)
     {
    	 return dateilisteMitte.get(pos);
     }
     
     
     public static String getDateilisteMitteInterpret(int pos)
     {
    	 return dateilisteMitteInterpret.get(pos);
     }
     
     public static String getDateilisteMitteAlbum(int pos)
     {
    	 return dateilisteMitteAlbum.get(pos);
     }
     
     public static String getDateilisteMitteJahr(int pos)
     {
    	 return dateilisteMitteJahr.get(pos);
     }
     public static String getDateilisteMitteTitel(int pos)
     {
    	 return dateilisteMitteTitel.get(pos);
     }
     public static String getDateilisteMitteBitrate(int pos)
     {
    	 return dateilisteMitteBitrate.get(pos);
     }
     public static String getDateilisteMitteFrequenz(int pos)
     {
    	 return dateilisteMitteFrequenz.get(pos);
     }
     
     public static void addDateilisteMitteEintrag(String Text)
     {
    	  dateilisteMitte.add(Text);
     }

     public static void addDateilisteMitteshortEintrag(String Text)
     {
    	  dateilisteMitteshort.add(Text);
     }
     
     public static int getDateilisteMitteGröße()
     {
    	 return dateilisteMitte.size();
     }
     
     public static String getDateilisteMitteshortEintrag(int pos)
     {
    	 return dateilisteMitteshort.get(pos);
     }
     
     
     
     
     public static void clearMitte()
     {
    	 	dateilisteMitte.clear();
    	 	dateilisteMitteshort.clear();
    	 	dateilisteMitteTitel.clear();
    	 	dateilisteMitteAlbum.clear(); 
    	 	dateilisteMitteInterpret.clear(); 
    	 	dateilisteMitteJahr.clear(); 
    	 	dateilisteMitteBitrate.clear(); 
    	 	dateilisteMitteFrequenz.clear(); 

     }
     public static void clearPlaylist()
     {
    	 	dateilistePlaylist.clear();
    	 	dateilistePlaylistshort.clear();
    	 	dateilistePlaylistTitel.clear();
    	 	dateilistePlaylistAlbum.clear(); 
    	 	dateilistePlaylistInterpret.clear(); 
    	 	dateilistePlaylistJahr.clear(); 
    	 	dateilistePlaylistBitrate.clear(); 
    	 	dateilistePlaylistFrequenz.clear(); 
     }
     
     //Gewählte Postition in der Playlist löschen
     public static void removeIndexPlaylist(int i)
     {
    		dateilistePlaylist.remove(i);
    		dateilistePlaylistshort.remove(i);
    		dateilistePlaylistTitel.remove(i);
    		dateilistePlaylistAlbum.remove(i);
    		dateilistePlaylistInterpret.remove(i); 
    		dateilistePlaylistJahr.remove(i);
    		dateilistePlaylistBitrate.remove(i); 
    		dateilistePlaylistFrequenz.remove(i);  
     }
     
     
     //playlist
     public static String getDateilistePlaylistEintrag(int pos)
     {
    	 return dateilistePlaylist.get(pos);
     }
     
     public static void addDateilistePlaylistEintrag(String Text)
     {
    	  dateilistePlaylist.add(Text);
     }
     
     public static void addDateilistePlaylistshortEintrag(String Text)
     {
    	  dateilistePlaylistshort.add(Text);
     }
     
     public static int getDateilistePlaylistGröße()
     {
    	 return dateilistePlaylist.size();
     }
     
     public static String getDateilistePlaylistshortEintrag(int pos)
     {
    	 return dateilistePlaylistshort.get(pos);
     }
     
     //gewähltern Eintrag kopieren zwischenspeichern(Liste enthält nur momentan ausgegebene Einträhe in der Mitte Dateipfad)
     //alle gewählten Einträge kopieren vom Baum zur Tabelle in der Mitte
     public static void copyArrayHauptZuMitte(int i)
     {
    	    dateilisteMitteshort.add(AlleDaten.dateilisteshort.get(i));
			dateilisteMitte.add(AlleDaten.dateiliste.get(i)); 
			dateilisteMitteAlbum.add(AlleDaten.dateilisteAlbum.get(i));
			dateilisteMitteTitel.add(AlleDaten.dateilisteTitel.get(i));
			dateilisteMitteInterpret.add(AlleDaten.dateilisteInterpret.get(i));
			dateilisteMitteJahr.add(AlleDaten.dateilisteJahr.get(i));
			dateilisteMitteBitrate.add(AlleDaten.dateilisteBitrate.get(i));
			dateilisteMitteFrequenz.add(AlleDaten.dateilisteFrequenz.get(i));
     
     }
     
     //gewähltern Eintrag kopieren zwischenspeichern(Liste enthält nur momentan ausgegebene Einträhe in Playlist Dateipfad)
     //alle gewählten Einträge kopieren vom Mitte zur Tabelle(Playlist) 
     public static void copyArrayMitteZuPlaylist(int i)
     {
    	    dateilistePlaylistshort.add(dateilisteMitteshort.get(i));
			dateilistePlaylist.add(dateilisteMitte.get(i)); 
			dateilistePlaylistAlbum.add(dateilisteMitteAlbum.get(i));
			dateilistePlaylistTitel.add(dateilisteMitteTitel.get(i));
			dateilistePlaylistInterpret.add(dateilisteMitteInterpret.get(i));
			dateilistePlaylistJahr.add(dateilisteMitteJahr.get(i));
			dateilistePlaylistBitrate.add(dateilisteMitteBitrate.get(i));
			dateilistePlaylistFrequenz.add(dateilisteMitteFrequenz.get(i));
     } 
     
     //Elemente in Array tauschen
     //i=gewählter Eintrag
     //j=Position an die verschoben werden soll
     public static void posÄndernPlaylist(int i,int j)
     {
    	 //Elemente zwischenspeichern und Einträge löschen
    	 	String temp1=dateilistePlaylistshort.get(i);	dateilistePlaylistshort.remove(i);
    	 	String temp2=dateilistePlaylist.get(i);			dateilistePlaylist.remove(i);
    	 	String temp3=dateilistePlaylistAlbum.get(i);	dateilistePlaylistAlbum.remove(i);
    	 	String temp4=dateilistePlaylistTitel.get(i);	dateilistePlaylistTitel.remove(i);
    	 	String temp5=dateilistePlaylistInterpret.get(i);dateilistePlaylistInterpret.remove(i);
    	 	String temp7=dateilistePlaylistJahr.get(i);		dateilistePlaylistJahr.remove(i);
    	 	String temp8=dateilistePlaylistBitrate.get(i);	dateilistePlaylistBitrate.remove(i);
    	 	String temp9=dateilistePlaylistFrequenz.get(i);	dateilistePlaylistFrequenz.remove(i);
    	 	//zwischengespeicherten Eintrag an neue posizion einfügen
    	 	dateilistePlaylistshort.add(j,temp1);
    	 	dateilistePlaylist.add(j,temp2);
    	 	dateilistePlaylistAlbum.add(j,temp3);
    	 	dateilistePlaylistTitel.add(j,temp4);
    	 	dateilistePlaylistInterpret.add(j,temp5);
    	 	dateilistePlaylistJahr.add(j,temp7);
    	 	dateilistePlaylistBitrate.add(j,temp8);
    	 	dateilistePlaylistFrequenz.add(j,temp9); 
     }
     
     
     //Elemente in Array tauschen
     //i=gewählter Eintrag
     //j=Position an die verschoben werden soll 
     public static void posÄndernMitte(int i,int j)
     {
    	 //Elemente zwischenspeichern und Einträge löschen
 	 	String temp1=dateilisteMitteshort.get(i);	dateilisteMitteshort.remove(i);
 	 	String temp2=dateilisteMitte.get(i);			dateilisteMitte.remove(i);
 	 	String temp3=dateilisteMitteAlbum.get(i);	dateilisteMitteAlbum.remove(i);
 	 	String temp4=dateilisteMitteTitel.get(i);	dateilisteMitteTitel.remove(i);
 	 	String temp5=dateilisteMitteInterpret.get(i);dateilisteMitteInterpret.remove(i);
 	 	String temp7=dateilisteMitteJahr.get(i);		dateilisteMitteJahr.remove(i);
 	 	String temp8=dateilisteMitteBitrate.get(i);	dateilisteMitteBitrate.remove(i);
 	 	String temp9=dateilisteMitteFrequenz.get(i);	dateilisteMitteFrequenz.remove(i);
 	 	
 	 	//zwischengespeicherten Eintrag an neue posizion einfügen
 	 	dateilisteMitteshort.add(j,temp1);
 	 	dateilisteMitte.add(j,temp2);
 	 	dateilisteMitteAlbum.add(j,temp3);
 	 	dateilisteMitteTitel.add(j,temp4);
 	 	dateilisteMitteInterpret.add(j,temp5);
 	 	dateilisteMitteJahr.add(j,temp7);
 	 	dateilisteMitteBitrate.add(j,temp8);
 	 	dateilisteMitteFrequenz.add(j,temp9);
     }
     
     
     public static Boolean FileExists(String file)
     {
    	  File lagerDatei = new File(file);
	      if(!lagerDatei.exists())
	      {
	        try {
	 
	        }
	        catch (Exception e) {
	          System.out.println("Datei konnte nicht gefunden werden");
	        }
	      }
	      else {
	    	 
	      } 
	      if(!lagerDatei.exists()){  
	      return false;
	    		  }
	      else{
	      return true;     
	      }	 
     }  
}