package a02_text2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a22_font {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setBounds(10, 10, 200, 200);

		//***********neu****************************
		Text text = new Text(shell, SWT.MULTI);
		text.setBounds(10, 10, 150, 150);

		Font startFont = text.getFont();
		System.out.println(startFont.toString());
		FontData[] fontData = startFont.getFontData();
		for (int i = 0; i < fontData.length; i++) {
			fontData[i].setHeight(25);
		}
		Font newFont = new Font(display, fontData);
		text.setFont(newFont);

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		newFont.dispose();
		display.dispose();
	}
}
