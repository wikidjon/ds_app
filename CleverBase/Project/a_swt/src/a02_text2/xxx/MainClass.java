package a02_text2.xxx;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class MainClass {

  public static void main(String args[]) throws Exception {

    URL u = new URL("http://www.hs-merseburg.de");
    HttpURLConnection http = (HttpURLConnection) u.openConnection();
    http.setRequestMethod("HEAD");
    System.out.println(u + "was last modified at " + new Date(http.getLastModified()));
  }

}