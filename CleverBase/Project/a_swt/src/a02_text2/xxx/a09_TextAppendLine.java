package a02_text2.xxx;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a09_TextAppendLine {

  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    Text text = new Text(shell, SWT.BORDER | SWT.V_SCROLL);
    text.setBounds(10, 10, 100, 100);
    for (int i = 0; i < 16; i++) {
      text.append("Line " + i + "\n");
    }
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}