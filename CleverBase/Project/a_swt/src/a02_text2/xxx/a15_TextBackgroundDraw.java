package a02_text2.xxx;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a15_TextBackgroundDraw {

  public static void main(String[] args) {
    final Display display = new Display();
    Shell shell = new Shell(display);

    shell.setLayout(new FillLayout());

    final Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);
    text.setText("press tab to draw");
    text.addTraverseListener(new TraverseListener() {
      public void keyTraversed(TraverseEvent e) {
        if (e.detail == SWT.TRAVERSE_TAB_NEXT || e.detail == SWT.TRAVERSE_TAB_PREVIOUS) {
          e.doit = true;

          GC gc = new GC(text);
          // Erase background first.
          Rectangle rect = text.getClientArea();
          gc.fillRectangle(rect.x, rect.y, rect.width, rect.height);

          Font font = new Font(display, "Arial", 32, SWT.BOLD);
          gc.setFont(font);

          gc.drawString("tab event", 15, 10);

          gc.dispose();
        }
      }
    });

    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
  }
}