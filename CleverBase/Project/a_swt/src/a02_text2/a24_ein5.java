package a02_text2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a24_ein5 {

public static void main (String [] args) {
	Display display = new Display ();
	Shell shell = new Shell (display);
	shell.setLayout (new RowLayout ());
	
	Combo combo = new Combo (shell, SWT.NONE);
	combo.setItems (new String [] {"A-1", "B-1", "C-1"});
	
	Text text = new Text (shell, SWT.SINGLE | SWT.BORDER);
	text.setText ("ein Text");
	
	combo.addListener (SWT.DefaultSelection, new Listener () {
		public void handleEvent (Event e) {
			System.out.println (e.widget + " - Standardauswahl");
		}
	});
	text.addListener (SWT.DefaultSelection, new Listener () {
		public void handleEvent (Event e) {
			System.out.println (e.widget + " - Standardauswahl");
		}
	});
	shell.pack ();
	shell.open ();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
} 
