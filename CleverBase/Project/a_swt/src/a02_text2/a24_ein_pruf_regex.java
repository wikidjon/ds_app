package a02_text2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a24_ein_pruf_regex {
	/*
	 * Telefonnummer Regeln [(][1-9][1-9][1-9][)][1-9][1-9][1-9][-][1-9][1-9][1-9][1-9]
	 */
	private static final String REGEX = "[(]\\d{3}[)]\\d{3}[-]\\d{4}";  
	private static final String muster = "(###)###-####"; 
	private static final String standardText = "(000)000-0000"; 
	
	
public static void main(String[] args) {
	
	Display display = new Display();
	Shell shell = new Shell(display);
	shell.setLayout(new GridLayout());
	
	final Text text = new Text(shell, SWT.BORDER);
	Font font = new Font(display, "Courier New", 10, SWT.NONE); 
	text.setFont(font);
	text.setText(muster);	
	text.addListener(SWT.Verify, new Listener() {
		//Muster fuer Ueberpruefung
		Pattern pattern = Pattern.compile(REGEX);	
		//ignorieren eines events beim einfuegen von text im event handler
		boolean ignorieren;
		public void handleEvent(Event e) {
			if (ignorieren) return;
			e.doit = false;
			if (e.start > 13 || e.end > 14) return;
			StringBuffer buffer = new StringBuffer(e.text);
			
			//backspace taste behandeln
			if (e.character == '\b') {
				for (int i = e.start; i < e.end; i++) {
					// separatoren ueberspringen
					switch (i) {
						case 0: 
							if (e.start + 1 == e.end) {
								return;
							} else {
								buffer.append('(');
							}
							break;
						case 4:
							if (e.start + 1 == e.end) {
								buffer.append(new char [] {'#',')'});
								e.start--;
							} else {
								buffer.append(')');
							}
							break;
						case 8:
							if (e.start + 1 == e.end) {
								buffer.append(new char [] {'#','-'});
								e.start--;
							} else {
								buffer.append('-');
							}
							break;
						default: buffer.append('#');
					}
				}
				text.setSelection(e.start, e.start + buffer.length());
				ignorieren = true;
				text.insert(buffer.toString());
				ignorieren = false;
				// cursor rueckwaerts ueber ) und - bewegen
				if (e.start == 5 || e.start == 9) e.start--;
				text.setSelection(e.start, e.start);
				return;
			}
			
			StringBuffer newText = new StringBuffer(standardText);
			char[] chars = e.text.toCharArray();
			int index = e.start - 1;
			for (int i = 0; i < e.text.length(); i++) {
				index++;
				switch (index) {
					case 0:
						if (chars[i] == '(') continue;
						index++;
						break;
					case 4:
						if (chars[i] == ')') continue;
						index++;
						break;
					case 8:
						if (chars[i] == '-') continue;
						index++;
						break;
				}
				if (index >= newText.length()) return;
				newText.setCharAt(index, chars[i]);
			}
			// if text is selected, do not paste beyond range of selection
			if (e.start < e.end && index + 1 != e.end) return;
			Matcher matcher = pattern.matcher(newText);
			if (matcher.lookingAt()) {
				text.setSelection(e.start, index + 1);
				ignorieren = true;
				text.insert(newText.substring(e.start, index + 1));
				ignorieren = false;
			}			
		}
	});
		
	shell.pack();
	shell.open();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch())
			display.sleep();
	}
	font.dispose();
	display.dispose();
}
}
