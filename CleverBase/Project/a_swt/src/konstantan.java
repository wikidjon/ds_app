/*********************************************************

Konstanten f�r Shells und Dialoge

SWT.NONE 		Standardfenster, betriebssystemabh�ngig.
SWT.BORDER 		Fenster hat einen Rand (plattformabh�ngig).
SWT.CLOSE 		Fenster hat eine Titelzeile mit einem Button zum Schlie�en des Fensters.
SWT.MIN 		Fenster hat eine Titelzeile mit einem Button zum Minimalisieren des Fensters.
SWT.MAX 		Fenster hat eine Titelzeile mit einem Button zum Maximalisieren des Fensters.
SWT.NO_TRIM 	Fenster hat weder Titelzeile noch Rand.
SWT.RESIZE 		Fenstergr��e kann durch Ziehen mit der Maus ver�ndert werden.
SWT.TITLE 		Fenster hat eine Titelzeile.
SWT.SHELL_TRIM 	Kombination von Stilelementen f�r das Top-Level-Fenster einer Applikation
			(SWT.CLOSE | SWT.TITLE | SWT.MIN | SWT.MAX | SWT.RESIZE).
SWT.DIALOG_TRIM	Kombination von Stilelementen f�r Dialog-Fenster
(SWT.CLOSE | SWT.TITLE | SWT.BORDER).


Konstanten f�r Modalit�tsverhalten von Fenstern

SWT.APPLICATION_MODAL, SWT.
MODELESS, SWT.PRIMARY_MODAL 
SWT.SYSTEM_MODAL,


Konstanten f�r Buttons
SWT.ARROW		Button mit einem kleinen Pfeil. �blicherweise verwendet, um kleine Men�s einzublenden.
SWT.CHECK		K�stchen (Checkbox) zum Ankreuzen. Der Text befindet sich neben dem K�stchen.
SWT.PUSH		Taste mit Beschriftung auf der Tastenfl�che.
SWT.RADIO		Radiobutton. Mehrere Radiobuttons innerhalb einer Gruppe l�sen sich gegenseitig aus.
SWT.TOGGLE		Wie SWT.PUSH, nur dass beim ersten Klick die Taste gedr�ckt bleibt und beim zweiten Klick wieder ausl�st.

Konstanten f�r das Aussehen von Buttons	(nicht von allen Plattformen unterst�tzt)
SWT.FLAT 		Der Button wird nicht dreidimensional gezeichnet, sondern �flach�.
SWT.BORDER 		Der Button wird mit einem Rahmen umgeben.


Konstanten f�r Slider und Scale
SWT.HORIZONTAL	Horizontale oder vertikale Darstellung
SWT.VERTICAL		
SWT.BORDER		Skalen werden mit einem Rahmen umgeben. Wirkungslos bei Slider

Zus�tzlich f�r ProgressBar
SWT.SMOOTH 		bewirkt, dass der Balken nicht durchbrochen gezeichnet
SWT.INDETERMINATE 	wird.



Konstanten f�r Scrollable und ScrollBar von vornherein mit Schiebern ausgestattet,
SWT.H_SCROLL und SWT.V_SCROL

Konstanten f�r Textfelder und Beschriftungen
SWT.MULTI		Bestimmt, ob das Textfeld mehrere oder nur eine Zeile hat.
SWT.SINGLE 
SWT.READ_ONLY 	Text im Textfeld kann nicht vom Benutzer ver�ndert werden.
SWT.WRAP 		Automatischer Umbruch wird unterst�tzt.

Konstanten f�r Labels (kann auch f�r Grafiken und Bilder genutzt werden)
SWT.SEPARATOR 	Stellt eine horizontale oder vertikale Linie dar.
SWT.HORIZONTAL	Stellt Orientierung einer Linie fest.
SWT.VERTICAL

SWT.SHADOW_IN		Schattendarstellung einer Linie.
SWT.SHADOW_OUT
SWT.SHADOW_NONE

SWT.CENTER		Ausrichtung eines Text- oder Bildlabels.
SWT.LEFT
SWT.RIGHT

SWT.WRAP 		Automatischer Umbruch bei Textlabels.


Konstanten f�r Tabellen
SWT.SINGLE		Der Benutzer kann nur eine bzw. mehrere Tabellenzeilen ausw�hlen.
SWT.MULTI
SWT.FULL_SELECTION	Die gesamte Tabellenzeile ist selektierbar (normalerweise kann nur das erste Element selektiert werden).
SWT.CHECK		Vor jeder Tabellenzeile wird eine Checkbox abgebildet. Der Zustand der Checkbox kann mit den Methoden setChecked() und getChecked() gesetzt bzw. abgefragt werden.
SWT.VIRTUAL		Diese Konstante zeigt eine virtuelle Tabelle an, d.h. eine Tabelle, deren Elemente erst erzeugt werden, wenn sie tats�chlich ben�tigt werden. Damit k�nnen sehr gro�e
Tabellen realisiert werden. Beim Aufbau einer virtuellen Tabelle muss die Anzahl der Tabelleneintr�ge mit der Methode setItemCount() gesetzt werden. Sobald ein neues
Tabellenelement (TableItem) ben�tigt wird, wird es von der Tabelle erzeugt. Dann wird ein SWT.SetData-Ereignis ausgel�st, wobei das Event-Objekt das neue Tabellenelement
enth�lt. Im Listener kann dann das Tabellenelement fertiggestellt werden,bevor es dann von der Tabelle zur Anzeige gebracht wird.

Konstanten f�r die Ausrichtung der Tabellenspalten
SWT.LEFT, SWT.CENTER und SWT.RIGHT

Konstanten f�r Listen
SWT.DROP_DOWN	Die Auswahlliste wird erst beim Klick auf die Pfeiltaste angezeigt.
SWT.READ_ONLY	Es ist nur m�glich, vorgegebene Werte auszuw�hlen, jedoch nicht, Werte im Textfeld einzugeben.
SWT.SIMPLE 		Die Auswahlliste ist immer sichtbar.




Konstanten f�r B�ume
SWT.SINGLE		Der Benutzer kann nur eine bzw. mehrere Baumknoten
SWT.MULTI		ausw�hlen.
SWT.CHECK		Vor jedem Baumknoten wird eine Checkbox abgebildet. Der Zustand der Checkbox kann mit den Methoden setChecked()
und getChecked() gesetzt bzw. abgefragt werden.


Konstanten f�r Toolbars (Werkzeugleisten)
SWT.FLAT 		Zweidimensionale statt dreidimensionaler Darstellung. Plattformabh�ngig.
SWT.WRAP 		Automatischer Umbruch
SWT.RIGHT 		Rechtsb�ndige Ausrichtung
SWT.HORIZONTAL	Horizontale oder vertikale Ausrichtung
SWT.VERTICAL

Konstanten f�r ToolItems
SWT.PUSH 		Normale Taste mit sofortiger Ausl�sung
SWT.CHECK 		Arretierende Taste
SWT.RADIO 		Radiotaste mit gegenseitiger Ausl�sung
SWT.SEPARATOR 	Passives Element zum Trennen von Tastengruppen
SWT.DROP_DOWN 	Normale Taste mit assoziierter Pfeiltaste


Konstanten f�r Men�s
SWT.BAR 		Men�leiste
SWT.DROP_DOWN 	Drop-down-Men�
SWT.POP_UP 		Pop-up-Men�


Konstanten f�r MenuItems
SWT.CHECK		Men�eintrag mit Check-Symbol. Bei Bet�tigung erscheint und verschwindet das Check-Symbol wechselweise.
SWT.CASCADE 	Men�eintrag f�r kaskadierende Men�s
SWT.PUSH 		Normaler Men�eintrag
SWT.RADIO 		Men�eintrag mit gegenseitiger Ausl�sung
SWT.SEPARATOR 	Passive Trennlinie



Konstanten f�r den Mauszeiger
CURSOR_ARROW 		Pfeil
CURSOR_WAIT 		Warten
CURSOR_CROSS 		Fadenkreuz
CURSOR_APPSTARTING 	Start einer Anwendung
CURSOR_HELP 		Hilfe
CURSOR_SIZEALL 		Gesamtgr��e �ndern
CURSOR_SIZENESW 	Gr��en�nderung auf NO/SW-Achse
CURSOR_SIZENS 		Gr��en�nderung auf N/S-Achse
CURSOR_SIZENWSE 	Gr��en�nderung auf NW/SE-Achse
CURSOR_SIZEWE 		Gr��en�nderung auf W/O-Achse
CURSOR_SIZEN 		Gr��en�nderung Nordrichtung
CURSOR_SIZES 		Gr��en�nderung S�drichtung
CURSOR_SIZEE 		Gr��en�nderung Ostrichtung
CURSOR_SIZEW 		Gr��en�nderung Westrichtung
CURSOR_SIZENE 		Gr��en�nderung Nordostrichtung
CURSOR_SIZESE 		Gr��en�nderung S�dostrichtung
CURSOR_SIZESW 		Gr��en�nderung S�dwestrichtung
CURSOR_SIZENW 		Gr��en�nderung Nordwestrichtung
CURSOR_UPARROW 		Pfeil nach oben
CURSOR_IBEAM 		Schreibmarke
CURSOR_NO 			unzul�ssige Operation
CURSOR_HAND 		Hand zum Verschieben

Konstanten f�r Drag and Drop Aktionen

DND.NONE, DND.MOVE, DND.COPY, DND.LINK)

*
*********/

