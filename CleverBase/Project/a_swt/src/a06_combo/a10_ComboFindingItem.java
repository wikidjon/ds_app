package a06_combo;



import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a10_ComboFindingItem {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		String[] ITEMS = { "A", "B", "C", "D" };

		final Combo combo = new Combo(shell, SWT.DROP_DOWN);
		combo.setItems(ITEMS);

		System.out.println(combo.indexOf("B"));

		combo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				System.out.println(combo.getText());
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(combo.getText());
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}