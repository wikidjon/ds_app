package a06_combo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a06_combo1 {

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300, 300);
		shell.open();

		final Label label = new Label(shell, SWT.NONE);
		label.setSize(200, 20);
		label.setLocation(10, 180);
		label.setText("Bitte waehlen Sie ! ");

		final Combo combo1 = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
		//Achtung kein Button!!!!
		combo1.setItems(new String[] { "Eins", "Zwei", "Drei", " 4 ", " 5 " });
		combo1.select(0);
		combo1.setLocation(10, 10);
		combo1.setSize(100, 20);

		final Combo combo2 = new Combo(shell, SWT.SIMPLE);
		combo2.setItems(new String[] { "Rot", "Gr�n", "Blau", "Gelb" });
		combo2.setBounds(50, 50, 200, 100);
		combo2.select(1);

		final Combo combo3 = new Combo(shell, SWT.DROP_DOWN);
		combo3.setItems(new String[] { "10", "50", "100", "234" });
		combo3.select(0);
		combo3.setLocation(200, 10);
		combo3.setSize(50, 50);


		combo1.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				System.out.println("Default index: "
						+ combo1.getSelectionIndex()
						+ ", gewaehlter Eintrag: "
						+ (combo1.getSelectionIndex() == -1 ? "<null>" : combo1
								.getItem(combo1.getSelectionIndex()))
								+ ", Inhalt des Feldes: " + combo1.getText());
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Gewaehlter index: "
						+ combo1.getSelectionIndex() + ", gewaehletr Eintrag: "
						+ combo1.getItem(combo1.getSelectionIndex())
						+ ", Inhalt des Feldes: " + combo1.getText());
			}
		});

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
		combo1.dispose();
		combo2.dispose();
		combo3.dispose();
	}
}