package a06_combo;
/************************************************************+
 * Add method appends an item to the end of the list
The convenient add method appends an item to the end of the list:
		public void add(String text)
This is equivalent to the following:
		public void add(String text, getItemCount())
 ****************************************************/
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a12_ComboAddItem {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		String[] ITEMS = { "A", "B", "C", "D" };

		final Combo combo = new Combo(shell, SWT.DROP_DOWN);
		combo.setItems(ITEMS);

		combo.add("XYZ");

		combo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				System.out.println(combo.getText());
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(combo.getText());
			}
		});

		shell.open();
		shell.pack();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}