package a06_combo;
//Getting Items
//The getItem method returns the item at the specified index
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a08_ComboAccessItems {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		String[] items = { "A", "B", "C", "D" };

		final Combo combo = new Combo(shell, SWT.DROP_DOWN);
		combo.setItems(items);
		//combo.select(0);			//!!!!!!!!!!!!!!!!!!!!
		System.out.println(combo.getItem(0));

		combo.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(combo.getText());
			}
		});

		shell.open();
		shell.pack();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}