package a01_treeS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;


public class Tree1 {


	public static void main(String[] args) {
		Display d = new Display();
		Shell s = new Shell(d);

		s.setSize(250,200);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Tree Beispiel");
		s.setLayout(new FillLayout());

		Tree t = new Tree(s, SWT.SINGLE | SWT.BORDER);
		TreeItem kind1 = new TreeItem(t, SWT.NONE, 0);
		kind1.setText("1");
		TreeItem kind2 = new TreeItem(t, SWT.NONE, 1);
		kind2.setText("2");
		TreeItem kind2a = new TreeItem(kind2, SWT.NONE, 0);
		kind2a.setText("2A");
		TreeItem kind2b = new TreeItem(kind2, SWT.NONE, 1);
		kind2b.setText("2B");
		TreeItem kind3 = new TreeItem(t, SWT.NONE, 2);
		kind3.setText("3");
		s.open();
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
