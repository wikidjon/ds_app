package u3;

/**
 * Created by Crazy_Jenny on 2015/5/2.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class data3rech {
    public int rechnen(){
        int j=0;
        Connection myConnection = null;
        // DB test ist unter MySql eingerichtet - und beinhaltet cd_liste
        String url = "jdbc:mysql://localhost:3306/test?";
        String driverClass = "org.gjt.mm.mysql.Driver";
        String user = "";
        String password = "";

        try {
            // Treiber laden.
            // Class.forName(driverClass);
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
            // Class.forName("com.mysql.jdbc.Driver").newInstance();

            // Verbindung aufbauen.
            myConnection = DriverManager.getConnection(url, user, password);

            String query = "SELECT Gruppe, Kennung FROM cd_liste";
            Statement stmt = myConnection.createStatement();
            ResultSet rs = stmt.executeQuery(query);


            while (rs.next()) {
                j++;
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return j;
    }
}
