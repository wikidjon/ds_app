
import java.awt.event.FocusEvent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
//Biblioteca gr�fica


//	import aula4.FakeDataBase;

public class dsad {
	public static void main(String[] args){
		//********************Sobre a janela em si*******************//
		Display Minhainterface = new Display();
		final Shell minhashell = new Shell(Minhainterface);
		final Image image = new Image(Minhainterface, "pokeball.png"); //carrega a imagem para ser a miniatura
		minhashell.setText("Trainer Beginner");	//Nome da janela
		minhashell.setImage(image); //Icone da janela
		minhashell.setSize(280, 280); //Tamanho da janela
		//minhashell.setBackground(Minhainterface.getSystemColor(14)); //Cor de fundo da janela
		Image bg = new Image(Minhainterface, "pokeball_bg.png"); //Escolhe a imagem bg
		minhashell.setBackgroundImage(bg); //Faz a imagem bg ser imagem de fundo
		minhashell.setBackgroundMode(SWT.INHERIT_FORCE); //faz os backgrounds serem transparentes
		//***********************************************************//

		//				  final FakeDataBase fk = new FakeDataBase(); //cria banco de dados falso

		//************Sobre o texto nome e o campo para digitar**********//
		final Label nome = new Label(minhashell,SWT.NULL);
		nome.setLocation(10,10);
		nome.setText("Name:");
		nome.pack(); //O que isso faz? Mostra o texto, mas como?
		final Text textNome = new Text(minhashell, SWT.SINGLE | SWT.BORDER);
		textNome.pack();
		textNome.setLocation(50,8);
		textNome.setSize(200, 20);
		textNome.setBackground(Minhainterface.getSystemColor(1));
		//*****************************************************************//

		//************Sobre o texto idade e o campo para digitar***********//
		final Label idade = new Label(minhashell, SWT.NULL);
		idade.setLocation(10,45);
		idade.setText("Age:");
		idade.pack();
		final Text textIdade = new Text(minhashell, SWT.SINGLE|SWT.BORDER);
		textIdade.setLocation(50, 43);
		textIdade.pack();
		textIdade.setSize(25, 20);
		textIdade.setBackground(Minhainterface.getSystemColor(1));

		//***************************************************************//


		//**************Sobre o sexo do jogador**************************//
		Label sexo = new Label(minhashell,SWT.NULL);
		sexo.setLocation(10, 80);
		sexo.setText("Gender:");
		sexo.pack();
		final Button fem = new Button(minhashell, SWT.RADIO);
		final Button male = new Button(minhashell, SWT.RADIO);

		fem.setText("Female");
		fem.setLocation(60, 80);
		fem.pack();
		fem.addSelectionListener(new SelectionListener(){
			@Override
			public void widgetDefaultSelected(SelectionEvent e){
			}
			@Override
			public void widgetSelected(SelectionEvent e){
				if(fem.getSelection()==true){
					male.setSelection(false);
				}
			}
		});

		male.setText("Male");
		male.setLocation(60, 100);
		male.pack();
		male.addSelectionListener(new SelectionListener(){
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
			@Override
			public void widgetSelected (SelectionEvent e){
				if(male.getSelection()==true){
					fem.setSelection(false);
				}
			}
		});
		//***************************************************************//


		//****************************Bot�es******************************//
		//Bot�o s� aparece depois que coloca .pack();
		final Button Squirtle = new Button(minhashell, SWT.CHECK);
		final Button Charmander = new Button(minhashell, SWT.CHECK);
		final Button Bulbasaur = new Button(minhashell, SWT.CHECK);
		Squirtle.setText("Squirtle");
		Squirtle.setLocation(20, 170);
		Squirtle.pack();
		Squirtle.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void widgetSelected(SelectionEvent e){
				if (Squirtle.getSelection()==true){
					Bulbasaur.setSelection(false);
					Charmander.setSelection(false);
				}
			}
		});


		Charmander.setText("Charmander");
		Charmander.setLocation(87, 170);
		Charmander.pack();
		Charmander.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(Charmander.getSelection() == true) {
					Squirtle.setSelection(false);
					Bulbasaur.setSelection(false);
				}
			}
		});

		Bulbasaur.setText("Bulbasaur");
		Bulbasaur.setLocation(180, 170);
		Bulbasaur.pack();
		Bulbasaur.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(Bulbasaur.getSelection() == true) {
					Squirtle.setSelection(false);
					Charmander.setSelection(false);
				}
			}
		});
		//****************************************************************//
		//PARTE QUE O RAMON FEZ QUE EU N�O LI
		Button Submit = new Button(minhashell, SWT.TOGGLE);
		Submit.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				String firstPokemon = null;
				Boolean sexo = null;

				if(!nome.getText().equals("") && !idade.getText().equals("")) {
					if(fem.getSelection() == true) {
						sexo = false;
					} else if(male.getSelection() == true) {
						sexo = true;
					} else {
						System.out.println("ERROR! You didn't do something!(Gender)");
					}


					if(Squirtle.getSelection() == true) {
						firstPokemon = "Squirtle";
					} else if(Charmander.getSelection() == true) {
						firstPokemon = "Charmander";
					} else if(Bulbasaur.getSelection() == true) {
						firstPokemon = "Bulbasaur";
					} else {
						System.out.println("ERROR! You didn't do something! (POKEMON)");
					}
				}

				//						Trainer trainer = new Trainer(textNome.getText(),
				//								Integer.parseInt(textIdade.getText()), sexo, firstPokemon);
				System.out.println("gender: T= Male, F=Female");

				//						fk.addTrainer(trainer);
				//						fk.listaTreinadores();

			}
		});
		Submit.setText("Submit Data");
		Submit.setLocation(100, 205);
		Submit.pack();
		//***************************************************//
		//Monitoramento do botao
		Listener listener = new Listener() {
			@Override
			public void handleEvent(Event e) {
				String string = "Unknown";
				switch (e.type) {
				case SWT.MouseDown:
					string = "DOWN";
					break;
				case SWT.MouseMove:
					string = "MOVE";
					break;
				case SWT.MouseUp:
					string = "UP";
					break;
				}
				string += ": button: " + e.button + ", ";
				string += "stateMask=0x" + Integer.toHexString(e.stateMask);
				if ((e.stateMask & SWT.BUTTON1) != 0) {
					string += " BUTTON1";
				}
				if ((e.stateMask & SWT.BUTTON2) != 0) {
					string += " BUTTON2";
				}
				if ((e.stateMask & SWT.BUTTON3) != 0) {
					string += " BUTTON3";
				}
				if ((e.stateMask & SWT.BUTTON4) != 0) {
					string += " BUTTON4";
				}
				if ((e.stateMask & SWT.BUTTON5) != 0) {
					string += " BUTTON5";
				}
				System.out.println(string);
			}
		};
		minhashell.addListener(SWT.MouseDown, listener);
		minhashell.addListener(SWT.MouseMove, listener);
		minhashell.addListener(SWT.MouseUp, listener);

		//monitoramento do teclado em nome
		textNome.addKeyListener(new KeyAdapter(){
			@Override
			public void  keyPressed(KeyEvent e){
				String string = "";
				if(e.keyCode == SWT.BS)
				{
					string += "BACKSPACE";
				}
				if ((e.stateMask & SWT.TAB) != 0) {
					string += "TAB";
				}
				//check characters
				if(e.keyCode >=97 && e.keyCode <=122)
				{
					string += " " + e.character;
				}

				//check digit
				if(e.keyCode >=48 && e.keyCode <=57)
				{
					string += " " + e.character;
				}

				if(!string.equals("")) {
					System.out.print (string);
				}
			}
		});

		//monitoramento do teclado em idade
		textIdade.addKeyListener(new KeyAdapter(){
			@Override
			public void  keyPressed(KeyEvent e){
				String string = "";
				if(e.keyCode == SWT.BS)
				{
					string += "BACKSPACE";
				}
				if ((e.stateMask & SWT.TAB) != 0)
				{
					string += "TAB"; //Nada acontece
				}
				//check characters
				if(e.keyCode >=97 && e.keyCode <=122)
				{
					string += " " + e.character;
				}

				//check digit
				if(e.keyCode >=48 && e.keyCode <=57)
				{
					string += " " + e.character;
				}

				if(!string.equals("")) {
					System.out.print (string);
				}
			}
		});

		//Focus
		Submit.addFocusListener(new FocusAdapter(){
			public void focusGained(FocusEvent e){
				System.out.println(e.getSource().toString() +"gkgkjh");
				//widget + "Hat den Focus");
			}
		});
		//Modify
		//*************************IBAGENS*******************************//
		final Image improfoak = new Image(Minhainterface,"Spr_FRLG_Oak.png");
		final Image imsquirtle = new Image(Minhainterface,"Squirtle.png");
		final Image imbulba = new Image(Minhainterface,"Bulbasaur.png");
		final Image imcharm = new Image(Minhainterface,"Charmander.png");
		minhashell.addListener (SWT.Paint, new Listener() {
			@Override
			public void handleEvent(Event e) {
				GC gc = e.gc;
				gc.drawImage(improfoak, 160, 50);
				gc.drawImage(imsquirtle, 30, 140);
				gc.drawImage(imcharm, 110, 140);
				gc.drawImage(imbulba, 195, 140);
				gc.dispose();
			}
		});
		//***************************************************************//

		minhashell.open();
		while (!minhashell.isDisposed()) {
			if (!Minhainterface.readAndDispatch()) {
				Minhainterface.sleep();
			}
		}


		//image.dispose();
		Minhainterface.dispose();
	}
}




