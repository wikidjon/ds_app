package aaText;
import org.eclipse.swt.SWT;
//import org.eclipse.swt.events.DisposeEvent;
//import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
//import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

import a13_layouts1.a_Table;

public class a03_Folder {

	public void folder(Shell s, Display d){
		FillLayout fillLayout = new FillLayout ();
		TabFolder tabFolder1 = new TabFolder(s,SWT.NONE);
		tabFolder1.setBounds(10,10,370,350);
		@SuppressWarnings("unused")
		final FormLayout form = new FormLayout ();
		tabFolder1.setLayout (fillLayout);
	
		//Tabellen Tab
		Composite buttonComp = new Composite(tabFolder1,SWT.NONE);
		new a_Table().tabelle(buttonComp,d);
		TabItem item1 = new TabItem(tabFolder1,SWT.NONE);
		item1.setText("Tabellen");
//		item1.setData(fillLayout);
		item1.setControl(buttonComp);
//		buttonComp.pack();	

		//Text Tab
		Composite labelComp = new Composite(tabFolder1,SWT.NONE);
//*		new a30_Notepad1().createWidgets();  		//(labelComp,d);
//		labelComp.setLayout (fillLayout); 			//erweitert texteingabebereich
		
		final Font font = new Font(d, "Lucida Console", 10, SWT.NORMAL);
		Text text = new Text(labelComp, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text.setBounds(0,0,300,300);
		text.setFont(font);
//		text.setLayoutData(fillLayout);
		text.setSize(1000, 600);
//		text.setEditable(false);
		text.setText("Hier sind Eingaben m�glich.");

		TabItem item2 = new TabItem(tabFolder1,SWT.NONE);
		item2.setText("Text");
		item2.setControl(labelComp);

		// ???? Tab
		Composite testComp = new Composite(tabFolder1,SWT.NONE);
	//	testComp.setParent(new a10_Table().tabelle(s,d));
		testComp.setLayout (fillLayout);

		Label label1 = new Label(testComp,SWT.NONE);
		label1.setText("Label 1");
		label1.setBounds(10,10,250,20);
		Label label2 = new Label(testComp,SWT.NONE);
		label2.setText("Label 2");
		label2.setBounds(30,40,200,20);
		Label label3 = new Label(testComp,SWT.NONE);
		label3.setText("Hallo");	
		
//		label3.setParent(new a10_Table().tabelle(s,d));
//			setLayoutData(new a10_Table().tabelle(s,d));
		label3.setBounds(10,10,250,20);
//		new a10_Table().tabelle(s,d);
		
		TabItem item3 = new TabItem(tabFolder1,SWT.NONE);
		item3.setText("???");
		item3.setControl(testComp);
	}
}