package aaText;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

/** Pulldown-Men�s... */

public class a_menue{

	public void menue(Shell s, Display d){

		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;
		
		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
			}
		};
		
		menu = new Menu(menubar);
		
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);
		
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);
	
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);  //neues Menue an MB
		
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("Extras");
		item.setMenu(menu);
	
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Textmodus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Formatieren");
		final MenuItem formatItem = item;
		
		//Textmodus und Formatieren haengen an menu
		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);
	
	}
}