package studenten;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;


public class Datenverwaltung {

	public static void main (String [] args) {
		final Display display = new Display ();
		Shell shell = new Shell(display);

		shell.setSize (400, 200);
		Monitor pMonitor = display.getPrimaryMonitor ();
		Rectangle bounds = pMonitor.getBounds ();
		Rectangle rect = shell.getBounds ();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation (x, y);

		Menu menuBar = new Menu(shell, SWT.BAR);

		MenuItem itemDatei = new MenuItem(menuBar, SWT.CASCADE);
		itemDatei.setText("&Eingabe");

		Menu filemenu = new Menu(shell, SWT.DROP_DOWN);
		itemDatei.setMenu(filemenu);

		MenuItem actionItem = new MenuItem(filemenu, SWT.PUSH);
		actionItem.setText("Neu");

		actionItem.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e){
				//				eins.ein(display);
			}
		});

		MenuItem actionItem2 = new MenuItem(filemenu, SWT.PUSH);
		actionItem2.setText("&�ndern");



		//Menu menuBarb = new Menu(shell, SWT.BAR);
		MenuItem itemBearbeiten = new MenuItem(menuBar, SWT.CASCADE);
		//itemDatei.setText("&Eingabe");
		//MenuItem itemBearbeiten = new MenuItem(menuBar, SWT.PUSH);
		itemBearbeiten.setText("&Anzeigen");


		Menu filemenu1 = new Menu(shell, SWT.DROP_DOWN);
		itemBearbeiten.setMenu(filemenu1);

		MenuItem actionItem1 = new MenuItem(filemenu1, SWT.PUSH);
		actionItem1.setText("&Alle");

		MenuItem actionItem3 = new MenuItem(filemenu1, SWT.PUSH);
		actionItem3.setText("&Ausw�hlen");

		final Composite com = new Composite(shell, SWT.NONE);
		FillLayout gridLayout = new FillLayout();
		//gridLayout.numColumns = 4;
		com.setLayout(gridLayout);
		Label L = new Label(com,0);
		L.setText ("Datenangabe");
		L.setBounds(100, 10 ,200,200);
		L.pack ();
		com.setBackground(display.getSystemColor(SWT.COLOR_RED));
		//shell.setMenuBar(menuBarb);
		shell.setMenuBar(menuBar);
		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}

}