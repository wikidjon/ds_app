package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a31_button2 {

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300, 300);
		shell.open();

		Button button1 = new Button(shell,SWT.PUSH);
		button1.setText("Button 1");
		button1.setLocation(10,10);
		button1.setSize(100,20);

		button1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Button1 gedrueckt!!");
			}
		});

		Button button2 = new Button(shell, SWT.ARROW);
		button2.setSize(140,140);
		//	button2.setSize(120,120);
		button2.setText("B2");    ///nicht bei Arrow!!!
		button2.setLocation(200,200);

		button2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Button2 gedrueckt");
			}
		});

		Button button3 = new Button(shell, SWT.FLAT|SWT.TOGGLE);
		//SWT.TOGGLE - wie SWT.PUSH, nur dass beim ersten Klick die Taste gedr�ckt bleibt und beim zweiten Klick wieder ausl�st.
		//SWT.FLAT - Button wird nicht dreidimensional gezeichnet, sondern �flach�.
		//wird nicht von allen Systemen unterst�tzt
		button3.setText("Button 3");
		button3.setSize(100,50);
		button3.setLocation(20,150);

		button3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Button3 gedrueckt");
			}
		});

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
		button1.dispose();
		button2.dispose();
		button3.dispose();
	}
}
