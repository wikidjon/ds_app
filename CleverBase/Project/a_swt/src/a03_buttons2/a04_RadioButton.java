package a03_buttons2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class a04_RadioButton {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setLayout(new GridLayout(3, true));

    // Create three radio buttons
    new Button(shell, SWT.RADIO).setText("Radio 1");
    new Button(shell, SWT.RADIO).setText("Radio 2");
    new Button(shell, SWT.RADIO).setText("Radio 3");


    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}