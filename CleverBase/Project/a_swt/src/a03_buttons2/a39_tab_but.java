package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a39_tab_but {

public static void main (String [] args) {
	Display display = new Display ();
	
	final Color red = display.getSystemColor (SWT.COLOR_RED);
	final Color blue = display.getSystemColor (SWT.COLOR_BLUE);
	
	Shell shell = new Shell (display);
	Button b = new Button (shell, SWT.PUSH);
	b.setBounds (10, 10, 100, 32);
	b.setText ("Button");
	shell.setDefaultButton (b);
	
	final Canvas c = new Canvas (shell, SWT.BORDER);
	c.setBounds (10, 50, 100, 32);
	c.addListener (SWT.Traverse, new Listener () {
		public void handleEvent (Event e) {
			switch (e.detail) {
				/* mit tab tasten traversieren */
				case SWT.TRAVERSE_ESCAPE:
				case SWT.TRAVERSE_RETURN:
				case SWT.TRAVERSE_TAB_NEXT:	
				case SWT.TRAVERSE_TAB_PREVIOUS:
				case SWT.TRAVERSE_PAGE_NEXT:	
				case SWT.TRAVERSE_PAGE_PREVIOUS:
					System.out.println("hallo");
					break;
			}
		}
	});
	c.addListener (SWT.FocusIn, new Listener () {
		public void handleEvent (Event e) {
			c.setBackground (red);
		}
	});
	c.addListener (SWT.FocusOut, new Listener () {
		public void handleEvent (Event e) {
			c.setBackground (blue);
		}
	});
	c.addListener (SWT.KeyDown, new Listener () {
		public void handleEvent (Event e) {
			System.out.println ("Taste");
			}
	});

	Text t = new Text (shell, SWT.SINGLE | SWT.BORDER);
	t.setBounds (10, 85, 100, 32);

	Text r = new Text (shell, SWT.MULTI | SWT.BORDER);
	r.setBounds (10, 120, 100, 32);
	
	c.setFocus ();
	shell.setSize (200, 200);
	shell.open ();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
} 
