package a03_buttons2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a06_ArrowButtonExample {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		/*
		shell.setLayout(new GridLayout(4, true));
		new Button(shell, SWT.ARROW);
		new Button(shell, SWT.ARROW | SWT.LEFT);
		new Button(shell, SWT.ARROW | SWT.DOWN);
		new Button(shell, SWT.ARROW | SWT.RIGHT);
		 */
		///*
		Button b1, b2, b3, b4;
		b1 = new Button(shell, SWT.ARROW);
		b1.setLocation(10,10);
		b1.setSize(40,40);

		b2 = new Button(shell, SWT.ARROW | SWT.LEFT);
		b2.setLocation(100,10);
		b2.setSize(40,40);

		b3 = new Button(shell, SWT.ARROW | SWT.DOWN);
		b3.setLocation(200,10);
		b3.setSize(40,40);

		b4 = new Button(shell, SWT.ARROW | SWT.RIGHT);
		b4.setLocation(300,10);
		b4.setSize(40,40);
		//*/
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}