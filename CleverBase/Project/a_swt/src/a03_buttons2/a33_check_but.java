package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a33_check_but {
	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);
		shell.setLayout (new RowLayout (SWT.VERTICAL));  		//!!!
		for (int i=0; i<10; i++) {
			final Button button = new Button (shell, SWT.CHECK); 		//!!! alle Widgets heissen button
			button.setText ("Button " + i);
			if (i == 0)
			{
				button.setSelection (true);  			// Anfangsauswahl
			}
		}
		final Button button = new Button (shell, SWT.PUSH);
		button.setText ("Auswahl auf B4");
		button.addListener (SWT.Selection, new Listener () {	// allgemeines Listenerobjekt !!!
			@Override
			public void handleEvent (final Event event) {
				final Control [] children = shell.getChildren ();
				final Button newButton = (Button) children [4];
				/************ Loeschen der alten Auswahl
				for (int i=0; i<children.length; i++) {
					final Control child = children [i];
					if (child instanceof Button && (child.getStyle () & SWT.CHECK) != 0) {
						( (Button) child).setSelection (false);
					}
				}
				//***********************/
				newButton.setSelection (true);
			}
		});
		shell.pack ();
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}