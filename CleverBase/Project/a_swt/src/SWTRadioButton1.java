
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

/**
 * @author Thomas.Darimont
 * 
 */
public class SWTRadioButton1{

	static String selectedChoice = "CHOICE_0";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setText("SWTRadioButtonExample");

		shell.setLayout(new GridLayout());

		Group group = new Group(shell, SWT.SHADOW_IN);
		group.setText("Select: ");
		group.setLayout(new RowLayout(SWT.VERTICAL));

		SelectionListener selectionListener = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectedChoice = (String) ((Button) e.getSource()).getData();
			}
		};

		for (int i = 0; i < 10; i++) {
			Button button = new Button(group, SWT.RADIO);
			button.addSelectionListener(selectionListener);
			button.setText("Choice: " + i);
			button.setData("CHOICE_" + i);

		}

		Button btnCheck = new Button(group, SWT.PUSH);
		btnCheck.setText("check");
		btnCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(selectedChoice);
			}
		});

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

}