import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;


public class Shelltestdesigner extends Shell {
	private DataBindingContext m_bindingContext;
	private Text text;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		Display display = Display.getDefault();
		Realm.runWithDefault(SWTObservables.getRealm(display), new Runnable() {
			public void run() {
				try {
					Display display = Display.getDefault();
					Shelltestdesigner shell = new Shelltestdesigner(display);
					shell.open();
					shell.layout();
					while (!shell.isDisposed()) {
						if (!display.readAndDispatch()) {
							display.sleep();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public Shelltestdesigner(Display display) {
		super(display, SWT.SHELL_TRIM);
		setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		setText("TEST");
		setLayout(new FillLayout(SWT.VERTICAL));
		
		Composite composite = new Composite(this, SWT.NONE);
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		composite.setLayout(new GridLayout(1, false));
		
		ToolBar toolBar = new ToolBar(composite, SWT.FLAT | SWT.RIGHT);
		toolBar.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		toolBar.setSize(450, 121);
		
		ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
		tltmNewItem.setText("test    ");
		
		ToolItem tltmNewItem_1 = new ToolItem(toolBar, SWT.RADIO);
		tltmNewItem_1.setImage(SWTResourceManager.getImage(Shelltestdesigner.class, "/icons/cancel.gif"));
		tltmNewItem_1.setText("New Item");
		
		ToolItem toolItem_1 = new ToolItem(toolBar, SWT.SEPARATOR);
		
		ToolItem tltmNewItem_2 = new ToolItem(toolBar, SWT.RADIO);
		tltmNewItem_2.setText("vvvvv");
		
		ToolItem toolItem = new ToolItem(toolBar, SWT.SEPARATOR);
		
		ToolItem tltmRadioItem = new ToolItem(toolBar, SWT.RADIO);
		tltmRadioItem.setText("gjhgjh");
		
		Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_BACKGROUND));
		composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite composite_2 = new Composite(composite_1, SWT.NONE);
		composite_2.setDragDetect(false);
		composite_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_YELLOW));
		composite_2.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		TabFolder tabFolder = new TabFolder(composite_2, SWT.NONE);
		tabFolder.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		tabFolder.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		
		TabItem tbtmTree = new TabItem(tabFolder, SWT.NONE);
		tbtmTree.setImage(SWTResourceManager.getImage(Shelltestdesigner.class, "/icons/ball.png"));
		tbtmTree.setText("Tree1");
		
		Tree tree = new Tree(tabFolder, SWT.BORDER);
		tbtmTree.setControl(tree);
		tree.setHeaderVisible(true);
		tree.setDragDetect(false);
		tree.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		
		TreeItem trtmNewTreeitem = new TreeItem(tree, SWT.NONE);
		trtmNewTreeitem.setImage(SWTResourceManager.getImage(Shelltestdesigner.class, "/icons/folder.gif"));
		trtmNewTreeitem.setText("Opa");
		
		TreeItem trtmNewTreeitem_3 = new TreeItem(trtmNewTreeitem, SWT.NONE);
		trtmNewTreeitem_3.setText("Tochter");
		trtmNewTreeitem.setExpanded(true);
		
		TreeItem trtmNewTreeitem_2 = new TreeItem(tree, SWT.NONE);
		trtmNewTreeitem_2.setImage(SWTResourceManager.getImage(Shelltestdesigner.class, "/icons/directory.gif"));
		trtmNewTreeitem_2.setText("Oma");
		
		TreeItem trtmSohn = new TreeItem(trtmNewTreeitem_2, 0);
		trtmSohn.setText("Sohn 1");
		
		TreeItem trtmNewTreeitem_1 = new TreeItem(trtmNewTreeitem_2, SWT.NONE);
		trtmNewTreeitem_1.setText("Sohn 2");
		trtmNewTreeitem_2.setExpanded(true);
		
		TabItem tbtmNewItem = new TabItem(tabFolder, SWT.NONE);
		tbtmNewItem.setText("tree 2");
		
		TabItem tbtmCtabFolder = new TabItem(tabFolder, SWT.NONE);
		tbtmCtabFolder.setText("CTab Folder");
		
		CTabFolder tabFolder_1 = new CTabFolder(tabFolder, SWT.BORDER);
		tabFolder_1.setSelectionBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		tbtmCtabFolder.setControl(tabFolder_1);
		
		CTabItem tbtmNewItem_1 = new CTabItem(tabFolder_1, SWT.NONE);
		tbtmNewItem_1.setText("hallo");
		
		CTabItem tbtmNewItem_2 = new CTabItem(tabFolder_1, SWT.NONE);
		tbtmNewItem_2.setText("New Item");
		
		CTabItem tbtmNewItem_3 = new CTabItem(tabFolder_1, SWT.NONE);
		tbtmNewItem_3.setImage(SWTResourceManager.getImage(Shelltestdesigner.class, "/icons/forward.gif"));
		tbtmNewItem_3.setText("New Item");
		
		TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("New Item");
		
		Composite composite_3 = new Composite(composite_1, SWT.NONE);
		
		Composite composite_4 = new Composite(composite_1, SWT.NONE);
		composite_4.setBackground(SWTResourceManager.getColor(SWT.COLOR_CYAN));
		
		text = new Text(composite, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Menu menu = new Menu(this, SWT.BAR);
		setMenuBar(menu);
		
		MenuItem mntmNewItem = new MenuItem(menu, SWT.NONE);
		mntmNewItem.setText("dasdasd");
		
		MenuItem mntmNewItem_1 = new MenuItem(menu, SWT.NONE);
		mntmNewItem_1.setText("dfgfdg");
		createContents();
		m_bindingContext = initDataBindings();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {

	}
	
	private void populateList(String type) {
		if(type.equals("test"))
		{
			
			//text.setText("Datei 1");
			//text.setText("Datei 2");
		}
		
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		return bindingContext;
	}
}
