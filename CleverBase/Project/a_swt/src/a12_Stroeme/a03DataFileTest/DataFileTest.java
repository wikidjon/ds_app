package a12_Stroeme.a03DataFileTest;

import java.io.*;
import java.util.*;

//**************************************************************************
/* Schreibt Datenarray (Angestellter) in eine Textdatei (Angestellter.dat)*/
/* mit PrintWriter                                                        */
/* Datensätze werden durch einen Begrenzer | getrennt                     */
//**************************************************************************

public class DataFileTest {
	public static void main(String[] args) {
		Angestellter[] angest = new Angestellter[3];

		angest[0] = new Angestellter("Angie Merkel", 750000, 1950, 12, 5);
		angest[1] = new Angestellter("Pofalla Pofalla Pofalla", 50000, 1958, 10, 1);
		angest[2] = new Angestellter("Guido Guido", 40000, 1960, 3, 15);

		try {
			// Alle Mitarbeiterdatensätze in die Datei Angestellte.dat schreiben
			PrintWriter out = new PrintWriter(new FileWriter("C:/Ausgang/Angestellte.dat"));
			writeData(angest, out);
			out.close();

			// Alle Datensätze in ein neues Array einlesen
			BufferedReader in = new BufferedReader(new FileReader("C:/Ausgang/Angestellte.dat"));
			Angestellter[] newAngest = readData(in);
			in.close();

			// neu eingelesenen Mitarbeiterdatensätze ausgeben
			for (int i = 0; i < newAngest.length; i++)
				System.out.println(newAngest[i]);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	/****************************************************************
	 * Schreibt alle Mitarbeiter in einem Array in einen PrintWriter
	 *****************************************************************/
	static void writeData(Angestellter[] e, PrintWriter out) throws IOException {
		// Anzahl der Mitarbeiter schreiben
		out.println(e.length);

		for (int i = 0; i < e.length; i++)
			e[i].writeData(out);
	}

	/*****************************************************************
	 * Liest ein Array von Mitarbeitern aus einem gepufferten Reader
	 *****************************************************************/
	static Angestellter[] readData(BufferedReader in) throws IOException {
		// Die Größe des Arrays abrufen
		int n = Integer.parseInt(in.readLine());

		Angestellter[] e = new Angestellter[n];
		for (int i = 0; i < n; i++) {
			e[i] = new Angestellter();
			e[i].readData(in);
		}
		return e;
	}
}

// *********************************************************************************************************
// *********************************************************************************************************

class Angestellter {
	private String name;
	private double lohn;
	private Date gebDatum;

	public Angestellter() {
	}

	public Angestellter(String n, double s, int jahr, int monat, int tag) {
		name = n;
		lohn = s;
		GregorianCalendar calendar = new GregorianCalendar(jahr, monat - 1, tag);
		// GregorianCalendar verwendet 0 für Januar
		gebDatum = calendar.getTime();
	}

	public String getName() {
		return name;
	}

	public double getlohn() {
		return lohn;
	}

	public Date getgebDatum() {
		return gebDatum;
	}

	public void lohnzuschlag(double Prozent) {
		double zuschlag = lohn * Prozent / 100;
		lohn += zuschlag;
	}

	public String toString() {
		return name + ":\tLohn=" + lohn + ",\t Geb.-Datum=" + gebDatum;
		// getClass().getName()
	}

	/**************************************************************
	 * Schreibt Mitarbeiterdaten in einen PrintWriter
	 *****************************************************************/
	public void writeData(PrintWriter out) throws IOException {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(gebDatum);
		// senkrechter Strich ist Trennzeichen (Token) und wird in readData mit
		// StringTokenizer behandelt
		out.println(name + "|" + lohn + "|" + calendar.get(Calendar.YEAR) + "|"
				+ (calendar.get(Calendar.MONTH) + 1) + "|"
				+ calendar.get(Calendar.DAY_OF_MONTH));
	}

	/********************************************************************
	 * Liest Mitarbeiterdaten aus einem gepufferten Reader mit Tokenizer
	 *********************************************************************/
	public void readData(BufferedReader in) throws IOException {
		String s = in.readLine();
		StringTokenizer t = new StringTokenizer(s, "|");	//Zerlegung lexiklischer Einheiten
		name = t.nextToken();
		lohn = Double.parseDouble(t.nextToken());
		int y = Integer.parseInt(t.nextToken());
		int m = Integer.parseInt(t.nextToken());
		int d = Integer.parseInt(t.nextToken());
		GregorianCalendar calendar = new GregorianCalendar(y, m - 1, d);
		// GregorianCalendar verwendet 0 für Januar
		gebDatum = calendar.getTime();
	}

}
