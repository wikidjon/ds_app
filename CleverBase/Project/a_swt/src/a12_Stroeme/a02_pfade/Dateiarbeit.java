package a12_Stroeme.a02_pfade;


// Spezifikation von Dateien bzw. Verzeichnissen und Ausgabe des Inhaltes �ber Bildschirm bzw. Druck
// c:\Lehre\  Spezifikation leer

import java.io.*;

class Dateiarbeit {

	private static String pfad = "";
	private static String extension = "";

	public static void main(String args[]) {
		do 
			System.out.println("\nDatei- und Verzeichnisarbeit");
		while (menue());
	}

	public static void Spezifikation() {
		Eingabe ein = new Eingabe();
		pfad = ein.EingabeEinesString("Datei- oder Pfadname: "); //c:  oder c:\  oder c:/Ausgang
		// ansonsten NullPointerException
		if (pfad.endsWith("\\"))
			pfad += "\\.";				// aktuelles Verzeichnis -    \.  txt
		File fil = new File(pfad);		// File-Objekt
		if (!fil.exists()) 
			System.out.println("Ihr Pfad/Datei existiert nicht");
		else  
			extension = ein.EingabeEinesString("Spezifikation: ");
	}

	public static void BildschirmAusgabe() {
		File fil = new File(pfad);		// File-Objekt
		if (fil.exists()) 	{
			if (fil.isDirectory()) 	{
				System.out.println("Ihr spezifiziertes Verzeichnis: ");
				String fils[] = fil.list();
				for (int i=0; i<fils.length; ++i) 	 
					if (fils[i].endsWith(extension)) { 
						if (((i+1) % 15) == 0) // 15 Zeilen
							new uti().waitForReturn(); 
							System.out.println("  "+fils[i]);
					}
			} else 	if (fil.isFile()) 	{
				System.out.println("Ihre Datei:  "+pfad);
		//		new TextdateiLesen().DateiEinLineNumber(pfad);
			}
		} 
	}

	public static void DruckAusgabe() {
		//Ausdruck eines Verzeichnisinhaltes
		OutputStream f1 ;				// Ausgabe-Strom
		File fil = new File(pfad);			// File-Objekt
		try 	{
			f1 = new FileOutputStream("LPT1");
			PrintWriter ps = new PrintWriter(f1);

			if (fil.exists()) 	{
				if (fil.isDirectory()) {
					// Ausdruck eines Verzeichnisses
					ps.println("Ihr spezifiziertes Verzeichnis: " + pfad + "\r\n");
					String fils[] = fil.list();
					for (int i=0; i<fils.length; ++i) 	{ 
						if (fils[i].endsWith(extension))  
							ps.println("  "+fils[i]+"\r");
					}
				} else 	
					
		// Ausdruck einer Datei		
			if (fil.isFile()) 	{
			ps.println("Ihre Datei:  "+pfad);

			String s ;
			// DataInputStream f2 ;
			BufferedReader f2;
			// LineNumberInputStream f;
			LineNumberReader f;
			
			f = new LineNumberReader(
				new BufferedReader(
//				new BufferedInputStream(
				new FileReader(pfad)));
//				new FileInputStream(pfad)));

			f2 = new BufferedReader(f);

			do  {
				s = f2.readLine();
				if (s == null) break;
				if ((f.getLineNumber() % 60) == 0) 	ps.print("\f");
				ps.println("\r" + f.getLineNumber() + " : " + s );
			}
			while (s != null);
			f.close();
			ps.print("\f");  		// form feed
			f2.close();
					
			}
		}
		f1.close();
	}
	catch (IOException e) {
		System.out.println("Fehler beim Drucken");
	}
	}

	public static boolean menue() {
		char ein;
		boolean Ende = true;
		System.out.println("(s)  Datei-/Verzeichnisspezifikation");
		System.out.println("(a)  Anzeigen");
		System.out.println("(d)  Drucken");
		ein = ((new Eingabe()).EingabeEinesChar("Ihre Wahl: "));
		switch (ein) {
			case 's': 
				Spezifikation(); 
				break;
			case 'a': 
				BildschirmAusgabe(); 
				break;
			case 'd': 
				DruckAusgabe();
				break;
			default: Ende = false; break;
		} 
		return Ende;
	}
}
