package a12_Stroeme.a01_stroeme;

// gepuffertes und ungepuffertes Lesen einer Textdatei und Ausgabe der Zeilennummer

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;

public class TextdateiLesen {

	// **********************************************************************************************************
	public void DateiDrucken(final String str1) {
		// Drucken einer Datei mit Zeilennumerierung

		String s;

		BufferedReader f1; // alt:DataInputStream f1 ;
		LineNumberReader f;
		OutputStream f2; // Ausgabe-Strom

		try {
			f = new LineNumberReader(new FileReader(str1));
			f1 = new BufferedReader(f);

			f2 = new FileOutputStream("LPT1");
			final PrintWriter ps = new PrintWriter(f2);
			ps.println(" Druck Test  ");
			do {
				s = f1.readLine();
				if (s == null) {
					break;
				}
				if ((f.getLineNumber() % 60) == 0) {
					ps.print("\f");
				}
				ps.println(f.getLineNumber() + " : " + s);
			} while (s != null);
			f.close();
			ps.print("\f"); 											// form feed
			f2.close();
		} catch (final IOException e) {
			System.out.println("Fehler beim Lesen der Datei" + e);
		}
	}

	// **********************************************************************************************************
	public void DateiEin(final String str1) { // Ungepuffertes Lesen einer Datei

		int c;
		Reader f;

		try {
			// Filereader ist eine konkrete Klasse der abstrakten Klasse
			// InputStream und stellt Verbindung zu einem Eingabeger�t her
			// -- �ffnet die Datei str1
			f = new FileReader(str1);

			while ((c = f.read()) != -1) {
				System.out.print((char) c);
			}
			f.close();
		} catch (final IOException e) {
			System.out.println("Fehler beim Lesen der Datei " +str1);
		}
	}

	// **********************************************************************************************************
	public void DateiEinLineNumber(final String str1) {
		// Zeilennumerierung beim gepufferten Lesen einer Datei

		String s;
		// BufferedReader f1;
		// bisher: DataInputStream f1 ;
		LineNumberReader f;

		try {
			// explizit benennen, um f.getLineNumber aufrufen zu k�nnen
			f = new LineNumberReader(new BufferedReader(new FileReader(str1)));

			// f = new LineNumberReader(new FileReader(str1));
			// f1 = new BufferedReader(f);

			do {
				s = f.readLine();
				// damit letzter null-s nicht ausgeschrieben wird
				if (s == null) {
					break;
				}
				// Bildschirmausgabe wird nach 10 Zeilen angehalten
				if ((f.getLineNumber() % 10) == 0) {
					new uti().waitForReturn();
				}
				System.out.println(f.getLineNumber() + " : " + s);
			} while (s != null);
			f.close();
		} catch (final IOException e) {
			System.out.println("Fehler beim Lesen der Datei - " + str1);
		}
	}

	// **********************************************************************************************************
	public void PufDateiEin(final String str1) {
		// Gepuffertes Lesen einer TextDatei

		int c;
		Reader f;
		try {
			f = new BufferedReader(new FileReader(str1));
			while ((c = f.read()) != -1) {
				System.out.print((char) c);
			}
			f.close();
		} catch (final IOException e) {
			System.out.println("Fehler beim Lesen der Datei");
		}
	}

}
