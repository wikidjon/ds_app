package a12_Stroeme.a01_stroeme;
import java.io.*;

public class TextdateiSchreiben 
{
	public void AusString(String str1) {						// Ungepuffertes Schreiben in TextDatei

		Writer f1 ;								// ist eine konkrete Klasse der abstrakten Klasse 
													// OutputStream und stellt Verbindung zu einem 
													// Ausgabeger�t her
		try {										// �ffnet eine Datei str1 und l�scht den Inhalt
													// wenn die Datei schon existiert
			f1 = new FileWriter(str1);

			for (int i=0; i < 10000; ++i){
				String s = "Das ist Zeile "+ i + "\r\n";
				for (int j=0; j < s.length(); ++j){
					f1.write(s.charAt(j));
				}
			}			
			f1.close();
		}
		catch (IOException e) {
			System.out.println("Fehler beim Erstellen der Datei");
		}
	}

//**********************************************************************************************************

	public void PufAusString(String str1) {					// gepuffertes Schreiben in TextDatei

		Writer f1;  										// ggf noch f2 deklarieren

		try {												//	f1 = new FileOutputStream(str1);
															//	f2 = new BufferedOutputStream(f1);     oder:

			f1 = new BufferedWriter(new FileWriter(str1));	
														//	es wird ein existierender OutputStream �bergeben, 
														//	an den die gepufferten Ausgaben weitergereicht werden.
			for (int i=0; i < 10000; ++i){
				String s = "Das ist Zeile "+ i + "\r\n";
				for (int j=0; j < s.length(); ++j){
					f1.write(s.charAt(j));
				}
			}
			f1.close();
		}
		catch (IOException e) {
			System.out.println("Fehler beim Erstellen der Datei");
		}
	}

//**********************************************************************************************************

	public void AlleAusDatei(String str1) {			// Obige Methoden geben nur Bytes in eine Datei aus.
													// Diese Methode gilt f�r alle Datentypen. Das Beispiel gibt
													// Strings, Ganz- und Flie�komazahlen aus.

		PrintWriter f;  
		double sum = 0.0;

		try {
		
			f = new PrintWriter(new BufferedWriter(	new FileWriter(str1)));
													// PrintWriter schreibt in einen BufferedWriter 
													// der wiederum in den FileWriter schreibt. 
													// So werden Datentypen im ASCII-Format gepuffert 
													// in eine Textdatei geschrieben.

			for (int i=1; i <= 1024; i*=2){
				sum = 1.0/i;
				f.print("\r\nOperation: 1/");
				f.print(i);
				f.print("   Ergebnis: ");
				f.println(sum);
			}
			f.close();
		}
		catch (IOException e) {
			System.out.println("Fehler beim Erstellen der Datei");
		}
	}

}

