package a12_Stroeme.a01_stroeme;


//	Hauptprogramm zum Lesen und Schreiben von verschiedenen
//	Datentypen in Dateien.

class DateiOperationen {

	public static void main(String args[]) {

		long start;

		TextdateiSchreiben aus = new TextdateiSchreiben();
//		Eingabe ein = new Eingabe();
		ZeitMessung dauer = new ZeitMessung();
		uti warten = new uti();
		TextdateiLesen einlesen = new TextdateiLesen();

		// Schreiben in eine Textdatei
		// aus.AusString (ein.EingabeEinesString("Name der Ausgabedatei (.txt): "));

		// Zeitvergleich zwischen gepufferter und ungepufferter Ausgabe
		System.out.println("Start der ungepufferten Dateiausgabe in Datei hallo1.txt ...");
		start = dauer.ZeitMessEin();
		aus.AusString ("hallo1.txt");
		System.out.println("Ungepuffert: "+ (dauer.ZeitMessAus() - start));
		warten.waitForReturn();

		System.out.println("Start der gepufferten Dateiausgabe in Datei hallo2.txt ...");
		start = dauer.ZeitMessEin();
		aus.PufAusString ("hallo2.txt");
		System.out.println("Gepuffert: "+ (dauer.ZeitMessAus() - start));

		System.out.println("\n... es folgt das gepufferte Einlesen der Datei TextdateiLesen.java ...");
		warten.waitForReturn();

		start = dauer.ZeitMessEin();
		// gepuffertes Einlesen einer Datei 
		einlesen.PufDateiEin("C:/Ausgang/TextdateiLesen.java");
		System.out.println("Gepuffertes Einlesen Textdatei: "+ (dauer.ZeitMessAus() - start));

		aus.AlleAusDatei ("C:/Ausgang/xxxxx.txt");
		
		warten.waitForReturn();
	}
}

