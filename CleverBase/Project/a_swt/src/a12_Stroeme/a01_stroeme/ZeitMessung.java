package a12_Stroeme.a01_stroeme;
// ZeitMessung mit Zeitstempel 
class ZeitMessung {

	private long t1, t2;

	public long ZeitMessEin(){
		t1 = System.currentTimeMillis();
		return t1;
	}

	public long ZeitMessAus(){
		t2 = System.currentTimeMillis();
		return t2;
	}
}

