package a11_table_old;

 
/*
 * example snippet: embed a JTable in SWT (no flicker)
 */
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.util.Vector;

import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class b154 {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		Composite composite = new Composite(shell, SWT.NO_BACKGROUND | SWT.EMBEDDED);
		
		/*
		* Set a Windows specific AWT property that prevents heavyweight
		* components from erasing their background. Note that this
		* is a global property and cannot be scoped. It might not be
		* suitable for your application.
		*/
		try {
			System.setProperty("sun.awt.noerasebackground","true");
		} catch (NoSuchMethodError error) {}

		/* Create and setting up frame */
		Frame frame = SWT_AWT.new_Frame(composite);
		Panel panel = new Panel(new BorderLayout()) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1193286454704531007L;

			public void update(java.awt.Graphics g) {
				/* Do not erase the background */ 
				paint(g);
			}
		};
		frame.add(panel);
		JRootPane root = new JRootPane();
		panel.add(root);
		java.awt.Container contentPane = root.getContentPane();

		/* Creating components */
		int nrows = 1000, ncolumns = 10;
		Vector<Vector<?>> rows = new Vector<Vector<?>>();
		for (int i = 0; i < nrows; i++) {
			@SuppressWarnings("rawtypes")
			Vector row = new Vector();
			for (int j = 0; j < ncolumns; j++) {
				row.addElement("Item " + i + "-" + j);
			}
			rows.addElement(row);
		}
		@SuppressWarnings("rawtypes")
		Vector columns = new Vector();
		for (int i = 0; i < ncolumns; i++) {
			columns.addElement("Column " + i);
		}
		JTable table = new JTable(rows, columns);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.createDefaultColumnsFromModel();
		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.setLayout(new BorderLayout());
		contentPane.add(scrollPane);
		
		shell.open();
		while(!shell.isDisposed()) {
			if (!display.readAndDispatch()) display.sleep();
		}
		display.dispose();
	}
}
