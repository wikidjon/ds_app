package a11_table_old;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.*;

public class TableShellExample {
	
	public static void main(String[] args) {
	    new TableShellExample();
		}
       
    TableShellExample()    {
        Display d = new Display();
        Shell s = new Shell(d);
        
        s.setSize(250,200);
        s.setImage(new Image(d, "src/icons/cut.gif"));
        s.setText("Tabllen Shell Beispiel");
        s.setLayout(new FillLayout());
        
        Table t = new Table(s, SWT.BORDER);

        TableColumn tc1 = new TableColumn(t, SWT.CENTER);
        TableColumn tc2 = new TableColumn(t, SWT.CENTER);
        TableColumn tc3 = new TableColumn(t, SWT.CENTER);
        tc1.setText("Vorname");
        tc2.setText("Nachname");
        tc3.setText("Addresse");
        tc1.setWidth(70);
        tc2.setWidth(70);
        tc3.setWidth(80);
        t.setHeaderVisible(true);

        TableItem item1 = new TableItem(t,SWT.NONE);
        item1.setText(new String[] {"Mick","Jagger","Merse"});
        TableItem item2 = new TableItem(t,SWT.NONE);
        item2.setText(new String[] {"Keith","Richard","Braunsbedra"});
        TableItem item3 = new TableItem(t,SWT.NONE);
        item3.setText(new String[] {"Brian","Jones","Klobikau"});
        
        s.open();
        while(!s.isDisposed()){
            if(!d.readAndDispatch())
                d.sleep();
        }
        d.dispose();
    }


}