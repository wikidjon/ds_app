package a13_layouts1;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
public class a_Table {

	public void tabelle(Composite s, Display d) {
		Table table1 = new Table(s, SWT.BORDER);
		FillLayout fillLayout = new FillLayout ();
		table1.setLayout(fillLayout);
		table1.setBounds(10,10,270,60);
		table1.setLinesVisible(true);

		TableColumn name = new TableColumn(table1,SWT.LEFT);
		name.setText("Name");
		name.setWidth(50);
		TableColumn alter = new TableColumn(table1,SWT.RIGHT);
		alter.setText("Alter");
		alter.setWidth(30);
		TableColumn address = new TableColumn(table1,SWT.LEFT);
		address.setText("Addresse");
		address.setWidth(200);

		TableItem item1 = new TableItem(table1,SWT.NONE);
		item1.setText(new String[] {"Ulf-Inge","25","Geusaerstrasse"});
		TableItem item2 = new TableItem(table1,SWT.NONE);
		item2.setText(new String[] {"Peter-Paul","22","Geusaerstrasse"});

		Table table2 = new Table(s, SWT.CHECK|SWT.HIDE_SELECTION);
		table2.setBounds(10,80,270,80);
		table2.setHeaderVisible(true);

		TableColumn fach = new TableColumn(table2,SWT.LEFT);
		fach.setText("Fach");
		fach.setWidth(100);
		TableColumn ergeb = new TableColumn(table2,SWT.LEFT);
		ergeb.setText("Ergebnis");
		ergeb.setWidth(170);

		TableItem fach1 = new TableItem(table2,SWT.NONE);
		fach1.setText(0,"Programmieren");
		fach1.setText(1,"nicht bestanden");
		fach1.setChecked(true);
		TableItem fach2 = new TableItem(table2,SWT.NONE);
		fach2.setText(new String[] {"Mathe","..???.."});
		fach2.setBackground(new Color(d,255,0,0));
		TableItem fach3 = new TableItem(table2,SWT.NONE);
		fach3.setText(new String[] {"Physik","nicht bestanden"});

		Table table3 = new Table(s, SWT.FULL_SELECTION);
		table3.setLinesVisible(true);
		table3.setBounds(10,180,270,80);
		//table3.setHeaderVisible(true);
		
		TableColumn erstSpa = new TableColumn(table3,SWT.LEFT);
		erstSpa.setResizable(true);
		erstSpa.setText("eins");
		erstSpa.setWidth(80);
		TableColumn zweitSpa = new TableColumn(table3,SWT.CENTER);
		zweitSpa.setText("zwei");
		zweitSpa.setWidth(80);
		TableColumn third = new TableColumn(table3,SWT.RIGHT);
		third.setText("drei");
		third.setWidth(60);

		String[] werte = new String[] {"Eins","Zwei","Drei"};
		TableItem Item1 = new TableItem(table3,SWT.NONE);
		Item1.setText(werte);
		TableItem Item2 = new TableItem(table3,SWT.NONE);
		Item2.setText(werte);
		TableItem Item3 = new TableItem(table3,SWT.NONE);
		Item3.setText(werte);
		TableItem Item4 = new TableItem(table3,SWT.NONE);
		Item4.setText(werte);

		table3.select(1);
	}
}
