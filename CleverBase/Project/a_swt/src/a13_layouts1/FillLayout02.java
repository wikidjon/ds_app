package a13_layouts1;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class FillLayout02 {
	Display display = new Display();
	Shell shell = new Shell(display);

	public static void main(String[] args) {
		new FillLayout02();
	}
	
	public FillLayout02() {
		FillLayout fillLayout = new FillLayout(SWT.VERTICAL);
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		
		shell.setLayout(fillLayout);

//		a_menue men = new a_menue();
//		men.menue(shell, display);
		
		Label l = new Label(shell, SWT.NONE);
		l.setText("Hallo");
		
		@SuppressWarnings("unused")
		Button button1 = new Button(shell, SWT.PUSH);

//		Button button2 = new Button(shell, SWT.PUSH);
		Text text = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text.setBounds(display.getClientArea ());
		text.setText("Hier sind Eingaben m�glich.");
		
		@SuppressWarnings("unused")
		Button button3 = new Button(shell, SWT.PUSH);
	
		shell.pack();
		shell.open();
	
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

}
