import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Button;
import swing2swt.layout.FlowLayout;
import org.eclipse.swt.widgets.Label;


public class swtdesignertest extends Shell {

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			swtdesignertest shell = new swtdesignertest(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public swtdesignertest(Display display) {
		super(display, SWT.SHELL_TRIM);
		setLayout(new FormLayout());
		
		Composite composite = new Composite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		composite.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		composite.setLayout(new FlowLayout(FlowLayout.CENTER, 55, 55));
		FormData fd_composite = new FormData();
		fd_composite.bottom = new FormAttachment(0, 262);
		fd_composite.right = new FormAttachment(0, 434);
		fd_composite.top = new FormAttachment(0, 10);
		fd_composite.left = new FormAttachment(0, 10);
		composite.setLayoutData(fd_composite);
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setText("New Label");
		
		Button btnNewButton_1 = new Button(composite, SWT.NONE);
		btnNewButton_1.setImage(SWTResourceManager.getImage(swtdesignertest.class, "/src/icons/ball.png"));
		btnNewButton_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		btnNewButton_1.setText("New Button");
		
		Button btnNewButton = new Button(composite, SWT.NONE);
		btnNewButton.setText("New Button");
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("SWT Application");
		setSize(450, 300);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
