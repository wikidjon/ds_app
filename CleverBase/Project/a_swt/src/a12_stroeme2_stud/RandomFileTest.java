package a12_stroeme2_stud;

import java.io.*;
import java.util.*;

//**********************************************************************************
/* Schreibt Datenarray (Student) in eine Binärdatei (Studenten.dat)         	   */ 
/* damit ist z.B. auch Positionierung möglich                                      */
/* anwendbar beim Schreiben von Datensätzen gleichen Typs                          */
//**********************************************************************************

public class RandomFileTest
{
   public static void main(String[] args)
   {
      Student[] studies= new Student[3];

      studies[0] = new Student("Paul Mueller", 1.3, 1993, 12, 15);
      studies[1] = new Student("Frieda Knopf", 2.0, 1995, 10, 1);
      studies[2] = new Student("Inge Meier", 2.3, 1992, 3, 15);
	//***********************************************************
	// Länge = 100 Byte - 2*40 Name + 8 Lohn (double) + 3* int (12)
	//***********************************************************

      try
      {
	   //****************************************************************************
         // Alle Mitarbeiterdatensätze in die Datei Angestellte.dat schreiben
	   //****************************************************************************
         DataOutputStream out = new DataOutputStream(new FileOutputStream("C:/Ausgang/Studenten.dat"));
         for (int i = 0; i < studies.length; i++)
            studies[i].writeData(out);				// Methode aus Angestellter
         out.close();

	   //****************************************************************************
         // Alle Datensätze in ein neues Array einlesen
         //****************************************************************************
	   RandomAccessFile in = new RandomAccessFile("Studenten.dat", "r"); 
         // Arraygroesse berechnen **************
         int n = (int)(in.length() / Student.RECORD_SIZE);
         Student[] angestArray= new Student[n];
         // Mitarbeiter in umgekehrter Reihenfolge lesen
         for (int i = n - 1; i >= 0; i--)
         {
            angestArray[i] = new Student();
            in.seek(i * Student.RECORD_SIZE);
            angestArray[i].readData(in);				// Methode aus Angestellter
         }
         in.close();

	   //****************************************************************************
         // eingelesene Mitarbeiterdatensätze ausgeben
         for (int i = 0; i < angestArray.length; i++)
            System.out.println("\n" + (i+1) + ". " + angestArray[i]);
      }
      catch(IOException e)
      {
         e.printStackTrace();
      }

   }
}

