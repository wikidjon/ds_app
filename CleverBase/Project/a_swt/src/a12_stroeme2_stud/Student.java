package a12_stroeme2_stud;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

//import a12_Stroeme1.a05_ObjectFileTest.DataIO;

class Student
{
   //**********************************************************************************
   public static final int NAME_SIZE = 40;
   public static final int RECORD_SIZE = 2 * NAME_SIZE + 8 + 4 + 4 + 4;

   private String name;
   private float prog;		//Note Programmieren
   private Date gebTag;
   public Student() {}
   //***********************************************************************************
   
   public Student(String n, double noteprog, int jahr, int monat, int day)
   {
      name = n;
      this.prog= (float) noteprog;
      GregorianCalendar calendar = new GregorianCalendar(jahr, monat - 1, day);
         // GregorianCalendar verwendet 0 für Januar
      gebTag = calendar.getTime();
   }

   public String getName()
   {      return name;		}

   public double getNoteProg()
   {      return prog;	 	}

   public Date getgebTag()
   {      return gebTag;	}

   public void Bienchen(double Prozent)
   {
      double zuschlag= prog* Prozent / 100;
      prog-= zuschlag;
   }

   public String toString()
   {
      return getClass().getName()+ " \n[Name: " + name + ",  \n\tNote Programmieren: " + prog  + ", \n\t GebTag: " + gebTag + "]";
   }

   /************************************************************************************
      Schreibt Mitarbeiterdaten in eine Datenausgabe
   *************************************************************************************/
   public void writeData(DataOutput out) throws IOException		//DataOutput ist Interface aus IO
   {
      DataIO.writeFString(name, NAME_SIZE, out);
      out.writeDouble(prog);						// Binärspeicherung

      GregorianCalendar calendar = new GregorianCalendar();
      calendar.setTime(gebTag);
      out.writeInt(calendar.get(Calendar.YEAR));			// Binärspeicherung
      out.writeInt(calendar.get(Calendar.MONTH) + 1);
      out.writeInt(calendar.get(Calendar.DAY_OF_MONTH));
   }

   /**************************************************************************************
      Liest Mitarbeiterdaten aus einer Dateneingabe
   ***************************************************************************************/
   public void readData(DataInput in) throws IOException
   {
      name = DataIO.readFString(NAME_SIZE, in);
      prog= (float) in.readDouble();							// Binärlesen
      int y = in.readInt();							// Binärlesen
      int m = in.readInt();
      int d = in.readInt();
      GregorianCalendar calendar = new GregorianCalendar(y, m - 1, d);
         // GregorianCalendar verwendet 0 für Januar
      gebTag = calendar.getTime();
   }

}

   //**************************************************************************************
   //**************************************************************************************
