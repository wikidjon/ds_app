package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolderAdapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a041_CTabFolder {

  private int einfuegeMerker = -1;
  private CTabFolder tabFolder;

  public void run() {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setText("Zeige CTabFolder");
    createContents(shell);
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }

  /**
   * Fensterinhalte
   */
  @SuppressWarnings("deprecation")
private void createContents(Shell shell) {
    shell.setLayout(new GridLayout(1, true));

    // Buttons fuer Tabs
    Composite composite = new Composite(shell, SWT.NONE);
    composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    composite.setLayout(new RowLayout());
    createButtons(composite);

    // Tabs
    tabFolder = new CTabFolder(shell, SWT.TOP);
    tabFolder.setBorderVisible(true);
    tabFolder.setLayoutData(new GridData(GridData.FILL_BOTH));
    Display display = shell.getDisplay();

    tabFolder.setSelectionBackground(new Color[] {
        display.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW),
        display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW),
        display.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW)}, new int[] { 50,
        100});
    // listener zum schliessen button jedes tabs
    tabFolder.addCTabFolderListener(new CTabFolderAdapter() {
      public void itemClosed(CTabFolderEvent event) {}
    });
  }

  /**
   * buttons zum einfuegen und bewegen ueber tabs
   */
  private void createButtons(Composite composite) {
    // bewegung links 
    Button button = new Button(composite, SWT.PUSH);
    button.setText("<<");
    button.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        if (einfuegeMerker > -1) {
          --einfuegeMerker;
          resetEinfuegeMerker();
        }
      }
    });

    // bewegung rechts
    button = new Button(composite, SWT.PUSH);
    button.setText(">>");
    button.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        if (einfuegeMerker < tabFolder.getItemCount() - 1) {
          ++einfuegeMerker;
          resetEinfuegeMerker();
        }
      }
    });

    // hinzufuegen
    button = new Button(composite, SWT.PUSH);
    button.setText("Add Tab");
    button.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        new CTabItem(tabFolder, SWT.NONE, einfuegeMerker + 1).setText("Tab ("
            + (einfuegeMerker + 1) + ")");
      }
    });
  }

 
   // einfuegen an beliebiger stelle
  private void resetEinfuegeMerker() {
    tabFolder.setInsertMark(einfuegeMerker, true);
//    setEinfuegeMerker(einfuegeMerker, true);

    // fehlerbehandlung
    if (einfuegeMerker == -1) {
      tabFolder.redraw();
    }
  }

  public static void main(String[] args) {
    new a041_CTabFolder().run();
  }
}

