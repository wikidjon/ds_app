package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ArmEvent;
import org.eclipse.swt.events.ArmListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class menue_create {

	menue_create (Display dis, Shell shell){

		final Image image1 = new Image(dis,
				menue_alles.class.getResourceAsStream("/icons/open.gif"));
		final Image image2 = new Image(dis,
				a04_Menue4.class.getResourceAsStream("/icons/save.gif"));


		Menu menu;
		MenuItem item;

		Menu menubar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menubar);

		SelectionListener l = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Widget-Auswahl: " + e.widget.getData());
			}
		};

		// ******************************************************
		// Menue Datei **************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setImage(image1);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.setImage(image2);
		item.addSelectionListener(l);

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		// ******************************************************
		// Menue Extras **************************
		menu = new Menu(menubar);
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);

		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Textmodus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		final MenuItem formatItem = item;

		// ******************************************************
		// Cascade Menue **************************
		menu = new Menu(menubar);
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Test");
		item.setMenu(menu);

		// Radio-Items zum TestMenue
		final MenuItem radioItem = new MenuItem(menu, SWT.RADIO);
		radioItem.setText("Test 1");

		item = new MenuItem(menu, SWT.RADIO);
		item.setText("Test 2");
		// final MenuItem schreibenItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Rechtschreibpruefung");
		// final MenuItem lesenItem = item;

		// Sub-Menue anfuegen mit Ctrl+C Auswahlm�glichkeit
		final MenuItem subactionLesenItem = new MenuItem(menu, SWT.PUSH);
		subactionLesenItem.setText("&Lesen\tCtrl+C");
		subactionLesenItem.setAccelerator(SWT.CTRL + 'C');
		subactionLesenItem.setEnabled(false);

		// Check-Item fuer enable/disable Items im Submenue
		final MenuItem enableItem = new MenuItem(menu, SWT.CHECK);
		enableItem.setText("&Sichern");

		// Listener f�r Anzeige Submenue
		menu.addMenuListener(new MenuListener() {
			@Override
			public void menuHidden(MenuEvent e) {
				System.out.println("SubMenue inaktiv");
			}

			@Override
			public void menuShown(MenuEvent e) {
				System.out.println("SubMenue |" + e.getSource() + " |geoeffnet");
			}
		});

		// Listener f�r enable/disable der SubAction
		enableItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Menue \"Enable SubAction\" auf "
						+ enableItem.getSelection() + " geaendert");
				subactionLesenItem.setEnabled(enableItem.getSelection());
			}
		});

		// Listener auf SubAction (SelectionListener)
		subactionLesenItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out
				.println("SubActionLesen ausgewaehlt! (SelectionListener) || "
						+ e.getSource());
			}
		});

		// listener auf SubAction
		subactionLesenItem.addArmListener(new ArmListener() {
			@Override
			public void widgetArmed(ArmEvent e) {
				System.out.println("SubActionLesen angefordert! (ArmListener)");
				// wenn Objekt erzeugt wird
			}
		});

		menu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		shell.setMenuBar(menubar);
		shell.open();

	}
}

