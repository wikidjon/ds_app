package a04_menue2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;


public class MenuShell {
	public static void main(final String[] args) {
		new MenuShell();
	}
	Display d;
	Shell s;
	MenuShell()
	{
		this.d = new Display();
		this.s = new Shell(this.d);
		this.s.setSize(500,500);
		this.s.setImage(new Image(this.d, "src/icons/cut.gif"));
		this.s.setText("A Shell Menu Example");

		final Menu m = new Menu(this.s,SWT.BAR );
		this.s.setMenuBar(m);

		this.s.open();
		while(!this.s.isDisposed()){
			if(!this.d.readAndDispatch()) {
				this.d.sleep();
			}
		}
		this.d.dispose();
	}
}
