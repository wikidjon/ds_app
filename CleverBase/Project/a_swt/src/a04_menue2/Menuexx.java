package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class Menuexx {

	static Display display;
	static Shell shell;
	static Text text;

	public static void main(String[] args) {

		Menu menuBar, fileMenu, editMenu;
		MenuItem fileMenuHeader, editMenuHeader;
		MenuItem fileExitItem, fileSaveItem, fileEnglishItem, fileGermanItem, editCopyItem;

		display = new Display();
		shell = new Shell(display);
		shell.setText("Menu Example");
		shell.setSize(300, 200);

		text = new Text(shell, SWT.BORDER);
		text.setBounds(80, 50, 150, 25);

		menuBar = new Menu(shell, SWT.BAR);
		fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuHeader.setText("&File");

		fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);

		fileSaveItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSaveItem.setText("&Speichern");

		fileEnglishItem = new MenuItem(fileMenu, SWT.RADIO);
		fileEnglishItem.setText("Englisch");

		fileGermanItem = new MenuItem(fileMenu, SWT.RADIO);
		fileGermanItem.setText("Deutsch");

		fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
		fileExitItem.setText("Beenden");

		editMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		editMenuHeader.setText("&Edit");

		editMenu = new Menu(shell, SWT.DROP_DOWN);
		editMenuHeader.setMenu(editMenu);

		editCopyItem = new MenuItem(editMenu, SWT.PUSH);
		editCopyItem.setText("&Copy");

		fileExitItem.addSelectionListener(new MenuItemListener());
		fileSaveItem.addSelectionListener(new MenuItemListener());
		editCopyItem.addSelectionListener(new MenuItemListener());

		fileEnglishItem.addSelectionListener(new RadioItemListener());
		fileGermanItem.addSelectionListener(new RadioItemListener());

		shell.setMenuBar(menuBar);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}

class MenuItemListener extends SelectionAdapter {

	private Object shell;

	@Override
	public void widgetSelected(SelectionEvent event) {
		if (((MenuItem) event.widget).getText().equals("E&xit")) {
			((Display) this.shell).close();
		}
		Menuexx.text.setText("Sei haben " + ((MenuItem) event.widget).getText() + "gew�hlt!");
	}
}
class RadioItemListener extends SelectionAdapter {


	@Override
	public void widgetSelected(SelectionEvent event) {
		MenuItem item = (MenuItem) event.widget;
		Menuexx.text.setText(item.getText() + " ist ausge�hlt.");
	}
}
