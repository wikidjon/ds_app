package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a04_Menue6 {

	static Display display;
	static Shell shell;
	static Text text;

	public static void main(String[] args) {

		Menu menuBar, fileMenu, editMenu;
		MenuItem fileMenuHeader, editMenuHeader;
		MenuItem fileExitItem, fileSpeichernItem, fileEnglischItem, fileDeutschItem, editCopyItem;

		display = new Display();
		shell = new Shell(display);
		shell.setText("Menuebeispiel");
		shell.setSize(300, 200);

		text = new Text(shell, SWT.BORDER);
		text.setBounds(80, 50, 200, 25);

		menuBar = new Menu(shell, SWT.BAR);
		fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuHeader.setText("&Datei");

		fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);

		fileSpeichernItem = new MenuItem(fileMenu, SWT.PUSH);
		fileSpeichernItem.setText("&Speichern");

		fileEnglischItem = new MenuItem(fileMenu, SWT.RADIO);
		fileEnglischItem.setText("Englisch");

		fileDeutschItem = new MenuItem(fileMenu, SWT.RADIO);
		fileDeutschItem.setText("Deutsch");

		fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
		fileExitItem.setText("Beenden");

		editMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		editMenuHeader.setText("Bearbeiten");

		editMenu = new Menu(shell, SWT.DROP_DOWN);
		editMenuHeader.setMenu(editMenu);

		editCopyItem = new MenuItem(editMenu, SWT.PUSH);
		editCopyItem.setText("&Copy");

		fileExitItem.addSelectionListener(new MenuItemListener1());
		fileSpeichernItem.addSelectionListener(new MenuItemListener1());
		editCopyItem.addSelectionListener(new MenuItemListener1());

		fileEnglischItem.addSelectionListener(new RadioItemListener1());
		fileDeutschItem.addSelectionListener(new RadioItemListener1());

		shell.setMenuBar(menuBar);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}

class MenuItemListener1 extends SelectionAdapter {

	private Object shell;

	@Override
	public void widgetSelected(SelectionEvent event) {
		if (((MenuItem) event.widget).getText().equals("Exit")) {
			((Display) this.shell).close();
		}
		a04_Menue6.text.setText("Sei haben " + ((MenuItem) event.widget).getText() + " gew�hlt!");
	}
}


class RadioItemListener1 extends SelectionAdapter {

	@Override
	public void widgetSelected(SelectionEvent event) {
		MenuItem item = (MenuItem) event.widget;
		a04_Menue6.text.setText("RI " + item.getText() + " ist ausge�hlt.");
	}
}
