package a04_menue2;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

// Menues mit aktiven/inaktiven Items
// Armlistener an Subaction - wenn Objekt erzeugt wird
// Achtung: alle heissen "item" !!!!!!!!!!!!!!!
public class menue_alles {

	public static void main(String[] arguments) {

		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setSize(300, 200);
		shell.setText("Menue Cascade Demo 2");

		menue_create mc =new menue_create(display, shell);

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}