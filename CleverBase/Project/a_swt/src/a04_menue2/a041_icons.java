package a04_menue2;

// programm icon

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a041_icons {

	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);
		final Label label = new Label (shell, SWT.NONE);
		label.setText ("kann icon .bmp nicht finden");
		Image image = null;
		//		final Image image =	new Image(display, "src/icons/icon.gif");
		final Program p = Program.findProgram (".bmp");
		if (p != null) {
			final ImageData data = p.getImageData ();
			if (data != null) {
				image = new Image (display, data);
				label.setImage (image);
			}
		}
		label.pack ();
		shell.pack ();
		shell.open ();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		if (image != null) {
			image.dispose ();
		}
		display.dispose ();
	}

}
