package a04_menue2;
// statuszeile bei Mausbewegung ueberToolItem

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class a04_statusline2 {

	static String statusText = "";
	public static void main(String[] args) {
		final Display display = new Display();
		Shell shell = new Shell(display);
		shell.setBounds(10, 10, 300, 200);

		final ToolBar bar = new ToolBar(shell, SWT.BORDER);
		bar.setBounds(10, 10, 250, 50);

		final Label statusLine = new Label(shell, SWT.BORDER);
		statusLine.setBounds(10, 90, 250, 30);

		new ToolItem(bar, SWT.NONE).setText("eintrag 1");
		new ToolItem(bar, SWT.NONE).setText("eintrag 2");
		new ToolItem(bar, SWT.NONE).setText("eintrag 3");

		bar.addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				ToolItem item = bar.getItem(new Point(e.x, e.y));
				//int item = bar.getItemCount();
				String name = "";
				if (item != null) {
					name = item.getText();
				}
				if (!statusText.equals(name)) {
					statusLine.setText(name);
					statusText = name;				//Bedeutung ????
				}
			}
		});
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}
