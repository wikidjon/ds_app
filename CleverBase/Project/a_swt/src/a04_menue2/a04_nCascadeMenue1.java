package a04_menue2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ArmEvent;
import org.eclipse.swt.events.ArmListener;
import org.eclipse.swt.events.HelpEvent;
import org.eclipse.swt.events.HelpListener;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class a04_nCascadeMenue1 {

	public static Display display;

	public static void main(String[] args) {
		display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Cascadenmenue Demo");

		//menubar ***************************************
		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);

		MenuItem file = new MenuItem(menu, SWT.CASCADE);  	// MenuItem an MenuBar
		file.setText("Datei");

		//Menue f�r Datei
		Menu filemenu = new Menu(shell, SWT.DROP_DOWN);
		file.setMenu(filemenu);								// !!!!!! Menue an MenueItem h�ngen

		//MenuItems zu Datei *************************************
		MenuItem actionItem = new MenuItem(filemenu, SWT.PUSH);
		actionItem.setText("Test");

		//listener zu Test anfuegen
		actionItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Test ausgewaehlt!");
			}
		});

		new MenuItem(filemenu, SWT.SEPARATOR);

		//Radio-Item zum DateiMenue
		final MenuItem radioItem = new MenuItem(filemenu, SWT.RADIO);
		radioItem.setText("Radio");
		final MenuItem radioItem2 = new MenuItem(filemenu, SWT.RADIO);
		radioItem2.setText("Radio2");

		//Check-Item zum DateiMenue
		final MenuItem checkItem = new MenuItem(filemenu, SWT.CHECK);
		checkItem.setText("Check");

		//Listener an Radio-Item
		radioItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Radio-Item ausgewaehlt:"+ radioItem.getSelection());
			}
		});

		//Listener Check-Item
		checkItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Check-Item ausgewaehlt:" + checkItem.getSelection());
			}
		});

		//Cascaden-Item anfuegen
		MenuItem cascadeItem = new MenuItem(filemenu, SWT.CASCADE);
		cascadeItem.setText("Cascade");

		Menu submenu = new Menu(shell, SWT.DROP_DOWN);    // aktiv / inaktiv -> Listener
		cascadeItem.setMenu(submenu);

		//Sub-Menue anfuegen mit Ctrl+C Auswahlm�glichkeit
		final MenuItem subactionItem = new MenuItem(submenu, SWT.PUSH);
		subactionItem.setText("&Cascade1\tCtrl+C");
		subactionItem.setAccelerator(SWT.CTRL+'C');
		subactionItem.setEnabled(false);

		//Check-Item fuer enable/disable Items im Submenue
		final MenuItem enableItem = new MenuItem(submenu, SWT.CHECK);
		enableItem.setText("Enable Cascade1");

		//Listener f�r Anzeige Submenue
		submenu.addMenuListener(new MenuListener() {
			@Override
			public void menuHidden(MenuEvent e) {
				System.out.println("SubMenue inaktiv");
			}
			@Override
			public void menuShown(MenuEvent e) {
				System.out.println("SubMenue geoeffnet");
			}
		});

		//Listener f�r enable/disable der SubAction
		enableItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Menue \"Enable SubAction\" auf " + enableItem.getSelection()+ " geaendert");
				subactionItem.setEnabled(enableItem.getSelection());
				//isEnabled());
			}
		});

		//Listener auf SubAction (SelectionListener)
		subactionItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("SubAction ausgewaehlt!");
			}
		});

		//listener auf SubAction
		subactionItem.addArmListener(new ArmListener() {
			@Override
			public void widgetArmed(ArmEvent e) {
				System.out.println("SubAction angefordert!");
			}
		});

		//Listener auf ein Helpevent (F1)
		subactionItem.addHelpListener(new HelpListener() {
			@Override
			public void helpRequested(HelpEvent e) {
				System.out.println("Hilfe angefordert / SubAction");
			}
		});

		shell.setSize(300,300);
		shell.open();
		while(!shell.isDisposed())
			if(!display.readAndDispatch()) {
				display.sleep();
			}
	}
}
