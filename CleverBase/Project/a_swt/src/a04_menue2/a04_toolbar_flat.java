package a04_menue2;

// tool bar mit grafiken

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class a04_toolbar_flat {

	public static void main (String [] args) {
		Display display = new Display();
		final Image image =	new Image(
				display, a04_toolbar_flat.class.getResourceAsStream("/images/open.gif"));

		Shell shell = new Shell (display);
		final ToolBar toolBar = new ToolBar (shell, SWT.FLAT | SWT.NONE);
		for (int i=0; i<12; i++) {
			final ToolItem item = new ToolItem (toolBar, SWT.DROP_DOWN);
			item.setImage (image);
			item.addListener (SWT.Selection, new Listener () {
				@Override
				public void handleEvent (Event event) {
					System.out.println("...hier muss was passieren ..:!!!");
				}
			});
		}
		toolBar.pack ();
		shell.open ();
		shell.pack();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		image.dispose ();
		display.dispose ();
	}

}
