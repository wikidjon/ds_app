package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class a041_coolbar {

	static int zaehler;

	static CoolItem createItem(CoolBar coolBar, int count, Image image) {
		ToolBar toolBar = new ToolBar(coolBar, SWT.FLAT);
		for (int i = 0; i < count; i++) {
			ToolItem item = new ToolItem(toolBar, SWT.PUSH);
			item.setText(zaehler++ +"");
			item.setImage(image);

		}
		toolBar.pack();
		Point size = toolBar.getSize();
		CoolItem item = new CoolItem(coolBar, SWT.NONE);
		item.setControl(toolBar);
		Point preferred = item.computeSize(size.x, size.y);
		item.setPreferredSize(preferred);
		return item;
	}

	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		Image bspIcon = new Image(display, "src/icons/folder.gif");

		CoolBar coolBar = new CoolBar(shell, SWT.NONE);
		createItem(coolBar, 3, bspIcon);
		createItem(coolBar, 1, bspIcon);
		createItem(coolBar, 3, bspIcon);
		createItem(coolBar, 4, bspIcon);
		int style = SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL;
		Text text = new Text(shell, style);
		FormLayout layout = new FormLayout();
		shell.setLayout(layout);
		FormData coolData = new FormData();
		coolData.left = new FormAttachment(0);
		coolData.right = new FormAttachment(100);
		coolData.top = new FormAttachment(0);
		coolBar.setLayoutData(coolData);
		coolBar.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				//	shell.layout();
			}
		});
		FormData textData = new FormData();
		textData.left = new FormAttachment(0);
		textData.right = new FormAttachment(100);
		textData.top = new FormAttachment(coolBar);
		textData.bottom = new FormAttachment(100);
		text.setLayoutData(textData);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}
