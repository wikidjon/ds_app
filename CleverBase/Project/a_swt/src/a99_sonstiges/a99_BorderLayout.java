package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Shell;

public final class a99_BorderLayout extends Layout {

	public static final String NORDEN = "NORDEN";
	public static final String SUEDEN = "SUEDEN";
	public static final String OSTEN = "OSTEN";
	public static final String WESTEN = "WESTEN";
	public static final String ZENTRUM = "ZENTRUM";

	private static final String[] position =
		{ NORDEN, SUEDEN, OSTEN, WESTEN, ZENTRUM };

	public int leer = 3;

	
	protected Point computeSize(
		Composite composite, int wHint, int hHint, boolean flushCache) {

		Control[] children = getChildren(composite);
		Point[] sizes = getSizes(children, flushCache);
		int xleer = 0;
		int yleer = 0;
		if (children[0] != null) {yleer += leer;}
		if (children[1] != null) {yleer += leer;}
		if (children[2] != null) {xleer += leer;}
		if (children[3] != null) {xleer += leer;}
		if (children[4] == null) {xleer -= leer;}
		return new Point(
			max(sizes[0].x,	sizes[1].x,
				sizes[2].x + sizes[3].x + sizes[4].x + xleer),
				sizes[0].y + sizes[1].y + yleer + max(sizes[2].y, sizes[3].y, sizes[4].y));
	}

	protected void layout(Composite composite, boolean flushCache) {
		Control[] children = getChildren(composite);
		Point[] sizes = getSizes(children, flushCache);
		Rectangle area = composite.getClientArea();
		int top = 0, bottom = 0, left = 0, right = 0;
		if (children[0] != null) {
			top = sizes[0].y;
			children[0].setBounds(0, 0, area.width, top);
			top += leer;
		}
		if (children[1] != null) {
			bottom = sizes[1].y;
			children[1].setBounds(0, area.height - bottom, area.width, bottom);
			bottom += leer;
		}
		if (children[2] != null) {
			right = sizes[2].x;
			children[2].setBounds(
				area.width - right,
				top,
				right,
				area.height - top - bottom);
			right += leer;
		}
		if (children[3] != null) {
			left = sizes[3].x;
			children[3].setBounds(0, top, left, area.height - top - bottom);
			left += leer;
		}
		if (children[4] != null) {
			children[4].setBounds(
				left,
				top,
				area.width - left - right,
				area.height - top - bottom);
		}
	}

	private Control[] getChildren(Composite composite) {
		Control[] allChildren = composite.getChildren();
		Control[] sortedChildren = new Control[5];
		for (int i = 0; i < allChildren.length; i++) {
			Control c = allChildren[i];
			Object data = c.getLayoutData();
			boolean found = false;
			for (int j = 0; j < position.length; j++) {
				if (position[j].equals(data)) {
					sortedChildren[j] = c;
					found = true;
					break;
				}
			}
			if (!found) {
				sortedChildren[4] = c;
			}
		}
		return sortedChildren;
	}

	private Point[] getSizes(Control[] children, boolean flushCache) {
		Point[] sizes = new Point[children.length];
		for (int i = 0; i < children.length; i++) {
			sizes[i] =
				children[i] != null
					? children[i].computeSize(SWT.DEFAULT,SWT.DEFAULT,flushCache)
					: new Point(0, 0);
		}
		return sizes;
	}

	private int max(int a, int b, int c) {
		return Math.max(a, Math.max(b, c));
	}

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell();
		shell.setText("BorderLayout Demo");
		shell.setLayout(new a99_BorderLayout());

		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Control c = (Control) e.getSource();
				Composite p = c.getParent();
				c.dispose();
				p.layout();
			}
		};

		Button button;
		button = new Button(shell, SWT.PUSH);
		button.setText(a99_BorderLayout.NORDEN);
		button.setLayoutData(a99_BorderLayout.NORDEN);
		button.addSelectionListener(l);
		button = new Button(shell, SWT.PUSH);
		button.setText(a99_BorderLayout.SUEDEN);
		button.setLayoutData(a99_BorderLayout.SUEDEN);
		button.addSelectionListener(l);
		button = new Button(shell, SWT.PUSH);
		button.setText(a99_BorderLayout.OSTEN);
		button.setLayoutData(a99_BorderLayout.OSTEN);
		button.addSelectionListener(l);
		button = new Button(shell, SWT.PUSH);
		button.setText(a99_BorderLayout.WESTEN);
		button.setLayoutData(a99_BorderLayout.WESTEN);
		button.addSelectionListener(l);
		button = new Button(shell, SWT.PUSH);
		button.setText(a99_BorderLayout.ZENTRUM);
		button.setLayoutData(a99_BorderLayout.ZENTRUM);
		button.addSelectionListener(l);

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}