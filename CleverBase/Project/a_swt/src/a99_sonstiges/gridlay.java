package a99_sonstiges;
import java.util.ResourceBundle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class gridlay {

	public gridlay(Display d, Shell s) {

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 4;
		s.setLayout(gridLayout);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.GRAB_HORIZONTAL;
		gridData.grabExcessHorizontalSpace = true;

		final Image image = new Image(d, gridlay.class
				.getResourceAsStream("/icons/icon.gif"));

		// Beschriftungen aus daten.properties
		// enhaelt locale spezifische Objekte
		final ResourceBundle datenRes = ResourceBundle.getBundle("daten");  //daten.properties
		Label label[] = new Label[10];
		final Text text[] = new Text[20];
		String name[] = { datenRes.getString("Nachname"),
				datenRes.getString("Vorname"), datenRes.getString("Strasse"),
				datenRes.getString("Wohnort"), datenRes.getString("Bundesland") };

		// "Tabellen" Kopf
		Label labela = new Label(s, SWT.None);
		labela.setText("Ihre Daten:");
		Label labelb = new Label(s, SWT.None);
		labelb.setImage(image);
		@SuppressWarnings("unused")
		Label labelc = new Label(s, SWT.None); // notwendig, da 4 Spalten
		Label labeld = new Label(s, SWT.None);
		labeld.setImage(image);

		// Aufbau der Eingabefelder
		for (int i = 0; i < 5; i++) {
			label[i] = new Label(s, SWT.None);
			label[i].setText(name[i]);
			// label[i].setLayoutData(gridData);
			label[i].setBackground(d.getSystemColor(SWT.COLOR_GRAY));

			text[i] = new Text(s, SWT.BORDER);
			text[i].setLayoutData(gridData);
			text[i].addKeyListener(new KeyListener() {
				public void keyPressed(KeyEvent e) {
					auswerten(e);
				}

				public void keyReleased(KeyEvent e) {
					if (e.stateMask == SWT.CTRL && e.keyCode != SWT.CTRL)
						System.out.println("KeyRealeased");
				}
			});
		}
		
		@SuppressWarnings("unused")
		Label labele = new Label(s, SWT.None); // notwendig, da 4 Spalten
	//	Label labelf = new Label(s, SWT.None); // notwendig, da 4 Spalten
///********************************************************************		
		Button button = new Button(s, SWT.PUSH);
		button.setText("Speichern");
		// button.setImage(image);
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				for (int i = 0; i < 5; i++) {
					System.out.println(text[i].getText());
				}

				message(e);
			}
		});
//*****************************************************************************/
		s.pack();
		s.open();
	}

	private static void message(SelectionEvent e) {
		Shell shell = ((Button) e.widget).getShell();
		MessageBox mb = new MessageBox(shell);
		mb.setMessage("Danke!");
		mb.open();
	}

	private static void auswerten(KeyEvent e) {
		String string = "";
		switch (e.character) {
		case 0:			string += " '\\0'";
						break;
		case SWT.BS:	string += " '\\b'";
						break;
		case SWT.CR:	string += " '\\r'";
						break;
		case SWT.DEL:	string += " DEL";
						break;
		case SWT.ESC:	string += " ESC";
						break;
		case SWT.LF:	string += " '\\n'";
						break;
		default:		string += " '" + e.character + "'";
						break;
		}
		// System.out.println (string);
	}

}
