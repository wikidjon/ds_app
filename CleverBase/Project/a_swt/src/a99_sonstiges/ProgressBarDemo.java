package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class ProgressBarDemo {

	public static Display myDisplay;
	public static boolean internalCall = false;

	public static void main(String[] args) {
		internalCall = true;
		myDisplay = new Display();
		ProgressBarDemo pbd = new ProgressBarDemo();
		pbd.runDemo(myDisplay);
	}

	public void runDemo(Display display) {
		myDisplay = display;
		Shell shell = new Shell(display);
		shell.setSize(300,300);
		shell.setText("Progress Bar Demo");

		ProgressBar progressBar1 = new ProgressBar(shell, SWT.HORIZONTAL);
		progressBar1.setMinimum(0);
		progressBar1.setMaximum(100);
		progressBar1.setSelection(30);
		progressBar1.setBounds(10,10,250,20);

		shell.open();

		while(!shell.isDisposed()){
		if(!display.readAndDispatch())
			display.sleep();
		}
		if (internalCall) display.dispose();
	}
}
