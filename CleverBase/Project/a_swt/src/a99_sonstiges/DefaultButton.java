package a99_sonstiges;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 *  
 */
public class DefaultButton {

	public DefaultButton() {
		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setLayout(new RowLayout());

		final String[] ratings =
			new String[] { "test!", "Super", "So-so", "mist" };
		final Button[] radios = new Button[ratings.length];
		for (int i = 0; i < ratings.length; i++) {
			radios[i] = new Button(shell, SWT.RADIO);
			radios[i].setText(ratings[i]);
		}

		Button cancelButton = new Button(shell, SWT.PUSH);
		cancelButton.setText("Canel");

		Button rateButton = new Button(shell, SWT.PUSH);
		rateButton.setText("Abstimmung!");
		rateButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				for (int i = 0; i < radios.length; i++)
					if (radios[i].getSelection())
						System.out.println("Rating: " + ratings[i]);

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				System.out.println("Default selection");
			}
		});

		shell.setDefaultButton(rateButton);
		System.out.println(shell.getDefaultButton());

		shell.pack();
		shell.open();
		//textUser.forceFocus();

		// Set up the event loop.
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

		display.dispose();
	}

	public static void main(String[] args) {
		new DefaultButton();
	}
}
