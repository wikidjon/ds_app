package a99_sonstiges;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

// Strukturierungsansatz f�r das Programm gridlay
// Startklasse

public class gridlay_start {

	public static void main(String[] args) {

		// Fenster holen
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(250, 300);
		shell.open();

		// Verarbeitung
	//	final gridlay a = 
			new gridlay(display, shell);
	//	a.gridlay(display, shell);

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
