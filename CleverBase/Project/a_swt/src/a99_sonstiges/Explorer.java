package a99_sonstiges;
import java.io.File;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;
/**
 * The example is based on: "Using the Eclipse GUI outside the Eclipse
 * Workbench, Part 1: Using JFace and SWT in stand-alone mode" from Adrian Van
 * Emmenis.
 * 
 * @see http://www-106.ibm.com/developerworks/java/library/os-ecgui1/index.html?dwzone=java
 */
public class Explorer extends ApplicationWindow {

	public Explorer() {
		super(null);
	}

	protected Control createContents(final Composite parent) {
		getShell().setText("SWT-Explorer");
		SashForm sash_form = new SashForm(parent, SWT.HORIZONTAL | SWT.NULL);

		TreeViewer tv = new TreeViewer(sash_form);
		tv.setContentProvider(new FileTreeContentProvider());
//		tv.setLabelProvider(new FileTreeLabelProvider());
		tv.setInput(new File("C:\\"));

		final TableViewer tbv = new TableViewer(sash_form, SWT.BORDER);
		tbv.setContentProvider(new FileTableContentProvider());
		tbv.setLabelProvider(new FileTableLabelProvider());

		TableColumn column = new TableColumn(tbv.getTable(), SWT.LEFT);
		column.setText("Name");
		column.setWidth(200);
		tbv.getTable().setHeaderVisible(true);
		tv.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event
						.getSelection();

				Object selected_file = selection.getFirstElement();
				tbv.setInput(selected_file);
			}
		});
		return sash_form;
	}

	public static void main(String[] args) {
		Explorer w = new Explorer();
		w.setBlockOnOpen(true);
		w.open();
		Display.getCurrent().dispose();
	}
}
