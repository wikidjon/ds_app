package a02b_events_listener2;
import java.util.Random;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a49_MouseMoveListener {

	public static void main(final String[] args) {

		final Display d = new Display();
		final Shell s = new Shell(d);
		s.setSize(300, 150);
		s.open();

		s.setSize(250,200);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("MouseListener Beispiel");

		final Button b = new Button(s, SWT.PUSH);
		b.setText("Drueck mich");
		b.setBounds(20,50, 90, 25);
		s.open();
		s.pack();
		b.addMouseMoveListener(new MouseMoveListener()
		{
			@Override
			public void mouseMove(final MouseEvent e) {
				final Random r = new Random(System.currentTimeMillis());
				final Point p = s.getSize();
				final int newX = r.nextInt(p.y);
				final int newY = r.nextInt(p.x);
				b.setBounds(newX+25, newY+15, 90, 25);
			}
		});
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}

}
