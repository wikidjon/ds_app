package a02b_events_listener2;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

// Strukturierungsansatz f�r Programme
// Startklasse

public class a50_start {

	public static void main(String[] args) {

		// Fenster holen
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(250,300);
		shell.open();

		// Verarbeitung
		a50_verarbeitung a = new a50_verarbeitung();
		a.setzeShell(shell);

		// Schliessen
		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}