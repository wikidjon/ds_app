package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

/** Men�s mit Maus- und Tastenevents... */

public class a42_menue_events{

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300, 200);

		Menu menubar = new Menu(shell, SWT.BAR);

		Menu menu;
		MenuItem item;

		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
				// Behandlung der Tastaturevents
				menuSelected(e);
			}
		};

		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...\t o");
		item.setData("oeffnen");
		// Tastaturauswahl anbinden - Auswahl �ber o
		item.setAccelerator('o');
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		// Tastaturauswahl anbinden - Auswahl �ber CTRL S
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);
		
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setAccelerator(SWT.CTRL + 'D');
		item.addSelectionListener(l);
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);

		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		
		shell.setMenuBar(menubar);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	static private void menuSelected(SelectionEvent e) {
		Object obj = ((MenuItem) e.getSource()).getData();
		if (obj == "Datei") {
			System.out.println("Datei ueber 'D'");
		} else if (obj == "oeffnen") {
			System.out.println("oeffnen ueber 'o'");
		} else if (obj == "speichern") {
			System.out.println("speichern ueber CTRL-S");
		}
	}
}