package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/***************************************
 * Buttons, Events und der Focus...
 *****************************************/
public class a44_focus_listner {
	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(320, 200);

		Button b1 = new Button(shell, SWT.PUSH);
		Button b2 = new Button(shell, SWT.PUSH);
		Button b3 = new Button(shell, SWT.PUSH);
		Button b4 = new Button(shell, SWT.PUSH);
		Button b5 = new Button(shell, SWT.PUSH);
		Button b6 = new Button(shell, SWT.PUSH);

		b1.setBounds(10,10,50,50);
		b2.setBounds(100,10,50,50);
		b3.setBounds(200,10,50,50);
		b4.setBounds(10,100,50,50);
		b5.setBounds(100,100,50,50);
		b6.setBounds(200,100,50,50);

		b1.setText("1");
		b2.setText("2");
		b3.setText("3");
		b4.setText("4");
		b5.setText("5");
		b6.setText("6");

		FocusListener focusListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				System.out.println(e.widget + " hat den Focus");
			}
			@Override
			public void focusLost(FocusEvent e) {
				System.out.println("... und wieder verloren!");
			}
		};
		// Traversieren mit Tastatur
		TraverseListener traverseListener = new TraverseListener() {
			@Override
			public void keyTraversed(TraverseEvent e) {
				if (e.detail == SWT.TRAVERSE_TAB_PREVIOUS) {
					System.out.println("Es gibt kein zurueck!");
					e.doit = false;
				}
				System.out.println(e.widget + " wurde traversiert");
			}
		};

		b1.addFocusListener(focusListener);
		b4.addTraverseListener(traverseListener);
		b4.addFocusListener(focusListener);
		b6.addTraverseListener(traverseListener);

		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
