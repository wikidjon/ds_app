package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a61_mausTrack {
	//static int zaehler1=0;
	public static void main(String[] arguments) {
		final Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300, 150);
		shell.open();

		final int zaehler2=0;
		final Canvas canvas = new Canvas(shell, SWT.BORDER);
		canvas.setBounds(10,10,100,100);
		Canvas canvas2 = new Canvas(shell, SWT.BORDER);
		canvas2.setBounds(150,150,300,100);
		final Text text = new Text(canvas2, SWT.READ_ONLY);
		text.setBounds(10,30,300,20);

		MouseMoveListener mouseMove = new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				int zaehler1 = 0;
				Color color = canvas.getBackground();
				color.dispose();
				//zaehler aendern - global !!!
				canvas.setBackground(new Color(display,zaehler1,0,255));
				zaehler1++;
				System.out.println(zaehler1 + " - mouseMove");
			}
		};
		canvas.addMouseMoveListener(mouseMove);
		MouseTrackListener mouseTrack = new MouseTrackListener() {
			@Override
			public void mouseEnter(MouseEvent arg0) {
				int zaehler2=1;
				text.setText(Integer.toString(zaehler2)+ "- mouseTrack");
			}
			@Override
			public void mouseExit(MouseEvent arg0) {
				int zaehler2;
				text.setText("Canvas2 - mouseExit");
				System.out.println("mouseExit");
				zaehler2 = 0;
				System.out.println(zaehler2);
			}
			@Override
			public void mouseHover(MouseEvent arg0) {
				int zaehler2=0;
				zaehler2++;
				System.out.println("mouseHover");
				text.setText(Integer.toString(zaehler2));
			}
		};
		canvas2.addMouseTrackListener(mouseTrack);

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
		canvas.dispose();
		canvas2.dispose();

	}}