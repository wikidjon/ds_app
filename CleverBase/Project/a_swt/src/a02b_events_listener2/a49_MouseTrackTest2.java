package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a49_MouseTrackTest2 {

	public static void main(final String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		final Label label = new Label(shell, SWT.SHADOW_IN | SWT.CENTER);

		shell.setLayout(new GridLayout());

		final MouseEnterExitListener listener = new MouseEnterExitListener();

		label.setText("Positionieren Sie Ihren Cursor hier ...");
		label.setBounds(30, 30, 200, 30);

		label.addMouseTrackListener(listener);

		shell.setSize(260, 120);
		shell.open();

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

}

class MouseEnterExitListener extends MouseTrackAdapter {
	@Override
	public void mouseEnter(final MouseEvent e) {
		System.out.println("Cursor im Label");
	}

	@Override
	public void mouseExit(final MouseEvent arg0) {
		System.out.println("Cursor aus label");
	}
}