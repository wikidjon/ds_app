package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a49_MouseTrackTest1 {

	public static void main(final String[] args) {
		{
			final Display d = new Display();;
			final Shell  s = new Shell(d);

			s.setSize(250,200);
			s.setImage(new Image(d, "src/icons/cut.gif"));
			s.setText("MouseTrackListener Beispiel");
			final Button b = new Button(s, SWT.PUSH);
			b.setText("Drueck mich");
			b.setBounds(20,50, 100, 25);
			s.open();
			final Color oldColor = b.getBackground();

			b.addMouseTrackListener(new MouseTrackAdapter() {
				@Override
				public void mouseEnter(final MouseEvent e)
				{
					b.setBackground(new Color(d,255,153,153));
					//s.setBackground(new Color(d,255,153,153));
				}

				@Override
				public void mouseExit(final MouseEvent e)
				{
					b.setBackground(oldColor);
					s.setBackground(oldColor);
				}
			});

			while(!s.isDisposed()){
				if(!d.readAndDispatch()) {
					d.sleep();
				}
			}
			d.dispose();
		}
	}
}
