package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a55border {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);
		//a55border bLayout = new a55border ();
		//shell.setLayout(new a55border());
		//shell.setLayout(new a99_BorderLayout());
		Button button0 = new Button (shell, SWT.PUSH);
		button0.setText ("button0");

		Button button1 = new Button (shell, SWT.PUSH);
		button1.setText ("button1");

		Button button2 = new Button (shell, SWT.PUSH);
		button2.setText ("button2");

		Button button3 = new Button (shell, SWT.PUSH);
		button3.setText ("button3");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}