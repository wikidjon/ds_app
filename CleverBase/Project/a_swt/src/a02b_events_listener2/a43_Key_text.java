package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

//Tastaturereignisse auf Textfelder

public class a43_Key_text {

	public static void main(final String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setSize(250,300);
		shell.open();

		final Text text1 = new Text(shell, SWT.BORDER);
		text1.setText("Bitte geben Sie Ihre Daten ein!");
		text1.setBounds(10,10,200,20);
		text1.setTextLimit(30);

		text1.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				String string = "";
				switch (e.character) {
				case 0: string += " '\\0'"; break;
				case SWT.BS: string += " '\\b'"; break;
				case SWT.CR: string += " '\\r'"; break;
				case SWT.DEL: string += " DEL"; break;
				case SWT.ESC: string += " ESC"; break;
				case SWT.LF: string += " '\\n'"; break;
				default: string += " '" + e.character +"'";
				break;	}
				System.out.println ("gedr�ckt: " + string);
				System.out.println ("e.stateMask: " + e.stateMask + string);
			}
			@Override
			public void keyReleased(final KeyEvent e) {
				if (e.stateMask == SWT.CTRL && e.keyCode != SWT.CTRL) {
					System.out.println("Taste losgelassen");
				}} });

		final Text text2 = new Text(shell, SWT.NONE);
		text2.setEchoChar('*');
		text2.setBounds(10,50,200,20);
		text2.setText("Password");


		final Text text3 = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text3.setBounds(10,90,200,100);
		text3.setEditable(false);
		text3.setText("Hier ist keine Eingabe m�glich.");

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
		text1.dispose();
		text2.dispose();
		text3.dispose();
	}
}