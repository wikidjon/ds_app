package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class a81_FocusTraversal {

	public static void main(String[] args) {
		final Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new RowLayout());

		FocusListener focusListener = new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				System.out.println(e.widget + " hat den Focus");
			}
			@Override
			public void focusLost(FocusEvent e) {
				System.out.println("... und wieder verloren!");
			}
		};

		Composite composite1 = new Composite(shell, SWT.BORDER);
		composite1.setLayout(new RowLayout());
		composite1.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		Button button1 = new Button(composite1, SWT.PUSH);
		button1.setText("Button1");
		button1.addFocusListener(focusListener);

		List list = new List(composite1, SWT.MULTI | SWT.BORDER);
		list.setItems(new String[] { "Eintrag 1", "Eintrag 2", "Eintrag 3" });
		list.addFocusListener(focusListener);

		Button radioButton1 = new Button(composite1, SWT.RADIO);
		radioButton1.setText("Radio 1");
		Button radioButton2 = new Button(composite1, SWT.RADIO);
		radioButton2.setText("Radio 2");
		radioButton2.addFocusListener(focusListener);

		Composite composite2 = new Composite(shell, SWT.BORDER);
		composite2.setLayout(new RowLayout());
		composite2.setBackground(display.getSystemColor(SWT.COLOR_GREEN));

		Button button2 = new Button(composite2, SWT.PUSH);
		button2.setText("Button 2");
		button2.addFocusListener(focusListener);

		final Canvas canvas = new Canvas(composite2, SWT.NULL);
		canvas.setSize(50, 50);
		canvas.setBackground(display.getSystemColor(SWT.COLOR_YELLOW));
		canvas.addFocusListener(focusListener);

		Combo combo = new Combo(composite2, SWT.DROP_DOWN);
		combo.add("combo");
		combo.select(0);

		canvas.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				GC gc = new GC(canvas);
				Rectangle rect = canvas.getClientArea();
				gc.fillRectangle(rect.x, rect.y, rect.width, rect.height);

				Font font = new Font(display, "Arial", 32, SWT.BOLD);
				gc.setFont(font);
				gc.drawString("" + e.character, 15, 10);

				gc.dispose();
				font.dispose();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println("Taste losgelassen...");
			}
		});

		canvas.addTraverseListener(new TraverseListener() {
			@Override
			public void keyTraversed(TraverseEvent e) {
				if (e.detail == SWT.TRAVERSE_TAB_NEXT
						|| e.detail == SWT.TRAVERSE_TAB_PREVIOUS) {
					e.doit = true;
				}
			}
		});

		composite1.setTabList(new Control[] { button1, list });
		composite2.setTabList(new Control[] { button2, canvas, combo });

		shell.setTabList(new Control[] { composite2, composite1 });
		//Setzt die Tabreihenfolge der Controls in der Reihenfolge ihres Auftretens in der Argumentliste
		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}
