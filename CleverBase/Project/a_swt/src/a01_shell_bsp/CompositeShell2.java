package a01_shell_bsp;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class CompositeShell2 {
	public static void main(String[] args) {

		Display d = new Display();
		Shell s = new Shell(d);

		GridLayout gl = new GridLayout();
		gl.numColumns=3;
		s.setLayout(gl);
		s.setSize(250,275);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Shell Composite Beispiel 2");

		GridComposite gc = new GridComposite(s);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 3;
		gc.setLayoutData(gd);
		gd = new GridData();

		// auskommentieren !!!
		Composite c1 = new Composite(s, SWT.NO_FOCUS);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		c1.setLayoutData(gd);
		c1.setBackground(new Color(d,31,133,31));
		Composite c2 = new Composite(s, SWT.NO_FOCUS);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		c2.setLayoutData(gd);

		Composite c = new Composite(s, SWT.NO_FOCUS);
		c.setLayout(new RowLayout());
		Button b1 = new Button(c, SWT.PUSH );
		b1.setText("OK");
		Button b2 = new Button(c, SWT.PUSH );
		b2.setText("Cancel");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		c.setLayoutData(gd);
		c.setBackground(new Color(d,111,133,31));

		s.open();
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
