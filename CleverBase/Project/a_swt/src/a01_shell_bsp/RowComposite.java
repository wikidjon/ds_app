package a01_shell_bsp;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class RowComposite extends Composite {

	final Button okButton;
	final Button cancelButton;

	static Display d;
	static Shell s;

	public static void main(final String[] args) {
		final Display d = new Display();
		final Shell  s = new Shell(d);
		s.setSize(250,275);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Ein Row Composite Beispiel");

		s.open();

		final Composite c = new Composite(s,SWT.BORDER);
		c.setBounds(10,30,170,150);
		// Randbegrenzung
		c.setBackground(new Color(d,31,133,31));

		//new RowComposite(c);

		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}


	public RowComposite(final Composite c)
	{
		super(c, SWT.NO_FOCUS);
		final RowLayout rl = new RowLayout();
		rl.wrap = false;
		rl.pack = false;
		this.setLayout(rl);
		this.okButton = new Button(this, SWT.BORDER | SWT.PUSH);
		this.okButton.setText("OK");
		this.okButton.setSize(30, 20);
		this.cancelButton = new Button(this, SWT.BORDER | SWT.PUSH);
		this.cancelButton.setText("Abbruch");
		this.cancelButton.setSize(30, 20);

		this.okButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				System.out.println("OK Button geklicked");
			}
		});

		this.cancelButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				System.out.println("Abbruch Button geklicked");
			}
		});
	}
}
