package a21b_clipboard;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.RTFTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class Ch12ClipboardComposite extends Composite {

  public Ch12ClipboardComposite(Composite parent) {
    super(parent, SWT.NONE);

    FillLayout layout = new FillLayout();
    setLayout(layout);

    Button b = new Button(this, SWT.NONE);
    b.setText("Copy to system clipboard");

    b.addSelectionListener(new SelectionListener() {

      public void widgetSelected(SelectionEvent e) {
        Clipboard clipboard = new Clipboard(getDisplay());
        String rtfData = "{\\rtf1\\b\\i Hello World}";
        RTFTransfer rtfTransfer = RTFTransfer.getInstance();
        clipboard.setContents(new Object[] { rtfData },
            new Transfer[] { rtfTransfer });
        clipboard.dispose();
      }

      public void widgetDefaultSelected(SelectionEvent e) {
      }

    });

  }
}

