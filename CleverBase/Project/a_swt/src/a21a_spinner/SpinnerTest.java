package a21a_spinner;

/*
 * (c) Copyright IBM Corp. 2000, 2001. All Rights Reserved.
 */

import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TypedListener;

class Spinner extends Composite {
	int handleSpinner;

	static Hashtable<Integer, Spinner> table = new Hashtable<Integer, Spinner>();
	static {
		System.loadLibrary("Spinner1");
	}

	static final native void computeSize(int handle, int[] result);

	/** ********* JAVA NATIVES *********** */

	static final native int createControl(int handle);

	static final native int getMaximum(int handle);

	static final native int getMinimum(int handle);

	static final native int getPosition(int handle);

	static final native void resizeControl(int handle, int x, int y, int width,
			int height);

	static final native void setFocus(int handle);

	static final native void setFont(int handle, long handle2);

	static final native void setMaximum(int handle, int max);

	static final native void setMinimum(int handle, int min);

	static final native void setPosition(int handle, int position);

	static void widgetSelected(int handle) {
		Spinner spinner = table.get(new Integer(handle));
		if (spinner == null)
			return;
		spinner.notifyListeners(SWT.Selection, new Event());
	}

	public Spinner(Composite parent, int style) {
		super(parent, style);
		int handleParent = (int) this.handle;
		this.handleSpinner = createControl(handleParent);
		if (this.handleSpinner == 0) {
			SWT.error(SWT.ERROR_NO_HANDLES);
		}
		table.put(new Integer(this.handleSpinner), this);
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				Spinner.this.widgetDisposed(e);
			}
		});
		addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				Spinner.this.controlResized(e);
			}
		});
		addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				Spinner.this.focusGained(e);
			}
		});
		Font font = getFont();
		setFont(this.handleSpinner, font.handle);
	}

	public void addSelectionListener(SelectionListener listener) {
		if (listener == null)
			throw new SWTError(SWT.ERROR_NULL_ARGUMENT);
		addListener(SWT.Selection, new TypedListener(listener));
	}

	@Override
	public Point computeSize(int wHint, int hHint, boolean changed) {
		checkWidget();
		int[] result = new int[2];
		computeSize(this.handleSpinner, result);
		if (wHint != SWT.DEFAULT) {
			result[0] = wHint;
		}
		if (hHint != SWT.DEFAULT) {
			result[1] = hHint;
		}
		int border = getBorderWidth();
		return new Point(result[0] + border * 2, result[1] + border * 2);
	}

	public void controlResized(ControlEvent e) {
		Rectangle rect = getClientArea();
		resizeControl(this.handleSpinner, rect.x, rect.y, rect.width, rect.height);
	}

	public void focusGained(FocusEvent e) {
		setFocus(this.handleSpinner);
	}

	public int getMaximum() {
		checkWidget();
		return getMaximum(this.handleSpinner);
	}

	public int getMinimum() {
		checkWidget();
		return getMinimum(this.handleSpinner);
	}

	public int getSelection() {
		checkWidget();
		return getPosition(this.handleSpinner);
	}

	@Override
	public void setFont(Font font) {
		super.setFont(font);
		int hFont = 0;
		if (font != null) {
			hFont = (int) font.handle;
		}
		setFont(this.handleSpinner, hFont);
	}

	public void setMaximum(int maximum) {
		checkWidget();
		setMaximum(this.handleSpinner, maximum);
	}

	public void setMinimum(int minimum) {
		checkWidget();
		setMinimum(this.handleSpinner, minimum);
	}

	public void setSelection(int selection) {
		checkWidget();
		setPosition(this.handleSpinner, selection);
	}

	public void widgetDisposed(DisposeEvent e) {
		table.remove(new Integer(this.handleSpinner));
		this.handleSpinner = 0;
	}
}

public class SpinnerTest {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new RowLayout());

		final Spinner1 spinner = new Spinner1(shell, 0);
		spinner.setMaximum(999);
		System.out.println("max set to " + spinner.getMaximum());
		spinner.setSelection(500);
		System.out.println("selection set to " + spinner.getSelection());
		spinner.setMinimum(100);
		System.out.println("min set to " + spinner.getMinimum());
		Font font = new Font(display, "Courier", 20, SWT.NORMAL);
		spinner.setFont(font);
		spinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(spinner.getSelection());
			}
		});
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		font.dispose();
	}
}
