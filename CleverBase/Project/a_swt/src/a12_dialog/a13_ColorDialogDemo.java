package a12_dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a13_ColorDialogDemo {

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display, SWT.DIALOG_TRIM |SWT.APPLICATION_MODAL);    //SWT.DIALOG_TRIM ???
		shell.setSize(320, 200);
		shell.open();

		ColorDialog colorDialog1 = new ColorDialog(shell);
		colorDialog1.setText("ColorDialog Demo");
		colorDialog1.setRGB(new RGB(255,0,0));
		RGB selectedColor = colorDialog1.open();
		System.out.println("Ausgewaehlte Farbe: " + selectedColor);
		System.out.println("R : " + selectedColor.red);
		System.out.println("G : " + selectedColor.green);
		System.out.println("B : " + selectedColor.blue);

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}