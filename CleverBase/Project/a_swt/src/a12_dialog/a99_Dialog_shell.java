package a12_dialog;

/* 
 * schaffen einer neuen shell und positionieren
 * mit oberen linken rand an der cursor position
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a99_Dialog_shell {
	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setText("Parent Shell");
		shell.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {					//!!!!!!
				Shell dialog = new Shell(shell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
				Point pt = display.getCursorLocation();
				dialog.setLocation(pt.x, pt.y);
				dialog.setText("Dialog Shell");
				dialog.setSize(200, 100);
				dialog.open();
			}
		});
		shell.setSize(400, 400);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
