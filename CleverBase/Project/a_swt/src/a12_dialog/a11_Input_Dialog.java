package a12_dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a11_Input_Dialog {

	// public void in_dialog (Shell shell, Display display) {
	public static void main(String[] arguments) {
		Display d = new Display();
		Shell s = new Shell(d);
		Label label = new Label(s, SWT.NONE);
		label.setText("Ihr Name:");
		final Text text = new Text(s, SWT.BORDER);
		text.setLayoutData(new RowData(100, SWT.DEFAULT));
		Button ok = new Button(s, SWT.PUSH);
		ok.setText("OK");

		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println("OK " + text.getSelectionText());
			}
		});

		Button cancel = new Button(s, SWT.PUSH);
		cancel.setText("Abbruch");

		cancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Abbruch");
			}
		});
		s.setDefaultButton(cancel);
		s.setLayout(new RowLayout());
		s.pack();
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch())
				d.sleep();
		}
		d.dispose();
	}
}
