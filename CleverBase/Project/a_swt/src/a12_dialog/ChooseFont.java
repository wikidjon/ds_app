package a12_dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;


public class ChooseFont {
	public static void main(String[] args) {
		new ChooseFont().run();
	}
	private Font font;

	private Color color;

	private void createContents(final Shell shell) {
		shell.setLayout(new GridLayout(2, false));

		final Label fontLabel = new Label(shell, SWT.NONE);
		fontLabel.setText("ausgewählter font");

		Button button = new Button(shell, SWT.PUSH);
		button.setText("Font...");
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// Create the color-change dialog
				FontDialog dlg = new FontDialog(shell);

				// Pre-fill the dialog with any previous selection
				if (ChooseFont.this.font != null) {
					dlg.setFontList(fontLabel.getFont().getFontData());
				}
				if (ChooseFont.this.color != null) {
					dlg.setRGB(ChooseFont.this.color.getRGB());
				}

				if (dlg.open() != null) {
					// Dispose of any fonts or colors we have created
					if (ChooseFont.this.font != null) {
						ChooseFont.this.font.dispose();
					}
					if (ChooseFont.this.color != null) {
						ChooseFont.this.color.dispose();
					}

					// Create the new font and set it into the label
					ChooseFont.this.font = new Font(shell.getDisplay(), dlg.getFontList());
					fontLabel.setFont(ChooseFont.this.font);

					// Create the new color and set it
					ChooseFont.this.color = new Color(shell.getDisplay(), dlg.getRGB());
					fontLabel.setForeground(ChooseFont.this.color);

					// Call pack() to resize the window to fit the new font
					shell.pack();
				}
			}
		});
	}

	public void run() {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Font Chooser");
		createContents(shell);
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		if (this.font != null) {
			this.font.dispose();
		}
		if (this.color != null) {
			this.color.dispose();
		}

		display.dispose();
	}
}
