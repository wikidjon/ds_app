package a12_dialog;

import java.util.regex.Pattern;

public class a00_StringSplitten
{
	public static void main(String[] args)
	{
		String satz = "DasHaus am Strand, wei� und sch�n, wurde von der Sonne " +
				"beschienen.\n Keiner war zu Hause.";
		Pattern p = Pattern.compile("[,. ]\\s*");	//kein Wei�raum, beliebiges Zeichen
		String[] sa = p.split(satz);
		int i = 1;
		for(String s: sa) {
			System.out.printf("%3d-ter Teil:  %s%n", i++ , s);
		}
	}
}
