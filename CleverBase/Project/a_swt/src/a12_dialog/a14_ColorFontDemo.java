package a12_dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a14_ColorFontDemo {

	public static Display disp;
	public static boolean internalCall = false;

	public static void main(String[] args) {
		disp = new Display();
		Shell shell = new Shell(disp);
		shell.setSize(200, 240);
		shell.setText("Color Font Demo");

		shell.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				//holen des Grafik-Kontext zum zeichnen des Text
				GC gc = e.gc;
				Display d = e.widget.getDisplay();

				//RGB Farbe zusammenstellen
				Color color = new Color(d, 220, 42, 105);
				gc.setForeground(color);

				//Font-Objekt anlegen mit Schriftart Arial,
				//Schriftgroesse 24 und BOLD / ITALIC
				Font font = new Font(d, "Arial", 24, SWT.BOLD | SWT.ITALIC);
				gc.setFont(font);

				//Text zeichnen
				gc.drawString("Text", 20, 20);

				// Freigaben
				color.dispose();
				font.dispose();
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!disp.readAndDispatch()) {
				disp.sleep();
			}
		}
	}
}
