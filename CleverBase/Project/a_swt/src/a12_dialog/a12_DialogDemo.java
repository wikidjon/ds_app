package a12_dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/*************************************************************************************************
 * Buttons, Events und Dialoge...
 * modale Dialoge verhindern das Benutzen der restlichen Ressourcen w�hrend der Anzeige des Dialogs
 * aus diesem Grund sollten modale Dialoge nur dann eingesetzt werden, wenn ohne
 * die Eingaben des Benutzers ein Weiterarbeiten nicht m�glich w�re.
 * 
 * 	SWT.MODELESS 						--> Dialog ist nicht modal
 * 	SWT.PRIMARY_MODAL 			--> Modal in Bezug auf die parent shell
 * 	SWT.APPLICATION_MODAL 	--> Modal in Bezug auf die Anwendung
 * 	SWT.SYSTEM_MODAL 				--> Modal in Bezug auf das ganze System
 ************************************************************************************************/
public class a12_DialogDemo {
	static Display display = new Display();
	static Shell shell = new Shell(display);

	public static void main(String[] arguments) {
		shell.setSize(300, 200);

		Button button = new Button(shell, SWT.PUSH);
		button.setLocation(10, 10);
		button.setSize(100, 20);
		button.setText("Klick mich!");
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode(e);
			}
		});

		Button button1 = new Button(shell, SWT.PUSH);
		button1.setLocation(10, 40);
		button1.setSize(100, 20);
		button1.setText("Eingabedialog");
		button1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode1(e);
			}
		});

		Button button2 = new Button(shell, SWT.PUSH);
		button2.setLocation(10, 70);
		button2.setSize(100, 20);
		button2.setText("Farbauswahl");
		button2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode2(e);
			}
		});

		Button button3 = new Button(shell, SWT.PUSH);
		button3.setLocation(10, 100);
		button3.setSize(100, 20);
		button3.setText("Dateiauswahl");
		button3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode3(e);
			}
		});

		Button button4 = new Button(shell, SWT.PUSH);
		button4.setLocation(150, 10);
		button4.setSize(100, 20);
		button4.setText("Directory");
		button4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode4(e);
			}
		});

		Button button5 = new Button(shell, SWT.PUSH);
		button5.setLocation(150, 40);
		button5.setSize(100, 20);
		button5.setText("Fontauswahl");
		button5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode5(e);
			}
		});

		Button button6 = new Button(shell, SWT.PUSH);
		button6.setLocation(150, 70);
		button6.setSize(100, 20);
		button6.setText("Drucker");
		button6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				methode6(e);
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private static void methode(SelectionEvent e) {
		int response1 = SWT.NO;
		Shell shell = ((Button) e.widget).getShell();

		MessageBox mb = new MessageBox(shell, SWT.ICON_QUESTION | SWT.YES
				| SWT.NO | SWT.CANCEL);
		do {
			mb.setMessage("Gef�llt Ihnen Java SWT?");
			int response = mb.open();
			switch (response) {
			case SWT.YES:
				System.out.println("Ja - super Sprache!.");
				response1 = SWT.NO;
				break;
			case SWT.NO:
				MessageBox mb1 = new MessageBox(shell);
				mb1.setMessage("Diese Antwort ist unzul�ssig!!!");
				response1 = mb1.open();
				System.out.println("Nein - ich trinke lieber ein Bier!");
				continue; // zur�ck zum vorhergehenden Dialog
			case SWT.CANCEL:
				System.out.println("Erst richtig antworten!!!");
				response1 = SWT.NO;
				break;
			}
		} while (response1 != SWT.NO);
	}

	private static void methode1(SelectionEvent e) {
		a15_ein_button eing = new a15_ein_button();
		eing.start(display);

	}

	private static void methode2(SelectionEvent e) {
		System.out.println("Quelle: " + e.getSource());
		Shell shell = ((Button) e.widget).getShell();
		ColorDialog colorDialog1 = new ColorDialog(shell);
		colorDialog1.setText("ColorDialog Demo");
		colorDialog1.setRGB(new RGB(255, 0, 0));
		RGB selectedColor = colorDialog1.open();
		System.out.println(selectedColor);
	}

	private static void methode3(SelectionEvent e) {
		Shell shell = ((Button) e.widget).getShell();
		String[] filter = { "*.txt", "*.doc", "*.*" };
		FileDialog fileDialog = new FileDialog(shell);
		fileDialog.setText("FileDialog Demo");
		fileDialog.setFilterPath("D:/");
		fileDialog.setFilterExtensions(filter);
		String selectedFile = fileDialog.open();
		System.out.println(selectedFile);
	}

	private static void methode4(SelectionEvent e) {
		Shell shell = ((Button) e.widget).getShell();
		DirectoryDialog dirDialog = new DirectoryDialog(shell, SWT.DIALOG_TRIM);
		dirDialog.setText("Verzeichnis Dialog Demo");
		dirDialog.setFilterPath("C:/");
		String selectedFile = dirDialog.open();
		System.out.println(selectedFile);
	}

	private static void methode5(SelectionEvent e) {
		Shell shell = ((Button) e.widget).getShell();
		FontDialog fontDialog = new FontDialog(shell, SWT.DIALOG_TRIM);
		fontDialog.setText("Font Dialog Demo");
		FontData fdata = fontDialog.open();
		System.out.println(fdata);
		System.out.println(fdata.getName());
		System.out.println(fdata.getStyle());
		System.out.println(fdata.getHeight());
	}

	/********************************************
	 * if (fdata != null) { Font font = new Font(display, fdata);
	 * text.setFont(font);
	 **********************************************/

	private static void methode6(SelectionEvent e) {
		Shell shell = ((Button) e.widget).getShell();
		PrintDialog printDialog = new PrintDialog(shell, SWT.DIALOG_TRIM);
		System.out.println("PrintDialog: " + printDialog);

		PrinterData pa = printDialog.open();
		printDialog.setText("Druck Dialog Demo");
		System.out.println("PrinterData: " + pa);

		System.out.println("Start (PD): " + printDialog.getStartPage());
		System.out.println("Ende: (PD)" + printDialog.getEndPage());

		System.out.println("Kopieanzahl " + pa.copyCount);
		System.out.println("Name: " + pa.name);
	}
};
