

/*******************************************

You can package your classes in a jar file and that Jar file can be used in class path. 

Following commands needs to be executed for various purposes in managing Jar files.

1.jar cf jar-file inputfiles: It is used to create new JAR.
2.jar tf jar-file: It is used to view all the files of the JAR.
3.jar xf jar-file: It is used to extract the JAR. 
4.java -jar app.jar: It is used to run a JAR if the manifest file is present in the JAR. This manifest file contains the information of the file having main method. Content of the manifest is:
Main-Class: classname






**********************************************/