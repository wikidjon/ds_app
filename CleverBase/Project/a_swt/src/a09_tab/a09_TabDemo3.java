package a09_tab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import a01_shell_bsp.GridComposite;
import a07_group.a07_Gruppe1;

public class a09_TabDemo3 {
    @SuppressWarnings("unused")       
    public static void main(String[] args) {
    	
        Display d = new Display();
        Shell s = new Shell(d);
        
        s.setSize(250,200);
 //       s.setImage(new Image(d, "icons/cut.gif"));
        s.setText("Tabsbeispiel");
        s.setLayout(new FillLayout());
        
        TabFolder tf = new TabFolder(s, SWT.BORDER);
//		tf.setBounds(10,10,270,250);		//**********
        
        TabItem ti1 = new TabItem(tf, SWT.BORDER);
        ti1.setText("Optionen Gruppe");
        ti1.setControl(new a07_Gruppe1(tf, SWT.SHADOW_ETCHED_IN, "Gruppe"));
        
        TabItem ti2 = new TabItem(tf, SWT.BORDER);
        ti2.setText("Grid");
        ti2.setControl(new GridComposite(tf));
        
        TabItem ti3 = new TabItem(tf, SWT.BORDER);
        ti3.setText("Text");
        Composite c1 = new Composite(tf, SWT.BORDER);
        c1.setLayout(new FillLayout());

		Text t = new Text(c1, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        ti3.setControl(c1);
        
        TabItem ti4 = new TabItem(tf, SWT.BORDER);
        ti4.setText("Settings");
        Composite c2 = new Composite(tf, SWT.BORDER);
        c2.setLayout(new RowLayout());
        Text t2 = new Text(c2, SWT.BORDER | SWT.SINGLE | SWT.WRAP | SWT.V_SCROLL);
        Button b = new Button(c2, SWT.PUSH |SWT.BORDER);
        b.setText("Speichern");
        ti4.setControl(c2);
        
        s.open();
        while(!s.isDisposed()){
            if(!d.readAndDispatch())
                d.sleep();
        }
        d.dispose();
    }
}
