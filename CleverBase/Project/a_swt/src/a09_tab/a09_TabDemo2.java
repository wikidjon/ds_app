package a09_tab;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

public class a09_TabDemo2 {

	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);

		TabFolder tabFolder = new TabFolder (shell, SWT.NONE);

		for (int i=0; i<6; i++) {
			TabItem item = new TabItem (tabFolder, SWT.NONE);
			item.setText ("Tab " + i);
			item.setImage(new Image(display, "src/icons/yes.gif"));//cancel.gif
			// !!!!		item.setImage(new Image(display, "src/images/splash.jpg"));//cancel.gif
			//final Image image = new Image(display, "src/images/splash.jpg");
			//*************************************************
			Composite labelComp = new Composite(tabFolder,SWT.NONE);
			//		labelComp .....
			Label label1 = new Label(labelComp,SWT.NONE);
			label1.setText("Label 1 - Tab " + i);
			label1.setBounds(10,10,250,20);
			Label label2 = new Label(labelComp,SWT.NONE);
			label2.setText("Label 2 - Tab " + i);
			label2.setBounds(10,40,200,20);
			item.setControl(labelComp);
			//****************************************************
			//Button button = new Button(tabFolder, SWT.PUSH);
			//button.setText ("Seite " + i);
			//item.setControl (button);
		}
		tabFolder.pack ();
		shell.pack ();
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
