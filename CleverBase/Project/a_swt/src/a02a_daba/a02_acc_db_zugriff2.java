package a02a_daba;

/***********************************************************
 * Zugriff auf ODBC Datenbank toolsys
 * zwei Wege:
 * 		1. konventionell �ber ODBC/JDBC Bridge
 * 		2. mit nativem Treiber (z.B. f�r 64 bit Systeme) �ber includierte jars,
 **********************************************************/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class a02_acc_db_zugriff2 {

	public static void main(final String[] args) {

		/** H�lt die Datenbankverbindung */
		Connection con = null;

		/** alte Version mit ODBC Bridge ********************************************************************/
		final String url = "jdbc:odbc:toolsys";			// Verwaltung

		/** neue Version mit nativem ODBC Treiber (auch f�r 64 bit Systeme *********************************/
		//final String url = "jdbc:ucanaccess://C:/Ausgang/ToolSys.mdb";

		/** ZUgriff ohne ODBC Eintrag ***********************************************************************/
		// funktioniert nicht !!!!
		//final String url =  "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=C:/Ausgang/ToolSys.mdb";

		//String treiber = "com.ms.jdbc.odbc.JdbcOdbcDriver";
		final String treiber = "sun.jdbc.odbc.JdbcOdbcDriver";
		final String user = "";
		final String password = "";

		try {
			Class.forName(treiber);  	// Treiber laden.

			// Verbindung aufbauen.
			//con = DriverManager.getConnection(url, user, password);
			con=DriverManager.getConnection( url);

			final Statement stmt = con.createStatement();
			final String query = "SELECT VName, NName FROM Teilnehmer";
			final ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				final String vn = rs.getString("VName");
				final String nn = rs.getString("NName");
				System.out.println(vn + "  " + nn);
			}
			rs.close();
		} catch (final Exception e) {
			System.out.println(e.toString());
		}
	}
}
