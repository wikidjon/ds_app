package a02a_daba;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collections;

public class a020_treiber_list {

	public static void main(String[] args) {

		ArrayList<Driver> drivers = Collections.list(DriverManager.getDrivers());
		for (int i = 0; i < drivers.size(); i++) {
			Driver driver = drivers.get(i);

			// Name des Treibers
			String name = driver.getClass().getName();
			System.out.println("\nName des Treibers: " + name);

			// Versionsinfo
			int majorVersion = driver.getMajorVersion();
			int minorVersion = driver.getMinorVersion();
			boolean isJdbcCompliant = driver.jdbcCompliant();
			System.out.println("majorVersion: " + majorVersion);
			System.out.println("minorVersion: " + minorVersion);
			System.out.println("isJDBCCompliant: " + isJdbcCompliant);
		}
	}
}
