package a02a_daba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/***********************************************************
 *Zugriff auf ODBC Datenbank toolsys
 * Einf�gen von (statischen) Daten in die Datenbank
 **********************************************************/

public class a02_acc_db_zugriff3 {

	public static void main(String[] args) {
		/** H�lt die Datenbankverbindung */
		Connection myConnection = null;
		String url = "jdbc:odbc:toolsys";
		//    String driverClass = "com.ms.jdbc.odbc.JdbcOdbcDriver";
		String driverClass = "sun.jdbc.odbc.JdbcOdbcDriver";
		String user = "";
		String password = "";
		//Beispiel einzufuegende Daten
		String nname = "meierjhgjhgj";
		String vname = "testjkhkjghk";

		try {
			// Treiber laden.
			Class.forName(driverClass);
			// Verbindung aufbauen.
			myConnection = DriverManager.getConnection(url, user, password);

			Statement stmt = myConnection.createStatement();

			//Einf�gen von Daten in die Datenbank
			String query = "INSERT INTO teilnehmer (VName, NName) VALUES ('" + nname + "','" + vname + "')";
			stmt.executeUpdate(query);

			query = "SELECT VName, NName FROM Teilnehmer";

			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				String vn = rs.getString("VName");
				String nn = rs.getString("NName");
				System.out.println(vn + "  " + nn);
			}
			rs.close();
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}

	}

	public a02_acc_db_zugriff3() {
	}
}



