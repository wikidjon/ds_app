package a02a_daba;

// Muster JDBC-ODBC Zugriff
//import java.net.URL;
import java.sql.*;

class einf_Select_Bsp {

	public static void main(String args[]) {
//		String url = "jdbc:odbc:my-dsn";
        String url = "jdbc:odbc:toolsys2";
//		String query = "SELECT * FROM emp";
		String query = "SELECT * FROM Teilnehmer";

		try {

			// Laden des jdbc-odbc bridge Treibers
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

			//DriverManager.setLogStream(System.out);

			Connection con = DriverManager.getConnection(url, "user", "pw");

			// pruefen und anzeigen von warnings waehrend der verbindung
			checkForWarning(con.getWarnings());

			// Anzeige der DatabaseMetaData 
			DatabaseMetaData dma = con.getMetaData();
			System.out.println("-----Metadaten-----");
			System.out.println("Verbunden mit " + dma.getURL());
			System.out.println("Treiber       " + dma.getDriverName());
			System.out.println("Produktname   " + dma.getDatabaseProductName());
			System.out.println("Version       " + dma.getDriverVersion());
			System.out.println("");

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			dispResultSet(rs);

			rs.close();
			stmt.close();
			con.close();
			
		} catch (SQLException ex) {
			System.out.println("\n*** SQLException geworfen ***\n");

			while (ex != null) {
				System.out.println("SQLStatus: " + ex.getSQLState());
				System.out.println("Message:  " + ex.getMessage());
				System.out.println("Fehlercode:   " + ex.getErrorCode());
				ex = ex.getNextException();
				System.out.println("");
			}
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
		}
	}

	// -------------------------------------------------------------------
	// weitere warnungen 
	// -------------------------------------------------------------------

	private static boolean checkForWarning(SQLWarning warn) throws SQLException {
		boolean rc = false;

		if (warn != null) {
			System.out.println("\n *** Warnung ***\n");
			rc = true;
			while (warn != null) {
				System.out.println("SQLStatus: " + warn.getSQLState());
				System.out.println("Message:  " + warn.getMessage());
				System.out.println("Fehlercode:   " + warn.getErrorCode());
				System.out.println("");
				warn = warn.getNextWarning();
			}
		}
		return rc;
	}

	// -------------------------------------------------------------------
	// Anzeige ResultSet
	// -------------------------------------------------------------------

	private static void dispResultSet(ResultSet rs) throws SQLException {
		int i;

		// ResultSetMetaData
		// fuer die Spaltenueberschriften 
		ResultSetMetaData rsmd = rs.getMetaData();

		// Anzahl Spalten im result set
		int numCols = rsmd.getColumnCount();

		// Anzeige Spaltenueberschriften
		for (i = 1; i <= numCols; i++) {
			if (i > 1)
				System.out.print(",   ");
			System.out.print(rsmd.getColumnLabel(i));
		}
		System.out.println("");

		// Anzeige der Daten 
		boolean more = rs.next();
		while (more) {

			for (i = 1; i <= numCols; i++) {
				if (i > 1)
					System.out.print(",       ");
				System.out.print(rs.getString(i));
			}
			System.out.println("");

			more = rs.next();
		}
	}
}