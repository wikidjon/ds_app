package a02a_daba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableItem;

@SuppressWarnings("unused")
public class dabamysql {

	public static Connection connection() {

		Connection myConnection = null;
		// DB test ist unter MySql eingerichtet - und beinhaltet cd_liste
		String url = "jdbc:mysql://localhost:3306/test?";
		String driverClass = "org.gjt.mm.mysql.Driver";
		String user = "";
		String password = "";

		try {
			// Treiber laden.
			Class.forName(driverClass).newInstance();
			// Verbindung aufbauen.
			myConnection = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return myConnection;
	}

	public static void statement(Connection myCon) {
		try {
			String query = "SELECT Gruppe, Titel FROM cd_liste";
			Statement stmt = myCon.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				String vn = rs.getString("Gruppe");
				String nn = rs.getString("Titel");
//				TableItem item1 = new TableItem(table1, SWT.NONE);
//				item1.setText(new String[] { vn, "1", nn });

				System.out.println(vn + " --  " + nn + "-");
			} // zu while

			rs.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}