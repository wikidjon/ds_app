package a02a_daba;

/***********************************************************
 *Zugriff auf MySQL Datenbank test als Applikation
 **********************************************************/

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class a02_mysql_musi {

	public static void main(String[] args) {
		Connection myConnection = null;
		// DB test ist unter MySql eingerichtet - und beinhaltet cd_liste
		String url = "jdbc:mysql://localhost:3306/test?";
		String driverClass = "org.gjt.mm.mysql.Driver";
		//String driverClass = "com.mysql.jdbc.Driver";
		String user = "";
		String password = "";

		try {
			// Treiber laden.
			Class.forName(driverClass).newInstance();
			// Verbindung aufbauen.
			myConnection = DriverManager.getConnection(url, user, password);

			String query = "SELECT Gruppe, Titel FROM cd_liste";
			Statement stmt = myConnection.createStatement();
			ResultSet rs = stmt.executeQuery (query);

			while (rs.next())        {
				String vn = rs.getString("Gruppe");
				String nn = rs.getString("Titel");
				System.out.println(vn + " --  " + nn);
			}
			rs.close();
		}
		catch(Exception e) {
			System.out.println(e.toString());
		}
	}
}

