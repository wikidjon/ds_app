package a99_jdombsp;

import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.output.SAXOutputter;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * XMLReader wrapper fuer JDOM documente.
 */
public class DocumentReader extends XMLReaderBase {

	private final Document doc;

	/** Creates new DocumentReader */
	public DocumentReader(Document doc) {
		this.doc = doc;
	}

	@Override
	public void parse(InputSource input) throws SAXException, IOException {
		SAXOutputter outputter = new SAXOutputter(this, this, this, this, this);
		try {
			outputter.output(this.doc);
		}
		catch (JDOMException ex) {
			throw new SAXException(ex);
		}
	}
}
