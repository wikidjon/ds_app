package a11_label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a02_SeparatorLabelExample {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell();
    shell.setLayout(new GridLayout(1, false));

    // Create a vertical separator
    new Label(shell, SWT.SEPARATOR);


    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}