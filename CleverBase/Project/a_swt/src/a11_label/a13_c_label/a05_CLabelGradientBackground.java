package a11_label.a13_c_label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a05_CLabelGradientBackground {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setText("CLabel Gradient");

    shell.setLayout(new GridLayout(1, false));

    // Create the CLabels
    CLabel one = new CLabel(shell, SWT.LEFT);
    one.setText("First Gradient Example");
    one.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    one.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_GRAY));
    // Set the background gradient
    one.setBackground(new Color[] { shell.getDisplay().getSystemColor(SWT.COLOR_RED),
        shell.getDisplay().getSystemColor(SWT.COLOR_GREEN),
        shell.getDisplay().getSystemColor(SWT.COLOR_BLUE) }, new int[] { 50, 50 });

    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}