package a11_label.a13_c_label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a07_CLabelBackgroundImage {
  public static void main(String[] args) {
    final Display display = new Display();
    final Shell shell = new Shell(display, SWT.SHELL_TRIM);
    shell.setLayout(new FillLayout());

    CLabel label = new CLabel(shell, SWT.BORDER );
    label.setText("text on the label");

    label.setBackground(new Image(display,"src/images/icon.gif"));
    shell.open();
    // Set up the event loop.
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        // If no more entries in event queue
        display.sleep();
      }
    }
    display.dispose();
  }

}