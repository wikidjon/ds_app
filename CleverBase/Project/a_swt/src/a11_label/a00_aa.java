/************************************************
package a11_label;
Labels are non-selectable user interface components that display strings or images. 
Styles supported by the Label class are as follows: SEPARATOR, HORIZONTAL, VERTICAL 
The SEPARATOR style causes a label to appear as a single line whose orientation can be either HORIZONTAL (default) or VERTICAL. 
To create a Label, use its only constructor: 
Label(Composite parent, int style)

parent: the Composite to house the Label,
style : the style.

Labels display either text or an image, but not both.


16. 13. 2. Label Styles

You can combine them using the bitwise OR operator,
You can specify only one alignment constant (SWT.LEFT, SWT.CENTER, or SWT.RIGHT).
Many of the styles have an effect only when creating separators

Style	Description
SWT.SEPARATOR	Creates a visual divider.
SWT.HORIZONTAL	Used with SWT.SEPARATOR to create a horizontal separator.
SWT.VERTICAL	Used with SWT.SEPARATOR to create a vertical separator. This is the default.
SWT.SHADOW_IN	Used with SWT.SEPARATOR to draw a separator that appears recessed.
SWT.SHADOW_OUT	Used with SWT.SEPARATOR to draw a separator that appears extruded.
SWT.SHADOW_NONE	Used with SWT.SEPARATOR to draw a separator that appears unshadowed.
SWT.CENTER	Orients the text or image in the center of this Label.
SWT.LEFT	Orients the text or image to the left of this Label.
SWT.RIGHT	Orients the text or image to the right of this Label.
SWT.WRAP	Creates a Label that can wrap. Support for wrapped labels depends on the layout manager and is spotty.


***/