package a11_table;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class a11_Tabelle1 {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300, 300);
		shell.setText("Tabellen Demo");

		Table table = new Table(shell, SWT.NONE);
		// Gestaltung!!!
		table.setBounds(10, 10, 270, 160);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableColumn a = new TableColumn(table, SWT.CENTER);
		a.setText("a");
		a.setWidth(50);
		TableColumn b = new TableColumn(table, SWT.CENTER);
		b.setText("b");
		b.setWidth(50);
		TableColumn c = new TableColumn(table, SWT.CENTER);
		c.setText("c");
		c.setWidth(50);
		TableColumn d = new TableColumn(table, SWT.CENTER);
		d.setText("d");
		TableColumn e = new TableColumn(table, SWT.CENTER);
		e.setText("e");
		e.setWidth(50);
		//************************
		d.setResizable(false);
		d.setWidth(50);

		TableItem a1 = new TableItem(table, SWT.NONE);
		a1.setText(new String[] { "22", "25", "33", "44", "99" }); //Achtung - ein Wert zuviel!!!
		TableItem a2 = new TableItem(table, SWT.NONE);
		a2.setText(new String[] { "5", "66", "26","", "109" });

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
