package a11_table;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.*;

public class a11_Tabelle9 {

	public static void main(String[] args) {
		final Display d = new Display();
		Shell s = new Shell(d);

		s.setSize(250, 200);
		s.setImage(new Image(d, "src/images/welcome.gif"));
		s.setText("Tabellen Shell Beispiel");
		GridLayout gl = new GridLayout();
		gl.numColumns = 2;
		s.setLayout(gl);

		final Table t = new Table(s, SWT.BORDER | SWT.CHECK | SWT.MULTI	| SWT.FULL_SELECTION);
		final GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 2;
		t.setLayoutData(gd);
		t.setHeaderVisible(true);
		final TableColumn tc1 = new TableColumn(t, SWT.LEFT);
		final TableColumn tc2 = new TableColumn(t, SWT.CENTER);
		final TableColumn tc3 = new TableColumn(t, SWT.CENTER);
		tc1.setText("Vorname");
		tc2.setText("Nachname");
		tc3.setText("Addresse");
		tc1.setWidth(70);
		tc2.setWidth(70);
		tc3.setWidth(80);
		final TableItem item1 = new TableItem(t, SWT.NONE);
		item1.setText(new String[] { "Erich", "Honnecker", "New York" });
		final TableItem item2 = new TableItem(t, SWT.NONE);
		item2.setText(new String[] { "Walter", "Ulbricht", "Berlin" });
		final TableItem item3 = new TableItem(t, SWT.NONE);
		item3.setText(new String[] { "Egon", "Krenz", "Wandlitz" });

		final Text input = new Text(s, SWT.SINGLE | SWT.BORDER);
		final Button searchBtn = new Button(s, SWT.PUSH);
		searchBtn.setText("Suchen");
		searchBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				TableItem[] tabitem = t.getItems();

				for (int i = 0; i < tabitem.length; i++) {
					if (tabitem[i].getText(2).equals(input.getText())) {
						tabitem[i].setBackground(new Color(d, 127, 178, 127));
					}
					// **************/
				}
			}
		});

		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch())
				d.sleep();
		}
		d.dispose();
	}
}
