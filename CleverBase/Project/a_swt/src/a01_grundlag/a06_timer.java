package a01_grundlag;
/*
 * anlegen eines timers und wiederholen alle 500 ms)
 */
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a06_timer {

	public static void main (String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);
		final int time = 500;
		Runnable timer = new Runnable () {
			@Override
			public void run () {
				Point point = display.getCursorLocation ();
				Rectangle rect = shell.getBounds ();
				if (rect.contains (point)) {
					System.out.println ("drinnen");
				} else {
					System.out.println ("draussen");
				}
				display.timerExec (time, this);
			}
		};
		display.timerExec (time, timer);
		shell.setSize (200, 200);
		shell.open ();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
