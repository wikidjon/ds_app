package a01_grundlag;
//package a01_grundlag;


// finden des programmicons das .bmp dateien editiert

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a12_find_icon {

	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);

		Label label = new Label (shell, SWT.NONE);
		label.setText ("kann das Icon nicht finden .bmp");
		Image image = null;
		//findet programm, das mit bmp verbunden ist
		Program p = Program.findProgram (".bmp");	//jpg - png ...
		if (p != null) {
			ImageData data = p.getImageData ();
			if (data != null) {
				image = new Image (display, data);
				label.setImage (image);
			}
		}
		label.pack ();
		shell.pack ();
		shell.open ();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		if (image != null) {
			image.dispose ();
		}
		display.dispose ();
	}

}
