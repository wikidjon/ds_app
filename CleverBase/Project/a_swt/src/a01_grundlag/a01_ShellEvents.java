package a01_grundlag;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a01_ShellEvents {
  public static void main(String[] args) {
    final Display display = new Display();
    final Shell shell = new Shell(display, SWT.SHELL_TRIM);
    shell.setLayout(new FillLayout());

    shell.addShellListener(new ShellListener() {

      public void shellActivated(ShellEvent event) {
        System.out.println("aktiviert");
      }

      public void shellClosed(ShellEvent arg0) {
        System.out.println("geschlossen");
      }

      public void shellDeactivated(ShellEvent arg0) {
    	  System.out.println("deaktiviert");
      }

      public void shellDeiconified(ShellEvent arg0) {
    	  System.out.println("deiconified");
      }

      public void shellIconified(ShellEvent arg0) {
    	  System.out.println("iconified");
      }
    });

    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }

}