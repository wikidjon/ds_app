package a01_grundlag;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class a0_shell2 {

	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);

		//***** Groesse, Positionierung **************
		//shell.setSize (200, 200);
		Monitor pMonitor = display.getPrimaryMonitor ();
		Rectangle bounds = pMonitor.getBounds ();
		Rectangle rect = shell.getBounds ();
		System.out.println("Monitor:  Breite " + bounds.width + " Hoehe " + bounds.height + "\nShell:  Breite "+ rect.width + " Hoehe: " +rect.height);
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		//shell.setLocation (x, y);

		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
