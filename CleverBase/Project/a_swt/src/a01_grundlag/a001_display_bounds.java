package a01_grundlag;

import org.eclipse.swt.widgets.Display;

/*
 * Grenzen des Clientbereiches und Display
 */

public class a001_display_bounds {

	public static void main (String [] args) {
		Display display = new Display ();
		System.out.println ("Display Grenzen=" + display.getBounds () + "\nDisplay ClientArea=" + display.getClientArea ());
		display.dispose ();
	}
}
