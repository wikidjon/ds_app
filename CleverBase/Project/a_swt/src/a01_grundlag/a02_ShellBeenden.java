package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class a02_ShellBeenden {
	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display, SWT.SHELL_TRIM);
		shell.setLayout(new FillLayout());

		shell.addShellListener(new ShellListener() {

			@Override
			public void shellActivated(ShellEvent event) {
				System.out.println("Shell aktiviert");
			}

			@Override
			public void shellClosed(ShellEvent event) {
				MessageBox messageBox = new MessageBox(shell, SWT.APPLICATION_MODAL | SWT.YES | SWT.NO);
				messageBox.setText("Warnung");
				messageBox
				.setMessage("Es gibt ungesicherte Daten. Wollen Sie die Shell trotzdem schlie�en?");
				if (messageBox.open() == SWT.YES) {
					event.doit = true;
				} else {
					event.doit = false;
				}
			}

			@Override
			public void shellDeactivated(ShellEvent arg0) {
				System.out.println("Shell deaktiviert");
			}

			@Override
			public void shellDeiconified(ShellEvent arg0) {
				System.out.println("Shell deiconified");
			}

			@Override
			public void shellIconified(ShellEvent arg0) {
				System.out.println("Shell iconifed");
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}