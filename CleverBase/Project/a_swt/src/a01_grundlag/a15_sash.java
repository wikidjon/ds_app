package a01_grundlag;
//package a01_grundlag;

// beweglich sash

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;

public class a15_sash {

	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);
		final Sash sash = new Sash (shell, SWT.BORDER | SWT.VERTICAL);
		sash.setBounds (10, 10, 40, 100);			//*************
		sash.addListener (SWT.Selection, new Listener () {
			@Override
			public void handleEvent (final Event e) {
				sash.setBounds (e.x, e.y, e.width, e.height);  ///**********
			}
		});
		shell.open ();
		sash.setFocus ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
