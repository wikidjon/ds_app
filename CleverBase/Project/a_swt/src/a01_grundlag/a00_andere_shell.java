package a01_grundlag;

// nicht-recteckiges fenster

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Region;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a00_andere_shell {

	static int[] circle(final int r, final int offsetX, final int offsetY) {
		final int[] polygon = new int[8 * r + 4];
		// x^2 + y^2 = r^2;
		for (int i = 0; i < 2 * r + 1; i++) {
			final int x = i - r;
			final int y = (int) Math.sqrt(r * r - x * x);
			polygon[2 * i] = offsetX + x;
			polygon[2 * i + 1] = offsetY + y;
			polygon[8 * r - 2 * i - 2] = offsetX + x;
			polygon[8 * r - 2 * i - 1] = offsetY - y;
		}
		return polygon;
	}

	public static void main(final String[] args) {
		final Display display = new Display();
		// Shell muss mit Style SWT.NO_TRIM erzeugt werden
		final Shell shell = new Shell(display, SWT.NO_TRIM | SWT.ON_TOP);
		shell.setBackground(display.getSystemColor(SWT.COLOR_RED));

		// Bereich definieren, der wie ein Schluesselloch aussieht
		final Region region = new Region();
		region.add(circle(67, 67, 67));
		region.subtract(circle(20, 67, 50));
		region.subtract(new int[] { 67, 50, 55, 105, 79, 105 });

		// Bereich der shell definieren, der Schluesselloch enthaelt
		shell.setRegion(region);
		final Rectangle size = region.getBounds();
		shell.setSize(size.width, size.height);
		// bewegen der shell
		final Listener l = new Listener() {
			Point origin;

			@Override
			public void handleEvent(final Event e) {
				switch (e.type) {
				case SWT.MouseDown:
					this.origin = new Point(e.x, e.y);
					System.out.println("Maus gedrueckt");
					break;
				case SWT.MouseUp:
					this.origin = null;
					System.out.println("Maus losgelassen");
					break;
				case SWT.MouseMove:
					if (this.origin != null) {
						final Point p = display.map(shell, null, e.x, e.y);
						shell.setLocation(p.x - this.origin.x, p.y - this.origin.y);
						//System.out.println("Mouse move");
					}
					break;
				}
			}
		};
		shell.addListener(SWT.MouseDown, l);
		shell.addListener(SWT.MouseUp, l);
		shell.addListener(SWT.MouseMove, l);

		//Button zum Schliessen
		final Button b = new Button(shell, SWT.PUSH);
		b.setBackground(shell.getBackground());
		b.setText("ende");
		b.pack();
		b.setLocation(10, 68);
		b.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				shell.close();
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		region.dispose();
		display.dispose();
	}
}
