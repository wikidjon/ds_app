package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class fenster {

	public static void main(final String[] arguments) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setSize(300, 300);
		shell.open();

		Button button1 = new Button(shell,SWT.PUSH);
		button1.setText("Button 1");


		button1.setLocation(10,10);
		button1.setSize(100,20);

		button1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {

				final Shell shell1 = new Shell(display);
				shell1.setSize(200, 200);
				shell1.open();



			}
		});

		final Button button2 = new Button(shell, SWT.ARROW);
		//final Button button2 = new Button(shell, SWT.TOGGLE/SWT.ARROW);
		button2.setSize(40,40);
		//button2.setSize(120,120);
		//button2.setText("B2");
		button2.setLocation(200,200);

		button2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				System.out.println("Button2 gedrueckt");
			}
		});

		final Button button3 = new Button(shell, SWT.FLAT|SWT.TOGGLE);
		button3.setText("Button 3");
		button3.setSize(100,50);
		button3.setLocation(20,150);

		button3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				System.out.println("Button3 gedrueckt");
			}
		});

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
		button1.dispose();
		button2.dispose();
		button3.dispose();
	}
}
