package a01_grundlag;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a0_shell4 {

public static void main (String [] args) {
	Display display = new Display ();
	Shell shell = new Shell (display);
	shell.setMaximized (true);
	shell.open ();
	
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}

} 
