package a01_grundlag;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class FadeOutImage extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(final String[] args) {
		final JFrame frame = new JFrame("Fade out");
		frame.add(new FadeOutImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 250);
		frame.setVisible(true);
	}

	Image myImage = new ImageIcon("src/images/splash.jpg").getImage();

	Timer timer = new Timer(200, this);

	private float alpha = 1f;

	public FadeOutImage() {
		this.timer.start();
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		this.alpha += -0.01f;
		if (this.alpha <= 0) {
			this.alpha = 0;
			this.timer.stop();
		}
		repaint();
	}
	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		final Graphics2D g2d = (Graphics2D) g;

		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, this.alpha));
		g2d.drawImage(this.myImage, 10, 10, null);
	}

}
