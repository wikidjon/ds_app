package a22a_xml;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.AttributeList;
import org.xml.sax.HandlerBase;

// eventorganisation
// Parser jdom -> properties
@SuppressWarnings("deprecation")

public class StudentenlisteSAX extends HandlerBase {
	protected static boolean parsVorname;
	protected static StringBuffer Vorname;

	protected static boolean parsNachname;
	protected static StringBuffer Nachname;

	protected static boolean parsWohnort;
	protected static StringBuffer Wohnort;

	public static void main(String[] args) {
		try {

			//instanz einer parser factory - stellt einer app einen parser zur verfuegung
			//spezielle Klasse zum Erzeugen von Klassen (z.B. bei dynamischer Anzahl von Klassen
			SAXParserFactory factory = SAXParserFactory.newInstance();

			// validierender parser
			factory.setValidating(true);

			// holen eines SAX parsers mit parse-Methode mit Xerces (Standardreader)
			SAXParser sParser = factory.newSAXParser();

			String filename = "C:/a_xml/student.xml";

			if (args.length > 0)
			{  filename = args[0];}

			// listener fuer Events  //HandlerBase
			StudentenlisteSAX  eventHandler = new StudentenlisteSAX();
			// parsen der datei
			sParser.parse(new File(filename), eventHandler);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ueberschriebene Callback-Methoden von ContentHandler werden bei Auftritt der
	// spezifischen Ereignisse aufgerufen - startElement(), characters(), endElement(), endDocument() etc.

	@Override
	public void characters(char[] chars, int start, int length) {
		// koennen die zeichen an die namensbuffer angehaengt werden
		if (parsVorname) {
			Vorname.append(chars, start, length);
		}
		else if (parsNachname) {
			Nachname.append(chars, start, length);
		}
		else if (parsWohnort) {
			Wohnort.append(chars, start, length);
		}
	}


	@Override
	public void endElement(String elementName) {
		// wenn ende des personen elements ausgabe der namen
		if (elementName.equals("person")) {
			if (Vorname != null) {
				System.out.print(Vorname.toString());
				System.out.print(" - ");
			}
			if (Nachname != null) {
				System.out.print(Nachname.toString());
				System.out.print(" - ");
			}
			if (Wohnort != null) {
				System.out.print(Wohnort.toString());
			}
			System.out.print("    *** Ende Person (endElement)");
			System.out.println();
		}
		// wenn ende eines der namen - anzeigen das keine namen mehr geparst werden
		else if (elementName.equals("vorname")) {
			parsVorname = false;
		}
		else if (elementName.equals("nachname")) {
			parsNachname = false;
		}
		else if (elementName.equals("wohnort")) {
			parsWohnort = false;
		}
	}

	@Override
	public void startElement(String elementName, AttributeList attributes) {
		// wenn das element eine person (zusammenfassung) ist, dann puffer null
		if (elementName.equals("person")) {
			Vorname = null;
			Nachname = null;
			Wohnort = null;
			System.out.print("Beginn Person (startElement) ");
		}

		// wenn Vorname element dann neuer puffer
		else if (elementName.equals("vorname")) {
			parsVorname = true;
			Vorname = new StringBuffer();
		}

		// wenn Nachname element dann neuer puffer
		else if (elementName.equals("nachname")) {
			parsNachname = true;
			Nachname = new StringBuffer();
		}

		// wenn Wohnort element dann neuer puffer
		else if (elementName.equals("wohnort")) {
			parsWohnort = true;
			Wohnort = new StringBuffer();
		}
	}
}
