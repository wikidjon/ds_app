package a13_layouts;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SimpleForm extends ApplicationWindow {

	public static void main(String[] args) {
		SimpleForm simpleForm = new SimpleForm(null);
		simpleForm.setBlockOnOpen(true);
		simpleForm.open();
	}
	public SimpleForm(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		composite.setLayout(new GridLayout());

		// Sets up the toolkit.
		FormToolkit toolkit = new FormToolkit(getShell().getDisplay());

		// create a form instance.
		Form form = toolkit.createForm(composite);
		form.setLayoutData(new GridData(GridData.FILL_BOTH));

		form.setText("Eclipse Forms");

		form.getBody().setLayout(new GridLayout());
		@SuppressWarnings("unused")
		Button button = toolkit.createButton(form.getBody(), "Test", SWT.NULL);

		// tool bar
		form.getToolBarManager().add(new Action("TEST") {
			@Override
			public void run() {
			}
		});

		Menu menu = new Menu(form.getBody());
		MenuItem item = new MenuItem(menu, SWT.NULL);
		item.setText("Testing item");
		form.setMenu(menu);

		form.updateToolBar();

		return composite;
	}

}
