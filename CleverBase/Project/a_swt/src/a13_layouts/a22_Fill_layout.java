package a13_layouts;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a22_Fill_layout {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);
		//		FillLayout fillLayout = new FillLayout (SWT.HORIZONTAL);
		FillLayout fillLayout = new FillLayout (SWT.VERTICAL);
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		shell.setLayout (fillLayout);

		Button button0 = new Button (shell, SWT.PUSH);
		button0.setText ("Bereich 0");

		Button button1 = new Button (shell, SWT.PUSH);
		button1.setText ("Bereich 1");

		Button button2 = new Button (shell, SWT.PUSH);
		button2.setText ("Bereich 2");

		Button button3 = new Button (shell, SWT.PUSH);
		button3.setText ("Bereich 3");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}