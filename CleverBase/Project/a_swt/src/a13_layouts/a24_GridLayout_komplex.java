package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a24_GridLayout_komplex {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		GridLayout layout = new GridLayout();

		layout.numColumns = 3;
		layout.makeColumnsEqualWidth = true;
		shell.setLayout(layout);

		// Button links oben
		GridData data = new GridData(GridData.FILL_BOTH);
		data.widthHint = 200;
		Button one = new Button(shell, SWT.PUSH);
		one.setText("eins");
		one.setLayoutData(data);

		// Composite f�r drei Buttons links oben
		Composite composite = new Composite(shell, SWT.NONE);
		composite.setBackground(display.getSystemColor(SWT.COLOR_BLUE));

		data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 2;
		composite.setLayoutData(data);
		layout = new GridLayout();
		layout.numColumns = 1;					// auf 2 aendern
		layout.marginHeight = 15;
		composite.setLayout(layout);

		// Button zwei
		data = new GridData(GridData.FILL_BOTH);
		Button zwei = new Button(composite, SWT.PUSH);
		zwei.setText("zwei");
		zwei.setLayoutData(data);

		// Button 3
		data = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
		Button drei = new Button(composite, SWT.PUSH);
		drei.setText("drei");
		drei.setLayoutData(data);

		// Button 4
		data = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		Button vier = new Button(composite, SWT.PUSH);
		vier.setText("vier");
		vier.setLayoutData(data);

		// Langer Button unten
		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		data.horizontalSpan = 3;
		data.heightHint = 100;					//hoehe button
		Button fuenf = new Button(shell, SWT.PUSH);
		fuenf.setText("fuenf");
		fuenf.setLayoutData(data);				// auskommentieren

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
