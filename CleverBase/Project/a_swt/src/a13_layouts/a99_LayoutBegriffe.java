package a13_layouts;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a99_LayoutBegriffe {
	Display display = new Display();
	Shell shell = new Shell(display);

	public a99_LayoutBegriffe() {
		init();
		
//		Composite composite;
		
		System.out.println("Bounds: " + shell.getBounds());
		System.out.println("Client Area: " + shell.getClientArea());
		
		shell.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
		
		RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.HORIZONTAL;
		rowLayout.wrap = true;
		
		rowLayout.marginLeft = 15;
		rowLayout.marginTop = 5;
		rowLayout.marginRight = 15;
		rowLayout.marginBottom = 10;
		
		rowLayout.spacing = 8;
		
		
		shell.setLayout(rowLayout);
		
		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("Button 1");
		
		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Das ist Button 2");
		
		Button button3 = new Button(shell, SWT.PUSH);
		button3.setText("3");
		


		shell.pack();
		shell.open();

		System.out.println("button1: " + button1.getBounds());
		System.out.println("button2: " + button2.getBounds());
		System.out.println("button3: " + button3.getBounds());

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

	private void init() {

	}

	public static void main(String[] args) {
		new a99_LayoutBegriffe();
	}
}
