package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a25_form_Layout4 {

	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);

		Label label = new Label(shell, SWT.WRAP);
		label.setText("Das it ein langer Text, der umschlaegt, wenn das Fenster verkleinert wird.");

		List list = new List(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		list.setItems(new String[] { "Auswahl 1", "Auswahl 2" });

		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("Ok");
		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Abbruch");

		final int X = 14, Y = 14;   //Abstaende
		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = X;
		formLayout.marginHeight = Y;
		shell.setLayout(formLayout);

		//Berechnung des Labelumschlages
		Point size = label.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		final FormData labelData = new FormData(size.x, SWT.DEFAULT);
		labelData.left = new FormAttachment(0, 0);
		labelData.right = new FormAttachment(100, 0);
		label.setLayoutData(labelData);


		shell.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event e) {
				Rectangle rect = shell.getClientArea();
				labelData.width = rect.width - X * 2;
				shell.layout();
			}
		});

		FormData button2Data = new FormData();
		button2Data.right = new FormAttachment(100, -X);
		button2Data.bottom = new FormAttachment(100, 0);
		button2.setLayoutData(button2Data);

		FormData button1Data = new FormData();
		button1Data.right = new FormAttachment(button2, -X);
		button1Data.bottom = new FormAttachment(100, 0);
		button1.setLayoutData(button1Data);

		FormData listData = new FormData();
		listData.left = new FormAttachment(0, 0);
		listData.right = new FormAttachment(100, 0);
		listData.top = new FormAttachment(label, Y);
		listData.bottom = new FormAttachment(button2, -Y);
		list.setLayoutData(listData);

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
