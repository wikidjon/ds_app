package a13_layouts;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a24_Grid_Layout {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);
		GridLayout gridLayout = new GridLayout ();
		gridLayout.numColumns = 4;   							//4!!!
		shell.setLayout (gridLayout);

		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;    /// auskommentieren !!!
		gridData.grabExcessHorizontalSpace = true;

		Label label1 = new Label(shell, SWT.BORDER);
		label1.setText("Name1");
		label1.setLayoutData(gridData);

		Label label2 = new Label(shell, SWT.BORDER);
		label2.setText("Name2");
		label2.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));

		Label label3 = new Label(shell, SWT.BORDER);
		label3.setText("Name3");
		label3.setSize(100,20);
		//label3.setLocation(230,230);    // Auswirkung ???

		Button button0 = new Button (shell, SWT.PUSH);
		button0.setText ("button0");

		Button button1 = new Button (shell, SWT.PUSH);
		button1.setText ("button1");
		//		button1.setLayoutData(gridData);

		Button button2 = new Button (shell, SWT.PUSH);
		button2.setText ("button2");

		Button button3 = new Button (shell, SWT.PUSH);
		button3.setText ("button3");
		button3.setLayoutData(gridData);

		Button button4 = new Button (shell, SWT.PUSH);
		button4.setText ("button4");

		Button button5 = new Button (shell, SWT.PUSH);
		button5.setText ("button5");

		Button button6 = new Button (shell, SWT.PUSH);
		button6.setText ("button6");

		Button button7 = new Button (shell, SWT.PUSH);
		button7.setText ("button7");

		Button button8 = new Button (shell, SWT.PUSH);
		button8.setText ("button8");

		Button button9 = new Button (shell, SWT.PUSH);
		button9.setText ("button9");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}