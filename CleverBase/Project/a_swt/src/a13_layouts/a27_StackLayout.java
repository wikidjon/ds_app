package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a27_StackLayout {

	public static void main(String[] args) {
		new a27_StackLayout();
	}
	Display display = new Display();

	Shell shell = new Shell(this.display);

	final Button[] buttons = new Button[3];

	public a27_StackLayout() {
		final StackLayout stackLayout = new StackLayout();
		this.shell.setLayout(stackLayout);

		for(int i=0; i<this.buttons.length; i++) {
			this.buttons[i] = new Button(this.shell, SWT.NULL);
			this.buttons[i].setText("Button " + i);

			this.buttons[i].addSelectionListener(new SelectionListener() {
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}

				@Override
				public void widgetSelected(SelectionEvent e) {
					Button naechsterButton = null;
					for(int i=0; i<a27_StackLayout.this.buttons.length; i++) {
						if(a27_StackLayout.this.buttons[i] == e.widget) {
							if(i == a27_StackLayout.this.buttons.length - 1) {
								naechsterButton = a27_StackLayout.this.buttons[0];
							} else {
								naechsterButton = a27_StackLayout.this.buttons[i+1];
							}
						}
					}
					stackLayout.topControl = naechsterButton;
					a27_StackLayout.this.shell.layout();
				}
			});
		}

		stackLayout.topControl = this.buttons[0];

		this.shell.setSize(200, 100);
		this.shell.open();

		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				this.display.sleep();
			}
		}
		this.display.dispose();
	}
}
