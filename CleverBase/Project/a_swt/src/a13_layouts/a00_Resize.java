package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a00_Resize {

	public static void main(String[] args) {
		new a00_Resize();
	}
	Display display = new Display();
	Shell shell = new Shell(this.display);

	int count = 0;

	public a00_Resize() {
		this.shell.setLayout(new RowLayout());

		final Composite composite = new Composite(this.shell, SWT.BORDER);
		composite.setLayout(new RowLayout());
		composite.setBackground(this.display.getSystemColor(SWT.COLOR_YELLOW));
		composite.addControlListener(new ControlListener() {
			@Override
			public void controlMoved(ControlEvent e) {

			}

			@Override
			public void controlResized(ControlEvent e) {
				System.out.println("Composite ge�ndert.");
			}
		});

		Button buttonAdd = new Button(this.shell, SWT.PUSH);
		buttonAdd.setText("Neuer Button");
		buttonAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button button = new Button(composite, SWT.PUSH);
				button.setText("Button " + (a00_Resize.this.count++));
				composite.layout(true);
				composite.pack();
				//a00_Resize.this.shell.layout(true);
				//a00_Resize.this.shell.pack(true);
			}
		});


		// shell.pack();
		this.shell.setSize(450, 100);
		this.shell.open();

		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				this.display.sleep();
			}
		}
		this.display.dispose();
	}

}
