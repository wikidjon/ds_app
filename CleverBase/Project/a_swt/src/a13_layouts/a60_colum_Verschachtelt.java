package a13_layouts;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a60_colum_Verschachtelt {
	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);

		final a24_gridlay a = new a24_gridlay();
		a.gridlay_test(display, shell);

/**/		for (int i = 0; i < 4; i++) {
			Label sep1 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
			sep1.setBounds(10, 60, 400, 40);
		}
/**/
		final a24_gridlay b = new a24_gridlay();
		b.gridlay_test(display, shell);

		for (int i = 0; i < 4; i++) {
			Label sep2 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL
					| SWT.SHADOW_IN);
			sep2.setBounds(30, 60, 100, 20);
		}

		final a24_gridlay c = new a24_gridlay();
		c.gridlay_test(display, shell);
		
		for (int i = 0; i < 4; i++) {
			Label sep2 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL
					| SWT.SHADOW_IN);
			sep2.setBounds(30, 60, 100, 20);
		}

		Button button0 = new Button(shell, SWT.PUSH);
		button0.setText("beenden");

		button0.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				shell.pack();
				shell.open();
			}
		});

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}