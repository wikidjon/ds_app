package a13_layouts;

// Groesse bleibt unveraendert
// Anordnung der Elemente der Reihe nach
// Bsp: entfernen Button 3 !!!

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class a24_Grid_Layout1 {
	public static void main (String [] args) {

		Display display = new Display();
		Shell shell = new Shell(display);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 5;    //!!!!!!!!!!!!

		shell.setLayout(gridLayout);

		new Label(shell, SWT.PUSH).setText("Label 1");
		new Button(shell, SWT.PUSH).setText("B 2");
		new Button(shell, SWT.PUSH).setText("Button 3");

		List list = new List(shell, SWT.BORDER);
		list.add("Eintrag 1");
		list.add("Eintrag 2");
		list.add("Eintrag 3");
		list.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		new Button(shell, SWT.PUSH).setText("B4");
		new Button(shell, SWT.PUSH).setText("Button 5");
		new Label(shell, SWT.PUSH).setText("     Label    2");
		new Button(shell, SWT.PUSH).setText("B       3");
		new Label(shell, SWT.PUSH).setText("Label 3");
		new Button(shell, SWT.PUSH).setText("B 4");
		new Label(shell, SWT.PUSH).setText("Label 4");
		new Button(shell, SWT.PUSH).setText("B 5");

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}



