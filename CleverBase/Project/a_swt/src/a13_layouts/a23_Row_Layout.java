package a13_layouts;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a23_Row_Layout {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);

		RowLayout rowLayout = new RowLayout ();
		rowLayout.marginLeft = 10;
		rowLayout.marginRight = 10;
		rowLayout.marginTop = 10;
		rowLayout.marginBottom = 10;
		rowLayout.spacing = 10;					// 100
		shell.setLayout (rowLayout);

		Button button0 = new Button (shell, SWT.PUSH);
		button0.setText ("Button0");

		Button button1 = new Button (shell, SWT.PUSH);
		button1.setText ("Button1");

		Button button2 = new Button (shell, SWT.PUSH);
		button2.setText ("Button2");

		Button button3 = new Button (shell, SWT.PUSH);
		button3.setText ("Button3");

		Button button4 = new Button (shell, SWT.PUSH);
		button4.setText ("Button4");

		Button button5 = new Button (shell, SWT.PUSH);
		button5.setText ("Button5");

		Button button6 = new Button (shell, SWT.PUSH);
		button6.setText ("Button6");

		Button button7 = new Button (shell, SWT.PUSH);
		button7.setText ("Button7");

		Button button8 = new Button (shell, SWT.PUSH);
		button8.setText ("Button8");

		Button button9 = new Button (shell, SWT.PUSH);
		button9.setText ("Button9");

		Button button10 = new Button (shell, SWT.PUSH);
		button10.setText ("Button10");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}