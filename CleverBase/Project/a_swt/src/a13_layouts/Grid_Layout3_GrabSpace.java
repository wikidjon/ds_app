package a13_layouts;


import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class Grid_Layout3_GrabSpace {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;

		GridLayout gridLayout1 = new GridLayout();
		gridLayout1.numColumns = 4;

		shell.setLayout(gridLayout1);			// auskommentieren !!

		// gridData2 wird spaeter (composite1) gebraucht
		GridData gridData2 = new GridData();
		gridData2.grabExcessVerticalSpace = false;
		gridData2.grabExcessHorizontalSpace = false;
		//gridData2.verticalAlignment = GridData.FILL;
		gridData2.horizontalAlignment = GridData.FILL;

		FillLayout filllayout = new FillLayout(SWT.VERTICAL);
		shell.setLayout(filllayout);

		GridData gridData1 = new GridData();
		/******/  gridData1.horizontalAlignment = GridData.FILL;
		gridData1.grabExcessHorizontalSpace = false;

		Composite composite1 = new Composite(shell,SWT.NONE);
		FillLayout fillLayout = new FillLayout ();
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		//		composite1.setLayout (fillLayout);
		//		composite1.setLayoutData(gridData1);

		GridData gridData4 = new GridData();
		gridData4.grabExcessVerticalSpace = false;
		gridData4.grabExcessHorizontalSpace = false;
		//		gridData4.verticalAlignment = GridData.FILL;
		gridData4.horizontalAlignment = GridData.FILL;
		composite1.setLayoutData(gridData2);
		//		composite1.setLayout (gridLayout1);
		//		composite1.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL ));
		composite1.setBackground(new Color(display,255,0,0));

		Composite composite2 = new Composite(shell,SWT.NONE);
		composite2.setLayout (gridLayout1);
		composite2.setBackground(new Color(display,0,255,0));

		Composite composite3 = new Composite(shell,SWT.NONE);
		composite3.setBackground(new Color(display,0,0,255));


		Label label = new Label(composite2, SWT.BORDER);
		label.setText("Label");

		GridData gridData3 = new GridData();	// Groessenveraenderung
		gridData3.widthHint = 60;				// Breite Label
		gridData3.heightHint = 20;				// Hoehe
		//		label.setLayoutData(gridData3);

		Text text = new Text(composite2, SWT.SINGLE | SWT.BORDER);
		text.setText("Text");

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		text.setLayoutData(gridData);

		Button button = new Button(composite2, SWT.PUSH);
		button.setText("Button");

		GridData gridData5 = new GridData();
		gridData5.grabExcessVerticalSpace = false;
		gridData5.grabExcessHorizontalSpace = false;
		//gridData5.verticalAlignment = GridData.FILL;
		gridData5.horizontalAlignment = GridData.FILL;
		button.setLayoutData(gridData5);

		//	shell.setSize(300, 80);
		//	shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
