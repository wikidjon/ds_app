package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a26_form_LayoutComplex {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		FormLayout layout = new FormLayout();
		shell.setLayout(layout);

		Button eins = new Button(shell, SWT.PUSH);
		eins.setText("Button1");

		FormData data = new FormData();
		data.top = new FormAttachment(0, 5);
		data.left = new FormAttachment(0, 5);
		data.bottom = new FormAttachment(50, -5);
		data.right = new FormAttachment(50, -5);
		eins.setLayoutData(data);

		Composite composite = new Composite(shell, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		composite.setLayout(gridLayout);

		Button zwei = new Button(composite, SWT.PUSH);
		zwei.setText("Button2");
		GridData gridData = new GridData(GridData.FILL_BOTH);
		zwei.setLayoutData(gridData);

		Button drei = new Button(composite, SWT.PUSH);
		drei.setText("Button3");
		gridData = new GridData(GridData.FILL_BOTH);
		drei.setLayoutData(gridData);

		Button vier = new Button(composite, SWT.PUSH);
		vier.setText("Button4");
		gridData = new GridData(GridData.FILL_BOTH);
		vier.setLayoutData(gridData);

		data = new FormData();
		data.top = new FormAttachment(0, 5);
		data.left = new FormAttachment(eins, 5);
		data.bottom = new FormAttachment(50, -5);
		data.right = new FormAttachment(100, -5);
		composite.setLayoutData(data);

		Button fuenf = new Button(shell, SWT.PUSH);
		fuenf.setText("Button5");
		data = new FormData();
		data.top = new FormAttachment(eins, 5);
		data.left = new FormAttachment(90, 5);
		data.bottom = new FormAttachment(100, -5);
		data.right = new FormAttachment(100, -5);
		fuenf.setLayoutData(data);

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
