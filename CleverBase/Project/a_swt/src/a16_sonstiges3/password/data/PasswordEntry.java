package a16_sonstiges3.password.data;

// Daten fuer einen einfachen Passworteintrag

public class PasswordEntry {
  private String category;
  private String name;
  private String userId;
  private String password;

  public PasswordEntry() {
    this(null, null, null, null);
  }

  public PasswordEntry(String newCategory, String newName, String newUserId,
      String newPassword) {
    setCategory(newCategory);
    setName(newName);
    setUserId(newUserId);
    setPassword(newPassword);
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void clone(PasswordEntry entry) {
    setCategory(entry.getCategory());
    setName(entry.getName());
    setUserId(entry.getUserId());
    setPassword(entry.getPassword());
  }
}
