package a98_graphic_context;
/*
 * (C) 2002 by 3plus4software GbR. All rights reserved.
 */
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class TwoDDemo {

    public static void main(String[] arguments) {
        Display display = new Display();

        // create some graphics with Java's 2D API
        BufferedImage awtImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) awtImage.getGraphics();
        g.setPaint(
            new GradientPaint(
                0,
                0,
                new Color(255, 0, 0, 255),
                0,
                100,
                new Color(0, 255, 0, 128)));
        g.fillRect(0, 0, 100, 100);
        g.setPaint(
            new GradientPaint(
                0,
                0,
                new Color(0, 0, 255, 128),
                100,
                0,
                new Color(255, 255, 0, 0)));
        g.fillRect(0, 0, 100, 100);
        g.dispose();

        // convert AWT image to SWT image
        int[] pixels = (int[]) awtImage.getRaster().getDataElements(0, 0, 100, 100, null);
        ImageData imageData =
            new ImageData(100, 100, 24, new PaletteData(0xff0000, 0xff00, 0xff));
        imageData.setPixels(0, 0, pixels.length, pixels, 0);
        final Image swtImage = new Image(display, imageData);

        Shell shell = new Shell(display);
        shell.addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent e) {
                Rectangle rect = ((Shell) e.widget).getClientArea();
                e.gc.drawImage(swtImage, rect.x, rect.y);
            }
        });
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();
    }
}