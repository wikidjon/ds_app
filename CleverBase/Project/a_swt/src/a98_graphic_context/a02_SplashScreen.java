package a98_graphic_context;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class a02_SplashScreen {

public static void main(String[] args) {
	final Display display = new Display();
	final int [] zaehler = new int [] {5};
	final Image image = new Image(display, "src/images/splash.jpg");
	final Shell splash = new Shell(SWT.ON_TOP);

	final ProgressBar bar = new ProgressBar(splash, SWT.NONE);
	bar.setMaximum(zaehler[0]);
	Label label = new Label(splash, SWT.NONE);
	label.setImage(image);
	FormLayout layout = new FormLayout();
	splash.setLayout(layout);
	
	// Positionierung des Progressbar
	FormData progressData = new FormData ();
	progressData.left = new FormAttachment (0, 5);
	progressData.right = new FormAttachment (100, -5);
	progressData.bottom = new FormAttachment (100, -5);
	bar.setLayoutData(progressData);
	splash.pack();
	splash.setSize(200,200);
	
	//Berechnung Mitte Bildschirm
	Rectangle splashRect = splash.getBounds();
	Rectangle displayRect = display.getBounds();
	int x = (displayRect.width - splashRect.width) / 2;
	int y = (displayRect.height - splashRect.height) / 2;
	splash.setLocation(x, y);
	splash.open();

	Shell shell  = new Shell(display);
	shell.setSize (300, 300);
/*	shell.addListener(SWT.Close, new Listener() {
		public void handleEvent (Event e) {
			--zaehler[0];
		}
	});
*/
	//	 shell = new Shell(display);
//	shell.setSize (300, 300);
	
//*********************************************	
	display.asyncExec(new Runnable() {
		public void run() {
//			Shell [] shells = new Shell[zaehler[0]];
			for (int i=0; i<zaehler[0]; i++) {
//				shells [i] = new Shell(display);
				//shells [i].setMaximized (true);
//				shells [i].setSize (300, 300);
/*				shells [i].addListener(SWT.Close, new Listener() {
					public void handleEvent (Event e) {
						--zaehler[0];
					}
				});*/
				bar.setSelection(i+1);
				try {Thread.sleep(1000);} catch (Throwable e) {}
			}
			splash.close();
			image.dispose();
			Shell shell = new Shell(display);
			shell.setMaximized (true);
//			for (int i=0; i<zaehler[0]; i++) {
				shell.open();
//			}
		}
	}); 
//	****/
	while (zaehler [0] != 0) {
//	splash.open();
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose();
}

}
