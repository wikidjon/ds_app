package a17_slider;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;

public class a17_Scales {
	Display display = new Display();
	Shell shell = new Shell(display);
	
	public static void main(String[] args) {
		new a17_Scales();
	}

	public a17_Scales() {
		Scale scaleH = new Scale(shell, SWT.NULL);
		Scale scaleV = new Scale(shell, SWT.VERTICAL);
		
		scaleH.setBounds(0, 0, 100, 50);
		scaleV.setBounds(0, 50, 50, 100);
		
		System.out.println("Min: " + scaleH.getMinimum());
		System.out.println("Max: " + scaleH.getMaximum());

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

}
