package a17_slider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

public class a17_SliderDemo {

	public static Display display;
	static Text value = null;


	public static void main(String[] args) {
		display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300,300);
		shell.setText("Slider Demo");
		
		value = new Text(shell, SWT.BORDER | SWT.SINGLE);
		value.setBounds(10, 150, 100, 50);
		value.setEditable(false);
		value.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));

		final Slider slider1 = new Slider(shell, SWT.HORIZONTAL);
		slider1.setBounds(10,10,200,20);
		slider1.setSelection(10);
		slider1.setMaximum(60);
		slider1.setMinimum(0);
		slider1.setThumb(30);	//Achtung: Breite des Reglers hat Auswirkung auf Werte!!!

		final Scale scale1 = new Scale(shell, SWT.HORIZONTAL);
		scale1.setBounds(10,60,200,40);
		scale1.setMinimum(0);
		scale1.setMaximum(500);
		scale1.setSelection(100);
		scale1.setPageIncrement(50);
		
		slider1.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				int perspectiveValue = slider1.getMaximum() - slider1.getSelection() + slider1.getMinimum() - slider1.getThumb();
				value.setText("Sliderwert: " + perspectiveValue);
			}
		});

		scale1.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				int perspectiveValue = scale1.getMaximum() - scale1.getSelection() + scale1.getMinimum(); //- scale1.getThumb();
				value.setText("Scalewert: " + perspectiveValue);
			}
		});
	
		shell.open();

		while(!shell.isDisposed()){
		if(!display.readAndDispatch())
			display.sleep();
		}
	}
}
