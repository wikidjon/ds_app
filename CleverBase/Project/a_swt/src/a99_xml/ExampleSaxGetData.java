package a99_xml;
// ExampleSaxGetData.java

import java.io.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

public class ExampleSaxGetData
{
  public static void main( String[] args )
  {
    if( args.length != 5 )
    {
      System.err.println( "Usage:" );
      System.err.println( "java ExampleSaxGetData <XmlFile> "
                        + "<MainElem> <ChildElem> <FindVal> <DataElem>" );
      System.err.println( "Example:" );
      System.err.println( "java ExampleSaxGetData MyXmlFile.xml "
                        + "Button Title \"Mein dritter Button\" Comment" );
      System.exit( 1 );
    }
    try {
      MyDefaultHandlerImpl handler = new MyDefaultHandlerImpl();
      handler.args = args;
      SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
      saxParser.parse( new File( args[0] ), handler );
      if( 0 < handler.sbResult.length() )
      {
        System.out.println( "Result string in child element '<"
                            + args[4] + ">':" );
        System.out.println( handler.sbResult );
        System.exit( 0 );
      }
      else
      {
        System.out.println( "No fitting element found." );
        System.exit( 2 );
      }
    } catch( Throwable t ) {
      t.printStackTrace();
      System.exit( 3 );
    }
  }
}

// ---- SAX DefaultHandler ----
class MyDefaultHandlerImpl extends DefaultHandler
{
  public  String[]     args;
  public  StringBuffer sbResult = new StringBuffer();
  private int          iState   = 0;

  public void startElement( String namespaceURI,
                            String localName,
                            String qName,
                            Attributes attrs )
  throws SAXException
  {
    if( 5 <= iState )  return;
    String eName = ( "".equals( localName ) ) ? qName : localName;
    if( null != eName )
    {
      if( eName.equals( args[1] ) )                 // <MainElem>
        iState = 1;
      if( 1 == iState && eName.equals( args[2] ) )  // <ChildElem>
        iState = 2;
      if( 4 == iState )
        sbResult.append( "<" + eName + ">" );
      if( 3 == iState && eName.equals( args[4] ) )  // <DataElem>
        iState = 4;
    }
  }

  public void endElement( String namespaceURI,
                          String localName,
                          String qName )
  throws SAXException
  {
    if( 5 <= iState )  return;
    String eName = ( "".equals( localName ) ) ? qName : localName;
    if( null != eName )
    {
      if( eName.equals( args[1] ) )                 // <MainElem>
        iState = 0;
      if( 2 == iState && eName.equals( args[2] ) )  // <ChildElem>
        iState = 1;
      if( 4 == iState && eName.equals( args[4] ) )  // <DataElem>
        iState = 5;
      if( 4 == iState )
        sbResult.append( "</" + eName + ">" );
    }
  }

  public void characters( char[] buf, int offset, int len )
  throws SAXException
  {
    if( 5 <= iState )  return;
    String s = new String( buf, offset, len );
    if( 2 == iState && s.equals( args[3] ) )        // <FindVal>
      iState = 3;
    if( 4 == iState )
      sbResult.append( s );
  }
}