package a99_xml;

import java.io.File;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

public class ExampleJdomWrite
{
	public static void main( String[] args )
	{
		if( args.length != 1 )
		{
			System.err.println( "Usage: java ExampleJdomWrite MyXmlFile.xml" );
			System.exit( 1 );
		}
		try {
			// ---- Read XML file ----
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build( new File( args[0] ) );
			// ---- Modify XML data ----
			//  ... do anything with XML data
			// ---- Write XML file ----
			XMLOutputter fmt = new XMLOutputter();
			//     fmt.setIndent( "  " );    // only for nicer formatting
			//     fmt.setNewlines( true );  // only for nicer formatting
			fmt.output( doc, System.out );
		} catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
}