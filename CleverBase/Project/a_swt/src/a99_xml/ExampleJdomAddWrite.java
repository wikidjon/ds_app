package a99_xml;
// ExampleJdomAddWrite.java

import java.util.*;
import java.io.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class ExampleJdomAddWrite
{
  @SuppressWarnings("unchecked")
public static void main( String[] args )
  {
    if( args.length != 6 )
    {
      System.err.println( "Attention:" );
      System.err.println( "jdom.jar must be added to classpath." );
      System.err.println( "Usage:" );
      System.err.println( "java ExampleJdomAddWrite <XmlFile> <NewFile>"
                            + " <MainElem> <Child> <FindVal> <New>" );
      System.err.println( "Example:" );
      System.err.println( "java -classpath .;jdom.jar ExampleJdomAddWrite"
                            + " MyXmlFile.xml NewXmlFile.xml Button Title"
                            + " \"Mein dritter Button\" \"Mein neuer Button\"" );
      System.exit( 1 );
    }
    try {
      // ---- Read XML file ----
      SAXBuilder builder = new SAXBuilder();
      Document doc = builder.build( new File( args[0] ) );
      // ---- Modify XML data ----
      Element root = doc.getRootElement();
      List<Element> listMainElements = root.getChildren( args[2] );  // <MainElem>
      for( int i=0; i<listMainElements.size(); i++ )
      {
        // Find searched element with given text:
        Element elMain  = (Element)(listMainElements.get( i ));
        if( null == elMain )  continue;
        Element elChild = elMain.getChild( args[3] );       // <Child>
        if( null == elChild )  continue;
        String s = elChild.getTextTrim();
        if( null == s || !s.equals( args[4] ) )  continue;  // <FindVal>
        // Add new element at correct position:
        Element elNew = new Element( args[2] );             // <MainElem>
        elNew.addContent( (new Element( args[3] )).addContent( args[5] ) );
        listMainElements.add( i, elNew );                   // <New>
        // ---- Write XML file ----
        XMLOutputter outp = new XMLOutputter();
//        outp.setIndent( "  " );
 //       outp.setNewlines( true );
        outp.output( doc, new FileOutputStream( new File( args[1] ) ) );
        break;                                              // <NewFile>
      }
    } catch( Exception ex ) {
      ex.printStackTrace();
    }
  }
}