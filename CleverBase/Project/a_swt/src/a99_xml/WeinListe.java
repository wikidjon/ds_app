package a99_xml;

import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class WeinListe extends DefaultHandler
{
	boolean istJahrgang = false;
	String jahrgang;
	boolean istLage = false;
	String lage;
	static String pfad = System.getProperty("user.dir");

	public static void main(String[] args)	
      {
		try
		{
                  DefaultHandler handler = new WeinListe();
                  SAXParser parser = 
					SAXParserFactory.newInstance().newSAXParser();
                  parser.parse(new File(pfad + "/sax/weine.xml"),
                  handler);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void startElement(String uri, String localName,
			   	String qName, Attributes attributes)
		throws SAXException	
      {
		if ("Wein".equals(qName))
		{
			lage = "";
			jahrgang = "";
		}
		if ("Lage".equals(qName))
		{
			istLage = true;
		}
		if ("Jahrgang".equals(qName))
		{
			istJahrgang = true;
		}
	}

	public void characters(char[] ch, int start, int length)
				throws SAXException	
      {
		if (istLage)
		{
			lage = new String(ch, start, length);
		}
		if (istJahrgang)
		{
			jahrgang = new String(ch, start, length);
		}
	}	

	public void endElement(String uri, String localName, 
			String qName)	throws SAXException	
      {
		if ("Wein".equals(qName))
		{
			System.out.println( lage +"\t"+	jahrgang );
		}
		if ("Lage".equals(qName))
		{
			istLage = false;
		}
		if ("Jahrgang".equals(qName))
		{
			istJahrgang = false;
		}
	}
}