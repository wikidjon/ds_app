package a33_dialoge;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

class DialogExample1 extends Dialog {
	public static void main(String[] argv) {
		new DialogExample1(new Shell());
	}

	DialogExample1(Shell parent) {
		super(parent);
	}

	public String open() {
		Shell parent = getParent();
		Shell dialog = new Shell(parent, SWT.DIALOG_TRIM
				| SWT.APPLICATION_MODAL);
		dialog.setSize(100, 100);
		dialog.setText("Java Programmierung");
		dialog.open();
		Display display = parent.getDisplay();
		while (!dialog.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return "... nach dem Dialog";
	}
}

public class ShellDialogExample {
	public static void main(String[] argv) {
		new ShellDialogExample();
	}

	ShellDialogExample() {
		Display d = new Display();
		Shell s = new Shell(d);
		s.setSize(300, 300);
		s.open();
		DialogExample de = new DialogExample(s);
		String result = de.open();
		System.out.println(result);
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();

	}

}

