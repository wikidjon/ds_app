package a08_browser;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationAdapter;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.StatusTextEvent;
import org.eclipse.swt.browser.StatusTextListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

// Funktioniert mit localhost - Webserver starten
public class a08_BrowserDemo {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout(3, false));

		// Widgets (Pfeilbuttons)n anlegen
		Button zurueck = new Button(shell, SWT.ARROW | SWT.LEFT);
		Button vorwaerts = new Button(shell, SWT.ARROW | SWT.RIGHT);

		final Text address = new Text(shell, SWT.BORDER); // final wegen Nutzung in innerer Klasse
		address.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		final Browser browser = new Browser(shell, SWT.NONE);
		// f�llt Fenster mit Browser
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 3; // ueber Pfeile und Textfeld
		browser.setLayoutData(gd);

		final Label status = new Label(shell, SWT.NONE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		status.setLayoutData(gd);

		address.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
				browser.setUrl(address.getText());
			}
		});
		browser.addStatusTextListener(new StatusTextListener() {
			public void changed(StatusTextEvent event) {
				status.setText(event.text);
			}
		});
		browser.addLocationListener(new LocationAdapter() {
			public void changed(LocationEvent event) {
				address.setText(event.location);
			}
		});
		zurueck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				browser.back();
			}
		});
		vorwaerts.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				browser.forward();
			}
		});

		browser.setUrl("localhost");

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
