package a07_group;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.*;

public class a07_Gruppe2 {

	public static void main(String[] args) {
		Display d = new Display();
		Shell s = new Shell(d);
		s.setSize(300, 300);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Shell Composite3 Beispiel");
		final a07_Gruppe1 ge1 = new a07_Gruppe1(s, SWT.SHADOW_ETCHED_IN,
				"Gruppe Eins");
		ge1.setLocation(10, 10);
		final a07_Gruppe1 ge2 = new a07_Gruppe1(s, SWT.SHADOW_ETCHED_IN,
				"Option Gruppe Zwei");
		ge2.setLocation(100, 100);
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch())
				d.sleep();
		}
		d.dispose();
	}
}
