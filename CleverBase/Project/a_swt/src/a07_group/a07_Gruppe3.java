package a07_group;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
//import org.eclipse.swt.graphics.Color;


public class a07_Gruppe3 {

	public static Display display;

	public static void main(String[] args) {
		display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300,300);
		shell.setText("Gruppen Demo");

		//Group group1 = new Group(shell, SWT.BORDER);
		Group group1 = new Group(shell, SWT.NONE); 
		group1.setBounds(30,30,200,200);
		group1.setText("Gruppe1");

		Button button = new Button(group1, SWT.PUSH);
		button.setBounds(10,20,80,20);
		button.setText("...in Gruppe1");

		Label label = new Label(group1, SWT.NONE);
		label.setBounds(10,50,80,20);
		label.setText("Label Gruppe1");

		Group group2 = new Group (group1, SWT.NONE);
		group2.setBounds(10,100,150,50);
//		group2.setBackground(new Color(display,150,20,233));
		group2.setText("Gruppe innerhalb der Gruppe");

		Button button2 = new Button(group2, SWT.PUSH);
		button2.setBounds(10,20,50,20);
		button2.setText("Text..");

		shell.open();

		while(!shell.isDisposed()){
		if(!display.readAndDispatch())
			display.sleep();
		}
	}
}
