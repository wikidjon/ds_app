package a22_sonstiges;

import java.io.*;


public class serialisieren {
  public static void main( String[] argv ) throws Exception
  {
	serialisieren a = new serialisieren();
	try{
		a.lese("serialisieren.java");
	}
	catch (IOException e){System.out.println("Fehler");};
  }

public Object lese(String dateiName) throws Exception
{
    Object o = null;
    FileInputStream fIn = new FileInputStream(dateiName);
    ObjectInputStream oIn = new ObjectInputStream(fIn);

    o = oIn.readObject();
    oIn.close();
    return o;
}

public static void schreibe(Object o, String dateiName) throws Exception
{
    FileOutputStream fOut = new FileOutputStream(dateiName);
    ObjectOutputStream oOut = new ObjectOutputStream(fOut);

    oOut.writeObject(o);
    oOut.flush();
    oOut.close();
}
}