package a22_sonstiges.ch02;

import java.io.File;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * 
 */
public class SimpleFileBrowser {
	public static void main(String[] args) {
		new SimpleFileBrowser();
	}
	Display display = new Display();

	Shell shell = new Shell(this.display);

	ImageRegistry imageRegistry;

	Table table;

	public SimpleFileBrowser() {
		init();

		this.shell.pack();
		this.shell.open();
		//textUser.forceFocus();

		// Set up the event loop.
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				// If no more entries in event queue
				this.display.sleep();
			}
		}

		this.display.dispose();
	}



	public void displayFiles(String[] files) {
		// Removes all existing table items.
		this.table.removeAll();

		for (int i = 0; files != null && i < files.length; i++) {
			TableItem item = new TableItem(this.table, SWT.NULL);
			Image image = null;

			if (files[i].endsWith(".jar")) {
				image = this.imageRegistry.get("jar");
			} else {
				image = this.imageRegistry.get("default");
			}

			item.setImage(image);
			item.setText(files[i]);
		}

	}

	public void displayFiles2(String[] files) {

		// Disposes all of the images used by the table items first.
		TableItem[] items = this.table.getItems();
		for(int i=0; items != null && i < items.length; i++) {
			if(items[i].getImage() != null) {
				items[i].getImage().dispose();
			}
		}

		// Removes all existing table items.
		this.table.removeAll();

		for (int i = 0; files != null && i < files.length; i++) {
			TableItem item = new TableItem(this.table, SWT.NULL);
			Image image = null;

			if (files[i].endsWith(".jar")) {
				image = new Image(this.display, "src/imgages/cut.gif");
			} else {
				image = new Image(this.display, "src/imgages/filesave.png");
			}

			item.setImage(image);
			item.setText(files[i]);
		}
	}

	private void init() {
		this.shell.setText("File Browser");
		this.shell.setLayout(new GridLayout(1, true));

		Button button = new Button(this.shell, SWT.PUSH);
		button.setText("Browse ...");
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(SimpleFileBrowser.this.shell, SWT.NULL);
				String path = dialog.open();
				if (path != null) {

					File file = new File(path);
					if (file.isFile()) {
						displayFiles(new String[] { file.toString()});
					} else {
						displayFiles(file.list());
					}

				}
			}
		});

		GridData gd = new GridData(GridData.FILL_BOTH);

		this.table = new Table(this.shell, SWT.MULTI);
		this.table.setLayoutData(gd);

		// creates an image registry and adds icons to the image registry.
		this.imageRegistry = new ImageRegistry();

		ImageDescriptor defaultIcon =
				ImageDescriptor.createFromFile(null, "src/icons/cancel.png");
		this.imageRegistry.put("default", defaultIcon);

		ImageDescriptor jarIcon =
				ImageDescriptor.createFromFile(null, "src/imgages/copy.gif");
		this.imageRegistry.put("jar", jarIcon);
	}
}
