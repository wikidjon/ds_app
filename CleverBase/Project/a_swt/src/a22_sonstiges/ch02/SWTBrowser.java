package a22_sonstiges.ch02;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.CloseWindowListener;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.browser.OpenWindowListener;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.browser.StatusTextEvent;
import org.eclipse.swt.browser.StatusTextListener;
import org.eclipse.swt.browser.TitleEvent;
import org.eclipse.swt.browser.TitleListener;
import org.eclipse.swt.browser.VisibilityWindowListener;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;

/**
 * 
 */
public class SWTBrowser {
	static void initialize(final Display display, Browser browser) {
		browser.addOpenWindowListener(new OpenWindowListener() {
			@Override
			public void open(WindowEvent event) {
				Shell shell = new Shell(display);
				shell.setText("New Window");
				shell.setLayout(new FillLayout());
				Browser browser = new Browser(shell, SWT.NONE);
				initialize(display, browser);
				event.browser = browser;
			}
		});
		browser.addVisibilityWindowListener(new VisibilityWindowListener() {
			@Override
			public void hide(WindowEvent event) {
				Browser browser = (Browser) event.widget;
				Shell shell = browser.getShell();
				shell.setVisible(false);
			}
			@Override
			public void show(WindowEvent event) {
				Browser browser = (Browser) event.widget;
				Shell shell = browser.getShell();
				if (event.location != null) {
					shell.setLocation(event.location);
				}
				if (event.size != null) {
					Point size = event.size;
					shell.setSize(shell.computeSize(size.x, size.y));
				}
				shell.open();
			}
		});
		browser.addCloseWindowListener(new CloseWindowListener() {
			@Override
			public void close(WindowEvent event) {
				Browser browser = (Browser) event.widget;
				Shell shell = browser.getShell();
				shell.close();
			}
		});
	}
	public static void main(String[] args) {
		new SWTBrowser();
	}

	Display display = new Display();

	Shell shell = new Shell(this.display);

	Text textLocation;

	Browser browser;

	Label labelStatus;

	public SWTBrowser() {
		this.shell.setLayout(new GridLayout());

		ToolBar toolBar = new ToolBar(this.shell, SWT.FLAT | SWT.RIGHT);
		final ToolBarManager manager = new ToolBarManager(toolBar);

		Composite compositeLocation = new Composite(this.shell, SWT.NULL);
		compositeLocation.setLayout(new GridLayout(3, false));
		compositeLocation.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label labelAddress = new Label(compositeLocation, SWT.NULL);
		labelAddress.setText("Address");

		this.textLocation = new Text(compositeLocation, SWT.SINGLE | SWT.BORDER);
		this.textLocation.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Button buttonGo = new Button(compositeLocation, SWT.NULL);
		buttonGo.setImage(new Image(this.shell.getDisplay(), "src/icons/folder.gif"));
		//		images/splash.jpg
		//	Image icon = new Image(shell.getDisplay(), "icons/folder.gif");

		this.browser = new Browser(this.shell, SWT.BORDER);
		this.browser.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite compositeStatus = new Composite(this.shell, SWT.NULL);
		compositeStatus.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		compositeStatus.setLayout(new GridLayout(2, false));

		this.labelStatus = new Label(compositeStatus, SWT.NULL);
		this.labelStatus.setText("Ready");
		this.labelStatus.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		final ProgressBar progressBar =
				new ProgressBar(compositeStatus, SWT.SMOOTH);

		Listener openURLListener = new Listener() {
			@Override
			public void handleEvent(Event event) {
				SWTBrowser.this.browser.setUrl(SWTBrowser.this.textLocation.getText());
			}
		};

		buttonGo.addListener(SWT.Selection, openURLListener);
		this.textLocation.addListener(SWT.DefaultSelection, openURLListener);

		// Adds tool bar items using actions.
		final Action actionBackward =
				new Action(
						"&Backword",
						ImageDescriptor.createFromFile(
								null,
								"src/icons/folder.gif")) {
			@Override
			public void run() {
				SWTBrowser.this.browser.back();
			}
		};
		actionBackward.setEnabled(false); // action is disabled at start up.

		final Action actionForward =
				new Action(
						"&Forward",
						ImageDescriptor.createFromFile(null,"src/icons/folder.gif")) {
			@Override
			public void run() {
				SWTBrowser.this.browser.forward();
			}
		};
		actionForward.setEnabled(false); // action is disabled at start up.

		Action actionStop =
				new Action(
						"&Stop",
						ImageDescriptor.createFromFile(null, "src/icons/folder.gif")) {
			@Override
			public void run() {
				SWTBrowser.this.browser.stop();
			}
		};

		Action actionRefresh =
				new Action(
						"&Refresh",
						ImageDescriptor.createFromFile(null,"src/icons/folder.gif")) {
			@Override
			public void run() {
				SWTBrowser.this.browser.refresh();
			}
		};

		Action actionHome =
				new Action(
						"&Home",
						ImageDescriptor.createFromFile(null, "src/icons/folder.gif")) {
			@Override
			public void run() {
				SWTBrowser.this.browser.setUrl("http://www.eclipse.org");
			}
		};

		manager.add(actionBackward);
		manager.add(actionForward);
		manager.add(actionStop);
		manager.add(actionRefresh);
		manager.add(actionHome);

		manager.update(true);
		toolBar.pack();

		this.browser.addLocationListener(new LocationListener() {
			@Override
			public void changed(LocationEvent event) {
				// Update tool bar items.
				actionBackward.setEnabled(SWTBrowser.this.browser.isBackEnabled());
				actionForward.setEnabled(SWTBrowser.this.browser.isForwardEnabled());
				manager.update(false);
			}

			@Override
			public void changing(LocationEvent event) {
				// Displays the new location in the text field.
				SWTBrowser.this.textLocation.setText(event.location);
			}
		});

		this.browser.addProgressListener(new ProgressListener() {
			@Override
			public void changed(ProgressEvent event) {
				progressBar.setMaximum(event.total);
				progressBar.setSelection(event.current);
			}

			@Override
			public void completed(ProgressEvent event) {
				progressBar.setSelection(0);
			}
		});

		this.browser.addStatusTextListener(new StatusTextListener() {
			@Override
			public void changed(StatusTextEvent event) {
				SWTBrowser.this.labelStatus.setText(event.text);
			}
		});

		this.browser.addTitleListener(new TitleListener() {
			@Override
			public void changed(TitleEvent event) {
				SWTBrowser.this.shell.setText(event.title + " - powered by SWT");
			}
		});

		initialize(this.display, this.browser);

		this.shell.setSize(500, 400);
		this.shell.open();
		//textUser.forceFocus();

		//browser.setText(
		//	"<html><body>" + "<h1>SWT &amp; JFace </h1>" + "</body/html>");

		// Set up the event loop.
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				// If no more entries in event queue
				this.display.sleep();
			}
		}

		this.display.dispose();
	}

	@SuppressWarnings("unused")
	private void init() {

	}
}
