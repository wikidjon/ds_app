package a22_sonstiges.ch02;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

/**
 * 
 */
public class TabFolderExample {
	public static void main(String[] args) {
		new TabFolderExample();
	}
	Display display = new Display();

	Shell shell = new Shell(this.display);

	Image icon = new Image(this.shell.getDisplay(), "src/icons/folder.gif");

	public TabFolderExample() {
		this.shell.setLayout(new FillLayout());

		final TabFolder tabFolder = new TabFolder(this.shell, SWT.BOTTOM);


		Button button = new Button(tabFolder, SWT.NULL);
		button.setText("This is a button.");

		TabItem tabItem1 = new TabItem(tabFolder, SWT.NULL);
		tabItem1.setText("item #1");
		tabItem1.setImage(this.icon);
		tabItem1.setControl(button);

		Text text = new Text(tabFolder, SWT.MULTI);
		text.setText("This is a text control.");

		TabItem tabItem2 = new TabItem(tabFolder, SWT.NULL);
		tabItem2.setText("item #2");
		tabItem2.setImage(this.icon);
		tabItem2.setControl(text);

		Label label = new Label(tabFolder, SWT.NULL);
		label.setText("This is a text lable.");

		TabItem tabItem3 = new TabItem(tabFolder, SWT.NULL);
		tabItem3.setText("item #3");
		tabItem3.setControl(label);

		tabFolder.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Selected item index = " + tabFolder.getSelectionIndex());
				System.out.println("Selected item = " + (tabFolder.getSelection() == null ? "null" : tabFolder.getSelection()[0].toString()));
			}
		});




		//tabFolder.setSelection(new TabItem[]{tabItem2, tabItem3});
		//tabFolder.setSelection(2);

		this.shell.setSize(400, 120);
		this.shell.open();
		//textUser.forceFocus();

		System.out.println(tabFolder.getSelectionIndex());

		// Set up the event loop.
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				// If no more entries in event queue
				this.display.sleep();
			}
		}

		this.display.dispose();
	}

	@SuppressWarnings("unused")
	private void init() {

	}
}
