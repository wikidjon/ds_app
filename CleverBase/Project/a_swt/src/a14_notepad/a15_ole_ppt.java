package a14_notepad;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.ole.win32.OLE;
import org.eclipse.swt.ole.win32.OleClientSite;
import org.eclipse.swt.ole.win32.OleFrame;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;


public class a15_ole_ppt {

	static OleClientSite clientSite;
	static OleFrame frame;
	static Shell shell;

	static void addFileMenu(OleFrame frame) {
		final Shell shell = frame.getShell();
		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);
		MenuItem fileMenu = new MenuItem(menuBar, SWT.CASCADE);
		fileMenu.setText("&Datei");
		Menu menuFile = new Menu(fileMenu);
		fileMenu.setMenu(menuFile);

		MenuItem menuFileOpen = new MenuItem(menuFile, SWT.CASCADE);
		menuFileOpen.setText("Oeffnen...");
		menuFileOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				fileOpen();
			}
		});

		MenuItem menuFileControl = new MenuItem(menuFile, SWT.CASCADE);
		menuFileControl.setText("Beenden");
		menuFileControl.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.dispose();
			}
		});

		frame.setFileMenus(new MenuItem[] { fileMenu });
		//		oleFrame = new OleFrame(panel, SWT.NONE);
	}

	static void fileOpen() {
		FileDialog dialog = new FileDialog(clientSite.getShell(), SWT.OPEN);
		dialog.setFilterExtensions(new String[] { "*.ppt", "*.pptx" });
		String fileName = dialog.open();
		if (fileName != null) {

			clientSite.dispose();
			clientSite = new OleClientSite(frame, SWT.NONE, "PowerPoint.Slide", new File(fileName));
			clientSite.doVerb(OLE.OLEIVERB_INPLACEACTIVATE);
			System.out.println(fileName);

		}//ppt.Document
	}


	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setText("PowerPoint Beispiel");
		shell.setLayout(new FillLayout());
		try {
			frame = new OleFrame(shell, SWT.NONE);
			clientSite  = new OleClientSite(frame, SWT.NONE, "PowerPoint.Slide");
			//			clientSite = new OleClientSite(frame, SWT.NONE, "PowerPoint.Slide", new File("c:/Lehre/c/ws11_12/C_v001a.pptx"));
			addFileMenu(frame);

		} catch (SWTError e) {
			System.out.println("ActiveX control kann nicht geoeffnet werden");
			return;
		}
		shell.setSize(800, 600);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}