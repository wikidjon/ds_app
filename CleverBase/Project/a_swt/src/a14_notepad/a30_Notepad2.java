package a14_notepad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

abstract class Anweisung {
	public abstract void run();

	public boolean isEnabled() {
		return true;
	}
}

abstract class Anwendung {

	protected class MenuBuilder {
		private Menu menu;

		private MenuBuilder(Menu menu) {
			this.menu = menu;
		}

		public MenuBuilder addItem(String text, int acc, Anweisung cmd) {
			return addItem(text, acc, SWT.PUSH, cmd);
		}

		public MenuBuilder addToggleItem(String text, int acc, Anweisung cmd) {
			return addItem(text, acc, SWT.CHECK, cmd);
		}

		private MenuBuilder addItem(String text, int acc, int style,
				Anweisung cmd) {
			MenuItem item = new MenuItem(menu, style);
			item.setText(text);
			item.setAccelerator(acc);
			item.setData(cmd);
			item.addSelectionListener(cmdListener);
			return this;
		}

		public MenuBuilder addSeparator() {
			new MenuItem(menu, SWT.SEPARATOR);
			return this;
		}
	}

	protected Display display;

	private SelectionListener cmdListener;
	private Map<String, Image> imagecache = new HashMap<String, Image>();

	public Anwendung() {
		display = new Display();

		cmdListener = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Object data = e.widget.getData();
				if (data instanceof Anweisung) {
					((Anweisung) data).run();
				}
			}
		};
	}

	public MenuBuilder createMenu(Menu menubar, String text) {
		final Menu menu = new Menu(menubar);
		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				int count = menu.getItemCount();
				for (int i = 0; i < count; i++) {
					MenuItem item = menu.getItem(i);
					Anweisung cmd = (Anweisung) item.getData();
					if (cmd != null) {
						item.setEnabled(cmd.isEnabled());
					}
				}
			}
		});
		MenuItem item = new MenuItem(menubar, SWT.CASCADE);
		item.setText(text);
		item.setMenu(menu);
		return new MenuBuilder(menu);
	}

	public ToolItem createToolItem(ToolBar toolbar, String imagename,
			Anweisung cmd) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(imagename));
		item.setData(cmd);
		item.addSelectionListener(cmdListener);
		return item;
	}

	public void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	public Image createImage(String name) {
		Image image = (Image) imagecache.get(name);
		if (image == null) {
			image = new Image(display,
					Anwendung.class.getResourceAsStream("/icons/" + name
							+ ".gif"));
			if (image != null) {
				imagecache.put(name, image);
			}
		}
		return image;
	}

	public void dispose() {
		Iterator<Image> i = imagecache.values().iterator();
		while (i.hasNext()) {
			((Image) i.next()).dispose();
		}
	}
}

public class a30_Notepad2 extends Anwendung {

	private Shell shell;
	private ToolBar toolbar;
	private Text text;
	private Font font;

	private String lastPath;
	private String savePath;

	public static void main(String[] arguments) {
		new a30_Notepad2().run();
	}

	public a30_Notepad2() {
		super();
		createShell();
		createMenu();
		createToolbar();
		createTextWidget(false);
		fileNew();
	}

	private void createShell() {
		shell = new Shell(display);
		shell.setImage(createImage("logo"));
		GridLayout l = new GridLayout();
		l.marginWidth = 0;
		l.marginHeight = 0;
		l.horizontalSpacing = 0;
		l.verticalSpacing = 0;
		shell.setLayout(l);
	}

	private void createMenu() {
		Menu menubar = new Menu(shell, SWT.BAR);

		createMenu(menubar, "&Datei")
				.addItem("&Neu\tStrg+N", SWT.CTRL + 'N', cmdFileNew)
				.addItem("�&ffnen...\tStrg+O", SWT.CTRL + 'O', cmdFileOpen)
				.addItem("&Speichern\tStrg+S", SWT.CTRL + 'S', cmdFileSave)
				.addItem("Speichern &unter...", 0, cmdFileSaveAs)
				.addSeparator().addItem("&Beenden", 0, cmdFileExit);

		createMenu(menubar, "&Bearbeiten")
				.addItem("&R�ckgangig\tStrg+Z", SWT.CTRL + 'Z', cmdEditUndo)
				.addSeparator()
				.addItem("&Ausschneiden\tStrg+X", SWT.CTRL + 'X', cmdEditCut)
				.addItem("&Kopieren\tStrg+C", SWT.CTRL + 'C', cmdEditCopy)
				.addItem("E&inf�gen\tStrg+V", SWT.CTRL + 'V', cmdEditPaste);

		createMenu(menubar, "F&ormat").addToggleItem("&Zeichenumbruch", 0,
				cmdFmtWrap).addItem("&Schriftart...", 0, cmdFmtFont);

		shell.setMenuBar(menubar);
	}

	private void createToolbar() {
		toolbar = new ToolBar(shell, SWT.FLAT);
		toolbar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
		GridData data = new GridData();
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		toolbar.setLayoutData(data);

		createToolItem(toolbar, "new", cmdFileNew);
		createToolItem(toolbar, "open", cmdFileOpen);
		createToolItem(toolbar, "save", cmdFileSave);
		createSeparator(toolbar);
		createToolItem(toolbar, "undo", cmdEditUndo);
		createSeparator(toolbar);
		createToolItem(toolbar, "cut", cmdEditCut);
		createToolItem(toolbar, "copy", cmdEditCopy);
		createToolItem(toolbar, "paste", cmdEditPaste);
	}

	private void createTextWidget(boolean wrap) {
		String oldText;
		if (text != null) {
			oldText = text.getText();
			text.dispose();
		} else {
			oldText = "";

			font = new Font(display, "Lucida Console", 10, SWT.NORMAL);
		}

		int wrapStyle = wrap ? SWT.WRAP | SWT.V_SCROLL : SWT.H_SCROLL
				| SWT.V_SCROLL;
		text = new Text(shell, SWT.BORDER | SWT.MULTI | wrapStyle);
		text.setFont(font);
		text.setText(oldText);

		GridData data = new GridData();
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.FILL;
		text.setLayoutData(data);

		shell.layout();

		text.setFocus();
	}

	Anweisung cmdFileNew = new Anweisung() {
		public void run() {
			fileNew();
		}
	};

	Anweisung cmdFileOpen = new Anweisung() {
		public void run() {
			fileOpen();
		}
	};

	Anweisung cmdFileSave = new Anweisung() {
		public void run() {
			fileSave(false);
		}
	};

	Anweisung cmdFileSaveAs = new Anweisung() {
		public void run() {
			fileSave(true);
		}
	};

	Anweisung cmdFileExit = new Anweisung() {
		public void run() {
			dispose();
		}
	};

	Anweisung cmdEditUndo = new Anweisung() {
		public void run() {
			editUndo();
		}
	};

	Anweisung cmdEditCut = new Anweisung() {
		public void run() {
			text.cut();
		}

		public boolean isEnabled() {
			return text.getSelectionCount() > 0;
		}
	};

	Anweisung cmdEditCopy = new Anweisung() {
		public void run() {
			text.copy();
		}

		public boolean isEnabled() {
			return text.getSelectionCount() > 0;
		}
	};

	Anweisung cmdEditPaste = new Anweisung() {
		public void run() {
			text.paste();
		}

		public boolean isEnabled() {
			Clipboard c = new Clipboard(display);
			Object o = c.getContents(TextTransfer.getInstance());
			c.dispose();
			return o != null;
		}
	};

	Anweisung cmdFmtWrap = new Anweisung() {
		public void run() {
			createTextWidget((text.getStyle() & SWT.WRAP) == 0);
		}
	};

	Anweisung cmdFmtFont = new Anweisung() {
		@SuppressWarnings("deprecation")
		public void run() {
			FontDialog d = new FontDialog(shell);
			d.setFontData(font.getFontData()[0]);
			FontData fd = d.open();
			if (fd != null) {
				font.dispose();
				font = new Font(display, fd);
				text.setFont(font);
			}
		}
	};

	private void fileNew() {
		text.setText("");
		shell.setText("Unbenannt - Notepad");
		savePath = null;
	}

	private void fileOpen() {
		String path = requestFile(SWT.OPEN);
		if (path != null) {
			loadFile(path);
			lastPath = path;
			savePath = path;
			shell.setText(new File(path).getName() + " - Notepad");
		}
	}

	private String requestFile(int style) {
		FileDialog d = new FileDialog(shell, style);
		d.setFilterPath(lastPath);
		d.setFilterNames(new String[] { "Textdateien (*.txt)", "Alle Dateien" });
		d.setFilterExtensions(new String[] { "*.txt", "*" });
		return d.open();
	}

	private void loadFile(String path) {
		try {
			StringBuffer sb = new StringBuffer(16000);
			BufferedReader r = new BufferedReader(new FileReader(path));
			String line;
			while ((line = r.readLine()) != null) {
				sb.append(line).append(Text.DELIMITER);
			}
			r.close();
			text.setText(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void fileSave(boolean ask) {
		if (ask || savePath == null) {
			String path = requestFile(SWT.SAVE);
			if (path != null) {
				if (path.indexOf('.') == -1) {
					path += ".txt";
				}
				saveFile(path);
				lastPath = path;
				savePath = path;
				shell.setText(new File(path).getName() + " - Notepad");
			}
		} else {
			saveFile(savePath);
		}
	}

	private void saveFile(String path) {
		try {
			PrintWriter w = new PrintWriter(new FileWriter(path));
			String s = text.getText();
			int i = s.indexOf(Text.DELIMITER);
			int j = 0;
			while (i != -1) {
				w.println(s.substring(j, i));
				j = i + Text.DELIMITER.length();
				i = s.indexOf(Text.DELIMITER, j);
			}
			w.print(s.substring(j));
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// private void fileExit() {
	// shell.close();
	// }
	private void editUndo() {
		MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
		mb.setText("Notepad");
		mb.setMessage("Funktion ist nicht implementiert");
		mb.open();
	}

	public void run() {
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		dispose();
	}

	public void dispose() {
		super.dispose();
		font.dispose();
		display.dispose();
	}

}
