package ds_files;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;

public class bubble extends miscCode implements main.include{
	
	static Composite bubble = new Composite(canvas, SWT.NONE);
	static ArrayList<Integer> list = new ArrayList<Integer>();
	static int speed = 500;
	public static void setCodeCol(String text){
		codeCol.setText(text);
	}
	
	public static void aboutBox(String heading, String msg){
		MessageBox aboutMessageBox = new MessageBox(shell, SWT.ABORT );
		aboutMessageBox.setText(heading);
		aboutMessageBox.setMessage(msg);
		@SuppressWarnings("unused")
		int result = aboutMessageBox.open();
	}
	
	public static void resetguilist(){
	}
	
	public static void resetlinkedlist(){
	}
	
	public static Control make(){
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.marginHeight = 15;
		gridLayout.marginWidth = 15;
		
		bubble.setLayout(gridLayout);
		GridData gd_canvas = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
		gd_canvas.heightHint = 447;
		gd_canvas.widthHint = 439;
		bubble.setLayoutData(gd_canvas);
		
		Canvas canvas_1 = new Canvas(bubble, SWT.NONE);
		canvas_1.setLayout(new GridLayout(1, false));
		canvas_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Label lblBubbleSort = new Label(canvas_1, SWT.NONE);
		Font boldFont = new Font(lblBubbleSort.getDisplay(), new FontData("Arial", 15, SWT.BOLD));
		lblBubbleSort.setFont(boldFont);
		lblBubbleSort.setText("Bubble Sort");
		
		Label lblUseTheText = new Label(canvas_1, SWT.NONE);
		lblUseTheText.setText("Use the text box below to insert elements and then press swap");
		
		new Label(canvas_1, SWT.NONE);
		
		Label lblInsertAt = new Label(canvas_1, SWT.NONE);
		lblInsertAt.setText("Insert value");
		
		text_1 = new Text(canvas_1, SWT.BORDER);
		text_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button btnInsert = new Button(canvas_1, SWT.NONE);
		btnInsert.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnInsert.setText("Insert");
		new Label(canvas_1, SWT.NONE);
		
		
		Label lblSpeed = new Label(canvas_1, SWT.NONE);
		lblSpeed.setText("Adjust speed in milliseconds");
		
		Button btnSpeed_1 = new Button(canvas_1, SWT.NONE);
		btnSpeed_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnSpeed_1.setText("Interval 500 milliseconds");
		
		Button btnSpeed_2 = new Button(canvas_1, SWT.NONE);
		btnSpeed_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnSpeed_2.setText("Interval 1000 milliseconds");
		
		Button btnSpeed_3 = new Button(canvas_1, SWT.NONE);
		btnSpeed_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnSpeed_3.setText("Interval 2000 milliseconds");
		
		new Label(canvas_1, SWT.NONE);
		
		Label lblNos = new Label(canvas_1, SWT.NONE);
		Font medFont = new Font(lblBubbleSort.getDisplay(), new FontData("Arial", 12, SWT.BOLD));
		GridData gd_lblNos = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		lblNos.setFont(medFont);
		lblNos.setLayoutData(gd_lblNos);
		lblNos.setText("Nos.");
		
		Button btnSwap = new Button(canvas_1, SWT.NONE);
		btnSwap.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnSwap.setText("Swap");
		
		Label lblSwapPossible = new Label(canvas_1, SWT.NONE);
		lblSwapPossible.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblSwapPossible.setText("Swap Possible");
		

		btnSpeed_1.addListener(SWT.Selection, Event -> {
			speed = 500;
			lblSwapPossible.setText("Speed set");
		}); btnSpeed_2.addListener(SWT.Selection, Event -> {
			speed = 1000;
			lblSwapPossible.setText("Speed set");
		}); btnSpeed_3.addListener(SWT.Selection, Event -> {
			speed = 2000;
			lblSwapPossible.setText("Speed set");
		});
		
		btnInsert.addListener(SWT.Selection, Event -> {
			
			if(isInteger(text_1.getText(), 10)){
				list.add(Integer.parseInt(text_1.getText()));
				String listString = "";
				
				for (Integer s : list)
				{
					listString += s + "  ";
				}
			
				lblNos.setText(listString);
			
				setCodeCol(bubble_insert);
			} else {
				aboutBox("Alert","Invalid Entry");
			}
		});
		
		btnSwap.addListener(SWT.Selection, Event -> {
			 
			if(list.isEmpty()){
				aboutBox("List empty","The list is empty, please add entries");
			} else {
				
			Integer[] num = list.toArray(new Integer[list.size()]);
			 int j;
			 int passes = 0;
		     boolean flag = true;   // set flag to true to begin first pass
		     int temp;   //holding variable
		     
		     while ( flag )
		     {
		            flag= false;    //set flag to false awaiting a possible swap
		            for( j=0;  j < num.length -1;  j++ )
		            {
		            		passes++;
		                   if ( num[ j ] < num[j+1] )   // change to > for ascending sort
		                   {
		                           temp = num[ j ];                //swap elements
		                           num[ j ] = num[ j+1 ];
		                           num[ j+1 ] = temp;
		                           
		                           try {
		                        	    Thread.sleep(speed);                 //1000 milliseconds is one second.
		                        	} catch(InterruptedException ex) {
		                        	    Thread.currentThread().interrupt();
		                        	}
		                           
		                           list = new ArrayList<Integer>(Arrays.asList(num));
		              		     String listString = "";
		              				
		              				for (Integer s : list)
		              				{
		              				    listString += s + "  ";
		              				}
		              				
		              				lblNos.setText(listString);
		                           
		                          flag = true;              //shows a swap occurred  
		                  } 
		            } 
		      } 
		     lblSwapPossible.setText("Bubble sort completed with " + passes + " passes");
		     setCodeCol(bubble_swap);
			}
		});
		
		return bubble;
	}
	
	public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }
}
