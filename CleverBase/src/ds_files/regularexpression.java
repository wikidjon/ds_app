package ds_files;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class regularexpression extends miscCode implements main.include{
	
	public static void clearCodeCol(){
		codeCol.setText(codeCol_msg);
	}
	
	//Next function
	
	public static Control make(){
		
		Composite regex = new Composite(canvas, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 20;
		gridLayout.marginWidth = 20;
		regex.setLayout(gridLayout);
		
		Label lblRegularExpressions = new Label(regex, SWT.NONE);
//		regex.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
		Font boldFont = new Font(lblRegularExpressions.getDisplay(), new FontData("Arial", 15, SWT.BOLD));
		lblRegularExpressions.setFont(boldFont);
		lblRegularExpressions.setText("Regular Expressions");
		
		Label lblTheFirstText = new Label(regex, SWT.NONE);
		lblTheFirstText.setText("The first text box takes regex as input and second, the string to be checked.");
//		lblTheFirstText.setBackground(display.getSystemColor(SWT.COLOR_BLACK));
//		lblTheFirstText.setForeground(display.getSystemColor(SWT.COLOR_YELLOW));
		new Label(regex, SWT.NONE);
		
		
		Font textFont = new Font(lblRegularExpressions.getDisplay(), new FontData("Arial", 12, SWT.NONE));
		
		Label lbl1= new Label(regex, SWT.NONE);
		lbl1.setText("Input regex");
		
		Text text_1 = new Text(regex, SWT.BORDER);
		text_1.setFont(textFont);
		text_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lbl2 = new Label(regex, SWT.NONE);
		lbl2.setText("Input String to be tested");
		
		Text text_2 = new Text(regex, SWT.BORDER);
		text_2.setFont(textFont);
		text_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		GridData gd_btnCheck = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_btnCheck.widthHint = 128;
		
		Button btnCheck = new Button(regex, SWT.NONE);
		btnCheck.setLayoutData(gd_btnCheck);
		btnCheck.setText("Check");
		new Label(regex, SWT.NONE);
		
		GridData gd_lblResult = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblResult.widthHint = 179;
		
		Label lblResult = new Label(regex, SWT.NONE);
		lblResult.setLayoutData(gd_lblResult);
		
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.heightHint = 428;
		gd_text.widthHint = 200;
//		text.setLayoutData(gd_text);
		
		btnCheck.addListener(SWT.Selection, event -> {
			
			setCode(regex_check_code);
			
			String tobematched = text_2.getText();
			String pattern = text_1.getText();
			
			Pattern result = Pattern.compile(pattern);
			Matcher match = result.matcher(tobematched);
			
			if(match.matches()){
				lblResult.setText("Das ist richtig!");
//				setStatus("Your string matches!",false);
			} else {
				lblResult.setText("Nein!");
//				setStatus("Your string doesn't match!",true);
			}
		});
		
		return regex;
		
	}
}
