
package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;

public interface codeStrings {
	String regex_check_code = "check.addListener(SWT.Selection, event -> {" + "\n" + "\n" + 
			"String tobematched = line.getText();" + "\n" +  "\n" +
			"String pattern = regex_exp.getText();" + "\n" +  "\n" +
			"Pattern result = Pattern.compile(pattern);" + "\n" + "\n" +
			"Matcher match = result.matcher(tobematched);" + "\n" + "\n" +
			"if(match.find()){" + "\n" + "\n" +
				"result_label.setText('Das ist richtig!');" + "\n" + "\n" +
			"} else {" + "\n" + "\n" +
				"result_label.setText('Nein!');" + "\n" + "\n" +
			"}" + "\n" + "\n" +
		"});";
	
	String ll_init = "";
	
	String ll_add = "public void add(Object data) {"

			+ "\n\n" + "if (head == null) {"
			+ "\n" + "head = new Node(data);"
			+ "\n" + "}"
			+ "\n\n" + "Node LLTemp = new Node(data);"
			+ "\n" + "Node LLCurrent = head;"
			+ "\n\n" + "if (LLCurrent != null) {"
			+ "\n" + "while (LLCurrent.getNext() != null) {"
			+ "\n" + "LLCurrent = LLCurrent.getNext();"
			+ "\n" 	+ "}"
			+ "\n" + "LLCurrent.setNext(LLTemp);"
			+ "\n" + "}"
			+ "\n\n" + "incrementCounter();"
			+ "\n\n" + "}";
	
	String ll_delete = "public boolean remove(int index) {"
			+ "\n\n" + "if (index < 1 || index > size())"
			+ "\n" + "return false;"
			+ "\n\n" + "Node LLCurrent = head;"
			+ "\n" + "if (head != null) {"
			+ "\n\n" + "for (int i = 0; i < index; i++) {"
			+ "\n" + "if (LLCurrent.getNext() == null)"
			+ "\n" + "return false;LLCurrent = LLCurrent.getNext();"
			+ "\n" + "}"
			+ "\n\n" + "LLCurrent.setNext(LLCurrent.getNext().getNext());"
			+ "\n" + "decrementCounter();"
			+ "\n" + "return true;"
			+ "\n" + "}"
			+ "\n" + "return false;"
			+ "\n\n" + "}";
	
	String llinit = "LLLinkedList LLlist = new LLLinkedList()";
	
	String dll_insert = "public void insertFirst(int dd) {"
			+ "\n" +"Link newLink = new Link(dd);"
			+ "\n" +"if (isEmpty()){"
			+ "\n" +"last = newLink;"
			+ "\n" +"}else{"
			+ "\n" +"first.previous = newLink;"
			+ "\n" +"}"
			+ "\n" +"newLink.next = first;"
			+ "\n" +"first = newLink;"
			+ "\n" +"}";
	
	String bubble_insert = "btnInsert.addListener(SWT.Selection, Event -> {"
			+ "\n" + "list.add(Integer.parseInt(text_1.getText()));"
			+ "\n" + "String listString = '';"
			+ "\n" + "for (Integer s : list)"
			+ "\n" + "{"
			+ "\n" + "listString += s + '  ';"
			+ "\n" + "}"
			+ "\n" + "lblNos.setText(listString);"
			+ "\n" + "});";
	
	String bubble_swap = "btnSwap.addListener(SWT.Selection, Event -> {"
			+ "\n" + "Integer[] num = list.toArray(new Integer[list.size()]);"
			+ "\n" + "int j;"
			+ "\n" + "int passes = 0;"
			+ "\n" + "boolean flag = true;   // set flag to true to begin first pass"
			+ "\n" + "int temp;   //holding variable"
			+ "\n" + "while ( flag )"
			+ "\n" + "{"
			+ "\n" + "flag= false;    //set flag to false awaiting a possible swap"
			+ "\n" + "for( j=0;  j < num.length -1;  j++ )"
			+ "\n" + "{"
			+ "\n" + "passes++;"
			+ "\n" + "if ( num[ j ] < num[j+1] )   // change to > for ascending sort"
			+ "\n" + "{"
			+ "\n" + "temp = num[ j ];                //swap elements"
			+ "\n" + "num[ j ] = num[ j+1 ];"
			+ "\n" + "num[ j+1 ] = temp;"
			+ "\n" + "try {"
			+ "\n" + "Thread.sleep(500);                 //1000 milliseconds is one second."
			+ "\n" + "} catch(InterruptedException ex) {"
			+ "\n" + "Thread.currentThread().interrupt();"
			+ "\n" + "}"
			+ "\n" + "list = new ArrayList<Integer>(Arrays.asList(num));"
			+ "\n" + "String listString = '';"
			+ "\n" + "for (Integer s : list)"
			+ "\n" + "{"
			+ "\n" + "listString += s + '  ';"
			+ "\n" 		+ "}"
			+ "\n" 		+ "lblNos.setText(listString);"
			+ "\n" 		+ "flag = true;              //shows a swap occurred  "
			+ "\n" 		+ "} "
			+ "\n" 		+ "} "
			+ "\n" 		+ "} "
			+ "\n" 		+ "lblSwapPossible.setText('Bubble sort completed with ' + passes + ' passes');"
			+ "\n" 				+ "});";
	
	String hash_insert = "btnInsert.addListener(SWT.Selection, Event -> {"
			+ "\n" + "String key = text_1.getText();"
			+ "\n" + "String value = text_2.getText();"
			+ "\n" + "hm.put(key, value);"
			+ "\n" + ""
			+ "\n" + "setCodeCol(hash_insert);"
			+ "\n" + "});";
	
	String hash_retrieve_by_key = "btnRetrieveByKey.addListener(SWT.Selection, Event -> {"
			+ "\n" + "String key = text_3.getText();"
			+ "\n" + "String value = (String) hm.get(key);"
			+ "\n" + "lblHashResult.setText(value);"
			+ "\n" + "setCodeCol(hash_retrieve_by_key);"
			+ "\n" + "});";
	
	String hash_retrieve_all = "btnRetrieveall.addListener(SWT.Selection, Event -> {"
			+ "\n" + "String key = text_3.getText();"
			+ "\n" + "String result = '';"
			+ "\n" + "Set set = hm.entrySet();Iterator iter = set.iterator();"
			+ "\n" + "while(iter.hasNext()){"
			+ "\n" + "Map.Entry me = (Map.Entry)iter.next();"
			+ "\n" + "result += 'Key: ' + me.getKey() + ' / Value: ' + me.getValue() + '\n';}"
			+ "\n" + "lblHashResult.setText(result);setCodeCol(hash_retrieve_all);"
			+ "\n" + "});";
}
