package main;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

public interface include extends codeStrings{
	
	
	
	Display display = Display.getDefault();
	Shell shell = new Shell(display);
	Menu menu = new Menu(shell, SWT.BAR);
	
	//Resize enabled menu with 3 cols
	SashForm form = new SashForm(shell,SWT.HORIZONTAL);
	Composite child1 = new Composite(form,SWT.NONE);
	Composite child2 = new Composite(form,SWT.NONE);
	
	//Central canvas
	Canvas canvas = new Canvas(child2, SWT.BORDER);
//	Label lblRegularExpressions = new Label(canvas, SWT.NONE);
//	Label lblTheFirstText = new Label(canvas, SWT.NONE);
	
	
	
//	Button btnCheck = new Button(canvas, SWT.NONE);
//	Label lblResult = new Label(canvas, SWT.NONE);
	
	Composite child3 = new Composite(form,SWT.NONE);
	
	//Left tree menu
	Tree tree = new Tree(child1, SWT.BORDER);
	Canvas leftpanel_menu = new Canvas(child1, SWT.NONE);
	
	Canvas rightpanel_menu = new Canvas(child3, SWT.NONE);
	Button copy = new Button(rightpanel_menu, SWT.PUSH);
	Button popout = new Button(rightpanel_menu, SWT.PUSH);
	Button text_dim = new Button(rightpanel_menu, SWT.PUSH);
	Button text_enl = new Button(rightpanel_menu, SWT.PUSH);
	
	
	Text codeCol = new Text(child3, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL);
	
	//menubar
	MenuItem mntmHelp = new MenuItem(menu, SWT.NONE);
	
	StackLayout stack = new StackLayout();
	
	
	
//	static Text text = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL);
	
	//String collection
	public String[] file_menu = {"New","Open","Exit"};
	public String[] help_menu = {"Help Manual","About Clever","Check for updates"};
	public String[] edit_menu = {"Copy Source Code","Copy","Paste"};
	public String[] tree_menu = {"Regular Expression","Linked List","Doubly Linked List","Bubble Sort","Quick Sort","Hash","Insertion Sort"};
	public String[] options = {"regularexpression","linkedlist","doublylinkedlist"};
	public String aboutClever= "Project done at Hochschule Merseburg by Jonathan Vijayakumar";
	public String appIconPath = "e:\\edu\\Merseburg\\Project\\apple_256.ico";
	public String appTitle = "SuperDSTeacher";
	public String codeCol_msg = "This is where the source code for your clicks appears";
	
}
