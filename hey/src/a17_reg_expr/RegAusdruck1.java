package a17_reg_expr;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class RegAusdruck1 {
	public static void main(String[] args) {

		// Pattern und Matcher
		Pattern p = Pattern.compile("'.*'");	
		// p ist eine Repr�sentation eines regul�ren Ausdruckes.
		// der Punkt steht f�r ein beliebiges Zeichen, und der folgende Stern 
		// ist ein Quantifizierer, der wahllos viele beliebige Zeichen erlaubt.
		
		Matcher m = p.matcher("'Hallo Merseburg'");
		// m ist ein Tool, das match operationen auf Strings mit Pattern ausf�hrt.
		
		boolean b = m.matches();
		// die statische Funktion Pattern.matches() und die Objektmethode matches() 
		// der Klasse String testen, ob ein regul�rer Ausdruck eine Zeichenfolge komplett beschreibt.

		System.out.println("1.: " + b);

		System.out.println("---------------------------------");
		
		//Testet, ob ZK in Hochkommas eingeschlossen ist 
		System.out.println("2.: " + Pattern.matches("'.*'", "'Hallo Merseburg'"));

		// die Objektmethode matches() der Klasse String 
		System.out.println("3.: " + "'Hallo Merseburg'".matches("'.*'"));
	
		// der Punkt im regul�ren Ausdruck steht f�r ein beliebiges Zeichen, 
		// und der folgende Stern ist ein Quantifizierer, der wahllos viele beliebige Zeichen erlaubt.
		System.out.println("4.: " + Pattern.matches("'.+'", "''"));			// LZ einbauen
		System.out.println("5.: " + Pattern.matches("'.*'", "Hallo Welt"));  // einf HK vorn weg
		System.out.println("6.: " + Pattern.matches("'.*'", "'Hallo Welt"));

		System.out.println("---------------------------------");
		System.out.println("7.: " + Pattern.matches("0", "0")); 					// true
		System.out.println("8.: " + Pattern.matches("0", "1")); 					// false
		System.out.println("9.: " + Pattern.matches("0", "0 0")); 				// false
		System.out.println("10.: " + Pattern.matches("0*", "0000")); 		// true
		System.out.println("11.: " + Pattern.matches("0*", "01")); 			// false
		System.out.println("12.: " + Pattern.matches("0\\*", "01")); 		// false
		System.out.println("13.: " + Pattern.matches("0\\*", "0*")); 			// true

		System.out.println("---------------------------------");
		System.out.println("14.: " + Pattern.matches("[01]*", "0")); 								// true
		System.out.println("15.: " + Pattern.matches("[01]*", "01001")); 					// true
		System.out.println("16.: " + Pattern.matches("[0123456789]*", "112")); 		// true

		System.out.println("---------------------------------");
		System.out.println("17.: " + Pattern.matches("\\d*", "112")); 							// true		warum steht hier \\... ???
		System.out.println("18.: " + Pattern.matches("\\d*", "112a")); 						// false
		System.out.println("19.: " + Pattern.matches("\\d*.", "112a")); 						// true
		System.out.println("20.: " + Pattern.matches(".\\d*.", "x112a")); 						// true

		// Finden und nicht matchen
		System.out.println("--------Finden und nicht matchen-------------------------");
		String s = "'Mancher bekommt in AlgoDat am 20.2. eine 1, mancher eine 2 und vielleicht auch jemand eine 5.0";
		Matcher matcher = Pattern.compile("\\d+").matcher(s);
		while (matcher.find())
			System.out.printf("%s an Postion [%d,%d]%n", matcher.group(), matcher.start(), matcher.end());

		// Gierige und nicht gierige Operatoren
		System.out.println("---------Gierige und nicht gierige Operatoren-<b>.*</b>----------------");
		String string = "Echt <b>fette</b> AlgoDat <b>Pruefung!!</b>!";
		Pattern pattern1 = Pattern.compile("<b>.*</b>");							//l�ngste m�gliche Zeichenkette
		Matcher matcher1 = pattern1.matcher(string);
		while (matcher1.find())
			System.out.println(matcher1.group());

		System.out.println("---------Gierige und nicht gierige Operatoren-<b>.*?</b>----------------");
		Pattern pattern2 = Pattern.compile("<b>.*?</b>");
		Matcher matcher2 = pattern2.matcher("Echt <b>fette</b>. AlgoDat <b>Pruefung!!</b>!");
		while (matcher2.find())
			System.out.println(matcher2.group());

		System.out.println("---------------------------------");
		String pattern = "\\d{8,9}[\\d|x|X]";							//mindestens 9 h�chsten 10 Zeichen
		String s1 = "HS Merseburg: 38984266789011, Regul�re Ausdr�cke: 38972134941";
		// mindestens 9 hoechstens 10 stellen
		for (MatchResult r : findMatches(pattern, s1))			// MatchResult ->Zugriff auf Ergebnisse 
			System.out.println(r.group() + " von " + r.start() + " bis " + r.end() );
	}

	static Iterable<MatchResult> findMatches(String pattern, CharSequence s) {
		List<MatchResult> results = new ArrayList<MatchResult>();
		for (Matcher m = Pattern.compile(pattern).matcher(s); m.find();)
			results.add(m.toMatchResult());
		return results;
	}

}



/**************************************
// Quantifizierer und Wiederholungen
final Pattern p1 = Pattern.compile("[\\w|-]+@\\w[\\w|-]*\\.[a-z]{2,3}");
**************************************/