package a17_reg_expr;

import java.util.*;

public class Systeminfos
{
  public static void main(String[] args)
  {
    // kurze Variante
    Properties p = System.getProperties();
    p.list(System.out);
    
    // zweite Variante mit formatierter Ausgabe
    System.out.println("\n\nListe verf�gbarer Properties:\n");
    for (Enumeration<?> e = System.getProperties().propertyNames(); e.hasMoreElements();)
    {
      String pName = e.nextElement().toString();
      System.out.printf("%-36s%s%n", pName + ":",
                        System.getProperty(pName));
    }
  }
}
