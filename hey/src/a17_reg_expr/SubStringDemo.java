package a17_reg_expr;
 /**
   * separieren des domainnamens wie "@hs-merseburg.de"
   * aus einer e-mail adresse"
   */   
  public class SubStringDemo {

	  public static void main(String[] args) {
        String s = "uwe.schroeter@hs-merseburg.de"; 		
        int IndexOf = s.indexOf("@"); 
        String domainName = s.substring(IndexOf); 
        System.out.println("Der Domainname Ihrer e-mail Adresse ist: "+domainName);
     }
  }

         