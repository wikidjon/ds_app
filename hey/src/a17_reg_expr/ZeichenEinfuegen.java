package a17_reg_expr;

import java.util.regex.*;

public class ZeichenEinfuegen {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("([A-Z]|[a-z])+");
		Matcher m = p.matcher("Dies ist ein Text zum Testen des Programms");
		StringBuffer sb = new StringBuffer();
		while (m.find())
			m.appendReplacement(sb, m.group() + " *");
		m.appendTail(sb);
		System.out.println("Ergebnis: " + sb + m.find());
	}
}
