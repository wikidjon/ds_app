package a17_reg_expr;
import java.io.*; 
import java.util.Scanner; 
 
// die Klasse Scanner

public class ReadAllLines 
{ 
  private static Scanner scanner;

public static void main( String[] args ) throws FileNotFoundException 
  { 
    scanner = new Scanner( new File("c:/Ausgang/TextdateiLesen.java") ); 
    // ein scanner kann primitive datentypen und strings mit regulären ausdrücken parsen
    while ( scanner.hasNextLine() ) 
      System.out.println( scanner.nextLine() ); 
  } 
}
