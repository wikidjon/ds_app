package a17_reg_expr;

/************************************************************

Metazeichen    Vergleich
^              Beginn der Zeile
$              Ende der Zeile
\b             Wortgrenze
\B             Nicht-Wort Grenze
\A             Beginn der Eingabe
\G             Ende des vorhergehenden Vergleiches
\Z             Ende der Eingabe vor Zeilenende (CR/LF)
\z             Ende der Eingabe

/************************************************************

Zeichenkombinationen 
.			   	jedes einfache Zeichen
\d			   	Ziffer: [0-9]
\D			keine Ziffer: [^0-9] bzw. [^\d]
\s			   	Wei�raum: [ \t\n\x0B\f\r]
\S			   	kein Wei�raum: [^\s]
\w			Wortzeichen: [a-zA-Z0-9]
\W			kein Wortzeichen: [^\w]
 
.*  Der Punkt im regul�ren Ausdruck steht f�r ein beliebiges Zeichen, 
    und der folgende Stern ist ein Quantifizierer, 
    der wahllos viele beliebige Zeichen erlaubt.

[aeiuo]		   Zeichen a, e, i, o oder u
[^aeiuo]	   nicht die Zeichen a, e, i, o, u
[0-9a-fA-F]	   Zeichen 0, 1, 2, �, 9 oder Gro�-/Klein-Buchstaben a, b, c, d, e, f

***************************************************

Gieriger Operator 	Beschreibung
X?					X kommt einmal oder keinmal vor.
X*					X kommt keinmal oder beliebig oft vor.
X+					X kommt einmal oder beliebig oft vor.
X{n}                X kommt genau n-mal vor.
X{n,}               X kommt mindestens n-mal vor
X{n,m}              X kommt mindestens n aber h�chstens m-mal vor

Gieriger / Nicht gieriger Operator  
X?			X??
X*			X*?
X+			X+?
X{n}			X{n}?
X{n,}			X{n,}?
X{n,m}		X{n,m}?

Die drei Operatoren ?, * und + haben die Eigenschaft, 
die l�ngste m�gliche Zeichenfolge abzudecken � das nennt 
man gierig (engl. greedy). 

********************************************************

Die Java-Bibliothek bietet eine Reihe von M�glichkeiten 
zum Zerlegen von Zeichenfolgen:

split() von String - Aufteilen mit einem Delimiter, der durch 
regul�re Ausdr�cke beschrieben wird. 

Scanner - Klasse zum Ablaufen einer Eingabe. 

StringTokenizer - Delimiter sind einzelne Zeichen. 

BreakIterator - Findet Zeichen-, Wort-, Zeilen- oder Satzgrenzen. 

********************************************************

Zeichenvergleich
\p{javaMirrored}        matches       

\p{Blank}						Leerzeichen oder Tab: [ \t]
\p{Lower}, \p{Upper}	Klein-/Gro�buchstabe: [a-z] bzw. [A-Z]
\p{Alpha}					Buchstabe: [\p{Lower}\p{Upper}]
\p{Alnum}					alphanumerisches Zeichen: [\p{Alpha}\p{Digit}]
\p{Punct}					Punkt-Zeichen: !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
\p{Graph}					sichtbares Zeichen: [\p{Alnum}\p{Punct}]
\p{Print}						druckbares Zeichen: [\p{Graph}]
 
********************************************************/
