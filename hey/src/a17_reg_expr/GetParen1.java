package a17_reg_expr;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetParen1 {
  public static void main(String[] args) {
	  
    Pattern p = Pattern.compile("(\\w+)\\s(\\d+)");     	//\ w = wortzeichen \s = Wei�raum  \d = Ziffer
    																			// das + ist ein Quantifizierer, der wahllos viele beliebige Zeichen erlaubt
    Matcher matcher = p.matcher("Mueller 2r3");
    matcher.lookingAt();														//   matcher.find();
    System.out.println("Name: \t\t\t" + matcher.group(1));	    // group(int group) gibt die subsequencen der match operation zur�ck
    System.out.println("Kennummer:\t\t" + matcher.group(2));
     //System.out.println("Testr:\t\t" + matcher.group(3));

  }
}

           
