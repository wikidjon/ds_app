package a17_reg_expr;

/********************************************************
 Pr�fen von E-Mail-Adressen durch einen regul�ren Ausdruck 
 ********************************************************/

import java.util.regex.*;

public class RegAusdruck_pruef {
	public static void main(String[] args) {
		System.out.println();
		
			// mindestens ein Zeichen, gefolgt von @hs-merseburg, beliebigen Zeichen und 2-3 stelliger Domain
		Pattern p = Pattern.compile(".+@*\\.hs-merseburg\\.[a-zA-Z]{2,3}");
			// . Der Punkt im regul�ren Ausdruck steht f�r ein beliebiges Zeichen, 
	    	// das + und der * sind Quantifizierer, die wahllos viele beliebige Zeichen erlauben
			// - l�ngste m�gliche Zeichenkette (greedy - gierig).

		String email[] = new String[] { 
				"peter@stu.hs-merseburg.de",
				"@hs-merseburg.de", 
				"pink.floyd@.hs-merseburg.de",
				"p@hs-merseburg.net",
				"test@abc.def.hs-merseburg.net"};								//aendern !!!!

		for (int i = 0; i < email.length; i++) {
			Matcher m = p.matcher(email[i]);
			System.out.println(" E-Mail \t" + email[i] + " :\t\t " + m.matches());
			// Die statische Funktion Pattern.matches() und die Objektmethode matches() 
			// der Klasse String testen, ob ein regul�rer Ausdruck eine Zeichenfolge komplett beschreibt.
		}

		System.out.println();
	}
}
