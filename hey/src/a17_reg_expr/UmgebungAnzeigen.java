package a17_reg_expr;

import java.util.*;
import java.util.Map.Entry;

public class UmgebungAnzeigen
{
  public static void main(String[] args)
  {
    Map<?,?> env = System.getenv();
    Set<?> map_entries = env.entrySet();
    System.out.println("Liste verf�gbarer Properties:\n");
    for(Iterator<?> i = map_entries.iterator(); i.hasNext();)
    {
      Map.Entry e = (Entry) i.next();
      System.out.printf("%-50s%s%n", e.getKey() + ":", e.getValue());
    }
  }
}
