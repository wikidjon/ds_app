package a17_reg_expr;

/********************************************************
 Pr�fen von E-Mail-Adressen durch einen regul�ren Ausdruck 
 ********************************************************/

import java.util.regex.*;

public class RegAusdruck3 {
	public static void main(String[] args) {
		System.out.println();

		// mindestens ein Zeichen, gefolgt von @hs-merseburg und
		// 2-3 stelliger Domain
		Pattern p = Pattern.compile(".+@*{4}\\.hs-merseburg\\.[a-zA-Z]{2,3}");

		String email[] = new String[] { 
				"peter@student.hs-merseburg.de",
				"@.hs-merseburg.de", 							// . hinter @ !!!
				"pink.floyd@hs-merseburg",
				"p@gjhgjhg.hs-merseburg.net" };

		for (int i = 0; i < email.length; i++) {
			Matcher m = p.matcher(email[i]);
			System.out.println(" E-Mail " + email[i] + " : \t\t" + m.matches());
		}

		System.out.println();
	}
}
