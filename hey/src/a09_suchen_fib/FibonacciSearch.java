package a09_suchen_fib;

/*******************************************************************************
 * Fibonacci Suche
 ******************************************************************************/
public class FibonacciSearch {

	private static int min(int a, int b) {
		if (a <= b)
			return a;
		else
			return b;
	}

	public static int search(Orderable A[], Orderable k) {
		/*************************************************************************************
		 * Durchsucht A[1], .., A[n] nach Element k und liefert den Index i mit A[i] = k; -1 sonst
		 *************************************************************************************/
		int n = A.length - 1;
		int fibMinus2 = 1;
		int fibMinus1 = 1;
		int fib = 2;
		while (fib - 1 < n) {
			fibMinus2 = fibMinus1;
			fibMinus1 = fib;
			fib = fibMinus1 + fibMinus2;
		}
		int offset = 0;

		while (fib > 1) {
			// Durchsuche den Bereich [offset + 1, offset + fib - 1] nach Schluessel k
			// Falls fib = 2, dann besteht [offset + 1, offset + fib - 1] aus einem Element!)
			int m = min(offset + fibMinus2, n);
			if (k.less(A[m])) {
				// Durchsuche den Bereich [offset + 1, offset + fibMinus2 - 1] nach Schluessel k
				fib = fibMinus2;
				fibMinus1 = fibMinus1 - fibMinus2;
				fibMinus2 = fib - fibMinus1;
			} else if (k.greater(A[m])) {
				// Durchsuche den Bereich [offset + fibMinus2 + 1, offset + fib - 1] nach Schluessel k
				offset = m;
				fib = fibMinus1;
				fibMinus1 = fibMinus2;
				fibMinus2 = fib - fibMinus1;
			} else { 							// A[m] = k 
				return m;
			}
		}
		return -1;
	}
}
