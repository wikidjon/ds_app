package a10_hastable;

import java.util.Enumeration;
import java.util.Hashtable;

//	Working with Key-Value Pairs in a Hashtable
public class PlanetDiameters {
  public static void main(String args[]) {
    String names[] = { "Merkur", "Venus", "Erde", "Mars", "Jupiter",
        "Saturn", "Uranus", "Neptun", "Pluto" };
    float diameters[] = { 4800f, 12103.6f, 12756.3f, 6794f, 142984f,
        120536f, 51118f, 49532f, 2274f };
    Hashtable<String, Float> hash = new Hashtable<String, Float>();
    for (int i = 0, n = names.length; i < n; i++) {
      hash.put(names[i], new Float(diameters[i]));
    }
    Enumeration<String> e = hash.keys();
    Object obj;
    while (e.hasMoreElements()) {
      obj = e.nextElement();
      System.out.println(obj + ": " + hash.get(obj));
    }
  }
}