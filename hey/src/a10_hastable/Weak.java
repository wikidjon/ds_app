package a10_hastable;

import java.util.Map;
import java.util.WeakHashMap;

//	Demonstrating the WeakHashMap
public class Weak {

  public static void main(String args[]) {
    final Map<String, String> map = new WeakHashMap<String, String>();
    map.put(new String("Java2s"), "www.java2s.com");
    Runnable runner = new Runnable() {
      public void run() {
        while (map.containsKey("Java2s")) {
          try {
            Thread.sleep(500);
          } catch (InterruptedException ignored) {
          }
          System.out.println("Waiting");
          System.gc();
        }
      }
    };
    Thread t = new Thread(runner);
    t.start();
    System.out.println("Main waiting");
    try {
      t.join();
    } catch (InterruptedException ignored) {
    }
  }
}