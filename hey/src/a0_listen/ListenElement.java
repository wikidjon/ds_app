package a0_listen;

class ListenElement {
    private Object daten;
    private ListenElement naechstes;
    
    ListenElement(Object daten) {
        this(daten, null);
    }
    
    ListenElement(Object daten, ListenElement naechstes) {
        this.daten = daten;
        this.naechstes = naechstes;
    }
    
    public Object getDaten() {
        return daten;
    }
    
    public ListenElement getNaechstes() {
        return naechstes;
    }
    
    public void setDaten(Object daten) {
        this.daten = daten;
    }
    
    public void setNaechstes(ListenElement naechstes) {
        this.naechstes = naechstes;
    }
}
