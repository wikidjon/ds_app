package a0_listen;

class Liste {
	private ListenElement aktuell;
    private ListenElement kopf;
    private ListenElement vorgaenger;
    
    void durchlaufe() {
        // lokale Variable elem verweist auf das gerade betrachtete Listenelement.
        ListenElement elem = kopf;
        while (elem != null) {
            // bearbeite aktuelles Element
            System.out.print(elem.getDaten() + " ");
            // gehe ein Element weiter
            elem = elem.getNaechstes();
        }
        System.out.println(" --- Listenende!!!");
    }
    
    void einfuegeEnde(Object neuesObject) {
        ListenElement neu = new ListenElement(neuesObject);
        
        // letztes Listenelement ermitteln, d.h. aktuell und vorgaenger korrekt setzen
        findeEnde();
        
        // falls aktuell == null gilt, dann ist die Liste leer -> einfuegen am Listenkopf 
        if (aktuell == null)
            kopf = neu;
        // andernfalls wird die Nachfolger-Referenz des bisher letzten Elements auf das neue Element gesetzt
        else
            aktuell.setNaechstes(neu);
    }
    
    void einfuegeHinter(Object neuesObject) {
        // vorhandenes Element muss angegeben sein
        if (aktuell == null)
            throw new NullPointerException();
        
        // einfuegen hinter Element aktuell
        ListenElement neu = new ListenElement(neuesObject, aktuell.getNaechstes());
        aktuell.setNaechstes(neu);
    }
    
    void einfuegeKopf(Object neuesObject) {
        ListenElement neu = new ListenElement(neuesObject, kopf);
        kopf = neu;
    }
    
    void einfuegeVor(Object neuesObject) {
        // aktuelles Element muss vorhanden sein
        if (aktuell == null)
            throw new NullPointerException();
        
        ListenElement neu = new ListenElement(neuesObject, aktuell);
        if (vorgaenger == null)
            kopf = neu;
        else
            vorgaenger.setNaechstes(neu);
    }
    
    boolean finde(Object einObject) {
        vorgaenger = null;
        aktuell = kopf;
        while (aktuell != null) {
            if (einObject.equals(aktuell.getDaten()))
                return true;
            else {
                vorgaenger = aktuell;
                aktuell = aktuell.getNaechstes();
            }
        }
        return false;
    }
    
    // letztes und vorletztes Listenelement ermitteln
    void findeEnde() {
        vorgaenger = null;
        aktuell = kopf;
        
        // Liste leer?
        if (aktuell == null)
            return;
        // wenn nicht -> Ende suchen:
        while (aktuell.getNaechstes() != null) {
            vorgaenger = aktuell;
            aktuell = aktuell.getNaechstes();
        }
        // aktuell verweist jetzt entweder auf null (wenn die Liste leer ist)
        // oder aber auf das letzte Listenelement
        // vorgaenger verweist jetzt entweder auf null (wenn die Liste leer ist
        // oder nur ein Element enthaelt) oder aber auf das vorletzte Listenelement
    }
    
    Object getAktuelleDaten() {
        // aktuelles Element muss vorhanden sein
        if (aktuell == null)
            throw new NullPointerException("kein aktuelles Element");
        
        return aktuell.getDaten();
    }
    
    Object getEndeDaten() {
        // letztes Listenelement ermitteln, d.h. aktuell und vorgaenger korrekt setzen
        findeEnde();
        
        return getAktuelleDaten();
    }
    
    Object getKopfDaten() {
        // Listenkopf muss vorhanden sein, d.h. Liste darf nicht leer sein
        if (kopf == null)
            throw new NullPointerException("Liste ist leer");
        
        return kopf.getDaten();
    }
    
    boolean istLeer() {
        return kopf == null;
    }
    
    void konkateniere(Liste l) {
        // letztes Listenelement der aktuellen Liste ermitteln
        findeEnde();
        
        // falls aktuell == null dann ist die Liste leer -> setzen des 
        // Kopf der aktuellen Listen auf den Listenkopf des Arguments l.
        if (aktuell == null)
            kopf = l.kopf;
        // andernfalls wird die Nachfolgerreferenz des bisher letzten Elements
        // auf auf den Listenkopf des Arguments l gesetzt.
        else
            aktuell.setNaechstes(l.kopf);
    }
    
    
    void loescheElement() {
        // aktuelles Element muss vorhanden sein
        if (aktuell == null)
            throw new NullPointerException("kein aktuelles Listenelement vorhanden");
        
        if (vorgaenger == null)
            kopf = aktuell.getNaechstes();
        else
            vorgaenger.setNaechstes(aktuell.getNaechstes());
    }
    
    void loescheEnde() {
        // letztes Listenelement ermitteln, d.h. aktuell und vorgaenger korrekt setzen
        findeEnde();
        loescheElement();
    }
    
    void loescheKopf() {
        // Liste darf nicht leer sein
        if (kopf == null)
            throw new NullPointerException("Liste ist leer");
        
        kopf = kopf.getNaechstes();
    }
    
    void loescheNachfolger() {
        // Nachfolger von aktuell muss vorhanden sein
        if (aktuell.getNaechstes() == null)
            throw new NullPointerException("kein Nachfolger vorhanden");
        
        aktuell.setNaechstes(aktuell.getNaechstes().getNaechstes());
    }
    
    void spiegeln() {
        // falls die Liste leer ist, ist nichts zu tun.
        if (kopf != null) {
            // in der Liste spiegel wird das Spiegelbild der Original-Liste aufgebaut.
            Liste spiegel = new Liste();
            // - durchlaufen der aktuellen Liste und anf�gen von Kopien
            // der Listenelemente vorne an die Liste spiegel
            // - die lokale Variable elem verweist auf das gerade betrachtete Listenelement
            ListenElement elem = kopf;
            while (elem != null) {
                spiegel.einfuegeKopf(elem.getDaten());
                elem = elem.getNaechstes();
            }
            
            // den Kopf der aktuellen Liste auf den Kopf 
            // der neu erzeugten Liste spiegel setzen.
            kopf = spiegel.kopf;
            System.out.println("gespiegelt:");
                      
            // auf die Listenelemente der Originalliste kann jetzt nicht mehr
            // zugegriffen werden. Diese werden irgendwann vom garbage collector aufgesammelt.
        }
    }
    
    boolean vergleiche(Liste l) {
        // Wird der Fall 1 auch dann korrekt behandelt,
        // wenn die folgenden beiden Anweisungen auskommentiert werden?
        // Fall 1: beide Listen sind leer und somit gleich
        if (kopf == null && l.kopf == null)
            return true;
        
        // Fall 2: mindestens eine der Listen ist nicht leer
        // durchlaufen der beiden Listen und vergleichen der jeweils
        // in den beiden Listenelementen enthaltenen Daten
        ListenElement elem = kopf, lElem = l.kopf;
        while (elem != null && lElem != null) {
            // Fall 2a: die Daten in den beiden betrachteten Listenelementen sind verschieden
            if (!elem.getDaten().equals(lElem.getDaten()))
                return false;
            // gehe zum jeweils naechsten Listenelement
            elem = elem.getNaechstes();
            lElem = lElem.getNaechstes();
        }
        
        // an dieser Stelle ist mindestens eine der beiden Referenzen elem bzw. lElem gleich null;
        // Fall 2b: wenn nur eine der beiden Referenzen gleich null ist,
        // dann haben die beiden Listen unterschiedliche Laengen, sind also nicht gleich
        // Fall 2c: wenn beide Referenzen gleich sind, d.h., wenn
        // elem == lElem == null gilt, dann sind die beiden Listen gleich
        return elem == lElem;
    }
    
    int zaehleElemente() {

        // Eine leere Liste enthaelt 0 Listenelemente.
        int anzahl = 0;
        // Die lokale Variable elem verweist auf das gerade betrachtete Listenelement.
        ListenElement elem = kopf;
        while (elem != null) {
            // bearbeite aktuelles Element
            anzahl++;
            // gehe ein Element weiter
            elem = elem.getNaechstes();
        }
        return anzahl;
    }
    
}
