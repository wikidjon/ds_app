package a0_listen;

class SchlangeTest {
    public static void main(String args[]) {
        SchlangeMitListe s = new SchlangeMitListe();
        
        String str = TastaturEingabe.readString("Bitte Zeichenkette eingeben: ");
        int len = str.length();
        
        for(int i = 0; i < len; i++)
            s.anfuege(new Character(str.charAt(i)));
        
        System.out.println();
        System.out.println("... Aufruf der durchlaufe()-Methode der Liste....");
        s.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der abarbeite()-Methode der Queue....");
        while(!s.istLeer()) {
            Object o = s.abarbeite();
            System.out.print(((Character)o).charValue());
        }
        System.out.println("\n---------------------------\n");
        System.out.println("... nochmaliger Aufruf der durchlaufe()-Methode der Liste....");
        s.durchlaufe();
        System.out.println();
    }
}
