package a00_listen_do_verk;

class ListElement {

    Object obj;
    ListElement naeElem, vorElem;

    public ListElement(Object obj) {
        this.obj = obj;
        naeElem = null;
    }

    public void setNaeElem(ListElement naeElem) {
        this.naeElem = naeElem;
    }
    
    public void setVorElem(ListElement vorElem) {
        this.vorElem = vorElem;
    }

    public ListElement getNaeElem() {
        return naeElem;
    }
    
    public ListElement getVorElem() {
        return this.vorElem;
    }

    public Object getObj() {
        return obj;
    }
} 