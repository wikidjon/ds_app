package a00_listen_do_verk;
public class DoppeltVerketteteListe {


    ListElement startElem = new ListElement("Kopf");
    ListElement endElement = new ListElement("Ende");

    public DoppeltVerketteteListe() {
        startElem.setNaeElem(endElement);
        endElement.setVorElem(startElem);
    }

    public void einfEnde(Object o){
        ListElement neuElem = new ListElement(o);
        ListElement letztesElem = getletztesElem();
       // letztesElem.getVorElem().setNaeElem(neuElem);
        letztesElem.setNaeElem(neuElem);
        neuElem.setVorElem(letztesElem);
       // neuElem.setNaeElem(letztesElem);
    }

    public void einfNach(Object prevItem, Object newItem) {
        ListElement neuElem, nextElem = null, zeigerElem;
        zeigerElem = startElem.getNaeElem();
        while(zeigerElem != null && !zeigerElem.getObj().equals(prevItem)){
            zeigerElem = zeigerElem.getNaeElem();
        }
        neuElem = new ListElement(newItem);
        if(zeigerElem != null){
            nextElem = zeigerElem.getNaeElem();
            zeigerElem.setNaeElem(neuElem);
            neuElem.setNaeElem(nextElem);
            neuElem.setVorElem(zeigerElem);
        }
        if(nextElem != null)
            nextElem.setVorElem(neuElem);
    }
    
    public void einfVor(Object insertItem, Object newItem){
        ListElement neuElem, zeigerElem;
        neuElem = new ListElement(newItem);
        zeigerElem = startElem.getNaeElem();
        while(zeigerElem != null){
            if(zeigerElem.getObj().equals(insertItem)){
                neuElem.setVorElem(zeigerElem.getVorElem());
                zeigerElem.getVorElem().setNaeElem(neuElem);
                zeigerElem.setVorElem(neuElem);
                neuElem.setNaeElem(zeigerElem);
                break;
            }
            zeigerElem = zeigerElem.getNaeElem();
        }
    }

    public void delete(Object o){
        ListElement le = startElem;
        while (le.getNaeElem() != null && !le.getObj().equals(o)){
            if(le.getNaeElem().getObj().equals(o)){
                if(le.getNaeElem().getNaeElem()!=null){
                    le.setNaeElem(le.getNaeElem().getNaeElem());
                    le.getNaeElem().setVorElem(le);
                }else{
                    le.setNaeElem(null);
                    break;
                }
            }
            le = le.getNaeElem();
        }
    }
    

    public boolean find(Object o){
        ListElement le = startElem;
        while (le != null){
            if(le.getObj().equals(o))
            return true;
            le = le.naeElem;
        }
        return false;
    }
    

    public ListElement getFirstElem() {
        return startElem;
    }

    public ListElement getletztesElem() {
        ListElement le = startElem;
        while(le.getNaeElem() != null){
            le = le.getNaeElem();
        }
        return le;
    }

    public void writeList() {
        ListElement le = startElem;
        while(le != null){
            System.out.print(le.getObj() + " - ");
            le = le.getNaeElem();
        }
    }

    public static void main(String[] args) {
        DoppeltVerketteteListe list = new DoppeltVerketteteListe();
        list.einfEnde("1");
        list.einfEnde("2");
        list.einfEnde("3");
        list.einfEnde("4");
        list.einfEnde("5");
        System.out.println("Die Liste: " );
        list.writeList();
        System.out.println("\n------------------------------------" );
        list.einfNach("2", "nach 2");
        list.delete("4");
        list.einfVor("3", "vor 3");
        System.out.println("erstes Element: " + list.getFirstElem().getObj());
        System.out.println("ist '4' enthalten? " + list.find("4"));
        System.out.println("ist '5' enthalten? " + list.find("5"));
        System.out.println("letztes Element: " + list.getletztesElem().getObj());
        System.out.println("vorletztes Element: " + list.getletztesElem().getVorElem().getObj());
        list.writeList();
    }
} 