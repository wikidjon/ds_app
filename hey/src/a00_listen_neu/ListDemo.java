package a00_listen_neu;

/*
time for LinkedList = 501
time for ArrayList = 126
*/
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//Add to end Performance compare: LinkList and ArrayList

public class ListDemo {
  // number of objects to add to list
  static final int SIZE = 1000000;

  static long timeList(List<Object> list) {
    long start = System.currentTimeMillis();
    Object obj = new Object();

    for (int i = 0; i < SIZE; i++) {
      // add object to the rear of the list
      list.add(obj);
    }

    return System.currentTimeMillis() - start;
  }

  public static void main(String args[]) {
    // do timing for LinkedList
    System.out.println("Zeit fuer LinkedList = " + timeList(new LinkedList<Object>()));

    // do timing for ArrayList
    System.out.println("Zeit fuer ArrayList = " + timeList(new ArrayList<Object>()));
  }
}

           