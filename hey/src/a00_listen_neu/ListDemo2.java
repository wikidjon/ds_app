package a00_listen_neu;

/*
time for LinkedList = 62
time for ArrayList = 7488
*/
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//Add to start Performance compare: LinkList and ArrayList

public class ListDemo2 {
  static final int SIZE = 100000;

  static long timeList(List<Object> list) {
    long start = System.currentTimeMillis();
    Object obj = new Object();
    for (int i = 0; i < SIZE; i++) {
      // add object to the head of the list
      list.add(0, obj);
    }

    return System.currentTimeMillis() - start;
  }

  public static void main(String args[]) {
    // do timing for LinkedList
    System.out.println("Zeit fuer LinkedList = " + timeList(new LinkedList<Object>()));

    // do timing for ArrayList
    System.out.println("Zeit fuer ArrayList = " + timeList(new ArrayList<Object>()));
  }
}