package a08_bbaum;

import java.util.NoSuchElementException;

public class BTree5 {
        private BNode root;

        public BTree5() {
                root = null;
        }

        public void inorderDisplay(BNode node) {
                if (node != null) {
                        inorderDisplay(node.left);
                        System.out.print(node.key + " ");
                        inorderDisplay(node.right);
                }
        }

        public void insert(int value) {
                BNode y = null; // 指向插入位置的parent
                BNode x = root; // 指向插入的位置
                while (x != null) {
                        y = x;
                        if (value < x.key) {
                                x = x.left;
                        } else {
                                x = x.right;
                        }
                }
                BNode z = new BNode(value);
                z.parent = y;
                if (y == null) {
                        root = z;
                } else {
                        {
                                if (value < y.key) {
                                        y.left = z;
                                } else {
                                        y.right = z;
                                }
                        }

                }

        }

        public boolean contains(BNode node, int value) {
                if (node == null)
                        return false;
                if (node.key == value)
                        return true;
                if (value < node.key) {
                        return contains(node.left, value);
                } else
                        return contains(node.right, value);

        }

        public boolean contains(int value) {
                BNode x = root;
                if (x == null)
                        return false;
                while (value != x.key) {
                        if (value < x.key) {
                                x = x.left;
                        } else
                                x = x.right;
                        if (x == null)
                                return false;

                }
                return true;

        }

        public int minValue() {
                BNode node = root;
                while (node.left != null) {
                        node = node.left;
                }
                if (node != null)
                        return node.key;
                else
                        throw new NoSuchElementException();
        }

        public int maxValue() {
                BNode node = root;
                while (node.right != null) {
                        node = node.right;
                }
                if (node != null)
                        return node.key;
                else
                        throw new NoSuchElementException();
        }

        public static void main(String[] args) {
                BTree5 tree = new BTree5();
                tree.insert(5);
                tree.insert(8);
                tree.insert(2);
                tree.insert(3);
                tree.insert(1);
                tree.insert(100);
                tree.insert(4);
                tree.insert(999);
                tree.insert(400);
                tree.insert(401);
                tree.insert(-1000);
                tree.inorderDisplay(tree.root);
                System.out.println(tree.contains(tree.root, -1));
                System.out.println(tree.contains(tree.root, 1));
                System.out.println(tree.contains(tree.root, 101));
                System.out.println(tree.contains(tree.root, 100));
                System.out.println("----------------------------");
                System.out.println(tree.contains(-1));
                System.out.println(tree.contains(3));
                System.out.println(tree.contains(1));
                System.out.println(tree.contains(101));
                System.out.println(tree.contains(100));
                System.out.println(tree.contains(4));
                System.out.println(tree.contains(7));

                System.out.println("min:" + tree.minValue());
                System.out.println("max:" + tree.maxValue());
        }
}
