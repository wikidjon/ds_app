package a08_bbaum;

import java.lang.Comparable;
import java.util.Vector;

public class BBaum2 {

	@SuppressWarnings("unchecked")
	class Node {
		/* Knotentypen */
		public static final int LEAF_PAGE = 0;
		public static final int NODE_PAGE = 1;
		
		/* Suchergebnisse */
		public static final int KEY_FOUND = -1;
		public static final int KEY_NOT_FOUND = -2;
				
		Node parent = null; // Elternknoten
		Vector<Comparable> keys; // Feld f�r Schl�ssel
		Vector<Node> pointers; // Feld f�r Verweise auf Kindknoten
		int ntype; // Knotentyp
		
		public Node(int nt) {
			ntype = nt;
			keys = new Vector<Comparable>();
			pointers = (ntype == NODE_PAGE ? new Vector<Node>() : null);
		}
		
		public void setNodeType(int nt) {
			ntype = nt;
			if (pointers == null && ntype == NODE_PAGE)
				pointers = new Vector<Node>();
		}

		public Comparable getKey(int idx) {
			return keys.get(idx);
		}
		
		public Node getPointer(int idx) {
			return pointers.get(idx);
		}
		
		public int getNumKeys() {
			return keys.size();
		}

		public Node getParent() {
			return parent;
		}
		
		public boolean insertIntoNode(Comparable obj, Node lnode, Node rnode) {
			boolean done = false;
			// Position f�r Schl�ssel suchen
			for (int i = 0; i < keys.size(); i++) {
				int res = keys.get(i).compareTo(obj);
				if (res == 0) {
					// Schl�ssel existiert schon -> ignorieren
					done = true;
					break;
				}
				else if (res > 0) {
					// Stelle gefunden -> einf�gen
					keys.insertElementAt(obj, i);
					if (rnode != null) {
						pointers.insertElementAt(rnode, i+1);
						rnode.parent = this;
					}
					done = true;
					break;
				}
			}
			if (!done) {
				// Schl�ssel muss am Ende eingef�gt werden
				keys.add(obj);
				if (lnode != null && pointers.isEmpty()) {
					pointers.add(lnode);
					lnode.parent = this;
				}
				if (rnode != null) {
					pointers.add(rnode);
					rnode.parent = this;
				}
			}
			// Knoten zu gro�?
			return keys.size() > degree * 2;
		}
		
		public Node split() {
			int pos = getNumKeys() / 2;
			// Geschwisterknoten erzeugen
			Node sibling = new Node(ntype);
			for (int i = pos+1; i < getNumKeys(); i++) {
				// die obere H�lfte der Schl�ssel und Verweise kopieren
				sibling.keys.add(this.getKey(i));
				if (ntype == Node.NODE_PAGE)
					sibling.pointers.add(this.getPointer(i));
			}
			// es gibt einen Verweis mehr als Schl�ssel
			if (ntype == Node.NODE_PAGE)
				sibling.pointers.add(this.getPointer(getNumKeys()));
			
			// und anschlie�end im Originalknoten l�schen
			for (int i = getNumKeys()-1; i >= pos; i--) {
				keys.remove(pos);
				if (ntype == Node.NODE_PAGE)
					pointers.remove(pos+1);
			}
			return sibling;
		}
		
		public int findKeyInNode(Comparable c, Comparable[] key) {
			for (int i = 0; i < keys.size(); i++) {
				int res = keys.get(i).compareTo(c);
				if (res == 0) {
					// Schl�ssel gefunden
					key[0] = keys.get(i);
					return KEY_FOUND;
				}
				else if (res > 0)
					return ntype == NODE_PAGE ? i : KEY_NOT_FOUND;
			}
			// wenn es ein innerer Knoten ist, geben wir den letzten Verweis
			// zur�ck, im Fall eines Blattes KEY_NOT_FOUND
			return (ntype == NODE_PAGE ? keys.size() : KEY_NOT_FOUND);
		}
		
		public void print(int depth) {
			for (int i = 0; i < depth; i++) System.out.print("  ");
			for (int i = 0; i < keys.size(); i++) {
				System.out.print("|" + keys.get(i));
			}
			System.out.println("|" + (pointers != null ? "size="+pointers.size() : ""));
			if (ntype == NODE_PAGE) {
				for (int i = 0; i < pointers.size(); i++) {
					Node n = pointers.get(i);
					n.print(depth+1);
				}
				
			}
		}
	}
	
	private Node root = null;
	private int degree;
	
	public BBaum2(int d) {
		degree = d;
		root = new Node(Node.LEAF_PAGE);
	}
	
	@SuppressWarnings("unchecked")
	public Comparable find(Comparable k) {
		Node node = root; // Startknoten
		boolean finished = false;
		Comparable[] res = { null };
		
		do {
			// Suche Schl�ssel im aktuellen Knoten
			int idx = node.findKeyInNode(k, res);
			if (idx == Node.KEY_FOUND || idx == Node.KEY_NOT_FOUND) 
				// Schl�ssel gefunden oder auf einem
				// Blattknoten nicht gefunden -> fertig
				finished = true;
			else
				// anderenfalls Verweis verfolgen
				node = node.getPointer(idx);
		} while (! finished);
		return res[0];
	}
	
	@SuppressWarnings("unchecked")
	public void insert(Comparable k) {
		Node lsibling = null, rsibling = null;
		// Suche Blattknoten, der den Schl�ssel aufnimmt
		Node node = findLeafNode(k);
		
		// Schl�ssel einf�gen
		while (node.insertIntoNode(k, lsibling, rsibling)) {
			// Split erforderlich
			int pos = node.getNumKeys() / 2;
			k = node.getKey(pos);
			Node parent = node.getParent();
			if (parent == null) 
				// ein neuer Elternknoten muss angelegt werden
				parent = new Node(Node.NODE_PAGE);
			// Split durchf�hren
			lsibling = node;
			rsibling = node.split();

			// Wurzel anpassen
			if (root == node) root = parent;
			// der aktuelle Knoten ist jetzt der Elternknoten
			node = parent;
			// und der muss ein innerer Knoten sein
			node.setNodeType(Node.NODE_PAGE);
		}
	}
	
	@SuppressWarnings("unchecked")
	private Node findLeafNode(Comparable c) {
		Node node = root;
		Comparable[] key = { null }; // wird hier eigentlich nicht ben�tigt
		
		// den Baum von der Wurzel aus nach unten durchlaufen,
		// bis ein Blattknoten gefunden wurde
		while (node.ntype != Node.LEAF_PAGE) {
			assert (!node.pointers.isEmpty());
			// Verweis verfolgen
			node = node.getPointer(node.findKeyInNode(c, key));
		}
		return node;
	}

	public void printTree() {
		root.print(0);
	}
	
	public static void main(String[] args) {
		BBaum2 btree = new BBaum2(2);
		btree.insert(new Integer(1));
		btree.insert(new Integer(2));
		btree.insert(new Integer(3));
		btree.insert(new Integer(8));
		btree.insert(new Integer(4));
		btree.printTree();
		btree.insert(new Integer(5));
		btree.insert(new Integer(6));
		btree.insert(new Integer(7));
		btree.insert(new Integer(9));
		btree.insert(new Integer(10));
		btree.insert(new Integer(11));
		btree.insert(new Integer(12));
		btree.insert(new Integer(13));
		btree.insert(new Integer(14));
		btree.insert(new Integer(15));
		btree.insert(new Integer(16));
		System.out.println("----------------------------"); 
		btree.printTree();
		System.out.println("----------------------------"); 
		btree.insert(new Integer(17));
		System.out.println("----------------------------"); 
		btree.printTree();
		
		System.out.println(btree.find(new Integer(7)));
		System.out.println(btree.find(new Integer(16)));
		System.out.println(btree.find(new Integer(3)));
		System.out.println(btree.find(new Integer(9)));
		System.out.println(btree.find(new Integer(0)));
	}
}
