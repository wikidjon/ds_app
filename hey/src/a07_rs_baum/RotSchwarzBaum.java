package a07_rs_baum;
@SuppressWarnings("unchecked")

//Anzahl Knoten auf laengstem Pfad < 2* Anzahl Knoten auf kuerzestem Pfad

public class RotSchwarzBaum  {
	
	static class RSKnoten {
		Object Schluessel;
		RSKnoten links = null, rechts = null;
		boolean Rot = false;
		
		public RSKnoten (Object e) 			{ Schluessel = e; }
		public RSKnoten getLinks () 		{ return links; }
		public RSKnoten getRechts () 		{ return rechts; }
		public Object getSchluessel () 		{ return Schluessel; }
		
		public void setlinks (RSKnoten n) 	{ links = n; }
		public void setrechts (RSKnoten n) 	{ rechts = n; }
		
		public void setRot(boolean b) 		{ Rot = b; }
		public boolean isRot() 				{ return Rot; }

		public int vergleicheKnoten(Comparable<?> c) {
			return (Schluessel == null ? -1 : ((Comparable<Comparable<?>>)Schluessel).compareTo(c));
		}
		
		public String toString () { return Schluessel.toString (); }
		
		public void ausgabeKnoten (StringBuffer sb, int tiefe, RSKnoten sentinel) {
			for (int i = 0; i < tiefe; i++)
				sb.append ("  ");
			sb.append (toString () + " : " + (Rot ? "(Rot)" : "(Schwarz)") + "\n");
			if (links != sentinel)
				links.ausgabeKnoten (sb, tiefe + 1, sentinel);
			if (rechts != sentinel)
				rechts.ausgabeKnoten(sb, tiefe + 1, sentinel);
		}
		
	}
	
	private RSKnoten kopf, ende;
	
	public RotSchwarzBaum () {
		kopf = new RSKnoten(null);
		ende = new RSKnoten(null);
		kopf.setrechts(ende); kopf.setlinks(ende);
		ende.setlinks(ende); ende.setrechts(ende);
	}
	
	public boolean einfuegen(Comparable<?> c) {
		RSKnoten knoten, urgrossvater, grossvater, eltern;
		int cmp = 0;
		
		knoten = eltern = grossvater = urgrossvater = kopf;
		
		System.out.println("einfuegen: " + c);
		while (knoten != ende) {
			urgrossvater = grossvater; grossvater = eltern; eltern = knoten;
			cmp = knoten.vergleicheKnoten(c);
			if (cmp == 0)
				return false;
			else
				knoten = cmp > 0 ? knoten.getLinks () : knoten.getRechts();
			//System.out.println("knoten??" + knoten.getSchluessel());
			if (knoten.getLinks().isRot() && knoten.getRechts().isRot())
				split(c, knoten, eltern, grossvater, urgrossvater);
		}
		knoten = new RSKnoten(c);
		knoten.setlinks(ende); knoten.setrechts(ende);
		if (eltern.vergleicheKnoten(c)  > 0)
			eltern.setlinks(knoten);
		else
			eltern.setrechts(knoten);
		split(c, knoten, eltern, grossvater, urgrossvater);
		return true;
	}
	
	private RSKnoten rotieren(Comparable<?> c, RSKnoten knoten) {
		RSKnoten child, gchild;
		child = knoten.vergleicheKnoten(c) > 0 ? knoten.getLinks() : knoten.getRechts();
		if (child.vergleicheKnoten(c) > 0) {
			gchild = child.getLinks(); 
			child.setlinks(gchild.getRechts()); 
			gchild.setrechts(child);
		}
		else {
			gchild = child.getRechts(); 
			child.setrechts(gchild.getLinks()); gchild.setlinks(child);
		}
		if (knoten.vergleicheKnoten(c) > 0)
			knoten.setlinks(gchild);
		else 
			knoten.setrechts(gchild);
		return gchild;
	}
	
	private void split(Comparable<?> c, RSKnoten knoten, RSKnoten eltern, RSKnoten grossvater, RSKnoten urgrossvater) {
		//Implementiert eine Klasse Comparable, so k�nnen sich die Objekte selbst mit anderen Objekten vergleichen
		System.out.println("   splitten: " + knoten.getSchluessel());
		knoten.setRot(true);
		knoten.getLinks().setRot(false);
		knoten.getRechts().setRot(false);
		if (eltern.isRot()) {
			System.out.println("Eltern sind rot");
			grossvater.setRot(true);
			if (grossvater.vergleicheKnoten(c) != eltern.vergleicheKnoten(c)) {
				System.out.println("Grossvater und Eltern != c -- rotieren");
				eltern = rotieren(c, grossvater);
				StringBuffer sb = new StringBuffer ();
				eltern.ausgabeKnoten (sb, 0, ende);
				System.out.println("-->" +  sb.toString ());
				
			}
			knoten = rotieren(c,urgrossvater);
			knoten.setRot(false);
			StringBuffer sb = new StringBuffer ();
			knoten.ausgabeKnoten (sb, 0, ende);
			System.out.println("==>" +  sb.toString ());
		}
		kopf.getRechts().setRot(false);
	}
	
	public boolean remove (Comparable<?> c) {
		return false;
	}
	
	public String toString () {
		StringBuffer sb = new StringBuffer ();
		if (kopf.getRechts() != ende)
			kopf.getRechts().ausgabeKnoten (sb, 0, ende);
		return sb.toString ();
	}
	
	public static void main(String[] args) {
		RotSchwarzBaum tree = new RotSchwarzBaum ();
		tree.einfuegen (new Integer (2));
		tree.einfuegen (new Integer (14));
		tree.einfuegen (new Integer (7));
		tree.einfuegen (new Integer (4));
		tree.einfuegen (new Integer (5));
		tree.einfuegen (new Integer (8));
		tree.einfuegen (new Integer (1));
		tree.einfuegen (new Integer (15));
		tree.einfuegen (new Integer (9));
		System.out.println("\n<---Ende Einfuegen Teil 1----------------------------");
		System.out.println("---Ausgabe Rot-Schwarz-Baum Teil 1------------------>");
		System.out.println (tree); 
		System.out.println("----Einfuegen Teil 2------------------->");
		tree.einfuegen (new Integer (6));
		tree.einfuegen (new Integer (11));
		tree.einfuegen (new Integer (10));
		tree.einfuegen (new Integer (3));
		tree.einfuegen (new Integer (12));
		System.out.println (tree); 
		tree.einfuegen (new Integer (13));
		System.out.println("\n<---Ende Einfuegen Teil 2----------------------------");
		System.out.println("---Ausgabe Rot-Schwarz-Baum Teil 1 + 2--------------->");
		System.out.println (tree); 
	}
}
