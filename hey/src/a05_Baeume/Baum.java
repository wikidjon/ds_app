package a05_Baeume;

/***************************  Baum.java  **************************************/

/** Klasse Baum  mit drei Konstruktoren und vier Methoden.
 *  Ein Baum besteht aus den Datenfeldern inhalt, links, rechts.
 */

public class Baum {

  protected Object inhalt;                           // Inhalt
  private Baum rechts;                      // linker, rechter Teilbaum
private Baum links;

  public final static Baum LEER = new Baum(); // leerer Baum als Klassenkonst.

  public Baum () {                         // konstruiert einen leeren Baum
    inhalt = null;                         // kein Inhalt
    setLinks(null);                         // keine
    setRechts(null);                         // Kinder
  }

  public Baum (Object x) {                 // konstruiert ein Blatt
    this(LEER, x, LEER); }                 // mit Objekt x

  public Baum (Baum l, Object x, Baum r) { // konstruiert einen Baum
    inhalt = x;                            // aus einem Objekt x und
    setLinks(l);                            // einem linken Teilbaum
    setRechts(r); }                          // und einem rechten Teilbaum

  public boolean empty () {                // liefert true,
    return (inhalt == null);               // falls Baum leer ist
  }

  public Baum left () {                    // liefert linken Teilbaum
    if (empty()) System.out.println("in links: leerer Baum");
    return getLinks();
  }

  public Baum right () {                   // liefert rechten Teilbaum
    if (empty()) System.out.println("in rechts: leerer Baum");
    return getRechts();
  }

  public Object value () {                 // liefert Objekt in der Wurzel
    if (empty()) System.out.println("in wert: leerer Baum");
    return inhalt;
  }

public void setLinks(Baum links) {
	this.links = links;
}

public Baum getLinks() {
	return links;
}

public void setRechts(Baum rechts) {
	this.rechts = rechts;
}

public Baum getRechts() {
	return rechts;
}
}
