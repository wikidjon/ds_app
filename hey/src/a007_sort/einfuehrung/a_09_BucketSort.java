package a007_sort.einfuehrung;

import einfuehrung.AlgoTools.IO;

/******************************************************************************
 * Sortieren durch Verteilen auf Buckets (Faecher). 
 * Idee: 
 * 		Verteilung der Elemente auf die Buckets (Partitionierung)
*		Jeder Bucket wird mit einem weiteren Sortierverfahren, wie beispielsweise Insertionsort sortiert.
*		Der Inhalt der sortierten Buckets wird konkateniert.
 *****************************************************************************/

public class a_09_BucketSort {

	static final int N = 256; 													// Alphabetgroesse N

	public static char[] sortieren(char[] a) {			 			// sortiere Character-Array a
																								// und liefere Ergebnis zurueck
		char[] fertig = new char[a.length];						    // Ergebniszeichenkette
		int[] b = new int[N]; 													// N Buckets
		int i, j, k = 0;

		for (i = 0; i < N; i++)
			b[i] = 0; 																		// Buckets initialisieren

		for (i = 0; i < a.length; i++)											// fuer jedes Eingabezeichen
			b[a[i]]++; 																	// zustaendiges Bucket erhoehen

		for (i = 0; i < N; i++)														// fuer jedes Bucket
			for (j = 0; j < b[i]; j++)												// gemaess Zaehlerstand
				fertig[k++] = (char) i; 											// sein Zeichen uebernehmen

		return fertig; 																	// Zeichenkette zurueckgeben
	}

	public static void main(String argv[]) {

		char[] zeile, ergebnis;
		zeile = IO.readChars("Bitte Zeichenkette eingeben: "); // Zeichenkette  einlesen
		ergebnis = sortieren(zeile); 												// Bucket-Sort aufrufen
		System.out.print("sortiert mit Bucket-Sort: ");
		System.out.println(ergebnis);
	}
}





