package a007_sort.einfuehrung;

/*****************************************************************
 * Partitionieren der Folge in groessere und kleinere Haelfte sortieren der
 * hinteren haelfte - Trennelement suchen mischen
 ******************************************************************/

public class a_05_QuickSort_v1 {
	private long[] daten;
	private int laenge;

	public a_05_QuickSort_v1(int max) {
		daten = new long[max];
		laenge = 0;
	}

	public void einfuegen(long wert) {
		daten[laenge] = wert;
		laenge++;
	}

	public void anzeigen() {
		for (int j = 0; j < laenge; j++)
			System.out.print(daten[j] + " ");
		System.out.println();
	}

	public void quickSort() {
		rekQuickSort(0, laenge - 1);
	}

	public void rekQuickSort(int links, int rechts) {
		System.out.println("QS Eintsieg  - links: " + links + " - rechts: " + rechts);
		if (rechts - links <= 0) 					// wenn <= 1 dann schon sortiert
			return;
		else 										// 2 oder groesser
		{
			long pivot = daten[rechts]; 			// pivot wird rechter wert
													// bereichseinteilung
			int bereich = bereichsDaten(links, rechts, pivot);
			System.out.println("pivot: " + pivot);
			rekQuickSort(links, bereich - 1); 		// sortieren linke seite
			rekQuickSort(bereich + 1, rechts); 		// sortieren rechte seite
		}
	}

	public int bereichsDaten(int links, int rechts, long pivot) {
		int linksPtr = links - 1; 					// links laeuft nach rechts
		int rechtsPtr = rechts; 					// rechts laeuft nach links
		while (true) { 								// finden groesseren wert
			while (daten[++linksPtr] < pivot)
				;
													// finden kleineren wert
			while (rechtsPtr > 0 && daten[--rechtsPtr] > pivot)
				;

			if (linksPtr >= rechtsPtr) 				// zeiger kreuzen sich, bereich sortiert
				break;
			else
				tauschen(linksPtr, rechtsPtr);
		}
		tauschen(linksPtr, rechts); 				// speichern pivot und rueckgabe pivot position
		return linksPtr;
	}

	public void tauschen(int d1, int d2) {
		long temp = daten[d1];
		daten[d1] = daten[d2];
		daten[d2] = temp;
	}

	public static void main(String[] args) {
		int groesse = 20;
		a_05_QuickSort_v1 arr = new a_05_QuickSort_v1(groesse); // anlegen array

		// array fuellen
		arr.einfuegen(77);
		arr.einfuegen(29);
		arr.einfuegen(34);
		arr.einfuegen(45);
		arr.einfuegen(52);
		arr.einfuegen(68);
		arr.einfuegen(71);
		arr.einfuegen(80);
		arr.einfuegen(96);
		arr.einfuegen(33);
		arr.einfuegen(17);
		arr.einfuegen(19);
		arr.einfuegen(37);
		arr.einfuegen(44);
		arr.einfuegen(55);
		arr.einfuegen(66);
		arr.einfuegen(79);
		
		
		
		System.out.print("Quicksort\n");
		System.out.print("----------------\n");

		System.out.print("Daten unsortiert: \n");
		arr.anzeigen();

		arr.quickSort();

		System.out.print("Daten sortiert  : \n");
		arr.anzeigen();
	}

}