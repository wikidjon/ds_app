package a007_sort.einfuehrung;
/*************************************************************************

Die Schnittstelle Comparable kommt aus dem java.lang-Paket und deklariert eine Methode:

interface java.lang.Comparable<T>

int compareTo( T o ) Vergleicht sich mit einem anderen.

o1.compareTo( o2 ) 		< 0 		wenn o1 < o2
o1.compareTo( o2 ) 		== 0 	wenn o1 == o2
o1.compareTo( o2 ) 		> 0 		wenn o1 > o2


In Java gibt es zwei unterschiedliche Schnittstellen (in zwei unterschiedlichen Paketen) zur Bestimmung der Ordnung:

Comparable: Implementiert eine Klasse Comparable, so k�nnen sich die Objekte selbst mit anderen Objekten vergleichen. 
			Da die Klassen im Allgemeinen nur ein Sortierkriterium implementieren, wird hier�ber eine nat�rliche Ordnung 
			realisiert.

Comparator: Eine implementierende Klasse, die sich Comparator nennt, nimmt zwei Objekte an und vergleicht sie. 
			Ein Comparator f�r R�ume k�nnte zum Beispiel nach der Anzahl der Personen oder auch nach der Gr��e in Quadratmetern vergleichen; 
			die Implementierung von Comparable w�re nicht sinnvoll, weil hier nur ein Kriterium nat�rlich umgesetzt werden kann, 
			ein Raum aber nicht die Ordnung hat.

W�hrend Comparable �blicherweise nur ein Sortierkriterium umsetzt, kann es viele Extraklassen vom Typ Comparator geben, 
die jeweils unterschiedliche Ordnungen definieren.

************************************************************************************/