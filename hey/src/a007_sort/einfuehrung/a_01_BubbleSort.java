package a007_sort.einfuehrung;

public class a_01_BubbleSort {

	private long[] a;
	private int anzElemente;

	public a_01_BubbleSort(int max) {
		a = new long[max];
		anzElemente = 0;
	}

	// fuellen des Arrays
	public void einfuegen(long wert) {
		a[anzElemente] = wert;
		anzElemente++;
	}

	// anzeigen der array inhalte
	public void anzeigen() {
		for (int j = 0; j < anzElemente; j++)
			System.out.print(a[j] + " ");
		System.out.println();
	}

	public void bubbleSort() {
		int aussen, in;

		for (aussen = anzElemente - 1; aussen > 1; aussen--)
			// aeussere schleife
			for (in = 0; in < aussen; in++)
				// innere schleife
				if (a[in] > a[in + 1])
					tausch(in, in + 1); // tausch
	}

	private void tausch(int li, int re) {
		long temp = a[li];
		a[li] = a[re];
		a[re] = temp;
	}

	public static void main(String[] args) {
		int groesse = 100;
		a_01_BubbleSort arr = new a_01_BubbleSort(groesse);

		arr.einfuegen(77);
		arr.einfuegen(66);
		arr.einfuegen(44);
		arr.einfuegen(34);
		arr.einfuegen(22);
		arr.einfuegen(88);
		arr.einfuegen(12);
		arr.einfuegen(00);
		arr.einfuegen(55);
		arr.einfuegen(33);

		System.out.print("Daten unsortiert: \n");
		arr.anzeigen();

		arr.bubbleSort();
		System.out.print("\nDaten sortiert: \n");
		arr.anzeigen();
	}
}