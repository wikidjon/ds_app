package a007_sort.einfuehrung;

/*******************************************************************************
 * kleinstes Element finden - tauschen gegen erstes // zweites etc.
 ******************************************************************************/

public class a_02_SelectionSort {
	private long[] a;
	private int anzElemente;

	public a_02_SelectionSort(int max) {
		a = new long[max];
		anzElemente = 0;
	}

	public void einfuegen(long wert) {
		a[anzElemente] = wert;
		anzElemente++;
	}

	public void anzeige() {
		for (int j = 0; j < anzElemente; j++)
			System.out.print(a[j] + " ");
		System.out.println("");
	}

	public void selectionSort() {
		int aussen, innen, min;

		for (aussen = 0; aussen < anzElemente - 1; aussen++) {
			min = aussen;
			for (innen = aussen + 1; innen < anzElemente; innen++)
																// minimum suchen
				if (a[innen] < a[min]) 				// min groesser,
					min = innen; 						// neues min
			
			System.out.println("Tausch: Min auf Pos " + min + " - mit aussen " + aussen);
			tausch(aussen, min); 					// tausch
		}
	}

	private void tausch(int eins, int zwei) {
		long temp = a[eins];
		a[eins] = a[zwei];
		a[zwei] = temp;
	}

	public static void main(String[] args) {
		int groesse = 100;
		a_02_SelectionSort arr = new a_02_SelectionSort(groesse);
			// Konstruktor 	- array anlegen

		arr.einfuegen(77);
		arr.einfuegen(29);
		arr.einfuegen(34);
		arr.einfuegen(45);
		arr.einfuegen(52);
		arr.einfuegen(68);
		arr.einfuegen(71);
		arr.einfuegen(38);
		arr.einfuegen(41);
		arr.einfuegen(33);

		System.out.print("Selectionsort\n");
		System.out.print("-----------------\n");
		System.out.print("Daten unsortiert: \n");

		arr.anzeige();

		System.out.print("-----------------\n");

		arr.selectionSort();

		System.out.print("Daten sortiert: \n");
		arr.anzeige();
	}
}

/*******************************************************************************
 * public class SelectionSort { public void sort(Integer list[]) { int min = 0;
 * for (int i = 0; i < list.length; i++) { min = i; // Position des Minimums //
 * Restliste ab Minimum durchlaufen und nach kleinerem Element // suchen for
 * (int j = min + 1; j < list.length; j++) // Wenn kleineres Element gefunden
 * dann index speichern if (list[j] < list[min]) min = j; int temp = list[i];
 * list[i] = list[min]; list[min] = temp; } } }
 ******************************************************************************/
