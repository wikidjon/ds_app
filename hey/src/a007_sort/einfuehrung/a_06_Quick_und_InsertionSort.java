package a007_sort.einfuehrung;
public class a_06_Quick_und_InsertionSort {
	private long[] daten;

	private int laenge;

	public a_06_Quick_und_InsertionSort(int max) {
		daten = new long[max];
		laenge = 0;
	}

	public void einfuegen(long wert) {
		daten[laenge] = wert;
		laenge++;
	}

	public void anzeigen() {
		System.out.print("Daten: ");
		for (int j = 0; j < laenge; j++)
			System.out.print(daten[j] + " ");
		System.out.println("");
	}

	public void quickSort() {
		rekQuickSort(0, laenge - 1);
	}

	public void rekQuickSort(int links, int rechts) {
		int groesse = rechts - links + 1;
		if (groesse < 10) 						// insertionsort bei wenigen werten
			insertionSort(links, rechts);
		else {									// quicksort bei vielen werten
			long mitte = mitteVon3(links, rechts);
			int bereich = bereichBilden(links, rechts, mitte);
			rekQuickSort(links, bereich - 1);
			rekQuickSort(bereich + 1, rechts);
		}
	}

	public long mitteVon3(int links, int rechts) {
		int mitte = (links + rechts) / 2;
												
		if (daten[links] > daten[mitte])		// sortieren links & mitte
			tauschen(links, mitte);
		if (daten[links] > daten[rechts])		// sortieren links & rechts
			tauschen(links, rechts);
		if (daten[mitte] > daten[rechts])   	// sortieren mitte & rechts
			tauschen(mitte, rechts);

		tauschen(mitte, rechts - 1);
		return daten[rechts - 1];
	}

	public void tauschen(int d1, int d2) {
		long temp = daten[d1];
		daten[d1] = daten[d2];
		daten[d2] = temp;
	}

	public int bereichBilden(int links, int rechts, long pivot) {
		int linksPtr = links; 					// rechts vom ersten element
		int rechtsPtr = rechts - 1; 			// links vom pivot
		while (true) {
												
			while (daten[++linksPtr] < pivot);		// finden groesseres
													
			while (daten[--rechtsPtr] > pivot);		// finden kleineres
			
			if (linksPtr >= rechtsPtr) 				// zeiger kreuzen sich, bereich sortiert
				break;
			else
				tauschen(linksPtr, rechtsPtr);
		}
		tauschen(linksPtr, rechts - 1); 			// pivot erneuern
		return linksPtr; 							// pivot position
	}

	public void insertionSort(int links, int rechts) {
		int innen, aussen;
													// von links nach aussen
		for (aussen = links + 1; aussen <= rechts; aussen++) {
			long temp = daten[aussen]; 				
			innen = aussen; 						
													// solange ein element kleiner ist
			while (innen > links && daten[innen - 1] >= temp) {
				daten[innen] = daten[innen - 1]; 	// verschieben nach rechts
				--innen; 							// nach links gehen
			}
			daten[innen] = temp; 					// einfuegen element
		}
	}

	public static void main(String[] args) {
		int maxGroesse = 26;
		a_06_Quick_und_InsertionSort arr = new a_06_Quick_und_InsertionSort(
				maxGroesse);

		for (int j = 0; j < maxGroesse; j++) {
			long n = (int) (java.lang.Math.random() * 99);
			arr.einfuegen(n);
		}
		System.out.print("Quick- und Insertionsort\n");
		System.out.print("------------------------\n");

		System.out.print("Daten unsortiert:\n");
		arr.anzeigen();
		arr.quickSort();
		System.out.print("Daten sortiert:\n");
		arr.anzeigen();
	}
}