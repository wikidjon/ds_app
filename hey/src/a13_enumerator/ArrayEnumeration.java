package a13_enumerator;

import java.lang.reflect.Array;
import java.util.Enumeration;

public class ArrayEnumeration implements Enumeration<Object> {
	
	//Schnittstelle Enumeration [Nicht Enumerable! ] gibt es seit den ersten Java-Tagen; 
	//die Schnittstelle Iterator gibt es seit Java 1.2, seit der Collection-API. 
	//der Typ Iterator ist jedoch deutlich weiter verbreitet. 
	
	private final int size;
	private int cursor;
	private final Object array;

	public ArrayEnumeration(Object obj) {
		Class<? extends Object> type = obj.getClass();
		if (!type.isArray()) {
			throw new IllegalArgumentException("Unzulaessiger Typ: " + type);
		}
		size = Array.getLength(obj);
		array = obj;
	}

	public boolean hasMoreElements() {
		return (cursor < size);
	}

	public Object nextElement() {
		return Array.get(array, cursor++);
	}

	public static void main(String args[]) {
		
		Object obj = new int[] { 2, 3, 5, 8, 13, 21 };
		ArrayEnumeration e = new ArrayEnumeration(obj);
		
		while (e.hasMoreElements()) {
			System.out.println(e.nextElement());
		}
		try {
			e = new ArrayEnumeration(ArrayEnumeration.class);
		} catch (IllegalArgumentException ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
}