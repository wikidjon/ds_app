package a13_enumerator;

// Iterator an Enumeration binden
// Schnittstelle Enumeration gibt es seit den ersten Java-Tagen; 
// die Schnittstelle Iterator gibt es seit Java 1.2, seit der Collection-API. 
// der Typ Iterator ist jedoch deutlich weiter verbreitet. 

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

public class EnumerationIterator1 {
	public static Iterator<?> iterator(final Enumeration<String> e) {
		return new Iterator<Object>() {
		
			public boolean hasNext() {
				return e.hasMoreElements();
			}

			public Object next() {
				return e.nextElement();
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	
	public static void main(String args[]) {
		
		String elements[] = { "Jimi", "Hendrix", "und", "Udo", "Juergens", "." };
		Vector<String> v = new Vector<String>(Arrays.asList(elements));   //dynamisches Array
		Enumeration<String> e = v.elements();
		Iterator<?> iter = EnumerationIterator1.iterator(e);
		
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
	}
}
