package a16_arrays;

import java.util.ArrayList;

public class ArrayListDemo {
  public static void main(String[] argv) {
    ArrayList<Integer> al = new ArrayList<Integer>();
    
    // einfuegen von elementen in Arrayliste
    al.add(5);
    al.add(7);
    al.add(9);
    al.add(100);
    al.add(23);

    System.out.println("Eingaben ueber index ausgelesen:");
    for (int i = 0; i < al.size(); i++) {
      System.out.println("Element " + i + " = " + al.get(i));
    }
  }
}