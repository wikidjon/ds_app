package hash_set;
//Remove one set from another set
import java.util.HashSet;
import java.util.Set;

public class FindDups2 {
  public static void main(String[] args) {
    Set<String> uniques = new HashSet<String>();
    Set<String> dups = new HashSet<String>();

    for (String a : args)
      if (!uniques.add(a))
        dups.add(a);

    uniques.removeAll(dups);

    System.out.println("einzelne Woerter:    " + uniques);
    System.out.println("mehrfache Woerter: " + dups);
  }
}
