package a00_listen;

public class ListeTest {
    
    public static void main(String [] args) {
        Liste liste = new Liste();
        System.out.println("Liste angelegt....");
        System.out.println("---------------------------\n");
        
        System.out.println("Die Liste sollte leer sein.");
        liste.durchlaufe();
        System.out.println("Anzahl der Listenelemente = " + liste.zaehleElemente());
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der Einf�gen-Methoden....");
        liste.einfuegeKopf(new Integer(3));
        liste.einfuegeKopf(new Integer(2));
        liste.einfuegeKopf(new Integer(1));
        liste.einfuegeEnde(new Integer(4));
        liste.einfuegeEnde(new Integer(5));
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 4, 5 enthalten.");
        liste.durchlaufe();
        System.out.println("Anzahl der Listenelemente = " + liste.zaehleElemente());
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der loescheKopf() und loescheEnde()-Methoden....");
        liste.loescheKopf();
        liste.loescheEnde();
        System.out.println("Die Liste sollte jetzt die Elemente 2, 3, 4 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... nochmaliger Aufruf der loescheKopf() und loescheEnde()-Methoden....");
        liste.loescheKopf();
        liste.loescheEnde();
        System.out.println("Die Liste sollte jetzt nur noch das Element 3 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der loescheEnde()-Methode....");
        liste.loescheEnde();
        System.out.println("Die Liste sollte jetzt leer sein.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        
    /*
    System.out.println("Jetzt wird eine NullPointerException ausgeloest.");
          liste.loescheKopf();
     */
        
        System.out.println("... nochmaliger Aufruf der einfuegeKopf()-Methode....");
        liste.einfuegeKopf(new Integer(5));
        liste.einfuegeKopf(new Integer(4));
        liste.einfuegeKopf(new Integer(3));
        liste.einfuegeKopf(new Integer(2));
        liste.einfuegeKopf(new Integer(1));
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 4, 5 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der finde(3)-Methode....");
        liste.finde(new Integer(3));
        // liste.aktuell sollte jetzt auf das Element mit dem Inhalt 3 zeigen.
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der einfuegeHinter(33)-Methode....");
        liste.einfuegeHinter(new Integer(33));
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 4, 5 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der finde(33)-Methode....");
        liste.finde(new Integer(33));
        // liste.aktuell sollte jetzt auf das Element mit dem Inhalt 33 zeigen.
        System.out.println("... Aufruf der loescheNachfolger()-Methode....");
        liste.loescheNachfolger();
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 5 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der loescheNachfolger()-Methode....");
        liste.loescheNachfolger();
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 33 enthalten.");
        liste.durchlaufe();
        // liste.aktuell sollte jetzt den Wert null haben.
        System.out.println("---------------------------\n");
        
        System.out.println("Jetzt wird eine NullPointerException ausgeloest.");
        // (Knoten mit Attribut daten == Integer(5) hat keinen Nachfolger.)
        liste.loescheNachfolger();
    }
}