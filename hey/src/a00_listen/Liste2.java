package a00_listen;

import java.util.*;
/** 
* Prioritaetswarteschlange
* Realisiert auf einer doppelt verketteten Liste mit einem zus�tzlichen
* Feld von Priorit�ts-Zeigern.
*/
public class Liste2
{
    // eigentlich private, nur f�r Grafikausgabe public
    public static int PrioritaetenAnzahl = 5;  // ggf. static
    
    //Attribute
    protected Link Anfang;
    protected Link Ende;
    protected Link AktuellerZeiger;
    //Neue Attribute eigentlich protected, f�r Grafikausgabe aber public
    public Link PrioritaetenZeigerAnfang[] = new Link[PrioritaetenAnzahl];
    public Link PrioritaetenZeigerEnde[] = new Link[PrioritaetenAnzahl];
   
    //Konstruktor
    public Liste2()
    {
        Anfang = null;
        Ende = null;
        AktuellerZeiger = null; 
        for (int i = 0; i < PrioritaetenAnzahl; i++)
        {
            PrioritaetenZeigerAnfang[i] = null;
            PrioritaetenZeigerAnfang[i] = null;
        }
    }
    
    public int getPrio(){
    	return PrioritaetenAnzahl;
    }
  
  
    public void enqueue(Object neuesElement, int Prioritaet) throws IndexOutOfBoundsException
    {
        if (Prioritaet < PrioritaetenAnzahl)
        {
            if (PrioritaetenZeigerAnfang[Prioritaet] == null)
            {
                int i = Prioritaet - 1;
                while ((i > 0) && (PrioritaetenZeigerEnde[i] == null))
                {
                    i--;
                }
                if (i < 0 || PrioritaetenZeigerEnde[i] == null)
                    setzeAktuellerZeigerZurueck();
                else
                    AktuellerZeiger = PrioritaetenZeigerEnde[i].Naechster;
                    
                einfuegenElement(neuesElement, Prioritaet);
                PrioritaetenZeigerAnfang[Prioritaet] = AktuellerZeiger;
                PrioritaetenZeigerEnde[Prioritaet] = AktuellerZeiger;
            }
            else
            {
                AktuellerZeiger = PrioritaetenZeigerAnfang[Prioritaet];
                einfuegenElement(neuesElement, Prioritaet); 
                PrioritaetenZeigerAnfang[Prioritaet] = AktuellerZeiger;
            }
        }
        else
            throw new IndexOutOfBoundsException("Priorit�ten nur bis " + (PrioritaetenAnzahl - 1) + "!");
    }
   
    // Element entfernen, innerhalb der Priorit�ten soll FIFO gelten
    public Link FIFOdequeue() throws NoSuchElementException
    {
        for (int i = PrioritaetenAnzahl - 1; i >= 0; i--)
        {
            if (PrioritaetenZeigerEnde[i] != null)
            {
                AktuellerZeiger = PrioritaetenZeigerEnde[i];
                Link einLink = entfernenElement(); 
                if (AktuellerZeiger == null)
                {
                    Anfang = null;
                    Ende = null;
                    PrioritaetenZeigerAnfang[i] = null;
                    PrioritaetenZeigerEnde[i] = null;
                }
                else
                {
                    if (AktuellerZeiger.Prioritaet == i)
                        PrioritaetenZeigerEnde[i] = AktuellerZeiger;
                    else
                    {
                        PrioritaetenZeigerAnfang[i] = null;
                        PrioritaetenZeigerEnde[i] = null;
                    }
                }
                return einLink;
            }
        }
        throw new NoSuchElementException("Keine Elemente vorhanden");
    }

    // Element entfernen, innerhalb der Priorit�ten soll LIFO gelten
    public Link LIFOdequeue() throws NoSuchElementException
    {

        for (int i = PrioritaetenAnzahl - 1; i >= 0; i--)
        {
            if (PrioritaetenZeigerAnfang[i] != null) // Diese Zeile f�r LIFO ge�ndert
            {
                AktuellerZeiger = PrioritaetenZeigerAnfang[i]; // Diese Zeile f�r LIFO ge�ndert
                Link einLink = entfernenElement(); 
                if (AktuellerZeiger == null)
                {
                    Anfang = null;
                    Ende = null;
                    PrioritaetenZeigerAnfang[i] = null;
                    PrioritaetenZeigerEnde[i] = null;
                }
                else
                {
                    if (AktuellerZeiger.Prioritaet == i)
                        PrioritaetenZeigerAnfang[i] = AktuellerZeiger; // Diese Zeile f�r LIFO ge�ndert
                    else
                    {
                        PrioritaetenZeigerAnfang[i] = null;
                        PrioritaetenZeigerEnde[i] = null;
                    }
                }
                return einLink;
            }
        }
        throw new NoSuchElementException("Keine Elemente vorhanden");
    }

    //setzeAktuellerZeigerZurueck
    //Zur�cksetzen des aktuellen Zeigers auf den Listenanfang
    public void setzeAktuellerZeigerZurueck()
    {
        AktuellerZeiger = Anfang;
    }
    
    //Einf�gen vor die aktuelle Zeigerposition
    public void einfuegenElement(Object neuesElement, int Prioritaet)
    {
        if (AktuellerZeiger == null) //Leere Liste
        {
            anfuegenElement(neuesElement, Prioritaet);
            return;
        }
        Link Neu = new Link(neuesElement, AktuellerZeiger.Vorgaenger, AktuellerZeiger, Prioritaet);
        AktuellerZeiger.Vorgaenger = Neu; 
        AktuellerZeiger = Neu;
        if (AktuellerZeiger.Vorgaenger == null) // Neues Anfangs-Element
            Anfang = Neu;
        else
            AktuellerZeiger.Vorgaenger.Naechster = Neu; 
    }
    //anfuegenElement
    //Anf�gen am Ende der Liste
    public void anfuegenElement(Object neuesElement, int Prioritaet)
    {
        Link Neu = new Link(neuesElement, Ende, null, Prioritaet);
        if (Ende == null) // Leere Liste ?
        {
            Anfang = Neu;
            Ende = Neu;
            AktuellerZeiger = Neu;
        }
        else
        {
            Ende.Naechster = Neu;
            Ende = Neu;
            AktuellerZeiger = Neu;
        }
    }

    //entfernenElement
    //Element unter dem aktuellen Zeiger entfernen
    //Ergebnis =  das entfernte Element
    //Ausnahme = NoSuchElementException, wenn bereits am Ende der Liste
    public Link entfernenElement()throws NoSuchElementException
    {
        if (AktuellerZeiger == null) // Liste ist leer!
            throw new NoSuchElementException("Liste ist leer!");

        Link AktuellesObjekt = AktuellerZeiger;
        
        if (AktuellerZeiger.Vorgaenger == null) // Erstes Element soll gel�scht werden
            Anfang = AktuellerZeiger.Naechster;
        else
            AktuellerZeiger.Vorgaenger.Naechster = AktuellerZeiger.Naechster;
        
        if (AktuellerZeiger.Naechster == null) // Letztes Element der Liste soll gel�scht werden
            Ende = null;
        else
            AktuellerZeiger.Naechster.Vorgaenger = AktuellerZeiger.Vorgaenger;

        if (AktuellerZeiger.Naechster == null)
        	AktuellerZeiger = AktuellerZeiger.Vorgaenger;
        else
        	AktuellerZeiger = AktuellerZeiger.Naechster;

        return AktuellesObjekt;
    }
    //elements
    public Enumeration<?> elements()
    {
        return new ListeEnumeration(Anfang, Ende);
    }
    public Link getAnfang()
    {
        return Anfang; 
    }
    public Link getEnde()
    {
        return Ende; 
    }
}
