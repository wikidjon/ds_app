package a80_generics;

/*
Java 2, v5.0 (Tiger) New Features
by Herbert Schildt
ISBN: 0072258543
Publisher: McGraw-Hill/Osborne, 2004
*/
// Stats attempts (unsuccessfully) to create a generic class that can compute 
// the average of an array of numbers of any given type. 

// The class contains an error! 
public class Stats<T> {  
  T[] nums; // nums is an array of type T 
    
  // Pass the constructor a reference to   
  // an array of type T. 
  Stats(T[] o) {  
    nums = o;  
  }  
  
  // Return type double in all cases. 
  double average() {  
    double sum = 0.0; 
 
    for(int i=0; i < nums.length; i++)  
   // 	sum += ((Object) nums[i]).doubleValue(); // Error!!! 
    sum=2.0;   // zur Vermeidung von Fehlerausschrift
 
    return sum / nums.length; 
  }  
}
