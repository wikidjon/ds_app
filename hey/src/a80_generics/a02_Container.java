package a80_generics;
class a02_Container<T> 
{ 
  @SuppressWarnings("unused")
private T wert1, wert2; 
 
  void setWert( T wert1, T wert2 ){  // die 2 paramter dienen
 									 // zur Ueberprufung in der main
    this.wert1 = wert1; 
  } 
 
  T getWert() 
  { 
    return wert1; 
  } 
}

/*****************************************************
Anstelle des konkreten Typs String, Integer etc. steht 
ein formaler Typ-Parameter T (Typ)
******************************************************/