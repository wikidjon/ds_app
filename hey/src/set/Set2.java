package set;
// Putting your own type in a Set.

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Set2 {

  public static Set<Integer> fill(Set<Integer> a, int size) {
    for (int i = 0; i < size; i++)
      a.add(new Integer(i));
    return a;
  }

  public static void test(Set<Integer> a) {
    fill(a, 10);
    fill(a, 10); // Try to add duplicates
    fill(a, 10);
    a.addAll(fill(new TreeSet<Integer>(), 10));
    System.out.println(a);
  }

  public static void main(String[] args) {
    test(new HashSet<Integer>());
    test(new TreeSet<Integer>());
    test(new LinkedHashSet<Integer>());
  }
} ///:~

