package a11_collections;

// Iterator an Enumeration binden
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

public class EnumerationIterator1 {

	public static Iterator<?> iterator(final Enumeration<String> e) {
		return new Iterator<Object>() {

			public boolean hasNext() {
				return e.hasMoreElements();
			}

			public Object next() {
				return e.nextElement();
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	// Vector ist ein "Array" mit variabler L�nge.
	// Der Zugriff auf die Elemente erfolgt �ber Indizes, jedoch nicht �ber den Operator [],
	// aber �ber Methoden, die einen Index als Parameter annehmen.
	// In einem Vector kann jedes Exemplar einer von Object abgeleiteten Klasse gespeichert werden,
	// so dass ein Vector nicht auf bestimmte Datentypen fixiert ist.
	
	public static void main(String args[]) {
		String elements[] = { "Jimi", "Hendrix", "und", "Udo", "Juergens", "." };
		Vector<String> v = new Vector<String>(Arrays.asList(elements)); // "dynamisches Array"
		Enumeration<String> e = v.elements();
		Iterator<?> iter = EnumerationIterator1.iterator(e);

		while (iter.hasNext()) {
			System.out.println(iter.next());
		}
	}
}
