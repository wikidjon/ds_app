package a11_collections;

import java.util.Vector;

// Vector ist ein "Array" mit variabler L�nge. 
// Der Zugriff auf die Elemente erfolgt �ber Indizes, jedoch nicht �ber den Operator [], 
// aber �ber Methoden, die einen Index als Parameter annehmen. 
// In einem Vector kann jedes Exemplar einer von Object abgeleiteten Klasse gespeichert werden, 
// so dass ein Vector nicht auf bestimmte Datentypen fixiert ist. 

public class FindVector2 {
	static String inhalte[] = { "Angela", "Merkel", "Emerson", "Lake", "und",
			"Palmer", "Pink", "Floyd", "Al", "Jarreau" };

	public static void main(String args[]) {
		Vector<String> v = new Vector<String>();
		for (int i = 0, n = inhalte.length; i < n; i++) {
			v.add(inhalte[i]);
		}
		System.out.println("Vector: " + v);
		System.out.println("------------------------------------------------------------------------------");
		System.out.println("enthalten Beatles?: " + v.contains("Beatles"));
		System.out.println("enthalten Emerson?: " + v.contains("Emerson"));
		System.out.println("Wo ist Anglea?: " + v.indexOf("Angela"));
		System.out.println("Wo ist Palmer?: " + v.indexOf("Palmer"));
		System.out.println("Wo ist Palmer vom Ende?: " + v.lastIndexOf("Palmer"));
		int index = 0;
		int length = v.size();
		while ((index < length) && (index >= 0)) {
			index = v.indexOf("Palmer", index);
			if (index != -1) {
				System.out.println("\nIndex: " + index);
				System.out.println(v.get(index));
				index++;
			}
		}
	}
}