package a11_collections;

//Benutzung der synchronized() Methode
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public class Synchronization {

	private static Collection<Object> c;
	private static Map<Object, Object> m;

	public static void main(String[] args) {
		c = Collections.synchronizedCollection(new ArrayList<Object>());
		List<Object> list = Collections.synchronizedList(new ArrayList<Object>());
		Set<Object> s = Collections.synchronizedSet(new HashSet<Object>());
		m = Collections.synchronizedMap(new HashMap<Object, Object>());
	}
}
