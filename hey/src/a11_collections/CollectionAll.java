package a11_collections;
//beispiele fuer collections: List, Set, SortedSet, LinkedHashSet, Map, LinkedHashMap

// HashMap gibt keine Garantie �ber Iterationsreihenfolge. 
// kann durch add neuer Elemente vollst�ndig ge�ndert werden.

// TreeMap iteriert nach einer nat�rlichen Ordnung der keys, die mit compareTo() verglichen werden

// das SortedMap interface enth�lt Methoden, die die Sortierordnung ber�cksichtigen.

// LinkedHashMap iteriert in der Ordnung, in der die Eintr�ge in den Baum eingef�gt werden

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;


public class CollectionAll {
	public static void main(String[] args) {
		List<String> list1 = new LinkedList<String>();
		list1.add("list1_1");
		list1.add("list1_2");
		list1.add("list1_3");
		list1.add("list1_4");
		System.out.println("\n************* LinkedList ************");
		traverse(list1);
//*****************************************************************		
		List<String> list2 = new ArrayList<String>();
		list2.add("ar_list1_1");
		list2.add("ar_list1_2");
		list2.add("ar_list1_3");
		list2.add("ar_list1_4");
		System.out.println("\n************* ArrayList ************");
		traverse(list2);
//*****************************************************************		
		Set<String> set1 = new HashSet<String>();
		//verwaltet die Elemente in einer schnellen hash-basierten Datenstruktur
		set1.add("hash_set_1");
		set1.add("hash_set_2");
		set1.add("hash_set_3");
		set1.add("hash_set_4");
		System.out.println("\n************** HashSet ************");
		traverse(set1);
//*****************************************************************		
		SortedSet<String> set2 = new TreeSet<String>();
		//verwaltet die Elemente immer sortiert (intern werden die Elemente in einem balancierten Bin�rbaum gehalten)
		set2.add("tree_set_1");
		set2.add("tree_set_2");
		set2.add("tree_set_3");
		set2.add("tree_set_4");
		System.out.println("\n*************** TreeSet ************");
		traverse(set2);
//*****************************************************************		
		LinkedHashSet<String> set3 = new LinkedHashSet<String>();
		//vereint die Reihenfolge einer Liste und die Performance f�r Mengenoperationen vom HashSet
		set3.add("linked_hash_set_1");
		set3.add("linked_hash_set_2");
		set3.add("linked_hash_set_3");
		set3.add("linked_hash_set_4");
		System.out.println("\n************** LinkedHashSet ************");
		traverse(set3);
//*****************************************************************		
		Map<String, String> m1 = new HashMap<String, String>();
		// Java implementiert Assoziativspeicher mit eigenen Klassen
		// die Schl�sselobjekte m�ssen �hashbar� sein, also equals() und hashCode() konkret implementieren
		m1.put("key1", "value1");
		m1.put("key2", "value2");
		m1.put("key3", "value3");
		m1.put("key4", "value4");
		System.out.println("\n\n**************HashMap -Keys ************");
		traverse(m1.keySet());
		System.out.println("*************HashMap -Values **********");
		traverse(m1.values());
//*****************************************************************		
		SortedMap<String, String> m2 = new TreeMap<String, String>();
		// etwas langsamer im Zugriff als Hashmap, doch daf�r sind alle Schl�sselobjekte immer sortiert . 
		// sortiert die Elemente in einen internen Bin�rbaum 
		m2.put("tree_map1", "value1");
		m2.put("tree_map2", "value2");
		m2.put("tree_map3", "value3");
		m2.put("tree_map4", "value4");
		System.out.println("\n\n************ TreeMap -Keys *************");
		traverse(m2.keySet());
		System.out.println("************ TreeMap -Values *************");
		traverse(m2.values());
//*****************************************************************		
		LinkedHashMap /* von String zu String */<String, String> m3 = new LinkedHashMap<String, String>();
		m3.put("linked_hash_map1", "value1");
		m3.put("linked_hash_map2", "value2");
		m3.put("linked_hash_map3", "value3");
		m3.put("linked_hash_map4", "value4");
		System.out.println("\n\nLinked HashMap -Keys \n*******************************");
		traverse(m3.keySet());
		System.out.println("Linked HashMap -Values \n*******************************");
		traverse(m3.values());
	}

	static void traverse(Collection<String> coll) {
		Iterator<String> iter = coll.iterator();
		while (iter.hasNext()) {
			String elem = (String) iter.next();
			System.out.print(elem + " * ");
		}
		System.out.println();
	}

}