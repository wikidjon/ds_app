package vector;
import java.util.Vector;
public class RemoveVector {
  static boolean removeAll(Vector<String> v, Object e) {
    Vector<Object> v1 = new Vector<Object>();
    v1.add(e);
    return v.removeAll(v1);
  }
  public static void main (String args[]) {
    Vector<String> v = new Vector<String>();
    for (int i=0, n=args.length; i<n; i++) {
      v.add(args[i]);
    }
    System.out.println(removeAll(v, args[0]));
    System.out.println("vector: " + v);
  }
}