package vector;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

//Serialisieren eines Vectors
public class SaveVector {
  public static void main(String args[]) throws Exception {
    String data[] = { "Java", "Vorlesung", "und", "Uebung", "."};
    Vector<String> v = new Vector<String>(Arrays.asList(data));
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    ObjectOutputStream o = new ObjectOutputStream(b);
    o.writeObject(v);
    o.close();
    ByteArrayInputStream bb = new ByteArrayInputStream(b.toByteArray());
    ObjectInputStream oo = new ObjectInputStream(bb);
    Vector<?> v2 = (Vector<?>) oo.readObject();
    Enumeration<String> e = v.elements();
    Enumeration<?> e2 = v2.elements();

    while (e.hasMoreElements()) {
      System.out.println(e.nextElement());
    }
    while (e2.hasMoreElements()) {
        System.out.println(e2.nextElement());
      }
  }
}