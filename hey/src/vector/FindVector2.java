package vector;
import java.util.Vector;
//Find Vector
public class FindVector2 {
  static String members[] = { "Angela", "Merkel", "Emerson", "Lake", "und",
      "Palmer", "Pink", "Floyd", "Al", "Jarreau" };

  public static void main(String args[]) {
    Vector<String> v = new Vector<String>();
    for (int i = 0, n = members.length; i < n; i++) {
      v.add(members[i]);
    }
    System.out.println(v);
    System.out.println("enthalten Beatles?: " + v.contains("Beatles"));
    System.out.println("enthalten Emerson?: " + v.contains("Emerson"));
    System.out.println("Wo ist Anglea?: " + v.indexOf("Angela"));
    System.out.println("Wo ist Palmer?: " + v.indexOf("Palmer"));
    System.out.println("Wo ist Palmer vom Ende?: " + v.lastIndexOf("Palmer"));
    int index = 0;
    int length = v.size();
    while ((index < length) && (index >= 0)) {
      index = v.indexOf("Palmer", index);
      if (index != -1) {
        System.out.println(v.get(index));
        index++;
      }
    }
  }
}