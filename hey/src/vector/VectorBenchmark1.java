package vector;
import java.util.Vector;

public class VectorBenchmark1 {

  public static final int MaxSize = 100000;

  public static final int NTRIES = 10;

  public static void main(String[] args) {
    Vector<Integer> v = new Vector<Integer>();

    long start = System.currentTimeMillis();
    for (int i = 0; i < MaxSize; i++)
      v.add(new Integer(i));
    long end = System.currentTimeMillis();
    System.out.println("Allokation der Vector Elemente: " + (end - start)
        + " milli sek");

    Integer[] integerArray = new Integer[1];
    start = System.currentTimeMillis();
    for (int i = 0; i < MaxSize; i++) {
      if (i >= integerArray.length) {
        Integer[] b = new Integer[i * 2];
        System.arraycopy(integerArray, 0, b, 0, integerArray.length);
        integerArray = b;
      }
      integerArray[i] = new Integer(i);
    }
    end = System.currentTimeMillis();
    System.out.println("Allokation der Array Elemente:  " + (end - start)
        + " milli sek");

    start = System.currentTimeMillis();
    for (int j = 0; j < NTRIES; j++)
      for (int i = 0; i < MaxSize; i++) {
        Integer r = (Integer) v.get(i);
        v.set(i, new Integer(r.intValue() + 1));
      }
    end = System.currentTimeMillis();
    System.out.println("Zugriff auf Vektor Elemente:   " + (end - start)
        + " milli sek");

    start = System.currentTimeMillis();
    for (int j = 0; j < NTRIES; j++)
      for (int i = 0; i < MaxSize; i++) {
        Integer r = integerArray[i];
        integerArray[i] = new Integer(r.intValue() + 1);
      }
    end = System.currentTimeMillis();
    System.out.println("Zugriff auf Array Elemente:    " + (end - start)
        + " milli sek");
  }
}