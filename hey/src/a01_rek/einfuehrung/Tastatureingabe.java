package a01_rek.einfuehrung;

import java.io.*;

public class Tastatureingabe	{

	static BufferedReader eing = new BufferedReader(new InputStreamReader(System.in));
	
	public static int readInt (String prompt)		{
		String zeile; 
		while (true)	{
			System.out.print (prompt);
			try		{
				zeile = eing.readLine(); 
				return Integer.parseInt(zeile);	
			} catch (NumberFormatException e)  {
				System.out.println ("Bitte eine ganze Zahl eingeben!!!");
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}


	public static String readString (String prompt)	{
		while (true){
			System.out.print(prompt);
			try			{
	  			return eing.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

	}

}


