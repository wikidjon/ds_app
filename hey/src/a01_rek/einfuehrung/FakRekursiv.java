package a01_rek.einfuehrung;

public class FakRekursiv {
  public static int fakBerechnung(int x) {
    if (x <= 1)  
      return 1; 
    else 
      return x * fakBerechnung(x - 1);
  }

  public static void main(String[] args) {
    int z;
    
    z = Tastatureingabe.readInt("Zahl: ");
    System.out.println("Fakultaet(" + z + "!) = " + fakBerechnung(z));

  }
}
