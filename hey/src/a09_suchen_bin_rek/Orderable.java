package a09_suchen_bin_rek;

/**
 * Beschreibt das Verhalten "vergleichbarer" Objekte
 */
interface Orderable {
   public boolean   equal        (Orderable o);
   public boolean   greater      (Orderable o);
   public boolean   greaterEqual (Orderable o);
   public boolean   less         (Orderable o);
   public boolean   lessEqual    (Orderable o);
   public Orderable minKey       ();
}
