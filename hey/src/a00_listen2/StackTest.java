package a00_listen2;

class StackTest {
    public static void main(String args[]) {
        StackMitListe s = new StackMitListe();
        String str = TastaturEingabe.readString("Bitte Zeichenkette eingeben: ");
        int len = str.length();
        
        for(int i = 0; i < len; i++){
            s.push(new Character(str.charAt(i)));
        }
        System.out.println();
        System.out.println("... Aufruf der durchlaufe()-Methode der Liste....");
        s.durchlaufe();
        System.out.println("---------------------------\n");
 
        System.out.println("... Aufruf der pop()-Methode des Stacks....");
         while(!s.isEmpty()) {
            Object o = s.pop();
            System.out.print(((Character)o).charValue());
        }
         System.out.println();
         System.out.println("---------------------------\n");
 
    }
}
