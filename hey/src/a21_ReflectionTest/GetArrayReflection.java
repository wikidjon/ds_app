package a21_ReflectionTest;

import java.lang.reflect.Array;

public class GetArrayReflection {

  public static void main(String[] args) {
    int[] sourceInts = { 12, 78 };
    int[] destInts = new int[2];
    copyArray(sourceInts, destInts);
    
    String[] sourceStrgs = { "Hallo ", "Studenten der HS Merseburg ", "die Pruefung naht ..." };
    String[] destStrgs = new String[3];
    copyArray(sourceStrgs, destStrgs);
  }

  public static void copyArray(Object source, Object dest) {
    for (int i = 0; i < Array.getLength(source); i++) {
      Array.set(dest, i, Array.get(source, i));
      System.out.println(Array.get(dest, i));
    }
   System.out.println("-----------------");
  }
}
