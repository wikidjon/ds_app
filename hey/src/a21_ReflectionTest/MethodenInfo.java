package a21_ReflectionTest;

import java.lang.reflect.*;

public class MethodenInfo {

	public static void main(String args[]) throws ClassNotFoundException {

		Class<?> refelect = a21_ReflectionTest.TestKlasse.class;
		//Class<?> refelect = Class.forName("java.lang.String");
		//Class<?> refelect = a21_ReflectionTest.ShowClass.class;

		while (refelect != null) {
			System.out.println("================");
			Method[] ref_Methoden = refelect.getDeclaredMethods();

			System.out.println(refelect.getName());
			System.out.println("----------------");
			for (Method m : ref_Methoden) {
				Class<?> retType = m.getReturnType();
				Class<?>[] params = m.getParameterTypes();
				System.out.print(retType.toString() + " " + m.getName() + "(");
				for (Class<?> c : params)
					System.out.print(c.toString() + " ");
				System.out.println(")");
			}
			System.out.println("*******Ende Klasse **************************");
			refelect = refelect.getSuperclass();
		}
	}

}
