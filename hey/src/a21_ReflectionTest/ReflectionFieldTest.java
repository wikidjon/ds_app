package a21_ReflectionTest;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectionFieldTest {

	public static void main(String[] args) {
		String name = "java.util.Date";
		try {
			Class<?> cl = Class.forName(name);
			@SuppressWarnings("unused")
			Class<?> supercl = cl.getSuperclass();
			System.out.println("class: " + name);
			System.out.println("\nIhre Varaiblen:");
			printFields(cl);
			System.out.println();
		} catch (ClassNotFoundException e) {
			System.out.println("Klasse nicht gefunden.");
		}
	}

	public static void printFields(Class<?> cl) {
		Field[] fields = cl.getDeclaredFields();

		for (int i = 0; i < fields.length; i++) {
			Field f = fields[i];
			Class<?> type = f.getType();
			String name = f.getName();
			System.out.print(Modifier.toString(f.getModifiers()));
			System.out.println(" " + type.getName() + " " + name + ";");
		}
	}
}
