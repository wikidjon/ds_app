package a21_ReflectionTest;

import java.lang.reflect.*;

public class Modifizierer {
	public Modifizierer() throws ClassNotFoundException {
		Class<Math> cStr = java.lang.Math.class;
		//Class<?> cStr = Class.forName("java.lang.String");
		//C:\Users\schroet\workspace\algodat_neu\src\a17_reg_expr\.....
		System.out.println("Modifizierer von " + cStr.getName());

		int modif = cStr.getModifiers();
		if (Modifier.isPrivate(modif))
			System.out.println("privat");
		if (Modifier.isFinal(modif))
			System.out.println("final");
		if ((modif & Modifier.PUBLIC) == Modifier.PUBLIC)
			System.out.println("public");
	}

	public static void main(String args[]) throws ClassNotFoundException {
		new Modifizierer();
	}
}
