package a21_ReflectionTest;

import java.lang.reflect.*;

public class VariablenInfo {
	public static void main(String args[]) {

		Class<?> reflect_v = a21_ReflectionTest.TestKlasse.class;

		while (reflect_v != null) {
			System.out.println("================");
			Field[] variable = reflect_v.getDeclaredFields();

			System.out.println(reflect_v.getName());
			System.out.println("----------------");
			for (Field f : variable)
				System.out.println(f.getType() + " " + f.getName());
			reflect_v = reflect_v.getSuperclass();
		}
	}
}
