package a21_ReflectionTest;

public class TestKlasse extends TestKlasse2 {
	private String autor = "unbekannt";
	public int version = 1;
	public boolean test = false;

	public TestKlasse() {
	}

	public TestKlasse(int wert) {
	}

	public String getVersion(int wert1, String wert2) {
		return "1.0";
	}

	public String getName() {
		return "TestKlasse ...";
	}

	@SuppressWarnings("unused")
	private String getAutor() {
		return autor;
	}
}
