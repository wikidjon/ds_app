package a21_ReflectionTest;

public class RefEx3 {
	public static void main(String[] args) {
		Person221 p1 = new Person221(); 			
		p1.hallo();

		try {
			Person221 p2 = (Person221) Class.forName("Person221").newInstance();
			p2.hallo();
		} catch (ClassNotFoundException cnfe) {
			System.out.println("Kann Klasse "+cnfe.toString()+" nicht finden \n"+cnfe.getMessage() );
		} catch (InstantiationException ie) {
			System.out.println("Eine InstantiationException ist aufgetreten \n"+ie.toString()+"\n"+ie.getMessage() );
		} catch (IllegalAccessException iae) {
			System.out.println("Eine IllegalAccessException ist aufgetreten \n"+iae.toString()+"\n"+iae.getMessage() );
		} 
	} 
} 

class Person221 {
	public void hallo() {
		System.out.println("hallo - ich bin person221!");
	} 
} 