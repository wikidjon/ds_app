package a21_ReflectionTest;

import java.lang.reflect.*;

public class InfoKlasseReflect
{
  public static void main(String[] args)
  {
    try
    {
      Class<?> infoKlasse = Class.forName("a21_ReflectionTest.InfoKlasse");
      Method m = infoKlasse.getMethod("zeigeNachricht");
      Object infoObject = infoKlasse.newInstance();
      m.invoke(infoObject);  //ruft die darunterliegende Methode des spezifizierten Objektes
    }
    catch(Exception ex)
    {}
  }
}

