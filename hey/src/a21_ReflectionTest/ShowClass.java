package a21_ReflectionTest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Member;
import java.lang.reflect.Field;
import java.lang.ClassNotFoundException;

public class ShowClass {

	public static void main(String[] args) {
		String reflect_v2 = "a21_ReflectionTest.TestKlasse";
		try {
			printClass(Class.forName(args[0]));
			//printClass(Class.forName(reflect_v2));
		} catch (ClassNotFoundException cnfe) {
			System.out.println("Klasse nicht gefunden ... " + args[0]);
		}
	}

	// ----------------------------------------------------------------------------
	public static void printClass(Class<?> c) {
		Package pck = c.getPackage();
		if (pck != null)
			System.out.println("Package: " + pck.getName() + ";");

		if (c.isInterface()) {
			System.out.print(Modifier.toString(c.getModifiers()) + " "
					+ c.getName());
		} else
			System.out.print(Modifier.toString(c.getModifiers()) + " class "
					+ c.getName() + " extends " + c.getSuperclass().getName());

		Class<?>[] interfaces = c.getInterfaces();
		if ((interfaces != null) && (interfaces.length > 0)) {
			if (c.isInterface())
				System.out.println(" extends ");
			else
				System.out.print(" implements ");
			for (int i = 0; i < interfaces.length; i++) {
				if (i > 0)
					System.out.print(", ");
				System.out.print(interfaces[i].getName());
			}
		}
		System.out.println("\n{");

		Constructor<?>[] constructors = c.getDeclaredConstructors();
		if (constructors.length > 0) {
			System.out.println("   //Konstruktoren");
			for (int i = 0; i < constructors.length; i++)
				printOperation(constructors[i]);
		}
		constructors = null;

		Class<?>[] classes = c.getDeclaredClasses();
		if (classes.length > 0) {
			System.out.println("   //Innere Klassen");
			for (int i = 0; i < classes.length; i++) {
				if (classes[i].isInterface() == false) {
					printClass(classes[i]);
					System.out.print("\n");
				}
			}
			System.out.println("   //Innere Interfaces");
			for (int i = 0; i < classes.length; i++) {
				if (classes[i].isInterface() == true) {
					printClass(classes[i]);
					System.out.print("\n");
				}
			}
		}
		classes = null;

		Field[] fields = c.getDeclaredFields();
		if (fields.length > 0) {
			System.out.println("\n  //Varablen ");
			for (int i = 0; i < fields.length; i++)
				printAttribute(fields[i]);
		}
		fields = null;

		Method[] operations = c.getDeclaredMethods();
		if (operations.length > 0) {
			System.out.println("\n  //Methoden");
			for (int i = 0; i < operations.length; i++)
				printOperation(operations[i]);
		}
		operations = null;

		System.out.print("} //end ");
		if (c.isInterface())
			System.out.print("interface ");
		else
			System.out.print("class ");
		System.out.print(c.getName());
	}

	// ----------------------------------------------------------------------------
	public static String typename(Class<?> t) {
		String hochkomma = "";
		while (t.isArray()) {
			hochkomma += "[]";
			t = t.getComponentType();
		} 
		return t.getName() + hochkomma;
	}

	// ----------------------------------------------------------------------------
	public static String modifiers(int m) {
		if (m == 0)
			return "";
		else
			return Modifier.toString(m) + " ";
	}

	// ----------------------------------------------------------------------------
	public static void printAttribute(Field f) {
		System.out.print("  " + modifiers(f.getModifiers())
				+ typename(f.getType()) + " " + f.getName() + ";     //"
				+ f.getType().getName() + " ist ");

		if ((f.getType()).isInterface())
			System.out.println("ein Interface");
		else {
			if (f.getType().isPrimitive())
				System.out.println("ein primitiver Typ");
			else {
				if (f.getType().isArray())
					System.out.println("ein Array");
				else
					System.out.println("eine Klasse");
			}
		}
	}

	// ----------------------------------------------------------------------------
	public static void printOperation(Member member) {
		Class<?> returntype = null, parameters[], exceptions[];

		if (member instanceof Method) {
			Method m = (Method) member;
			returntype = m.getReturnType();
			parameters = m.getParameterTypes();
			exceptions = m.getExceptionTypes();
		} else {
			Constructor<?> c = (Constructor<?>) member;
			parameters = c.getParameterTypes();
			exceptions = c.getExceptionTypes();
		} // else

		System.out.print("  " + modifiers(member.getModifiers())
				+ ((returntype != null) ? typename(returntype) + " " : "")
				+ member.getName() + "(");
		for (int i = 0; i < parameters.length; i++) {
			if (i > 0)
				System.out.print(", ");

			System.out.print(typename(parameters[i]));
		}

		System.out.print(")");

		if (exceptions.length > 0)
			System.out.print(" throws ");

		for (int i = 0; i < exceptions.length; i++) {
			if (i > 0)
				System.out.print(", ");

			System.out.print(typename(exceptions[i]));
		}

		System.out.println(";");
	}
}

// --------------------------------------------
interface TestInterface {
}

class TestClass11 {
	public TestInterface i1;
	@SuppressWarnings("unused")
	private TestClass11 c1;
	protected int p1;
	int[] ta1;
	int[][] tam1;

	class Inner {
		int i;

		class InnerInner {
			int j;
		}
	}

	interface InnerInterface {
	}

	@SuppressWarnings("unused")
	private TestClass11 testc() {
		return new TestClass11() {
			public void hello() {
				System.out.println("ueberschreiben ... test!");
			}
		};
	}
}