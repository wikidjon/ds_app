package a11_collections_2;
//Collections.fill

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

class CollFill {
  public static void main(String[] args) {
    String[] coins = { "A", "B", "C", "D", "E" };

    List<String> einfuehrung = new LinkedList<String>();
    for (int i = 0; i < coins.length; i++)
      einfuehrung.add(coins[i]);

    List<String> dst = new ArrayList<String>();
    for (int i = 0; i < coins.length; i++)
      dst.add("");

    Collections.copy(dst, einfuehrung);

    ListIterator<String> liter = dst.listIterator();

    while (liter.hasNext())
      System.out.println(liter.next());

    Collections.fill(einfuehrung, "kein Geld");

    liter = einfuehrung.listIterator();

    while (liter.hasNext())
      System.out.println(liter.next());
  }
}
