package a11_collections_2;
//Collections.reverse
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

class UtilDemo3 {
  public static void main(String[] args) {
    String[] coins = { "A", "B", "C", "D", "E" };

    List<String> l = new ArrayList<String>();
    for (int i = 0; i < coins.length; i++)
      l.add(coins[i]);

    ListIterator<String> liter = l.listIterator();

    while (liter.hasNext())
      System.out.println(liter.next());

    Collections.reverse(l);

    liter = l.listIterator();

    while (liter.hasNext())
      System.out.println(liter.next());
  }
}
