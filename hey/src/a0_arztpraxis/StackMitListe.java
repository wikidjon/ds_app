package a0_arztpraxis;


public class StackMitListe
{
   private Liste l = new Liste();

   void durchlaufe()
   {
      l.durchlaufe();
   }

   boolean isEmpty()
   {
      return l.istLeer();
   }

   Object pop()
   {
      Object o = l.getKopfDaten();
      l.loescheKopf();
      return o;
   }

   void push(Object newObject)
   {
      l.einfuegeKopf(newObject);
   }
}