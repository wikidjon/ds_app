package a0_arztpraxis;

class ListenElement {
    private Patient daten;
    private ListenElement naechstes;
    
   ListenElement(Patient daten) {
        this(daten, null);
    }
    
    ListenElement(Patient daten, ListenElement naechstes) {
        this.daten = daten;
        this.naechstes = naechstes;
    }
    
    public String getDaten() {
        return daten.getName();
//        		toString();
    }
    
    public ListenElement getNaechstes() {
        return naechstes;
    }
    
 /*   public void setDaten(Object daten) {
        this.daten = daten;
    }
    */
    public void setNaechstes(ListenElement naechstes) {
        this.naechstes = naechstes;
    }
}
