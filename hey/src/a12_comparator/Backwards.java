package a12_comparator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Backwards {
	public static void main(String args[]) {
		Employee3 emps[] = { 
				new Employee3("Finance", "Degree, Debbie"),
				new Employee3("Finance", "Grade, Geri"),
				new Employee3("Finance", "Extent, Ester"),
				new Employee3("Engineering", "Measure, Mary"),
				new Employee3("Engineering", "Amount, Anastasia"),
				new Employee3("Engineering", "Ratio, Ringo"),
				new Employee3("Sales", "Stint, Sarah"),
				new Employee3("Sales", "Pitch, Paula"),
				new Employee3("Support", "Rate, Rhoda"), };
		SortedSet<Object> set = new TreeSet<Object>(Arrays.asList(emps));
		System.out.println(set);

		try {
			Object last = set.last();
			boolean first = true;
			while (true) {
				if (!first) {
					System.out.print(", ");
				}
				System.out.println(last);
				last = set.headSet(last).last();
			}
		} catch (NoSuchElementException e) {
			System.out.println();
		}

		Set<Object> subset = set.headSet(emps[4]);
		subset.add(emps[5]);

	}
}

class EmpComparator2 implements Comparator<Object> {

	public int compare(Object obj1, Object obj2) {
		Employee3 emp1 = (Employee3) obj1;
		Employee3 emp2 = (Employee3) obj2;

		int nameComp = emp1.getName().compareTo(emp2.getName());

		return ((nameComp == 0) ? emp1.getDepartment().compareTo(
				emp2.getDepartment()) : nameComp);
	}
}

class Employee3 implements Comparable<Object> {
	String department, name;

	public Employee3(String department, String name) {
		this.department = department;
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return "[dept=" + department + ",name=" + name + "]";
	}

	public int compareTo(Object obj) {
		Employee3 emp = (Employee3) obj;
		int deptComp = department.compareTo(emp.getDepartment());

		return ((deptComp == 0) ? name.compareTo(emp.getName()) : deptComp);
	}

	public boolean equals(Object obj) {
		if (!(obj instanceof Employee3)) {
			return false;
		}
		Employee3 emp = (Employee3) obj;
		return department.equals(emp.getDepartment())
				&& name.equals(emp.getName());
	}

	public int hashCode() {
		return 31 * department.hashCode() + name.hashCode();
	}
}
