package a12_comparator;

import java.util.*;

//Listen und Comparatoren
// Um Ordnung herzustellen, unterscheidet Java zwei Wege:
// Elemente k�nnen eine nat�rliche Ordnung haben. Dann implementieren Klassen die Schnittstelle Comparable. 
// Beispiele sind String, Date und Integer.
// Ein Vergleichsobjekt, das die Schnittstelle Comparator implementiert, stellt fest, wie die Ordnung f�r zwei Elemente ist.
// Um Such- oder Sortieroperationen m�glichst unabh�ngig von Klassen zu machen, die eine nat�rliche Ordnung besitzen 
// oder die eine Ordnung �ber einen externen Comparator definiert bekommen, haben Utility-Klassen wie java.util.Arrays 
// oder java.util.Collections oft zwei Arten von Methoden: einmal mit einem zus�tzlichen Comparator-Parameter 
// und einmal ohne. Wird kein Comparator angegeben, so m�ssen die Objekte vom Typ Comparable sein.

public class CompTest {
	public static void main(String args[]) {
		ArrayList<String> u2 = new ArrayList<String>();
		u2.add("Jethro Tull");
		u2.add("Jimi Hendrix");
		u2.add("Eric Clapton");
		u2.add("Johnny Winter");
		u2.add("Otis Reading");
		u2.add("Al Jarreau");
		u2.add("Colosseum");
		u2.add("Mody Blues");
		u2.add("When A Men Loves a Women");
		u2.add("Nazareth");
		u2.add("Simon and Garfunkel");

		Comparator<Object> comp = Comparators.stringComparator();
		Collections.sort(u2, comp);
		System.out.println(u2 +"\n");
		Collections.reverse(u2);
		System.out.println(u2);
		
		Arrays.sort(args, comp);
		System.out.print("[");
		for (int i = 0, n = args.length; i < n; i++) {
			if (i != 0)
				System.out.print(", ");
			System.out.print(args[i]);
		}
		System.out.println("]");
	}
}

class Comparators {

	public static Comparator<Object> stringComparator() {
		return new Comparator<Object>() {

			public int compare(Object o1, Object o2) {
				String s1 = (String) o1;
				String s2 = (String) o2;
				int len1 = s1.length();
				int len2 = s2.length();
				int n = Math.min(len1, len2);
				char v1[] = s1.toCharArray();
				char v2[] = s2.toCharArray();
				int pos = 0;

				while (n-- != 0) {
					char c1 = v1[pos];
					char c2 = v2[pos];
					if (c1 != c2) {
						return c1 - c2;
					}
					pos++;
				}
				return len1 - len2;
			}
		};
	}

	public static Comparator<Object> integerComparator() {
		return new Comparator<Object>() {

			public int compare(Object o1, Object o2) {
				int val1 = ((Integer) o1).intValue();
				int val2 = ((Integer) o2).intValue();
				return (val1 < val2 ? -1 : (val1 == val2 ? 0 : 1));
			}
		};
	}

	public static Comparator<Object> dateComparator() {
		return new Comparator<Object>() {

			public int compare(Object o1, Object o2) {
				long val1 = ((Date) o1).getTime();
				long val2 = ((Date) o2).getTime();
				return (val1 < val2 ? -1 : (val1 == val2 ? 0 : 1));
			}
		};
	}
}
