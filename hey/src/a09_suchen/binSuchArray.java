package a09_suchen;

public class binSuchArray {
	private static long[] a;

	private int anzElemente;

	public binSuchArray(int max) {
		a = new long[max]; // array anlegen
		anzElemente = 0;
	}

	public int size() {
		return anzElemente;
	}

	public int finde(long suchKey) {
		return recfinde(suchKey, 0, anzElemente - 1);
	}

	private int recfinde(long suchKey, int unterGrenze, int oberGrenze) {
		int cursorIn;

		cursorIn = (unterGrenze + oberGrenze) / 2;
		if (a[cursorIn] == suchKey)
			return cursorIn;					 									// gefunden
		else if (unterGrenze > oberGrenze)
						return anzElemente; 								// nicht gefunden
				else 																	// Bereich verkleinern
				{
					if (a[cursorIn] < suchKey) 							// obere haelfte
						return recfinde(suchKey, cursorIn + 1, oberGrenze);
					else																	// untere haelfte
						return recfinde(suchKey, unterGrenze, cursorIn - 1);
		}
	}

	public void einfuegen(long wert) { 							// sortiert
		int j;
		for (j = 0; j < anzElemente; j++)								// finden
			if (a[j] > wert) 														// (lineare suche)
				break;
		for (int k = anzElemente; k > j; k--)						// greosseres nach oben
			a[k] = a[k - 1];
		a[j] = wert; 																	// einfuegen
		anzElemente++; 														// inkrementieren der groesse
	}

	public void anzeigen() {
		for (int j = 0; j < anzElemente; j++)
			System.out.print(a[j] + " ");
		System.out.println("");
	}

	public static void main(String[] args) {
		int maxSize = 100;
		binSuchArray arr = new binSuchArray(maxSize);

		arr.einfuegen(12);
		arr.einfuegen(20);
		arr.einfuegen(35);
		arr.einfuegen(426);
		arr.einfuegen(54);
		arr.einfuegen(69);
		arr.einfuegen(744);
		arr.einfuegen(87);
		arr.einfuegen(895);
		arr.einfuegen(89);
		arr.einfuegen(8);
		arr.einfuegen(208);
		arr.einfuegen(4);
		arr.einfuegen(617);
		arr.einfuegen(83);
		arr.einfuegen(96);

		arr.anzeigen(); 						// anzeigen array

		int suchKey = 208;				 // suchbegriff
		if (arr.finde(suchKey) != arr.size())
			System.out.println("Gefunden " + suchKey );
		else
			System.out.println("Nicht gefunden " + suchKey);
	}
}