package collections_2;


import java.awt.Color;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Demonstrates the use of final collections.
 */
public class FinalCollections {

	public static final void main(final String[] args) {
    
	Rainbow.someMethod();
    System.out.println("------------------");
    try {
      RainbowBetter.someMethod();
    } catch (final UnsupportedOperationException ex) {
      ex.printStackTrace(System.out);
      System.out.println("Told you it would exception =)");
    }
  }

  /**
   * A class that demonstrates the rainbow.
   * 
   * @author <a href=mailto:kraythe@arcor.de>Robert Simmons jr. (kraythe)</a>
   * @version $Revision: 1.3 $
   */
  public static class Rainbow {
    /** The set of valid colors of the rainbow. */
    public static final Set<Color> VALID_COLORS;

    static {
      VALID_COLORS = new HashSet<Color>();
      VALID_COLORS.add(Color.red);
      VALID_COLORS.add(Color.orange);
      VALID_COLORS.add(Color.yellow);
      VALID_COLORS.add(Color.green);
      VALID_COLORS.add(Color.blue);
      VALID_COLORS.add(Color.decode("#4B0082")); // indigo
      VALID_COLORS.add(Color.decode("#8A2BE2")); // violet
    }

    /**
     * A demo method.
     */
    public static final void someMethod() {
      Set<Color> colors = Rainbow.VALID_COLORS;
      colors.add(Color.black); // <= logic error but allowed by compiler
      System.out.println(colors);
    }
  }

  /**
   * A better version of the rainbow class using an unmodifiable set.
   * 
   * @author <a href=mailto:kraythe@arcor.de>Robert Simmons jr. (kraythe)</a>
   * @version $Revision: 1.3 $
   */
  public static class RainbowBetter {
    /** The valid colors of the rainbow. */
    public static final Set<Color> VALID_COLORS;

    static {
      Set<Color> temp = new HashSet<Color>();
      temp.add(Color.red);
      temp.add(Color.orange);
      temp.add(Color.yellow);
      temp.add(Color.green);
      temp.add(Color.blue);
      temp.add(Color.decode("#4B0082")); // indigo
      temp.add(Color.decode("#8A2BE2")); // violet
      VALID_COLORS = Collections.unmodifiableSet(temp);
    }

    /**
     * Some demo method.
     */
    public static final void someMethod() {
      Set<Color> colors = RainbowBetter.VALID_COLORS;
      colors.add(Color.black); // <= exception here
      System.out.println(colors);
    }
  }
}



 