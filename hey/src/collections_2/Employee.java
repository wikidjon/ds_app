package collections_2;

import java.util.Comparator;

class Employee implements Comparable<Object> {
  private String name;

  private double salary;

  Employee(String name, double salary) {
    this.name = name;
    this.salary = salary;
  }

  String getName() {
    return name;
  }

  double getSalary() {
    return salary;
  }

  public String toString() {
    return "Name = " + getName() + ", Salary = " + getSalary();
  }

  public int compareTo(Object o) {
    if (!(o instanceof Employee))
      throw new ClassCastException();

    Employee e = (Employee) o;

    return name.compareTo(e.getName());
  }

  static class SalaryComparator implements Comparator<Object> {
    public int compare(Object o1, Object o2) {
      if (!(o1 instanceof Employee) || !(o2 instanceof Employee))
        throw new ClassCastException();

      Employee e1 = (Employee) o1;
      Employee e2 = (Employee) o2;

      return (int) (e1.getSalary() - e2.getSalary());
    }
  }
}


