package collections_2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CollShuff {
  public static void main(String[] args) {
    List<String> argList = Arrays.asList(args);
    Collections.shuffle(argList);
    for (String arg : argList) {
      System.out.format("%s ", arg);
    }
    System.out.println();
  }
}
