package sample;

import java.sql.*;

public class gui {
	public static void main(String args[]){
		Connection c = null;
		Statement stmt = null;
		
		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			c.setAutoCommit(false);
			System.out.print("Connection opened");	
			
		} catch (Exception e){
			System.err.println(e.getClass() + ": " + e.getMessage());
		}
	}
}
