package sample;

public class config {
//	private static String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	private static config instance = null;
	
	private static String name;
	
	private config(){}
	
	public static config getInstance(){
		if (instance != null) {
			instance = new config();
		}
		return instance;
	}
	
	public static boolean setName(String str){
		boolean status = false;
		
		name = str;
		if (name != "") {
			status = true;
		}
		
		return status;
	}
	
	public static String getName(){
		return name;
	}
}
