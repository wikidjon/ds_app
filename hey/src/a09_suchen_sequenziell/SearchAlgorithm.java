package a09_suchen_sequenziell;
/**
 * File: SearchAlgorithm.java
 * @author S. Schuierer
 */
public class SearchAlgorithm {

    static int search (Orderable A[], Orderable k) {
	return SequentialSearch.search(A, k);
    }

}
