package a0_listen1;

public class ListenStack implements Stack {
	private List list; // Liste zur Verwaltung der Elemente
	
	public ListenStack () {
		list = new List ();
	}
	
	public void push (Object obj) {
		// Element vorn anf�gen
		list.addFirst (obj);
	}
	
	public Object pop () throws StackException {
		if (isEmpty ())
			throw new StackException ();
		// Element von vorn entfernen
		return list.removeFirst ();
	}
	
	public Object top () throws StackException {
		if (isEmpty ())
			throw new StackException ();
		return list.getFirst ();
	}
	
	public boolean isEmpty () {
		return list.isEmpty ();
	}
}
