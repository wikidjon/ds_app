package neu;

public class AVLknoten{
  int key;              //Schluessel
  AVLknoten l,r,v;      //linker,-rechter Sohn,Vater
  int b;                //Balance

  //---Konstruktor----------------------------------------------------------------
  public AVLknoten(int key, AVLknoten v, AVLknoten l, AVLknoten r, int b){
   this.key=key;
   this.v=v;
   this.l=l;
   this.r=r;
   this.b=b;
  }
}
