package neu.listen.rekursion;

public class FakRekursiv {
	public static int fakBerechnung(final int x) {
		if (x <= 1)
			return 1;
		else
			return x * fakBerechnung(x - 1);
	}

	public static void main(final String[] args) {
		int z;

		final eingabe2 ein = new eingabe2();
		System.out.print("Zahl: ");
		z = ein.eingInt();
		System.out.println("Fakultaet(" + z + "!) = " + fakBerechnung(z));
	}
}
