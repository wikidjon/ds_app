package neu;

//@SuppressWarnings( "all" )
@SuppressWarnings( { "rawtypes", "unchecked" } )

// Die Annotation @SuppressWarnings steuert Compiler-Warnungen. 
// Unterschiedliche Werte bestimmen genauer, welche Hinweise unterdr�ckt werden. 
// N�tzlich ist die Annotation bei der Umstellung von Quellcode, der vor Java 5 entwickelt wurde. 
// Mit Java 5 zogen Generics ein, eine M�glichkeit, dem Compiler noch mehr Informationen �ber Typen zu geben. 
// Die Java API-Designer haben daraufhin die Deklaration der Datenstrukturen �berarbeitet und Generics eingef�hrt, 
// was dazu f�hrt, dass vor Java 5 entwickelte Quellcode mit einem Java 5 Compiler eine Vielzahl von 
// Warnungen meldet. 




public class a_08_HeapSort2 {

	private static void percolate(Comparable<Object>[] f, int index, int ende) {
		int i = index + 1, j;
		while (2 * i <= ende) { 						// f[i] hat linken Sohn
			j = 2 * i; 									// f[j] ist linker Sohn von f[i]
			if (j < ende) 								// f[i] hat auch rechten Sohn
				if (f[j - 1].compareTo(f[j]) > 0)
					j++; 								// f[j] ist jetzt kleiner
			if (f[i - 1].compareTo(f[j - 1]) > 0) {
				tauschen(f, i - 1, j - 1);
				i = j; 									// versickere weiter
			} else
				break; 									// heap-Bedingung erf�llt
		}
	}

	private static void tauschen(Comparable<Object>[] f, int i1, int i2) {
		Comparable<Object> tmp = f[i1];
		f[i1] = f[i2];
		f[i2] = tmp;
	}

	public static void heapSort(Comparable<Object>[] f) {
		int i;
		for (i = f.length / 2; i >= 0; i--)
			percolate(f, i, f.length);

		for (i = f.length - 1; i > 0; i--) {
			tauschen(f, 0, i); 							// tauscht jeweils letztes Element des Heaps mit dem ersten
			percolate(f, 0, i); 						// heap wird von Position 0 bis i hergestellt
		}
	}


	static Comparable[] initArray(int num) {			// Initialisierung eines Feldes mit Zufallszahlen
		Integer[] result = new Integer[num];
		for (int i = 0; i < num; i++)
			result[i] = new Integer((int) (Math.random() * 100.0));
		return result;
	}

	static void ausgabeArray(Comparable<Object>[] array) {	 	// Ausgabe der Elemente eines Feldes
		for (int i = 0; i < array.length; i++)
			System.out.print(array[i] + " ");
		System.out.println();
	}


	public static void main(String[] args) {

		Comparable<Object>[] array = initArray(20);
		System.out.print("Heap:\n");
		ausgabeArray(array);
		heapSort(array);
		System.out.print("\nabsteigend sortiert:\n");
		ausgabeArray(array);
	}
}
