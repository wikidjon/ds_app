package neu;

import java.io.*;

/**  Die Klasse enth�lt Eingabemethoden f�r die
 * Eingabe eines Strings mit und ohne Eingabeaufforderung
 * Eingabe eines Zeichens mit und ohne Eingabeaufforderung
 * Eingabe einer Zahl (integer) mit und ohne Eingabeaufforderung
 **/

public class eingabe3 {
    
    /**  Eingabe eines String ohne Eingabeaufforderung *******
     * die Methode liefert einen String zur�ck, der �ber Tastatur eingeben wurde **/

	//***************************************************************************************************************    
    public String eingString() {
        String sin = " ";
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            sin=in.readLine();
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
        return sin;
    }

	//***************************************************************************************************************    
    	/**  Eingabe eines String mit Eingabeaufforderung *******
     	* die Methode liefert einen String zur�ck, der �ber Tastatur eingeben wurde **/
    
    public static String eingString(String s) {
        String sin = " ";
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print(s);
            sin=in.readLine();
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
        return sin;
    }
    

	//***************************************************************************************************************    
	/**  Eingabe eines Zeichens ohne Eingabeaufforderung *******
     	* die Methode liefert ein zeichen zur�ck, das �ber Tastatur eingeben wurde **/
    
    public char eingChar() {
        char cin = ' ';
        DataInputStream in = new DataInputStream(System.in);
        try {
            cin=in.readChar();
            System.out.print("..." + cin);
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
        return cin;
    }
    
	//***************************************************************************************************************    
    	/**  Eingabe eines Zeichens mit Eingabeaufforderung *******
     	* die Methode liefert ein zeichen zur�ck, das �ber Tastatur eingeben wurde **/
    
    public char eingChar(String s) {
        char cin = ' ';
        DataInputStream in = new DataInputStream(System.in);
        try {
            System.out.print(s);
            cin=in.readChar();
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
        return cin;
    }
    

	//***************************************************************************************************************    
    	/**  Eingabe eines Integer ohne Eingabeaufforderung *******
     	* die Methode liest einen String ein und wandelt ihn in int **/
    
    public int eingInt() {
        Integer i = new Integer(eingString());
        return i.intValue();
    }
    

	//***************************************************************************************************************    
    	/**  Eingabe eines Integer mit Eingabeaufforderung *******
     	* die Methode liest einen String ein und wandelt ihn in int **/
    public int eingInt(String s) {
        Integer i = new Integer(eingString(s));
        return i.intValue();
    }
}
