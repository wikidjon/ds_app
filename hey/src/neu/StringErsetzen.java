package neu;

import java.util.regex.*;

public class StringErsetzen
{
  public static void main(String[] args)
  {
    String se, sa, so = "Klasse.Methode(Argument1, Argument2)";
    Pattern p = Pattern.compile("[,.()]");
    Matcher m = p.matcher(so);
    System.out.println("originale Zeichenfolge: " + so);
    se = m.replaceFirst("_");
    System.out.println("erstes Vorkommen ersetzt: " + se);
    sa = m.replaceAll("_");
    System.out.println("alle Vorkommen ersetzt: " + sa);
  }
}
