package a06_avl_baum;

public class AVLTree {

	static class AVLNode {
		int balance; // -1, 0, oder 1
		Object key;
		AVLNode left = null, right = null;

		public AVLNode(Object e) {
			key = e;
			balance = 0;
		}

		public AVLNode getLeft() {
			return left;
		}

		public AVLNode getRight() {
			return right;
		}

		public Object getKey() {
			return key;
		}

		public void setLeft(AVLNode n) {
			left = n;
		}

		public void setRight(AVLNode n) {
			right = n;
		}

		@SuppressWarnings("unchecked")
		public int compareKeyTo(Comparable<?> c) {
			return (key == null ? -1 : ((Comparable<Comparable<?>>) key).compareTo(c));
		}

		public String toString() {
			return key == null ? "(null)" : key.toString();
		}

		public void printNode(StringBuffer sb, int depth, AVLNode nullNode) {
			for (int i = 0; i < depth; i++)
				sb.append("  ");
			sb.append(toString() + "\n");
			if (left != nullNode)
				left.printNode(sb, depth + 1, nullNode);
			if (right != nullNode)
				right.printNode(sb, depth + 1, nullNode);
		}

		public int getBalance() {
			return balance;
		}

		public void setBalance(int b) {
			balance = b;
		}
	}

	private int elems;
	private AVLNode head, nullNode;
	private boolean rebalance = false;

	// Hilfsvariable fuer Einfuegen u. Loeschen

	public AVLTree() {
		head = new AVLNode(null);
		nullNode = new AVLNode(null);
		nullNode.setLeft(nullNode);
		nullNode.setRight(nullNode);
		head.setRight(nullNode);
		head.setLeft(nullNode);
		elems = 0;
	}

	public boolean find(Comparable<?> c) {
		return (findNode(c) != null);
	}

	public AVLNode findNode(Comparable<?> c) {
		AVLNode n = head.getRight();
		while (n != nullNode) {
			int cmp = n.compareKeyTo(c);
			if (cmp == 0)
				return n;
			else
				n = cmp > 0 ? n.getLeft() : n.getRight();
		}
		return null;
	}

	private AVLNode rotateLeft(AVLNode n) {
		AVLNode current = n.getRight();
		n.setRight(n.getRight().getLeft());
		current.setLeft(n);
		return current;
	}

	private AVLNode rotateRight(AVLNode n) {
		AVLNode current = n.getLeft();
		n.setLeft(n.getLeft().getRight());
		current.setRight(n);
		return current;
	}

	AVLNode insertNode(AVLNode n, Comparable<?> k) {
		AVLNode tmp;
		if (n.compareKeyTo(k) == 0)
			return n;
		else if (n.compareKeyTo(k) < 0) {
			// weiter nach rechts gehen
			if (n.getRight() != nullNode) {
				// rechts einf�gen
				n.setRight(insertNode(n.getRight(), k));
				if (n != head && rebalance)
					// Ausgleichen notwendig
					switch (n.getBalance()) {
					case 1:
						if (n.getRight().getBalance() == 1) {
							// Rotation nach links
							tmp = rotateLeft(n);
							tmp.getLeft().setBalance(0);
						} else {
							// doppelte Rotation rechts-links
							int b = n.getRight().getLeft().getBalance();
							n.setRight(rotateRight(n.getRight()));
							tmp = rotateLeft(n);
							tmp.getRight().setBalance((b == -1) ? 1 : 0);
							tmp.getLeft().setBalance((b == 1) ? -1 : 0);
						}
						tmp.setBalance(0);
						rebalance = false;
						return tmp;
					case 0:
						n.setBalance(1);
						return n;
					case -1:
						n.setBalance(0);
						rebalance = false;
						return n;
					}
				else
					return n;
			} else {
				AVLNode newNode = new AVLNode(k);
				newNode.setLeft(nullNode);
				newNode.setRight(nullNode);
				n.setRight(newNode);
				n.setBalance(n.getBalance() + 1);
				rebalance = (n.getBalance() >= 1);
				return n;
			}
		} else { // links einfuegen (analog)
			if (n.getLeft() != nullNode) {
				n.setLeft(insertNode(n.getLeft(), k));
				if (n != head && rebalance)
					switch (n.getBalance()) {
					case -1:
						if (n.getLeft().getBalance() == -1) {
							// Rechtsrotation
							tmp = rotateRight(n);
							tmp.getRight().setBalance(0);
						} else {
							// Linksrechts-Rotation
							int b = n.getLeft().getRight().getBalance();
							n.setLeft(rotateLeft(n.getLeft()));
							tmp = rotateRight(n);
							tmp.getRight().setBalance((b == -1) ? 1 : 0);
							tmp.getLeft().setBalance((b == 1) ? -1 : 0);
						}
						tmp.setBalance(0);
						rebalance = false;
						return tmp;
					case 0:
						n.setBalance(-1);
						return n; // rebalance bleibt true
					case 1:
						n.setBalance(0);
						rebalance = false;
						return n;
					}
				else
					return n;
			} else {
				AVLNode newNode = new AVLNode(k);
				newNode.setLeft(nullNode);
				newNode.setRight(nullNode);
				n.setLeft(newNode);
				n.setBalance(n.getBalance() - 1);
				rebalance = (n.getBalance() <= -1);
				return n;
			}
		}
		return null;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (head.getRight() != nullNode)
			head.getRight().printNode(sb, 0, nullNode);
		return sb.toString();
	}

	public void insert(Comparable<?> k) {
		insertNode(head, k);
		elems++;
	}

	public int size() {
		return elems;
	}

	public AVLNode getRootNode() {
		return head.getRight();
	}

	public AVLNode getNullNode() {
		return nullNode;
	}

	public static void main(String[] args) {
		AVLTree tree = new AVLTree();
		tree.insert(new Integer(3));
		tree.insert(new Integer(2));
		tree.insert(new Integer(1));
		System.out.println("-------------");
		System.out.println(tree);
		System.out.println("-------------");
		tree.insert(new Integer(4));
		tree.insert(new Integer(5));
		tree.insert(new Integer(6));
		tree.insert(new Integer(7));
		tree.insert(new Integer(16));
		tree.insert(new Integer(15));
		System.out.println(tree);
	}
}
