package a06_enumerator;
// eine robustere enumeration 

public class Monate {

  private String name;

  private Monate(String nm) {
    name = nm;
  }

  public String toString() {
    return name;
  }

  public static final Monate JAN = new Monate("Januar"), 
  							 FEB = new Monate("Februar"), 
  							 MAR = new Monate("Maerz"), 
  							 APR = new Monate("April"),
  							 MAI = new Monate("Mai"), 
  							 JUN = new Monate("Juni"), 
  							 JUL = new Monate("Juli"), 
  							 AUG = new Monate("August"), 
  							 SEP = new Monate("September"), 
  							 OKT = new Monate("Oktober"), 
  							 NOV = new Monate("November"), 
  							 DEZ = new Monate("Dezember");

  public static final Monate[] Mon = { JAN, FEB, MAR, APR, MAI, JUN, JUL, AUG, SEP, OKT, NOV, DEZ };

  public static final Monate number(int ord) {
    return Mon[ord - 1];
  }

  public static void main(String[] args) {
    Monate m = Monate.JAN;
    System.out.println(m);
    m = Monate.number(12);
    System.out.println(m);
    System.out.println(m == Monate.DEZ);
    System.out.println(m.equals(Monate.DEZ));
    System.out.println(Mon[3]);
  }
} 
