package a15_set.hash_set;

import java.util.HashSet;
import java.util.Set;

public class FindDups {
  public static void main(String args[]) {
    Set<String> s = new HashSet<String>();
    String[] values = new String[] { "java", "java2", "java2s", "javas", "java" };
    for (int i = 0; i < values.length; i++)
      if (!s.add(values[i]))
        System.out.println("Duplikat erkannt: " + values[i]);

    System.out.println(s.size() + " aehnliche Woerter erkannt: " + s);
  }
}
