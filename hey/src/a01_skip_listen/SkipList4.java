package a01_skip_listen;

import java.util.Iterator;

/**
 * Entwurf des Interfaces f�r Skip-Listen - Version 1.0.
 */
public interface SkipList4 {

	/**
	 * F�gt das �bergebene Objekt in die Skip-Liste ein. Befindet sich
	 * bereits ein identisches Objekt in der Skip-Liste, so wird das
	 * �bergebene Objekt <b>nicht</b> eingef�gt und das schon enthaltene
	 * zur�ckgegeben.
	 *
	 * @param object das Objekt, welches in die Skip-Liste eingef�gt
	 *        werden soll.
	 * @return das eingef�gte Objekt, falls dieses noch nicht in der
	 *         Skip-Liste enthalten war, ansonsten das bereits enthaltene
	 *         Objekt.
	 */
	public abstract Object insert (Object object);

	/**
	 * Suche das �bergebene Objekt in den Skip-Liste. Enth�lt die
	 * Skip-Liste kein zum �bergebenen Objekt identisches Objekt, so
	 * wird <tt>null</tt> zur�ckgegeben.
	 *
	 * @param object das Objekt, welches in der Skip-Liste gesucht werden
	 *        soll.
	 * @return das gefundene Objekt, falls die Skip-Liste ein zum
	 *         �bergebenen Objekt identisches Objekt enth�lt, ansonsten
	 *         <tt>null</tt>.
	 */
	public abstract Object find (Object object);

	/**
	 * L�scht das �bergebene Objekt aus der Skip-Liste. Enth�lt die
	 * Skip-Liste kein zum �bergebenen Objekt identisches Objekt, so wird
	 * <tt>null</tt> zur�ckgegeben.
	 *
	 * @param object das Objekt, welches aus der Skip-Liste gel�scht
	 *        werden soll.
	 * @return das gel�schte Objekt, falls die Skip-Liste ein zum
	 *         �bergebenen Objekt identisches Objekt enth�lt, ansonsten
	 *         <tt>null</tt>.
	 */
	public abstract Object remove (Object object);

	/**
	 * Gibt die Anzahl der in der Skip-Liste gespeicherten Objekte
	 * zur�ck.
	 *
	 * @return die Anzahl der in der Skip-Liste gespeicherten Objekte.
	 */
	public abstract int size ();

	/**
	 * Gibt einen Iterator, der die in der Skip-Liste gespeicherten
	 * Objekte in aufsteigender (falls <tt>forward==true</tt>) bzw.
	 * absteigender (falls <tt>forward==false</tt>) Reihenfolge enth�lt.<br>
	 * Wird die unterliegende Skip-Liste ver�ndert, w�hrend eine
	 * Iteration �ber sie erfolgt, so soll der Iterator beim n�chsten
	 * Aufruf seiner Methoden eine
	 * <tt>ConcurrentModificationException</tt> werfen.
	 *
	 * @param forward falls (<tt>forward==true</tt>), werden die Objekte
	 *        der Skip-Liste in aufsteigender Reihenfolge, ansonsten in
	 *        absteigender Reihenfolge ausgegeben.
	 * @return eine Iteration �ber die in der Skip-Liste gespeicherten
	 *         Objekte.
	 */
	public abstract Iterator<?> iterator (boolean forward);

	/**
	 * Gibt einen Iterator, der die in der Skip-Liste gespeicherten
	 * Objekte, die gr��er oder gleich dem �bergebenen Objekt
	 * <tt>from</tt> und kleiner dem �bergebenen Objekt <tt>to</tt> sind.<br>
	 * Wird die unterliegende Skip-Liste ver�ndert, w�hrend eine
	 * Iteration �ber sie erfolgt, so soll der Iterator beim n�chsten
	 * Aufruf seiner Methoden eine
	 * <tt>ConcurrentModificationException</tt> werfen.
	 *
	 * @param from der Startwert der Bereichsanfrage. Alle der in der
	 *        Iteration enthaltenen Objekte sind gr��er oder gleich
	 *        diesem Objekt.
	 * @param to der Endwert der Bereichsanfrage. Alle der in der
	 *        Iteration enthaltenen Objekte sind kleiner diesem Objekt.
	 * @return eine Bereichsanfrage �ber die in der Skip-Liste
	 *         gespeicherten Objekte.
	 */
	public abstract Iterator<?> rangeQueryIterator (Object from, Object to);
}