package einfuehrung;
/*****************************************************************
 * Sortieren vordere Haelfte 
 * Sortieren hintere Haelfte
 * mischen
 ******************************************************************/

public class a_07_MergeSort {

	private long[] zahlenArray;

	private int anzElemente;

	public a_07_MergeSort(int max) {
		zahlenArray = new long[max];
		anzElemente = 0;
	}

	public void einfuegen(long wert) {
		zahlenArray[anzElemente] = wert; 				// werte einfuegen
		anzElemente++; 									// elementzaehler
	}

	public void anzeigen() {
		for (int j = 0; j < anzElemente; j++)
			System.out.print(zahlenArray[j] + " ");
		System.out.println("");
	}

	public void mergeSort() {
		long[] daten = new long[anzElemente];
		rekMergeSort(daten, 0, anzElemente - 1);
	}

	private void rekMergeSort(long[] daten, int untereGrenze, int obereGrenze) {
		if (untereGrenze == obereGrenze) 			// wenn Bereich = 1,
			return; 								// keine Sortierung
		else { 										// finden mittelpunkt
			int mitte = (untereGrenze + obereGrenze) / 2;
													// sortieren untere haelfte
			rekMergeSort(daten, untereGrenze, mitte);
													// sortieren obere haelfte
			rekMergeSort(daten, mitte + 1, obereGrenze);
													// verschmelzen
			merge(daten, untereGrenze, mitte + 1, obereGrenze);
		}
	}

	private void merge(long[] daten, int untPtr, int obPtr, int obereGrenze) {
		int j = 0; 
		int untereGrenze = untPtr;
		int mitte = obPtr - 1;
		int n = obereGrenze - untereGrenze + 1; 		// anzahl werte

		while (untPtr <= mitte && obPtr <= obereGrenze)
			if (zahlenArray[untPtr] < zahlenArray[obPtr])
				daten[j++] = zahlenArray[untPtr++];
			else
				daten[j++] = zahlenArray[obPtr++];

		while (untPtr <= mitte)
			daten[j++] = zahlenArray[untPtr++];

		while (obPtr <= obereGrenze)
			daten[j++] = zahlenArray[obPtr++];

		for (j = 0; j < n; j++)
			zahlenArray[untereGrenze + j] = daten[j];
	}

	public static void main(String[] args) {
		int maxSize = 100; 									// arraygroesse
		a_07_MergeSort arr = new a_07_MergeSort(maxSize); 	// array anlegen
		arr.einfuegen(14);
		arr.einfuegen(21);
		arr.einfuegen(43);
		arr.einfuegen(50);
		arr.einfuegen(62);
		arr.einfuegen(75);
		arr.einfuegen(14);
		arr.einfuegen(2);
		arr.einfuegen(39);
		arr.einfuegen(5);
		arr.einfuegen(608);
		arr.einfuegen(36);

		System.out.print("Mergesort\n");
		System.out.print("---------\n");

		System.out.print("Daten unsortiert: \n");
		arr.anzeigen();
		arr.mergeSort();
		System.out.print("Daten sortiert  : \n");
		arr.anzeigen();
	}
}