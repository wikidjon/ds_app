
package einfuehrung.AlgoTools;

import java.awt.Event;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.BorderLayout;

public class InputDevice extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TextField pr;
	private TextField in;

	public InputDevice() {

		Panel p = new Panel();

		p.setLayout(new BorderLayout());

		pr = new TextField("", 42);
		in = new TextField("", IOAppletServer.TEXT_WIDTH - 42);

		pr.setFont(IOAppletServer.TEXT_FONT);
		pr.setEditable(false);

		in.setFont(IOAppletServer.TEXT_FONT);
		in.setEditable(false);

		p.add("West", pr);
		p.add("East", in);

		add(p);
	}

	public String getLine(String prompt) {

		pr.setText(prompt);

		in.setText("");
		in.setEditable(true);

		synchronized(this) { try { wait(); } catch (InterruptedException ex) {} }

		String answer = in.getText();

		pr.setText("");
		in.setText("");

		return(answer);
	}

	public boolean action(Event e, Object o) {

		in.setEditable(false);

		synchronized(this) { notify(); }

		return(false);
	}

	public void reset() {

		pr.setText("");
		in.setText("");

		in.setEditable(false);
	}

	public void close() {

		in.setEditable(false);
	}
}
