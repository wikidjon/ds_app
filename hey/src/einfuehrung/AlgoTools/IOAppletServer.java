
package einfuehrung.AlgoTools;

import java.awt.Font;

public class IOAppletServer extends IOS {

	public static int TEXT_WIDTH  = 80;
	public static int TEXT_HEIGHT = 25;

	public static Font TEXT_FONT  = new Font("Courier", Font.PLAIN, 12);

	private static InputDevice  in;
	private static OutputDevice out;

	public static void connect(InputDevice i, OutputDevice o) {

		in  = i;
		out = o;
	}

	public String getLine(String prompt) {

		if (prompt == null) {

			prompt = "";
		}

		String answer = in.getLine(prompt);

		out.append(prompt + answer + NL);

		return(answer);
	}

	public void append(String msg) {

		out.append(msg);
	}

	public void clear() {

		out.clear();
	}

	public void quit(String msg, int code) {

		out.append("Fatal Error: " + msg + NL);
		out.append("Please terminate program!" + NL);

		try { Thread.sleep(Long.MAX_VALUE); } catch (Exception e) {}
	}
}
