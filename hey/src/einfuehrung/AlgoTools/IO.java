
package einfuehrung.AlgoTools;

/**
  * Die Klasse <code>IO</code> beinhaltet Methoden zum Einlesen und Ausgeben von Daten. <br>
  * Alle Methoden sind Klassenmethoden und m&uuml;ssen mit den Pr&auml;fix IO. aufgerufen werden.<br>
  * Es gibt zwei Gruppen von Methoden:<br>
  * <br>
  *      1.) Eingabe-Methoden. <br>
  *           Diese Methoden beginnen mit dem Prefix <code>read</code> und liefern immer einen g&uuml;ltigen Wert.<br>
  *          Alle Eingabe-Methoden gibt es in zwei Varianten: 
  *               Die erste Variante hat als ersten Parameter einen String (prompt),
  *               der zuerst ausgegeben wird. Danach wird dann die Eingabe gelesen.<br>
  *               Bei der zweiten Variante entfaellt dieser String, d.h. es wird
  *               sofort die Eingabe gelesen.<br>
  *          Alle Eingabe-Methoden lesen eine Zeile ein und versuchen, diese in die
  *          entsprechenden Datentypen umzuwandeln (dabei zaehlt das abschliessende
  *          Newline nicht zur Eingabezeile). Falls dies moeglich ist, wird der
  *          entsprechende Wert zurueckgeliefert. Entspricht die Eingabe nicht der
  *          Vorgabe, so wird eine Fehlermeldung ausgegeben und eine neue Zeile
  *          eingelesen, mit der der Vorgang wiederholt wird. Dies geschieht solange,
  *          bis eine korrekte Eingabe gelesen werden konnte.<br>
  *<br>
  *      2.) Ausgabe-Methoden. <br>
  *          Diese Methoden heissen (fast) alle <code>print</code> oder <code>println</code> und geben einen entsprechend
  *          formatierten String aus.<br>
  *          Alle Ausgabe-Methoden gibt es in zwei Varianten:<br>
  *               Die erste Variante heisst print(...) und gibt abhaengig von
  *               den Parametern einen passend formatierten String aus.<br>
  *               Die zweite Variante heisst println(...) und gibt einen
  *               analog formatierten String sowie ein Newline aus.<br>
  *          Die Methoden unterscheiden sich im wesentlichen durch ihren ersten Parameter.
  *          Dieser kann aus {String, long, double, boolean, char, char[], Object} sein.
  *          Andere Typen sind wie folgt enthalten: byte, short, int -&gt; long; float -&gt; double.<br>
  *          Die einfachsten Ausgabe-Methoden enthalten nur einen Parameter und
  *          geben ihre Variable unformatiert aus.<br>
  *          Die zweite Gruppe von Ausgabe-Methoden hat zusaetzlich einen Parameter len,
  *          der die Breite des Ausgabe-Feldes angibt. Die Variable wird innerhalb dieser
  *          Breite rechtsbuendig ausgegeben. Ist das Ergebnis laenger als die Vorgabe,
  *          wird das Ausgabe-Feld passend verlaengert.<br>
  * @author  Thomas Richter
  * @version  %I%, %G%
  */
public abstract class IO {

        private static String NL = IOS.NL;

        //private static IOServer ios = new IOServer();
        private static IOS ios = new IOServer();

        public static void initAppletExecution() {
          ios = new IOAppletServer();
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der zur&uuml;ckgegeben wird.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als String
          */
        public static String readLine(String prompt) {

                return(ios.getLine(prompt));
        }

        /**
          * Liest einen String ein, der zur&uuml;ckgegeben wird.
          * @return der eingegebene Text als String
          */
        public static String readLine() { return(readLine("")); }


        /**
          * Gibt einen Text aus und liest dann einen String ein, der zur&uuml;ckgegeben wird.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als String
          */
        public static String readString(String prompt) {

                return(readLine(prompt));
        }

        /**
          * Liest einen String ein, der zur&uuml;ckgegeben wird.
          * @return der eingegebene Text als String
          */
        public static String readString() { return(readString("")); }

        /**
          * Gibt einen Text aus, liest dann einen String ein und gibt das erste Zeichen zur&uuml;ck.
          * @param prompt Auszugebender Text
          * @return  das erste Zeichen der Eingabe. Falls diese leer ist, ist der Character '\n'.
          */
        public static char readChar(String prompt) {

                String input = readLine(prompt);

                if (input.length() > 0) {

                        return(input.charAt(0));
                }

                return('\n');
        }

        /**
          * Liest einen String ein und gibt das erste Zeichen zur&uuml;ck.
          * @return  das erste Zeichen der Eingabe. Falls diese leer ist, ist der Character '\n'.
          */
        public static char readChar() { return(readChar("")); }

        /**
          * Gibt einen Text aus, liest einen String ein und liefert einen Boolean zur&uuml;ck.
          * <br>Die Eingabe mu&szlig; aus folgender Menge sein:<br>
          *  &nbsp;&nbsp;&nbsp; true TRUE yes Yes YES ja Ja JA t T y Y j J 1  <br>     
          *  &nbsp;&nbsp;&nbsp; false False FALSE no No NO nein Nein NEIN f F n N 0  <br>
          * Ist die Eingabe nicht aus dieser Menge, wird erneut um Eingabe gebeten.
          * @param prompt auszugebender Text
          * @return
          *     true:  Bei Eingabe aus : true True TRUE yes Yes YES ja Ja JA t T y Y j J 1 <br>
          *     false: Bei Eingabe aus : false False FALSE no No NO nein Nein NEIN f F n N 0 
          */
        public static boolean readBoolean(String prompt) {

                boolean result = false;

                boolean noBoolean;
                String input;

                do {
                        noBoolean = false;

                        input = readLine(prompt);

                        if (input.equals("true") || input.equals("True") || input.equals("TRUE") ||
                            input.equals("yes")  || input.equals("Yes")  || input.equals("YES")  ||
                            input.equals("ja")   || input.equals("Ja")   || input.equals("JA")   ||
                            input.equals("t")    || input.equals("T")    ||
                            input.equals("y")    || input.equals("Y")    ||
                            input.equals("j")    || input.equals("J")    ||
                            input.equals("1")) {

                                result = true;

                        } else if (input.equals("false") || input.equals("False") || input.equals("FALSE") ||
                                 input.equals("no")      || input.equals("No")    || input.equals("NO")    ||
                                 input.equals("nein")    || input.equals("Nein")  || input.equals("NEIN")  ||
                                 input.equals("f")       || input.equals("F")     ||
                                 input.equals("n")       || input.equals("N")     ||
                                 input.equals("0")) {

                                result = false;

                        } else {

                                noBoolean = true;
                                prompt    = "Fehler! Bitte ein boolean: ";
                        }

                } while (noBoolean);

                return(result);
        }

        /**
          * Liest einen String ein und liefert einen Boolean zur&uuml;ck.
          * <br>Die Eingabe mu&szlig; aus folgender Menge sein:<br>
          *  &nbsp;&nbsp;&nbsp; true TRUE yes Yes YES ja Ja JA t T y Y j J 1  <br>     
          *  &nbsp;&nbsp;&nbsp; false False FALSE no No NO nein Nein NEIN f F n N 0  <br>
          * Ist die Eingabe nicht aus dieser Menge, wird erneut um Eingabe gebeten.
          * @return
          *     true:  Bei Eingabe aus : true True TRUE yes Yes YES ja Ja JA t T y Y j J 1 <br>
          *     false: Bei Eingabe aus : false False FALSE no No NO nein Nein NEIN f F n N 0 
          */
        public static boolean readBoolean() { return(readBoolean("")); }


        /**
          * Gibt einen Text aus und liest dann einen String ein, der in einen Longwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Long
          */
        public static long readLong(String prompt) {

                long result = 0L;

                boolean noLong;

                do {
                        noLong = false;

                        try {
                                result = (new Long(readLine(prompt))).longValue();

                        } catch (NumberFormatException ex) {

                                noLong = true;
                                prompt = "Fehler! Bitte ein Long: ";
                        }

                } while (noLong);

                return(result);
        }

        /**
          * Liest einen String ein, der in einen Longwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @return der eingegebene Text als Long
          */
        public static long readLong() { return(readLong("")); }

        /**
          * Wartet eine vom Aufrufer zu bestimmende Zeit.
          * @param ms Zeit in Millisekunden
          */
        public static void wait(int ms) {
           synchronized(IO.class) {
              try {
                 IO.class.wait(ms);
              } catch (InterruptedException e) {
                 e.printStackTrace();
              }
           }
        }
        
        /**
          * Gibt einen Text aus und liest dann einen String ein, der in einen Integerwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Integer umgewandelt
          */
        public static int readInt(String prompt) {

                int result = 0;

                boolean noInt;

                do {
                        noInt = false;

                        try {
                                result = (new Integer(readLine(prompt))).intValue();

                        } catch (NumberFormatException ex) {

                                noInt  = true;
                                prompt = "Fehler! Bitte ein int: ";
                        }

                } while (noInt);

                return(result);
        }

        /**
          * Liest einen String ein, der in einen Integerwert umgewandelt und zur&uuml;ckgegeben wird.
         * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @return der eingegebene Text als Integer umgewandelt
          */
        public static int readInt() { return(readInt("")); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in einen Shortwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Short umgewandelt
          */
        public static short readShort(String prompt) {

                short result = 0;

                boolean noShort;

                do {
                        noShort = false;

                        try {
                                result = (new Short(readLine(prompt))).shortValue();

                        } catch (NumberFormatException ex) {

                                noShort = true;
                                prompt  = "Fehler! Bitte ein short: ";
                        }

                } while (noShort);

                return(result);
        }

        /**
          * Liest einen String ein, der in einen Shortwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @return der eingegebene Text als Short umgewandelt
          */
        public static short readShort() { return(readShort("")); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in einen Bytewert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Byte umgewandelt
          */
        public static byte readByte(String prompt) {

                byte result = 0;

                boolean noByte;

                do {
                        noByte = false;

                        try {
                                result = (new Byte(readLine(prompt))).byteValue();

                        } catch (NumberFormatException ex) {

                                noByte = true;
                                prompt = "Fehler! Bitte ein byte ";
                        }

                } while (noByte);

                return(result);
        }

        /**
          * Liest einen String ein, der in einen Bytewert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @return der eingegebene Text als Byte umgewandelt
          */
        public static byte readByte() { return(readByte("")); }


        /**
          * Gibt einen Text aus und liest dann einen String ein, der in einen Doublewert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Double umgewandelt
          */
        public static double readDouble(String prompt) {

                double result = 0.0;

                boolean noDouble;

                do {
                        noDouble = false;

                        try {
                                result = (new Double(readLine(prompt))).doubleValue();

                        } catch (NumberFormatException ex) {

                                noDouble = true;
                                prompt   = "Fehler! Bitte ein double: ";
                        }

                } while (noDouble);

                return(result);
        }

        /**
          * Liest dann einen String ein, der in einen Doublewert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @return der eingegebene Text als Double umgewandelt
          */
        public static double readDouble() { return(readDouble("")); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in einen Floatwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Float umgewandelt
          */
        public static float readFloat(String prompt) {

                float result = 0.0F;

                boolean noFloat;

                do {
                        noFloat = false;

                        try {
                                result = (new Float(readLine(prompt))).floatValue();

                        } catch (NumberFormatException ex) {

                                noFloat = true;
                                prompt  = "Fehler! Bitte ein float: ";
                        }

                } while (noFloat);

                return(result);
        }

        /**
          * Liest einen String ein, der in einen Floatwert umgewandelt und zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * @return der eingegebene Text als Float umgewandelt
          */
        public static float readFloat() { return(readFloat("")); }


        /**
          * Gibt einen Text aus und liest dann einen String ein, der als Array von Charactern zur&uuml;ckgegeben wird.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Charactern
          */
        public static char[] readChars(String prompt) {

                return(readLine(prompt).toCharArray());
        }

        /**
          * Liest einen String ein, der als Array von Charactern zur&uuml;ckgegeben wird.
          * @return der eingegebene Text als Array von Charactern
          */
        public static char[] readChars() { return(readChars("")); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der als Worte interpretiert und als Stringarray zur&uuml;ckgegeben wird.
          * <br>Worte sind dabei durch Whitespace getrennte Teilstrings.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Strings, das die inezelnen Wort enth&auml;lt
          */
        public static String[] readWords(String prompt) {

                String input = readLine(prompt).trim().replace('\t', ' ');

                int numberOfWords;
                int wordNumber;
                String  word;
                boolean ws;
                char ch;


                numberOfWords = 0;
                ws = true;

                for (int i=0; i<input.length(); i++) {

                        ch = input.charAt(i);

                        if (ws && ch != ' ') {

                                numberOfWords++;

                                ws = false;

                        } else if (!ws && ch == ' ') {

                                ws = true;
                        }
                }

                String[] words = new String[numberOfWords];

                wordNumber = 0;
                word = "";
                ws = true;

                for (int i=0; i<input.length(); i++) {

                        ch = input.charAt(i);

                        if (ws && ch != ' ') {

                                word += ch;

                                ws = false;

                        } else if (!ws && ch != ' ') {

                                word += ch;

                        } else if (!ws && ch == ' ') {

                                words[wordNumber++] = word;

                                word = "";
                                ws = true;
                        }
                }

                if (!ws) {

                        words[wordNumber] = word;
                }

                return(words);
        }

        /**
          * Liest einen String ein, der als Worte interpretiert und als Stringarray zur&uuml;ckgegeben wird.
          * <br>Worte sind dabei durch Whitespace getrennte Teilstrings.
          * @return der eingegebene Text als Array von Strings, das die inezelnen Wort enth&auml;lt
          */
        public static String[] readWords() { return(readWords("")); }


        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array von Longwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Longwerten umgewandelt
          */
        public static long[] readLongs(String prompt) {

                String[] words;
                long[] result;

                boolean noLongs;

                do {
                        noLongs = false;

                        words  = readWords(prompt);
                        result = new long[words.length];

                        for (int i=0; i < words.length; i++) {

                                try {
                                        result[i] = (new Long(words[i])).longValue();

                                } catch (NumberFormatException ex) {

                                        noLongs = true;
                                        prompt  = "Fehler! Bitte nur long's: ";

                                        break;
                                }
                        }

                } while (noLongs);

                return(result);
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array mit fester Anzahl von Longwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Longwerten umgewandelt
          */
        public static long[] readLongs(String prompt, int anz) {

                long[] result;

                boolean wrongCount;

                do {
                        wrongCount = false;

                        result = readLongs(prompt);

                        if (result.length != anz) {

                                wrongCount = true;
                                prompt     = "Fehler! Bitte genau " + anz + " long's: ";
                        }

                } while (wrongCount);

                return(result);
        }

        /**
          * Liest einen String ein, der in ein Array von Longwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @return der eingegebene Text als Array von Longwerten umgewandelt
          */
        public static long[] readLongs()        { return(readLongs(""));      }

        /**
          * Liest einen String ein, der in ein Array mit fester Anzahl von Longwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Longwerten umgewandelt
          */
        public static long[] readLongs(int anz) { return(readLongs("", anz)); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array von Integerwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Integerwerten umgewandelt
          */
        public static int[] readInts(String prompt) {

                String[] words;
                int[] result;

                boolean noInts;

                do {
                        noInts = false;

                        words  = readWords(prompt);
                        result = new int[words.length];

                        for (int i=0; i < words.length; i++) {

                                try {
                                        result[i] = (new Integer(words[i])).intValue();

                                } catch (NumberFormatException ex) {

                                        noInts  = true;
                                        prompt  = "Fehler! Bitte nur int's: ";

                                        break;
                                }
                        }

                } while (noInts);

                return(result);
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array mit fester Anzahl von Integerwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Integerwerten umgewandelt
          */
        public static int[] readInts(String prompt, int anz) {

                int[] result;

                boolean wrongCount;

                do {
                        wrongCount = false;

                        result = readInts(prompt);

                        if (result.length != anz) {

                                wrongCount = true;
                                prompt     = "Fehler! Bitte genau " + anz + " int's: ";
                        }

                } while (wrongCount);

                return(result);
        }

        /**
          * Liest  einen String ein, der in ein Array von Integerwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @return der eingegebene Text als Array von Integerwerten umgewandelt
          */
        public static int[] readInts()        { return(readInts(""));      }

        /**
          * Liest einen String ein, der in ein Array mit fester Anzahl von Integerwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Integerwerten umgewandelt
          */
        public static int[] readInts(int anz) { return(readInts("", anz)); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array von Shortwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Shortwerten umgewandelt
          */
        public static short[] readShorts(String prompt) {

                String[] words;
                short[] result;

                boolean noShorts;

                do {
                        noShorts = false;

                        words  = readWords(prompt);
                        result = new short[words.length];

                        for (int i=0; i < words.length; i++) {

                                try {
                                        result[i] = (new Short(words[i])).shortValue();

                                } catch (NumberFormatException ex) {

                                        noShorts = true;
                                        prompt   = "Fehler! Bitte nur short's: ";

                                        break;
                                }
                        }

                } while (noShorts);

                return(result);
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array mit fester Anzahl von Shortwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Shortwerten umgewandelt
          */
        public static short[] readShorts(String prompt, int anz) {

                short[] result;

                boolean wrongCount;

                do {
                        wrongCount = false;

                        result = readShorts(prompt);

                        if (result.length != anz) {

                                wrongCount = true;
                                prompt     = "Fehler! Bitte genau " + anz + " short's: ";
                        }

                } while (wrongCount);

                return(result);
        }

        /**
          * Liest einen String ein, der in ein Array von Shortwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @return der eingegebene Text als Array von Shortwerten umgewandelt
          */
        public static short[] readShorts()        { return(readShorts(""));      }

        /**
          * Liest einen String ein, der in ein Array mit fester Anzahl von Shortwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Shortwerten umgewandelt
          */
        public static short[] readShorts(int anz) { return(readShorts("", anz)); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array von Bytewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Bytewerten umgewandelt
          */
        public static byte[] readBytes(String prompt) {

                String[] words;
                byte[] result;

                boolean noBytes;

                do {
                        noBytes = false;

                        words  = readWords(prompt);
                        result = new byte[words.length];

                        for (int i=0; i < words.length; i++) {

                                try {
                                        result[i] = (new Byte(words[i])).byteValue();

                                } catch (NumberFormatException ex) {

                                        noBytes = true;
                                        prompt  = "Fehler! Bitte nur byte's: ";

                                        break;
                                }
                        }

                } while (noBytes);

                return(result);
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array mit fester Anzahl von Bytewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Bytewerten umgewandelt
          */
        public static byte[] readBytes(String prompt, int anz) {

                byte[] result;

                boolean wrongCount;

                do {
                        wrongCount = false;

                        result = readBytes(prompt);

                        if (result.length != anz) {

                                wrongCount = true;
                                prompt     = "Fehler! Bitte genau " + anz + " byte's: ";
                        }

                } while (wrongCount);

                return(result);
        }

        /**
          * Liest einen String ein, der in ein Array von Bytewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @return der eingegebene Text als Array von Bytewerten umgewandelt
          */
        public static byte[] readBytes()        { return(readBytes(""));      }

        /**
          * Liest einen String ein, der in ein Array mit fester Anzahl von Bytewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Bytewerten umgewandelt
          */
        public static byte[] readBytes(int anz) { return(readBytes("", anz)); }


        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array von Doublewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Doublewerten umgewandelt
          */
        public static double[] readDoubles(String prompt) {

                String[] words;
                double[] result;

                boolean noDoubles;

                do {
                        noDoubles = false;

                        words  = readWords(prompt);
                        result = new double[words.length];

                        for (int i=0; i < words.length; i++) {

                                try {
                                        result[i] = (new Double(words[i])).doubleValue();

                                } catch (NumberFormatException ex) {

                                        noDoubles = true;
                                        prompt    = "Fehler! Bitte nur double's: ";

                                        break;
                                }
                        }

                } while (noDoubles);

                return(result);
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array mit fester Anzahl von Doublewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Doublewerten umgewandelt
          */
        public static double[] readDoubles(String prompt, int anz) {

                double[] result;

                boolean wrongCount;

                do {
                        wrongCount = false;

                        result = readDoubles(prompt);

                        if (result.length != anz) {

                                wrongCount = true;
                                prompt     = "Fehler! Bitte genau " + anz + " double's: ";
                        }

                } while (wrongCount);

                return(result);
        }

        /**
          * Liest einen String ein, der in ein Array von Doublewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @return der eingegebene Text als Array von Doublewerten umgewandelt
          */
        public static double[] readDoubles()        { return(readDoubles(""));      }

        /**
          * Liest einen String ein, der in ein Array mit fester Anzahl von Doublewerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Doublewerten umgewandelt
          */
        public static double[] readDoubles(int anz) { return(readDoubles("", anz)); }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array von Floatwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @return der eingegebene Text als Array von Floatwerten umgewandelt
          */
        public static float[] readFloats(String prompt) {

                String[] words;
                float[] result;

                boolean noFloats;

                do {
                        noFloats = false;

                        words  = readWords(prompt);
                        result = new float[words.length];

                        for (int i=0; i < words.length; i++) {

                                try {
                                        result[i] = (new Float(words[i])).floatValue();

                                } catch (NumberFormatException ex) {

                                        noFloats = true;
                                        prompt   = "Fehler! Bitte nur float's: ";

                                        break;
                                }
                        }

                } while (noFloats);

                return(result);
        }

        /**
          * Gibt einen Text aus und liest dann einen String ein, der in ein Array mit fester Anzahl von Floatwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param prompt Auszugebender Text
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Floatwerten umgewandelt
          */
        public static float[] readFloats(String prompt, int anz) {

                float[] result;

                boolean wrongCount;

                do {
                        wrongCount = false;

                        result = readFloats(prompt);

                        if (result.length != anz) {

                                wrongCount = true;
                                prompt     = "Fehler! Bitte genau " + anz + " float's: ";
                        }

                } while (wrongCount);

                return(result);
        }

        /**
          * Liest einen String ein, der in ein Array von Floatwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @return der eingegebene Text als Array von Floatwerten umgewandelt
          */
        public static float[] readFloats()        { return(readFloats(""));      }

        /**
          * Liest einen String ein, der in ein Array mit fester Anzahl von Floatwerten umgewandelt wird, das zur&uuml;ckgegeben wird.
          * <br>Bei fehlerhafter Eingabe oder ungen&uuml;gender Anzahl von Werten wird eine erneute Eingabe angefordert.
          * <br>Die einzelnen Werte m&uuml;ssen mit Leerzeichen getrennt eingegeben werden.
          * @param anz      Anzahl der Longwerte, die eingelesen werden sollen
          * @return der eingegebene Text als Array von Floatwerten umgewandelt
          */
        public static float[] readFloats(int anz) { return(readFloats("", anz)); }

        /**
          * Gibt einen String auf dem Bildschirm aus.
          * @param msg Der Text, der auf dem Bildschirm ausgegeben werden soll.
          */
        public static void print(String msg) {

                ios.append(msg);
        }

        /**
          * Gibt einen String mit abschlie&szlig;endem Zeilenumbruch auf dem Bildschirm aus.
          * @param msg Der Text, der auf dem Bildschirm ausgegeben werden soll.
          */
        public static void println(String msg) { print(msg + NL); }
        /**
          * Gibt einen Zeilenumbruch auf dem Bildschirm aus.
          */
        public static void println()           { print(""  + NL); }


        /**
          * Gibt einen Longwert auf dem Bildschirm aus.
          * @param l Wert, der ausgegeben werden soll.
          */
        public static void print(long l)        { print(l + ""); }
        /**
          * Gibt einen Longwert und einen Zeilenumbruch auf dem Bildschirm aus.
          * @param l Wert, der ausgegeben werden soll.
          */
        public static void println(long l)      { print(l + NL); }

        /**
          * Gibt einen Doublewert auf dem Bildschirm aus.
          * @param d Wert, der ausgegeben werden soll.
          */
        public static void print(double d)      { print(d + ""); }
        /**
          * Gibt einen Doublewert und einen Zeilenumbruch auf dem Bildschirm aus.
          * @param d Wert, der ausgegeben werden soll.
          */
        public static void println(double d)    { print(d + NL); }

        /**
          * Gibt einen Character auf dem Bildschirm aus.
          * @param c Wert, der ausgegeben werden soll.
          */
        public static void print(char c)        { print(c + ""); }

        /**
          * Gibt einen Character und einen Zeilenumbruch auf dem Bildschirm aus.
          * @param c Wert, der ausgegeben werden soll.
          */
        public static void println(char c)      { print(c + NL); }

        /**
          * Gibt ein Array von Charactern auf dem Bildschirm aus.
          * @param cs Array, das ausgegeben werden soll.
          */
        public static void print(char[] cs)     { print(String.valueOf(cs)); }
        /**
          * Gibt ein Array von Charactern und einen Zeilenumbruch auf dem Bildschirm aus.
          * @param cs Array, das ausgegeben werden soll.
          */
        public static void println(char[] cs)   { print(cs); print(NL); }

        /**
          * Gibt einen Boolean auf dem Bildschirm aus.
          * @param b Wert, der ausgegeben werden soll.
          */
        public static void print(boolean b)     { print(b + ""); }
        /**
          * Gibt einen Boolean und einen Zeilenumbruch auf dem Bildschirm aus.
          * @param b Wert, der ausgegeben werden soll.
          */
        public static void println(boolean b)   { print(b + NL); }

        /**
          * Gibt ein Object auf dem Bildschirm aus.
          * @param o Object, das ausgegeben werden soll.
          */
        public static void print(Object o)      { print(o + ""); }

        /**
          * Gibt ein Object und einen Zeilenumbruch auf dem Bildschirm aus.
          * @param o Object, das ausgegeben werden soll.
          */
        public static void println(Object o)    { print(o + NL); }

        /**
          * Gibt einen String auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der String wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der String laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param s Der auszugebende Text
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(String s, int len)  {

                int strlen = s.length();

                for (int i=0; i < len-strlen; i++) {

                        s = " " + s;
                }

                print(s);
        }

        /**
          * Gibt einen Longwert auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Longwert wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Longwert laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param l Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(long l,      int len)  { print(l + "",             len); }
        /**
          * Gibt einen Doublewert auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Doublewert wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Doublewert laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param d Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(double d,    int len)  { print(d + "",             len); }
        /**
          * Gibt einen Character auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Character wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Character laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param c Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(char c,      int len)  { print(c + "",             len); }

        /**
          * Gibt einen Character-Array auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Character-Array wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Character-Array laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param cs Der auszugebende Array
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(char[] cs,   int len)  { print(String.valueOf(cs), len); }

        /**
          * Gibt einen Boolean auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Boolean wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Boolean laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param b Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(boolean b,   int len)  { print(b + "",             len); }

        /**
          * Gibt ein Object auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Das Object wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist das Object laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param o Der auszugebende Object
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void print(Object o,    int len)  { print(o + "",             len); }

        /**
          * Gibt einen String und einen Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der String wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der String laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param s Der auszugebende Text
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(String s,  int len)  { print(s,  len); print(NL); }

        /**
          * Gibt einen Longwert und einen Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Longwert wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Longwert laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param l Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(long l,    int len)  { print(l,  len); print(NL); }

        /**
          * Gibt einen Doublewert und einen Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Doublewert wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Doublewert laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param d Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(double d,  int len)  { print(d,  len); print(NL); }

        /**
          * Gibt einen Character und einen Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Character wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Character laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param c Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(char c,    int len)  { print(c,  len); print(NL); }

        /**
          * Gibt einen Character-Array und einen Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Character-Array wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Character-Array laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param cs Der auszugebende Array
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(char[] cs, int len)  { print(cs, len); print(NL); }

        /**
          * Gibt einen Boolean und eine Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Boolean wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist der Boolean laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param b Der auszugebende Wert
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(boolean b, int len)  { print(b,  len); print(NL); }

        /**
          * Gibt ein Object und einen Zeilenumbruch auf dem Bildschirm in formatierter L&auml;nge aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Das Object wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          *<br>Ist das Object laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * @param o Das auszugebende Object
          * @param len Die L&auml;nge des Ausgabe-Feldes
          */
        public static void println(Object o,  int len)  { print(o,  len); print(NL); }

        /**
          * Gibt einen Doublewert auf dem Bildschirm in formatierter L&auml;nge,Form und Genauigkeit aus.
          * <br>Der Parameter len gibt die Breite des Ausgabe-Feldes an. Der Doublewert wird innerhalb dieser
          * Breite rechtsbuendig ausgegeben. 
          * <br>Ist der Doublewert laenger als die Vorgabe, wird das Ausgabe-Feld passend verlaengert.
          * Die Genauigkeit wird ueber den Parameter prec, die Art der Darstellung ueber den Parameter sci bestimmt.
          * <br>Genauigkeit bedeutet dabei die Anzahl der Nachkommastellen. Ist der Parameter sci true, so wird
folgende Exponential-Darstellung gewaehlt:
          * <br>&lt;Vorzeichen&gt;&lt;Vorkomma-Stelle&gt;.&lt;Nachkomma-Stellen&gt;e&lt;Vorzeichen&gt;&lt;Exponent&gt;<br>
          * Das Vorzeichen ist dabei + oder -, die Vorkomma-Stelle aus [1-9], die Anzahl
          * Nachkomma-Stellen entsprechend dem Parameter prec und der Exponent ist immer
          * dreistellig (evtl. mit fuehrenden Nullen).
          * <br>Ist der Parameter sci false, so wird folgende Darstellung gewaehlt:
          * <br>    [&lt;Vorzeichen&gt;]&lt;Vorkomma-Stellen&gt;.&lt;Nachkomma-Stellen&gt;
          * <br>Das Vorzeichen ist dabei + oder -, die Anzahl Vorkomma-Stellen ergibt sich
          * aus der Groesse der Zahl, die Anzahl Nachkomma-Stellen aus dem Parameter prec.
          * Ist prec <= 0, so wird auch in der zweiten Darstellungs-Form ein Vorzeichen
          * erzwungen, bei prec > 0 wird das positive Vorzeichen unterdrueckt.
          * @param d Wert, der ausgegeben werden soll
          * @param len L&auml;nge des Ausgabefeldes
          * @param prec Anzahl der Nachkommastellen
          * @param sci Art der Darstellung
          */
        public static void print(double d, int len, int prec, boolean sci) {

                boolean plus = false;

                if (prec <= 0) {

                        prec = - prec;
                        plus = true;
                }

                String out = "";

                if (Double.isNaN(d) || Double.isInfinite(d)) {

                        out += d;

                } else if (sci) {


                        String pre  = "";
                        String post = "";
                        String exp  = "";

                        if (d == 0.0 || d == -0.0) {

                                out += "+";

                                pre  += "0";
                                post += "0";
                                exp  += "+000";

                        } else {

                                String temp = "" + d;

                                int start = 0;

                                if (temp.charAt(0) == '-') {

                                        out += "-";
                                        start = 1;

                                } else {

                                        out += "+";
                                }

                                int indexOfDot = temp.indexOf('.');
                                int indexOfExp = temp.indexOf('E');

                                if (indexOfExp == -1) {


                                        String preDot  = temp.substring(start, indexOfDot);
                                        String postDot = temp.substring(indexOfDot + 1);

                                        if (preDot.length() > 1) {

                                                pre  += preDot.charAt(0);

                                                post += preDot.substring(1);
                                                post += postDot;

                                                exp  += "+00";
                                                exp  += (preDot.length() - 1);

                                        } else if (preDot.charAt(0) != '0') {

                                                pre  += preDot.charAt(0);
                                                post += postDot;
                                                exp  += "+000";

                                        } else {

                                                for (int i=0; i < postDot.length(); i++) {

                                                        if (postDot.charAt(i) != '0') {

                                                                pre  += postDot.charAt(i);

                                                                post += (i+1 < postDot.length()) ? postDot.substring(i+1) : "0";

                                                                exp  += "-00";
                                                                exp  += (i+1);

                                                                break;
                                                        }
                                                }
                                        }

                                } else {


                                        pre  += temp.charAt(start);
                                        post += temp.substring(indexOfDot+1, indexOfExp);

                                        String expnt = temp.substring(indexOfExp+1);

                                        String expVal;

                                        if (expnt.charAt(0) == '-') {

                                                exp += "-";

                                                expVal = expnt.substring(1);

                                        } else {

                                                exp += "+";

                                                expVal = expnt;
                                        }

                                        if (expVal.length() == 1) {

                                                exp += "00";

                                        } else if (expVal.length() == 2) {

                                                exp += "0";
                                        }

                                        exp += expVal;
                                }
                        }


                        int diff = post.length() - prec;

                        if (diff < 0) {

                                for (int i=0; i < -diff; i++) {

                                        post += "0";
                                }

                        } else if (diff > 0) {

                                char[] chs = post.toCharArray();

                                boolean overflow = false;
                                int val;

                                if (Integer.parseInt("" + chs[prec]) > 4)  { overflow = true; }

                                for (int i=prec-1; i>=0 && overflow; i--) {

                                        val = (Integer.parseInt("" + chs[i]) + 1) % 10;

                                        chs[i] = ("" + val).charAt(0);

                                        if (val != 0)  { overflow = false; }
                                }

                                if (overflow) {

                                        val = Integer.parseInt(pre) + 1;

                                        if (val == 10) {

                                                pre  = "1";
                                                post = "0" + post;


                                                String expStr = "";

                                                if (exp.charAt(0) == '-')       { expStr += exp.charAt(0); }
                                                if (exp.charAt(1) != '0')       { expStr += exp.charAt(1); }
                                                if (exp.charAt(2) != '0')       { expStr += exp.charAt(2); }
                                                                                { expStr += exp.charAt(3); }

                                                int expVal = Integer.parseInt(expStr) + 1;

                                                if (expVal < 0) {

                                                        expVal = - expVal;

                                                        exp = "-";

                                                } else  {

                                                        exp = "+";
                                                }

                                                expStr = "" + expVal;

                                                if (expStr.length() == 1) {

                                                        exp += "00";

                                                } else if (expStr.length() == 2) {

                                                        exp += "0";
                                                }

                                                exp += expStr;

                                        } else {

                                                pre = String.valueOf(val);
                                        }
                                }

                                post = String.valueOf(chs).substring(0, prec);
                        }

                        out += pre;
                        out += ".";
                        out += post;
                        out += "e";
                        out += exp;

                } else {


                        String pre  = "";
                        String post = "";

                        if (d == 0.0 || d == -0.0) {

                                if (plus)  { out += "+"; }

                                pre  += "0";
                                post += "0";

                        } else {

                                String temp = "" + d;

                                int start = 0;

                                if (temp.charAt(0) == '-') {

                                        out += "-";
                                        start = 1;

                                } else if (plus) {

                                        out += "+";
                                }

                                int indexOfDot = temp.indexOf('.');
                                int indexOfExp = temp.indexOf('E');

                                if (indexOfExp == -1) {         // No Exponent

                                        pre  = temp.substring(start, indexOfDot);
                                        post = temp.substring(indexOfDot + 1);

                                } else {


                                        pre  = temp.charAt(start) + "";
                                        post = temp.substring(indexOfDot+1, indexOfExp);

                                        int preLen  = pre.length();
                                        int postLen = post.length();

                                        int expVal = Integer.parseInt(temp.substring(indexOfExp+1));

                                        if (expVal < 0) {               // pre >> post

                                                expVal = -expVal;

                                                if (expVal >= preLen) {

                                                        post = pre + post;
                                                        pre  = "0";

                                                        for (int i=0; i<expVal-preLen; i++)  { post = "0" + post; }

                                                } else {

                                                        post = pre.substring(preLen-expVal) + post;
                                                        pre  = pre.substring(0, preLen-expVal);
                                                }

                                        } else if (expVal > 0) {        // pre << post

                                                if (expVal >= postLen) {

                                                        pre += post;
                                                        post = "0";

                                                        for (int i=0; i<expVal-postLen; i++)  { pre += "0"; }

                                                } else {

                                                        pre += post.substring(0, expVal);
                                                        post = post.substring(expVal);
                                                }

                                        }
                                }
                        }


                        int diff = post.length() - prec;

                        if (diff < 0) {

                                for (int i=0; i < -diff; i++) {

                                        post += "0";
                                }

                        } else if (diff > 0) {

                                char[] chs = post.toCharArray();

                                boolean overflow = false;
                                int val;

                                if (Integer.parseInt("" + chs[prec]) > 4)  { overflow = true; }

                                for (int i=prec-1; i>=0 && overflow; i--) {

                                        val = (Integer.parseInt("" + chs[i]) + 1) % 10;

                                        chs[i] = ("" + val).charAt(0);

                                        if (val != 0)  { overflow = false; }
                                }

                                post = String.valueOf(chs).substring(0, prec);

                                if (overflow) {

                                        chs = pre.toCharArray();

                                        for (int i=chs.length-1; i>=0 && overflow; i--) {

                                                val = (Integer.parseInt("" + chs[i]) + 1) % 10;

                                                chs[i] = ("" + val).charAt(0);

                                                if (val != 0)  { overflow = false; }
                                        }

                                        pre = String.valueOf(chs);

                                        if (overflow)  { pre = "1" + pre; }
                                }
                        }

                        out += pre;
                        out += ".";
                        out += post;
                }

                print(out, len);
        }

        /**
          * Funkioniert wie <code>print(double, int, int, boolean)</code> mit der Annahme, da <code>sci</code> auf <code>false</code> gesetzt ist.
          * @param d Wert, der ausgegeben werden soll
          * @param len L&auml;nge des Ausgabefeldes
          * @param prec Anzahl der Nachkommastellen
          * @see #print(double, int, int, boolean)
          */
        public static void print(double d, int len, int prec) {

                print(d, len, prec, false);
        }

        /**
          * Funktioniert wie <code>print(double, int, int, boolean)</code> und gibt zus&auml;tzlich einen Zeilenumbruch auf dem Bildschirm aus.
          * <br>
          * @param d Wert, der ausgegeben werden soll
          * @param len L&auml;nge des Ausgabefeldes
          * @param prec Anzahl der Nachkommastellen
          * @param sci Art der Darstellung
          * @see #print(double, int, int, boolean)
          */
        public static void println(double d, int len, int prec, boolean sci) { print(d, len, prec, sci); print(NL); }
      
        /**
          * Funktioniert wie <code>print(double, int, int, boolean)</code> mit der Annahme, da  <code>sci</code> auf <code>false</code> gesetzt wurde und gibt zus&auml;tzlich einen Zeilenumbruch auf dem Bildschirm aus.
          * @param d Wert, der ausgegeben werden soll
          * @param len L&auml;nge des Ausgabefeldes
          * @param prec Anzahl der Nachkommastellen
           * @see #print(double, int, int, boolean)
           */
        public static void println(double d, int len, int prec)              { print(d, len, prec);      print(NL); }

        /**
          * L&ouml;scht den Bildschirminhalt. <br>
          * Funktioniert leider nicht auf allen Terminals.<br>
          */
        public static void clearScreen() {

                ios.clear();
        }


        /**
          * Die Methode gibt den uebergegebenen String als Fehlermeldung
          * aus und bricht dann das Programm ab.
          * <br> Dabei wird ein (optional) angegebener Fehler-Code geliefert.
          * @param msg Text der Fehlermeldung
          * @param code Zu uebergebender Fehlercode
          */
        public static void error(String msg, int code) {

                ios.quit(msg, code);
        }

        /**
          * Die Methode gibt den uebergegebenen String als Fehlermeldung
          * aus und bricht dann das Programm ab.
          * <br> 
          * @param msg Text der Fehlermeldung
          */
        public static void error(String msg) {

                ios.quit(msg, 1);
        }

}
