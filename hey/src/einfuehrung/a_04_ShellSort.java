package einfuehrung;
/*******************************************************************************
 * Erweiterung von Insertionsort - langsam, weil nur benachbarte Elemente
 * betrachtet werden - Verbesserung durch Tausch von Objekten, die weiter
 * voneinander entfernt sind.
 ******************************************************************************/

public class a_04_ShellSort {

	private long[] daten;

	private int laenge;

	public a_04_ShellSort(int max) {
		daten = new long[max];
		laenge = 0;
	}

	public void einfuegen(long wert) {
		daten[laenge] = wert;
		laenge++;
	}

	public void anzeige() {
		for (int j = 0; j < laenge; j++)
			System.out.print(daten[j] + " ");
		System.out.println("");
	}

	public void shellSort() {
		int innen, aussen;
		long temp;

		int abst = 1; 							// Anfangsbelegung von abst
		while (abst <= laenge / 3) {
			abst = abst * 3 + 1; 				// Abstaende (1, 4, 13, 40, 121, ...)
		}

		while (abst > 0) { 						// dekrementieren von abst bis abst=1
			System.out.println("abst: " + abst + "-> tauschen");

			for (aussen = abst; aussen < laenge; aussen++) {
				temp = daten[aussen];
				innen = aussen;
												// ein Durchgang (zB 1, 4, 13)
				while (innen > abst - 1 && daten[innen - abst] >= temp) {
					daten[innen] = daten[innen - abst];
					innen -= abst;
				}
				daten[innen] = temp;
			}
			abst = (abst - 1) / 3; 					// veraendern von abst
			System.out.println("abst neu: " + abst);

		}
	}

	public static void main(String[] args) {
		int groesse = 20;
		a_04_ShellSort arr = new a_04_ShellSort(groesse);

		for (int j = 0; j < groesse; j++) {
			long n = (int) (java.lang.Math.random() * 99);
			arr.einfuegen(n);
		}
		System.out.println("Shellsort");
		System.out.println("----------------------------");
		System.out.println("Daten unsortiert: ");
		arr.anzeige();
		arr.shellSort();
		System.out.println("Daten sortiert  : ");
		arr.anzeige();
	}
}