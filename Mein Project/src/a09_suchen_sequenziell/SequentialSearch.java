package a09_suchen_sequenziell;
/**
 * File: SearchAlgorithm.java
 * @author S. Schuierer
 */
public class SequentialSearch extends SearchAlgorithm {

    public static int search (Orderable A[], Orderable k) {
	/* Durchsucht A[1], .., A[n] nach Element k und liefert den
	   Index i mit A[i] = k; -1 sonst  */
	A[0] = k;      // Stopper
	int i = A.length;
	do i--; while (!A[i].equal(k));
	if (i !=  0) {
	    /* A[i] ist gesuchtes Element */
	    return i;
	}
	else { /* es gibt kein Element mit Schl"ussel k */
	    return -1;
	}
    }
}
