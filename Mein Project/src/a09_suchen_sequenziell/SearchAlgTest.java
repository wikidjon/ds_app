package a09_suchen_sequenziell;
/**
 *  Programm zum Testen von Suchalgorithmen 
 *
 * File: SearchAlgorithm.java
 * @author S. Schuierer
 */
public class SearchAlgTest {

   public static void main(String args[]){
      int vec[] = {15, 2, 43, 17, 4, 8, 47, 53}; // "Standard"-Array
      int k     = 4;                             // "Standard"-Schl"ussel
      if (args.length != 0) {
	 k = Integer.valueOf(args[0]).intValue();
	 if (args.length > 1) {
	    vec = new int [args.length-1];
	    for (int j = 1 ; j < args.length ; j++){
	        vec [j-1] = Integer.valueOf(args[j]).intValue();
	    }
	 }
      }

      /* t[0].i = 0 wird als Stopper verwendet */
      OrderableInt t[] = OrderableInt.array (vec);

      /* Sortieren mit Auswahlsort */
      for (int i = 1; i < t.length-1; i++) {
	  int min = i;
	  for (int j = i+1; j <= t.length-1; j++) {
	      if (t[j].less(t[min])) {
		  min = j;
	      }
	  }
	  
	  OrderableInt h = t[i]; t[i] = t[min]; t[min] = h; // swap
      }

      for (int i=1;i<t.length;i++) System.out.print(t[i]+" ");
      System.out.println();

      System.out.println(k+":"+SearchAlgorithm.search(t, new OrderableInt (k)));
      System.out.println("16:"+SearchAlgorithm.search(t, new OrderableInt (16)));
      System.out.println("-3:"+SearchAlgorithm.search(t, new OrderableInt (-3)));
   }
}
