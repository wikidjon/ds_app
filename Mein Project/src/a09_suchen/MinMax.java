package a09_suchen;
 
public class MinMax {
	
		int min1;
		int max1;
		int[] arr1;
        int first1;
        int minOfTwo;
        int maxOfTwo;
    
        public static void main(String[] args) {
    
        	MinMax m = new MinMax();
        
            int[]testarray = new int[7];
        
            testarray[0] = 3;
            testarray[1] = 23;
            testarray[2] = 5;
            testarray[3] = 4;
            testarray[4] = 34;
            testarray[5] = 24;
            testarray[6] = 12;
        
            m.findMinMax(testarray);
            System.out.println("min: " + m.min1);
            System.out.println("max: " + m.max1);
                              
	}
	
	public void findMinMax(int[] array){
	
		if(gerade(array.length)){
            
		    min1 = max1 = array[0];       
            first1 = 1;
		}
        else {
            
            first1 = 2;
            
            if(array[0] <= array[1]) {
            
                min1 = array[0];
                max1 = array[1];
            }
            
            else {
                
                min1 = array[1];
                max1 = array[0];
            }
        }
        
        for(int i = first1; i <= array.length-1 ; i= i+2) {
            
            if(array[i] <= array[i+1]) {
                
                minOfTwo = array[i];
                maxOfTwo = array[i+1];
            }
            
            else {
                
                minOfTwo = array[i+1];
                maxOfTwo = array[i];
            }
                       
            if(minOfTwo <= min1)
                min1 = minOfTwo;
            if(maxOfTwo >= max1) 
                max1 = maxOfTwo;   
        }
    }


    private boolean gerade(int a) {
        
        if(a % 2 == 0) {
            
            return false;
        
        }
        return true;
    }
}

/*Ausgabe: 
    
min: 3
max: 34

**/
