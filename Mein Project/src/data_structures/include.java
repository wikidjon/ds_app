package data_structures;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

public interface include {
	
	public Display display = new Display();
	public Shell shell = new Shell(display);
	//Setting properties for shell
	
	
	public GridLayout grid = new GridLayout(5, false);
	public Composite main_route = new Composite(shell, SWT.NO_BACKGROUND);
	public StackLayout stack = new StackLayout();
	public Composite app_page = null;
	
	//Canvas
	public Canvas canvas = new Canvas(shell, SWT.NONE);
	
	//Menus
	Menu menuBar = new Menu(shell, SWT.BAR);
	public Menu filemenu = new Menu(shell, SWT.DROP_DOWN);
	
	//Lists
	public Tree tree = new Tree(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
    
	//Buttons
	
	//Strings
	public String[] file_menu = {"New","Open","Exit"};
	public String[] help_menu = {"Help Topics","Help Manual","About Clever"};
	public String[] edit_menu = {"Cut","Copy","Paste"};
	public String[] tree_menu = {"Regular Expression","Liked List","Doubly Linked List"};
	public String aboutClever= "Project done at Hochschule Merseburg by Jonathan Vijayakumar";
	public String appIconPath = "e:\\edu\\Merseburg\\Project\\apple_256.ico";
	public String appTitle = "Clever Teacher";
	public String codeCol_msg = "This is where the source code for your clicks appears";
}
