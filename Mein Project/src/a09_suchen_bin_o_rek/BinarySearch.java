package a09_suchen_bin_o_rek;

/**
 * Binaere Suche ohne Rekursion
 */

public class BinarySearch extends SearchAlgorithm {

	public static int search(Orderable A[], Orderable k) {
		/*
		 * Durchsucht A[1], .., A[n] nach Element k und liefert den groessten
		 * Index i >= 1 mit A[i] <= k; 0 sonst
		 */
		int n = A.length;
		int l = 1;
		int r = n;
		while (l <= r) {
			int m = (l + r) / 2;
			if (k.less(A[m])) {
				r = m - 1;
			} else if (k.greater(A[m])) {
				l = m + 1;
			} else { /* A[m] = k */
				return m;
			}
		}
		return l - 1;
	}
}
