package a15_set.hash_set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetExample {
  public static void main(String[] args) {
    Set<Integer> set = new HashSet<Integer>();

    set.add(new Integer(1));
    set.add(new Integer(2));
    set.add(new Integer(3));
    set.add(new Integer(4));
    set.add(new Integer(5));
    set.add(new Integer(6));
    set.add(new Integer(7));
    set.add(new Integer(8));
    set.add(new Integer(9));
    set.add(new Integer(10));

    System.out.println("HashSet Start: ");
    for (Iterator<Integer> i = set.iterator(); i.hasNext();) {
      Integer integer = (Integer) i.next();
      System.out.println(integer);
    }

    // entfernen 6
    System.out.println("\nEntfernen integer 6");
    set.remove(new Integer(6));

    // iterator 
    System.out.println("\nHashSet Ende: ");
    for (Iterator<Integer> i = set.iterator(); i.hasNext();) {
      Integer integer = (Integer) i.next();
      System.out.println(integer);
    }

  }
}
