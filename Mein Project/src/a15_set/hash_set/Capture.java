package a15_set.hash_set;
//Generic collection conversion: HashSet and ArrayList
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Capture {

  static <T> Set<T> listToSet(List<T> list) {
    Set<T> set = new HashSet<T>();
    set.addAll(list);
    return set;
  }

  public static void main(String[] args) {
    List<?> list = new ArrayList<Date>();
    @SuppressWarnings("unused")
	Set<?> set = listToSet(list);
  }
}
