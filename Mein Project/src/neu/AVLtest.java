package neu;

import java.awt.*;

@SuppressWarnings("deprecation")

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//                              AVL-TEST-KLASSE
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
class AVLtest extends Frame{

private static final long serialVersionUID = 1L;
static AVLbaum Baum1;


//Konstruktor des Info-Fensters:
//@SuppressWarnings("deprecation")
public AVLtest(){
  super("avltest");
  setSize(1000,270);
  show();
}


//---zeichnet einen avlbaum-Knoten----------------------------------------------
public void zeichneKnoten(AVLknoten k,Graphics g,int x,int y,int vx,int vy){
 if (k==null) return;

 g.setColor(new Color(255,255,255));
 g.drawOval(x-3,y,30,20);

 g.drawLine(x+10,y,vx+10,vy+20);

 g.setColor(new Color(0,255,0));
 g.drawString(new Integer(k.key).toString(),x+5,y+13);

 g.setColor(new Color(255,0,0));
 g.drawString(new Integer(k.b).toString(),x+5,y-7);

}


//---Zeichen des avl-Baumes bis zur Tiefe 4-------------------------------------
 public void update(Graphics g){paint(g);}
 public void paint(Graphics g){
    if (Baum1==null) return;

   g.setColor(new Color(0,0,0));
   g.fillRect(0,0,1000,500);

   g.setColor(new Color(255,255,255));
   g.drawString("AVL-BAUM-VISUALISIERUNG (bis zur Tiefe 4)",20,40);
   g.setColor(new Color(255,0,0));
   g.drawString("Balance-Werte",20,70);
   g.setColor(new Color(0,255,0));
   g.drawString("Schluessel",20,85);



  //die Knoten zeichen:
   zeichneKnoten(Baum1.Wurzel,g,500,50,500,30);

   zeichneKnoten(Baum1.Wurzel.l,g,250,100,500,50);
   zeichneKnoten(Baum1.Wurzel.r,g,750,100,500,50);

   if (Baum1.Wurzel.l!=null){
     zeichneKnoten(Baum1.Wurzel.l.l,g,125,150,250,100);
     zeichneKnoten(Baum1.Wurzel.l.r,g,375,150,250,100);

     if (Baum1.Wurzel.l.l!=null){
        zeichneKnoten(Baum1.Wurzel.l.l.l,g,75,200,125,150);
        zeichneKnoten(Baum1.Wurzel.l.l.r,g,175,200,125,150);
     }
     if (Baum1.Wurzel.l.r!=null){
        zeichneKnoten(Baum1.Wurzel.l.r.l,g,315,200,375,150);
        zeichneKnoten(Baum1.Wurzel.l.r.r,g,415,200,375,150);
     }
   }

   if (Baum1.Wurzel.r!=null){
     zeichneKnoten(Baum1.Wurzel.r.l,g,625,150,750,100);
     zeichneKnoten(Baum1.Wurzel.r.r,g,875,150,750,100);

     if (Baum1.Wurzel.r.l!=null){
        zeichneKnoten(Baum1.Wurzel.r.l.l,g,575,200,625,150);
        zeichneKnoten(Baum1.Wurzel.r.l.r,g,675,200,625,150);
     }
     if (Baum1.Wurzel.r.r!=null){
        zeichneKnoten(Baum1.Wurzel.r.r.l,g,815,200,875,150);
        zeichneKnoten(Baum1.Wurzel.r.r.r,g,915,200,875,150);
     }
   }

}






//------------------------------------------------------------------------------
 public static void main(String args[]){
    System.out.println("------------avlbaum-test--------------");

  //Test:
    Baum1=new AVLbaum();

    Baum1.einfuegen(10);
    Baum1.einfuegen(15);
    Baum1.einfuegen(11);
    Baum1.einfuegen(4);
    Baum1.einfuegen(8);
    Baum1.einfuegen(7);
    Baum1.einfuegen(3);
    Baum1.einfuegen(2);
    Baum1.einfuegen(13);

    System.out.println("------ Durchlaufreihenfolgen----------");
    Baum1.preorder();
    Baum1.inorder();
    Baum1.postorder();

    System.out.println("Tiefe des Baumes: "+Baum1.Tiefe(Baum1.Wurzel));

   //Baum grafisch darstellen:
    @SuppressWarnings("unused")
	AVLtest f = new AVLtest();

  }
}
