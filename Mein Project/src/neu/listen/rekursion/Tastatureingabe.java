package neu.listen.rekursion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tastatureingabe	{

	static BufferedReader eing = new BufferedReader(new InputStreamReader(System.in));

	public static int readInt (final String prompt)		{
		String zeile;
		while (true)	{
			System.out.print (prompt);
			try		{
				zeile = eing.readLine();
				return Integer.parseInt(zeile);
			} catch (final NumberFormatException e)  {
				System.out.println ("Bitte eine ganze Zahl eingeben!!!");
			} catch (final IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}


	public static String readString (final String prompt)	{
		while (true){
			System.out.print(prompt);
			try			{
				return eing.readLine();
			} catch (final IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

	}

}


