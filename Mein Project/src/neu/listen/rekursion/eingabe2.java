package neu.listen.rekursion;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**  Die Klasse enth�lt Eingabemethoden f�r die
 * Eingabe eines Strings mit und ohne Eingabeaufforderung
 * Eingabe eines Zeichens mit und ohne Eingabeaufforderung
 * Eingabe einer Zahl (integer) mit und ohne Eingabeaufforderung
 **/

public class eingabe2 {

	//***************************************************************************************************************
	/**  Eingabe eines Zeichens ohne Eingabeaufforderung *******
	 * die Methode liefert ein zeichen zur�ck, das �ber Tastatur eingeben wurde **/

	public char eingChar() {
		char cin = ' ';
		final DataInputStream in = new DataInputStream(System.in);
		try {
			cin=in.readChar();
			System.out.print("..." + cin);
		}
		catch(final IOException e) {
			System.out.println(e.toString());
		}
		return cin;
	}

	//***************************************************************************************************************
	/**  Eingabe eines Zeichens mit Eingabeaufforderung *******
	 * die Methode liefert ein zeichen zur�ck, das �ber Tastatur eingeben wurde **/

	public char eingChar(final String s) {
		char cin = ' ';
		final DataInputStream in = new DataInputStream(System.in);
		try {
			System.out.print(s);
			cin=in.readChar();
		}
		catch(final IOException e) {
			System.out.println(e.toString());
		}
		return cin;
	}


	//***************************************************************************************************************
	/**  Eingabe eines Integer ohne Eingabeaufforderung *******
	 * die Methode liest einen String ein und wandelt ihn in int **/

	public int eingInt() {
		final Integer i = new Integer(eingString());
		return i.intValue();
	}

	//***************************************************************************************************************
	/**  Eingabe eines Integer mit Eingabeaufforderung *******
	 * die Methode liest einen String ein und wandelt ihn in int **/
	public int eingInt(final String s) {
		final Integer i = new Integer(eingString(s));
		return i.intValue();
	}


	/**  Eingabe eines String ohne Eingabeaufforderung *******
	 * die Methode liefert einen String zur�ck, der �ber Tastatur eingeben wurde **/

	//***************************************************************************************************************
	public String eingString() {
		String sin = " ";
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			sin=in.readLine();
		}
		catch(final IOException e) {
			System.out.println(e.toString());
		}
		return sin;
	}


	//***************************************************************************************************************
	/**  Eingabe eines String mit Eingabeaufforderung *******
	 * die Methode liefert einen String zur�ck, der �ber Tastatur eingeben wurde **/

	public String eingString(final String s) {
		String sin = " ";
		final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.print(s);
			sin=in.readLine();
		}
		catch(final IOException e) {
			System.out.println(e.toString());
		}
		return sin;
	}
}
