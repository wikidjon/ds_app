package neu.listen.rekursion;

public class FakImperativ {
	public static int fakBerechnung(int x) {
		int y = 1;

		while (x > 1) {
			y = y * x;
			x--;
		}
		return y;
	}

	public static void main(final String[] args) {
		int z;

		final eingabe2 ein = new eingabe2();
		System.out.print("Zahl: ");
		z = ein.eingInt();
		System.out.println("Fakultaet(" + z + "!) = " + fakBerechnung(z));
	}
}
