package a0_listen1;

public class ListEmptyException extends RuntimeException {
	
	static final long serialVersionUID = 12345;
	
    public ListEmptyException (String msg) {
	super (msg);
    }

    public ListEmptyException () {
	super ();
    }
}
