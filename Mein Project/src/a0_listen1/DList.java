package a0_listen1;

import java.util.Iterator;

public class DList {
	public static class Knoten {
		Object obj;
		Knoten vorgaenger, naechster;

		public Knoten(Object o, Knoten v, Knoten n) {
			obj = o;
			vorgaenger = v;
			naechster = n;
		}

		public Knoten() {
			obj = null;
			vorgaenger = naechster = null;
		}

		public void setElement(Object o) {
			obj = o;
		}

		public Object getElement() {
			return obj;
		}

		public void setNaechster(Knoten n) {
			naechster = n;
		}

		public Knoten getNaechster() {
			return naechster;
		}

		public void setVorgaenger(Knoten p) {
			vorgaenger = p;
		}

		public Knoten getVorgaenger() {
			return vorgaenger;
		}
	}

	private Knoten kopf = null;
	private Knoten ende = null;

	public DList() {
		kopf = new Knoten();
		ende = new Knoten();
		kopf.setNaechster(ende);
		ende.setVorgaenger(kopf);
		ende.setNaechster(ende);
	}

	public void addVorn(Object o) {
		Knoten n = new Knoten(o, kopf, kopf.getNaechster());
		kopf.getNaechster().setVorgaenger(n);
		kopf.setNaechster(n);
	}

	public void addEnde(Object o) {
		Knoten l = ende.getVorgaenger();
		Knoten n = new Knoten(o, l, ende);
		l.setNaechster(n);
		ende.setVorgaenger(n);
	}

	public Object getVorn() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		return kopf.getNaechster().getElement();
	}

	public Object getEnde() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		return ende.getVorgaenger().getElement();
	}

	public Object removeVorn() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		Object o = kopf.getNaechster().getElement();
		kopf.setNaechster(kopf.getNaechster().getNaechster());
		kopf.getNaechster().setVorgaenger(kopf);
		return o;
	}

	public Object removeHinten() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		Knoten n = ende.getVorgaenger();
		n.getVorgaenger().setNaechster(ende);
		ende.setVorgaenger(n.getVorgaenger());
		return n.getElement();
	}

	public int size() {
		int s = 0;
		Knoten n = kopf;
		while (n.getNaechster() != null) {
			s++;
			n = n.getNaechster();
		}
		return s;
	}

	public boolean isEmpty() {
		return kopf.getNaechster() == ende;
	}

	public Iterator<?> iterator() {
		return new ListenIterator();
	}

	public static void main(String args[]) {
		DList lst = new DList();
		Iterator<?> it = lst.iterator();
		System.out.println("Listenaufbau von vorn");
		System.out.println("---------------------");
		lst.addVorn("Drei");
		lst.addVorn("Zwei");
		lst.addVorn("Eins");
		System.out.println("Elemente anhaengen");
		System.out.println("---------------------");
		lst.addEnde("Vier");
		lst.addEnde("Fuenf");
		lst.addEnde("Sechs");

		System.out.println("Durch Liste iterieren");
		System.out.println("---------------------");
		it = lst.iterator();
		while (it.hasNext()) { 		// Problem: Iteration nur bis vorletztes Element - bitte aendern
			System.out.println((String) it.next());
		}
		// System.out.println ((String) it.next());
		System.out.println("--------------------");
		System.out.println("Elemente entfernen");
		System.out.println("--------------------");
		while (!lst.isEmpty()) {
			System.out.println((String) lst.removeVorn());
			if (lst.isEmpty())
				System.out.println("Liste ist leer");
		}
	}
	
	// ********************************************************
		class ListenIterator implements Iterator<Object> {
			private Knoten Knoten = null;

			public ListenIterator() {
				Knoten = kopf.getNaechster();
			}

			public boolean hasNext() { 				// kommt von Iterator
				return Knoten.getNaechster() != ende;
			}

			public void remove() { 					// kommt von Iterator
				throw new UnsupportedOperationException();
			}

			public Object next() { 					// kommt von Iterator
				if (!hasNext())
					throw new java.util.NoSuchElementException();
				Object o = Knoten.getElement();
				Knoten = Knoten.getNaechster();
				return o;
			}
		}

		// ********************************************************
}
