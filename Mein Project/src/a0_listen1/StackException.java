package a0_listen1;

public class StackException extends RuntimeException {
	static final long serialVersionUID = 12345;
	
	public StackException (String msg) { super (msg); }
	
	public StackException () {}
}
