package a0_listen1;
public class List {
	static class Knoten {
		Object obj;
		Knoten naechster;
		
		public Knoten(Object o, Knoten n) {
			obj = o;
			naechster = n;
		}
		
		public Knoten() {
			obj = null;
			naechster = null;
		}
		
		public void setElement(Object o) {
			obj = o;
		}
		
		public Object getElement() {
			return obj;
		}
		
		public void setNaechster(Knoten n) {
			naechster = n;
		}
		
		public Knoten getNaechster() {
			return naechster;
		}
	}
	
	private Knoten head = null;
	
	public List() {
		head = new Knoten();
	}
	
	public void addFirst(Object o) {
		Knoten n = new Knoten(o, head.getNaechster());
		head.setNaechster(n);
	}
	
	public void addLast(Object o) {
		Knoten l = head;
		while (l.getNaechster() != null)
			l = l.getNaechster();
		Knoten n = new Knoten(o, null);
		l.setNaechster(n);
	}
	
	public Object getFirst() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		return head.getNaechster().getElement();
	}
	
	public Object getLast() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		Knoten l = head;
		while (l.getNaechster() != null)
			l = l.getNaechster();
		return l.getElement();
	}
	
	public Object removeFirst() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		Object o = head.getNaechster().getElement();
		head.setNaechster(head.getNaechster().getNaechster());
		return o;
	}
	
	public Object removeLast() throws ListEmptyException {
		if (isEmpty())
			throw new ListEmptyException();
		Knoten l = head;
		while (l.getNaechster().getNaechster() != null)
			l = l.getNaechster();
		Object o = l.getNaechster().getElement();
		l.setNaechster(null);
		return o;
	}
	
	public int size() {
		int s = 0;
		Knoten n = head;
		while (n.getNaechster() != null) {
			s++;
			n = n.getNaechster();
		}
		return s;
	}
	
	public boolean isEmpty() {
		return head.getNaechster() == null;
	}
	
	public static void main(String args[]) {
		List lst = new List();
		lst.addFirst("Drei");
		lst.addFirst("Zwei");
		lst.addFirst("Eins");
		
		lst.addLast("Vier");
		lst.addLast("Fuenf");
		lst.addLast("Sechs");
		System.out.println("Listengroesse: " + lst.size());
		
		while (! lst.isEmpty()) {
			System.out.println((String) lst.removeFirst());
		}
		System.out.println("Listengroesse: " + lst.size());
	}
}

