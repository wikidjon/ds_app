package a80_generics;
class a01_IntegerContainer 
{ 
  private int wert; 
 
  void setWert( int wert )   { 
    this.wert = wert; 
  } 
 
  int getWert() { 
    return wert; 
  } 
}

/*************************************************************
man muesste einen zweiten Container f�r Strings implementieren 
-> Quellcode duplizieren und int mit String ersetzen

class StringContainer { 
  private String wert; 
 
  void setWert( String wert ) { 
    this.wert = wert; 
  } 
 
  String getWert()   { 
    return wert; 
  } 
}

Ein von einem Datentyp unabh�ngig programmierter Algorithmus
hei�t generisch und die M�glichkeit, in Java mit generischen 
Typen zu arbeiten, hei�t Generics

In Java koennen generische Typen nur Objekte sein, aber keine 
primitiven Datentypen. 
*************************************/