package a80_generics;
public class ContainerRun {
	@SuppressWarnings("unused")
	public static void main(String[] args) {

		a02_Container<String> stringa02_Container = new a02_Container<String>();
		a02_Container<Integer> inta02_Container = new a02_Container<Integer>();
		a02_Container<Double> class_Container = new a02_Container<Double>();

		class_Container.setWert(2.6, 4.6);
		stringa02_Container.setWert("Hallo", "Merseburg");
		
		System.out.println(class_Container.getWert());
		System.out.println(stringa02_Container.getWert());
		
	}
}
