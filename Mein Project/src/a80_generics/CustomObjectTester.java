package a80_generics;

/*
License for Java 1.5 'Tiger': A Developer's Notebook
     (O'Reilly) example package

Java 1.5 'Tiger': A Developer's Notebook (O'Reilly) 
by Brett McLaughlin and David Flanagan.
ISBN: 0-596-00738-8

You can use the examples and the source code any way you want, but
please include a reference to where it comes from if you use it in
your own products or services. Also note that this software is
provided by the author "as is", with no expressed or implied warranties. 
In no event shall the author be liable for any direct or indirect
damages arising in any way out of the use of this software.
*/


import java.io.IOException;
import java.io.PrintStream;


import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
class GuitarManufacturerList extends LinkedList<String> {

	private static final long serialVersionUID = 1L;

public GuitarManufacturerList() {
    super();
  }

  public boolean add(String manufacturer) {
    if (manufacturer.indexOf("Guitars") == -1) {
      return false;
    } else {
      super.add(manufacturer);
      return true;
    }
  }
}

public class CustomObjectTester {

  /** A custom object that extends List */
  private GuitarManufacturerList manufacturers;

  public CustomObjectTester() {
    this.manufacturers = new GuitarManufacturerList();
  }

  /**
   * <p>Test iterating over an object that extends List</p>
   */
  public void testListExtension(PrintStream out) throws IOException {
    // Add some items for good measure
    manufacturers.add("Epiphone Guitars");
    manufacturers.add("Gibson Guitars");

    // Iterate with for/in
    for (String manufacturer : manufacturers) {
      out.println(manufacturer);
    }
  }

  public static void main(String[] args) {
    try {
      CustomObjectTester tester = new CustomObjectTester();

      tester.testListExtension(System.out);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
