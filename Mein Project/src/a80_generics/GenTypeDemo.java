package a80_generics;

// Here, T is bound by Object by default. 
class Gen3<T> {  
  T ob; // here, T will be replaced by Object 
    
  Gen3(T o) {  
    ob = o;  
  }  
  
  // Return ob.  
  T getob() {  
    return ob;  
  }  
}  
 
// Here, T is bound by String.

class GenStr<T extends String> { 
  T str; // here, T will be replaced by String 
 
  GenStr(T o) {  
    str = o;  
  }  
 
  T getstr() { return str; } 
}

public class GenTypeDemo {  
  public static void main(String args[]) {  
    Gen3<Integer> iOb = new Gen3<Integer>(99);  
    Gen3<Float> fOb = new Gen3<Float>(102.2F); 
 
    System.out.println(iOb.getClass().getName()); 
    System.out.println(fOb.getClass().getName()); 
  } 
}

