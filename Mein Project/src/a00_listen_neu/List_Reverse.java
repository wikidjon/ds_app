package a00_listen_neu;

import java.util.*;

public class List_Reverse {
  public static void main(String args[]) {
    ArrayList<String> simpsons = new ArrayList<String>();
    simpsons.add("Bart");
    simpsons.add("Hugo");
    simpsons.add("Lisa");
    simpsons.add("Marge");
    simpsons.add("Homer");
    simpsons.add("Maggie");
    simpsons.add("Roy");
    Collections.sort(simpsons);
    System.out.println(simpsons);
  }
}