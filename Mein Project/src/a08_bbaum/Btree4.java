package a08_bbaum;


public class Btree4 {

  private int[] keys;
  private Btree4[] subs;
  private int order;

  public Btree4 (int n) {
    keys = new int[n-1];
    subs = new Btree4[n];
    order = n;
  }

  private boolean leaf () {
   for (int i=0;i<order;i++)
      if (subs[i] != null)
	{return false;};
   return true;
  }

  private boolean full () {
   for (int i=0;i<order;i++)
      if (subs[i] == null)
	{return false;};
   return true;
  }  
}
