package a17_reg_expr;

/********************************************************
 Zerlegen von Zeichenketten durch einen regul�ren Ausdruck 
 ********************************************************/

import java.util.Arrays;
import java.util.regex.*;

public class RegAusdruck2 {
	public static void main(String[] args) {

		String pfad = "www.hs-merseburg.de";
		String[] teile = pfad.split(Pattern.quote("."));	//. weg!!
		System.out.println(Arrays.toString(teile)); 			// [www, hs-merseburg, de]
	}

	public String[] split(String regex, int limit) {
		return Pattern.compile(regex).split((CharSequence) this, limit);
	}

	public String[] split(String regex) {
		return split(regex, 0);		// auch  split(".", 0)
	}
}