package a17_reg_expr;

import java.util.*;

public class StringScannen2
{
  public static void main(String[] args)
  {
    String s1 = "3 Katzen jagten in 30 Tagen 58 M�use";
    Scanner sc = new Scanner(s1).useDelimiter("\\D+");
//  ein scanner kann primitive datentypen und strings mit regulaeren ausdruecken parsen
    int i = 1;
    while(sc.hasNextInt())
      System.out.println(i++ + "-ter Wert: " + sc.nextInt());
    sc.close();
  }
}
