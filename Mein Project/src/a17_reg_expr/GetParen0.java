package a17_reg_expr;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetParen0 {
	public static void main(String[] args) {
		
			// Vorgehen: Aufbau eines regul�ren Ausdrucks und Pr�fung �ber die Klasse Pattern
		Pattern p = Pattern.compile("i.*ava"); 	
			// mit d beginnen, dann mehrere Zeichen und auf ava enden
			// p ist eine Repr�sentation eines regul�ren Ausdruckes.
			// der Punkt steht f�r ein beliebiges Zeichen, und der * ist ein Quantifizierer, 
			// der wahllos viele beliebige Zeichen erlaubt (greedy - gierig)
		Matcher m = p.matcher("ie sprache java ist keine java insel ... ");   //mit lava
			// m ist ein Tool, das match operationen auf Strings mit Pattern ausf�hrt.
		m.lookingAt();
		String result1 = m.group(0);
		 // group(int group) gibt die subsequencen der match operation zur�ck
		System.out.println("Ergebnis:   "+ result1);
	}
}


// der einfache Aufruf matches() auf einem String-Objekt beziehungsweise Pattern.matches() 
// ist nur eine Abk�rzung f�r die �bersetzung eines Patterns und Anwendung von matches():

