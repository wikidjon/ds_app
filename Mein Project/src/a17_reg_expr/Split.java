package a17_reg_expr;

import java.util.regex.*;

// Splittet einen String in ein Array

public class Split {
  public static void main(String[] args) {
    String[] x = Pattern.compile(" ").split("Das ist der String");
    for (int i=0; i<x.length; i++) {
      System.out.println(i + " \"" + x[i] + "\"");		// ????
    }
  }
}
