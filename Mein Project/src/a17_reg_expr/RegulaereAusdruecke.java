package a17_reg_expr;

import java.util.*;
import java.util.regex.*;

public class RegulaereAusdruecke
{
  public static void main(String[] args)
  {
    System.out.print("Zeichenkette eingeben: ");
    Scanner sc = new Scanner(System.in);
    	// ein scanner kann primitive datentypen und strings mit regul�ren ausdr�cken parsen
    String s = sc.nextLine();					
    
    Pattern p = Pattern.compile("<.+>");
    	// der Punkt im regul�ren Ausdruck steht f�r ein beliebiges Zeichen, 
 		// und das + ein Quantifizierer, der wahllos viele beliebige Zeichen erlaubt.
    Matcher m = p.matcher(s);
    if(m.find())
      System.out.printf("Zeichenfolge %s gefunden, Position %d - %d ",
                         m.group(), m.start(), (m.end()-1));
    else
      System.out.println("Muster nicht gefunden");
    sc.close();
  }
}
