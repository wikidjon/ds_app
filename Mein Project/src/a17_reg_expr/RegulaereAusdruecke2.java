package a17_reg_expr;

import java.util.*;
import java.util.regex.*;

public class RegulaereAusdruecke2
{
  public static void main(String[] args)
  {
    System.out.print("Zeichenkette eingeben: ");
    Scanner sc = new Scanner(System.in);
//   ein text scanner kann primitive datentypen und strings mit regulaeren ausdruecken parsen
    String s = sc.nextLine();
    Pattern p = Pattern.compile("<\\w+>");
    Matcher m = p.matcher(s);
    while(m.find())
      System.out.printf("Zeichenfolge %s gefunden, Position %d - %d ",
                         m.group(), m.start(), (m.end()-1));
  }
}
