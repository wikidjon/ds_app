package a17_reg_expr;

import java.util.regex.*;
import java.util.*;

public class SchrittweiseErsetzen
{
  public static void main(String[] args)
  {
    Pattern p = Pattern.compile("[A-Z]+");
    Matcher m = p.matcher("DIES IST EIN TEXT ZUM TESTEN DES PROGRAMMS");
    StringBuffer sb = new StringBuffer();
    while(m.find())
    {
      String teil = m.group();
      System.out.print(teil + " umwandeln?");
      Scanner sc = new Scanner(System.in);
      String antw = sc.nextLine();
      if (antw.charAt(0) == 'j')
        m.appendReplacement(sb, teil.toLowerCase());
    }
    m.appendTail(sb);
    System.out.println("Ergebnis: " + sb);
  }
}
