package a17_reg_expr;

import java.util.regex.*;

public class StringSplitten
{
  public static void main(String[] args)
  {
    String satz = "Das Haus am Strand, wei� und sch�n, wurde von der Sonne " +
                  "beschienen.\n Keiner war zu Hause.";
    Pattern p = Pattern.compile("[,.]\\s*");			//Leerzeichen weg!!!
    String[] sa = p.split(satz);
    int i = 1;
    for(String s: sa)
      System.out.printf("%3d-ter Teil:  %s\n", i++ , s);
  }
}
