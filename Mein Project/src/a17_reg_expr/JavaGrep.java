package a17_reg_expr;

/*
 * JavaGrep.java - Beispiel f�r grep-Funktionalit�t 
 *
 */

import java.io.*;
import java.util.regex.*;

public class JavaGrep {
   public static void main(String[] args) {

	   // grep dient unter Unix der Suche und Filterung definierter Zeichenketten in Dateien 
	   // grep steht f�r global/regular expression/print 
	   // oder auch global search for a regular expression and print out matched lines, 
	   // also f�r �globale Suche nach einem regul�ren Ausdruck und Ausgabe �bereinstimmender Zeilen�. 
	   // Historisch entwickelte sich der Name aus dem Kommando g/re/p des Unix-Standardeditors ed.

      try {
         LineNumberReader eingabe = new LineNumberReader(new FileReader("D:/Ausgang/TextdateiLesen.java"));
         String zeile;

         Pattern p = Pattern.compile("DateiDrucken");			//System

         while((zeile = eingabe.readLine()) != null) {
           // Datei zeilenweise durchsuchen
           Matcher m = p.matcher(zeile);

           while(m.find() == true) {
        	   System.out.println (" Gefunden in Zeile "+ eingabe.getLineNumber() 
        			   + "   --> Start Spalte: " + ((m.start()) + "   - Ende Spalte: " + m.end())); 
        			   	  
           }
         }
      } catch(Exception e) {
    	  System.out.println(" Problem:  " + e.getMessage()); 
      } 
   }
}
