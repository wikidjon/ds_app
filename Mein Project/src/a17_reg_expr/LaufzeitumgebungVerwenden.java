package a17_reg_expr;

import java.io.*;

public class LaufzeitumgebungVerwenden {
	public static void main(String[] args) {
		try {
			Process p = Runtime.getRuntime().exec("calc");
			// jede Java Applikation verf�gt �ber einfache  Instanz der Runtime-Klasse, die es gestattet 
			// auf die Laufzeitumgebung der App zuzugreifen (getRuntime())
			p.waitFor();
			System.out.println("R�ckgabewert: " + p.exitValue());
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		} catch (InterruptedException iex) {
			System.err.println(iex.getMessage());
		}
	}
}
