package a10_hastable;

public class HashTableOverflowException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6782098770146148193L;

	public HashTableOverflowException (String s) {
	super (s);
    }

    public HashTableOverflowException () {
    }
}