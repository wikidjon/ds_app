package a10_hastable;

import java.util.LinkedList;
import java.util.Iterator;
@SuppressWarnings("unchecked")

public class LinkedHashTable implements Hashing {
	@SuppressWarnings("rawtypes")
	LinkedList[] table; 					// Feld mit Listen

	public LinkedHashTable(int size) {
		table = new LinkedList[size];		// Feld aufbauen
	}

	public void add(Object o) {
		int i = (o.hashCode() & 0x7fffffff) % table.length;  		// Feldindex ueber Hashwert bestimmen
		if (table[i] == null)										// noch keine Liste vorhanden
			table[i] = new LinkedList<Object>();
		table[i].addLast(o);										// an Liste anhaengen
	}

	public boolean contains(Object o) {
		int idx = (o.hashCode() & 0x7fffffff) % table.length;		// Feldindex �ber Hashwert bestimmen
		if (table[idx] != null) {
			Iterator<?> it = table[idx].iterator();					// Liste gefunden
			while (it.hasNext()) {									// sequenzielle Suche nach Element
				Object obj = it.next();
				if (obj.equals(o))
					return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		LinkedHashTable tbl = new LinkedHashTable(20);
		tbl.add("Hallo");
		tbl.add("Du");
		tbl.add("kleiner");
		tbl.add("Hash");

		System.out.println("Du: " + tbl.contains("Du"));
		System.out.println("grosser: " + tbl.contains("grosser"));
	}
}
