package a10_hastable;

public interface Hashing {
	public void add (Object o) throws HashTableOverflowException;

	public boolean contains (Object o);
}
