package a10_hastable;

import java.util.Map;
import java.util.TreeMap;

//Use treemap
public class Freq {
  @SuppressWarnings("unused")
private static final Integer ONE = new Integer(1);

  public static void main(String args[]) {
    Map<String, String> m = new TreeMap<String, String>();

    m.put("a Key", "a Value");
    m.put("Java2s", "www.java2s.com");

    System.out.println(m.size() + " keys detected:");
    System.out.println(m);
  }
}