package a10_hastable;

import java.util.Enumeration;
import java.util.Hashtable;

public class HashtableDemo {

  public static void main(String[] argv) {

    Hashtable<String, String> h = new Hashtable<String, String>();

	h.put("Pink Floyd   ", "Umma Gumma");
	h.put("Eric Clapton", "My Fathers Eyes");
	h.put("Otis Reading", "Dock of the Bay");
	h.put("Rolling Stones", "As Tears go by");
	h.put("Al Jarrau  ", "Hallo");
	h.put("Jimi Hendix", "Hey Joe");
	h.put("Iron Butterfly", "In a Gadda Da Vida");

	// 1. version: ein wert zum key
	String queryString = "Eric Clapton";
	System.out.println("Suche nach :" + queryString + ".");
	String resultString = (String) h.get(queryString);
	System.out.println("Gefunden : " + resultString);
	System.out.println();
    

    // 2. alle key/value Paare
    Enumeration<String> k = h.keys();
    while (k.hasMoreElements()) {
      String key = (String) k.nextElement();
      System.out.println("Key: " + key + "; \tValue: " + (String) h.get(key));
    }
  }
}