package sample;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class gui {
	public static void main(String args[]){
		Display display = new Display();
		Shell shell = new Shell(display);
				
		GridLayout grid = new GridLayout(3, false);
		
		shell.setLayout(grid);
		
		//Menu drop down 1
		Menu menuBar = new Menu(shell, SWT.BAR);
        MenuItem cascadeFileMenu = new MenuItem(menuBar, SWT.CASCADE);
        cascadeFileMenu.setText("&File");
        
        Menu filemenu = new Menu(shell, SWT.DROP_DOWN);
        cascadeFileMenu.setMenu(filemenu);
        
        MenuItem item1 = new MenuItem(filemenu, SWT.PUSH);
        item1.setText("Hey!");
        
        MenuItem item2 = new MenuItem(filemenu, SWT.PUSH);
        item2.setText("Exit");
        
        //menu drop down 2
        MenuItem cascadeFileMenu2 = new MenuItem(menuBar, SWT.CASCADE);
        cascadeFileMenu2.setText("&Help");
        
        Menu filemenu2 = new Menu(shell, SWT.DROP_DOWN);
        cascadeFileMenu2.setMenu(filemenu2);
        
        MenuItem item3 = new MenuItem(filemenu2, SWT.PUSH);
        item3.setText("Hey!");
        
        MenuItem item4 = new MenuItem(filemenu2, SWT.PUSH);
        item4.setText("Exit");
        
        
        
        
        
        item2.addListener(SWT.Selection, event -> {
        	shell.getDisplay().dispose();
        	System.exit(0);
        });
        
        shell.setMenuBar(menuBar);
		
		
//		shell.pack();
		shell.open();
		
		while(!shell.isDisposed()){
			if(!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
