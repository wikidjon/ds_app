package a0_arztpraxis;

public class ListeTest {
    
    public static void main(String [] args) {
        Liste liste = new Liste();
        System.out.println("Liste angelegt....");
        System.out.println("---------------------------\n");
        
        System.out.println("Die Liste sollte leer sein.");
        liste.durchlaufe();
        System.out.println("Anzahl der Listenelemente = " + liste.zaehleElemente());
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der Einf�gen-Methoden....");
        liste.einfuegeKopf(new Patient("Meier"));
        liste.einfuegeKopf(new Patient("Mueller"));
        liste.einfuegeKopf(new Patient("Schulze"));
        liste.einfuegeEnde(new Patient("Bauer"));
        liste.einfuegeEnde(new Patient("Test"));
        System.out.println("Die Liste sollte jetzt die Elemente Meier ... enthalten.");
        liste.durchlaufe();
        System.out.println("Anzahl der Listenelemente = " + liste.zaehleElemente());
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der loescheKopf() und loescheEnde()-Methoden....");
        liste.loescheKopf();
        liste.loescheEnde();
        System.out.println("Die Liste sollte jetzt die Elemente 2, 3, 4 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... nochmaliger Aufruf der loescheKopf() und loescheEnde()-Methoden....");
        liste.loescheKopf();
        liste.loescheEnde();
        System.out.println("Die Liste sollte jetzt nur noch das Element 3 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der loescheEnde()-Methode....");
        liste.loescheEnde();
        System.out.println("Die Liste sollte jetzt leer sein.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        
    /*
    System.out.println("Jetzt wird eine NullPointerException ausgeloest.");
          liste.loescheKopf();
     */
        
        System.out.println("... nochmaliger Aufruf der einfuegeKopf()-Methode....");
        liste.einfuegeKopf(new Patient("Paul"));
        liste.einfuegeKopf(new Patient("Inge"));
        liste.einfuegeKopf(new Patient("Ulf"));
        liste.einfuegeKopf(new Patient("Hubert"));
        liste.einfuegeKopf(new Patient("test"));
        System.out.println("Die Liste sollte jetzt die Elemente Paul ... enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der finde(3)-Methode....");
        liste.finde(new Patient("Meier"));
        // liste.aktuell sollte jetzt auf das Element mit dem Inhalt 3 zeigen.
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der einfuegeHinter(33)-Methode....");
        liste.einfuegeHinter(new Patient("Meier"));
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 4, 5 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der finde(33)-Methode....");
        liste.finde(new Integer(33));
        // liste.aktuell sollte jetzt auf das Element mit dem Inhalt 33 zeigen.
        System.out.println("... Aufruf der loescheNachfolger()-Methode....");
        liste.loescheNachfolger();
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 5 enthalten.");
        liste.durchlaufe();
        System.out.println("---------------------------\n");
        
        System.out.println("... Aufruf der loescheNachfolger()-Methode....");
        liste.loescheNachfolger();
        System.out.println("Die Liste sollte jetzt die Elemente 1, 2, 3, 33 enthalten.");
        liste.durchlaufe();
        // liste.aktuell sollte jetzt den Wert null haben.
        System.out.println("---------------------------\n");
        
        System.out.println("Jetzt wird eine NullPointerException ausgeloest.");
        // (Knoten mit Attribut daten == Integer(5) hat keinen Nachfolger.)
        liste.loescheNachfolger();
    }
}