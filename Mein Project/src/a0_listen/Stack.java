package a0_listen;

import java.util.*;

class Stack {
    private ListenElement stackpointer;
    
    void durchlaufe() {
        ListenElement aktuell = stackpointer;
        while (aktuell != null) {
            // bearbeite aktuelles Element, z.B.
            System.out.print(aktuell.getDaten() + " ");
            // gehe ein Element weiter
            aktuell = aktuell.getNaechstes();
        }
        System.out.println();
    }
    
    boolean isEmpty() {
        return stackpointer == null;
    }
    
    Object pop() {
        if (isEmpty())
            throw new NoSuchElementException("pop() for empty stack");
        Object popObject = stackpointer.getDaten();
        stackpointer = stackpointer.getNaechstes();
        return popObject;
    }
    
    void push(Object newObject) {
        ListenElement newElement =
        new ListenElement(newObject, stackpointer);
        stackpointer = newElement;
    }
}
