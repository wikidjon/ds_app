package a0_listen;

import java.util.*;

class Schlange
{
   private ListenElement ende;
   private ListenElement kopf;

   Object abarbeite()
   {
      if (istLeer())
         throw new NoSuchElementException("Schlange ist leer");
      Object zuBearbeiten = kopf.getDaten();
      kopf = kopf.getNaechstes();
      return zuBearbeiten;
   }

   void anfuege(Object neuesObject)
   {
      if (istLeer())
      {
         kopf = new ListenElement(neuesObject);
         ende = kopf;
      }
      else
      {
         ende.setNaechstes(new ListenElement(neuesObject));
         ende = ende.getNaechstes();
      }
   }

   void durchlaufe()
   {
      ListenElement aktuell = kopf;
      while (aktuell != null)
      {
         // bearbeite aktuelles Element, z.B.
         System.out.print(aktuell.getDaten() + " ");
         // gehe ein Element weiter
         aktuell = aktuell.getNaechstes();
      }
      System.out.println();
   }

   boolean istLeer()
   {
      return kopf == null;
   }
}
