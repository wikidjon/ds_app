package a0_listen;

/*
    SchlangeMitStack (Schlange implementiert mit Stack)
    Die Klasse SchlangeMitListe verwendet intern weder ein Feld noch eine Liste,
    sondern einen Stack.
*/

public class SchlangeMitStack
{
   private Stack s = new Stack();


   /*
   Eine Schlange liefert das zuerst gespeicherte Element zur�ck,
   ein Stack jedoch das zuletzt gespeicherte.
   Das von abarbeite() ben�tigte Element liegt also als unterstes Element
   auf dem Stack, der Stack gibt Ihnen jedoch nur das oberste Element.

   Sie m�ssen eine M�glichkeit finden, die Elemente, die auf dem von Ihnen
   ben�tigten Element liegen, an anderer Stelle zwischenzuspeichern.
   Benutzen Sie z.B. einen zweiten, tempor�ren Stack f�r die Zwischenspeicherung
   der Elemente.
*/
   Object abarbeite()
   {
      Stack buf = new Stack();

      // alle Elemente vom Stack entnehmen und im Zwischenspeicher buf ablegen
      while(!s.isEmpty())
         buf.push(s.pop());

      // das gew�nschte Element liegt zuoberst auf dem Zwischenspeicher
      Object o = buf.pop();

      // Inhalt des Zwischenspeichers wieder in s einfuegen
      while(!buf.isEmpty())
         s.push(buf.pop());

      return o;
   }

   // anfuege() legt das einzuf�gende Element auf dem Stack s ab,
   // der intern verwendet wird.
   void anfuege(Object neuesObject)
   {
      s.push(neuesObject);
   }

// Hier haben wir das gleiche Problem, wie bei der Operation abarbeite().
   void durchlaufe()
   {
      Stack buf = new Stack();

      // alle Elemente vom Stack entnehmen und im Zwischenspeicher buf ablegen
      while(!s.isEmpty())
         buf.push(s.pop());

      // Inhalt des Zwischenspeichers wieder in s einf�gen und dabei
      // das gerade dem Zwischenspeicher entnommenen Objekt bearbeiten
      while(!buf.isEmpty())
      {
         Object tmp = buf.pop();
         System.out.print(tmp + " ");
         s.push(tmp);
      }

      System.out.println();
   }


   boolean istLeer()
   {
      return s.isEmpty();
   }


}