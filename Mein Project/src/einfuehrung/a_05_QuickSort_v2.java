package einfuehrung;
/*****************************************************************
 * Partitionieren der Folge in groessere und kleinere Haelfte
 * sortieren der hinteren haelfte
 * mischen
 ******************************************************************/

public class a_05_QuickSort_v2 {
	private long[] daten;

	private int laenge;

	public a_05_QuickSort_v2(int max) {
		daten = new long[max];
		laenge = 0;
	}

	public void einfuegen(long wert) {
		daten[laenge] = wert; 					// einfuegen und groesse incrementieren
		laenge++;
	}

	public void anzeigen() {
		// System.out.print("Daten:");
		for (int j = 0; j < laenge; j++)
			System.out.print(daten[j] + " ");
		System.out.println("");
	}

	public void quickSort() {
		rekQuickSort(0, laenge - 1);
	}

	public void rekQuickSort(int links, int rechts) {
		int groesse = rechts - links + 1;
		if (groesse <= 3) 						// "manuelles" sortieren wenn klein
			manuellSortieren(links, rechts);
		else 									// quicksort wenn viele daten
		{
			long median = medianOf3(links, rechts);
			int bereich = bereichBilden(links, rechts, median);
			rekQuickSort(links, bereich - 1);
			rekQuickSort(bereich + 1, rechts);
		}
	}

	public long medianOf3(int links, int rechts) {
		int mitte = (links + rechts) / 2;
											
		if (daten[links] > daten[mitte])		// sortieren links und mitte
			tauschen(links, mitte);
		if (daten[links] > daten[rechts])		// sortieren links und rechts
			tauschen(links, rechts);
		if (daten[mitte] > daten[rechts])		// sortieren mitte und rechts
			tauschen(mitte, rechts);
		tauschen(mitte, rechts - 1); 			// pivot nach rechts
		return daten[rechts - 1]; 				// mittelwert
	}

	public void tauschen(int d1, int d2) {
		long temp = daten[d1];
		daten[d1] = daten[d2];
		daten[d2] = temp;
	}

	public int bereichBilden(int links, int rechts, long pivot) {
		int linksPtr = links; 					// rechts vom ersten element ******of first elem
		int rechtsPtr = rechts - 1; 			// links vom pivot 	*****of pivot

		while (true) {
												// groesseres finden
			while (daten[++linksPtr] < pivot) ;
												// kleineres finden 
			while (daten[--rechtsPtr] > pivot);
			if (linksPtr >= rechtsPtr) 			// zeiger kreuzen, bereich sortiert
				break;
			else
				tauschen(linksPtr, rechtsPtr); 	// tauschen elemente
		}
		tauschen(linksPtr, rechts - 1); 		// pivot erneuern
		return linksPtr; 						// pivot position
	}

	public void manuellSortieren(int links, int rechts) {
		int groesse = rechts - links + 1;
		if (groesse <= 1)
			return; 							// sortieren nicht erforderlich
		if (groesse == 2) { 					// 2er schritte sortieren links and rechts
			if (daten[links] > daten[rechts])
				tauschen(links, rechts);
			return;
		} else 									// groesse = 3
		{ 										// 3er schritte sortieren links, mitte, & rechts
			if (daten[links] > daten[rechts - 1])
				tauschen(links, rechts - 1); 	// links, mitte
			if (daten[links] > daten[rechts])
				tauschen(links, rechts); 		// links, rechts
			if (daten[rechts - 1] > daten[rechts])
				tauschen(rechts - 1, rechts); 	// mitte, rechts
		}
	}

	public static void main(String[] args) {
		int maxGroesse = 16;
		a_05_QuickSort_v2 arr = new a_05_QuickSort_v2(maxGroesse);

		for (int j = 0; j < maxGroesse; j++) { 				// zufallszahlen
			long n = (int) (java.lang.Math.random() * 99);
			arr.einfuegen(n);
		}
		
		System.out.print("Quicksort 2\n");
		System.out.print("----------------\n");

		System.out.print("Daten unsortiert:\n");
		arr.anzeigen();
		arr.quickSort();
		System.out.print("Daten sortiert:\n");
		arr.anzeigen();
	}
}