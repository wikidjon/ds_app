package einfuehrung;
// Backtracking
public class Damen {
	private final static int n = 8;
	private int[] dameInDerSpalte = new int[n]; 					// Werte 0 bis 7
	private boolean[] 	reihe = new boolean[n],
						diagonaleLinks = new boolean[2 * n - 1],
						diagonaleRechts = new boolean[2 * n - 1]; 	// alles mit true vorbesetzt

	private boolean versuchen(int x) { 			// versuch, Dame in Spalte x zu platzieren
		if (x == n)
			return true; 						// Erfolg
		else
			for (int y = 0; y < n; y++) { 					// probiert alle Reihen aus
				final int links = (x + y) % (2 * n - 1); 	// Index der Diagonale links
				final int rechts = (x - y - 1 + 2 * n) % (2 * n - 1); // Diagonale rechts

				if (reihe[y] && diagonaleLinks[links]&& diagonaleRechts[rechts]) {
					dameInDerSpalte[x] = y;
					reihe[y] = diagonaleLinks[links] = diagonaleRechts[rechts] = false;
											// Reihe und Diagonalen sind jetzt besetzt
					final boolean erfolg = versuchen(x + 1);
					if (erfolg)
						return true;
					else {
						reihe[y] = diagonaleLinks[links] = diagonaleRechts[rechts] = true;
					}
					// Reihe und Diagonale zur�ckgesetzt
				}
			}
		return false;
	}

	public Damen() {
		// dameInDerSpalte = new int[n]; // Werte 0 bis 7
		// reihe = new boolean[n];
		for (int i = 0; i < reihe.length; i++)
			reihe[i] = true;
		diagonaleLinks = new boolean[2 * n - 1];
		diagonaleRechts = new boolean[2 * n - 1];
		for (int i = 0; i < diagonaleLinks.length; i++)
			diagonaleLinks[i] = diagonaleRechts[i] = true;
	}

	public static void main(String[] args) {
		String a = "", b = "";
		System.out.println("Acht Damen");
		Damen d = new Damen();

		boolean ergebnis = d.versuchen(0);
		if (ergebnis)

			for (int i = 0; i < n; i++) {
				switch (d.dameInDerSpalte[i]) {
				case 0:
					a = "|";
					b = "| | | | | | | |";
					break;
				case 1:
					a = "| |";
					b = "| | | | | | |";
					break;
				case 2:
					a = "| | |";
					b = "| | | | | |";
					break;
				case 3:
					a = "| | | |";
					b = "| | | | |";
					break;
				case 4:
					a = "| | | | |";
					b = "| | | |";
					break;
				case 5:
					a = "| | | | | |";
					b = "| | |";
					break;
				case 6:
					a = "| | | | | | |";
					b = "| |";
					break;
				case 7:
					a = "| | | | | | | |";
					b = "|";
					break;
				}

				System.out.print(a + d.dameInDerSpalte[i] + b);
				System.out.println("");
			}
		else
			System.out.println("Keine Loesung");
	}
}
