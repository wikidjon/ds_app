package einfuehrung;
public class a_01_BubbleSort {
	private long[] a;

	private int anzElemente;

	public a_01_BubbleSort(int max) {
		a = new long[max];
		anzElemente = 0;
	}

	// fuellen des Arrays
	public void insert(long wert) {
		a[anzElemente] = wert;
		anzElemente++;
	}

	// anzeigen der array inhalte
	public void display() {
		for (int j = 0; j < anzElemente; j++)
			System.out.print(a[j] + " ");
		System.out.println("");
	}

	public void bubbleSort() {
		int out, in;

		for (out = anzElemente - 1; out > 1; out--)
			// aeussere schleife
			for (in = 0; in < out; in++)
				// innere schleife
				if (a[in] > a[in + 1])
					tausch(in, in + 1); // tausch
	}

	private void tausch(int li, int re) {
		long temp = a[li];
		a[li] = a[re];
		a[re] = temp;
	}

	public static void main(String[] args) {
		int maxSize = 100;
		a_01_BubbleSort arr;
		arr = new a_01_BubbleSort(maxSize);

		arr.insert(77);
		arr.insert(66);
		arr.insert(44);
		arr.insert(34);
		arr.insert(22);
		arr.insert(88);
		arr.insert(12);
		arr.insert(00);
		arr.insert(55);
		arr.insert(33);

		System.out.print("Daten unsortiert: \n");
		arr.display();

		arr.bubbleSort();
		System.out.print("Daten sortiert: \n");
		arr.display();
	}
}