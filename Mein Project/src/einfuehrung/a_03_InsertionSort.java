package einfuehrung;
/**********************************************************************
 *  betrachte elemente nacheinander und fuege jedes an seinen richt�gen
 *  Platz zwischen den bereits betrachteten ein
 **********************************************************************/

public class a_03_InsertionSort {

  private long[] zahlen;

  private int anzElemente;

  public a_03_InsertionSort(int max) {
    zahlen = new long[max];
    anzElemente = 0;
  }

  public void einfuegen(long wert) {
    zahlen[anzElemente] = wert;
    anzElemente++;
  }

  public void anzeige() {
    for (int j = 0; j < anzElemente; j++)
      System.out.print(zahlen[j] + " ");
    System.out.println("");
  }

  public void insertionSort() {
    int innen, aussen;

    for (aussen = 1; aussen < anzElemente; aussen++) {
      long temp = zahlen[aussen]; 						// merker
      innen = aussen; 									// start aussen
      while (innen > 0 && zahlen[innen - 1] >= temp) 	// vergleichen
      {
        zahlen[innen] = zahlen[innen - 1]; 				// rechts schieben
        System.out.print("nach rechts " + zahlen[innen] +"  \n");
        --innen; 										// nach links gehen
      }
      zahlen[innen] = temp; 							// einfuegen merker

//*********Kontrollausschrift Array Zwischenstufen************************
      System.out.println("***********************");
      for (int j = 0; j < anzElemente; j++)
          System.out.print(zahlen[j] + "  ");
        System.out.println("");
//*********Ende Kontrollausschrift***************************************/
    }
  }

  public static void main(String[] args) {
    int maxSize = 100; 									// array groesse
    a_03_InsertionSort arr; 			
    arr = new a_03_InsertionSort(maxSize); 				// anlegen array

    arr.einfuegen(47);
    arr.einfuegen(99);
    arr.einfuegen(44);
    arr.einfuegen(35);
    arr.einfuegen(22);
    arr.einfuegen(88);
    arr.einfuegen(41);
    arr.einfuegen(00);
    arr.einfuegen(16);
    arr.einfuegen(33);
    
    System.out.print("Insertionsort\n"); 
    System.out.print("-----------------\n");
    System.out.print("Daten unsortiert:\n");
    arr.anzeige();

    arr.insertionSort();
    
    System.out.print("Daten sortiert:\n");
    arr.anzeige();
  }
}









/*******************************************************************************
public class InsertionSort extends SortAnimation {

public void sort() throws InterruptedException {
for (int i = 1; i < length; i++) {
  //Insert a[i] into sorted sequence a[0..i-1].
  red(i);
  int x = a[i];
  int j = i - 1;
  while (0 <= j && x < a[j]) {
blue(j);
a[j + 1] = a[j];
    redraw(j + 1);
step();
j--;
  }
  a[j + 1] = x;
  redraw(j + 1);
}
}
}

**************************************************************************
*************************************************************************
public class InsertionSort extends SortAnimation {

public void sort() throws InterruptedException {
for (int i = 1; i < length; i++) {
  //Insert a[i] into sorted sequence a[0..i-1].
  red(i);
  int x = a[i];
  int j = i - 1;
  while (0 <= j && x < a[j]) {
blue(j);
a[j + 1] = a[j];
    redraw(j + 1);
step();
j--;
  }
  a[j + 1] = x;
  redraw(j + 1);
}
}
}



********************************************************************************/