package einfuehrung;
@SuppressWarnings("unchecked")

public class a_08_HeapSort2 {

	private static void percolate(Comparable[] f, int index, int ende) {
		int i = index + 1, j;
		while (2 * i <= ende) { 						// f[i] hat linken Sohn
			j = 2 * i; 									// f[j] ist linker Sohn von f[i]
			if (j < ende) 								// f[i] hat auch rechten Sohn
				if (f[j - 1].compareTo(f[j]) > 0)
					j++; 								// f[j] ist jetzt kleiner
			if (f[i - 1].compareTo(f[j - 1]) > 0) {
				tauschen(f, i - 1, j - 1);
				i = j; 									// versickere weiter
			} else
				break; 									// heap-Bedingung erf�llt
		}
	}

	private static void tauschen(Comparable[] f, int i1, int i2) {
		Comparable<?> tmp = f[i1];
		f[i1] = f[i2];
		f[i2] = tmp;
	}

	public static void heapSort(Comparable[] f) {
		int i;
		for (i = f.length / 2; i >= 0; i--)
			percolate(f, i, f.length);

		for (i = f.length - 1; i > 0; i--) {
			tauschen(f, 0, i); 							// tauscht jeweils letztes Element des Heaps mit dem ersten
			percolate(f, 0, i); 						// heap wird von Position 0 bis i hergestellt
		}
	}

	static Comparable[] initArray(int num) {			// Initialisierung eines Feldes mit Zufallszahlen
		Integer[] result = new Integer[num];
		for (int i = 0; i < num; i++)
			result[i] = new Integer((int) (Math.random() * 100.0));
		return result;
	}

	static void ausgabeArray(Comparable[] array) {	 	// Ausgabe der Elemente eines Feldes
		for (int i = 0; i < array.length; i++)
			System.out.print(array[i] + " ");
		System.out.println();
	}

	public static void main(String[] args) {
		Comparable[] array = initArray(20);
		System.out.print("Heap:\n");
		ausgabeArray(array);
		heapSort(array);
		System.out.print("\nabsteigend sortiert:\n");
		ausgabeArray(array);
	}
}
