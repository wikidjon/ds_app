package einfuehrung.AlgoTools;

import java.awt.Panel;
import java.awt.TextArea;

public class OutputDevice extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8480202224787009926L;
	private TextArea out;

	public OutputDevice() {

// 1.1		out = new TextArea("", IOAppletServer.TEXT_HEIGHT, IOAppletServer.TEXT_WIDTH, TextArea.SCROLLBARS_VERTICAL_ONLY);
		out = new TextArea("", IOAppletServer.TEXT_HEIGHT, IOAppletServer.TEXT_WIDTH);

		out.setFont(IOAppletServer.TEXT_FONT);
		out.setEditable(false);

		add(out);
	}
		
	public void append(String msg) {

		out.append(msg);
	}

	public void clear() {

		out.setText("");

		for (int i=0; i<IOAppletServer.TEXT_HEIGHT-1; i++) {

			out.append(IOS.NL);
		}
	}
}
