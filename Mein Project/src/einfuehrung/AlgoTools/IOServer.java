
package einfuehrung.AlgoTools;

/* Copyright 1997/98: Arbeitsgruppe Praktische Informatik, Universitaet Osnabrueck 
   Frank Lohmeyer, Frank M. Thiesing, Oliver Vornberger: oliver@informatik.uni-osnabrueck.de
 */

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
// 1.0 import java.io.DataInputReader;

public class IOServer extends IOS {

	private BufferedReader in;
// 1.0	private DataInputStream in;

	public IOServer() {

		in = new BufferedReader(new InputStreamReader(System.in));
// 1.0		in = new DataInputStream(System.in);
	}

	public String getLine(String prompt) {

		if (prompt == null) {

			prompt = "";
		}

		System.out.print(prompt);
		System.out.flush();

		String answer = "";

		try {
			answer = in.readLine();

		} catch (IOException ex) {

			System.out.println("Error (IOException): " + ex);
			System.out.flush();
		}

		return(answer);
	}

	public void append(String msg) {

		System.out.print(msg);
		System.out.flush();
	}

	public void clear() {

		System.out.print("\f");
		System.out.flush();
	}

	public void quit(String msg, int code) {

		System.out.println("Fatal Error: " + msg);
		System.out.println("Terminating program!");
		System.out.flush();

		System.exit(code);
	}
}
