package einfuehrung.AlgoTools;

import java.applet.Applet;
import java.awt.Event;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Button;
import java.awt.BorderLayout;

public class ProgApplet extends Applet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Button start;
	protected Button stop;
	protected Button clear;
	protected Label  info;

	protected InputDevice  in;
	protected OutputDevice out;

	protected String className;
	protected Thread prog;

	public void init() {

		Panel a = new Panel();
		Panel b = new Panel();
		Panel c = new Panel();
		Panel d = new Panel();

		a.setLayout(new BorderLayout());
		b.setLayout(new BorderLayout());
		c.setLayout(new BorderLayout());
		d.setLayout(new BorderLayout());

		a.add("West",   stop  = new Button("Stop"));
		a.add("East",   clear = new Button("Clear Screen"));

		b.add("West",   start = new Button("Start"));
		b.add("Center", info  = new Label ("", Label.CENTER));
		b.add("East",   a);

		c.add("North", out = new OutputDevice());
		c.add("South", in  = new InputDevice());

		d.add("North",  b);
		d.add("South",  c);

		add(d);
                
                IO.initAppletExecution();
	}

	public void start() {

		in.reset();
		out.clear();

		resetControls();
	}

	@SuppressWarnings("deprecation")
	public void stop() {

		prog.stop();

		in.close();
	}

	public boolean action(Event e, Object o) {

		if (e.target == start) {

			start.setEnabled(false);
			clear.setEnabled(false);

			IOAppletServer.connect(in, out);

			prog = initProg();

			prog.start();

			stop.setEnabled(true);

			info.setText("Stop program " + className + " -->");

		} else if (e.target == stop) {

			stop();

			resetControls();

		} else if (e.target == clear) {

			out.clear();
		}

		return(false);
	}

	public void resetControls() {

		stop.setEnabled(false);

		start.setEnabled(true);
		clear.setEnabled(true);

		info.setText("<-- Start program " + className);
	}

	protected Thread initProg() {

		return(null);
	}
}
