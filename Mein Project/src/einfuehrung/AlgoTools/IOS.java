
package einfuehrung.AlgoTools;

/* Copyright 1997/98: Arbeitsgruppe Praktische Informatik, Universitaet Osnabrueck 
   Frank Lohmeyer, Frank M. Thiesing, Oliver Vornberger: oliver@informatik.uni-osnabrueck.de
 */

public abstract class IOS {

	public static String NL = System.getProperty("line.separator", "\n");

	public abstract String getLine(String prompt);

	public abstract void append(String msg);

	public abstract void clear();

	public abstract void quit(String msg, int code);
}
