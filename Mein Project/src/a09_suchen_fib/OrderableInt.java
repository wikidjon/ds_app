package a09_suchen_fib;

/*******************************************************************************
 * Ein Objekt mit einem int-Key, der die Grundlage der Vergleiche darstellt
 ******************************************************************************/
class OrderableInt implements Orderable {

	protected int i; // key - hier koennte noch ein Datentyp kommen

	OrderableInt(int i) {
		this.i = i;
	} // Konstruktor

	// Implementierung der abstakten Vergleiche aus der Oberklasse
	public boolean equal(Orderable o) {
		return i == ((OrderableInt) o).i;
	}

	public boolean greater(Orderable o) {
		return i > ((OrderableInt) o).i;
	}

	public boolean less(Orderable o) {
		return o.greater(this);
	}

	public boolean greaterEqual(Orderable o) {
		return !less(o);
	}

	public boolean lessEqual(Orderable o) {
		return !greater(o);
	}

	public Orderable minKey() {
		return new OrderableInt(Integer.MIN_VALUE);
	}

	int intValue() {
		return i;
	} // Gibt Key zurueck

	public String toString() {
		return new Integer(i).toString();
	}

	/***************************************************************************
	 * statische Methode zur Erzeugung eines Arrays "vergleichbarer"
	 * OrderableInt-Objekte aus einem int-Array. Komponente 0 enthaelt Element
	 * mit key MIN_VALUE
	 **************************************************************************/

	static OrderableInt[] array(int A[]) {
		OrderableInt orderableIntArray[] = new OrderableInt[A.length + 1];
		orderableIntArray[0] = new OrderableInt(Integer.MIN_VALUE);
		for (int i = 0; i < A.length; i++) {
			orderableIntArray[i + 1] = new OrderableInt(A[i]);
		}
		return orderableIntArray;
	}

}
