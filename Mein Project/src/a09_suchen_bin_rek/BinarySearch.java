package a09_suchen_bin_rek;

/**
 * Binaere Suche mit Rekursion
 */
public class BinarySearch extends SearchAlgorithm {

	public static int search(Orderable A[], Orderable k) {
		/*
		 * Durchsucht A[1], .., A[n] nach Element k und liefert den groessten
		 * Index i >= 1 mit A[i] <= k; 0 sonst
		 */
		int n = A.length;
		return search(A, 1, n, k);
	}

	public static int search(Orderable A[], int l, int r, Orderable k) {
		/*
		 * Durchsucht A[1], .., A[n] nach Element k und liefert den groessten
		 * Index l <= i <= r mit A[i] <= k; l-1 sonst
		 */
		if (l > r) /* Suche erfolglos */
			return l - 1;

		int m = (l + r) / 2;
		if (k.less(A[m]))
			return search(A, l, m - 1, k);

		if (k.greater(A[m]))
			return search(A, m + 1, r, k);

		else
			/* A[m] = k */
			return m;

	}
}
