package a01_assoziative_arr;

public class AssoziativeArray {

	//Ein assoziativer Speicher verbindet einen Schl�ssel mit einem Wert. 
	//Java bietet f�r Datenstrukturen dieser Art die allgemeine Schnittstelle Map mit wichtigen Operationen wie put(key, value) zum Aufbau 
	//einer Assoziation und get(key) zum Erfragen eines assoziierten Wertes.
	
	private Object[][] paare;

	private int index;

	public AssoziativeArray(int laenge) {
		paare = new Object[laenge][2];			// Achtung !!! Paare
	}

	public void put(Object key, Object value) {
		if (index >= paare.length)
			throw new ArrayIndexOutOfBoundsException();
		paare[index++] = new Object[] { key, value };
	}

	public Object get(Object key) {
		for (int i = 0; i < index; i++)
			if (key.equals(paare[i][0]))
				return paare[i][1];
		throw new RuntimeException("Schluessel nicht gefunden!!");
	}

	public String toString() {
		String result = "";
		for (int i = 0; i < index; i++) {
			result += paare[i][0] + " :- " + paare[i][1];
			if (i < index - 1)
				result += "\n";
		}
		return result;
	}

	public static void main(String[] args) {
		AssoziativeArray assarr = new AssoziativeArray(6);
		assarr.put("Himmel", "blau");
		assarr.put("Grass", "gruen");
		assarr.put("Musik", "tanzen");
		assarr.put("Baum", "hoch");
		assarr.put("Erde", "rund");
		assarr.put("Sonne", "warm");
		try {
			assarr.put("Student", "klug"); 			// ende erreicht - 6 Objekte
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Zu viele Objekte!");
		}
		System.out.println(assarr);
		System.out.println(assarr.get("Sonne"));

	}
} 
