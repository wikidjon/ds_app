package a12_comparator;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Company {
  public static void main(String args[]) {
    Employee4 emps[] = { new Employee4("Finance", "Degree, Debbie"),
        new Employee4("Finance", "Grade, Geri"),
        new Employee4("Finance", "Extent, Ester"),
        new Employee4("Engineering", "Measure, Mary"),
        new Employee4("Engineering", "Amount, Anastasia"),
        new Employee4("Engineering", "Ratio, Ringo"),
        new Employee4("Sales", "Stint, Sarah"),
        new Employee4("Sales", "Pitch, Paula"),
        new Employee4("Support", "Rate, Rhoda"), };
    Set<Employee4> set = new TreeSet<Employee4>(Arrays.asList(emps));
    System.out.println(set);
    Set<Employee4> set2 = new TreeSet<Employee4>(Collections.reverseOrder());
    set2.addAll(Arrays.asList(emps));
    System.out.println(set2);

    Set<Employee4> set3 = new TreeSet<Employee4>(new EmpComparator3());
    for (int i = 0, n = emps.length; i < n; i++) {
      set3.add(emps[i]);
    }
    System.out.println(set3);
  }
}

class EmpComparator3 implements Comparator<Object> {

  public int compare(Object obj1, Object obj2) {
    Employee4 emp1 = (Employee4) obj1;
    Employee4 emp2 = (Employee4) obj2;

    int nameComp = emp1.getName().compareTo(emp2.getName());

    return ((nameComp == 0) ? emp1.getDepartment().compareTo(
        emp2.getDepartment()) : nameComp);
  }
}

class Employee4 implements Comparable<Object> {
  String department, name;

  public Employee4(String department, String name) {
    this.department = department;
    this.name = name;
  }

  public String getDepartment() {
    return department;
  }

  public String getName() {
    return name;
  }

  public String toString() {
    return "[dept=" + department + ",name=" + name + "]";
  }

  public int compareTo(Object obj) {
    Employee4 emp = (Employee4) obj;
    int deptComp = department.compareTo(emp.getDepartment());

    return ((deptComp == 0) ? name.compareTo(emp.getName()) : deptComp);
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof Employee4)) {
      return false;
    }
    Employee4 emp = (Employee4) obj;
    return department.equals(emp.getDepartment())
        && name.equals(emp.getName());
  }

  public int hashCode() {
    return 31 * department.hashCode() + name.hashCode();
  }
}
