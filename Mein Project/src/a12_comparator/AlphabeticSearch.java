package a12_comparator;

//Searching with a Comparator.

import java.util.Arrays;
import java.util.Comparator;

public class AlphabeticSearch {

  public static void main(String[] args) { 
    String[] sa = new String[] { "a", "c", "d", "e", "f", "g","h", "i", "j", "k", "l", "m" };
    AlphabeticComparator comp = new AlphabeticComparator();
    Arrays.sort(sa, comp);
    int index = Arrays.binarySearch(sa, sa[10], comp);
    System.out.println("Index = " + index);
  }
} 

class AlphabeticComparator implements Comparator<Object> {
  public int compare(Object o1, Object o2) {
    String s1 = (String) o1;
    String s2 = (String) o2;
    return s1.toLowerCase().compareTo(s2.toLowerCase());
  }
} 
