package a03_map;

import java.util.*;

@SuppressWarnings("unchecked")
public class Map_beisp implements Map<Object, Object> {

	private ArrayList<Object> keys;
	private ArrayList<Object> values;

	public Map_beisp() {
		keys = new ArrayList<Object>();
		values = new ArrayList<Object>();
	}

	public int size() {							 	// Zahl der Eintraege
		return keys.size();
	}

	public boolean isEmpty() { 						// Map leer
		return size() == 0;
	}

	public boolean containsKey(Object o) { 			// key o ist enthalten
		return keys.contains(o);
	}

	public boolean containsValue(Object o) { 		// value o ist enthalten
		return keys.contains(o);
	}

	public Object get(Object k) { 					// value zum key zurueck
		int i = keys.indexOf(k);
		if (i == -1)
			return null;
		return values.get(i);
	}

	public Object put(Object k, Object v) { 		// k v sortiert in die map
		for (int i = 0; i < keys.size(); i++) {
			Object old = keys.get(i);
					// key schon enthalten?
			if (((Comparable<Object>) k).compareTo(keys.get(i)) == 0) {
				keys.set(i, v);
				return old;
			}
					// keys sortiert
			if (((Comparable<Object>) k).compareTo(keys.get(i)) == +1) {
				int where = i > 0 ? i - 1 : 0;
				keys.add(where, k);
				values.add(where, v);
				return null;
			}
		}
		// else zum ende
		keys.add(k);
		values.add(v);
		return null;
	}

	public void putAll(Map<?, ?> oldMap) { 					// alle paare der oldMap in neue
		Iterator<?> keysIter = oldMap.keySet().iterator();
		while (keysIter.hasNext()) {
			Object k = keysIter.next();
			Object v = oldMap.get(k);
			put(k, v);
		}
	}

	public Object remove(Object k) {
		int i = keys.indexOf(k);
		if (i == -1)
			return null;
		Object old = values.get(i);
		keys.remove(i);
		values.remove(i);
		return old;
	}

	public void clear() {
		keys.clear();
		values.clear();
	}

	public Set<Object> keySet() { 						// Methode des IF map
		return new TreeSet<Object>(keys);
	}

	public Collection<Object> values() {
		return values;
	}

	// ******************************************************************
	// *********Eintraege im Set
	public class MapEintraege implements Map.Entry, Comparable<Object> {
		private Object key, value;

		MapEintraege(Object k, Object v) {
			key = k;
			value = v;
		}

		public Object getKey() {
			return key;
		}

		public Object getValue() {
			return value;
		}

		public Object setValue(Object nv) {
			throw new UnsupportedOperationException("setValue");
		}

		public int compareTo(Object o2) {
			if (!(o2 instanceof MapEintraege))
				throw new IllegalArgumentException("Hupps !! Kein MapEintrag ?");
			Object otherKey = ((MapEintraege) o2).getKey();
			return ((Comparable<Comparable<?>>) key)
					.compareTo((Comparable<?>) otherKey);
		}
	}

	// ******************************************************************
	// Menge der Map.Entry Objekte von entrySet()
	private class MapEintrag extends AbstractSet<Object> {
		List<MapEintraege> list;

		MapEintrag(ArrayList<MapEintraege> al) {
			list = al;
		}

		public Iterator iterator() {
			return list.iterator();
		}

		public int size() {
			return list.size();
		}
	}

	public Set entrySet() {
		if (keys.size() != values.size())
			throw new IllegalStateException(
					"InternalError: keys and values out of sync");
		ArrayList<MapEintraege> al = new ArrayList<MapEintraege>();
		for (int i = 0; i < keys.size(); i++) {
			al.add(new MapEintraege(keys.get(i), values.get(i)));
		}
		return new MapEintrag(al);
	}

	public static void main(String[] argv) {

		// konstruiert und laedt die map
		Map<Object, Object> map = new Map_beisp();

		map.put("Pink Floyd   ", "Umma Gumma");
		map.put("Eric Clapton", "My Fathers Eyes");
		map.put("Otis Reading", "Doc of a Bay");
		map.put("Rolling Stones", "As Tears go by");
		map.put("Al Jarrau     ", "Hallo");
		map.put("Jimi Hendix", "Hey Joe");
		map.put("Iron Butterfly", "In a Gadda Da Vida");

		// 1. version: ein wert zum key
		String queryString = "Eric Clapton";
		System.out.println("Suche nach :" + queryString + ".");
		String resultString = (String) map.get(queryString);
		System.out.println("Gefunden : " + resultString);
		System.out.println();

		// 2: alle key - value - paare
		Iterator<Object> k = map.keySet().iterator();
		// Iterator<String> k = map.keySet().iterator();
		while (k.hasNext()) {
			String key = (String) k.next();
			System.out.println("Key: " + key + "; \tValue: "
					+ (String) map.get(key));
		}

		// 3: anwendung der entrySet() methode
		Set<?> es = map.entrySet();
		System.out
				.println("\nentrySet() liefert " + es.size() + " Map.Entry's");
	}

}