package a00_listen2;

class StackTest2 {
    public static void main(String args[]) {
        StackMitListe s = new StackMitListe();
//        String str = TastaturEingabe.readString("Bitte Zeichenkette eingeben: ");
        String bier[]={"Bierkasten1","Bierkasten2","Bierkasten3","Bierkasten4"};
        int len = bier.length; 
        
        for(int i = 0; i < len; i++){
            //s.push(new Character(str.charAt(i)));
        	s.push(bier[i]);
        }
        System.out.println();
        System.out.println("... Aufruf der durchlaufe()-Methode der Liste....");
        s.durchlaufe();
        System.out.println("---------------------------\n");
 
        System.out.println("... Aufruf der pop()-Methode des Stacks....");
         while(!s.isEmpty()) {
            Object o = s.pop();			//auskommentieren !!!
            System.out.println(s.pop());
        }
         System.out.println();
         System.out.println("---------------------------\n");
 
    }
}
