package a00_listen2;

public class SchlangeMitListe
{
   private Liste l = new Liste();

   boolean istLeer()
   {
      return l.istLeer();
   }

   void durchlaufe()
   {
      l.durchlaufe();
   }

   void anfuege(Object neuesObject)
   {
      l.einfuegeEnde(neuesObject);
   }

   Object abarbeite()
   {
      Object o = l.getKopfDaten();
      l.loescheKopf();
      return o;
   }
}