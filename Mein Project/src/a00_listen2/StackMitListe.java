package a00_listen2;

public class StackMitListe {
	private Liste l = new Liste();

	boolean isEmpty() {
		return l.istLeer();
	}

	void durchlaufe() {
		l.durchlaufe();
	}

	void push(Object newObject) {
		l.einfuegeKopf(newObject);
	}

	Object pop() {
		Object o = l.getKopfDaten();
		l.loescheKopf();
		return o;
	}
}