package collections_2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

class UtilDemo {
  public static void main(String[] args) {
    String[] names = { "A", "B", "C", "D" };

    double[] salaries = { 2.0, 5.0, 6.0, 4.0 };

    List<Employee> l = new ArrayList<Employee>();

    for (int i = 0; i < names.length; i++)
      l.add(new Employee(names[i], salaries[i]));

    Collections.sort(l);

    ListIterator<Employee> liter = l.listIterator();

    while (liter.hasNext())
      System.out.println(liter.next());

    Collections.sort(l, new Employee.SalaryComparator());

    liter = l.listIterator();

    while (liter.hasNext())
      System.out.println(liter.next());
  }
}