package a16_arrays;


import java.io.Serializable;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

@SuppressWarnings("unchecked")
public class ArraySet extends AbstractSet<Object> implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<Object> list;

  public ArraySet() {
    list = new ArrayList<Object>();
  }

  public ArraySet(Collection<String> col) {
    list = new ArrayList<Object>();
  

    // No need to check for dups if col is a set
    Iterator<String> itor = col.iterator();
    if (col instanceof Set) {
      while (itor.hasNext()) {
        list.add(itor.next());
      }
    } else {
      while (itor.hasNext()) {
        add(itor.next());
      }
    }
  }

  public Iterator<Object> iterator() {
    return list.iterator();
  }

  public int size() {
    return list.size();
  }

  public boolean add(Object element) {
    boolean modified;
    if (modified = !list.contains(element)) {
      list.add(element);
    }
    return modified;
  }

  public boolean remove(Object element) {
    return list.remove(element);
  }

  public boolean isEmpty() {
    return list.isEmpty();
  }

  public boolean contains(Object element) {
    return list.contains(element);
  }

  public void clear() {
    list.clear();
  }

  public Object clone() {
    try {
      ArraySet newSet = (ArraySet) super.clone();
      newSet.list = (ArrayList<Object>) list.clone();
      return newSet;
    } catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }

  public static void main(String args[]) {
    String elements[] = { "Java", "Source", "and",
        "Support", "." };
    Set<?> set = new ArraySet(Arrays.asList(elements));
    Iterator<?> iter = set.iterator();
    while (iter.hasNext()) {
      System.out.println(iter.next());
    }
  }
}