package a16_arrays;


import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

/** Convert an Array to a Vector. */
public class ArrayToVector {
  public static void main(String[] args) {
    Object[] a1d = { "Hello World", new Date(), Calendar.getInstance(), };
    // Arrays.asList(Object[]) --> List
    List<Object> l = Arrays.asList(a1d);

    // Vector contstructor takes Collection
    // List is a subclass of Collection
    Vector<Object> v;
    v = new Vector<Object>(l);

    // Or, more simply:
    v = new Vector<Object>(Arrays.asList(a1d));

    // Just to prove that it worked.
    Enumeration<Object> e = v.elements();
    while (e.hasMoreElements()) {
      System.out.println(e.nextElement());
    }
  }
}