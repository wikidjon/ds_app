package vector;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.SequenceInputStream;
import java.util.Enumeration;
import java.util.Vector;

public class Sequence {
  public static void main(String args[]) throws IOException {
    Vector<FileInputStream> v = new Vector<FileInputStream>(3);
    v.add(new FileInputStream("C:/Ausgang/"));
    v.add(new FileInputStream("test.txt"));
    v.add(new FileInputStream("/temp/test.txt"));
    Enumeration<FileInputStream> e = v.elements();
    SequenceInputStream sis = new SequenceInputStream(e);
    InputStreamReader isr = new InputStreamReader(sis);
    BufferedReader br = new BufferedReader(isr);
    String line;
    while ((line = br.readLine()) != null) {
      System.out.println(line);
    }
    br.close();
  }
}