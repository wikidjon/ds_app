package vector;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

//Save Vector to file
public class SaveVector1 {
  public static void main(String args[]) throws Exception {
    Vector<String> v = new Vector<String>(Arrays.asList(args));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(baos);
    oos.writeObject(v);
    oos.close();
    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
    ObjectInputStream ois = new ObjectInputStream(bais);
    Vector<?> v2 = (Vector<?>) ois.readObject();
    Enumeration<String> e = v.elements();
    Enumeration<?> e2 = v2.elements();
    while (e.hasMoreElements()) {
      System.out.println(e.nextElement());
    }
    while (e2.hasMoreElements()) {
        System.out.println(e2.nextElement());
      }
  }
}