package vector;
import java.util.Vector;

public class FindVector {

  public static void main(String args[]) {
    String data[] = { "Java", "Vorlesung", "und", "Uebung", "."};
    
    Vector<String> v = new Vector<String>();
    for (int i = 0, n = data.length; i < n; i++) {
      v.add(data[i]);
    }
    System.out.println("Vector: " + v);
    System.out.println("****************************************");
    System.out.println("AlgoDat enthalten?  -> " + v.contains("AlgoDat"));
    System.out.println("Java enthalten?  -> " + v.contains("Java"));
    System.out.println("Wo steht und?  -> " + v.indexOf("und"));
    System.out.println("Wo steht Vorlesung?  -> " + v.indexOf("Vorlesung"));
    System.out.println("Wo steht Java vom Ende?  -> " + v.lastIndexOf("Java"));
  }
}