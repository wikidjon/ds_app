package a007_sort.einfuehrung;

/*****************************************************************
 * Sortieren vordere Haelfte Sortieren hintere Haelfte mischen
 ******************************************************************/

public class a_07_MergeSort {

	private long[] zahlenArray;

	private int anzElemente;

	public a_07_MergeSort(int max) {
		zahlenArray = new long[max];
		anzElemente = 0;
	}

	public void einfuegen(long wert) {
		zahlenArray[anzElemente] = wert; // werte einfuegen
		anzElemente++; // elementzaehler
	}

	public void anzeigen() {
		for (int j = 0; j < anzElemente; j++)
			System.out.print(zahlenArray[j] + " ");
		System.out.println("");
	}

	public void mergeSort() {
		long[] daten = new long[anzElemente];
		rekMergeSort(daten, 0, anzElemente - 1);
	}

	private void rekMergeSort(long[] daten, int untereGrenze, int obereGrenze) {

		if (untereGrenze == obereGrenze) 									// wenn Bereich = 1,
			return; 																				// keine Sortierung
		else {
			int mitte = (untereGrenze + obereGrenze) / 2; 			// finden mittelpunkt
			System.out.println("untere Grenze: " + untereGrenze
					+ " - obere Grenze: " + obereGrenze + " - Mitte:" + mitte); // !!!!
			rekMergeSort(daten, untereGrenze, mitte); 					// sortieren untere
																										// haelfte

			rekMergeSort(daten, mitte + 1, obereGrenze); 				// sortieren obere
																										// haelfte

			System.out.println(" untere Grenze: " + untereGrenze
					+ " - obere Grenze: " + obereGrenze
					+ " -> jetzt mergen ..."); // !!!!
			merge(daten, untereGrenze, mitte + 1, obereGrenze); // verschmelzen
		}
	}

	private void merge(long[] daten, int untPtr, int obPtr, int obereGrenze) {
		int j = 0;
		int untereGrenze = untPtr;
		int mitte = obPtr - 1;
		int n = obereGrenze - untereGrenze + 1; // anzahl werte

		System.out.println(" --> Mergen"); // !!!!
		while (untPtr <= mitte && obPtr <= obereGrenze)
			if (zahlenArray[untPtr] < zahlenArray[obPtr])
				daten[j++] = zahlenArray[untPtr++];
			else
				daten[j++] = zahlenArray[obPtr++];

		while (untPtr <= mitte)
			daten[j++] = zahlenArray[untPtr++];

		while (obPtr <= obereGrenze)
			daten[j++] = zahlenArray[obPtr++];

		for (j = 0; j < n; j++)
			zahlenArray[untereGrenze + j] = daten[j];
	}

	public static void main(String[] args) {
		int groesse = 100; // arraygroesse
		a_07_MergeSort arr = new a_07_MergeSort(groesse); // array anlegen
		arr.einfuegen(77);
		arr.einfuegen(29);
		arr.einfuegen(34);
		arr.einfuegen(45);
		arr.einfuegen(52);
		arr.einfuegen(68);
		arr.einfuegen(71);
		arr.einfuegen(80);
		arr.einfuegen(96);
		arr.einfuegen(33);
		arr.einfuegen(17);
		arr.einfuegen(19);
		arr.einfuegen(37);
		arr.einfuegen(44);
		arr.einfuegen(55);
		arr.einfuegen(66);
		arr.einfuegen(79);

		System.out.print("Mergesort\n");
		System.out.print("---------\n");

		System.out.print("Daten unsortiert: \n");
		arr.anzeigen();
		arr.mergeSort();
		System.out.print("\nDaten sortiert  : \n");
		arr.anzeigen();
	}
}