package a007_sort.einfuehrung;

/*******************************************************************************
 * betrachte elemente (objekte) nacheinander und fuege jedes an seinem richtigen
 * Platz zwischen den bereits betrachteten ein 
 * Objektarray (Person) fuellen und sortieren
 ******************************************************************************/

public class a_03_ObjectInsertionSort {
	
	private Person[] objArr;
	private int anzElemente;

	public a_03_ObjectInsertionSort(int max) {
		objArr = new Person[max];
		anzElemente = 0;
	}

	// Objektarray (Person) fuellen
	public void einfuegen(String Nachname, String Vorname, int Alter) {
		objArr[anzElemente] = new Person(Nachname, Vorname, Alter);
		anzElemente++;
	}

	public void anzeigen() {
		for (int j = 0; j < anzElemente; j++)
			objArr[j].anzeigenPerson();
	}

	public void insertionSort() {
		int innen, aussen;

		for (aussen = 1; aussen < anzElemente; aussen++) {
			Person temp = objArr[aussen]; 												// Vergleichsopjekt aussen festlegen - merker
			innen = aussen; 																		// start schieben bei aussen

			while (innen > 0 && 																// bis kleineres (nachname) gefunden wurde,
					objArr[innen - 1].getNachname().compareTo(temp.getNachname()) > 0)  {   //int compareTo( T o ) 
				objArr[innen] = objArr[innen - 1]; 										// nach rechts schieben
				--innen; 																					// linke position
			}
			objArr[innen] = temp; 																// einfuegen merker
		}
	}

	public static void main(String[] args) {
		int groesse = 10;
		a_03_ObjectInsertionSort arr;
		arr = new a_03_ObjectInsertionSort(groesse);

		arr.einfuegen("de Luc�a ", "Paco", 24);
		arr.einfuegen("Jagger  ", "Mick", 59);
		arr.einfuegen("Hendrix", "Jimi ", 37);
		arr.einfuegen("McCartney", "Paul ", 37);
		arr.einfuegen("Santana", "Carlos", 43);
		arr.einfuegen("di Meola", "Al    ", 21);
		arr.einfuegen("Corea   ", "Chick", 29);
		arr.einfuegen("Clapton", "Eric  ", 62);
		arr.einfuegen("McLaughlin", "John ", 62);

		System.out.print("Object-Insertionsort\n");
		System.out.print("--------------------\n");

		System.out.println("Vor dem Sortieren:");
		arr.anzeigen();

		arr.insertionSort();

		System.out.println("\nNach dem Sortieren:");
		arr.anzeigen();
	}
}

class Person {

	private String Nachname;
	private String Vorname;
	private int Alter;

	public Person(String nachname, String vorname, int a) {
		Nachname = nachname;
		Vorname = vorname;
		Alter = a;
	}

	public void anzeigenPerson() {
		System.out.print("   Name:   " + Nachname + ",\t\t" + Vorname
				+ "\t\tAlter: " + Alter + "\n");
	}

	public String getNachname() {
		return Nachname;
	}
}