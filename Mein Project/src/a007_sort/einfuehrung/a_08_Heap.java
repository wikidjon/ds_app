package a007_sort.einfuehrung;
import java.io.IOException;

//***********************************************
class Knoten {
	private int Daten;

	public Knoten(int schluessel) {
		Daten = schluessel;		}

	public int getDaten() {
		return Daten;	}
}
//***********************************************
//***********************************************

public class a_08_Heap {
	private Knoten[] heapArray;
	private int maxGroesse;
	private int aktuelleGroesse; 							// anzahl array eintraege

	public a_08_Heap(int mx) {
		maxGroesse = mx;
		aktuelleGroesse = 0;
		heapArray = new Knoten[maxGroesse];
	}

	public Knoten entfernen() {
		Knoten root = heapArray[0];
		heapArray[0] = heapArray[--aktuelleGroesse];
		absenken(0);
		return root;
	}

	public void absenken(int index) {
		int groesseresKind;
		Knoten oben = heapArray[index];
		while (index < aktuelleGroesse / 2) {
			int linkesKind = 2 * index + 1;
			int rechtesKind = linkesKind + 1;
														// finde groesseres kind
			if (rechtesKind < aktuelleGroesse
					&& heapArray[linkesKind].getDaten() < heapArray[rechtesKind].getDaten())
				groesseresKind = rechtesKind;
			else
				groesseresKind = linkesKind;

			if (oben.getDaten() >= heapArray[groesseresKind].getDaten())
				break;

			heapArray[index] = heapArray[groesseresKind];
			index = groesseresKind;
		}
		heapArray[index] = oben;
	}

	public void anzeigenHeap() {
		int leerzeichen = 32;							//n Leerzeichen
		int werte_je_reihe = 1;							//reihe
		int spalten = 0;
		int aktIndex = 0;

		while (aktuelleGroesse > 0) {
			if (spalten == 0)
				for (int k = 0; k < leerzeichen; k++)
					System.out.print(' '); 				// Leerzeichen zum linken rand
			System.out.print(heapArray[aktIndex].getDaten());

			if (++aktIndex == aktuelleGroesse) 			// fertig ?
				break;

			if (++spalten == werte_je_reihe) { 			// ende der reihe?
				leerzeichen /= 2;
				werte_je_reihe *= 2;
				spalten = 0;
				System.out.println();
			} else
				for (int k = 0; k < leerzeichen * 2 - 2; k++)
					System.out.print(' '); 				// Leerzeichen zwischn den werten
		} 										
	}

	public void anzeigeArray() {
		for (int j = 0; j < maxGroesse; j++)
			System.out.print(heapArray[j].getDaten() + " "); // sortierte werte
		System.out.println("");
	}

	public void einfuegenAn(int index, Knoten newNode) {
		heapArray[index] = newNode;
	}

	public void vergroessern() {
		aktuelleGroesse++;
	}

	public static void main(String[] args) throws IOException {
		int groesse, i;

		groesse = 20;
		a_08_Heap halde = new a_08_Heap(groesse);

		for (i = 0; i < groesse; i++) {
			int random = (int) (java.lang.Math.random() * 100);
			Knoten newNode = new Knoten(random);
			halde.einfuegenAn(i, newNode);
			halde.vergroessern();
		}

		System.out.print("Zufallszahlen:\t");
		halde.anzeigeArray();
		for (i = groesse / 2 - 1; i >= 0; i--)
			halde.absenken(i);

		System.out.print("Heap:\t\t");
		halde.anzeigeArray();
		halde.anzeigenHeap();
		for (i = groesse - 1; i >= 0; i--) {
			Knoten biggestNode = halde.entfernen();
			halde.einfuegenAn(i, biggestNode);
		}
		System.out.print("\n\nSortiert: ");
		halde.anzeigeArray();
	}
}