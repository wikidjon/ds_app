package a06_enumerator;
//Wrapping eines Iterator zur Bearbeitung einer Enumeration
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

public class EnumerationIterator {
  public static Iterator<?> iterator(final Enumeration<String> e) {
    return new Iterator<Object>() {
      public boolean hasNext() {
        return e.hasMoreElements();
      }

      public Object next() {
        return e.nextElement();
      }

      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }

  public static void main(String args[]) {
    String elements[] = { "Java", "ist", "eine", "wunderbare","Sprache", "." };
    Vector<String> v = new Vector<String>(Arrays.asList(elements));
    Enumeration<String> e = v.elements();
    Iterator<?> itor = EnumerationIterator.iterator(e);
    while (itor.hasNext()) {
      System.out.println(itor.next());
    }
  }
}