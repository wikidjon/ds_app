package a05_Baeume;

@SuppressWarnings("unchecked")

public class BinBaum {
	public static final int INORDER = 1;
	public static final int PREORDER = 2;
	public static final int POSTORDER = 3;

	static class Knoten {
		Object Schluessel;								// struct in c
		Knoten links = null; 
		Knoten rechts = null;
		
		public Knoten (Object e) 	{Schluessel = e; }
		public Knoten getlinks () 	{return links; }
		public Knoten getrechts () 	{return rechts; }
		public Object getSchluessel () 	{return Schluessel; }
		
		public void setlinks (Knoten n) 	{links = n; }
		public void setrechts (Knoten n) 	{rechts = n; }
		
		public int vergleicheKnoten(Comparable<?> c) {
			Schluessel = c;
			return (Schluessel == null ? -1 : ((Comparable<Comparable<?>>) Schluessel).compareTo(c));
											// >0; 0 oder <0
		}
		
		public String toString () { return Schluessel.toString (); }
		
		public void printKnoten (StringBuffer sb, int tiefe, Knoten nullKnoten) {
			for (int i = 0; i < tiefe; i++)
				sb.append ("  ");
			sb.append (toString () + "\n");
			if (links != nullKnoten)
				links.printKnoten (sb, tiefe + 1, nullKnoten);
			if (rechts != nullKnoten)
				rechts.printKnoten (sb, tiefe + 1, nullKnoten);
		} 
	}
	//*******Ende class Knoten*******************************************************
	
	private Knoten head, nullKnoten;
	
	public BinBaum () {
		head = new Knoten(null);
		nullKnoten = new Knoten(null);
		nullKnoten.setlinks(nullKnoten);
		nullKnoten.setrechts(nullKnoten);
		head.setrechts(nullKnoten);
		head.setlinks(nullKnoten);
	}
	
	public boolean einfuegen (Comparable<?> c) {
		System.out.println("\neinzufuegen ist: " + c);
		Knoten eltern = head, kind = head.getrechts();
		// bewegen durch Baum
		//System.out.println ("-- vor li - re----");
		while (kind != nullKnoten) {
			eltern = kind;
			int cmp = kind.vergleicheKnoten(c);  //kleiner >1 -- groesser <1
			//System.out.println ("-- in while ----");
			//System.out.println ("-- cmp: " + cmp);

			if (cmp == 0){
				//System.out.println ("-- in cmp==0 ----");
				return false;}
			else if (cmp > 0){
				System.out.println ("<-- nach links gegangen (cmp: " + cmp +")");
				kind = kind.getlinks ();
			}
			else{
				kind = kind.getrechts ();
				System.out.println ("--> nach rechts gegangen (cmp: " + cmp +")");}

		}
		// einfuegen Knoten *********************************
		Knoten neuKnoten = new Knoten(c);
		neuKnoten.setlinks(nullKnoten); neuKnoten.setrechts(nullKnoten);
		
		if (eltern.vergleicheKnoten(c) > 0){
			eltern.setlinks (neuKnoten);}
		else{
			eltern.setrechts (neuKnoten);}
		return true;
	}
	
	public boolean finde (Comparable<?> c) {
		return (findeKnoten (c) != null);
	}
	
	protected Knoten findeKnoten (Comparable<?> c) {
		Knoten n = head.getrechts();
		while (n != nullKnoten) {
			int cmp = n.vergleicheKnoten(c);
			if (cmp == 0)
				return n;
			else
				n = (cmp > 0 ? n.getlinks () : n.getrechts ());
		}
		return null;
	}
	
	protected Knoten rekursivFindeKnoten (Knoten n, Comparable<?> c) {
		if (n != nullKnoten) {
			int cmp = n.vergleicheKnoten (c);
			if (cmp == 0)
				return n;
			else if (cmp > 0)
				return rekursivFindeKnoten (n.getlinks (), c);
			else
				return rekursivFindeKnoten (n.getrechts (), c);
		}
		else
			return null;
	}
	
	public boolean rekursivFinde (Comparable<?> c) {
		return rekursivFindeKnoten (head.getrechts(), c) != null;
	}
	
	public Object findeMinElement () {
		Knoten n = head.getrechts();
		while (n.getlinks () != nullKnoten)
			n = n.getlinks ();
		return n.getSchluessel ();
	}
	
	public Object findeMaxElement () {
		Knoten n = head.getrechts();
		while (n.getrechts () != nullKnoten)
			n = n.getrechts ();
		return n.getSchluessel ();
	}
	
	public boolean entfernen (Comparable<?> c) {
		Knoten eltern = head, neuKnoten = head.getrechts(), kind = null, tmp = null;
		
		// zu löschenden Knoten suchen
		while (neuKnoten != nullKnoten) {
			int cmp = neuKnoten.vergleicheKnoten(c);
			if (cmp == 0)
				break;
			else { 
				eltern = neuKnoten;
				neuKnoten = (cmp > 0 ? neuKnoten.getlinks() : neuKnoten.getrechts());
			}
		}
		if (neuKnoten == nullKnoten)
			// Kein Knoten gefunden
			return false;
		// Fall 1
		if (neuKnoten.getlinks() == nullKnoten && neuKnoten.getrechts() == nullKnoten)
			kind = nullKnoten;
		else if (neuKnoten.getlinks() == nullKnoten)
			kind = neuKnoten.getrechts();
		else if (neuKnoten.getrechts() == nullKnoten)
			kind = neuKnoten.getlinks();
		else {
			// minElement
			kind = neuKnoten.getrechts(); tmp = neuKnoten;
			while (kind.getlinks () != nullKnoten) {
				tmp = kind;
				kind = kind.getlinks ();
			}
			kind.setlinks(neuKnoten.getlinks());
			if (tmp != neuKnoten) {
				tmp.setlinks(kind.getrechts());
				kind.setrechts(tmp);
			}
		}
		if (eltern.getlinks() == neuKnoten)
			eltern.setlinks(kind);
		else 
			eltern.setrechts(kind);
		
		neuKnoten = null;
		return true;
	}
	
	private void printPreorder (Knoten n) {
		if (n != nullKnoten) {
			//System.out.println (n.toString ());
			System.out.print (n.toString()+ " -> " );
			printPreorder (n.getlinks ());
			printPreorder (n.getrechts ());
		}
	}
	
	private void printPostorder (Knoten n) {
		if (n != nullKnoten) {
			printPostorder (n.getlinks ());
			printPostorder (n.getrechts ());
			System.out.print (n.toString()+ " -> " );
		}
	}
	
	protected void printInorder (Knoten n) {
		if (n != nullKnoten) {
			printInorder (n.getlinks ());
			System.out.print (n.toString()+ " -> " );
			printInorder (n.getrechts ());
		}
	}

	public void traversieren (int order) {
		switch (order) {
			case INORDER:
				printInorder (head.getrechts());
				break;
			case PREORDER:
				printPreorder (head.getrechts());
				break;
			case POSTORDER:
				printPostorder (head.getrechts());
				System.out.print ("\n****\n " );
				break;
			default:
		}
	}
	
	public String toString () {
		StringBuffer sb = new StringBuffer ();
		if (head.getrechts() != nullKnoten)
			head.getrechts().printKnoten (sb, 0, nullKnoten);
		return sb.toString ();
	}
	
	public static void main (String[] args) {
		BinBaum tree = new BinBaum ();
		System.out.println ("-------Aufbau BinBaum ------------------ ");
		tree.einfuegen (new Integer (6));
		tree.einfuegen (new Integer (3));
		tree.einfuegen (new Integer (1));
		tree.einfuegen (new Integer (5));
		tree.einfuegen (new Integer (4));
		tree.einfuegen (new Integer (9));
		tree.einfuegen (new Integer (7));
		tree.einfuegen (new Integer (10));
		System.out.println ("\n-----traversieren Inorder ------------------ ");
		tree.traversieren(1);
		System.out.println ("\n\n-----traversieren Preorder ------------------ ");
		tree.traversieren(2);
		System.out.println ("\n\n-----traversieren Postorder ------------------ ");
		tree.traversieren(3);
		System.out.println ("\n\n-------min-/max-Element ------------------ ");
		System.out.println ("min = " + tree.findeMinElement ());
		System.out.println ("max = " + tree.findeMaxElement ());
		System.out.println ("\n\n-----Ausgabe des Objektes Baum------------------ ");
		System.out.println (tree);
		System.out.println ("\n----- findeen von Werten------------------ ");
		System.out.println("gesucht: 5   gefunden: --> " + tree.finde(new Integer(5)));
		System.out.println("gesucht: 8   gefunden: --> " + tree.finde(new Integer(8)));
		System.out.println("gesucht: 5 rek  gefunden: --> " + tree.rekursivFinde(new Integer(5)));
		System.out.println("gesucht: 8 rek  gefunden: --> " + tree.rekursivFinde(new Integer(8)));

		System.out.println ("\n----- entfernen von Werten------------------ ");
		System.out.println("entfernt 4: " + tree.entfernen (new Integer (4)));
		System.out.println (tree);
		System.out.println("entfernt 3: " + tree.entfernen (new Integer (3)));
		System.out.println (tree);
		System.out.println("entfernt 9: " + tree.entfernen (new Integer (9)));
		System.out.println (tree);
		System.out.println("entfernt 6: " + tree.entfernen (new Integer (6)));
		System.out.println (tree);
		
	}
}
