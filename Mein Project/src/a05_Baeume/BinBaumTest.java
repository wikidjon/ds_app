package a05_Baeume;

public class BinBaumTest {

	public static void main(String[] args) {
		new BinBaumTest().start();
	}

	static class Knoten {
		Knoten links;
		Knoten rechts;
		int wert;

		public Knoten(int wert) {
			this.wert = wert;
		}
	}

	public void start() {
		// anlegen des baumes
		Knoten root = new Knoten(5);
		System.out.println("Aufbau Binaerbaum");
		System.out.println("Root:" + root.wert);
		einfuegen(root, 1);
		einfuegen(root, 8);
		einfuegen(root, 6);
		einfuegen(root, 3);
		einfuegen(root, 9);
		System.out.println("Traversieren Inorder");       //Inorder LWR  Preorder WLR   Postorder LRW
		printInOrder(root);
	}

	public void einfuegen(Knoten knoten, int wert) {
		if (wert < knoten.wert) {
			if (knoten.links != null) {
				einfuegen(knoten.links, wert);
			} else {
				System.out.println("  Eingefuegt " + wert + " links von " + knoten.wert);
				knoten.links = new Knoten(wert);
			}
		} else if (wert > knoten.wert) {
			if (knoten.rechts != null) {
				einfuegen(knoten.rechts, wert);
			} else {
				System.out.println("  Eingefuegt " + wert + " rechts von " + knoten.wert);
				knoten.rechts = new Knoten(wert);
			}
		}
	}

	public void printInOrder(Knoten knoten) {
		if (knoten != null) {
			printInOrder(knoten.links);
			System.out.println("  Traversiert " + knoten.wert);
			printInOrder(knoten.rechts);
		}
	}
}