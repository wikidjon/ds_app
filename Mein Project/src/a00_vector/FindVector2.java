package a00_vector;

import java.util.Vector;

// Jedes Exemplar der Klasse Vector vertritt ein Array mit variabler L�nge. 
// Der Zugriff auf die Elemente erfolgt �ber Indizes, also �ber Methoden, 
// die einen Index als Parameter annehmen. 
// Da in einem Vector jedes Exemplar einer von Object abgeleiteten Klasse Platz findet, 
// ist ein Vector nicht auf bestimmte Datentypen fixiert. 
// Dies ist ein Problem, denn beim Zugriff auf Elemente des Vektors muss der Entwickler wissen, 
// welchen Typ die Vektorelemente haben werden. 

public class FindVector2 {
  static String inhalte[] = { "Angela", "Merkel", "Emerson", "Lake", "und",
      "Palmer", "Pink", "Floyd", "Al", "Jarreau" };

  public static void main(String args[]) {
    Vector<String> v = new Vector<String>();
    for (int i = 0, n = inhalte.length; i < n; i++) {
      v.add(inhalte[i]);
    }
    System.out.println("Vector: " + v);
    System.out.println("------------------------------------------------------------------------------");
    System.out.println("enthalten Beatles?: " + v.contains("Beatles"));
    System.out.println("enthalten Emerson?: " + v.contains("Emerson"));
    System.out.println("Wo ist Anglea?: " + v.indexOf("Angela"));
    System.out.println("Wo ist Palmer?: " + v.indexOf("Palmer"));
    System.out.println("Wo ist Palmer vom Ende?: " + v.lastIndexOf("Palmer"));
    int index = 0;
    int length = v.size();
    while ((index < length) && (index >= 0)) {
      index = v.indexOf("Palmer", index);
      if (index != -1) {
        System.out.println(v.get(index));
        index++;
      }
    }
  }
}