package a11_collections;

// manche methoden im collection interfaces arbeiten nicht korrekt
// {ThrowsException}

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class Unsupported {
	static List<String> a = Arrays
			.asList("eins zwei drei vier fuenf sechs sieben acht".split(" "));

	static List<String> a2 = a.subList(3, 6);

	public static void main(String[] args) {
		System.out.println(a);
		System.out.println(a2);
		System.out.println("a.contains(" + a.get(0) + ") = " + a.contains(a.get(0)));
		System.out.println("a.containsAll(a2) = " + a.containsAll(a2));
		System.out.println("a.isEmpty() = " + a.isEmpty());
		System.out.println("a.indexOf(" + a.get(5) + ") = "+ a.indexOf(a.get(5)));
												// rueckwaerts traversieren
		ListIterator<String> lit = a.listIterator(a.size());
		
		while (lit.hasPrevious())
			System.out.print(lit.previous() + "-- ");

		System.out.println();
												// setzt elemente auf verschiedene Werte
		for (int i = 0; i < a.size(); i++)
			a.set(i, "47");
		System.out.println(a);
												// compiliert, aber funktioniert nicht :
		lit.add("X"); 						// nicht unterstuetzte operation
		a.clear(); 							// nicht unterstuetzt
		a.add("elf"); 						// nicht unterstuetzt
		a.addAll(a2); 						// nicht unterstuetzt
		a.retainAll(a2); 					// nicht unterstuetzt
		a.remove(a.get(0)); 				// nicht unterstuetzt
		a.removeAll(a2); 					// nicht unterstuetzt
	}
} 
