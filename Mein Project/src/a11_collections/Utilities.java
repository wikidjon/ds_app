package a11_collections;

//Collections utilities
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

public class Utilities {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		List<String> list = Arrays.asList("eins Zwei drei Vier fuenf sechs eins".split("  "));
		System.out.println(list);
		System.out.println("max: " + Collections.max(list));
		System.out.println("min: " + Collections.min(list));
		AlphaComparator comp = new AlphaComparator();
		System.out.println("max w/ comparator: " + Collections.max(list, comp));
		System.out.println("min w/ comparator: " + Collections.min(list, comp));
		
		List<String> sublist = Arrays.asList("Vier fuenf sechs".split(" "));
		System.out.println("indexOfSubList: " + Collections.indexOfSubList(list, sublist));
		System.out.println("lastIndexOfSubList: " + Collections.lastIndexOfSubList(list, sublist));
		
		Collections.replaceAll(list, "eins", "EINZ");
		System.out.println("replaceAll: " + list);
		Collections.reverse(list);
		System.out.println("reverse: " + list);
		Collections.rotate(list, 3);
		System.out.println("rotate: " + list);
		List<String> source = Arrays.asList("eine kleine Wortfolge".split(" "));
		Collections.copy(list, source);			//Zielindex ist gleich Quellindex !!!
		System.out.println("copy: " + list);
		Collections.swap(list, 0, list.size() - 1);
		System.out.println("swap: " + list);
		Collections.fill(list, "fill");
		System.out.println("fill: " + list);
		List duplikate = Collections.nCopies(3, "Kopie");
		System.out.println("duplikate: " + duplikate);
		
		// eine Enumeration aus den duplikaten holen
		Enumeration e = Collections.enumeration(duplikate);
		Vector v = new Vector();
		while (e.hasMoreElements())
			v.addElement(e.nextElement());
		// Converting eines Vectors zu einer Arrayliste ueber eine Enumeration:
		ArrayList arrayList = Collections.list(v.elements());
		System.out.println("ArrayListe: " + arrayList);

	}
} 

@SuppressWarnings("unchecked")
class AlphaComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		String s1 = (String) o1;
		String s2 = (String) o2;
		return s1.toLowerCase().compareTo(s2.toLowerCase());
	}
}
