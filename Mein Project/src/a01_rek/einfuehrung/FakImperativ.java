package a01_rek.einfuehrung;

public class FakImperativ {
	public static int fakBerechnung(int x) {
		int y = 1;

		while (x > 1) {
			y = y * x;
			x--;
		}
		return y;
	}

	public static void main(String[] args) {

		int z;
		z = Tastatureingabe.readInt("Zahl: ");
		System.out.println("Fakultaet(" + z + "!) = " + fakBerechnung(z));
	}
}
