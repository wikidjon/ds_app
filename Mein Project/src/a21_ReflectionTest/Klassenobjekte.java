package a21_ReflectionTest;

@SuppressWarnings("unused")
public class Klassenobjekte {
	public static void main(String args[]) throws ClassNotFoundException {
		String 										s = "Das ist ein kleiner Text";
		Class<? extends String> 			cStr1 = s.getClass();
		Class<String> 							cStr2 = java.lang.String.class;
		Class<?> 									cStr3 = Class.forName("java.lang.String");

		System.out.println("Klassenname: \t" + cStr1.getName());  			//siehe auch a11_threads ... Thread02
		System.out.println("Package: \t\t" + 		cStr1.getPackage().getName());

		Class<?>[] intf = 	cStr1.getInterfaces();
		for (Class<?> c : intf)
			System.out.println("Interfaces: \t\t" + 		c.getName());

		System.out.println("Basisklasse: \t\t" + cStr1.getSuperclass().getName());
	}

}
