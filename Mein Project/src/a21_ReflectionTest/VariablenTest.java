package a21_ReflectionTest;

import java.lang.reflect.*;

public class VariablenTest {
	public static void main(String args[]) {

		Class<TestKlasse> reflect_v2 = a21_ReflectionTest.TestKlasse.class;

		try {
			Object obj = reflect_v2.newInstance();
			Field[] variable = reflect_v2.getDeclaredFields();

			Field.setAccessible(variable, true);
			for (Field f : variable)
				System.out.println(f.getName() + ":" + f.get(obj).toString());
			
			Field priv = reflect_v2.getDeclaredField("autor");
			priv.setAccessible(true);
			priv.set(obj, "Test");
		} catch (Exception ex) {
		}
	}
}
