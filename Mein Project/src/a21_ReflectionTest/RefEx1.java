package a21_ReflectionTest;

public class RefEx1 {
	public static void main(String[] args) {
		Person1 p1 = new Person1();
		Mann1 m1 = new Mann1();

		System.out.println("runtime klasse der variable p1 = "+ p1.getClass().getName() );
		System.out.println("runtime klasse der variable m1 = "+ m1.getClass().getName() );

		p1 = m1; 			//up-cast ist typsicher
		System.out.println("runtime klasse der variable p1 = "+ p1.getClass().getName() );
	} 
} 

class Person1 {
} 

class Mann1 extends Person1 {
} 

class Frau1 extends Person1 {
} 