package a21_ReflectionTest;

import java.lang.reflect.*;

public class MethodenTest
{
  public MethodenTest()
  {
    try
    {
      Class<TestKlasse> cTK = TestKlasse.class;
      TestKlasse tk = (TestKlasse)cTK.newInstance();
      Method m = cTK.getMethod("getName");
      System.out.println(m.invoke(tk));
    }
    catch(Exception ex)
    {}
  }

  public static void main(String args[])
  {
    new MethodenTest();
  }
}
