package a21_ReflectionTest;

public class refEx4
{
	public static void main(String[] args)
	{
			Person p1 = new Person();
			Mann m1 = new Mann();
			@SuppressWarnings("unused")
			Frau f1 = new Frau();

			System.out.println( p1.getClass().isAssignableFrom( m1.getClass() ) );

	}
} 

class Person
{ }

class Mann extends Person
{ }

class Frau extends Person
{ }