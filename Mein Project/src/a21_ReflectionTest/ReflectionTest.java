package a21_ReflectionTest;

import java.lang.reflect.*;

public class ReflectionTest
{
	
   public static void main(String[] args)
   {
      // Klassenname von Befehlszeile oder Benutzereingabe lesen
	  eingabe ein = new eingabe();
      String name;
      if (args.length > 0)
         name = args[0];
      else
			name = ein.eingString("Klassenname: "); 		// a21_ReflectionTest.TestKlasse 

      try
      {
         // Klassenname und Name der Oberklasse ausgeben (wenn != Object)
         Class<?> cl 		= Class.forName(name);
         Class<?> supercl 	= cl.getSuperclass();
         System.out.print("\n\n*************************\n");
         System.out.print("class " + name);
         if (supercl != null && supercl != Object.class)
            System.out.print(" extends " + supercl.getName());

         System.out.print("\n{\n");
         System.out.print("\n*************************\n");
         System.out.print("Konstruktoren:\n");
         printKonstruktoren(cl);
         System.out.println();
         System.out.print("\n*************************\n");
         System.out.print("Methoden:\n");
         printMethoden(cl);
         System.out.println();
         System.out.print("\n*************************\n");
         System.out.print("Attribute:\n");
         printAttribute(cl, 3);
         System.out.println("}");
      }
      catch(ClassNotFoundException e) 
	{ 
	   e.printStackTrace(); 
	}
      System.exit(0);
   }

//   int feld1;
//   float feld2;
   /*******************************************************
      Alle Konstruktoren einer Klasse ausgeben
    *******************************************************/
   public static void printKonstruktoren(Class<?> cl)
   {
      Constructor<?>[] konstruktoren = cl.getDeclaredConstructors();

      for (int i = 0; i < konstruktoren.length; i++)
      {
         Constructor<?> k = konstruktoren[i];
         String name = k.getName();
         System.out.print(Modifier.toString(k.getModifiers()));
         System.out.print("   " + name + "(");

         // Parametertypen ausgeben
         Class<?>[] paramTypes = k.getParameterTypes();
         for (int j = 0; j < paramTypes.length; j++)
         {
            if (j > 0) System.out.print(", ");
            System.out.print("ParameterTypen: " + paramTypes[j].getName());
         }
         System.out.println(");");
      }
   }

   /*************************************************************
      Alle Methoden einer Klasse ausgeben
    ************************************************************/
   public static void printMethoden(Class<?> cl)
   {
      Method[] methoden = cl.getDeclaredMethods();

      for (int i = 0; i < methoden.length; i++)
      {
         Method m = methoden[i];
         Class<?> retType = m.getReturnType();
         String name = m.getName();

         // Modifizierer, Rueckgabetyp und Methodenname ausgeben
         System.out.print(Modifier.toString(m.getModifiers()));
         System.out.print("  " + retType.getName() + " " + name + "(");

         // Parametertypen ausgeben
         Class<?>[] paramTypes = m.getParameterTypes();
         for (int j = 0; j < paramTypes.length; j++)
         {
            if (j > 0) System.out.print(", ");
            //System.out.print("Parametertypen: " + paramTypes[j].getName());
            System.out.print(paramTypes[j].getName());
         }
         System.out.println(");");
      }
   }

   /***************************************************************
      Alle Felder einer Klasse ausgeben
    ***************************************************************/
   public static void printAttribute(Class<?> cl, int abc)
   {
      Field[] attrib = cl.getDeclaredFields();

      for (int i = 0; i < attrib.length; i++)
      {
         Field a = attrib[i];
         Class<?> type = a.getType();
         String name = a.getName();
         System.out.print(Modifier.toString(a.getModifiers()));
         System.out.println("   " + type.getName() + " " + name + ";");
      }
   }

}
