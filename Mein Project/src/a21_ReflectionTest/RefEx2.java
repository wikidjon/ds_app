package a21_ReflectionTest;

public class RefEx2 {
	public static void main(String[] args) {
		try {
			Class<?> c1 = Class.forName("Person11");
			System.out.println("Name der Klasse:"+c1.getName() );
		} catch (ClassNotFoundException cnfe) {
			System.out.println("Kann Klasse " + cnfe.toString()+ " - \n " + "Message: " + cnfe.getMessage() );
		} 
	} 
} 

class Person11 {
} 