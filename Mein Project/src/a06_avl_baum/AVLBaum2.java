package a06_avl_baum;

public class AVLBaum2 {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static class AVLKnoten {

		int balance; // -1, 0, oder 1
		Object schluessel;
		AVLKnoten links = null, rechts = null;

		public AVLKnoten(Object e) {
			schluessel = e;
			balance = 0;
		}

		public AVLKnoten getLinks() {
			return links;
		}

		public AVLKnoten getRechts() {
			return rechts;
		}

		public Object getSchluessel() {
			return schluessel;
		}

		public void setlinks(AVLKnoten n) {
			links = n;
		}

		public void setrechts(AVLKnoten n) {
			rechts = n;
		}

		public int vergleichKnoten(Comparable<?> c) { 
			// wildcard-typ - durch die Implementierung dieses Interface kann eine Ordnung auf den
			// Objekten einer Klasse definiert werden.
			// Objekte von Klassen, die dieses Interface implementieren,
			// k�nnen z. B. von der Methode Arrays.sort() sortiert werden
			
			// negativer R�ckgabewert: Der erste Parameter ist untergeordnet
			// 0 als R�ckgabewert: Beide Parameter werden gleich eingeordnet
			// positiver R�ckgabewert: Der erste Parameter ist  �bergeordnet
			return (schluessel == null ? -1
					: ((Comparable<Comparable>) schluessel).compareTo(c));
		}

		public String toString() {
			return schluessel == null ? "(null)" : schluessel.toString();
		}

		public void printKnoten(StringBuffer sb, int tiefe, AVLKnoten nullKnoten) {
			for (int i = 0; i < tiefe; i++)
				sb.append("  ");
			sb.append(toString() + "\n");
			if (links != nullKnoten)
				links.printKnoten(sb, tiefe + 1, nullKnoten);
			if (rechts != nullKnoten)
				rechts.printKnoten(sb, tiefe + 1, nullKnoten);
		}

		public int getBalance() {
			return balance;
		}

		public void setBalance(int b) {
			balance = b;
		}
	} // Ende AVL Knoten **************************************************

	private int elemente;
	private AVLKnoten kopf, ende;
	private boolean rebalance = false; // Hilfsvariable fuer Einfuegen u.
										// Loeschen

	public AVLBaum2() {
		kopf = new AVLKnoten(null);
		ende = new AVLKnoten(null);
		ende.setlinks(ende);
		ende.setrechts(ende);
		kopf.setrechts(ende);
		kopf.setlinks(ende);
		elemente = 0;
	}

	public boolean finde(Comparable<?> c) { 
		// Durch die Implementierung dieses Interface kann eine Ordnung auf den Objekten einer Klasse definiert werden.
		// Objekte von Klassen, die dieses Interface implementieren, k�nnen z. B. von der Methode Arrays.sort() sortiert werden
		return (findeKnoten(c) != null);
	}

	public AVLKnoten findeKnoten(Comparable<?> c) {
		AVLKnoten n = kopf.getRechts();
		while (n != ende) {
			int cmp = n.vergleichKnoten(c);
			if (cmp == 0)
				return n;
			else
				n = cmp > 0 ? n.getLinks() : n.getRechts();
		}
		return null;
	}

	private AVLKnoten rotierenlinks(AVLKnoten n) {
		AVLKnoten aktuell = n.getRechts();
		n.setrechts(n.getRechts().getLinks()); // ggf. linken nf des rechten nf
												// anhaengen
		aktuell.setlinks(n);
		return aktuell;
	}

	private AVLKnoten rotierenrechts(AVLKnoten n) {
		AVLKnoten aktuell = n.getLinks();
		n.setlinks(n.getLinks().getRechts()); // ggf rechten nf des linken nf
												// anhaengen
		aktuell.setrechts(n);
		return aktuell;
	}

	AVLKnoten einfuegenKnoten(AVLKnoten n, Comparable<?> k) { 
		// vergleich kopf/knoten  und aktuelles Objekt
		AVLKnoten tmp;
		System.out.println("   - n (Vater): " + n);
		if (n.vergleichKnoten(k) == 0) 
			// objekte gleich - ggf. andere verarbeitung
			return n;
		else if (n.vergleichKnoten(k) < 0) {
			// weiter nach rechts gehen
			if (n.getRechts() != ende) {
				// rechts durchlaufen
				System.out.println("   **** nach rechts gegangen *****");
				n.setrechts(einfuegenKnoten(n.getRechts(), k)); // Rekursion
																// !!!!
				System.out.println("   rest der rekursion rechts *****");
				if (n != kopf && rebalance) {
					System.out.println(" -> ausgleichen notwendig ?*****");
					System.out.println("         balance: " + n.getBalance());
					switch (n.getBalance()) {
					case 1:
						if (n.getRechts().getBalance() == 1) {
							// Rotation nach links
							System.out.println("****Rotation nach links***** n"
									+ n);
							tmp = rotierenlinks(n);
							tmp.getLinks().setBalance(0);
						} else {
							// doppelte Rotation rechts-links
							System.out
									.println("****Rotation nach rechts-links*****");
							int b = n.getRechts().getLinks().getBalance();
							n.setrechts(rotierenrechts(n.getRechts()));
							tmp = rotierenlinks(n);
							tmp.getRechts().setBalance((b == -1) ? 1 : 0);
							tmp.getLinks().setBalance((b == 1) ? -1 : 0);
						}
						tmp.setBalance(0);
						rebalance = false;
						return tmp;
					case 0:
						n.setBalance(1);
						System.out.println("   setBalance(1)*****");
						return n;
					case -1:
						n.setBalance(0);
						System.out.println("   setBalance(0)*****");
						rebalance = false;
						return n;
					}
				} else
					return n;
			} else {
				AVLKnoten newKnoten = new AVLKnoten(k);
				System.out
						.println("   - neuer AVL Knoten rechts angelegt -> Schluessel: "
								+ k + " ->  n(Vater):" + n + " -> kopf " + kopf);
				newKnoten.setlinks(ende);
				newKnoten.setrechts(ende);
				n.setrechts(newKnoten);
				n.setBalance(n.getBalance() + 1);
				rebalance = (n.getBalance() >= 1); // ******
				System.out.println("   - rebalance: " + rebalance
						+ " - balance Vater: " + n.getBalance()
						+ " - balance neu: " + newKnoten.getBalance());
				return n;
			}
		} else { // links einfuegen (analog)
			System.out.println("   *** nach links gegangen *****");
			if (n.getLinks() != ende) {
				n.setlinks(einfuegenKnoten(n.getLinks(), k)); // Rekursion
																// !!!!
				System.out.println("   rest der rekursion links *****");
				if (n != kopf && rebalance) {
					System.out.println("*** ausgleichen notwendig*****");
					System.out.println("   balance: " + n.getBalance());
					switch (n.getBalance()) {
					case -1:
						if (n.getLinks().getBalance() == -1) {
							// Rechtsrotation
							System.out.println("****Rotation nach rechts*****n"
									+ n);
							tmp = rotierenrechts(n);
							tmp.getRechts().setBalance(0);
						} else {
							// Linksrechts-Rotation
							System.out
									.println("   ****Rotation nach links-rechts*****");
							int b = n.getLinks().getRechts().getBalance();
							n.setlinks(rotierenlinks(n.getLinks()));
							tmp = rotierenrechts(n);
							tmp.getRechts().setBalance((b == -1) ? 1 : 0);
							tmp.getLinks().setBalance((b == 1) ? -1 : 0);
						}
						tmp.setBalance(0);
						rebalance = false;
						return tmp;
					case 0:
						n.setBalance(-1);
						return n; // rebalance bleibt true
					case 1:
						n.setBalance(0);
						rebalance = false;
						return n;
					}
				} else
					return n;
			} else {
				AVLKnoten newKnoten = new AVLKnoten(k);
				System.out
						.println("   --- neuer AVL Knoten links angelegt  -> Schluessel: "
								+ k + " -> n(Vater):" + n + " -> kopf " + kopf);
				newKnoten.setlinks(ende);
				newKnoten.setrechts(ende);
				n.setlinks(newKnoten);
				n.setBalance(n.getBalance() - 1);
				rebalance = (n.getBalance() <= -1);
				System.out.println("   - rebalance: " + rebalance
						+ " - balance Vater: " + n.getBalance()
						+ " - balance neu: " + newKnoten.getBalance());

				return n;
			}
		}
		return null;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (kopf.getRechts() != ende)
			kopf.getRechts().printKnoten(sb, 0, ende);
		return sb.toString();
	}

	public void einfuegen(Comparable<?> k) {
		einfuegenKnoten(kopf, k);
		elemente++;
	}

	public int size() {
		return elemente;
	}

	public AVLKnoten getWurzelKnoten() {
		return kopf.getRechts();
	}

	public AVLKnoten getende() {
		return ende;
	}

	public static void main(String[] args) {
		AVLBaum2 tree = new AVLBaum2();
		System.out.println("AVL-Baum \nAufbau Teil 1");
		System.out.println("--------------");
		System.out.println("tree.einfuegen(3)");
		tree.einfuegen(new Integer(3));
		System.out.println("\ntree.einfuegen(2)");
		tree.einfuegen(new Integer(2));
		// System.out.println (tree);
		System.out.println("\ntree.einfuegen(1)");
		tree.einfuegen(new Integer(1));
		System.out.println("\nAusgabe");
		System.out.println("-------------");
		System.out.println(tree);
		System.out.println("\nAufbau Teil 2");
		System.out.println("-------------");
		System.out.println("\ntree.einfuegen(4)");
		tree.einfuegen(new Integer(4));
		System.out.println("\ntree.einfuegen(5)");
		tree.einfuegen(new Integer(5));
		System.out.println("\ntree.einfuegen(6)");
		tree.einfuegen(new Integer(6));
		System.out.println("\ntree.einfuegen(7)");
		tree.einfuegen(new Integer(7));
		System.out.println("\ntree.einfuegen(16)");
		tree.einfuegen(new Integer(16));
		System.out.println("\ntree.einfuegen(15)");
		tree.einfuegen(new Integer(15));
		System.out.println("\nAusgabe");
		System.out.println("-------------");
		System.out.println(tree);
	}
}
