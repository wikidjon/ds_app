package cleverApp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

public interface include extends codeSamples{
	
	Display display = Display.getDefault();
	Shell shell = new Shell();
	Menu menu = new Menu(shell, SWT.BAR);
	MenuItem mntmHelp = new MenuItem(menu, SWT.NONE);
	Tree tree = new Tree(shell, SWT.BORDER);
	Canvas canvas = new Canvas(shell, SWT.NONE);
	public static Text text = new Text(shell, SWT.BORDER | SWT.V_SCROLL | SWT.V_SCROLL);
	public static Text text_1 = new Text(shell, SWT.BORDER);
	public static Text text_2 = new Text(shell, SWT.BORDER);
	Label lblNewLabel = new Label(shell, SWT.NONE);
	
	public String[] file_menu = {"New","Open","Exit"};
	public String[] help_menu = {"Help Manual","About Clever"};
	public String[] edit_menu = {"Cut","Copy","Paste"};
	public String[] tree_menu = {"Regular Expression","Liked List","Doubly Linked List"};
	public String aboutClever= "Project done at Hochschule Merseburg by Jonathan Vijayakumar";
	public String appIconPath = "e:\\edu\\Merseburg\\Project\\apple_256.ico";
	public String appTitle = "SuperDSTeacher";
	public String codeCol_msg = "This is where the source code for your clicks appears";
	
}
