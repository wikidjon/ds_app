package cleverApp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;

public interface codeSamples {
	String regex_check_code = "check.addListener(SWT.Selection, event -> {" + "\n" + "\n" + 
			"String tobematched = line.getText();" + "\n" +  "\n" +
			"String pattern = regex_exp.getText();" + "\n" +  "\n" +
			"Pattern result = Pattern.compile(pattern);" + "\n" + "\n" +
			"Matcher match = result.matcher(tobematched);" + "\n" + "\n" +
			"if(match.find()){" + "\n" + "\n" +
				"result_label.setText('Das ist richtig!');" + "\n" + "\n" +
			"} else {" + "\n" + "\n" +
				"result_label.setText('Nein!');" + "\n" + "\n" +
			"}" + "\n" + "\n" +
		"});";
}
