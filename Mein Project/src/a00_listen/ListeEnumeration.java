package a00_listen;

/* Programmname: Verkettete Liste
* Iterator-Klasse: ListeEnumeration
*/

import java.util.*;

class ListeEnumeration implements Enumeration<Object>
{
    //Attribute
    private Link aktuellerLink;
    //private Link ende;
    //Konstruktor
    ListeEnumeration(Link start, Link ende)
    {
        aktuellerLink = start;
        //this.ende = ende;
    }
    //Operationen
    public boolean hasMoreElements()
    {
        return (aktuellerLink != null);
    }
    public Object nextElement()
    {
        Object Daten = aktuellerLink;
        aktuellerLink = aktuellerLink.Naechster;
        return Daten;
    }
}