package a14_bitset;
import java.util.BitSet;

public class NumSeries {
  public static void main(String[] args) {
    for (int i = 1; i <= months.length; i++)
      System.out.println("Month # " + i);
  
    for (int i = 0; i < months.length; i++)
      System.out.println("Month " + months[i]);

    BitSet b = new BitSet();
    b.set(0);  // January
    b.set(3);  // April

    for (int i = 0; i<months.length; i++) {
      if (b.get(i))
        System.out.println("Month " + months[i] + " requested");
    }
  }
  /** The names of the months. See Dates/Times chapter for a better way */
  protected static String months[] = {
    "January", "February", "March", "April", "May", "June", "July", "August",
    "September", "October", "November", "December"
  };
}