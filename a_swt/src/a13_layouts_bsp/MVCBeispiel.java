package a13_layouts_bsp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

public class MVCBeispiel {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		String[] toolkits = new String[] { "AWT", "Swing", "SWT/JFace" };

		init(shell, toolkits);

		shell.pack();
		shell.open();
		// textUser.forceFocus();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

	private static void init(Shell shell, String[] toolkits) {
		shell.setLayout(new GridLayout());
		shell.setText("Java UI Toolkit List");

		List list = new List(shell, SWT.MULTI | SWT.BORDER);
		for (int i = 0; i < toolkits.length; i++)
			list.add(toolkits[i]);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		list.setLayoutData(gridData);
	}

}
