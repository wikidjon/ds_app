package a13_layouts_bsp;

// hier start

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public final class a20_border3 {

	public int spacing = 3;

	public static void main(String[] arguments) {
		Display display = new Display();
		final Shell shell = new Shell();
		shell.setText("Layout Demo");
		shell.setLayout(new BorderLayout());

		/*
		 * SelectionListener l = new SelectionAdapter() { public void
		 * widgetSelected(SelectionEvent e) { Control c = (Control)
		 * e.getSource(); Composite p = c.getParent(); c.dispose(); p.layout();
		 * } };
		 */
		Composite composite1 = new Composite(shell, SWT.NONE);
		composite1.setLayoutData("NORTH");
		FillLayout fillLayout = new FillLayout();
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		composite1.setLayout(fillLayout);

		// ***************Menue oben anzeigen *******************
		a20_menue2 men = new a20_menue2();
		men.menue2(shell, display, composite1);

		final ToolBar toolbar = new ToolBar(composite1, SWT.FLAT);
		toolbar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");

		shell.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Rectangle r = shell.getClientArea();
				toolbar.setBounds(r.x, r.y, r.width, toolbar.computeSize(
						SWT.DEFAULT, SWT.DEFAULT).y);
			}
		});

		// ***************Gruppe unten anzeigen *******************
		SashForm form = new SashForm(shell, SWT.HORIZONTAL);
		form.setLayout(new FillLayout());

		Composite composite2 = new Composite(shell, SWT.NONE);
		composite2.setLayoutData("SOUTH");
		/*
		 * final Label label1 = new Label(composite2,SWT.NONE);
		 * label1.setText("hier k�nnen andere Widgets stehen ");
		 * 
		 * // final Label label = new Label(composite2, SWT.BORDER);
		 * //************** Listener armListener = new Listener() { public void
		 * handleEvent(Event event) { MenuItem item = (MenuItem) event.widget;
		 * label1.setText(item.getText()); label1.update(); } };
		 */

		Group group2 = new Group(composite2, SWT.NONE);
		group2.setBounds(5, 5, 200, 40);
		group2.setText("Gruppe SOUTH");

		// ***************Tabelle in der Mitte anzeigen *******************
		Composite composite3 = new Composite(form, SWT.NONE);
		composite3.setLayoutData("CENTER");
		composite3.setLayout(new BorderLayout());

		a20_table tab = new a20_table();
		tab.createTable(composite3);

		Composite composite4 = new Composite(shell, SWT.BORDER);
		composite4.setLayoutData("EAST");

		// *****************************************************************
		// zum EAST neues FILL-Layout hinzufuegen - geschachtelte Layouts
		FillLayout fillLayout2 = new FillLayout(SWT.VERTICAL);
		composite4.setLayout(fillLayout2);

		Button button0 = new Button(composite4, SWT.PUSH);
		button0.setText("button0");
		Button button1 = new Button(composite4, SWT.PUSH);
		button1.setText("button1");
		Button button2 = new Button(composite4, SWT.PUSH);
		button2.setText("button2");
		Button button3 = new Button(composite4, SWT.PUSH);
		button3.setText("button3");

		// *******************************************************************
		// Menue im linken Bildschirm anfuegen
		Composite composite5 = new Composite(shell, SWT.NONE);
		composite5.setLayoutData("WEST");

		final Tree tree = new Tree(composite5, SWT.NONE);
		tree.setSize(100, 250); // 150, 250
		tree.setLocation(5, 5);

		TreeItem ebene1 = new TreeItem(tree, SWT.NONE);
		ebene1.setText("Oma");
		TreeItem ebene1a = new TreeItem(tree, SWT.NONE);
		ebene1a.setText("Opa");
		TreeItem ebene1b = new TreeItem(tree, SWT.NONE);
		ebene1b.setText("Weihnachtsmann");
		TreeItem ebene1c = new TreeItem(tree, SWT.NONE); // ebene1
		ebene1c.setText("Hausfreund");
		TreeItem ebene1d = new TreeItem(tree, SWT.NONE); // ebene1
		ebene1d.setText("AAAAAA");
		// zweite Ebene
		TreeItem ebene12 = new TreeItem(ebene1, SWT.NONE, 0);
		ebene12.setText("Tochter");
		TreeItem ebene120 = new TreeItem(ebene1, SWT.NONE, 1);
		ebene120.setText("Sohn");
		TreeItem ebene121 = new TreeItem(ebene1b, SWT.NONE, 0);
		ebene121.setText("Weihnachtsmaennchen");
		TreeItem ebene122 = new TreeItem(ebene1a, SWT.NONE, 0);
		ebene122.setText("(Freundin!!!)");

		TreeItem ebene130 = new TreeItem(ebene12, SWT.NONE);
		ebene130.setText("Enkelsohn");

		tree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				TreeItem[] t = tree.getSelection();
				System.out.print("Auswahl: ");
				for (int i = 0; i < t.length; i++) {
					System.out.print(t[i].getText() + ", ");
					// MenuItem item = (MenuItem) event.widget;
					// label1.setText(t[i].getText());
					// label1.update();

				}
				System.out.println();
			}
		});

		tree.addTreeListener(new TreeListener() {
			public void treeCollapsed(TreeEvent e) {
				System.out.println("Tree collapsed.");
			}

			public void treeExpanded(TreeEvent e) {
				System.out.println("Tree expanded.");
			}
		});

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static Image createImage(String name) {
		return new Image(Display.getCurrent(), a04_Menue3.class
				.getResourceAsStream("../icons/" + name + ".gif"));
	}

}