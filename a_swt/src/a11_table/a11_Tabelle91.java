package a11_table;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class a11_Tabelle91 {

	public static void main(String[] args) {
		Display d = new Display();
		Shell s = new Shell(d);

		s.setSize(250, 200);
		s.setImage(new Image(d, "src/images/welcome.gif"));
		s.setText("Tabellen Shell Beispiel");
		GridLayout gl = new GridLayout();
		gl.numColumns = 4;
		s.setLayout(gl);

		final Table t = new Table(s, SWT.BORDER | SWT.CHECK | SWT.MULTI | SWT.FULL_SELECTION);
		final GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 4;
		t.setLayoutData(gd);
		t.setHeaderVisible(true);
		final TableColumn tc1 = new TableColumn(t, SWT.LEFT);
		final TableColumn tc2 = new TableColumn(t, SWT.CENTER);
		final TableColumn tc3 = new TableColumn(t, SWT.CENTER);
		tc1.setText("Vorname");
		tc2.setText("Nachname");
		tc3.setText("Addresse");
		tc1.setWidth(70);
		tc2.setWidth(70);
		tc3.setWidth(80);
		final TableItem item1 = new TableItem(t, SWT.NONE);
		item1.setText(new String[] { "Eric", "Clapton", "Halle" });
		final TableItem item2 = new TableItem(t, SWT.NONE);
		item2.setText(new String[] { "Ennio", "Morricone", "Zeitz" });
		final TableItem item3 = new TableItem(t, SWT.NONE);
		item3.setText(new String[] { "Charles", "Bronson", "Potsdam" });

		final Text find = new Text(s, SWT.SINGLE | SWT.BORDER);
		final Text ersetzen = new Text(s, SWT.SINGLE | SWT.BORDER);
		final Button replaceButton = new Button(s, SWT.PUSH);
		replaceButton.setText("Ersetzen");
		replaceButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem[] tabit = t.getItems();
				for (int i = 0; i < tabit.length; i++) {
					if (tabit[i].getText(2).equals(find.getText())) {
						tabit[i].setText(2, ersetzen.getText());
					}

				}
			}
		});

		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
