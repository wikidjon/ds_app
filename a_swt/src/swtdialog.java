import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;


public class swtdialog extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text text;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public swtdialog(Shell parent, int style) {
		super(parent, style);
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		this.shell = new Shell(getParent(), getStyle());
		this.shell.setSize(450, 300);
		this.shell.setText(getText());

		Button btnNewButton = new Button(this.shell, SWT.NONE);
		btnNewButton.setBounds(10, 97, 75, 25);
		btnNewButton.setText("hallo");

		this.text = new Text(this.shell, SWT.BORDER);
		this.text.setBounds(233, 36, 76, 21);

		Label lblHfgffgh = new Label(this.shell, SWT.NONE);
		lblHfgffgh.setBounds(181, 39, 55, 15);
		lblHfgffgh.setText("hfgffgh");

		Button btnRadioButton = new Button(this.shell, SWT.RADIO);
		btnRadioButton.setBounds(164, 138, 90, 16);
		btnRadioButton.setText("Radio Button");

		Button btnRadioButton_1 = new Button(this.shell, SWT.RADIO);
		btnRadioButton_1.setBounds(282, 138, 90, 16);
		btnRadioButton_1.setText("Radio Button");

		Menu menu_1 = new Menu(this.shell);
		this.shell.setMenu(menu_1);

		MenuItem mntmBbb_1 = new MenuItem(menu_1, SWT.NONE);
		mntmBbb_1.setText("bbb");

		Composite composite = new Composite(this.shell, SWT.NONE);
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_YELLOW));
		composite.setBounds(10, 10, 408, 252);

		Menu menu = new Menu(composite);
		composite.setMenu(menu);

		MenuItem mntmAaaaa = new MenuItem(menu, SWT.NONE);
		mntmAaaaa.setText("aaaaa");

		MenuItem mntmBbb = new MenuItem(menu, SWT.NONE);
		mntmBbb.setText("bbb");

	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		this.shell.open();
		this.shell.layout();
		Display display = getParent().getDisplay();
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return this.result;
	}
}
