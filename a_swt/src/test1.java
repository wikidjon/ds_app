import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;


public class test1 {

	static Composite testCell;
	static int i=1;

	//******************geaendert ************************
	private static void changeText(Shell shell, Label l, int i) {

		//******************neu ************************
		if (i % 2 == 0){
			l.setText("Neuer Text sdas");}
		else{
			l.setText("Alter Text");}
		l.pack();
		shell.layout();
	}

	private static Label createLabel( Composite parent) {
		Label label = new Label(parent, SWT.NONE);
		label.setAlignment(SWT.LEFT);

		label.setLayoutData( new GridData( SWT.CENTER, SWT.CENTER, false, false) );
		return label;
	}


	public static void main(String[] args) {
		Display display = new Display();
		final Shell shell = new Shell(display);

		GridData gridData = new GridData();
		/********		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		gridData.horizontalAlignment = GridData.FILL;
		 *****/		gridData.verticalAlignment = GridData.FILL;
		 shell.setLayout(new GridLayout(2,true));  //false

		 testCell = new Composite(shell,SWT.NONE);
		 testCell.setLayout(new GridLayout());

		 final Label l = createLabel(testCell);
		 l.setSize(400, 220);
		 l.pack();
		 l.setText("Alter Text");

		 //		l.setLayoutData(gridData);


		 Composite btnCell = new Composite(shell,SWT.NONE);
		 btnCell.setLayout(new GridLayout());
		 Button b = new Button(btnCell, SWT.PUSH);
		 b.setText("Aendern");
		 b.addListener(SWT.MouseDown, new Listener() {
			 @Override
			 public void handleEvent(Event e) {
				 //******************geaendert ************************
				 changeText(shell, l, i++);
				 // shell.layout();
			 }
		 });

		 shell.open();
		 shell.pack();

		 while (!shell.isDisposed()) {
			 if (!display.readAndDispatch()) {
				 display.sleep();
			 }
		 }

		 display.dispose();
	}
}