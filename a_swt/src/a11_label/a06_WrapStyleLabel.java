package a11_label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a06_WrapStyleLabel {

  public static void main(String[] args) {
    final Display display = new Display();
    Shell shell = new Shell(display);

    String text = "This is a long long long long long long long text.";
    
    Label labelNoWrap = new Label(shell, SWT.BORDER);
    labelNoWrap.setText(text);
    labelNoWrap.setBounds(10, 10, 100, 100);
    
    Label labelWrap = new Label(shell, SWT.WRAP | SWT.BORDER);
    labelWrap.setText(text);
    labelWrap.setBounds(120, 10, 100, 100);
      
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
  }
}