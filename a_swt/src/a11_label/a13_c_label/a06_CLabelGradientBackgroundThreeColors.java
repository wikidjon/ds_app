package a11_label.a13_c_label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a06_CLabelGradientBackgroundThreeColors {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setText("CLabel Gradient");

    shell.setLayout(new GridLayout(1, false));

    CLabel one = new CLabel(shell, SWT.LEFT);
    one.setText("Second Gradient Example");
    one.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

    // Set the background gradient
    one.setBackground(new Color[] { shell.getDisplay().getSystemColor(SWT.COLOR_WHITE),
        shell.getDisplay().getSystemColor(SWT.COLOR_GRAY),
        shell.getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY),
        shell.getDisplay().getSystemColor(SWT.COLOR_BLACK) }, new int[] { 33, 67, 100 });


    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}