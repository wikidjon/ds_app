package a11_label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a05_LabelSetImage {
  public static void main(String[] args) {
    final Display display = new Display();
    final Shell shell = new Shell(display, SWT.SHELL_TRIM);
    shell.setLayout(new RowLayout());

    Label label = new Label(shell, SWT.BORDER );
    label.setText("text on the label");

    label.setImage(new Image(display,"src/icons/swt.png"));
    shell.open();
    // Set up the event loop.
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        // If no more entries in event queue
        display.sleep();
      }
    }
    display.dispose();
  }

}