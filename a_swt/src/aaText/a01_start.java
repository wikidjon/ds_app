package aaText;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
//import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

//import a13_layouts1.a_menue;

public class a01_start {

public static void main(String[] args) {

	final Display display = new Display();
	final int zaehler = 5;
	final Image image = new Image(display, "src/images/splash.jpg");
	final Shell splash = new Shell(display);
	splash.setLayout(new FillLayout());
	
	final ProgressBar bar = new ProgressBar(splash, SWT.NONE);
	bar.setMaximum(zaehler);
	
	Label label = new Label(splash, SWT.NONE);
	label.setImage(image);
	
	FormLayout layout = new FormLayout();
	splash.setLayout(layout);
	
	FormData labelData = new FormData ();
	labelData.right = new FormAttachment (100, 0);
	labelData.bottom = new FormAttachment (100, 0);
	label.setLayoutData(labelData);
	
	FormData progressData = new FormData ();
	progressData.left = new FormAttachment (0, 5);
	progressData.right = new FormAttachment (100, -5);
	progressData.bottom = new FormAttachment (100, -5);
	bar.setLayoutData(progressData);
	
	//splash.pack();	
	splash.setSize(300,200);
	Rectangle splashRect = splash.getBounds();
	Rectangle displayRect = display.getBounds();
	//Positionierung in der Mitte des BS
	int x = (displayRect.width - splashRect.width) / 2;
	int y = (displayRect.height - splashRect.height) / 2;
	splash.setLocation(x, y);
	splash.open();
	
	Shell  shell = new Shell(display);
//	shell.setImage(createImage("logo"));
//	shell.setSize (300, 300);
	shell.setLayout(new FillLayout());
//	shell.setLayout(new GridLayout());
	a_menue men = new a_menue();
	men.menue(shell, display);

		GridLayout l = new GridLayout();
		l.marginWidth = 0;
		l.marginHeight = 0;
		l.horizontalSpacing = 0;
		l.verticalSpacing = 0;
		shell.setLayout(l);

	//new a10_Table().tabelle(shell,display);

//a04_notepad men = new a04_notepad();
//	men.start();
//	new a04_notepad().run();
	
//	a30_Notepad2 men = 	new a30_Notepad2();
//	men.start();
//	.start();
//	men.menue(shell);
//	new a30_Notepad2().run();
//	men.run();
	
	// Verzoegerung beim Slash
	for (int i=0; i<zaehler; i++) {
		bar.setSelection(i+1);
		try {Thread.sleep(100);} catch (Throwable e) {}
	}

	splash.close();
	image.dispose();
	shell.open();
	
	while(!shell.isDisposed())
	     if (!display.readAndDispatch())
	           display.sleep();
	display.dispose();
	label.dispose();
	}
}


