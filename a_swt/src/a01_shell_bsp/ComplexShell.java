package a01_shell_bsp;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class ComplexShell {

	public static void main(String[] args) {
		Display d = new Display();
		Shell s = new Shell(d);
		s.setSize(250,275);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Ein Shell Composite Beispiel");

		GridLayout gl = new GridLayout();
		gl.numColumns=3;
		s.setLayout(gl);

		//Inhalt
		GridComposite gc = new GridComposite(s);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 4;
		gc.setLayoutData(gd);
		gd = new GridData();

		RowComposite rc = new RowComposite(s);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		rc.setLayoutData(gd);

		s.open();
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
