package a01_shell_bsp;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class CompositeShell1 {

	public static void main(String[] args) {

		Display d = new Display();
		Shell s = new Shell(d);
		s.setSize(500,500);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Ein Shell Composite Beispiel");

		Composite c = new Composite(s,SWT.BORDER);
		c.setBounds(100,50,170,150);
		c.setBackground(new Color(d,31,133,31));
		// Achtung !!! Widerspruch zu c.setBounds...
		s.setLayout(new FillLayout());

		s.open();
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
