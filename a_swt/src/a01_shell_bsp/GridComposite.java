package a01_shell_bsp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class GridComposite extends Composite {
	static Display d;
	static Shell s;

	public static void main(final String[] args) {

		d = new Display();
		s = new Shell(d);
		s.setSize(250, 275);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Ein Shell Composite Beispiel");

		final Composite c = new Composite(s, SWT.NO_FOCUS);
		new GridComposite(c);
		c.setBackground(new Color(d,31,133,31));

		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}

	public GridComposite(final Composite c) {

		super(c, SWT.BORDER);
		final GridLayout gl = new GridLayout();
		gl.numColumns = 3;
		this.setLayout(gl);
		final Label l1 = new Label(this, SWT.BORDER);
		l1.setText(" Spalte Eins ");
		// *************************************************************
		l1.setBackground(new Color(d, 150, 150, 150));
		// *************************************************************
		final Label l2 = new Label(this, SWT.BORDER);
		l2.setText(" Spalte Zwei ");
		final Label l3 = new Label(this, SWT.BUTTON1);
		l3.setText(" Spalte Drei ");

		final Text t1 = new Text(this, SWT.SINGLE | SWT.BORDER);
		final Text t2 = new Text(this, SWT.SINGLE | SWT.BORDER);
		final Text t3 = new Text(this, SWT.SINGLE | SWT.BORDER);
		final Text t4 = new Text(this, SWT.SINGLE | SWT.BORDER);
		final Text t5 = new Text(this, SWT.SINGLE | SWT.BORDER);
		final Text t6 = new Text(this, SWT.SINGLE | SWT.BORDER);

		GridData gd = new GridData();
		gd.horizontalAlignment = GridData.CENTER;
		l1.setLayoutData(gd);

		gd = new GridData();
		gd.horizontalAlignment = GridData.CENTER;
		l2.setLayoutData(gd);

		gd = new GridData();
		gd.horizontalAlignment = GridData.CENTER;
		l3.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		t1.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		t2.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		t3.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		t4.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		t5.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		t6.setLayoutData(gd);

	}

}
