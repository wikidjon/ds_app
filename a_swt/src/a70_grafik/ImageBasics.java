package a70_grafik;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * 
 */
public class ImageBasics {
	Display display = new Display();
	Shell shell = new Shell(display);

	public ImageBasics() {
		Image image = new Image(shell.getDisplay(), "icons/icon.gif");
		shell.setImage(image);

		shell.pack();
		shell.open();
		//textUser.forceFocus();

		// Set up the event loop.
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

		display.dispose();
	}

	@SuppressWarnings("unused")
	private void init() {

	}

	public static void main(String[] args) {
		new ImageBasics();
	}
}
