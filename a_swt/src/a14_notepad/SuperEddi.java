package a14_notepad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font; //import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;

public class SuperEddi {

	Display display = new Display();
	Shell shell = new Shell(display);

	StyledText text;
	boolean ungesicherteInhalte;
	File file;
	private String lastOpenDirectory;
	public static final String APP_NAME = "SuperEddi v2.0";
	MenuItem miWrap = null;

	ImageRegistry imageRegistry = new ImageRegistry();

	public static void main(String[] args) {
		new SuperEddi();
	}

	public SuperEddi() {

		// ***********************************************
		Action actionNew = new Action("&Neu", ImageDescriptor.createFromFile(
				null, "src/icons/new.gif")) {
			public void run() {
				if (speichernVorEnde()) {
					file = null;
					text.setText("");
				}
			}
		};
		actionNew.setAccelerator(SWT.CTRL + 'N');

		// ***********************************************
		Action actionOpen = new Action("&Oeffnen", ImageDescriptor
				.createFromFile(null, "src/icons/open.gif")) {
			public void run() {
				if (speichernVorEnde())
					loadTextFromFile();
			}
		};
		actionOpen.setAccelerator(SWT.CTRL + 'O');

		// ***********************************************
		Action actionSave = new Action("&Speichern\tCtrl+S", ImageDescriptor
				.createFromFile(null, "src/icons/save.gif")) {
			public void run() {
				saveTextToFile();
			}
		};
		actionSave.setAccelerator(SWT.CTRL + 'S');

		// ***********************************************
		Action actionCopy = new Action("&Kopieren", ImageDescriptor
				.createFromFile(null, "src/icons/copy.gif")) {
			public void run() {
				text.copy();
			}
		};
		actionCopy.setAccelerator(SWT.CTRL + 'C');

		// Separator.

		// ***********************************************
		Action actionCut = new Action("&Ausschneiden", ImageDescriptor
				.createFromFile(null, "src/icons/cut.gif")) {
			public void run() {
				text.cut();
			}
		};
		actionCut.setAccelerator(SWT.CTRL + 'X');

		// ***********************************************
		Action actionPaste = new Action("&Einfuegen", ImageDescriptor
				.createFromFile(null, "src/icons/paste.gif")) {
			public void run() {
				text.paste();
			}
		};
		actionPaste.setAccelerator(SWT.CTRL + 'P');

		// Separator.

		// ***********************************************
		Action actionWrap = new Action("&Umbruch", IAction.AS_CHECK_BOX) {
			public void run() {
				text.setWordWrap(isChecked());
			}
		};
		actionWrap.setAccelerator(SWT.CTRL + 'W');

		// ***********************************************
		Action actionExit = new Action("&Exit@Ctrl+X") {
			public void run() {
				if (speichernVorEnde())
					shell.dispose();
			}
		};

		System.out.println(actionWrap.getText());

		// Toolbar anfuegen
		ToolBar toolBar = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);
		ToolBarManager toolBarManager = new ToolBarManager(toolBar);

		toolBarManager.add(actionNew);
		toolBarManager.add(actionOpen);
		toolBarManager.add(actionSave);
		toolBarManager.add(new Separator());
		toolBarManager.add(actionCopy);
		toolBarManager.add(actionCut);
		toolBarManager.add(actionPaste);

		toolBarManager.add(new Separator());
		toolBarManager.add(actionWrap);
		toolBarManager.update(true);

		shell.setLayout(new GridLayout());

		System.out.println("Client area: " + shell.getClientArea());

		text = new StyledText(shell, SWT.MULTI | SWT.WRAP | SWT.BORDER
				| SWT.H_SCROLL | SWT.V_SCROLL);
		text.setLayoutData(new GridData(GridData.FILL_BOTH));

		Font font = new Font(shell.getDisplay(), "Courier New", 10, SWT.NORMAL);
		text.setFont(font);

		text.setText("SuperEddi Version Erlkoenig ");
		text.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				ungesicherteInhalte = true;
			}
		});

		// Menues
		MenuManager barMenuManager = new MenuManager();
		MenuManager fileMenuManager = new MenuManager("&Datei");
		MenuManager editMenuManager = new MenuManager("&Editor");
		MenuManager formatMenuManager = new MenuManager("&Format");

		barMenuManager.add(fileMenuManager);
		barMenuManager.add(editMenuManager);
		barMenuManager.add(formatMenuManager);

		fileMenuManager.add(actionNew);
		fileMenuManager.add(actionOpen);
		fileMenuManager.add(actionSave);
		fileMenuManager.add(new Separator());

		fileMenuManager.add(actionExit);

		editMenuManager.add(actionCopy);
		editMenuManager.add(actionCut);
		editMenuManager.add(actionPaste);

		formatMenuManager.add(actionWrap);

		// Add the menu bar to the shell.
		// shell.setMenuBar(menuBar);
		barMenuManager.updateAll(true);
		shell.setMenuBar(barMenuManager.createMenuBar((Decorations) shell));

		shell.setSize(400, 200);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	/**
	 * Retrieves the image corresponding to the given file name. Note that the
	 * image is managed by an image registry. You should not dispose the image
	 * after use.
	 */
	/*
	 * @SuppressWarnings("unused") private Image getImage(String shortFileName)
	 * { if (imageRegistry.getDescriptor(shortFileName) == null) {
	 * ImageDescriptor descriptor = ImageDescriptor.createFromFile(null,
	 * "icons/" + shortFileName); imageRegistry.put(shortFileName, descriptor);
	 * } return imageRegistry.get(shortFileName); }
	 */
	/**
	 * Speichern vor Beenden
	 */
	boolean speichernVorEnde() {
		if (!ungesicherteInhalte)
			return true;

		MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING
				| SWT.YES | SWT.NO | SWT.CANCEL);
		messageBox.setMessage("Moechten Sie Ihre Aenderungen sichern "
				+ (file == null ? "in einer Datei?" : file.getName()));
		messageBox.setText(APP_NAME);
		int ret = messageBox.open();
		if (ret == SWT.YES) {
			return saveTextToFile();
		} else if (ret == SWT.NO) {
			return true;
		} else {
			return false;
		}
	}

	// **************************************************************
	boolean loadTextFromFile() {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		if (lastOpenDirectory != null)
			dialog.setFilterPath(lastOpenDirectory);

		String selectedFile = dialog.open();
		if (selectedFile == null) {
			log("Aktion abgebrochen: Laden einer Textdatei");
			return false;
		}

		file = new File(selectedFile);
		lastOpenDirectory = file.getParent();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			StringBuffer sb = new StringBuffer();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\r\n");
			}
			text.setText(sb.toString());
			return true;
		} catch (IOException e) {
			log("Fehler beim Lesen von Datei : " + file);
			log(e.toString());
		}
		return false;
	}

	/*********************************************
	 * Sichern des Styled Text in Datei. Anzeige eines FileDialogs
	 ************************************************/
	boolean saveTextToFile() {
		if (file == null) {
			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			if (lastOpenDirectory != null)
				dialog.setFilterPath(lastOpenDirectory);

			String selectedFile = dialog.open();
			if (selectedFile == null) {
				log("Aktion abgebrochen: Laden einer Textdatei");
				return false;
			}

			file = new File(selectedFile);
			lastOpenDirectory = file.getParent();
		}

		try {
			FileWriter writer = new FileWriter(file);
			writer.write(text.getText());
			writer.close();
			log("Text wurde gesichert in Datei: " + file);
			ungesicherteInhalte = false;
			return true;

		} catch (IOException e) {
			log("Fehler beim Schreiben in Datei: " + file);
			log(e.toString());
		}
		return false;
	}

	void log(String message) {
		System.out.println(message);
	}
	/*
	 * public static void main(String[] args) { new SuperEddi(); }
	 */
}
