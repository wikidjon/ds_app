package a14_notepad;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;


public class a30_Notepad1 {

	private Display display;
	private Shell shell;
	private Text text;

	private String merkePfad;
	private String sicherPfad;

	public a30_Notepad1() {
		createDisplay();
		createShell();
		createMenu();
		createWidgets();
		fileNew();
	}

	private void createDisplay() {
		display = new Display();
	}

	private void createShell() {
		shell = new Shell(display);
		shell.setLayout(new FillLayout());
	}

	private void createMenu() {
		SelectionListener listener = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				a30_Notepad1.this.menuSelected(e);
			}
		};
		Menu menubar = new Menu(shell, SWT.BAR);
		Menu menu;
		MenuItem item;

		// Menue bauen
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Neu\tStrg+N");
		item.setAccelerator(SWT.CTRL + 'N');
		item.setData("neu");
		item.addSelectionListener(listener);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...\tStrg+O");
		item.setAccelerator(SWT.CTRL + 'O');
		item.setData("oeffnen");
		item.addSelectionListener(listener);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(listener);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Speichern &unter...");
		item.setData("speichernunter");
		item.addSelectionListener(listener);

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Beenden");
		item.setData("beenden");
		item.addSelectionListener(listener);

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		// Edit Menue
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&R�ckgangig\tStrg+Z");
		item.setAccelerator(SWT.CTRL + 'Z');
		item.setData("undo");
		item.addSelectionListener(listener);

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Ausschneiden\tStrg+X");
		item.setAccelerator(SWT.CTRL + 'X');
		item.setData("ausschneiden");
		item.addSelectionListener(listener);
		final MenuItem cutItem = item;

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Kopieren\tStrg+C");
		item.setAccelerator(SWT.CTRL + 'C');
		item.setData("kopieren");
		item.addSelectionListener(listener);
		final MenuItem copyItem = item;

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("E&inf�gen\tStrg+V");
		item.setAccelerator(SWT.CTRL + 'V');
		item.setData("einfuegen");
		item.addSelectionListener(listener);
		final MenuItem pasteItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Bearbeiten");
		item.setMenu(menu);

		// Edit Menu enable/disable Items
		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				// ermitteln, ob Text ausgew�hlt wurde
				boolean auswahl = text.getSelectionCount() > 0;
				cutItem.setEnabled(auswahl);
				copyItem.setEnabled(auswahl);

				// ermitteln, ob das Clipboard Text enthaelt
				Clipboard c = new Clipboard(display);
				Object o = c.getContents(TextTransfer.getInstance());
				//Plaintext zu plattformspezifischem Text
				c.dispose();
				pasteItem.setEnabled(o != null);
			}
		});

		shell.setMenuBar(menubar);
	}
	
	//*******Textarea anlegen*****************************
	private void createWidgets() {   
		// Standardfont (Windows-spezifisch) festlegen
		final Font font = new Font(display, "Lucida Console", 10, SWT.NORMAL);
		text = new Text(shell, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		text.setFont(font);
		text.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				font.dispose();
			}
		});
	}

	private void menuSelected(SelectionEvent e) {
		Object auswahl = ((Item) e.getSource()).getData();

		if (auswahl == "neu") {
			fileNew();
		} else if (auswahl == "oeffnen") {
			fileOpen();
		} else if (auswahl == "speichern") {
			fileSave(false);
		} else if (auswahl == "speichernunter") {
			fileSave(true);
		} else if (auswahl == "beenden") {
			fileExit();
		} else if (auswahl == "undo") {
			editUndo();
		} else if (auswahl == "ausschneiden") {
			text.cut();
		} else if (auswahl == "kopieren") {
			text.copy();
		} else if (auswahl == "einfuegen") {
			text.paste();
		}
	}

	private void fileNew() {
		text.setText("");
		shell.setText("Unbenannt - Notepad");
		sicherPfad = null;
	}

	private void fileOpen() {
		String path = requestFile(SWT.OPEN);
		if (path != null) {
			loadFile(path);
			merkePfad = path;
			sicherPfad = path;
			shell.setText(new File(path).getName() + " - Notepad");
		}
	}

	private String requestFile(int style) {
		FileDialog d = new FileDialog(shell, style);
		d.setFilterPath(merkePfad);
		d.setFilterNames(
			new String[] { "Textdateien (*.txt)", "Alle Dateien" });
		d.setFilterExtensions(new String[] { "*.txt", "*" });
		return d.open();
	}

	private void loadFile(String path) {
		try {
			StringBuffer sb = new StringBuffer(16000);
			BufferedReader r = new BufferedReader(new FileReader(path));
			String line;
			while ((line = r.readLine()) != null) {
				sb.append(line).append(Text.DELIMITER);
			}
			r.close();
			text.setText(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void fileSave(boolean ask) {
		if (ask || sicherPfad == null) {
			String path = requestFile(SWT.SAVE);
			if (path != null) {
				if (path.indexOf('.') == -1) {
					path += ".txt";
				}
				saveFile(path);
				merkePfad = path;
				sicherPfad = path;
				shell.setText(new File(path).getName() + " - Notepad");
			}
		} else {
			saveFile(sicherPfad);
		}
	}

	private void saveFile(String path) {
		try {
			PrintWriter w = new PrintWriter(new FileWriter(path));
			String s = text.getText();
			int i = s.indexOf(Text.DELIMITER);
			int j = 0;
			while (i != -1) {
				w.println(s.substring(j, i));
				j = i + Text.DELIMITER.length();
				i = s.indexOf(Text.DELIMITER, j);
			}
			w.print(s.substring(j));
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void fileExit() {
		shell.close();
	}
	private void editUndo() {
		MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
		mb.setText("Notepad");
		mb.setMessage("Funktion ist nicht implementiert");
		mb.open();
	}

	public void run() {
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	public static void main(String[] arguments) {
		new a30_Notepad1().run();
	}
}
