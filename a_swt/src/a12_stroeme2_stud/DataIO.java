package a12_stroeme2_stud;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


class DataIO {
	/***************************************************************************************
	 * da die Gr�sse des Datensatzes konstant sein muss, werden die DatenS�tze
	 * mit * den F-Methoden auf die gleiche L�nge gebracht *
	 ****************************************************************************************/
	public static String readFString(int size, DataInput in) throws IOException {
		StringBuffer b = new StringBuffer(size);
		int i = 0;
		boolean mehr = true;
		while (mehr && i < size) {
			char ch = in.readChar();
			i++;
			if (ch == 0)
				mehr = false;
			else
				b.append(ch);
		}
		in.skipBytes(2 * (size - i));
		return b.toString();
	}

	// **************************************************************************************
	public static void writeFString(String s, int size, DataOutput out)
			throws IOException {
		int i;
		for (i = 0; i < size; i++) {
			char ch = 0;
			if (i < s.length())
				ch = s.charAt(i);
			out.writeChar(ch);
		}
	}
}
