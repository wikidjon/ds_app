package a02a_daba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class a02db_textarea {

	static Connection myConnection = null;
	static String vn;
	static int x, y = 10, i;
	static int zeilenZaehler = 64, spaltenZaehler = 4;

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		FillLayout fillLayout = new FillLayout (SWT.VERTICAL);
		shell.setLayout (fillLayout);

		final Table table1 = new Table(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		//table1.setBounds(x, y, 270, 60);
		table1.setLinesVisible(true);
		table1.setHeaderVisible(true);

		TableColumn gruppe = new TableColumn(table1, SWT.LEFT);
		gruppe.setText("Gruppe");
		gruppe.setWidth(200);
		TableColumn kenn = new TableColumn(table1, SWT.RIGHT);
		kenn.setText("Kennung");
		kenn.setWidth(100);
		TableColumn titel = new TableColumn(table1, SWT.LEFT);
		titel.setText("Titel");
		titel.setWidth(200);

		Connection myConnection = null;
		// DB test ist unter MySql eingerichtet - und beinhaltet cd_liste
		String url = "jdbc:mysql://localhost:3306/test?";
		String driverClass = "org.gjt.mm.mysql.Driver";
		String user = "";
		String password = "";

		// ********************************
		try {
			// Treiber laden.
			Class.forName(driverClass).newInstance();
			// Verbindung aufbauen.
			myConnection = DriverManager.getConnection(url, user, password);

			String query = "SELECT Gruppe, Titel FROM cd_liste";
			Statement stmt = myConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				String vn = rs.getString("Gruppe");
				String nn = rs.getString("Titel");
				TableItem item1 = new TableItem(table1, SWT.NONE);
				item1.setText(new String[] { vn, "1", nn });

				System.out.println(vn + " --  " + nn + "-" + i);
			}

			rs.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		/**************************
		Button ok = new Button(shell, SWT.PUSH);
		ok.setText("OK");
		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println("OK1");
				i = 1;
			}
		});
		Button cancel = new Button(shell, SWT.PUSH);
		cancel.setText("Abbruch");
		cancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Cancel");
			}
		});
		 *************************************/
		//Berechnet Groesse der Tabelle
		//Point size = table1.computeSize(SWT.DEFAULT, 200); //SWT.DEFAULT
		//table1.setSize(size);

		//Position des Cursors in Tabelle
		table1.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Point pt = new Point(event.x, event.y);
				TableItem item = table1.getItem(pt);
				if (item == null)
					return;
				for (int i = 0; i < spaltenZaehler; i++) {
					Rectangle rect = item.getBounds(i);
					if (rect.contains(pt)) {
						int index = table1.indexOf(item);
						table1.getData();
						System.out.println("Eintrag " + index);
					}
				}
			}
		});

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
