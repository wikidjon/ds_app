package a02a_daba;

//Einfacher ODBC/JDBC ZUgriff auf Access DB

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class a02_acc_db_zugriff1 {

	public static void main(String[] args) {
		/** H�lt die Datenbankverbindung */
		Connection myConnection = null;
		String url = "jdbc:odbc:toolsys";
		// String driverClass = "com.ms.jdbc.odbc.JdbcOdbcDriver";
		String driverClass = "sun.jdbc.odbc.JdbcOdbcDriver";
		String user = "";
		String password = "";

		try {
			// Treiber laden.
			Class.forName(driverClass);
			// Verbindung aufbauen
			myConnection = DriverManager.getConnection(url, user, password);

			// SQL Statement aufbauen
			String query = "SELECT VName, NName FROM Teilnehmer";
			Statement stmt = myConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				String vn = rs.getString("VName");
				String nn = rs.getString("NName");
				System.out.println(vn + "  " + nn);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

}
