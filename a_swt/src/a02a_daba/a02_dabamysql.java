package a02a_daba;
//aktuelle Mysql DaBa
//D:\laufwerk_c\MySql\mysql-5.5.28-win32\bin>

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

//import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.Shell;

public class a02_dabamysql {

	public static void main(final String[] args) {

		//		Display display = new Display();
		//		Shell shell = new Shell(display);

		Connection myConnection = null;
		// DB test ist unter MySql eingerichtet - und beinhaltet cd_liste
		final String url = "jdbc:mysql://localhost:3306/test?";
		final String driverClass = "com.mysql.jdbc.Driver";
		//com.mysql.jdbc.Driver
		final String user = "";
		final String password = "";

		try {
			// Treiber laden.
			Class.forName(driverClass).newInstance();
			// Verbindung aufbauen.
			myConnection = DriverManager.getConnection(url, user, password);
			//myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?");

			final String query = "SELECT Gruppe, Titel FROM cd_liste";
			final Statement stmt = myConnection.createStatement();
			final ResultSet rs = stmt.executeQuery(query);
			System.out.println("Resultset\n ");
			while (rs.next()) {
				final String vn = rs.getString("Gruppe");
				final String nn = rs.getString("Titel");
				// Formatierung wie in C ...!!!!!
				System.out.format("\nGruppe: %-40s -- Titel: %-20s  ", vn, nn);
				//System.out.format("\nGruppe: %20s", vn + "\t -- \t  " + nn);
			}
			rs.close();
		} catch (final Exception e) {
			System.out.println(":-( leider gibt es ein Problem: " + e.toString());
		}
		/*
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
		 */	}

}
