package a02a_daba;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a25_form_layout3<ResultSetIterator> {
	public static void main(String[] args) {
		// dabamysql daba = new dabamysql();
		Connection con = dabamysql.connection();
		new a25_form_layout3(con);

	}
	Display d;
	Shell s;

	Boolean b;

	a25_form_layout3(Connection con) {
		this.d = new Display();
		this.s = new Shell(this.d);
		this.s.setSize(250, 250);
		this.s.setImage(new Image(this.d, "src/icons/cut.gif"));
		this.s.setText("FormLayout Beispiel");
		this.s.setLayout(new FormLayout());
		FormData fd = new FormData();


		/**		final Label la1 = new Label(s, SWT.RIGHT);
		la1.setText("lfd. Nr.");
		fd = new FormData();
		fd.top = new FormAttachment(10, 10);
		fd.left = new FormAttachment(0, 10);
		fd.bottom = new FormAttachment(30, 0);
		fd.right = new FormAttachment(40, 0);
		la1.setLayoutData(fd);
		 **/

		final Label la2 = new Label(this.s, SWT.RIGHT);
		la2.setText("Gruppe");
		fd = new FormData();
		fd.top = new FormAttachment(10, 10);
		fd.left = new FormAttachment(0, 10);
		fd.bottom = new FormAttachment(30, 0);
		fd.right = new FormAttachment(40, 0);
		la2.setLayoutData(fd);

		final Label la3 = new Label(this.s, SWT.RIGHT);
		la3.setText("Titel");
		fd = new FormData();
		fd.top = new FormAttachment(la2, 30);
		fd.left = new FormAttachment(20, 10);
		fd.bottom = new FormAttachment(60, 0);
		fd.right = new FormAttachment(40, 0);
		la3.setLayoutData(fd);

		final Text t2 = new Text(this.s, SWT.BORDER | SWT.SINGLE);
		fd = new FormData();
		fd.top = new FormAttachment(la2, 0, SWT.TOP);
		fd.left = new FormAttachment(la2, 10);
		t2.setLayoutData(fd);

		final Text t3 = new Text(this.s, SWT.BORDER | SWT.SINGLE);
		fd = new FormData();
		fd.top = new FormAttachment(la3, 0, SWT.TOP);
		fd.left = new FormAttachment(la3, 10);
		t3.setLayoutData(fd);

		try {
			String query = "SELECT Gruppe, Titel FROM cd_liste";
			Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				//				ResultSetIterator rsi = new ResultSetIterator(rs);
				final String vn = rs.getString("Gruppe");
				final String nn = rs.getString("Titel");
				t2.setText(vn);
				t3.setText(nn);
				//Object ob = (Object[]) new Object();
				//ob[0]= rs.getRow();
				//	System.out.println(ob[0].toString()  );
				Button button = new Button(this.s, SWT.PUSH);
				button.setText("weiter >>");
				fd = new FormData();
				fd.top = new FormAttachment(la3, 20);
				fd.left = new FormAttachment(20, 10);
				fd.bottom = new FormAttachment(80, 0);
				fd.right = new FormAttachment(60, 0);
				button.setLayoutData(fd);

				button.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						t2.setText(vn);
						t3.setText(nn);
						/*
						try {
							rs.next();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						 */
						//	System.out.println(vn + " --  " + nn + "-");
					}
				});
				System.out.println(vn + " --  " + nn + "-");

			} // zu while

			rs.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		this.s.open();
		this.s.pack();
		while (!this.s.isDisposed()) {
			if (!this.d.readAndDispatch()) {
				this.d.sleep();
			}
		}
		this.d.dispose();
	}
}
