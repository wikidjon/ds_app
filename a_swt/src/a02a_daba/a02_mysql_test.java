package a02a_daba;

/****************************************************
 *Zugriff auf die MySQL Datenbank test
 *nur Kennung wird ausgelesen ...
 ***************************************************/

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class a02_mysql_test {

	public static void main(String[] args) {
		Connection myConnection = null;
		String url = "jdbc:mysql://localhost:3306/test? ";
		// String driverClass = "org.gjt.mm.mysql.Driver";
		String user = "";
		String password = "";

		try {
			// Treiber laden.
			// Class.forName(driverClass);
			Class.forName("org.gjt.mm.mysql.Driver").newInstance();   //jetzt: MySQL Connector/J !
			// Class.forName("com.mysql.jdbc.Driver").newInstance();

			// Verbindung aufbauen.
			myConnection = DriverManager.getConnection(url, user, password);

			String query = "SELECT Gruppe, Kennung FROM cd_liste";
			Statement stmt = myConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				int vn = rs.getInt(2);
				//			.getNString(arg0)(3);
				String nn = rs.getString("Kennung");
				System.out.println(vn + "  " + nn);
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}