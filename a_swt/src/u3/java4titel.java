package u3;

/**
 * Created by Crazy_Jenny on 2015/5/2.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class java4titel {
	public static String[] connect(int i) {
		String[] t = new String[i];

		Connection myConnection = null;
		String url = "jdbc:mysql://localhost:3306/test?";
		String driverClass = "org.gjt.mm.mysql.Driver";
		String user = "";
		String password = "";


		try {
			Class.forName("org.gjt.mm.mysql.Driver").newInstance();
			myConnection = DriverManager.getConnection(url, user, password);

			String query = "SELECT Gruppe, Titel FROM cd_liste";
			Statement stmt = myConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int j=0;
			while (rs.next()) {
				t[j] = rs.getString("Titel");
				j++;
			}
			rs.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return t;
	}
}
