package u3;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Created by Crazy_Jenny on 2015/5/2.
 */
public class data1 {
	public static void main(String[] args) {

		final int i = new data3rech().rechnen();
		//System.out.println("i="+i);
		String[] titel = new String[i];
		String[] gruppe = new String[i];
		gruppe = data2.connect(i);
		titel = java4titel.connect(i);

		final int[] start = {0};

		Display dispaly = new Display();
		final Shell shell = new Shell();
		shell.setText("Zeigen:");
		shell.setSize(300, 200);

		Label Gruppe = new Label(shell, SWT.CENTER);
		Gruppe.setText("Gruppe:");
		Gruppe.setBounds(20, 10, 50, 20);
		final Text t1 = new Text(shell,SWT.BORDER |SWT.CENTER);
		t1.setBounds(70, 10, 200, 20);
		t1.setText(gruppe[start[0]]);

		//final Label lb1 = new Label(shell, SWT.BORDER | SWT.CENTER);
		//lb1.setBounds(70, 10, 200, 20);
		//lb1.setText(gruppe[start[0]]);

		Label Titel = new Label(shell, SWT.CENTER);
		Titel.setText("Titel:");
		Titel.setBounds(20, 40, 40, 20);
		final Text t2 = new Text(shell,SWT.BORDER |SWT.CENTER);
		t2.setBounds(70, 40, 200, 20);
		t2.setText(titel[start[0]]);
		//final Label lb2 = new Label(shell, SWT.BORDER | SWT.CENTER);
		//lb2.setBounds(70, 40, 200, 20);
		//lb2.setText(titel[start[0]]);

		Label DS = new Label(shell, 1);
		DS.setText("Anzahl Datensätze:");
		DS.setBounds(20, 70, 100, 20);
		final Label lb1 = new Label(shell, 1);
		lb1.setBounds(130, 70, 80, 20);
		lb1.setText(" " +i);


		final Button b1 = new Button(shell, SWT.PUSH);
		b1.setSize(25, 25);
		b1.setLocation(40, 100);
		b1.setText("<");

		final Button b2 = new Button(shell, SWT.PUSH);
		b2.setSize(25, 25);
		b2.setLocation(80, 100);
		b2.setText(">");

		final Button b3 = new Button(shell, SWT.PUSH);
		b3.setSize(25, 25);
		b3.setLocation(160, 100);
		b3.setText("|<");

		final Button b4 = new Button(shell, SWT.PUSH);
		b4.setSize(25, 25);
		b4.setLocation(200, 100);
		b4.setText(">|");

		final String[] finalTitel = titel;
		final String[] finalGruppe = gruppe;
		//		final int temp;

		b1.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				if (start[0]>0){//-1
					t1.setText(finalGruppe[start[0] -1]);
					t2.setText(finalTitel[start[0] -1]);
					start[0]--;
				}else{
					MessageBox dialog=new MessageBox(shell,SWT.OK|SWT.ICON_INFORMATION);
					dialog.setText("Error");
					dialog.setMessage("Das ist der erste Datensatz!");
					dialog.open();
				}
			}
		});

		b2.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				if (start[0] <i){
					t1.setText(finalGruppe[start[0] +1]);
					t2.setText(finalTitel[start[0] +1]);
					start[0]++;
				}else{
					MessageBox dialog=new MessageBox(shell,SWT.OK|SWT.ICON_INFORMATION);
					dialog.setText("Error");
					dialog.setMessage("Das ist der letzte Datesatz");
					dialog.open();
				}
			}
		});


		b3.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				if (start[0]>0){//-1
					t1.setText(finalGruppe[0]);
					t2.setText(finalTitel[0]);
					start[0]--;
				}else{
					MessageBox dialog=new MessageBox(shell,SWT.OK|SWT.ICON_INFORMATION);
					dialog.setText("Error");
					dialog.setMessage("Das ist der erste Datensatz!");
					dialog.open();
				}
			}
		});

		b4.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				if (start[0] <i){
					t1.setText(finalGruppe[i-1]);
					//	t1.setText(finalGruppe[start[0] +1]);
					t2.setText(finalTitel[i-1]);
					start[0]++;
				}else{
					MessageBox dialog=new MessageBox(shell,SWT.OK|SWT.ICON_INFORMATION);
					dialog.setText("Error");
					dialog.setMessage("Das ist der letzte Datesatz");
					dialog.open();
				}
			}
		});


		shell.open();
		while (!shell.isDisposed()) {
			if (!dispaly.readAndDispatch()) {
				dispaly.sleep();
			}
		}

		dispaly.dispose();
	}
};