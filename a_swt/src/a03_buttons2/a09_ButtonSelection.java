package a03_buttons2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a09_ButtonSelection {
  public static void main(String[] args) {
    Display display = new Display();
    final Shell shell = new Shell(display);
    shell.setLayout(new FillLayout());
    
    for (int i = 0; i < 20; i++) {
      Button button = new Button(shell, SWT.TOGGLE);
      button.setText("B" + i);
    }
    Control[] children = shell.getChildren();
    for (int i = 0; i < children.length; i++) {
      Control child = children[i];
      ((Button) child).setSelection(true);
    }

    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}