package a03_buttons2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

public class a04_GroupWithRadioButtons {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		Button b1, b2, b3,b4;
		// Create the first Group
		Group group1 = new Group(shell, SWT.SHADOW_IN);
		group1.setText("Waehlen Sie:");
		group1.setLayout(new RowLayout(SWT.VERTICAL));
		b1 = new Button(group1, SWT.RADIO);
		b1.setText("A");
		b2 = new Button(group1, SWT.RADIO);
		b2.setText("B");
		b3 = new Button(group1, SWT.RADIO);
		b3.setText("C");
		b4 = new Button(group1, SWT.RADIO);
		b4.setText("D");
		//	if (b1.getData() getSelection()) {
		System.out.println("Auswahl b1") ;
		//}


		shell.open();
		System.out.println("Auswahl b1" + b1.getData()) ;
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}