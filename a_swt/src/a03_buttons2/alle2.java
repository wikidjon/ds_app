package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class alle2 {


	public static void main(final String[] args) {
		new alle2();
	}

	public alle2() {
		final Display display = new Display();
		final Shell shell = new Shell(display);

		shell.setLayout(new GridLayout(5, true));

		final Button arrowButton = new Button(shell, SWT.ARROW);
		arrowButton.setText("&Arrow Button");

		final Button checkButton = new Button(shell, SWT.CHECK);
		checkButton.setText("&Check Button");

		final Button pushButton = new Button(shell, SWT.PUSH);
		pushButton.setText("&Push Button");

		final Button radioButton = new Button(shell, SWT.RADIO);
		radioButton.setText("&Radio Button");

		final Button toggleButton = new Button(shell, SWT.TOGGLE);
		toggleButton.setText("&Toggle Button");

		shell.pack();
		shell.setSize(500, 100);
		shell.open();
		//textUser.forceFocus();

		// Set up the event loop.
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

		display.dispose();
	}

	@SuppressWarnings("unused")
	private void init() {

	}
}
