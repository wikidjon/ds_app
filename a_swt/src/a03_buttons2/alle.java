package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class alle {

  public static void main(String[] a) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setLayout(new GridLayout(3, true));
    
    new Button(shell, SWT.PUSH).setText("Push 1");
    new Button(shell, SWT.PUSH).setText("Push 2");
    new Button(shell, SWT.PUSH).setText("Push 3");
    
    new Button(shell, SWT.CHECK).setText("Checkbox 1");
    new Button(shell, SWT.CHECK).setText("Checkbox 2");
    new Button(shell, SWT.CHECK).setText("Checkbox 3");
    
    new Button(shell, SWT.TOGGLE).setText("Toggle 1");
    new Button(shell, SWT.TOGGLE).setText("Toggle 2");
    new Button(shell, SWT.TOGGLE).setText("Toggle 3");
    
    new Button(shell, SWT.RADIO).setText("Radio 1");
    new Button(shell, SWT.RADIO).setText("Radio 2");
    new Button(shell, SWT.RADIO).setText("Radio 3");
    
    new Button(shell, SWT.FLAT).setText("Flat 1");
    new Button(shell, SWT.FLAT).setText("Flat 2");
    new Button(shell, SWT.FLAT).setText("Flat 3");
    
    new Button(shell, SWT.ARROW);
    new Button(shell, SWT.ARROW | SWT.LEFT);
    new Button(shell, SWT.ARROW | SWT.DOWN);

    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}
