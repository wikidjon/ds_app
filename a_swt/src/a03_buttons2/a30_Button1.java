package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/***************************************
 * Buttons, Events und eine MessageBox...
 *****************************************/
public class a30_Button1 {
	public static void main(final String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setSize(320, 200);
		//***
		shell.setLayout(new RowLayout());
		//shell.setLayout(new FillLayout());
		// */
		final Button button = new Button(shell, SWT.PUSH);
		button.setBounds(10, 10, 100, 20);
		button.setText("Klick mich!");
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				message(e);
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private static void message(final SelectionEvent e) {
		final Shell shell = ((Button) e.widget).getShell();
		final MessageBox mb = new MessageBox(shell);
		mb.setMessage("Danke!");
		mb.open();
	}
}
