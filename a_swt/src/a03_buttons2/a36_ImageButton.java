package a03_buttons2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a36_ImageButton {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		Image image = new Image(display, "src/icons/hotel.gif");

		Button button = new Button(shell, SWT.PUSH);
		button.setBounds(10, 10, 80, 30);

		button.setImage(image);
		button.setText("button");

		System.out.println(button.getImage());

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

}
