package a000_komposition.ccc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

//import a000_komposition.a04_Menue_toolbar;	//fuer icons

public final class a04_MenuShell_composite  {

	private static Image createImage(String name) {
		return new Image(
				Display.getCurrent(),"src/icons/"+ name + ".gif");
	}
	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	static void setLabelText(String object) {
		label1.setText(object);
	}

	public int spacing = 3;

	static Label label1;

	public static void hallo_main(Display display, final Shell shell) {
		shell.setText("Layout Demo");
		shell.setLayout(new BorderLayout());

		Composite composite1 = new Composite(shell,SWT.NONE);
		composite1.setLayoutData("NORDEN");
		FillLayout fillLayout = new FillLayout ();
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		composite1.setLayout (fillLayout);


		//		***************Menue oben anzeigen *******************
		a20_menue2 men = new a20_menue2();
		men.menue2(shell, display, composite1);

		final ToolBar toolbar = new ToolBar(composite1, SWT.FLAT);
		toolbar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");

		shell.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				Rectangle r = shell.getClientArea();
				toolbar.setBounds(r.x,	r.y, r.width, toolbar.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			}
		});


		//		***************Gruppe unten anzeigen *******************
		SashForm form = new SashForm(shell,SWT.HORIZONTAL);
		form.setLayout(new FillLayout());

		Composite composite2 = new Composite(shell,SWT.NONE);
		composite2.setLayoutData("SUEDEN");


		label1 = new Label(shell, SWT.BORDER);
		label1.setLayoutData("SUEDEN");


		//      ***************Tabelle in der Mitte anzeigen *******************
		Composite composite3 = new Composite(shell,SWT.NONE);
		composite3.setLayoutData("ZENTRUM");
		composite3.setLayout(new BorderLayout());

		composite3.setBackground(new Color(display,255,0,0));

		Composite composite4 = new Composite(shell,SWT.BORDER);
		composite4.setLayoutData("OSTEN");
		composite4.setBackground(new Color(display,31,133,31));


		//Menue im linken Bildschirm anfuegen
		Composite composite5 = new Composite(shell,SWT.NONE);
		composite5.setLayoutData("WESTEN");
		composite5.setBackground(new Color(display,131,133,31));


		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}