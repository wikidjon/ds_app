package a000_komposition;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

//import a04_menue.BorderLayout;

public class aaa {
	@SuppressWarnings("unused")
	public static void cb(Display d, Shell s) {
		final CoolBar coolBar = new CoolBar(s, SWT.NONE);
		// coolBar.setSize(395,70);
		// coolBar.setLocation(0,0);
		// create images for toolbar buttons

		final Image saveIcon = new Image(d, "icons/hotel.gif");
		final Image openIcon = new Image(d, "icons/hotel.gif");
		final Image childIcon = new Image(d, "icons/cut.gif");
		final Image cutIcon = new Image(d, "icons/cut.gif");
		final Image copyIcon = new Image(d, "icons/cut.gif");
		final Image pasteIcon = new Image(d, "icons/cut.gif");

		final CoolItem openCoolItem = new CoolItem(coolBar, SWT.NONE);

		final ToolBar fileToolBar = new ToolBar(coolBar, SWT.HORIZONTAL);
		final ToolItem openToolItem = new ToolItem(fileToolBar, SWT.PUSH);
		openToolItem.setImage(openIcon);
		openToolItem.setText("Oeffnen");
		openToolItem.setToolTipText("Oeffnen");

		final ToolItem saveToolItem = new ToolItem(fileToolBar, SWT.PUSH);
		saveToolItem.setImage(openIcon);
		saveToolItem.setText("Speichern");
		saveToolItem.setToolTipText("Speichern");

		fileToolBar.pack();
		Point size = fileToolBar.getSize();
		openCoolItem.setControl(fileToolBar);
		openCoolItem.setSize(openCoolItem.computeSize(size.x, size.y));

		final CoolItem editbarCoolItem = new CoolItem(coolBar, SWT.PUSH);
		final ToolBar editToolBar = new ToolBar(coolBar, SWT.HORIZONTAL);

		final ToolItem cutToolItem = new ToolItem(editToolBar, SWT.PUSH);
		cutToolItem.setImage(cutIcon);
		cutToolItem.setText("Ausschneiden");
		cutToolItem.setToolTipText("Ausschneiden");

		final ToolItem copyToolItem = new ToolItem(editToolBar, SWT.PUSH);
		copyToolItem.setImage(copyIcon);
		copyToolItem.setText("Copy");
		copyToolItem.setToolTipText("Copy");

		final ToolItem pasteToolItem = new ToolItem(editToolBar, SWT.PUSH);
		pasteToolItem.setImage(pasteIcon);
		pasteToolItem.setText("Einfuegen");
		pasteToolItem.setToolTipText("Einfuegen");
		editToolBar.pack();
		size = editToolBar.getSize();
		editbarCoolItem.setControl(editToolBar);
		editbarCoolItem.setSize(editbarCoolItem.computeSize(size.x, size.y));

		final CoolItem fontCoolItem = new CoolItem(coolBar, SWT.PUSH);
		final Combo fontCombo = new Combo(coolBar, SWT.READ_ONLY | SWT.BORDER);
		String[] items = { "Arial", "Courier", "Times New Roman" };
		fontCombo.setItems(items);
		fontCombo.pack();
		size = fontCombo.getSize();
		fontCoolItem.setControl(fontCombo);
		fontCoolItem.setSize(fontCoolItem.computeSize(size.x, size.y));
		fontCoolItem.setMinimumSize(size);

		openToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("Oeffnen");

			}
		});

		saveToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("Speichern");

			}
		});

		cutToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("Ausschneiden");

			}
		});

		copyToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("Copy");

			}
		});

		pasteToolItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				System.out.println("Einfuegen");

			}
		});
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		final Display d = new Display();
		final Shell s = new Shell(d);
		s.setSize(600, 400);
		s.setImage(new Image(d, "src/icons/hotel.gif"));

		// BorderLayout bl = new BorderLayout() ;
		// BorderLayout.borderlayout(d, s);
		// s.setLayout(bl);

		FillLayout fillLayout1 = new FillLayout(SWT.VERTICAL);
		// FillLayout fillLayout2 = new FillLayout (SWT.HORIZONTAL);

		fillLayout1.marginHeight = 15;
		fillLayout1.marginWidth = 15;
		fillLayout1.spacing = 1;
		// fillLayout2.spacing = 1;
		s.setLayout(fillLayout1);

		// cb(d, s);

		// menu anlegen
		Menu m = new Menu(s, SWT.BAR);

		final MenuItem file = new MenuItem(m, SWT.CASCADE);
		file.setText("&Datei");
		final Menu filemenu = new Menu(s, SWT.DROP_DOWN);
		file.setMenu(filemenu);
		final MenuItem openMenuItem = new MenuItem(filemenu, SWT.PUSH);
		openMenuItem.setText("&Oeffnen\tCTRL+O");
		openMenuItem.setAccelerator(SWT.CTRL + 'O');
		final MenuItem saveMenuItem = new MenuItem(filemenu, SWT.PUSH);
		saveMenuItem.setText("&Speichern\tCTRL+S");
		saveMenuItem.setAccelerator(SWT.CTRL + 'S');
		final MenuItem separator = new MenuItem(filemenu, SWT.SEPARATOR);
		final MenuItem exitMenuItem = new MenuItem(filemenu, SWT.PUSH);
		exitMenuItem.setText("E&xit");

		final MenuItem edit = new MenuItem(m, SWT.CASCADE);
		edit.setText("&Bearbeiten");
		final Menu editmenu = new Menu(s, SWT.DROP_DOWN);
		edit.setMenu(editmenu);
		final MenuItem cutMenuItem = new MenuItem(editmenu, SWT.PUSH);
		cutMenuItem.setText("&Ausschneiden");
		final MenuItem copyMenuItem = new MenuItem(editmenu, SWT.PUSH);
		copyMenuItem.setText("Co&py");
		final MenuItem pasteMenuItem = new MenuItem(editmenu, SWT.PUSH);
		pasteMenuItem.setText("&Einfuegen");

		final MenuItem window = new MenuItem(m, SWT.CASCADE);
		window.setText("&Fenster");
		final Menu windowmenu = new Menu(s, SWT.DROP_DOWN);
		window.setMenu(windowmenu);
		final MenuItem maxMenuItem = new MenuItem(windowmenu, SWT.PUSH);
		maxMenuItem.setText("Ma&ximieren");
		final MenuItem minMenuItem = new MenuItem(windowmenu, SWT.PUSH);
		minMenuItem.setText("Mi&nimieren");

		final MenuItem help = new MenuItem(m, SWT.CASCADE);
		help.setText("&Hilfe");
		final Menu helpmenu = new Menu(s, SWT.DROP_DOWN);
		help.setMenu(helpmenu);
		final MenuItem aboutMenuItem = new MenuItem(helpmenu, SWT.PUSH);
		aboutMenuItem.setText("&About");

		// Listener fuer menueeintraege

		openMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Oeffnen");
			}
		});

		saveMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Speichern");
			}
		});

		exitMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.exit(0);
			}
		});

		cutMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Ausschneiden");
			}
		});

		copyMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Kopieren");
			}
		});

		pasteMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Einfuegen");
			}
		});

		maxMenuItem.addSelectionListener(new SelectionListener() {
			private Control maxItem;

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				s.setMaximized(true);
			}
		});

		minMenuItem.addSelectionListener(new SelectionListener() {
			private Control minItem;

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				s.setMinimized(true);
			}
		});

		aboutMenuItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Hilfe About");
			}
		});

		final Composite composite1 = new Composite(s, SWT.BORDER);
		composite1.setBackground(new Color(d, 31, 133, 31));
		// composite1.setBounds(shell.getClientArea());
		// composite1.setBounds(shell.getBounds());

		final Label label = new Label(s, SWT.BORDER);
		label.setLayoutData("SUEDEN");

		final Label label2 = new Label(composite1, SWT.NONE);
		label2.setText("Das ist ein gr�nes Composite ");
		label2.setBounds(10, 10, 250, 15);

		s.setMenuBar(m);
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}

}
