package a000_komposition.fff;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;


public final class a00_tree  {

	public void createTree(Composite com, Display d ){ //, Display d){


		final Tree tree = new Tree(com, SWT.NONE);
		// c.setBackground(new Color(display, 231, 133, 31));
		com.setBackground(new Color(d, 231, 133, 31));
		tree.setSize(100, 250); //150, 250
		tree.setLocation(5,5);

		TreeItem ebene1 = new TreeItem(tree, SWT.NONE);
		ebene1.setText("Oma");
		TreeItem ebene1a = new TreeItem(tree, SWT.NONE);
		ebene1a.setText("Opa");
		TreeItem ebene1b = new TreeItem(tree, SWT.NONE);
		ebene1b.setText("Weihnachtsmann");
		TreeItem ebene1c = new TreeItem(tree, SWT.NONE);     //ebene1
		ebene1c.setText("Hausfreund");
		TreeItem ebene1d = new TreeItem(tree, SWT.NONE);     //ebene1
		ebene1d.setText("AAAAAA");
		//zweite Ebene
		TreeItem ebene12 = new TreeItem(ebene1, SWT.NONE,0);
		ebene12.setText("Tochter");
		TreeItem ebene120 = new TreeItem(ebene1, SWT.NONE, 1);
		ebene120.setText("Sohn");
		TreeItem ebene121 = new TreeItem(ebene1b, SWT.NONE, 0);
		ebene121.setText("Weihnachtsmaennchen");
		TreeItem ebene122 = new TreeItem(ebene1a, SWT.NONE, 0);
		ebene122.setText("(Freundin!!!)");

		TreeItem ebene130 = new TreeItem(ebene12, SWT.NONE);
		ebene130.setText("Enkelsohn");

		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeItem[] t = tree.getSelection();
				System.out.print("Auswahl: ");
				for(int i = 0; i < t.length; i++) {
					System.out.print(t[i].getText() + ", ");
					a_MenuShell_composite.setLabelText(t[i].getText());
				}
				System.out.println();
			}
		});

		tree.addTreeListener(new TreeListener() {
			@Override
			public void treeCollapsed(TreeEvent e) {
				System.out.println("Tree collapsed.");
			}
			@Override
			public void treeExpanded(TreeEvent e) {
				System.out.println("Tree expanded.");
			}
		});
	}
}
