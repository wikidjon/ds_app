package a000_komposition;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * Eine Toolbar...
 */
public class a04_Menue_toolbar {
	public static void main(String[] arguments) {
		Display display = new Display();
		final Shell shell = new Shell(display);

		final ToolBar toolbar = new ToolBar(shell, SWT.FLAT);
		toolbar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");

		shell.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Rectangle r = shell.getClientArea();
				toolbar.setBounds(
					r.x,
					r.y,
					r.width,
					toolbar.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			}
		});
		
		

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static Image createImage(String name) {
		return new Image(
			Display.getCurrent(), "src/icons/"+ name + ".gif");
//   		Display.getCurrent(), a04_Menue_toolbar.class.getResourceAsStream("/icons/" + name + ".gif"));
	}
}
