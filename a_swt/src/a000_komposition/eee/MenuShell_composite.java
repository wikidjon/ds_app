package a000_komposition.eee;

//gesamtes Menue ausgelagert
//sash im centerbereich

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public final class MenuShell_composite {

	static Label label1;
	protected static SashForm sashForm;
	static Composite c;
	protected static SashForm createHSashForm(Composite c) {
		return createSashForm(c, SWT.HORIZONTAL);
	}

	protected static Label createLabel(Composite c, String text, Image icon,
			int style) {
		Label l = new Label(c, style);
		if (text != null) {
			l.setText(text);
		}
		if (icon != null) {
			l.setImage(icon);
		}
		return l;
	}

	protected static SashForm createSashForm(Composite parent, int style) {
		SashForm sf = new SashForm(parent, style);
		return sf;
	}


	protected static SashForm createVSashForm(Composite c) {
		return createSashForm(c, SWT.VERTICAL);
	}

	public static void hallo_main(Display display, Shell shell) {

		shell.setText("Layout Demo");
		shell.setLayout(new BorderLayout());

		// composite 1 f�r Menue
		Composite composite1 = new Composite(shell, SWT.NONE);
		FillLayout fillLayout = new FillLayout();
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		composite1.setLayout(fillLayout);
		composite1.setLayoutData("NORDEN");

		// ***************Menue oben (in composite1) anzeigen
		// *******************
		a00_menue2 men = new a00_menue2();
		men.menue2(shell, display, composite1);

		// ***************Gruppe unten anzeigen *******************
		label1 = new Label(shell, SWT.BORDER);
		label1.setLayoutData("SUEDEN");

		// ****************Zentrum mit Sash***********************************
		// horizontale Sashform holen und tree/table usw. anbinden
		Composite sash = new Composite(shell, SWT.BORDER);
		sash.setLayout(new FillLayout());
		sashForm = createHSashForm(sash);
		//sashForm = createVSashForm(sash);

		// **************Mittelbereich - linke Seite***************************
		c = new Composite(sashForm, SWT.BORDER);
		c.setLayout(new FillLayout());
		// c.setBackground(new Color(display, 231, 133, 31));
		a00_tree tree = new a00_tree();
		tree.createTree(c);
		// l = createLabel(c, "Left Pane", null, SWT.LEFT);

		// **************Mittelbereich - mitte*********************************
		c = new Composite(sashForm, SWT.BORDER);
		c.setLayout(new FillLayout());

		a20_table tab = new a20_table();
		tab.createTable(c);

		// **************Mittelbereich - rechts*******************************

		c = new Composite(sashForm, SWT.BORDER);
		c.setLayout(new FillLayout());
		c.setBackground(new Color(display, 31, 133, 31));

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	static void setLabelText(String object) {
		label1.setText(object);
	}

	Label l;

	protected Label createLabel(Composite c, String text) {
		return createLabel(c, text, null);
	}

	protected Label createLabel(Composite c, String text, Image icon) {
		return createLabel(c, text, icon, SWT.LEFT);
	}

}