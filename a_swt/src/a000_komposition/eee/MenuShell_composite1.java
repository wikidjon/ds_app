package a000_komposition.eee;

//gesamtes Menue ausgelagert
//sash im centerbereich

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import a000_komposition.a04_Menue_toolbar;
//import a04_menue.BorderLayout;

@SuppressWarnings("unused")
public final class MenuShell_composite1 {

	public int spacing = 3;
	static Label label1;
	protected static SashForm sashForm;
	static Composite c;
	Label l;

	public static void hallo_main(Display display, Shell shell) {

		shell.setText("Layout Demo");
		shell.setLayout(new BorderLayout());

		//composite 1 f�r Menue
		Composite composite1 = new Composite(shell, SWT.NONE);
		FillLayout fillLayout = new FillLayout();
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		composite1.setLayout(fillLayout);
		composite1.setLayoutData("NORDEN");

		// ***************Menue oben (in composite1) anzeigen *******************
		a00_menue2 men = new a00_menue2();
		men.menue2(shell, display, composite1);

		// ***************Gruppe unten anzeigen *******************
		label1 = new Label(shell, SWT.BORDER);
		label1.setLayoutData("SUEDEN");

		// ****************Zentrum mit Sash***********************************
		// horizontale Sashform holen und tree/table usw. anbinden
		Composite sash = new Composite(shell, SWT.FLAT);	//Border
		sash.setLayout(new FillLayout());
		sashForm = createHSashForm(sash);
		
		//**************Mittelbereich - linke Seite***************************
		c = new Composite(sashForm, SWT.FLAT);	//Border
		c.setLayout(new FillLayout());
		c.setBackground(new Color(display, 231, 133, 31));

		//**************Mittelbereich - mitte*********************************
		c = new Composite(sashForm, SWT.FLAT);	//Border
		c.setLayout(new FillLayout());
		c.setBackground(new Color(display, 255, 0, 0));
	
		//**************Mittelbereich - rechts*******************************
		c = new Composite(sashForm, SWT.FLAT);	//Border
		c.setLayout(new FillLayout());
		c.setBackground(new Color(display, 31, 133, 31));

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	static void setLabelText(String object) {
		label1.setText("hallo");
	}

	protected static SashForm createSashForm(Composite parent, int style) {
		SashForm sf = new SashForm(parent, style);
		return sf;
	}

	protected static SashForm createVSashForm(Composite c) {
		return createSashForm(c, SWT.VERTICAL);
	}

	protected static SashForm createHSashForm(Composite c) {
		return createSashForm(c, SWT.HORIZONTAL);
	}

	protected static Label createLabel(Composite c, String text, Image icon, int style) {
		Label l = new Label(c, style);
		if (text != null) {
			l.setText(text);
		}
		if (icon != null) {
			l.setImage(icon);
		}
		return l;
	}

	protected Label createLabel(Composite c, String text, Image icon) {
		return createLabel(c, text, icon, SWT.LEFT);
	}

	protected Label createLabel(Composite c, String text) {
		return createLabel(c, text, null);
	}

}