package a000_komposition.bbb;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

//import a000_komposition.a04_Menue_toolbar; //fuer Methode createImgage unten...

//********************************************************************************
// Warum vergrößert sich nur der Mittelteil????
//********************************************************************************

public final class Start {


	private static Image createImage(String name) {
		return new Image(Display.getCurrent(),"src/icons/"+ name + ".gif");
		//				a04_Menue_toolbar.class.getResourceAsStream("../icons/" + name	+ ".gif"));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	public static void main(String[] arguments) {
		Display display = new Display();
		final Shell shell = new Shell();
		shell.setText("Layout Demo");
		shell.setLayout(new BorderLayout()); 			// Auskommentieren !!!!!

		Composite composite1 = new Composite(shell, SWT.NONE);
		composite1.setLayoutData("NORDEN");				// SUEDEN
		FillLayout fillLayout = new FillLayout();
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		composite1.setLayout(fillLayout);

		// ***************Menue oben anzeigen *******************
		a20_menue2 men = new a20_menue2();
		men.menue2(shell, display, composite1);

		final ToolBar toolbar = new ToolBar(composite1, SWT.FLAT);
		toolbar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");

		// ***************Gruppe unten anzeigen *******************
		//SashForm form = new SashForm(shell, SWT.HORIZONTAL);
		//	form.setLayout(new FillLayout());

		Composite composite2 = new Composite(shell, SWT.NONE);
		composite2.setLayoutData("SUEDEN");						//NORDEN

		Label label1 = new Label(shell, SWT.BORDER);
		label1.setLayoutData("SUEDEN");							//NORDEN
		//label1.setText("Hier kann ein Text stehen... ");

		// *************** Mitte anzeigen *******************
		Composite composite3 = new Composite(shell, SWT.BORDER);
		composite3.setLayoutData("ZENTRUM");
		composite3.setBackground(new Color(display, 255, 0, 0));

		Composite composite4 = new Composite(shell, SWT.BORDER);
		composite4.setLayoutData("OSTEN");
		composite4.setBackground(new Color(display, 0, 0, 255));

		Composite composite5 = new Composite(shell, SWT.BORDER);
		composite5.setLayoutData("WESTEN");
		composite5.setBackground(new Color(display, 0, 255, 0));
		// *******************************************************************

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}


}