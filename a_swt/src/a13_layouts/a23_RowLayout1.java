package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a23_RowLayout1 {
	public static void main(String[] args) {
		Display display = new Display();

		Shell shell = new Shell(display);

		RowLayout rowLayout = new RowLayout();
		rowLayout.fill = true;
		//rowLayout.justify = true; 			// auch bei Größenveränderungen zentrieren   !!!!!!
		rowLayout.pack = false;					// verändern ....
		rowLayout.type = SWT.VERTICAL;
		//rowLayout.wrap = false;

		shell.setLayout(rowLayout);

		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("Button1");
		button1.setLayoutData(new RowData(100, 35));

		Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);
		text.setText("Text 1");

		List list = new List(shell, SWT.BORDER);
		list.add("Eintrag 1");
		list.add("Eintrag 2");
		list.add("Eintrag 3");

		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Button  2");

		Text text2 = new Text(shell, SWT.SINGLE | SWT.BORDER);
		text2.setText("Text 2");

		// wietere Komponenten
		Button button3 = new Button(shell, SWT.PUSH);
		button3.setText("Button3");
		button3.setLayoutData(new RowData(100, 35));

		Text text3 = new Text(shell, SWT.SINGLE | SWT.BORDER);
		text3.setText("Text 3");

		List list2 = new List(shell, SWT.BORDER);
		list2.add("Eintrag 21");
		list2.add("Eintrag 22");
		list2.add("Eintrag 23");

		// shell.setSize(120, 120);
		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

}
