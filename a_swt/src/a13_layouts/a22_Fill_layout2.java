package a13_layouts;

// "Nachbildung" Borderlayout
// Verschachtelung

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a22_Fill_layout2 {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);

		FillLayout fillLayout2 = new FillLayout (SWT.VERTICAL);
		FillLayout fillLayout3 = new FillLayout (SWT.HORIZONTAL);

		fillLayout2.marginHeight = 5;
		fillLayout2.marginWidth = 5;

		fillLayout2.spacing = 1;
		fillLayout3.spacing = 1;
		shell.setLayout (fillLayout2);	// aendern auf 2!!!

		Button button0 = new Button (shell, SWT.PUSH);
		button0.setText ("Bereich 0");

		//******Composite **************************************************
		Composite a = new Composite(shell, SWT.BORDER);
		a.setLayout (fillLayout3);			// auskommentieren !!!!!!!!!!!!!!!

		Button button1 = new Button (a, SWT.PUSH);
		button1.setText ("Bereich 1");

		Button button2 = new Button (a, SWT.PUSH);
		button2.setText ("Bereich 2");

		Button button3 = new Button (a, SWT.PUSH);
		button3.setText ("Bereich 3");
		//******Composite Ende **************************************************

		Button button4 = new Button (shell, SWT.PUSH);
		button4.setText ("Bereich 4");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}