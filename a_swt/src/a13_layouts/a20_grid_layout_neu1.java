package a13_layouts;

/*************************************************************************
 * 3 Zeilen (grid), wobei die obere und untere nicht gr��er wird, sondern
 * nur die mittlere Zeile,
 ************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a20_grid_layout_neu1 {
	public static void main(String[] args) {
		Display display = new Display();

		Shell shell = new Shell(display);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);

		// allgemeine Eigenschaften (f�r mittleren Bereich)
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;

		// Eigenschaften f�r oberen und unteren Bereich - w�chst nicht - absolute H�he
		GridData gridData1 = new GridData();
		gridData1.horizontalAlignment = GridData.FILL;
		gridData1.heightHint = 120; 			// !!!!

		final Composite comp = new Composite(shell, SWT.BORDER);
		comp.setBackground(new Color(display, 255, 255, 0));
		comp.setLayoutData(gridData1);

		final Composite comp1 = new Composite(shell, SWT.BORDER);
		comp1.setBackground(new Color(display, 255, 255, 255));
		comp1.setLayoutData(gridData);

		final Composite comp3 = new Composite(shell, SWT.BORDER);
		comp3.setBackground(new Color(display, 255, 255, 0));
		comp3.setLayoutData(gridData1);

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

}
