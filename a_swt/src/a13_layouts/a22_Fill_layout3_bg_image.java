package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a22_Fill_layout3_bg_image {
	//	static Image beliebigesBild;

	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);

		FillLayout layout1 = new FillLayout(SWT.VERTICAL);
		layout1.marginWidth = layout1.marginHeight = 100;		// aendern auf 100!!!
		shell.setLayout(layout1);

		final Group group = new Group(shell, SWT.NONE);
		group.setText("Buttongruppe ");
		//***************!!!!**************************
		RowLayout layout2 = new RowLayout(SWT.VERTICAL);
		layout2.marginWidth = layout2.marginHeight = layout2.spacing = 10;
		group.setLayout(layout2);
		for (int i = 0; i < 8; i++) {
			Button button = new Button(group, SWT.RADIO);
			button.setText("Button " + i);
		}

		shell.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				Rectangle rect = shell.getClientArea();
				Image newImage = new Image(display, Math.max(1, rect.width), 1);
				GC gc = new GC(newImage);
				gc.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
				gc.setBackground(display.getSystemColor(SWT.COLOR_BLUE));
				gc.fillGradientRectangle(rect.x, rect.y, rect.width, 1, false);
				gc.dispose();
				//group.setBackgroundImage(newImage);
				shell.setBackgroundImage(newImage);

			}
		});
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
