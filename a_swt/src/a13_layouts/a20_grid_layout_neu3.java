package a13_layouts;

/*************************************************************************
 * 3 Zeilen (grid), wobei die obere und untere nicht gr��er wird, sondern
 * nur die mittlere Zeile,
 * die Mittlere Zeile ist nochmals durch ein grid mit 3 Spalten untersetzt
 * unterschiedliche Gr��e / Wachstum der mittleren Komponenten
 ************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a20_grid_layout_neu3 {
	public static void main(String[] args) {
		Display display = new Display();

		Shell shell = new Shell(display);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;

		// oben und unten sollen nur breiter werden
		GridData gridData1 = new GridData();
		gridData1.horizontalAlignment = GridData.FILL;
		gridData1.heightHint = 20; // !!!!

		GridData gridData2 = new GridData();
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.grabExcessVerticalSpace = true;
		gridData2.horizontalAlignment = GridData.FILL;
		gridData2.verticalAlignment = GridData.FILL;

		GridData gridData3 = new GridData();
		gridData3.grabExcessHorizontalSpace = true;
		//gridData3.grabExcessVerticalSpace = true;
		gridData3.horizontalAlignment = GridData.FILL;
		gridData3.verticalAlignment = GridData.FILL;
		gridData3.minimumWidth=40;
		//gridData3.horizontalIndent = 40;	// horuzontale Einr�ckung

		// gridlayout fuer Mitte
		GridLayout gridLayout2 = new GridLayout();
		gridLayout2.numColumns = 3;





		final Composite comp = new Composite(shell, SWT.BORDER);
		comp.setBackground(new Color(display, 0, 255, 255));
		comp.setLayoutData(gridData1);

		final Composite comp1 = new Composite(shell, SWT.NONE);
		// comp1.setBackground(new Color(display, 255, 255, 255));
		comp1.setLayoutData(gridData);
		// comp1.setLayout(rowLayout);

		// mittlerer Bereich *********************************************
		comp1.setLayout(gridLayout2);

		final Composite comp11 = new Composite(comp1, SWT.BORDER);
		comp11.setBackground(new Color(display, 255, 0, 0));
		comp11.setLayoutData(gridData3);

		final Composite comp12 = new Composite(comp1, SWT.NONE);
		comp12.setBackground(new Color(display, 255, 255, 255));
		comp12.setLayoutData(gridData);

		final Composite comp13 = new Composite(comp1, SWT.BORDER);
		comp13.setBackground(new Color(display, 0, 0, 255));
		comp13.setLayoutData(gridData3);
		// mittlerer Bereich *********************************************

		final Composite comp2 = new Composite(shell, SWT.BORDER);
		comp2.setBackground(new Color(display, 255, 255, 0));
		comp2.setLayoutData(gridData1);

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

}
