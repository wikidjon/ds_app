package a13_layouts;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a25_form_layout {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);

		FormLayout formLayout = new FormLayout ();
		formLayout.marginHeight = 20;
		formLayout.marginWidth = 150;
		shell.setLayout (formLayout);

		RowLayout rowLayout = new RowLayout ();
		//shell.setLayout(rowLayout);

		Button button0 = new Button (shell, SWT.PUSH);
		button0.setText ("button0");

		Button button1 = new Button (shell, SWT.PUSH);
		button1.setText ("button1");
		//		button1.setLocation(10, 10);

		Button button2 = new Button (shell, SWT.PUSH);
		button2.setText ("button2");

		Button button3 = new Button (shell, SWT.PUSH);
		button3.setText ("button3");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}