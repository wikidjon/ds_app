package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a27_StackLayout1 {

	static int seitenNum = -1;

	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);
		shell.setBounds (10, 10, 300, 200);

		final Composite inhalt = new Composite (shell, SWT.BORDER);
		inhalt.setBounds (100, 10, 190, 90);
		final StackLayout layout = new StackLayout ();
		inhalt.setLayout (layout);

		// erste inhaltsseite
		final Composite seite0 = new Composite (inhalt, SWT.NONE);
		seite0.setLayout (new RowLayout ());
		Label label = new Label (seite0, SWT.NONE);
		label.setText ("Das ist Seite 0");
		label.pack ();

		// zweite inhaltsseite
		final Composite seite1 = new Composite (inhalt, SWT.NONE);
		// seite1.setBackground(display.getSystemColor(SWT.COLOR_BLUE));	//Farbwechsel
		seite1.setLayout (new RowLayout ());
		Button button = new Button (seite1, SWT.NONE);
		button.setText ("Button Seite 1");
		button.pack ();

		// Aktionsbutton
		Button losButton = new Button (shell, SWT.PUSH);
		losButton.setText ("Los");
		losButton.setBounds (10, 10, 80, 25);
		losButton.addListener (SWT.Selection, new Listener () {
			@Override
			public void handleEvent (Event event) {
				seitenNum = ++seitenNum % 2;
				layout.topControl = seitenNum == 0 ? seite0 : seite1;
				inhalt.layout ();
			}
		});

		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
