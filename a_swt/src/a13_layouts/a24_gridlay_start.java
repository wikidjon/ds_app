package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

// Strukturierungsansatz f�r das Programm gridlay
// Startklasse

public class a24_gridlay_start {

	public static void main(String[] args) {

		// Fenster holen
		Display display = new Display();
		//		Shell shell = new Shell(display);
		Shell shell = new Shell(display, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL );

		shell.setSize(250, 300);
		shell.open();
		shell.pack();
		//shell.setEnabled(false);

		// Verarbeitung   --> Properties - Datei   !!!!!
		a24_gridlay a = new a24_gridlay();
		a.gridlay_test(display, shell);

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
