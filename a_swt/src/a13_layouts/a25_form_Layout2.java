package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a25_form_Layout2 {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		shell.setLayout(new FormLayout());
		Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("Button1");

		FormData formData = new FormData();
		formData.left = new FormAttachment(20);
		formData.top = new FormAttachment(20);
		button1.setLayoutData(formData);

		Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Button Nummer 2");

		formData = new FormData();
		//formData.left = new FormAttachment(button1, 0);
		formData.left = new FormAttachment(button1, 20);			// !!!!

		formData.top = new FormAttachment(button1, 20, SWT.NONE);
		formData.top = new FormAttachment(button1, 0, SWT.CENTER); 		//, SWT.CENTER);
		button2.setLayoutData(formData);

		Button button3 = new Button(shell, SWT.PUSH);
		button3.setText("3");

		formData = new FormData();
		formData.top = new FormAttachment(button2, 10);
		formData.left = new FormAttachment(button2,0, SWT.CENTER);
		button3.setLayoutData(formData);

		shell.pack();
		shell.open();

		System.out.println("Button3: " + button3.getBounds());

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}
}
