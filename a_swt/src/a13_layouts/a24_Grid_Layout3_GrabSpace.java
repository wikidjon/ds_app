package a13_layouts;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a24_Grid_Layout3_GrabSpace {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		shell.setLayout(gridLayout);

		Label label = new Label(shell, SWT.BORDER);
		label.setText("Label");

		GridData gridData3 = new GridData();	// Groessenveraenderung
		gridData3.widthHint = 60;				// Breite Label
		gridData3.heightHint = 20;				// Hoehe

		label.setLayoutData(gridData3);

		Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);
		text.setText("Das ist ein Textfeld.");

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;//!!!!!!!!!
		gridData.verticalAlignment = GridData.FILL;
		text.setLayoutData(gridData);

		Button button = new Button(shell, SWT.PUSH);
		button.setText("Button");

		GridData gridData2 = new GridData();
		gridData2.grabExcessVerticalSpace = true;
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.verticalAlignment = GridData.FILL;
		gridData2.horizontalAlignment = GridData.FILL;

		button.setLayoutData(gridData2);

		shell.setSize(300, 80);

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
