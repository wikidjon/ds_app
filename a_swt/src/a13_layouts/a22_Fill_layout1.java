package a13_layouts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a22_Fill_layout1 {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Display d;
		Shell s;

		d = new Display();
		s = new Shell(d);
		s.setSize(250, 250);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("FillLayout Beispiel");
		s.setLayout(new FillLayout());

		final Text t = new Text(s, SWT.MULTI | SWT.BORDER | SWT.WRAP);
		//s.pack();
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
