package a12_dialog;

import java.io.File;
import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class a17_FileDialog_save {

	private static final String[] FILTER_NAMEN = {
			"OpenOffice.org Dateien (*.sxc)",
			"Microsoft Excel Dateien (*.xls)",
			"Comma Separated Values Dateien (*.csv)", "Alle Dateien (*.*)" };

	private static final String[] FILTER_EXTENSION = { "*.sxc", "*.xls", "*.csv", "*.*" };

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("File Dialog");
		createContents(shell);
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	public static void createContents(final Shell shell) {
		shell.setLayout(new GridLayout(5, true));

		new Label(shell, SWT.NONE).setText("Datei Name:");

		final Text fileName = new Text(shell, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 4;
		fileName.setLayoutData(data);

		Button multi = new Button(shell, SWT.PUSH);
		multi.setText("Oeffnen Multiple...");
		multi.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				// User has selected to open multiple files
				FileDialog dlg = new FileDialog(shell, SWT.MULTI);
				dlg.setFilterNames(FILTER_NAMEN);
				dlg.setFilterExtensions(FILTER_EXTENSION);
				String fn = dlg.open();
				if (fn != null) {
					// Append all the selected files. Since getFileNames()returns only
					// the names, and not the path, prepend the path, normalizing if necessary
					StringBuffer buf = new StringBuffer();
					String[] files = dlg.getFileNames();
					for (int i = 0, n = files.length; i < n; i++) {
						buf.append(dlg.getFilterPath());
						if (buf.charAt(buf.length() - 1) != File.separatorChar) {
							buf.append(File.separatorChar);
						}
						buf.append(files[i]);
						buf.append(" ");
					}
					fileName.setText(buf.toString());
				}
			}
		});

		Button open = new Button(shell, SWT.PUSH);
		open.setText("Oeffnen...");
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				FileDialog dlg = new FileDialog(shell, SWT.OPEN);
				dlg.setFilterNames(FILTER_NAMEN);
				dlg.setFilterExtensions(FILTER_EXTENSION);
				String fn = dlg.open();
				if (fn != null) {
					fileName.setText(fn);
				}
			}
		});

		Button save = new Button(shell, SWT.PUSH);
		save.setText("Speichern...");
		save.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				// User has selected to save a file
				FileDialog dlg = new FileDialog(shell, SWT.SAVE);
				dlg.setFilterNames(FILTER_NAMEN);
				dlg.setFilterExtensions(FILTER_EXTENSION);
				String fn = dlg.open();
				if (fn != null) {
					fileName.setText(fn);
				}
			}
		});
	}

}
