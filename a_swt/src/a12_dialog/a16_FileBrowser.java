package a12_dialog;

import java.io.File;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class a16_FileBrowser {
	public static void main(String[] args) {
		new a16_FileBrowser();
	}

	Display display = new Display();

	Shell shell = new Shell(this.display);
	Tree tree;

	File rootDir;

	private ImageRegistry imageRegistry;

	Image iconFolder = new Image(this.shell.getDisplay(),
			"src/icons/folder.gif");

	Image iconFile = new Image(this.shell.getDisplay(), "src/icons/file.gif");

	public a16_FileBrowser() {
		Action actionSetRootDir = new Action("Auswahl Wurzelverzeichnis") {
			@Override
			public void run() {
				DirectoryDialog dialog = new DirectoryDialog(
						a16_FileBrowser.this.shell);
				String path = dialog.open();
				if (path != null) {
					setRootDir(new File(path));
				}
			}
		};

		ToolBar toolBar = new ToolBar(this.shell, SWT.FLAT);
		ToolBarManager manager = new ToolBarManager(toolBar);
		manager.add(actionSetRootDir);

		manager.update(true);

		this.shell.setLayout(new GridLayout());

		this.tree = new Tree(this.shell, SWT.BORDER);
		this.tree.setLayoutData(new GridData(GridData.FILL_BOTH));

		setRootDir(new File("C:/Ausgang"));

		this.tree.addTreeListener(new TreeListener() {
			@Override
			public void treeCollapsed(TreeEvent e) {
			}

			@Override
			public void treeExpanded(TreeEvent e) {
				TreeItem item = (TreeItem) e.item;
				TreeItem[] children = item.getItems();

				for (int i = 0; i < children.length; i++)
					if (children[i].getData() == null) {
						children[i].dispose();
					} else
						// Child files already added to the tree.
						return;

				File[] files = ((File) item.getData()).listFiles();
				for (int i = 0; files != null && i < files.length; i++) {
					addFileToTree(item, files[i]);
				}
			}
		});

		this.tree.addSelectionListener(new SelectionListener() {
			// Gets called when a tree item is double-clicked.
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				TreeItem item = (TreeItem) e.item;
				File file = (File) item.getData();
				if (Program.launch(file.getAbsolutePath())) {
					System.out.println("File wurde aufgerufen: " + file);
				} else {
					System.out.println("File konnte nicht gestartet werden: "
							+ file);
				}
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});

		final TreeEditor editor = new TreeEditor(this.tree);

		this.tree.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {
				// Locate the cell position.
				Point point = new Point(event.x, event.y);
				final TreeItem item = a16_FileBrowser.this.tree.getItem(point);
				if (item == null)
					return;

				final Text text = new Text(a16_FileBrowser.this.tree, SWT.NONE);
				text.setText(item.getText());
				text.setBackground(a16_FileBrowser.this.shell.getDisplay()
						.getSystemColor(SWT.COLOR_YELLOW));

				editor.horizontalAlignment = SWT.LEFT;
				editor.grabHorizontal = true;
				editor.setEditor(text, item);

				Listener textListener = new Listener() {
					@Override
					public void handleEvent(final Event e) {
						switch (e.type) {
						case SWT.FocusOut:
							File renamed = renameFile((File) item.getData(),
									text.getText());
							if (renamed != null) {
								item.setText(text.getText());
								item.setData(renamed);
							}
							text.dispose();
							break;
						case SWT.Traverse:
							switch (e.detail) {
							case SWT.TRAVERSE_RETURN:
								renamed = renameFile((File) item.getData(),
										text.getText());
								if (renamed != null) {
									item.setText(text.getText());
									item.setData(renamed);
								}
								// FALL THROUGH
							case SWT.TRAVERSE_ESCAPE:
								text.dispose();
								e.doit = false;
							}
							break;
						}
					}
				};

				text.addListener(SWT.FocusOut, textListener);
				text.addListener(SWT.Traverse, textListener);

				text.setFocus();
			}

		});

		this.shell.setSize(400, 260);
		this.shell.open();
		// textUser.forceFocus();

		// Set up the event loop.
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				// If no more entries in event queue
				this.display.sleep();
			}
		}

		this.display.dispose();
	}

	/**
	 * Adds the given file to the tree.
	 * 
	 * @param parent
	 * @param file
	 */
	private void addFileToTree(Object parent, File file) {
		TreeItem item = null;

		if (parent instanceof Tree) {
			item = new TreeItem((Tree) parent, SWT.NULL);
		} else if (parent instanceof TreeItem) {
			item = new TreeItem((TreeItem) parent, SWT.NULL);
		} else
			throw new IllegalArgumentException(
					"parent should be a tree or a tree item: " + parent);

		item.setText(file.getName());
		item.setImage(getIcon(file));

		item.setData(file);

		if (file.isDirectory()) {
			// // recursively adds all the children of this file.
			// File[] files = file.listFiles();
			// for(int i=0; i<files.length; i++)
			// buildTree(item, files[i]);
			if (file.list() != null && file.list().length > 0) {
				new TreeItem(item, SWT.NULL);
			}
		}
	}

	/**
	 * Returns an icon representing the specified file.
	 * 
	 * @param file
	 * @return
	 */
	private Image getIcon(File file) {
		if (file.isDirectory())
			return this.iconFolder;

		int lastDotPos = file.getName().indexOf('.');
		if (lastDotPos == -1)
			return this.iconFile;

		Image image = getIcon(file.getName().substring(lastDotPos + 1));
		return image == null ? this.iconFile : image;
	}

	/**
	 * Returns the icon for the file type with the specified extension.
	 * 
	 * @param extension
	 * @return
	 */
	private Image getIcon(String extension) {
		if (this.imageRegistry == null) {
			this.imageRegistry = new ImageRegistry();
		}
		Image image = this.imageRegistry.get(extension);
		if (image != null)
			return image;

		Program program = Program.findProgram(extension);
		ImageData imageData = (program == null ? null : program.getImageData());
		if (imageData != null) {
			image = new Image(this.shell.getDisplay(), imageData);
			this.imageRegistry.put(extension, image);
		} else {
			image = this.iconFile;
		}

		return image;
	}

	private File renameFile(File file, String newName) {
		File dest = new File(file.getParentFile(), newName);
		if (file.renameTo(dest))
			return dest;
		else
			return null;
	}

	private void setRootDir(File root) {
		// validates the root first.
		if ((!root.isDirectory()) || (!root.exists()))
			throw new IllegalArgumentException("Invalid root: " + root);

		this.rootDir = root;
		this.shell.setText("Aktuelles Verzeichnis: " + root.getAbsolutePath());

		// Remove all the items in the tree.
		if (this.tree.getItemCount() > 0) {
			TreeItem[] items = this.tree.getItems();
			for (int i = 0; i < items.length; i++) {
				items[i].dispose();
				// Dispose itself and all of its descendants.
			}
		}

		// Adds files under the root to the tree.
		File[] files = root.listFiles();
		for (int i = 0; files != null && i < files.length; i++) {
			addFileToTree(this.tree, files[i]);
		}
	}
}
