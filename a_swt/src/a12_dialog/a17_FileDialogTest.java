package a12_dialog;
/*import org.eclipse.swt.*;
//import org.eclipse.swt.events.*;
//import org.eclipse.swt.widgets.*;
//import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
//import org.eclipse.swt.widgets.ColorDialog;
//import org.eclipse.swt.graphics.*;
import org.eclipse.jface.dialogs.*;
import org.eclipse.swt.widgets.*;
//import org.eclipse.swt.widgets.DirectoryDialog;
//import org.eclipse.swt.widgets.FontDialog;
//import org.eclipse.swt.printing.PrintDialog;  // !!! kein widget !!!
*/
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;



public class a17_FileDialogTest {

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(320, 200);
		shell.open();
		
		/*************************************************************
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setFilterNames(new String[] { "Batch Files", "All Files (*.*)" });
		dialog.setFilterExtensions(new String[] { "*.bat", "*.*" }); //wildcards
        dialog.setFilterPath("c:\\"); // Windows path
		dialog.setFileName("fred.bat");
		System.out.println("Save to: " + dialog.open());
		***************************************************************/

		String[] filterExtensions = {"*.txt","*.doc", "*.*"};
		FileDialog fileDialog = new FileDialog(shell, SWT.DIALOG_TRIM);
		
//      FileDialog dlg = new FileDialog(shell, SWT.SAVE);
		
		fileDialog.setText("FileDialog Demo");
		fileDialog.setFilterPath("C:/");
		fileDialog.setFilterExtensions(filterExtensions);
		String selectedFile = fileDialog.open();
		System.out.println("Ausgewaehlte Datei: " + selectedFile);
	
		while (!shell.isDisposed()) {
		if (!display.readAndDispatch()) {
			display.sleep();
		}
	}
	display.dispose();
	}
};