package a12_dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a11_ZahleneingabeDialog extends Dialog {

	public static void main(String[] args) {
		Shell shell = new Shell();
		a11_ZahleneingabeDialog dialog = new a11_ZahleneingabeDialog(shell);
		System.out.println(dialog.open()); 	// Auslesen des Inhaltes !!!!
	}

	Double wert;

	public a11_ZahleneingabeDialog(Shell parent) {
		super(parent);
	}

	public a11_ZahleneingabeDialog(Shell parent, int style) {
		super(parent, style);
	}

	public Double open() {
		Shell parent = getParent();
		final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER| SWT.APPLICATION_MODAL);  //!!!!Modal
		shell.setText("ZahleneingabeDialog");

		shell.setLayout(new GridLayout(2, true));

		Label label = new Label(shell, SWT.NULL);
		label.setText("Geben Sie eine gueltige Zahl ein:");

		final Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);

		final Button buttonOK = new Button(shell, SWT.PUSH);
		buttonOK.setText("Ok");
		buttonOK.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
		Button buttonCancel = new Button(shell, SWT.PUSH);
		buttonCancel.setText("Abbruch");

		text.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event event) {
				try {
					a11_ZahleneingabeDialog.this.wert = new Double(text.getText());
					buttonOK.setEnabled(true);
					System.out.println(a11_ZahleneingabeDialog.this.wert);		//text.getText());
				} catch (Exception e) {
					buttonOK.setEnabled(false);
				}
			}
		});

		buttonOK.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				shell.dispose();
			}
		});

		buttonCancel.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				a11_ZahleneingabeDialog.this.wert = null;
				shell.dispose();
			}
		});

		shell.addListener(SWT.Traverse, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.detail == SWT.TRAVERSE_ESCAPE) {
					event.doit = false;
				}
			}
		});

		text.setText("");
		shell.pack();
		shell.open();

		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return this.wert;
	}

}
