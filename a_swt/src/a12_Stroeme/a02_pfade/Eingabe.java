package a12_Stroeme.a02_pfade;

import java.io.*;
class Eingabe {

	@SuppressWarnings("unused")
	private int i = 0; 
	private float f = 0.0f;
//	Eingabe konv = new Eingabe();

//*********** Eingabe von Float ******************************
	public float EingabeEinesFloat(String s) {
        BufferedReader din = new BufferedReader(new InputStreamReader(System.in));	
		Eingabe konv = new Eingabe();
		try	{
			System.out.print(s);
			f=konv.getFloatFromString(din.readLine());
		}
		catch(IOException e){
			System.out.println(e.toString());
		}
		catch(NumberFormatException e){
			System.out.println("Zahlen-Fehler" + e.toString()); 
			//erneute Eingabe fordern;
		}
		return f;
	}

	public float EingabeEinesFloat() {
        BufferedReader din = new BufferedReader(new InputStreamReader(System.in));			Eingabe konv = new Eingabe();
		try	{
			f=konv.getFloatFromString(din.readLine());
		}
		catch(IOException e){
			System.out.println(e.toString());
		}
		catch(NumberFormatException e){
			System.out.println("Zahlen-Fehler" + e.toString()); 
		}
		return f;
	}


//********** Konvertierungen von Stringeingaben *************
// Umwandeln eines Strings in einen Floatwert
	public float getFloatFromString(String str)	{
		Float f = new Float(str);
		return f.floatValue();
	}

// Umwandeln eines Strings in einen Integerwert
	public int getIntFromString(String str) {
		Integer i = new Integer(str);
		return i.intValue();
	}

// Umwandeln eines Strings in einen Doublewert
	public double getDoubleFromString(String str) {
		Double d = new Double(str);
		return d.doubleValue();
	}

//*********** Eingabe von String ******************************

	public String EingabeEinesString() 
	{
		String sin = " ";
        BufferedReader din = new BufferedReader(new InputStreamReader(System.in));			try
		{
//			System.out.print("Ihre Eingabe: ");
			sin=din.readLine();
		}
		catch(IOException e)
		{
			System.out.println(e.toString());
		}
		return sin;
	}

	//	Überlagerung
	public String EingabeEinesString(String s) 
	{
		String sin = " ";
        BufferedReader din = new BufferedReader(new InputStreamReader(System.in));			try
		{
			System.out.print(s);
			sin=din.readLine();
		}
		catch(IOException e)
		{
			System.out.println(e.toString());
		}
		return sin;
	}

	public char EingabeEinesChar(String s) 
	{
		char cin = ' ';
        BufferedReader din = new BufferedReader(new InputStreamReader(System.in));			
		try
		{
			System.out.print(s);
		//	cin=din.readChar();
			cin=(char)din.read();
		}
		catch(IOException e)
		{
			System.out.println(e.toString());
		}
		return cin;
	}
}

