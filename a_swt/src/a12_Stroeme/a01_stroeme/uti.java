package a12_Stroeme.a01_stroeme;
import java.io.*;

public class uti 
{

//******* Warten auf Eingabe ********************************
	public void waitForReturn() 
	{
		System.out.println("\n Bitte ENTER druecken ...");
		BufferedReader din = new BufferedReader(new InputStreamReader(System.in));
		try
		{
			din.readLine();
		}
		catch (IOException e)
		{
			System.out.println(e.toString());
		}
	}
}