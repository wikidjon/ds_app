package a13_layouts1;

/*******************************
 * SashForm mit drei children
 *******************************/
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import a13_layouts_bsp.a20_table;

@SuppressWarnings("unused")
public class muster0 {

public static void main (String [] args) {
	final Display display = new Display ();
	Shell shell = new Shell(display);
	shell.setLayout (new FillLayout());
	
	FillLayout fillLayout2 = new FillLayout (SWT.VERTICAL);
	FillLayout fillLayout3 = new FillLayout (SWT.HORIZONTAL);

	SashForm form = new SashForm(shell,SWT.HORIZONTAL);
	form.setLayout(fillLayout3);

	/*	Composite child0 = new Composite(form,SWT.NONE);
	child0.setLayout(fillLayout3);
	a_menue men = new a_menue();
	men.menue(shell, display);
	   final Label label = new Label(shell, SWT.BORDER);
	   label.setText("   Hallo   ");
*/

	
//*****Beginn des "Mittelteils"**************************	
	Composite child1 = new Composite(form,SWT.NONE);
	child1.setLayout(fillLayout3);

	final Tree tree = new Tree(child1, SWT.NONE);
	tree.setSize(100, 250); //150, 250
	tree.setLocation(5,5);
	
	TreeItem ebene1 = new TreeItem(tree, SWT.NONE);
	ebene1.setText("Oma");
	TreeItem ebene1a = new TreeItem(tree, SWT.NONE);
	ebene1a.setText("Opa");
	TreeItem ebene1b = new TreeItem(tree, SWT.NONE);
	ebene1b.setText("Weihnachtsmann");
	TreeItem ebene1c = new TreeItem(tree, SWT.NONE);     //ebene1
	ebene1c.setText("Hausfreund");
	TreeItem ebene1d = new TreeItem(tree, SWT.NONE);     //ebene1
	ebene1d.setText("AAAAAA");
	//zweite Ebene
	TreeItem ebene12 = new TreeItem(ebene1, SWT.NONE,0);
	ebene12.setText("Tochter");
	TreeItem ebene120 = new TreeItem(ebene1, SWT.NONE, 1);
	ebene120.setText("Sohn");
	TreeItem ebene121 = new TreeItem(ebene1b, SWT.NONE, 0);
	ebene121.setText("Weihnachtsmaennchen");
	TreeItem ebene122 = new TreeItem(ebene1a, SWT.NONE, 0);
	ebene122.setText("(Freundin!!!)");

	TreeItem ebene130 = new TreeItem(ebene12, SWT.NONE);
	ebene130.setText("Enkelsohn");

	tree.addSelectionListener(new SelectionAdapter() {
		public void widgetSelected(SelectionEvent e) {
			TreeItem[] t = tree.getSelection();
			System.out.print("Auswahl: ");
			for(int i = 0; i < t.length; i++) {
				System.out.print(t[i].getText() + ", ");
			}}});
	
	Composite child2 = new Composite(form,SWT.NONE);
	child2.setLayout(fillLayout3);
	a20_table tab = new a20_table();
	tab.createTable(child2);
	
//********************************************
	Composite child3 = new Composite(form,SWT.BORDER);
	child3.setLayoutData("EAST");
	child3.setLayout (fillLayout2);
	
	Button button0 = new Button (child3, SWT.PUSH);
	button0.setText ("button0");
	Button button1 = new Button (child3, SWT.PUSH);
	button1.setText ("button1");
	Button button2 = new Button (child3, SWT.PUSH);
	button2.setText ("button2");
	Button button3 = new Button (child3, SWT.PUSH);
	button3.setText ("button3");

	shell.open ();
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
}
