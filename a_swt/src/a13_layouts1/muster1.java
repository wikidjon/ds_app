package a13_layouts1;

// "Nachbildung" Borderlayout

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Composite;

@SuppressWarnings("unused")
public class muster1 {
	public static void main (String [] args) {
		Display display = new Display ();
		Shell shell = new Shell (display);
		shell.setLayout (new FillLayout());	
		
		FillLayout fillLayout2 = new FillLayout (SWT.VERTICAL);
		FillLayout fillLayout3 = new FillLayout (SWT.HORIZONTAL);
	
		fillLayout2.marginHeight = 5;
		fillLayout2.marginWidth = 5;
		fillLayout2.spacing = 1;
	    fillLayout3.spacing = 1;
		shell.setLayout (fillLayout2);

		a_menue men = new a_menue();
		men.menue(shell, display);
//		Button button0 = new Button (shell, SWT.PUSH);
//		button0.setText ("Bereich 0");

		//******Composite **************************************************
		Composite a = new Composite(shell, SWT.BORDER);
		a.setLayout (fillLayout3);
//		SashForm form = new SashForm(a,SWT.HORIZONTAL);
//		form.setLayout(fillLayout3);

		Composite child1 = new Composite(shell,SWT.NONE);
		child1.setLayout(fillLayout3);
		new Label(child1,SWT.NONE).setText("Label im composite 1");
		
		Composite child2 = new Composite(shell,SWT.NONE);
		child2.setLayout(fillLayout3);
		new Button(child2,SWT.PUSH).setText("Button im composite 2");

		Composite child3 = new Composite(shell,SWT.NONE);
		child3.setLayout(fillLayout2);
		new Label(child3,SWT.PUSH).setText("Label im composite 3");
/*****		
		Button button1 = new Button (a, SWT.PUSH);
		button1.setText ("Bereich 1");
		
		Button button2 = new Button (a, SWT.PUSH);
		button2.setText ("Bereich 2");
		
		Button button3 = new Button (a, SWT.PUSH);
		button3.setText ("Bereich 3");
//*******/
		//******Composite Ende **************************************************
		
		Button button4 = new Button (shell, SWT.PUSH);
		button4.setText ("Bereich 4");

		shell.pack ();
		shell.open ();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ())
				display.sleep ();
		}
		display.dispose ();
	}
}