package a12_dialoge_speziell;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class mehrereDialoge {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Shell");
		shell.setSize(200, 200);
		shell.open();
		String s = new String ("Hallo");

		Shell dialog1 = new Shell(shell);
		dialog1.setText("Dialog1");
		dialog1.setSize(200, 200);
		dialog1.open();


		Shell dialog2 = new Shell(shell);
		dialog2.setText("Dialog2");
		dialog2.setSize(200, 200);
		dialog2.open();
		Label l2 = new Label(dialog2, 0);
		l2.setText(s);
		l2.setLocation(20, 20);
		l2.pack();

		Shell dialog3 = new Shell(shell);
		dialog3.setText("Dialog3");
		dialog3.setSize(200, 200);
		dialog3.open();



		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}

