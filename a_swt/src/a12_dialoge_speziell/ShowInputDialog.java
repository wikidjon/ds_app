package a12_dialoge_speziell;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

/**
 *datenaustausch zwischen Dialogen/Fenstern
 */
public class ShowInputDialog {
	
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		createContents(shell);
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private static void createContents(final Shell parent) {
		parent.setLayout(new FillLayout(SWT.VERTICAL));
		final Label label = new Label(parent, SWT.NONE);

		Button button = new Button(parent, SWT.PUSH);
		button.setText("Los geht�s ...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
                InputDialog2 dlg = new InputDialog2(parent);
                String input = dlg.open();
                if (input != null) {
                    label.setText(input);
                    label.getParent().pack();
                }
			}
		});
	}

}
