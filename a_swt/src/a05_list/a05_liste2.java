package a05_list;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

/*********************************************************
zwei Auswahllisten miteinander kombiniert
und Ausschrift ausgewaehlter Items auf Konosle und Fenster
!!! Achtung Mouse Down und Mouse Up -Event
 ***********************************************************/
public class a05_liste2 {

	public static void main(String[] args) {
		Display myDisplay = new Display();
		Shell shell = new Shell(myDisplay);
		shell.setSize(300,300);
		shell.setText("Listen-Demo");

		GridLayout gl = new GridLayout();
		gl.numColumns=2;
		shell.setLayout(gl);

		// Label f�r Handlungsanweisung ****************+
		final Label label = new Label(shell, SWT.NONE);
		label.setSize(200,20);
		label.setLocation(10,10);
		label.setText("Bitte waehlen Sie ! ");

		// Label f�r Ergebnisausgabe ********************
		// Auswertung der Auswahl ***********************
		final Label label2 = new Label(shell, SWT.NONE);
		label2.setSize(200,20);
		label2.setLocation(100,200);

		final List list1 = new List(shell, SWT.MULTI|SWT.H_SCROLL|SWT.V_SCROLL);
		list1.setItems(new String[] {"Erdbeeren","Bananen","Aepfel","Birnen","Waldhimbeeren"});
		list1.add("Kirschen");
		list1.add("Apfelsinen");
		list1.setBounds(10,30,80,100);
		list1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final List list2 = new List(shell, SWT.SINGLE|SWT.BORDER);
		list2.setItems(new String[] {"Korb","Papier","Schuessel"});
		list2.setBounds(110,30,80,50);
		list2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		list2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				System.out.println(list1.getSelection()[0] + " und " +
						list2.getSelection()[0] + " passt");
				label2.setText(list1.getSelection()[0] + " und " +
						list2.getSelection()[0]);
			}
			@Override
			public void mouseUp(MouseEvent e) {
				System.out.println("Noch mal ?");
			}
		});

		shell.open();

		while(!shell.isDisposed()){
			if(!myDisplay.readAndDispatch()) {
				myDisplay.sleep();
			}
		}

	}
}
