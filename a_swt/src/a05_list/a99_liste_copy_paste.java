package a05_list;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a99_liste_copy_paste {

public static void main (String [] args) {
	Display display = new Display ();
	Shell shell = new Shell (display);
	Text text = new Text (shell, SWT.BORDER | SWT.V_SCROLL);
	text.setBounds (10, 10, 100, 100);
	for (int i=0; i<16; i++) {
		text.append ("Zeile " + i + "\n");
	}
	shell.open ();
	text.setSelection (9);
	System.out.println ("selection=" + text.getSelection ());
	System.out.println ("position=" + text.getCaretPosition ());
	System.out.println ("location=" + text.getCaretLocation ());
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
}
