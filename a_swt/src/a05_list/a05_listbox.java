package a05_list;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

/*** Listbox und Tooltips... */
public class a05_listbox {
	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(320, 200);
		shell.setLayout(new GridLayout());

		Label label = new Label(shell, SWT.LEFT);
		label.setText("Tooltip beobachten!");

		final List list = new List(shell, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		list.setItems(new String[] {
				"Jim Morrison", "Rolf-Inge", "Peter-Pan",	"Rotk�ppchen",
				"Rumpelstilzchen", "Philip Boa", "Gerhard", "�nschii",
				"Heinz Peter", "Juri Gagarin", "Ernest Hemmingway"});

		//*******Listener anh�ngen******************
		list.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent e) {
				int index = e.y / list.getItemHeight() + list.getTopIndex();
				System.out.println("index berechnung: " + index + ": "+ e.y + "/" + list.getItemHeight()+ "+" + list.getTopIndex() );
				if (index >= 0 && index < list.getItemCount()) {
					list.setToolTipText(
							"Wer bist Du denn , " + list.getItem(index)+ "?? " + index);
				} else {
					list.setToolTipText(null);
				}
			}
		});

		GridData data = new GridData();
		data.grabExcessVerticalSpace = true;
		data.verticalAlignment = GridData.FILL;
		list.setLayoutData(data);

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}