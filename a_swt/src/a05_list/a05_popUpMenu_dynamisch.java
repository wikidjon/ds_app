package a05_list;

/* dynamisches fuellen eines menues */
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class a05_popUpMenu_dynamisch {

public static void main (String [] args) {
	final Display display = new Display ();
	Shell shell = new Shell (display);
	final Tree tree = new Tree (shell, SWT.BORDER | SWT.MULTI);
	final Menu menu = new Menu (shell, SWT.POP_UP);
	tree.setMenu (menu);
	for (int i=0; i<12; i++) {
		TreeItem item = new TreeItem (tree, SWT.NONE);
		item.setText ("Eintrag " + i);
	}
	menu.addListener (SWT.Show, new Listener () {		//SWT Felder show event type
		public void handleEvent (Event event) {			// alternativ mit ALT
			MenuItem [] menuItems = menu.getItems ();
			for (int i=0; i<menuItems.length; i++) {
				menuItems [i].dispose ();
			}
			TreeItem [] treeItems = tree.getSelection ();
			for (int i=0; i<treeItems.length; i++) {
				MenuItem menuItem = new MenuItem (menu, SWT.PUSH);
				// ***************************************************
				// popup menue
				menuItem.setText ("gew�hlt: "+treeItems [i].getText ());
			}
		}
	});
	tree.setSize (200, 200);
	shell.setSize (300, 300);
	shell.open ();
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
} 
