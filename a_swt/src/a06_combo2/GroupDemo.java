package a06_combo2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class GroupDemo {

	public static Display myDisplay;
	public static boolean internalCall = false;

	public static void main(String[] args) {
		internalCall = true;
		myDisplay = new Display();
		GroupDemo gd = new GroupDemo();
		gd.runDemo(myDisplay);
	}

	public void runDemo(Display display) {
		myDisplay = display;
		Shell shell = new Shell(display);
		shell.setSize(300,300);
		shell.setText("Group Demo");

		Group group1 = new Group(shell, SWT.BORDER);
		group1.setBounds(30,30,200,200);
		group1.setText("Gruppe 1");

		Button button = new Button(group1, SWT.PUSH);
		button.setBounds(10,20,80,20);
		button.setText("... in Gruppe..");

		Label label = new Label(group1, SWT.NONE);
		label.setBounds(10,50,80,20);
		label.setText("Hallo!!!");

		Group group2 = new Group (group1, SWT.NONE);
		group2.setBounds(10,100,150,50);
		group2.setBackground(new Color(display,233,20,233));
		group2.setText("Gruppe in der Gruppe");

		Button button2 = new Button(group2, SWT.PUSH);
		button2.setBounds(10,20,70,20);
		button2.setText("Halloechen..");

		shell.open();

		while(!shell.isDisposed()){
		if(!display.readAndDispatch())
			display.sleep();
		}
		if (internalCall) display.dispose();
	}
}
