package a15_ole;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.ole.win32.OLE;
import org.eclipse.swt.ole.win32.OleAutomation;
import org.eclipse.swt.ole.win32.OleControlSite;
import org.eclipse.swt.ole.win32.OleFrame;
import org.eclipse.swt.ole.win32.Variant;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a15_ole_browser {
	static final int Navigate = 0x68;

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		OleFrame frame = new OleFrame(shell, SWT.NONE);
		OleControlSite site = new OleControlSite(frame, SWT.NONE, "Shell.Explorer.1");
		site.doVerb(OLE.OLEIVERB_INPLACEACTIVATE);
		OleAutomation auto = new OleAutomation(site);
		//		auto.invoke(Navigate, new Variant[] { new Variant("http://localhost:8081")});
		auto.invoke(Navigate, new Variant[] { new Variant("http://www.hs-merseburg.de")});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
