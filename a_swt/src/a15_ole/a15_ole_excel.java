package a15_ole;


import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.ole.win32.OLE;
import org.eclipse.swt.ole.win32.OleClientSite;
import org.eclipse.swt.ole.win32.OleFrame;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

//  OLE Excel

public class a15_ole_excel {

	public static final String ID = "ExcelView";
	static OleClientSite clientSite;
	static OleFrame frame;


	static void addFileMenu(OleFrame frame) {
		final Shell shell = frame.getShell();
		Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		MenuItem fileMenu = new MenuItem(menuBar, SWT.CASCADE);
		fileMenu.setText("&File");
		Menu menuFile = new Menu(fileMenu);
		fileMenu.setMenu(menuFile);

		MenuItem menuFileOpen = new MenuItem(menuFile, SWT.CASCADE);
		menuFileOpen.setText("Oeffnen...");
		menuFileOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				fileOpen(shell);
			}
		});

		MenuItem menuFileControl = new MenuItem(menuFile, SWT.CASCADE);
		menuFileControl.setText("Exit");
		menuFileControl.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.dispose();
			}
		});

		frame.setFileMenus(new MenuItem[] { fileMenu });
	}

	static void fileOpen(Shell s) {
		//		FileDialog dialog = new FileDialog(clientSite.getShell(), SWT.OPEN);
		FileDialog dialog = new FileDialog(s, SWT.OPEN);

		dialog.setFilterExtensions(new String[] { "*.xls" });
		String fileName = dialog.open();
		if (fileName != null) {
			clientSite.dispose();
			//clientSite = new OleClientSite(frame, SWT.NONE, "Excel.Document", new File("c:/Lehre/klausuren/netzwerk07.xls"));				//fileName));
			clientSite = new OleClientSite(frame, SWT.NONE, "Excel.Chart", new File("D:/Lehre/noten_prog_WS14_15.xls"));

			clientSite.doVerb(OLE.OLEIVERB_INPLACEACTIVATE);
		}
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Excel Beispiel");
		shell.setLayout(new FillLayout());
		try {
			frame = new OleFrame(shell, SWT.NONE);
			//clientSite = new OleClientSite(frame, SWT.NONE, "Excel.Chart");
			clientSite = new OleClientSite(frame, SWT.NONE, "Excel.Sheet");

			addFileMenu(frame);

		} catch (SWTError e) {
			System.out.println("ActiveX control kann nicht geoeffnet werden.");
			return;
		}
		shell.setSize(800, 600);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}




}
