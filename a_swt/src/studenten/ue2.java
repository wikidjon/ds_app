package studenten;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ue2 {

	public static void main(String[] args) {

		Display display = new Display();

		//Gr��enver�nderung der shell ausschlie�en
		Shell shell = new Shell(display,SWT.SHELL_TRIM  & (~SWT.RESIZE));

		shell.setSize(300, 320);
		shell.setLayout(new GridLayout());

		final Text text = new Text(shell, SWT.SINGLE | SWT.BORDER);

		SelectionListener l = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
				text.setText((String) e.widget.getData());
			}
		};

		// Anzeige der Eingaben / Berechnungen
		text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		//text.setSize(250,400);
		text.setText("Das ist ein Text");


		// Composite f�r dei Buttons
		Composite c1 = new Composite(shell, SWT.NO_FOCUS);

		// Layout f�r die Composite (Buttons)
		GridLayout gridLayout = new GridLayout ();
		gridLayout.numColumns = 4;
		c1.setLayout (gridLayout);

		// Griddata f�r Buttons
		GridData gridData2 = new GridData();
		gridData2.horizontalAlignment = GridData.FILL;
		gridData2.verticalAlignment = GridData.FILL;
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.grabExcessVerticalSpace = true;

		// Span eines Buttons
		GridData gridData3 = new GridData();
		gridData3.verticalAlignment = GridData.FILL;
		gridData3.horizontalAlignment = GridData.FILL;
		gridData3.horizontalSpan = 2;

		c1.setLayoutData(gridData2);

		Button B = new Button(c1, 0);
		B.setText("MC");
		B.setData("MC");
		B.setLayoutData(gridData2);
		//B.setBounds(20, 250, 30, 20);

		Button B1 = new Button(c1, 0);
		B1.setText("MR");
		B1.setLayoutData(gridData2);
		B1.addSelectionListener(l);
		//B1.setBounds(60, 250, 30, 20);

		Button B2 = new Button(c1, 0);
		B2.setText("MS");
		B2.setLayoutData(gridData2);
		//B2.setBounds(100, 250, 30, 20);

		Button B3 = new Button(c1, 0);
		B3.setText("M+");
		B3.setLayoutData(gridData2);

		Button B4 = new Button(c1, 0);
		B4.setText("1");
		B4.setLayoutData(gridData2);
		B4.setData("1");
		B4.addSelectionListener(l);
		//B2.setBounds(100, 250, 30, 20);

		Button B5 = new Button(c1, 0);
		B5.setText("2");
		B5.setLayoutData(gridData2);

		Button B6 = new Button(c1, 0);
		B6.setText("3");
		B6.setLayoutData(gridData2);

		Button B7 = new Button(c1, 0);
		B7.setText("4");
		B7.setLayoutData(gridData2);

		Button B8 = new Button(c1, 0);
		B8.setText("5");
		B8.setLayoutData(gridData2);
		B8.addSelectionListener(l);
		//B2.setBounds(100, 250, 30, 20);

		Button B9 = new Button(c1, 0);
		B9.setText("6");
		B9.setLayoutData(gridData2);

		Button B10 = new Button(c1, 0);
		B10.setText("7");
		B10.setLayoutData(gridData2);

		Button B11 = new Button(c1, 0);
		B11.setText("8");
		B11.setLayoutData(gridData2);


		Button B12 = new Button(c1, 0);
		B12.setText("0");
		B12.setLayoutData(gridData3);

		Button B13 = new Button(c1, 0);
		B13.setText("+");
		B13.setLayoutData(gridData2);


		Menu menubar = new Menu(shell, SWT.BAR);
		Menu menu;
		MenuItem itemoe;
		final MenuItem itemsa;
		MenuItem itemda, itembe;
		final MenuItem itemlo;
		MenuItem itemko;

		// Erstes Menu
		menu = new Menu(menubar);

		itemoe = new MenuItem(menu, SWT.PUSH);
		itemoe.setText("�ffnen");
		itemoe.setData("oeffnen");
		itemoe.addSelectionListener(l);

		itemsa = new MenuItem(menu, SWT.PUSH);
		itemsa.setText("Speichern");
		itemsa.setData("save");
		itemsa.setEnabled(false);

		itemda = new MenuItem(menubar, SWT.CASCADE);
		itemda.setText("Datei");
		itemda.setMenu(menu);

		// Zweites Menu
		menu = new Menu(menubar);

		itembe = new MenuItem(menubar, SWT.CASCADE);
		itembe.setText("Bearbeiten");
		itembe.setMenu(menu);

		itemlo = new MenuItem(menu, SWT.PUSH);
		itemlo.setText("L�schen");
		itemlo.setData("delete");
		itemlo.addSelectionListener(l);

		itemko = new MenuItem(menu, SWT.PUSH);
		itemko.setText("Kopieren");
		itemko.setData("copy");
		itemko.addSelectionListener(l);

		text.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				itemsa.setEnabled(true);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

		});


		//		bar.pack();
		shell.setMenuBar(menubar);
		shell.setSize(250, 350);
		//		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}
