package a16_sonstiges3.password;

import java.io.IOException;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

import a16_sonstiges3.password.data.*;
import a16_sonstiges3.password.ui.*;

public class Password {
  private static final Password app = new Password();

  // liefert einen pointer auf die application
  public static Password getApp() {
    return app;
  }

  // Referenz auf das main window der application
  private PasswordMainWindow mainWindow;

  public void run() {
    Display display = new Display();
    Shell shell = new Shell(display);
    mainWindow = new PasswordMainWindow(shell);
    newFile();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }

  public PasswordMainWindow getMainWindow() {
    return mainWindow;
  }

  public void newFile() {
    PasswordFile passwordFile = new PasswordFile();
    mainWindow.add(passwordFile);
  }

  public void openFile() {
    FileDialog dlg = new FileDialog(mainWindow.getShell());
    dlg.setFilterNames(PasswordConst.FILTER_NAMES);
    dlg.setFilterExtensions(PasswordConst.FILTER_EXTS);
    String fileName = dlg.open();
    if (fileName != null) {
      // legt ein PasswordFile object an und setzt den Dateinamen
      PasswordFile passwordFile = new PasswordFile();
      passwordFile.setFilename(fileName);
      try {
        // Oeffnen der datei, hinzufuegen zur app und refreshen der Dateinamen
        passwordFile.open();
        mainWindow.add(passwordFile);
        mainWindow.refreshTabs();
      } catch (IllegalStateException e) {
        showError(e.getMessage());
      } catch (IOException e) {
        showError("Kann Datei nicht oeffnen " + passwordFile.getFilename());
      }
    }
  }

  public boolean saveFile() {
    boolean saved = false;
    PasswordFile passwordFile = mainWindow.getCurrentFile();
    if (passwordFile != null) {
      // Kein filename -> eingabe
      if (passwordFile.getFilename() == null) {
        saveFileAs();
      } else {
        try {
          passwordFile.save();
          mainWindow.refreshTabs();
          saved = true;
        } catch (IllegalStateException e) {
          showError(e.getMessage());
        } catch (IOException e) {
          showError("Kann Datei nicht speichern " + passwordFile.getFilename());
        }
      }
    }
    return saved;
  }

  public void saveFileAs() {
    PasswordFile passwordFile = mainWindow.getCurrentFile();
    if (passwordFile != null) {
      // SafeSaveDialog - Verhindert ueberschreiben
      SafeSaveDialog dlg = new SafeSaveDialog(mainWindow.getShell());
      if (passwordFile.getFilename() != null) {
        dlg.setFileName(passwordFile.getFilename());
      }
      dlg.setFilterNames(PasswordConst.FILTER_NAMES);
      dlg.setFilterExtensions(PasswordConst.FILTER_EXTS);
      String filename = dlg.open();
      if (filename != null) {
        passwordFile.setFilename(filename);
        saveFile();
        mainWindow.refreshTabs();
      }
    }
  }

  public void newPassword() {
    PasswordFile passwordFile = mainWindow.getCurrentFile();
    if (passwordFile != null) {
      // Passwordeingabedialog
      PasswordEntryDialog dlg = new PasswordEntryDialog(mainWindow.getShell());
      PasswordEntry entry = dlg.open();
      if (entry != null) {
        // Hinzufuegen des passwordeintrages und refreshen der filenamen
        passwordFile.add(entry);
        mainWindow.getCurrentFile().setDirty(true);
        mainWindow.getCurrentTab().refresh();
        mainWindow.refreshTabs();
      }
    }
  }

  public void editPassword() {
    // Bestimmen des zu editierenden Eintrages
    PasswordFileTab tab = mainWindow.getCurrentTab();
    if (tab != null) {
      editPassword(tab.getSelection());
    }
  }

  public void editPassword(PasswordEntry entry) {
    if (entry != null) {
      PasswordEntryDialog dlg = new PasswordEntryDialog(mainWindow.getShell());
      dlg.setEntry(entry);
      PasswordEntry tempEntry = dlg.open();
      if (tempEntry != null) {
        // Kategorie geaendert?
        if (!entry.getCategory().equals(tempEntry.getCategory())) {
          // wenn ja, entfernen des eintrages der alten Kategorie
          // und addieren zur neuen
          mainWindow.getCurrentFile().getEntries(entry.getCategory()).remove(entry);
          mainWindow.getCurrentFile().getEntries(tempEntry.getCategory()).add(entry);
        }

        // Copy der daten vom temp eintrag in den realen 
        entry.clone(tempEntry);
        mainWindow.getCurrentFile().setDirty(true);
        mainWindow.getCurrentTab().refresh();
        mainWindow.refreshTabs();
      }
    }
  }

  public void deletePassword() {
    PasswordFileTab tab = mainWindow.getCurrentTab();
    if (tab != null) {
      PasswordEntry entry = tab.getSelection();
      if (entry != null) {
        // Loeschbestaetigung 
        MessageBox mb = new MessageBox(mainWindow.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
        mb.setMessage("Sind Sie sicher?");
        if (mb.open() == SWT.YES) {
          mainWindow.getCurrentFile().remove(entry);
          tab.refresh();
          mainWindow.refreshTabs();
        }
      }
    }
  }

  public void about() {
    MessageBox mb = new MessageBox(mainWindow.getShell(), SWT.ICON_INFORMATION| SWT.OK);
    mb.setMessage("Passwort\nGeschrieben in SWT");
    mb.open();
  }

  public String getMasterPassword() {
    MasterPasswordInputDialog dlg = new MasterPasswordInputDialog(mainWindow
        .getShell());
    dlg.setMessage("Bitte geben Sie das Master Passwort ein!");
    return dlg.open();
  }

  
  private void showError(String s) {
    MessageBox mb = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR | SWT.OK);
    mb.setMessage(s);
    mb.open();
  }

  public static void main(String[] args) {
    Password.getApp().run();
  }
}
