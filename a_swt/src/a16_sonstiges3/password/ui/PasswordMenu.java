package a16_sonstiges3.password.ui;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.*;

import a16_sonstiges3.password.Password;

public class PasswordMenu {
  Menu menu = null;

  public PasswordMenu(final Shell shell) {

	menu = new Menu(shell, SWT.BAR);

    MenuItem item = new MenuItem(menu, SWT.CASCADE);
    item.setText("Datei");
    Menu dropMenu = new Menu(shell, SWT.DROP_DOWN);
    item.setMenu(dropMenu);

    // Create File->New
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Neu...\tCtrl+N");
    item.setAccelerator(SWT.CTRL + 'N');
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().newFile();
      }
    });

    // Create File->Open
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Oeffnen...\tCtrl+O");
    item.setAccelerator(SWT.CTRL + 'O');
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().openFile();
      }
    });

    // Create File->Save
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Speichern\tCtrl+S");
    item.setAccelerator(SWT.CTRL + 'S');
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().saveFile();
      }
    });

    // Create File->Save As
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Speichern als...");
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().saveFileAs();
      }
    });

    new MenuItem(dropMenu, SWT.SEPARATOR);

    // Create File->Exit
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Beenden\tAlt+F4");
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        shell.close();
      }
    });

    // Create Password
    item = new MenuItem(menu, SWT.CASCADE);
    item.setText("Passwort");
    dropMenu = new Menu(shell, SWT.DROP_DOWN);
    item.setMenu(dropMenu);

    // Create Password->New
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Neu...\tCtrl+N");
    item.setAccelerator(SWT.CTRL + 'N');
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().newPassword();
      }
    });

    // Create Password->Edit
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Bearbeiten...\tCtrl+B");
    item.setAccelerator(SWT.CTRL + 'B');
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().editPassword();
      }
    });

    // Create Password->Delete
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("Loeschen...\tDel");
    item.setAccelerator(SWT.DEL);
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().deletePassword();
      }
    });

    // Create Help
    item = new MenuItem(menu, SWT.CASCADE);
    item.setText("Hilfe");
    dropMenu = new Menu(shell, SWT.DROP_DOWN);
    item.setMenu(dropMenu);

    // Create Help->About
    item = new MenuItem(dropMenu, SWT.NULL);
    item.setText("About\tCtrl+A");
    item.setAccelerator(SWT.CTRL + 'A');
    item.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
        Password.getApp().about();
      }
    });
  }

  /**
   * Gets the underlying menu
   * 
   * @return Menu
   */
  public Menu getMenu() {
    return menu;
  }
}
