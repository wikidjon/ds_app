
package a99a_xml.SAX;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXCrimsonHelloWorld {

	public static void main(String[] args) throws Exception  {
		/*		FileWriter fw = new FileWriter("Test.xml");

		fw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
		fw.write("<methodCall>\n");
		fw.write("	<methodName>primeServer.getPrime</methodName>\n");
		fw.write("	<params>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<int>123</int>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<boolean>1</boolean>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("	</params>\n");
		fw.write("</methodCall>\n");
		fw.flush();
		fw.close();
		 */
		SAXParserFactory saxFactory = javax.xml.parsers.SAXParserFactory.newInstance();
		SAXParser sax = saxFactory.newSAXParser();
		sax.parse(new File("c:/a_xml/people.xml"), new SAXCrimsonHelloWorldHandler());
	}
}
