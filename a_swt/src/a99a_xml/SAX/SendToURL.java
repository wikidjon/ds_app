package a99a_xml.SAX;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class SendToURL {

	public static void main(String[] args) {
		System.out.println("[SendToURL]main()");
		try {
			URL url = new URL("http://www.hs-merseburg.de");
			URLConnection urlConnection = url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());

			String xml_rpc_body = 	"<?xml version=\"1.0\"?>\n"+
					"<methodCall>\n"+
					"<methodName>primeServer.getPrime</methodName>\n"+
					"<params>\n"+
					"<param>\n"+
					"<value><int>2345</int></value>\n"+
					"</param>\n"+
					"<param>\n"+
					"<value><boolean>true</boolean></value>\n"+
					"</param>\n"+
					"</params>\n"+
					"</methodCall>\n";
			wr.write(xml_rpc_body);
			wr.flush();

			// Schreibe die Antwort in die Datei XMLRPCPrimzahl.xml
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line;
			BufferedWriter bw = new BufferedWriter(new FileWriter("XMLRPCPrimzahl.xml"));
			while ((line = rd.readLine()) != null) {
				bw.write(line);
			}
			bw.flush();
			wr.close();
			rd.close();

		} catch(MalformedURLException urlexp) {
			urlexp.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
