package a99a_xml.SAX;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.AttributeList;
import org.xml.sax.HandlerBase;

@SuppressWarnings("deprecation")
public class ListPeopleSAX extends HandlerBase {
	protected static boolean parsingVorname;
	protected static StringBuffer currVorname;

	protected static boolean parsingNachname;
	protected static StringBuffer currNachname;

	protected static boolean parsingWohnort;
	protected static StringBuffer currWohnort;

	public static void main(String[] args) {
		try {

			// lokalisieren der parser factory
			SAXParserFactory factory = SAXParserFactory.newInstance();

			// validierender parser
			factory.setValidating(true);

			// holen eines SAX parser
			SAXParser builder = factory.newSAXParser();

			String filename = "c:/a_xml/student.xml";
			if (args.length > 0)
			{  filename = args[0];}

			HandlerBase eventHandler = new ListPeopleSAX();
			// parsen des file
			builder.parse(new File(filename), eventHandler);
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	@Override
	public void characters(char[] chars, int start, int length) {
		// See if the characters should be appended to one of the name buffers
		if (parsingVorname) {
			currVorname.append(chars, start, length);
		}
		else if (parsingNachname) {
			currNachname.append(chars, start, length);
		}
		else if (parsingWohnort) {
			currWohnort.append(chars, start, length);
		}
	}

	@Override
	public void endElement(String elementName) {
		// wenn ende des personen elements ausgabe der namen
		if (elementName.equals("person")) {
			if (currVorname != null) {
				System.out.print(currVorname.toString());
				System.out.print(" ");
			}
			if (currNachname != null) {
				System.out.print(currNachname.toString());
				System.out.print(" ");
			}
			if (currWohnort != null) {
				System.out.print(currWohnort.toString());
			}
			System.out.println();
		}
		// wenn ende eines der namen anzeigen das keine namen mehr geparst werden
		else if (elementName.equals("vorname")) {
			parsingVorname = false;
		}
		else if (elementName.equals("nachname")) {
			parsingNachname = false;
		}
		else if (elementName.equals("wohnort")) {
			parsingWohnort = false;
		}
	}

	@Override
	public void startElement(String elementName, AttributeList attributes) {
		// wenn das ein personenelement ist, dann alle puffer l�schen
		if (elementName.equals("person")) {
			currVorname = null;
			currNachname = null;
			currWohnort = null;
		}

		// wenn Vorname element dann neuer puffer
		else if (elementName.equals("vorname")) {
			parsingVorname = true;
			currVorname = new StringBuffer();
		}

		// wenn Nachname element dann neuer puffer
		else if (elementName.equals("nachname")) {
			parsingNachname = true;
			currNachname = new StringBuffer();
		}

		// wenn Wohnort element dann neuer puffer
		else
			if (elementName.equals("wohnort")) {
				parsingWohnort = true;
				currWohnort = new StringBuffer();
			}
	}
}
