/*
 * Created on 02.11.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package a99a_xml.SAX;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import org.jdom.Comment;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
@SuppressWarnings("rawtypes")

public class DOMJDOMPrimzahlClient {
	private static final String DEFAULT_HOST_URL =
		"http://localhost:5555/JavaXML5/servlet/parsers.SOAPPrimzahlServlet";
	public static void main(String[] args) {
		System.out.println("[DOMJDOMPrimzahlClient]main()");
		String pServer = DEFAULT_HOST_URL;
		DOMJDOMPrimzahlClient dc = new DOMJDOMPrimzahlClient();
		int iStart = 2345;
//		int iPrim = 0;

		// Aufbau des DOM Dokuments
		Document doc = dc.bildeDocument(iStart);

		// Server
		try {

			URL url = new URL(pServer);
			URLConnection uc = url.openConnection();
			HttpURLConnection connection = (HttpURLConnection) uc;
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			OutputStream out = connection.getOutputStream();

			// Primzahl abfragen
			dc.callServer(doc, out);

			out.flush();
			out.close();

			// Antwort lesen
			InputStream in = connection.getInputStream();
			int iPrime = dc.extrahierePrimzahl(in);

			// Abschluss
			in.close();
			connection.disconnect();
			System.out.println("Startzahl :\t" + iStart);
			System.out.println("Primzahl : \t" + iPrime);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("\nWurde der Tunnel gestartet?\n");
			e.printStackTrace();
		}
	}
	/**
	 * Lesen der Antwort des Primzahl Servers aus dem InputStream
	 * der URLConnection zum Server
	 * @param pIn InputStream von der URLConnection
	 * @return naechste Primzahl zur gesendeten int Zahl
	 */
	private int extrahierePrimzahl(InputStream pIn) {
		System.out.println("[DOMJDOMPrimzahlClient]extrahierePrimzahl()");
		int iPrime = 0;
		// Lesen der Antwort vom Server
			try {
				InputStream in = pIn;
				SAXBuilder parser = new SAXBuilder();
				org.jdom.Document doc = parser.build(in);
				in.close();
				// <params><param><value><int>
				Element el = doc.getRootElement();
				System.out.println("[Root]"+el.getName());
								
				java.util.List params = el.getChildren("params");
				System.out.println("Anzahl Child Elemente: "+params.size());
				Element elParms = (Element)params.get(0);
				System.out.println("[params]"+elParms.getName());
				
				java.util.List param = elParms.getChildren("param");
				System.out.println("Anzahl Child Elemente: "+params.size());
				Element elParm = (Element)param.get(0);
				System.out.println("[param]"+elParm.getName());
				
				java.util.List values = elParm.getChildren("value");
				System.out.println("Anzahl Child Elemente: "+params.size());
				Element elVal = (Element)values.get(0);
				System.out.println("[value]"+elVal.getName());
				
				java.util.List intL = elVal.getChildren("int");
				System.out.println("Anzahl Child Elemente: "+params.size());
				Element elInt = (Element)intL.get(0);
				System.out.println("[params]"+elInt.getName());
				
				System.out.println("[Wert]"+elInt.getText());

				String strPrime=elInt.getText();
				iPrime = Integer.parseInt(strPrime);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JDOMException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return iPrime;
	}
	/**
	 * Kommunikation mit dem Primzahl Server
	 * @param pDoc DOM Document, in dem die XML Anfrage steckt
	 * @param pOut OutputStream zum Server
	 */
	private void callServer(Document pDoc, OutputStream pOut) {
			try {
				System.out.println("[DOMJDOMPrimzahlClient]callServer()");
				OutputStream out = pOut;
				
				XMLOutputter serializer = new XMLOutputter();
				serializer.output(pDoc, out);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	/**
	 * Aufbau eines DOM Baumes (von Hand)
	 * @return DOM Baum als Document
	 */
	private Document bildeDocument(int pStart) {
		System.out.println("[DOMJDOMPrimzahlClient]bildeDocuemnt()");
		Document doc = null;
		/*
		 * 	<?xml version="1.0" encoding="UTF-8" ?> 
			<methodCall>
				<methodName>primeServer.getPrime</methodName>
				<params>
					<param>
						<value><int>2345</int></value>
					</param>
					<param>
						<value><boolean>true</boolean></value>
					</param>
				</params>
			</methodCall>
		 */
		System.out.println("[bildeDocument]Aufbau eines DOM Baumes");
		doc = new Document();
		Element root = new Element("methodCall");
		doc.setRootElement(root);

		// Kommentar einfügen
		Comment comment =
			new Comment("Abfrage der naechst groesseren Primzahl zu einer int");
		doc.addContent(comment);

		// Aufbau des DOM Baumes
		// Achtung : die Reihenfolge des Einfügens ist wichtig
		// sonst erscheint ein Tag zweimal im XML Doc
		Element methodCall = new Element("methodCall");

		Element methodName = new Element("methodName");
		methodName.addContent("primeServer.getPrime");

		Element params = new Element("params");
		Element param1 = new Element("param");
		Element value1 = new Element("value");
		Element intValue = new Element("int");

		intValue.addContent("2345");
		value1.addContent(intValue);
		param1.addContent(value1);
		params.addContent(param1);

		Element param2 = new Element("param");
		Element value2 = new Element("value");
		Element booleanValue = new Element("boolean");

		booleanValue.addContent("true");
		value2.addContent(booleanValue);
		param2.addContent(value2);
		params.addContent(param2);

		methodCall.addContent(methodName);
		methodCall.addContent(params);
		root.addContent(methodCall);

		return doc;

	}
}
