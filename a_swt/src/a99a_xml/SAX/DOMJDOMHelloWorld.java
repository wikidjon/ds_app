/*
 * Created on 02.11.2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package a99a_xml.SAX;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.jdom2.Comment;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
@SuppressWarnings("rawtypes")

public class DOMJDOMHelloWorld {
	private static String tab = "\t";

	/**
	 * Aufbau eines DOM Baumes (von Hand)
	 * @return DOM Baum als Document
	 */
	private static Document buildTree() {
		// Aufbau eines DOM Baumes und Serialisierung in eine Datei
		Document doc = new Document();
		System.out.println("[buildTree]Aufbau eines DOM Baumes");
		/*
		 *     <?xml version="1.0" encoding="UTF-8"?>
		 *		<!--dynamisch generiertes XML -->
		 * 		<Adresse>
		 * 			<Name>Huber<Name/>
		 * 			<Ort>Jona</Ort>
		 * 		</Adresse>
		 */
		// Kommentar einf�gen
		Comment comment = new Comment("dynamisch generiertes XML");
		doc.addContent(comment);

		// Root Element Node : dieses muss zwingend gesetzt werden
		// (im Konstruktor oder mit der Methode)
		Element adresse = new Element("Adresse");
		doc.setRootElement(adresse);

		Element name = new Element("Name");
		adresse.addContent(name);
		name.setText("Huber");
		adresse.addContent("\n");

		Element ort = new Element("Ort");
		ort.setText("Jona");
		adresse.addContent(ort);
		adresse.addContent("\n");

		return doc;
	}
	public static void main(String[] args) throws Exception {
		FileWriter fw = new FileWriter("c:/a_xml/sDOMJDOMOut.xml");

		fw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
		fw.write("<methodCall>\n");
		fw.write("	<methodName>primeServer.getPrime</methodName>\n");
		fw.write("	<params>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<int>123</int>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<boolean>1</boolean>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("	</params>\n");
		fw.write("</methodCall>\n");
		fw.flush();
		fw.close();

		// XML DAtei
		InputStream in = new FileInputStream("DOMJDOMOut.xml");

		// Document
		SAXBuilder parser = new SAXBuilder();
		Document doc = parser.build(in);
		System.out.println("[Document]");
		System.out.println("[Document]Dokumnet-Informationen (Root Element)");
		Element root = doc.getRootElement();
		System.out.println("[Root]getName : " + root.getName());
		System.out.println("[Root]getDocType : " + doc.getDocType());
		System.out.println("[Root]getNamespaceURI : " + root.getNamespaceURI());
		System.out.println("[Root]getNamespace : " + root.getNamespace());

		System.out.println("JDOM kennt keine Node, nur Element");
		System.out.println("[Document]rekursive Analyse");

		// Children : durch alle Child Knoten sausen
		System.out.println("\nRekursives Durchlaufen der Knoten\n");
		DOMJDOMHelloWorld.visitElement(root); // ab root

		// gezielt suchen
		String strInt = root.getAttributeValue("int");
		System.out.println("Inhalt des 'int' Knoten : " + strInt);

		System.out.println("[Document]");
		// Document aufbauen und in eine Datei schreiben
		Document newDoc = DOMJDOMHelloWorld.buildTree();
		DOMJDOMHelloWorld.visitElement(newDoc.getRootElement());
		DOMJDOMHelloWorld.writeXmlFile(newDoc, "DOMausJDOMHelloWorld.xml");
	}
	/**
	 * rekursive Methode, mit der pro Node alle Child Knoten bestimmte werden
	 * (NodeList) und pro Knoten in der NodeList wird jeweils (rekursiv)
	 * diese Methode gleich wieder aufgerufen.
	 * @param pStartNode	Start Knoten (dessen Node List wird bestimmt)
	 * @throws Exception
	 */
	public static void visitElement(Element pStartElement) throws Exception {
		if (pStartElement != null) {
			System.out.println(
					tab
					+ "[StartElement]Element Name : "
					+ pStartElement.getName());
			Element el = pStartElement;
			//			@SuppressWarnings({ "rawtypes"})
			java.util.List li = el.getChildren();
			for (int i = 0; i < li.size(); i++) {
				Element elTemp = (Element) li.get(i);
				System.out.println(tab + "\tElement Name: " + elTemp.getName());
				System.out.println(tab + "\tElement Text: " + elTemp.getText());
				// Rekusrion
				DOMJDOMHelloWorld.visitElement(elTemp);
			}
		}
	}

	/**
	 * Serialisierung eines DOM Document in eine XML Datei
	 * Dies geschieht mit Hilfe der transform() Methode
	 * @param doc		DOM Document
	 * @param filename	Dateiname
	 */
	public static void writeXmlFile(Document pDoc, String pFilename) {
		try {
			System.out.println(
					"[writeXMLFile]Ausgabe eines Document in die Datei "
							+ pFilename);

			// Ausgabedatei vorbereiten
			File file = new File(pFilename);
			OutputStream out = new FileOutputStream(file);

			// JDOM : DOM -> OutputStream
			XMLOutputter xmlout = new XMLOutputter();

			// DOM in Datei speichern
			xmlout.output(pDoc, out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
