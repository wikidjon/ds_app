package a99a_xml.SAX;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class SAXXercesHelloWorld {

	public static void main(String[] args) throws Exception  {
		FileWriter fw = new FileWriter("Test.xml");		
		
		fw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
		fw.write("<methodCall>\n");
		fw.write("	<methodName>primeServer.getPrime</methodName>\n");
		fw.write("	<params>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<int>123</int>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("		<param>\n");
		fw.write("			<value>\n");
		fw.write("				<boolean>1</boolean>\n");
		fw.write("			</value>\n");
		fw.write("		</param>\n");
		fw.write("	</params>\n");
		fw.write("</methodCall>\n");
		fw.flush();
		fw.close();
		
		// Lesen des XML Dokuments
		XMLReader parser = XMLReaderFactory.createXMLReader(
		  "org.apache.xerces.parsers.SAXParser"
		);
		org.xml.sax.ContentHandler handler 
		 = new SAXXercesHelloWorldHandler();
		parser.setContentHandler(handler);
    
		InputStream in = new FileInputStream("Test.xml");
		InputSource source = new InputSource(in);
		parser.parse(source);
	}
}
