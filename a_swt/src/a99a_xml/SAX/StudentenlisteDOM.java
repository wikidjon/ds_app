package a99a_xml.SAX;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
//baumorganisation
public class StudentenlisteDOM {
	// handeln aller texteintraege unter einem element als string
	public static String getElementText(Element elem) {
		// System.out.print("[texteintrag] ");

		StringBuffer buff = new StringBuffer();

		// holen der childs eines elements
		NodeList list = elem.getChildNodes();

		for (int i=0; i < list.getLength(); i++) {
			Node item = list.item(i);

			// wenn child ein texteintrag ist addieren zum puffer
			if (item instanceof Text) {
				Text charItem = (Text) item;
				buff.append(charItem.getData());
			}
		}
		return buff.toString();
	}

	public static void main(String[] args) {
		try {

			//instanz einer parser factory - stellt einer app einen parser zur verfuegung
			//spezielle Klasse zum Erzeugen von Klassen (z.B. bei dynamischer
			//Anzahl von Klassen
			DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();

			// initialisieren eines validierenden parsers
			factory.setValidating(true);

			// holen eines dom parsers
			DocumentBuilder builder = factory.newDocumentBuilder();
			// SAXParser sParser = factory.newSAXParser();

			String filename = "c:/a_xml/student.xml";

			// xml Datei als Kommandozeilenparameter
			if (args.length > 0) {
				filename = args[0];
			}

			// parsen der Datei
			Document doc = builder.parse(new File(filename));
			// root element holen
			Element studentElement = doc.getDocumentElement();
			System.out.print("root-element [" + studentElement.getNodeName() + "]\n");

			// child elemente des root elements holen
			//----------------------------------------------
			NodeList studentList = studentElement.getChildNodes();  //von org.w3c.dom.Node

			for (int i=0; i < studentList.getLength(); i++) {
				Node item = studentList.item(i);
				// -------------------------------------------------
				System.out.print(" [" + item.getNodeName() + "] ");

				// überspringen der items die keine elemente sind
				if (item.getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}

				Element person = (Element) item;
				//-------------------------------------------------
				// holen vorname
				NodeList vorName = person.getElementsByTagName("vorname");

				if (vorName.getLength() > 0) {
					System.out.print(getElementText((Element) vorName.item(0)));
					// aus Document
					System.out.print(' ');
				}
				//-------------------------------------------------
				// holen nachnamen
				NodeList nachName = person.getElementsByTagName("nachname");

				if (nachName.getLength() > 0) {
					System.out.print(getElementText((Element) nachName.item(0)));
					System.out.print(' ');
				}
				//-------------------------------------------------
				// holen wohnorte
				NodeList wohnort = person.getElementsByTagName("wohnort");

				if (wohnort.getLength() > 0) {
					System.out.print(getElementText((Element) wohnort.item(0)));
					System.out.print(' ');
				}
				System.out.println();
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
