package a99_sonstiges.ch10;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class datenspeichern {

	public static void main (String [] args) {
		
		// eine Shell
		Display display = new Display ();
		Shell shell = new Shell(display);
		shell.setSize(400, 400);
		
		// ein Textfeld
		final Text textFeld = new Text(shell, SWT.MULTI);
		textFeld.setBounds(10, 10, 300, 100);
		
		final Text textFeld2 = new Text(shell, SWT.MULTI);
		textFeld2.setBounds(10, 180, 300, 100);
				
		// noch ein Textfeld für den Speicherpfad
		final Text speicherPfad = new Text(shell, SWT.SINGLE);
		speicherPfad.setBounds(10, 120, 300, 20);
		speicherPfad.setText("H:/ProgrammierungSS09/daten.sav");
		
		// der Knopf zum Speichern
		Button speichernKnopf = new Button(shell, SWT.PUSH);
		speichernKnopf.setBounds(10, 150, 100, 20);
		speichernKnopf.setText("Speichern");
		
		// Listener an den Knopf
		speichernKnopf.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				speichern(textFeld.getText(), speicherPfad.getText());
				
			}
		});
		
		
		shell.open ();
		
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		display.dispose (); 
	}

	
	
	public static void speichern(String text, String pfad) {
		try
	      {
		   //****************************************************************************
	         // Alle Mitarbeiterdatensätze in die Datei daten.sav schreiben
		   //****************************************************************************
	         DataOutputStream out = new DataOutputStream(new FileOutputStream(pfad));
	         writeFString(text, text.length(), out);
	         out.close();
	      }
	      catch(IOException e)
	      {
	         e.printStackTrace();
	      }
	}

	public static void writeFString(String s, int size, DataOutput out) throws IOException  {
      int i;
      for (i = 0; i < size; i++)
      {
         char ch = 'a';
         if (i < s.length()) ch = s.charAt(i);
         out.writeChar(ch);
         System.out.println(ch);
      }
   }
}