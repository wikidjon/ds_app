package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public final class a20_border  {

	public int spacing = 3;
	
	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell();
		shell.setText("BorderLayout Demo");
//		shell.setLayout(new a20_BorderLayout());
		
		@SuppressWarnings("unused")
		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Control c = (Control) e.getSource();
				Composite p = c.getParent();
				c.dispose();p.layout();
			}
		};

		Composite composite1 = new Composite(shell,SWT.NONE);
		composite1.setLayoutData("NORTH");
		composite1.setBounds(1,1,22,22);
		Label label1 = new Label(composite1,SWT.NONE);
		label1.setSize(250,20);
		label1.setLocation(10,5);
		label1.setText("hier k�nnen andere Widgets stehen ");
		
		Composite composite2 = new Composite(shell,SWT.NONE);
		composite2.setLayoutData("SOUTH");
		Group group2 = new Group(composite2, SWT.NONE); 
		group2.setBounds(5,5,200,40);    //20
		group2.setText("Gruppe SOUTH");
		
		Composite composite3 = new Composite(shell,SWT.NONE);
		composite3.setBackground(new Color(display,31,233,31));
		composite3.setLayoutData("CENTER");
		
		Composite composite4 = new Composite(shell,SWT.BORDER);
		composite4.setLayoutData("EAST");
		Label label4 = new Label(composite4,SWT.NONE);
		label4.setSize(50,20);
		label4.setLocation(10,5);
		label4.setText("EAST ");
		
		Composite composite5 = new Composite(shell,SWT.BORDER);
		composite5.setLayoutData("WEST");
		Label label5 = new Label(composite5,SWT.NONE);
		label5.setSize(50,20);
		label5.setLocation(10,5);
		label5.setText("WEST ");
		

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}