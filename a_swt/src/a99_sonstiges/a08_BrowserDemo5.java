package a99_sonstiges;


/*
 * Browser example snippet: render HTML that includes relative links from memory
 *
 * For a list of all SWT example snippets see
 * http://www.eclipse.org/swt/snippets/
 * 
 * @since 3.0
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a08_BrowserDemo5 {

public static void main(String [] args) {
	/* Relative links: use the HTML base tag */
	String html = "<html><head>"+
		"<base href=\"http://www.eclipse.org/swt/\" >"+
		"<title>HTML Test</title></head>"+
		"<body><a href=\"faq.php\">local link</a></body></html>";

	Display display = new Display();
	Shell shell = new Shell(display);
	shell.setLayout(new FillLayout());
	Browser browser = new Browser(shell, SWT.NONE);
	browser.setText(html);
	shell.open();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch())
			display.sleep();
	}
	display.dispose();
}
}
