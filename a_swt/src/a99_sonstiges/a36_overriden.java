package a99_sonstiges;
//package a03_buttons;


/*
 * Override the text that is spoken for a native Button.
 *
 * For a list of all SWT example snippets see
 * http://www.eclipse.org/swt/snippets/
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a36_overriden {

public static void main(String[] args) {
	Display display = new Display();
	Shell shell = new Shell(display);
	shell.setBounds(10, 10, 200, 200);

	Button button1 = new Button (shell, SWT.PUSH);
	button1.setText("&Typical button");
	button1.setBounds(10,10,180,30);
	Button button2 = new Button (shell, SWT.PUSH);
	button2.setText("&Overidden button");
	button2.setBounds(10,50,180,30);
	button2.getAccessible().addAccessibleListener(new AccessibleAdapter() {
		public void getName(AccessibleEvent e) {
			e.result = "Speak this instead of the button text";
		}
	});
	
	shell.open();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch()) display.sleep();
	}
	display.dispose();
}
}
