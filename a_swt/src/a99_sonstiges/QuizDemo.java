package a99_sonstiges;
/*
 * (C) 2002 by 3plus4software GbR. All rights reserved.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class QuizDemo {

	Display display;
	private Shell shell;
	private Label question;
	private Button[] answer;

	public QuizDemo() {
		display = new Display();
		shell = new Shell(display);
		shell.setText("The Quiz");
		shell.setSize(800, 400);
		shell.setLayout(new GridLayout(2, true));

		final Font font = new Font(display, "Arial", 30, SWT.NORMAL);
		GridData data;

		question = new Label(shell, SWT.CENTER);
		question.setFont(font);
		data = new GridData();
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.FILL;
		data.horizontalSpan = 2;
		question.setLayoutData(data);

		answer = new Button[4];
		for (int i = 0; i < 4; i++) {
			answer[i] = new Button(shell, SWT.PUSH);
			answer[i].setFont(font);
			data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			answer[i].setLayoutData(data);
			answer[i].setData(new Integer(i));
			answer[i].addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					theAnswerIs(((Button) e.widget).getText());
				}
			});
		}

		chooseQuestion();

		shell.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				font.dispose();
			}
		});
	}

	private static final String[] questions =
		{
			"Wie lautet die 'magische' Kennung eines Classfiles?|CAFEBABE|CAFEBABY|COFFEEBABE|JAVABABE",
			"Wann wurde Java das erste Mal veröffentlicht?|1995|1991|1993|1997",
			"Was ist Java nicht?|Teesorte|Programmiersprache|Kaffeesorte|Insel",
			};

	private Random r = new Random();
	private String correctAnswer;

	private void chooseQuestion() {
		String q = questions[r.nextInt(questions.length)];
		StringTokenizer st = new StringTokenizer(q, "|");
		question.setText(st.nextToken());
		List<String> a = new ArrayList<String>(4);
		while (st.hasMoreTokens()) {
			a.add(st.nextToken());
		}
		correctAnswer = (String) a.get(0);
		Collections.shuffle(a, r);
		for (int i = 0; i < 4; i++) {
			answer[i].setText((String) a.get(i));
		}
	}

	private void theAnswerIs(String a) {
		final boolean correct = correctAnswer.equals(a);
		final String oldQuestion = question.getText();
		question.setText(correct ? "Das war richtig!" : "Leider falsch!");
		display.timerExec(1500, new Runnable() {
			public void run() {
				if (correct) {
					chooseQuestion();
				} else {
					question.setText(oldQuestion);
				}
			}
		});
	}

	public void run() {
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	public static void main(String[] arguments) {
		new QuizDemo().run();
	}
}