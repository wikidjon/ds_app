package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a01_starta {

public static void main(String[] args) {

	final Display display = new Display();
//	final Image image = new Image(display, "splash.jpg");
	final Shell splash = new Shell(display);
	splash.setLayout(new FillLayout());
	
	Label label = new Label(splash, SWT.NONE);
//	label.setImage(image);
	
	Shell  shell = new Shell(display);
	shell.setLayout(new FillLayout());
	
	a99_menue2 men = new a99_menue2();
	men.menue2(shell, display, shell);

	// Verzoegerung beim Slash
/*	for (int i=0; i<zaehler; i++) {
		bar.setSelection(i+1);
		try {Thread.sleep(100);} catch (Throwable e) {}
	}
*******************************/
	splash.close();
//	image.dispose();
	shell.open();
	
	while(!shell.isDisposed())
	     if (!display.readAndDispatch())
	           display.sleep();
	display.dispose();
	label.dispose();
	}
}


