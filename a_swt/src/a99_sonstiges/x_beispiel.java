package a99_sonstiges;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class x_beispiel {
	Display display = new Display();
	Shell shell = new Shell(display);

	public static void main(String[] args) {
		new x_beispiel();
	}

	public x_beispiel() {
		shell.setText("Bucheintrag");

		GridLayout gridLayout = new GridLayout(4, false);
		gridLayout.verticalSpacing = 8;

		shell.setLayout(gridLayout);

		// Titel
		Label label = new Label(shell, SWT.NULL);
		label.setText("Titel: ");

		Text title = new Text(shell, SWT.SINGLE | SWT.BORDER);
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridData.horizontalSpan = 3;
		title.setLayoutData(gridData);

		// Autor
		label = new Label(shell, SWT.NULL);
		label.setText("Autor/en: ");

		Text authors = new Text(shell, SWT.SINGLE | SWT.BORDER);
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridData.horizontalSpan = 3;
		authors.setLayoutData(gridData);

		// Bild
		label = new Label(shell, SWT.NULL);
		label.setText("Bild: ");

		gridData = new GridData();
		gridData.verticalSpan = 3;
		label.setLayoutData(gridData);

		CLabel bild = new CLabel(shell, SWT.NULL);

		gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 1;
		gridData.verticalSpan = 3;
		gridData.heightHint = 100;
		gridData.widthHint = 100;

		bild.setLayoutData(gridData);

		// weitere Daten
		label = new Label(shell, SWT.NULL);
		label.setText("Seiten");

		Text pages = new Text(shell, SWT.SINGLE | SWT.BORDER);
		pages.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		label = new Label(shell, SWT.NULL);
		label.setText("Verlag");

		Text pubisher = new Text(shell, SWT.SINGLE | SWT.BORDER);
		pubisher.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		label = new Label(shell, SWT.NULL);
		label.setText("Wertung");

		Combo rating = new Combo(shell, SWT.READ_ONLY);
		rating.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		rating.add("5");
		rating.add("4");
		rating.add("3");
		rating.add("2");
		rating.add("1");

		// Abstract
		label = new Label(shell, SWT.NULL);
		label.setText("Inhalt:");

		Text bookAbstract =	new Text(shell,	SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		gridData =	new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_FILL);
		gridData.horizontalSpan = 3;
		gridData.grabExcessVerticalSpace = true;

		bookAbstract.setLayoutData(gridData);

		Button enter = new Button(shell, SWT.PUSH);
		enter.setText("Auswahl");

		gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.horizontalAlignment = GridData.END;
		enter.setLayoutData(gridData);

		title.setText("Java ist auch eine Insel");
		authors.setText("Robinson Crusoe");
		pages.setText("500");
		pubisher.setText("Kinderbuchverlag");
		bild.setBackground(new Image(display, "images/splash.jpg"));
		bookAbstract.setText("Dieses Buch bietet einen guten und kompakten \n"
				+ "�berblick zur Programmierung von Java GUIs mit SWT/JFace. ");

		shell.pack();
		shell.open();

		// Set up the event loop.
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				// If no more entries in event queue
				display.sleep();
			}
		}

		display.dispose();
	}

}
