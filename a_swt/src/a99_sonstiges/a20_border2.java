package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public final class a20_border2  {

	public int spacing = 3;
	
	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell();
		shell.setText("BorderLayout Demo");
//		shell.setLayout(new a20_BorderLayout());

		@SuppressWarnings("unused")
		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Control c = (Control) e.getSource();
				Composite p = c.getParent();
				c.dispose();
				p.layout();
			}
		};

		Composite composite1 = new Composite(shell,SWT.NONE);
		composite1.setLayoutData("NORTH");
		composite1.setBounds(1,1,22,22);
		
		Label label1 = new Label(composite1,SWT.NONE);
		label1.setSize(250,20);
		label1.setLocation(10,5);
		label1.setText("hier k�nnen andere Widgets stehen ");
		
		Composite composite2 = new Composite(shell,SWT.NONE);
		composite2.setLayoutData("SOUTH");
		
		Group group2 = new Group(composite2, SWT.NONE); 
		group2.setBounds(5,5,200,40);
		group2.setText("Gruppe SOUTH");
		
		Composite composite3 = new Composite(shell,SWT.NONE);
		//composite3.setBackground(new Color(display,31,233,31));
		//composite3.setLayout(new a20_border_layout);
		composite3.setLayoutData("CENTER");
//		composite3.setLayout(new a20_BorderLayout());
		
		// zum CENTER neues FILL-Layout hinzufuegen
		// !!!!verschachtelte Layouts
		FillLayout fillLayout = new FillLayout ();
		composite3.setLayout (fillLayout);
		
		Button button0 = new Button (composite3, SWT.PUSH);
		button0.setText ("button0");
		Button button1 = new Button (composite3, SWT.PUSH);
		button1.setText ("button1");
		Button button2 = new Button (composite3, SWT.PUSH);
		button2.setText ("button2");
		//Button button3 = new Button (composite3, SWT.PUSH);
		//button3.setText ("button3");
		
		
		//*******************
				
		Composite composite4 = new Composite(shell,SWT.BORDER);
		composite4.setLayoutData("EAST");

		Label label4 = new Label(composite4,SWT.NONE);
		label4.setSize(50,20);
		label4.setLocation(10,5);
		label4.setText("EAST ");
		
		Composite composite5 = new Composite(shell,SWT.BORDER);
		//composite1.setBounds(10,30,270,250);
		//composite5.setBackground(new Color(display,31,233,31));
		//composite1.setForeground(red);
		composite5.setLayoutData("WEST");

		Label label5 = new Label(composite5,SWT.NONE);
		label5.setSize(50,20);
		label5.setLocation(10,5);
		label5.setText("WEST ");

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}