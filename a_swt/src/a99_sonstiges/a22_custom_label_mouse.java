package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a22_custom_label_mouse extends Canvas {

	private boolean hover;
	private String text;

	a22_custom_label_mouse(Composite parent) {
		super(parent, SWT.NONE);

		final Cursor hand = new Cursor(Display.getCurrent(), SWT.CURSOR_HAND);
		setCursor(hand);

		addMouseTrackListener(new MouseTrackAdapter() {
			public void mouseEnter(MouseEvent e) {
				setHover(true);
			}
			public void mouseExit(MouseEvent e) {
				setHover(false);
			}
		});
		addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				a22_custom_label_mouse.this.paintControl(e);
			}
		});
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				hand.dispose();
			}
		});
	}

	private void setHover(boolean hover) {
		this.hover = hover;
		redraw();
	}

	private void paintControl(PaintEvent e) {
		Rectangle bounds = getBounds();
		e.gc.setBackground(getBackground());
		e.gc.fillRectangle(0, 0, bounds.width, bounds.height);
		if (text != null) {
			e.gc.setFont(e.display.getSystemFont());
			Point extent = e.gc.stringExtent(text);
			e.gc.drawString(
				text,
				(bounds.width - extent.x) / 2,
				(bounds.height - extent.y) / 2);
		}
		if (hover) {
			e.gc.setForeground(
				e.display.getSystemColor(SWT.COLOR_WIDGET_BORDER));
			e.gc.drawRectangle(0, 0, bounds.width - 1, bounds.height - 1);
		}
	}

	public Point computeSize(int wHint, int hHint, boolean changed) {
		if (text != null) {
			Display display = Display.getCurrent();
			GC gc = new GC(display);
			gc.setFont(display.getSystemFont());
			Point size = gc.stringExtent(text);
			gc.dispose();
			size.x += 4;
			size.y += 4;
			return size;
		}
		return new Point(
			wHint == SWT.DEFAULT ? 32 : wHint,
			hHint == SWT.DEFAULT ? 16 : hHint);
	}

	public void setText(String text) {
		this.text = text;
		redraw();
	}

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new RowLayout());

		a22_custom_label_mouse b;
		b = new a22_custom_label_mouse(shell);
		b.setText("Bewege deine Maus");
		b = new a22_custom_label_mouse(shell);
		b.setText("�ber den Text");

		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
