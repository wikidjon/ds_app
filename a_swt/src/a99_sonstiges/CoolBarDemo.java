package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class CoolBarDemo {

	public static Display myDisplay;
	public static boolean internalCall = false;

	public static void main(String[] args) {
		internalCall = true;
		myDisplay = new Display();
		CoolBarDemo tbd = new CoolBarDemo();
		tbd.runDemo(myDisplay);
	}

	public void runDemo(Display display) {

		myDisplay = display;
		Shell shell = new Shell(display);
		shell.setText("CoolBar Demo");

		//create a coolbar
		final CoolBar bar = new CoolBar(shell, SWT.BORDER);

		//create 4 coolitems to place on it		
		CoolItem item1 = new CoolItem(bar, SWT.NONE);
		CoolItem item2 = new CoolItem(bar, SWT.NONE);
		CoolItem item3 = new CoolItem(bar, SWT.NONE);
//		CoolItem item4 = new CoolItem(bar, SWT.NONE, 2);		

		//create a button to put on one of the coolitems
		Button button1 = new Button(bar, SWT.FLAT | SWT.BORDER);
		button1.setText("Button");
		button1.pack();

		//add a listener to the button.
		//when pressed, it will "lock" (and then unlock) the coolbar
		//so the coolitems cannot be moved
		button1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				bar.setLocked(!bar.getLocked())	;
			}
		});

		//create another button for a different coolitem
		Button button2 = new Button(bar, SWT.PUSH);
		button2.setText("Another button");
		button2.pack();

		//create a toolbar to place on yet another coolitem
		ToolBar tools = new ToolBar(bar, SWT.NONE);
		ToolItem b1 = new ToolItem(tools, SWT.FLAT);
		b1.setText("Tool");
		ToolItem b2 = new ToolItem(tools, SWT.FLAT);
		b2.setText("Bar");
		tools.pack();

		//set the controls to appear
		//on each coolitem, and set the size 
		//of the coolitems
		Point size = button1.getSize();	
		item1.setControl(button1);
		item1.setSize(item1.computeSize(size.x, size.y));

		size = button2.getSize();					
		item2.setControl(button2);
		item2.setSize(item2.computeSize(size.x, size.y));

		size = tools.getSize();
		item3.setControl(tools);
		item3.setSize(item3.computeSize(size.x, size.y));
		//set a minimum size for the coolitem
		item3.setMinimumSize(size);

		//wrap to the next line after the 3 item
		bar.setWrapIndices(new int[] {3});

		bar.setSize(300, 120);

		shell.setSize(320,150);
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		if (internalCall) display.dispose();

	}

}
