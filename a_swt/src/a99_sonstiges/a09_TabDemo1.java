package a99_sonstiges;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

public class a09_TabDemo1 {

	public static Display display;
//	public static boolean internalCall = false;

	public static void main(String[] args) {
//		internalCall = true;
		display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300,300);
		shell.setText("Tab Demo");

		TabFolder tabFolder1 = new TabFolder(shell,SWT.NONE);
		tabFolder1.setBounds(10,10,270,250);
	
		//Button Tab
		Composite buttonComp = new Composite(tabFolder1,SWT.NONE);
		Button button1 = new Button(buttonComp,SWT.PUSH);
		button1.setSize(80,20);
		button1.setText("Hallo");
		button1.setLocation(10,10);
		Button button2 = new Button(buttonComp,SWT.ARROW);
		button2.setBounds(100,0,20,20);

		TabItem item1 = new TabItem(tabFolder1,SWT.NONE);
		item1.setText("Buttons");
		item1.setControl(buttonComp);

		//Label Tab
		Composite labelComp = new Composite(tabFolder1,SWT.NONE);
		Label label1 = new Label(labelComp,SWT.NONE);
		label1.setText("Label 1");
		label1.setBounds(10,10,250,20);
		Label label2 = new Label(labelComp,SWT.NONE);
		label2.setText("Label 2");
		label2.setBounds(10,40,200,20);

		TabItem item2 = new TabItem(tabFolder1,SWT.NONE);
		item2.setText("Labels");
		item2.setControl(labelComp);

		// ???? Tab
		Composite testComp = new Composite(tabFolder1,SWT.NONE);
		Label label3 = new Label(testComp,SWT.NONE);
		label3.setText("Hallo");
		label3.setBounds(10,10,250,20);
		
		TabItem item3 = new TabItem(tabFolder1,SWT.NONE);
		item3.setText("???");
		item3.setControl(testComp);

		shell.open();

		while(!shell.isDisposed()){
		if(!display.readAndDispatch())
			display.sleep();
		}
		//if (internalCall) 
		display.dispose();

	}
}
