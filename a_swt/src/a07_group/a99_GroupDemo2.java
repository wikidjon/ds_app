package a07_group;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

public class a99_GroupDemo2 {

public static void main (String [] args) {
	Display display = new Display ();
	final Image image = new Image (display, 20, 20);
	Color color = display.getSystemColor (SWT.COLOR_RED);
	GC gc = new GC (image);
	gc.setBackground (color);
	gc.fillRectangle (image.getBounds ());
	gc.dispose ();

	Shell shell = new Shell (display);
	shell.setLayout (new FillLayout ());
	Group group = new Group (shell, SWT.NONE);
	group.setLayout (new FillLayout ());
	group.setText ("Gruppe");
	Canvas canvas = new Canvas (group, SWT.NONE);
	canvas.addPaintListener (new PaintListener () {
		public void paintControl (PaintEvent e) {
			e.gc.drawImage (image, 0, 0);
		}
	});

	shell.pack ();
	shell.open ();
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ())
			display.sleep ();
	}
	image.dispose ();
	display.dispose ();
}
}
