package a07_group.AdressBuch;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class Addressbuch {

	public static void main(String[] args) {
		Addressbuch ab = new Addressbuch();
		ab.start();
	}
	Shell 	shell;
	Text 	sonstiges;
	Text 	addrText;
	Text 	emailText;
	Text 	telefonText;
	Text 	NachnameText;
	Text 	VornameText;
	Button 	AbbruchButton;
	Button 	SpeichernButton;
	Button 	NaechsterButton;
	Button 	VorherButton;
	Button 	EditButton;
	Button 	LoeschenButton;

	Button 	NeuButton;

	private void enableEditButtons(boolean enable) {
		this.NeuButton.setEnabled(enable);
		this.LoeschenButton.setEnabled(enable);
		this.EditButton.setEnabled(enable);
	}

	private void enableSpeichernButtons(boolean enable) {
		this.SpeichernButton.setEnabled(enable);
		this.AbbruchButton.setEnabled(enable);
	}

	private void loescheTextfeld() {
		this.VornameText.setText("");
		this.NachnameText.setText("");
		this.telefonText.setText("");
		this.emailText.setText("");
		this.addrText.setText("");
		this.sonstiges.setText("");
	}

	private void setTextEditable(boolean editable) {
		this.VornameText.setEditable(editable);
		this.NachnameText.setEditable(editable);
		this.telefonText.setEditable(editable);
		this.emailText.setEditable(editable);
		this.addrText.setEditable(editable);
		this.sonstiges.setEditable(editable);
	}

	private void setupMenu() {
		Menu menu = new Menu(this.shell, SWT.BAR);
		this.shell.setMenuBar(menu);

		MenuItem file = new MenuItem(menu, SWT.CASCADE);
		file.setText("Datei");

		Menu filemenu = new Menu(file);
		file.setMenu(filemenu);

		MenuItem prevItem = new MenuItem(filemenu, SWT.NONE);
		prevItem.setText("Vorheriger DS");

		MenuItem nextItem = new MenuItem(filemenu, SWT.PUSH);
		nextItem.setText("Naechster DS");

		//		MenuItem separator =
		new MenuItem(filemenu, SWT.SEPARATOR);

		MenuItem quitItem = new MenuItem(filemenu, SWT.PUSH);
		quitItem.setText("Beenden");

		//Listener anbinden
		prevItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Vorheriger DS -Menueitem ausgewaehlt.");
			}
		});

		nextItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				System.out.println("Naechster DS -Menueitem ausgewaehlt.");
			}
		});

		quitItem.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				Addressbuch.this.shell.dispose();
			}
		});
	}

	public void start() {
		Display display = new Display();
		this.shell = new Shell();
		this.shell.setSize(610, 477);
		this.shell.setText("Kontakte");
		{
			this.NeuButton = new Button(this.shell, SWT.NONE);
			this.NeuButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					loescheTextfeld();
					setTextEditable(true);
					enableEditButtons(false);
					enableSpeichernButtons(true);

					System.out.println("NEU Button ausgewaehlt.");  }	});
			this.NeuButton.setBounds(10, 380, 75, 35);
			this.NeuButton.setText("Neu");
		}
		//*********************************************
		{
			this.LoeschenButton = new Button(this.shell, SWT.NONE);
			this.LoeschenButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					loescheTextfeld();

					System.out.println("LOESCHEN Button ausgewaehlt.");
				}		});
			this.LoeschenButton.setBounds(85, 380, 75, 35);
			this.LoeschenButton.setText("Loeschen");
		}
		//*************************************************
		{
			this.EditButton = new Button(this.shell, SWT.NONE);
			this.EditButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setTextEditable(true);
					enableEditButtons(false);
					enableSpeichernButtons(true);

					System.out.println("Edit Button ausgewaehlt.");
				}
			});
			this.EditButton.setBounds(160, 380, 75, 35);
			this.EditButton.setText("Edit");
		}
		//****************************************************
		{
			this.VorherButton = new Button(this.shell, SWT.NONE);
			this.VorherButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					System.out.println("Vorher Button ausgewaehlt.");
				}
			});
			this.VorherButton.setBounds(265, 380, 75, 35);
			this.VorherButton.setText("Vorher");
		}
		//****************************************************
		{
			this.NaechsterButton = new Button(this.shell, SWT.NONE);
			this.NaechsterButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					System.out.println("Naechster Button ausgewaehlt.");
				}
			});
			this.NaechsterButton.setBounds(340, 380, 75, 35);
			this.NaechsterButton.setText("Naechster");
		}
		//****************************************************
		{
			this.SpeichernButton = new Button(this.shell, SWT.NONE);
			this.SpeichernButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setTextEditable(false);
					enableEditButtons(true);
					enableSpeichernButtons(false);

					System.out.println("Speichern Button ausgewaehlt.");
				}
			});
			this.SpeichernButton.setBounds(445, 380, 75, 35);
			this.SpeichernButton.setText("Speichern");
			this.SpeichernButton.setEnabled(false);
		}
		{
			this.AbbruchButton = new Button(this.shell, SWT.NONE);
			this.AbbruchButton.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					setTextEditable(false);
					enableEditButtons(true);
					enableSpeichernButtons(false);

					System.out.println("Abbruch Button ausgewaehlt.");
				}
			});
			this.AbbruchButton.setBounds(520, 380, 75, 35);
			this.AbbruchButton.setText("Abbruch");
			this.AbbruchButton.setEnabled(false);
		}
		{
			Group group = new Group(this.shell, SWT.NONE);
			group.setText("Details");
			group.setBounds(10, 10, 585, 355);
			{
				Label label = new Label(group, SWT.NONE);
				label.setBounds(10, 20, 135, 25);
				label.setText("Vorname:");
			}
			{
				Label label = new Label(group, SWT.NONE);
				label.setBounds(10, 60, 135, 25);
				label.setText("Nachname:");
			}
			{
				Label label = new Label(group, SWT.NONE);
				label.setBounds(10, 100, 135, 25);
				label.setText("Telefon:");
			}
			{
				Label label = new Label(group, SWT.NONE);
				label.setBounds(10, 140, 135, 25);
				label.setText("Email:");
			}
			{
				Label label = new Label(group, SWT.NONE);
				label.setBounds(10, 180, 135, 25);
				label.setText("Adresse:");
			}
			{
				Label label = new Label(group, SWT.NONE);
				label.setBounds(10, 255, 135, 25);
				label.setText("weitere Daten:");
			}
			{
				this.VornameText = new Text(group, SWT.BORDER | SWT.READ_ONLY);
				this.VornameText.setBounds(150, 15, 420, 25);
			}
			{
				this.NachnameText = new Text(group, SWT.BORDER | SWT.READ_ONLY);
				this.NachnameText.setBounds(150, 55, 420, 25);
			}
			{
				this.telefonText = new Text(group, SWT.BORDER | SWT.READ_ONLY);
				this.telefonText.setBounds(150, 95, 420, 25);
			}
			{
				this.emailText = new Text(group, SWT.BORDER | SWT.READ_ONLY);
				this.emailText.setBounds(150, 135, 420, 25);
			}
			{
				this.addrText = new Text(group, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
				this.addrText.setBounds(150, 175, 420, 60);
			}
			{
				this.sonstiges = new Text(group, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
				this.sonstiges.setBounds(150, 250, 420, 65);
			}
		}

		setupMenu();

		this.shell.open();
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}