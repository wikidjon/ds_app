package a07_group;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

public class a07_Gruppe4 {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout());

		Group group1 = new Group(shell, SWT.SHADOW_IN);
		group1.setText("Wer war der Kopf der Beatles");
		group1.setLayout(new RowLayout(SWT.VERTICAL));
		new Button(group1, SWT.RADIO).setText("John Lennon");
		new Button(group1, SWT.RADIO).setText("Paul McCartney");
		new Button(group1, SWT.RADIO).setText("George Harrison");
		new Button(group1, SWT.RADIO).setText("Ringo Star");

		final Group group2 = new Group(shell, SWT.SHADOW_NONE);
		//Group group2 = new Group(shell, SWT.NO_RADIO_GROUP);
		group2.setText("Wer geh�rt zu den Stones?");
		group2.setLayout(new RowLayout(SWT.HORIZONTAL));
		new Button(group2, SWT.RADIO).setText("Barry Gibb");
		new Button(group2, SWT.RADIO).setText("Mick Jagger");
		new Button(group2, SWT.RADIO).setText("Erich Honnecker");

		System.out.println(group2.getText());
		//Listener anh�ngen....

		shell.open();
		shell.pack();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}