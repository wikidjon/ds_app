package a07_group;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.*;

public class a07_Gruppe0 {

	public static void main(String[] args) {
		Display d;
		Shell s;

		d = new Display();
		s = new Shell(d);
		s.setSize(200, 200);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Ein Group Shell Beispiel");
		
		final a07_Gruppe1 ge = new a07_Gruppe1(s, SWT.SHADOW_ETCHED_IN, "Hallo");
		ge.setLocation(20, 20);
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch())
				d.sleep();
		}
		d.dispose();
	}
}
