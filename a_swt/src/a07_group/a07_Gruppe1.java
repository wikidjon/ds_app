package a07_group;
//import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

public class a07_Gruppe1 extends Composite {

	public static void main(String[] args) {
		Display d = new Display();
		Shell  s = new Shell(d);
		s.setSize(250,275);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Ein Group Composite Beispiel");

		Composite composite1 = new Composite(s,SWT.BORDER);
		composite1.setBounds(10,30,170,150);
		// Randbegrenzung
		//composite1.setBackground(new Color(d,31,133,31));

		new a07_Gruppe1(composite1, 1, "Optionen");

		s.open();
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
	final Button b1;
	final Button b2;

	final Button b3;

	public a07_Gruppe1(Composite c, int style, String string)
	{
		super(c, SWT.NO_BACKGROUND );
		this.setSize(110, 75);
		this.setLayout(new FillLayout());
		final Group g = new Group(this, style);
		g.setSize(110, 75);
		g.setText(string);
		this.b1 = new Button(g, SWT.RADIO);
		this.b1.setBounds(10,20,85, 15);
		this.b1.setText("Option Eins");
		this.b2 = new Button(g, SWT.RADIO);
		this.b2.setBounds(10,35,85, 15);
		this.b2.setText("Option Zwei");
		this.b3 = new Button(g, SWT.RADIO);
		this.b3.setBounds(10,50,85, 15);
		this.b3.setText("Option Drei");
	}

	public String getSelected(){
		if(this.b1.getSelection()) return "Option Eins";
		if(this.b2.getSelection()) return "Option Zwei";
		if(this.b3.getSelection()) return "Option Drei";
		return "nichts Selected";
	}
}
