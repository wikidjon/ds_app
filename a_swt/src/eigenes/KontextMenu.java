package eigenes;

import java.io.IOException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;






public class KontextMenu
{
	
	//Kontextmen� in Playlist
	@SuppressWarnings("static-access")
	public void konetxtMen�Playlist()
	{
			SelectionListener l = new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e){ 
				SelectedMenuItemPlaylist(e);//�berpr�fung welches Men�item gew�hlt wurde
			}};
			Menu contentMenu = new Menu(global.playlist.table.getShell());//Parentfenster f�r das Men�
			global.playlist.table.setMenu(contentMenu);
			
			// Eintr�ge erstellen
		   	MenuItem L�schen = new MenuItem(contentMenu, SWT.PUSH);
		   	L�schen.addSelectionListener(l);
		   	L�schen.setText("Eintrag aus Playlist l�schen");
		   	L�schen.setData("L�schen");
		  
		  
		   	MenuItem delete = new MenuItem(contentMenu, SWT.PUSH);
		   	delete.addSelectionListener(l);
		   	delete.setText("Position tauschen");
			delete.setData("Position");
		
			MenuItem Extern�ffnen = new MenuItem(contentMenu, SWT.PUSH);
			Extern�ffnen.addSelectionListener(l);
			Extern�ffnen.setText("Mit externem Player �ffnen");
			Extern�ffnen.setData("�ffnen Extern");
			
			MenuItem ExportPlaylist = new MenuItem(contentMenu, SWT.PUSH);
			ExportPlaylist.addSelectionListener(l);
			ExportPlaylist.setText("Playlist exportieren");
			ExportPlaylist.setData("PlaylistExport");
			
			MenuItem PlaylistAbspielen = new MenuItem(contentMenu, SWT.PUSH);
			PlaylistAbspielen.addSelectionListener(l);
			PlaylistAbspielen.setText("Playlist abspielen");
			PlaylistAbspielen.setData("PlayPlaylist");
			
				
			MenuItem neuePlaylist = new MenuItem(contentMenu, SWT.PUSH);
			neuePlaylist.addSelectionListener(l);
			neuePlaylist.setText("Neue Playlist");
			neuePlaylist.setData("NewPlaylist");
			
			MenuItem Infos = new MenuItem(contentMenu, SWT.PUSH);
			Infos.addSelectionListener(l);
			Infos.setText("Info Anzeigen");
			Infos.setData("Info Anzeigen");
	}
	
	private void SelectedMenuItemPlaylist(SelectionEvent e)
	{
		Object object = ((MenuItem) e.getSource()).getData();
		if (object == "L�schen"){
			global.playlist.remove();
		}else if (object == "Info Anzeigen") {
			int s=global.playlist.getSelected();
			Infofenster info=new Infofenster();
			if(!(s<0)){info.run(s,0);}//Position an Fenster �bergeben "0=Playlist"
		}else if (object == "NewPlaylist") {
			global.playlist.newPlaylist();
		}else if (object == "Position") {
			global.playlist.changeEntry();//Position eines Eintrages in der Playlist �ndern
			
		}else if (object == "PlaylistExport") {
			global.gem.exportPlaylistMediaplayer();//
		}
		else if (object == "PlayPlaylist") {
			global.gem.PlaylistAbspielen();
		}
		else if (object == "�ffnen Extern") {
			//Mediaplayer Starten und Datei abspielen
			if(!(global.playlist.getSelected()<0))//falls kein Eintrag ausgew�hlt wurde nichts machen sonnst -1 Zugriff auf Array
			{
			String ausgew�hlteDatei=global.dateilistePlaylist.get(global.playlist.getSelected());
			ProcessBuilder builder = new ProcessBuilder( "C:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe",ausgew�hlteDatei);
			@SuppressWarnings("unused")
			Process p = null;
			try {
				p = builder.start();
			} catch (IOException e1) {
				e1.printStackTrace();
			}}}
	}
	
	
	//Kontextmen� in Tabelle
	public void konetxtMen�Tabelle()
	{
			SelectionListener l = new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) 
			{
				SelectedMenuItemTabelle(e);//�berpr�fung welches Men�item gew�hlt wurde
			}};
			Menu contentMenu = new Menu(global.tabelle.table.getShell());//Parentfenster f�r das Men�
			global.tabelle.table.setMenu(contentMenu);
			
			// Eintr�ge erstellen
		   	MenuItem L�schen = new MenuItem(contentMenu, SWT.PUSH);
		   	L�schen.addSelectionListener(l);
		   	L�schen.setText("Eintrag zu Playliste hinzuf�gen");
		   	L�schen.setData("AddToPlaylist");
		  
		   	MenuItem delete = new MenuItem(contentMenu, SWT.PUSH);
		   	delete.addSelectionListener(l);
		   	delete.setText("Alles Exportieren");
			delete.setData("Export");/**/
		
			MenuItem Extern�ffnen = new MenuItem(contentMenu, SWT.PUSH);
			Extern�ffnen.addSelectionListener(l);
			Extern�ffnen.setText("Mit externem Player �ffnen");
			Extern�ffnen.setData("�ffnen Extern");
			
			MenuItem Infos = new MenuItem(contentMenu, SWT.PUSH);
			Infos.addSelectionListener(l);
			Infos.setText("Info Anzeigen");
			Infos.setData("Info Anzeigen");
	}
	
	
	private void SelectedMenuItemTabelle(SelectionEvent e)
	{
		Object object = ((MenuItem) e.getSource()).getData();
		if (object == "AddToPlaylist"){
			global.tabelle.addToPlaylist();
		}else if (object == "Info Anzeigen") {
			Infofenster info=new Infofenster();
			int s=global.tabelle.getSelected();
			if(!(s<0)){info.run(s,1);}//Position an Fenster �bergeben "1=Tabelle"
		}else if (object == "Export") {
		global.gem.exportPlaylistMediaplayerMitte();	
		}
		else if (object == "�ffnen Extern") {
			
			//Mediaplayer Starten und Datei abspielen
			if(!(global.tabelle.getSelected()<0))//falls kein Eintrag ausgew�hlt wurde nichts machen sonnst -1 Zugriff auf Array
			{
			String ausgew�hlteDatei=AlleDaten.dateiliste.get(global.tabelle.getSelected());
			ProcessBuilder builder = new ProcessBuilder( "C:\\Program Files\\Windows Media Player\\wmplayer.exe",ausgew�hlteDatei);
			@SuppressWarnings("unused")
			Process p = null;
			try {
				p = builder.start();
			} catch (IOException e1) {
				e1.printStackTrace();
			}}}
	}
}