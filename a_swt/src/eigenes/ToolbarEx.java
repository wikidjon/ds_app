package eigenes;




import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;


public class ToolbarEx
{
	//Globale variablen
	ToolBar toolbar;
	Shell shell;

	@SuppressWarnings("unused")
	public void getToolbar(final Shell shell,Display display)
	{
		this.shell=shell;
		Listener toolbarListener = new Listener() {
			public void handleEvent(Event event) {
				ToolBarSelection(event);
			}};

			toolbar = new ToolBar(shell, SWT.FLAT);

			//Farbe der Toolbar �ndern
			Device device = Display.getCurrent (); 
			Color red = new Color (device, 240, 240, 240); 
			toolbar.setBackground(red);
			

			ToolItem newPlaylist = new ToolItem(toolbar, SWT.PUSH);
			ToolItem �ffnenPlaylist = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SpeichernPlaylist = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SEPARATOR1 = new ToolItem(toolbar, SWT.SEPARATOR);
			ToolItem ExportMPList = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SEPARATOR2 = new ToolItem(toolbar, SWT.SEPARATOR);
			ToolItem �ffnenSuche = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SpeichernSuche = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SEPARATOR3 = new ToolItem(toolbar, SWT.SEPARATOR);
			ToolItem SucheStartenKomplett = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SucheStartenVerzeichnis = new ToolItem(toolbar, SWT.PUSH);
			ToolItem SEPARATOR4 = new ToolItem(toolbar, SWT.SEPARATOR);
			ToolItem PlayerAbspielen = new ToolItem(toolbar, SWT.PUSH);
			
			//Icons einlesen
			Image Icon�ffnen = null,IconSpeichern=null,IconNewPlaylist=null,
			IconSearchAll=null,IconSearchFolder=null,IconPlay=null,IconExport=null;
			String vereichnis=System.getProperty("user.dir");//Verzeichnis aus dem das Programm gestartet wurde
			try {
				Display d=toolbar.getDisplay();//Icons auf Toolbar zeichen
				Icon�ffnen = new Image(d, vereichnis+"/openfolder.png");//GR�?E
				IconNewPlaylist = new Image(d, vereichnis+"/NewFile.png");
				IconSearchAll = new Image(d, vereichnis+"/lupe.png");
				IconSearchFolder = new Image(d, vereichnis+"/searchfolder.png");
				IconSpeichern= new Image(d, vereichnis+"/savedisk.png");
				IconPlay= new Image(d, vereichnis+"/play.png");
				IconExport=new Image(d, vereichnis+"/wmp.png");
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			

			//Open Search
			
			�ffnenSuche.setText("Suche �ffnen");
			�ffnenSuche.setToolTipText("Gespeicherte Suchergebnisse �ffnen");
			�ffnenSuche.addListener(SWT.Selection, toolbarListener);
			�ffnenSuche.setData("Open Search");
			�ffnenSuche.setImage(Icon�ffnen);
			
			SpeichernSuche.setText("Suche Speichern");
			SpeichernSuche.setToolTipText("Suchergebnisse speichern");
			SpeichernSuche.addListener(SWT.Selection, toolbarListener);
			SpeichernSuche.setData("Save Search");
			SpeichernSuche.setImage(IconSpeichern);
			
			newPlaylist.setText("Neue Playlist erstellen");
			newPlaylist.setToolTipText("Neue Playlist erstellen");
			newPlaylist.setEnabled(true);
			newPlaylist.addListener(SWT.Selection, toolbarListener);
			newPlaylist.setData("newPlayList");
			newPlaylist.setImage(IconNewPlaylist);
			
			
			�ffnenPlaylist.setText("Playlist �ffnen");
			�ffnenPlaylist.setToolTipText("Gespeicherte Playlist �ffnen");
			�ffnenPlaylist.addListener(SWT.Selection, toolbarListener);
			�ffnenPlaylist.setData("Open Playlist");
			�ffnenPlaylist.setImage(Icon�ffnen);
			
		
			SpeichernPlaylist.setText("Playlist speichern");
			SpeichernPlaylist.setToolTipText("Playlist auf Festplatte speichern");
			SpeichernPlaylist.addListener(SWT.Selection, toolbarListener);
			SpeichernPlaylist.setData("Save Playlist");
			SpeichernPlaylist.setImage(IconSpeichern);
			
			
			
			SucheStartenKomplett.setText("Alles Durchsuchen");
			SucheStartenKomplett.setToolTipText("Kompletten Computer nach mp3 Dateien durchsuchen");
			SucheStartenKomplett.addListener(SWT.Selection, toolbarListener);
			SucheStartenKomplett.setData("Alles Durchsuchen");
			SucheStartenKomplett.setImage(IconSearchAll);
			
		
			SucheStartenVerzeichnis.setText("Verzeichnis durchsuchen");
			SucheStartenVerzeichnis.setToolTipText("Verzeichnis nach mp3 Dateien durchsuchen");
			SucheStartenVerzeichnis.addListener(SWT.Selection, toolbarListener);
			SucheStartenVerzeichnis.setData("Verzeichnis durchsuchen");
			SucheStartenVerzeichnis.setImage(IconSearchFolder);
			
			
			PlayerAbspielen.setText("Play");
			PlayerAbspielen.setToolTipText("Playlist an Windows Media Player �bergeben und abspielen");
			PlayerAbspielen.addListener(SWT.Selection, toolbarListener);
			PlayerAbspielen.setData("Play");
			PlayerAbspielen.setImage(IconPlay);
			
			
			ExportMPList.setText("Exportieren");
			ExportMPList.setToolTipText("Windows Media Player Playlist Exportieren");
			ExportMPList.addListener(SWT.Selection, toolbarListener);
			ExportMPList.setData("Exportieren");
			ExportMPList.setImage(IconExport);
			
		
			//Berechnen der Groesse fuer toolbar
			shell.addControlListener(
					new ControlAdapter() {
			public void controlResized(ControlEvent e) {
			Rectangle r = shell.getClientArea();
			toolbar.setBounds(r.x, r.y, r.width, toolbar.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
				}});
	}


	
public void ToolBarSelection(Event e)
{
		ToolItem item = (ToolItem)e.widget;
		String tmp=item.getData().toString();
		
		if (tmp == "newPlayList"){ 
			global.playlist.newPlaylist();//Funktion aufrufen
		} 
		else if (tmp == "Verzeichnis durchsuchen") {
			global.gem.systemDurchsuchenVerzeichnis();//Funktion aufrufen
		} 
		else if (tmp == "Alles Durchsuchen") {
			global.gem.systemDurchsuchenAlles();//Funktion aufrufen
		}
		else if (tmp == "Open Playlist") {
			global.playlist.openPlaylist();	
		}
		else if (tmp == "Save Playlist") {
			global.playlist.savePlaylist();//Funktion aufrufen
		}
		else if (tmp == "Open Search") {
			global.gem.openGespeicherteSuche();//Funktion aufrufen
		}
		else if (tmp == "Save Search") {
			global.gem.saveGespeicherteSuche();//Funktion aufrufen	
		}
		else if (tmp == "Play") {
			global.gem.PlaylistAbspielen();	
		}
		else if (tmp == "Stop") {
		}
		else if (tmp == "Pause") {	
		}
		else if (tmp == "Exportieren") {
			global.gem.exportPlaylistMediaplayer();//Funktion ausgelagert
		}

	}
	

	
public void setSize(int posx,int posy,int sizex,int sizey)//�ndern des Baums (Position und Gr��e)
{
		 toolbar.setBounds(posx, posy, sizex, sizey);
}
	
}