package eigenes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;



public class Infofenster
{
int x=0;
//Typ 0=Playlistfenster
//1 Tabelle
//2 Baumansicht
final Shell dialog = new Shell(global.getShell(), SWT.TITLE| SWT.PRIMARY_MODAL);
public void run(int auswahl,int Typ)
{
		dialog.setLocation(100, 100);
		dialog.setText("Info");
		dialog.setSize(600, 300);
		dialog.open();
		
		Label label1 = new Label(dialog, SWT.LEFT);
	    label1.setText("Dateipfad:");
	    label1.setBounds(0,0,100,0);
	    label1.pack();
	    
	    Label labe2 = new Label(dialog, SWT.LEFT);
	    labe2.setText("Dateiname:");
	    labe2.setBounds(0,30,100,0);
	    labe2.pack();
	    
	    Label labe3 = new Label(dialog, SWT.LEFT);
	    labe3.setText("Titel:");
	    labe3.setBounds(0,60,50,80);
	    labe3.pack();
	    
	    Label labe4 = new Label(dialog, SWT.LEFT);
	    labe4.setText("Erscheinungsjahr:");
	    labe4.setBounds(0,90,50,80);
	    labe4.pack();
	    
	    Label labe5 = new Label(dialog, SWT.LEFT);
	    labe5.setText("Interpret:");
	    labe5.setBounds(0,120,50,80);
	    labe5.pack();
	    
	    Label labe6 = new Label(dialog, SWT.LEFT);
	    labe6.setText("Album:");
	    labe6.setBounds(0,150,50,80);
	    labe6.pack();
	    
	    Label labe7 = new Label(dialog, SWT.LEFT);
	    labe7.setText("Bitrate:");
	    labe7.setBounds(0,180,50,80);
	    labe7.pack();
	    
	    Label labe8 = new Label(dialog, SWT.LEFT);
	    labe8.setText("Frequenz:");
	    labe8.setBounds(0,210,50,80);
	    labe8.pack();

	    //Aufrufen je nachdem in welchem Bereich die Datei liegt
	   if(Typ==0){
		   Playlsit(auswahl);
	   }else if(Typ==1){
		   Tabelle(auswahl);
	   }else if(Typ==2){
		   Baumansicht(auswahl);
	   }
	    
		Button ok=new Button(dialog, 0);
		ok.setBounds(240,222,70,45);
		ok.setText("Schlie�en");
		ok.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				dialog.close();//falls der Wert korrekt ist Fenster schlie�en
			}});
		

		while (!dialog.isDisposed ()) {
			if (!dialog.getDisplay().readAndDispatch ()) dialog.getDisplay().sleep ();
		}
			}
	
//in den Tabellen gew�hlter Eintrag
public void Tabelle(int auswahl)
{
	if(!(auswahl<1)){//falls kein oder falscher Eintrag gew�hlt wurde "-1"
	 //Informationen abrufen
    Label ausgabe1 = new Label(dialog, SWT.LEFT);
    ausgabe1.setText(global.dateilisteMitte.get(auswahl));
    ausgabe1.setBounds(120,0,100,0);
    ausgabe1.pack();
    
    Label ausgabe2 = new Label(dialog, SWT.LEFT);
    ausgabe2.setText(global.dateilisteMitteshort.get(auswahl));
    ausgabe2.setBounds(120,30,100,0);
    ausgabe2.pack();
    
    Label ausgabe3 = new Label(dialog, SWT.LEFT);
    ausgabe3.setText(global.dateilisteMitteTitel.get(auswahl));
    ausgabe3.setBounds(120,60,50,80);
    ausgabe3.pack();
    
    Label ausgabe4 = new Label(dialog, SWT.LEFT);
    ausgabe4.setText(global.dateilisteMitteJahr.get(auswahl));
    ausgabe4.setBounds(120,90,50,80);
    ausgabe4.pack();
    
    Label ausgabe5 = new Label(dialog, SWT.LEFT);
    ausgabe5.setText(global.dateilisteMitteInterpret.get(auswahl));
    ausgabe5.setBounds(120,120,50,80);
    ausgabe5.pack();
    
    Label ausgabe6 = new Label(dialog, SWT.LEFT);
    ausgabe6.setText(global.dateilisteMitteAlbum.get(auswahl));
    ausgabe6.setBounds(120,150,50,80);
    ausgabe6.pack();
    
    Label ausgabe7 = new Label(dialog, SWT.LEFT);
    ausgabe7.setText(global.dateilisteMitteBitrate.get(auswahl));
    ausgabe7.setBounds(120,180,50,80);
    ausgabe7.pack();
    
    Label ausgabe8 = new Label(dialog, SWT.LEFT);
    ausgabe8.setText(global.dateilisteMitteFrequenz.get(auswahl));
    ausgabe8.setBounds(120,210,50,80);
    ausgabe8.pack();
	}
}

public void Playlsit(int auswahl)
{
	if(!(auswahl<1)){//falls kein oder falscher Eintrag gew�hlt wurde "-1"
		 //Informationen abrufen
	    Label ausgabe1 = new Label(dialog, SWT.LEFT);
	    ausgabe1.setText(global.dateilistePlaylist.get(auswahl));
	    ausgabe1.setBounds(120,0,100,0);
	    ausgabe1.pack();
	    
	    Label ausgabe2 = new Label(dialog, SWT.LEFT);
	    ausgabe2.setText(global.dateilistePlaylist.get(auswahl));
	    ausgabe2.setBounds(120,30,100,0);
	    ausgabe2.pack();
	    
	    Label ausgabe3 = new Label(dialog, SWT.LEFT);
	    ausgabe3.setText(global.dateilistePlaylistTitel.get(auswahl));
	    ausgabe3.setBounds(120,60,50,80);
	    ausgabe3.pack();
	    
	    Label ausgabe4 = new Label(dialog, SWT.LEFT);
	    ausgabe4.setText(global.dateilistePlaylistJahr.get(auswahl));
	    ausgabe4.setBounds(120,90,50,80);
	    ausgabe4.pack();
	    
	    Label ausgabe5 = new Label(dialog, SWT.LEFT);
	    ausgabe5.setText(global.dateilistePlaylistInterpret.get(auswahl));
	    ausgabe5.setBounds(120,120,50,80);
	    ausgabe5.pack();
	    
	    Label ausgabe6 = new Label(dialog, SWT.LEFT);
	    ausgabe6.setText(global.dateilistePlaylistAlbum.get(auswahl));
	    ausgabe6.setBounds(120,150,50,80);
	    ausgabe6.pack();
	    
	    Label ausgabe7 = new Label(dialog, SWT.LEFT);
	    ausgabe7.setText(global.dateilistePlaylistBitrate.get(auswahl));
	    ausgabe7.setBounds(120,180,50,80);
	    ausgabe7.pack();
	    
	    Label ausgabe8 = new Label(dialog, SWT.LEFT);
	    ausgabe8.setText(global.dateilistePlaylistFrequenz.get(auswahl));
	    ausgabe8.setBounds(120,210,50,80);
	    ausgabe8.pack();	
	}
}

public void Baumansicht(int auswahl)
{
	if(!(auswahl<1)){//falls kein oder falscher Eintrag gew�hlt wurde "-1"	
		 //Informationen abrufen
	    Label ausgabe1 = new Label(dialog, SWT.LEFT);
	    ausgabe1.setText(AlleDaten.dateiliste.get(auswahl));
	    ausgabe1.setBounds(120,0,100,0);
	    ausgabe1.pack();
	    
	    Label ausgabe2 = new Label(dialog, SWT.LEFT);
	    ausgabe2.setText(AlleDaten.dateilisteshort.get(auswahl));
	    ausgabe2.setBounds(120,30,100,0);
	    ausgabe2.pack();
	    
	    Label ausgabe3 = new Label(dialog, SWT.LEFT);
	    ausgabe3.setText(AlleDaten.dateilisteTitel.get(auswahl));
	    ausgabe3.setBounds(120,60,50,80);
	    ausgabe3.pack();
	    
	    Label ausgabe4 = new Label(dialog, SWT.LEFT);
	    ausgabe4.setText(AlleDaten.dateilisteJahr.get(auswahl));
	    ausgabe4.setBounds(120,90,50,80);
	    ausgabe4.pack();
	    
	    Label ausgabe5 = new Label(dialog, SWT.LEFT);
	    ausgabe5.setText(AlleDaten.dateilisteInterpret.get(auswahl));
	    ausgabe5.setBounds(120,120,50,80);
	    ausgabe5.pack();
	    
	    Label ausgabe6 = new Label(dialog, SWT.LEFT);
	    ausgabe6.setText(AlleDaten.dateilisteAlbum.get(auswahl));
	    ausgabe6.setBounds(120,150,50,80);
	    ausgabe6.pack();
	    
	    Label ausgabe7 = new Label(dialog, SWT.LEFT);
	    ausgabe7.setText(AlleDaten.dateilisteBitrate.get(auswahl));
	    ausgabe7.setBounds(120,180,50,80);
	    ausgabe7.pack();
	    
	    Label ausgabe8 = new Label(dialog, SWT.LEFT);
	    ausgabe8.setText(AlleDaten.dateilisteFrequenz.get(auswahl));
	    ausgabe8.setBounds(120,210,50,80);
	    ausgabe8.pack();
	}
}
}