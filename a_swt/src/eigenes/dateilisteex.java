package eigenes;

import java.io.File;
import org.eclipse.swt.widgets.Display;



class FileList extends Thread
{
	String[] directory;
	String singleDirectory;
	Display display;//notwendig f�r Zugriff auf GUI
	public void sucheStartenAlles(String[] directory)
	{
		for(int i=0;i<directory.length;i++)
		{
		File dir = new File(directory[i]);
		listDir(dir);
		}

		ThreadBeendet();
	}
	
	public void sucheStartenVerzeichnis(String directory)
	{
		File dir = new File(directory);
		listDir(dir);
		ThreadBeendet();
	}
	
	
	//Konstruktor
	FileList(String[] directory,String singleDirectory,Display display) 
	{ 
		//Variablen initialisieren
		this.display=display;
		//falls einer der Werte null ist in der Klasse auch auf null setzen f�r sp�tere Abfragen
		//damit unterscheiden ob ein String Array oder ein einzelner String �bergeben wurde
		if(!(directory==null))
		{
		this.directory= directory;
		this.singleDirectory=null;
		}
		
		if(!(singleDirectory==null))
		{
		this.directory= null;
		this.singleDirectory=singleDirectory;
		}
		
		ThreadBeginnt();//abrfrage ob Thread schon fertig ist
    } 
	
	
	//Festellen ob Thread noch l�uft
	private void ThreadBeginnt()
	{
		global.SucheImGange=true;//abrfrage ob Thread schon fertig ist	
	}
	
	private void ThreadBeendet()
	{
		global.SucheImGange=false;//abrfrage ob Thread schon fertig ist	
		//Ausgeben im Fenster
		System.out.print("SUCHE BEENDET");
		//auf GUI aus einem anderen Thread zugreifen
		display.asyncExec(new Runnable() {
		   // @Override
		    public void run() {
		        // Executed on UI Thread
		    	//global.tabelle.ListeF�llen();	
		    	global.baum.ListeF�llen();
		    }
		});
	}
	
	

 public void run()
 {
	 global.clearMain();//Alles l�schen bei neuer suche
	 global.clearMitte();
	 global.SucheImGange=true;

	 //mit welchem Parameter wurde der Thread aufgerufen
	 	if((directory==null)){
		sucheStartenVerzeichnis(singleDirectory);
		}
		
		if((singleDirectory==null)){
		sucheStartenAlles(directory);
		
		}
 }
 
 //dateien auf datentr�ger suchen im angegebenen Verzeichnis
 public static void listDir(File dir) {

	 	Mp3info mp3=new Mp3info();
		File[] files = dir.listFiles();	
		
		if (files != null) 
		{
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()){ 
					// ruft sich selbst mit dem 
					// Unterverzeichnis als Parameter auf
					listDir(files[i]);		
				}
				else 
				{
					if(files[i].canWrite())//Datei Lesbar?
					{
					if (files[i].getName().endsWith(".mp3"))
					{	
					//Von Windows 7 gesicherte mp3( Fehler bei Zugriff)
					String t=files[i].getName();	
					String x1="Kalimba.mp3";
					String x2="Maid with the Flaxen Hair.mp3";
					String x3="Sleep Away.mp3";
					
					if(!x3.equals(t))
					if(!x2.equals(t))
					if(!x1.equals(t))
						//////////////////////////////////////////////////////
					{
					
					String datei=files[i].getAbsolutePath();
					//Informationen �ber Datei abrufen und abspreichern
					global.addDateiliste(files[i].getAbsolutePath());//komplette Verzeichnis
					global.addDateilisteShort(files[i].getName());//Nur Dateiname
					global.addDateilisteTitel(mp3.getTitle(datei));
					global.addDateilisteAlbum(mp3.getAlbum(datei));
					global.addDateilisteInterpret(mp3.getArtist(datei));
					global.addDateilisteJahr(mp3.getYear(datei));
					String tmp=Integer.toString(mp3.getBitrate(datei));
					global.addDateilisteBitrate(tmp);
					tmp=Double.toString(mp3.getFrequency(datei));
					global.addDateilisteFrequenz(tmp);
					}
					}
				}
			}
			}
		}
}
}