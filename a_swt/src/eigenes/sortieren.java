package eigenes;





class sortieren 
{
	//speichern ob die Sortierung absteigend oder aufsteigend erfolgt ist
	Boolean spalte1absteigend=false;
	Boolean spalte2absteigend=true;
	Boolean spalte3absteigend=true;
	Boolean spalte4absteigend=true;
	Boolean spalte5absteigend=true;
	Boolean spalte6absteigend=true;
	Boolean spalte7absteigend=true;
	//i=Spalte
	//Die Tabellenansicht sortieren
public void sortierenTabelle(int i)
{
	SortierReihenfolge();//�berpr�fen wie sortiert werden muss

	//!!spalte 2,5,6 Nummern
	//�berpr�fen welche Spalte gew�hlt wurde und entscheiden ob lexikalisch oder numerisch sortiert wird
	if(i==2||i==5||i==6){
		sortNumbersTabelle(i);
	}
	else{
		sortLettersTabelle(i);
	}
	
}


public void sortierenPlaylist(int i)
{
	SortierReihenfolge();//�berpr�fen wie sortiert werden muss

	//!!spalte 2,5,6 Nummern
	//�berpr�fen welche Spalte gew�hlt wurde und entscheiden ob lexikalisch oder numerisch sortiert wird
	if(i==2||i==5||i==6){
		sortNumbersPlaylist(i);
	}
	else{
		sortLettersPlaylist(i);
	}
	
}

//Spalte lexikalisch sortieren
private void sortLettersTabelle(int i)
{
	int gr��e=global.dateilisteMitteshort.size();//Listengr��e zwischenspeichern

	for(int j=0;j<gr��e;j++)												//einzelnen Eintr�ge durchgehen
		{
			 for(int k=0;k<gr��e;k++) 											//den Namen aus dem Array j mit allen anderen nachfolgenden vergleichen
			 {	
				 if(!(j>gr��e)||!(k>gr��e))//nicht �ber die malimale Gr��e des Arrays zugreifen
				 {
				 //Inhalt der Spalten(einzeln) f�r Vergleich
				 String wert1=sortierSpalteW�rterTabelle(i, j);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
				 String wert2=sortierSpalteW�rterTabelle(i, k);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
			
				 if(spalte1absteigend)
				 {	//absteigend sortieren
					 if(wert1.compareToIgnoreCase(wert2)<0)									//zewi Zeichenketten lexikografisch Vergleichen
					 {
						 //Alle Arrays sortieren
						 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
						 global.pos�ndernMitte(j, k);								//Positionen �bergeben (Elemente verschieben)
					 }
				 }
				 else
				 {	//aufsteigend sortieren
					 if(wert1.compareTo(wert2)>0)									//zewi Zeichenketten lexikografisch Vergleichen
					 {
						 //Alle Arrays sortieren
						 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
						 global.pos�ndernMitte(j, k);								//Positionen �bergeben (Elemente verschieben)
					 }
				 }
			 }
			 }
	}	
}


//Spalte numerisch sortieren
private void sortNumbersTabelle(int i)
{
	
	int gr��e=global.dateilisteMitteshort.size();//Listengr��e zwischenspeichern

	for(int j=0;j<gr��e;j++)												//einzelnen Eintr�ge durchgehen
			{
				 for(int k=0;k<gr��e;k++) 											//den Namen aus dem Array j mit allen anderen nachfolgenden vergleichen
				 {
					 if(!(j>gr��e)||!(k>gr��e))//nicht �ber die malimale Gr��e des Arrays zugreifen
					 {
					 //Inhalt der Spalten(einzeln) f�r Vergleich
					 int wert1=sortierSpalteZahlenTabelle(i, j);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
					 int wert2=sortierSpalteZahlenTabelle(i, k);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
				
					 if(spalte1absteigend)
					 {	//absteigend sortieren
						 if(wert1<wert2)									//zewi Zeichenketten lexikografisch Vergleichen
						 {
							 //Alle Arrays sortieren
							 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
							 global.pos�ndernMitte(j, k);								//Positionen �bergeben (Elemente verschieben)
						 }
					 }
					 else
					 {	//aufsteigend sortieren
						 if(wert1>wert2)									//zewi Zeichenketten lexikografisch Vergleichen
						 {
							 //Alle Arrays sortieren
							 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
							 global.pos�ndernMitte(j, k);								//Positionen �bergeben (Elemente verschieben)
						 }
					 }
				 }
				 }
		}		
}






//hier wird nur ein Teil �berpr�fe, da die Funktion nur aufgerufen wirs wenn eine Spalte mit W�rtern sortiert wird
//i=Spalte die beim Klick in der Tabelle gew�hlt wurde
//index=Position in der Liste (gibt den Inhalt dementsprechend zur�ck)
private String sortierSpalteW�rterTabelle(int i,int index)
{
	//Spalte 1
	if(i==0)
	{
		return global.dateilisteMitteshort.get(index).toLowerCase();
	}else if(i==1){
		return global.dateilisteMitteTitel.get(index).toLowerCase();
	}else if(i==3){
		return global.dateilisteMitteInterpret.get(index).toLowerCase();	
	}else if(i==4){
		return global.dateilisteMitteAlbum.get(index).toLowerCase();	
	}
	
	
	return "";
}

//hier wird nur ein Teil �berpr�fe, da die Funktion nur aufgerufen wirs wenn eine Spalte mit Zahlen sortiert wird
//i=Spalte die beim Klick in der Tabelle gew�hlt wurde ()
//index=Position in der Liste (gibt den Inhalt dementsprechend zur�ck)
private int sortierSpalteZahlenTabelle(int i,int index)
{
	if(i==5){
		int x=0;
		try{//�berpr�fen ob nur Zahlen im String sind
		x = Integer.valueOf(global.dateilisteMitteBitrate.get(index));
		}
		catch ( NumberFormatException nfe )
		{
		//Zahl auf definierten Wert setzen
		//System.out.println("Keine g�ltige Zahl!:"+x);
		//tmpx=0;
		x=0;
		}
		
		return x;
	}else if(i==6){
		int x=0;
		try{//�berpr�fen ob nur Zahlen im String sind
		x = Double.valueOf(global.dateilisteMitteFrequenz.get(index)).intValue();
		}
		catch ( NumberFormatException nfe )
		{
		//Zahl auf definierten Wert setzen
		System.out.println("Keine g�ltige Zahl!:"+x);
		//tmpx=0;
		x=0;
		}
		
		return x;
	}else if(i==2){
		int x=0;
		try{//�berpr�fen ob nur Zahlen im String sind
		x = Integer.valueOf(global.dateilisteMitteJahr.get(index));
		}
		catch ( NumberFormatException nfe )
		{
		//Zahl auf definierten Wert setzen
		System.out.println("Keine g�ltige Zahl!:"+x);
		//tmpx=0;
		x=0;
		}
		
		return x;	
	}
	return 0;
}





//hin und her wechseln zwichen jedem Klick auf Sortieren in der Tabelle
private void SortierReihenfolge()
{
	//Spalte1
	if(spalte1absteigend){
		 spalte1absteigend=false;
	}else{
		 spalte1absteigend=true;
	}
	
	//Spalte2
	if(spalte2absteigend){
		 spalte2absteigend=false;
	}else{
		 spalte2absteigend=true;
	}
	
	//Spalte1
	if(spalte3absteigend){
		 spalte3absteigend=false;
	}else{
		 spalte3absteigend=true;
	}
	
	//Spalte1
	if(spalte4absteigend){
		 spalte4absteigend=false;
	}else{
		 spalte4absteigend=true;
	}
	
	
	//Spalte1
	if(spalte5absteigend){
		 spalte5absteigend=false;
	}else{
		 spalte5absteigend=true;
	}
	
	//Spalte1
	if(spalte6absteigend){
		 spalte6absteigend=false;
	}else{
		 spalte6absteigend=true;
	}
	
	
	//Spalte1
	if(spalte7absteigend){
		 spalte7absteigend=false;
	}else{
		 spalte7absteigend=true;
	}
	

}



//Spalte lexikalisch sortieren f�r die Playlist
private void sortLettersPlaylist(int i)
{
	int gr��e=global.dateilistePlaylistshort.size();//Listengr��e zwischenspeichern

	for(int j=0;j<gr��e;j++)												//einzelnen Eintr�ge durchgehen
		{
			 for(int k=0;k<gr��e;k++) 											//den Namen aus dem Array j mit allen anderen nachfolgenden vergleichen
			 {
				 if(!(j>gr��e)||!(k>gr��e))//nicht �ber die malimale Gr��e des Arrays zugreifen
				 {
				 //Inhalt der Spalten(einzeln) f�r Vergleich
				 String wert1=sortierSpalteW�rterPlaylist(i, j);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
				 String wert2=sortierSpalteW�rterPlaylist(i, k);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
			
				 if(spalte1absteigend)
				 {	//absteigend sortieren
					 if(wert1.compareToIgnoreCase(wert2)<0)									//zewi Zeichenketten lexikografisch Vergleichen
					 {
						 //Alle Arrays sortieren
						 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
						 global.pos�ndernPlaylist(j, k);								//Positionen �bergeben (Elemente verschieben)
					 }
				 }
				 else
				 {	//aufsteigend sortieren
					 if(wert1.compareTo(wert2)>0)									//zewi Zeichenketten lexikografisch Vergleichen
					 {
						 //Alle Arrays sortieren
						 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
						 global.pos�ndernPlaylist(j, k);								//Positionen �bergeben (Elemente verschieben)
					 }
				 }
			 }
			 }
	}	
}


//Spalte numerisch sortieren f�r die Playlist
private void sortNumbersPlaylist(int i)
{
	
	int gr��e=global.dateilistePlaylistshort.size();//Listengr��e zwischenspeichern

	for(int j=0;j<gr��e;j++)												//einzelnen Eintr�ge durchgehen
			{
				 for(int k=0;k<gr��e;k++) 											//den Namen aus dem Array j mit allen anderen nachfolgenden vergleichen
				 {
					 if(!(j>gr��e)||!(k>gr��e))//nicht �ber die malimale Gr��e des Arrays zugreifen
					 {
					 //Inhalt der Spalten(einzeln) f�r Vergleich
					 int wert1=sortierSpalteZahlenPlaylist(i, j);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
					 int wert2=sortierSpalteZahlenPlaylist(i, k);//�berpr�fen welche Spalte gew�hlt wurde und dementsprechend sortieren
				
					 if(spalte1absteigend)
					 {	//absteigend sortieren
						 if(wert1<wert2)									//zewi Zeichenketten lexikografisch Vergleichen
						 {
							 //Alle Arrays sortieren
							 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
							 global.pos�ndernPlaylist(j, k);								//Positionen �bergeben (Elemente verschieben)
						 }
					 }
					 else
					 {	//aufsteigend sortieren
						 if(wert1>wert2)									//zewi Zeichenketten lexikografisch Vergleichen
						 {
							 //Alle Arrays sortieren
							 //Zwischenspeicherung und Tausch der Eintr�ge erfolgt dort
							 global.pos�ndernPlaylist(j, k);								//Positionen �bergeben (Elemente verschieben)
						 }
					 }
				 }
			}
		}		
}






//hier wird nur ein Teil �berpr�fe, da die Funktion nur aufgerufen wirs wenn eine Spalte mit W�rtern sortiert wird
//i=Spalte die beim Klick in der Tabelle gew�hlt wurde
//index=Position in der Liste (gibt den Inhalt dementsprechend zur�ck)
private String sortierSpalteW�rterPlaylist(int i,int index)
{
	//Spalte 1
	if(i==0)
	{
		return global.dateilistePlaylistshort.get(index).toLowerCase();
	}else if(i==1){
		return global.dateilistePlaylistTitel.get(index).toLowerCase();
	}else if(i==3){
		return global.dateilistePlaylistInterpret.get(index).toLowerCase();	
	}else if(i==4){
		return global.dateilistePlaylistAlbum.get(index).toLowerCase();	
	}
	
	
	return "";
}

//hier wird nur ein Teil �berpr�fe, da die Funktion nur aufgerufen wirs wenn eine Spalte mit Zahlen sortiert wird
//i=Spalte die beim Klick in der Tabelle gew�hlt wurde ()
//index=Position in der Liste (gibt den Inhalt dementsprechend zur�ck)
private int sortierSpalteZahlenPlaylist(int i,int index)
{
	if(i==5){
		int x=0;
		try{//�berpr�fen ob nur Zahlen im String sind
		x = Integer.valueOf(global.dateilistePlaylistBitrate.get(index));
		}
		catch ( NumberFormatException nfe )
		{
		//Zahl auf definierten Wert setzen
		System.out.println("Keine g�ltige Zahl!:"+x);
		//tmpx=0;
		x=0;
		}
		
		return x;
	}else if(i==6){
		int x=0;
		try{//�berpr�fen ob nur Zahlen im String sind
		x = Double.valueOf(global.dateilistePlaylistFrequenz.get(index)).intValue();
		}
		catch ( NumberFormatException nfe ){
		//Zahl auf definierten Wert setzen
		System.out.println("Keine g�ltige Zahl!:"+x);
		//tmpx=0;
		x=0;
		}
		
		return x;
	}else if(i==2){
		int x=0;
		try{//�berpr�fen ob nur Zahlen im String sind
		x = Integer.valueOf(global.dateilistePlaylistJahr.get(index));
		}catch ( NumberFormatException nfe )
		{
		//Zahl auf definierten Wert setzen
		System.out.println("Keine g�ltige Zahl!:"+x);
		//tmpx=0;
		x=0;
		}
		
		return x;	
	}
	return 0;
}


}