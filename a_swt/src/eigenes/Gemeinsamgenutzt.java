package eigenes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;

//alles was von mehreren Positionen genutzt wird
public class Gemeinsamgenutzt
{
	 static FileList filelist;
	 
	 //Verzeichnis w�hlen und durchsuchen
	 public void systemDurchsuchenVerzeichnis()
	 {
		 	String gew�hltesVerzeichnis = "c:/";//standartverzeichnis
		 
		 	DirectoryDialog dirDialog = new DirectoryDialog(global.getShell(), SWT.DIALOG_TRIM);
		 	dirDialog.setText("Verzeichnis");
		 	dirDialog.setFilterPath("C:/");
		 	gew�hltesVerzeichnis = "c:/";//standartverzeichnis
		 	gew�hltesVerzeichnis = dirDialog.open();
		
		 	SuchFenster fenster=new SuchFenster();
		 	if(!(gew�hltesVerzeichnis==null)){
		 	filelist = new FileList(null,gew�hltesVerzeichnis,global.getDisplay());
		 	filelist.start();	 //Thread starten
		 	fenster.run();//Suchfenster starten
		 	}
	 }
	 
	 
	 public void systemDurchsuchenAlles()
	 {
		File[] drives=File.listRoots();//Laufwerke holen
		String[] laufwerke = new String[drives.length];//alle Laufwerke hier gespeichert
		for(int i=0;i<drives.length;i++){
		laufwerke[i]= new String(drives[i].toString());
		}
		
		filelist = new FileList(laufwerke,null,global.getDisplay());
		filelist.start();	
		
		SuchFenster fenster=new SuchFenster();
		fenster.run();//Suchfenster starten
		
	 }
	
	 //Schreibvorgang f�r Playlist
	private void datenSchreibenPlaylist(String datei)
	{
		//batchdatei f�r den Player (Liste)
			try
			{ 

			String fz="\u003F";//Fragezeichen
			String pu="\u002e";//Punkt
     
			@SuppressWarnings("unused")
			File file = new File(datei);
			BufferedWriter fw = new BufferedWriter(new FileWriter(datei));
			
			fw.write("<"+fz+"wpl version=\"1"+pu+"0\""+fz+">");	fw.newLine();
			fw.write("<smil>");									fw.newLine();
			fw.write("<head>");									fw.newLine();
			fw.write("<title>"+datei+"</title>");				fw.newLine();
			fw.write("</head>");								fw.newLine();
			fw.write("<body>");									fw.newLine();
			fw.write("<seq>");									
			
				int anzahl=global.dateilistePlaylist.size();//Anzahl Dateien in der Playlist
				for(int i=0;i<anzahl;i++){
				//hier Dateien einf�gen
				String name=global.dateilistePlaylist.get(i);
				String tmp=name.replace("&","&amp;");//Sonderzeichenschreibweise der Playlist des WMP
				fw.newLine();
				fw.write("<media src=\""+tmp+"\"/>");
				}
				
			fw.newLine();
			fw.write("</seq>\n");								fw.newLine();
			fw.write("</body>\n");								fw.newLine();
			fw.write("</smil>\n");								fw.newLine();
		

			fw.flush();
			fw.close();
		}
		catch( IOException e ){
			e.printStackTrace();
		}	
	}


	 //Windows Media Player Playlist erstellen
	//Aus dem Inhalt der Playlist
public void exportPlaylistMediaplayer()
{
		 String datei=SaveDialogExport();//R�ckgabewert vom Dialog
		 
		 if(!(datei==null)){
			datenSchreibenPlaylist(datei);
		 }
}

//Windows Media Player Playlist erstellen
//Aus dem Inhalt der Tabelle mitte
public void exportPlaylistMediaplayerMitte()
{
	 String datei=SaveDialogExport();//R�ckgabewert vom Dialog
	 
	 if(!(datei==null)){
		datenSchreibenMitte(datei);
	 }
}


//Schreibvorgang f�r Playlist
private void datenSchreibenMitte(String datei)
{
	//batchdatei f�r den Player (Liste)
		try
		{ 
		String fz="\u003F";//Fragezeichen
		String pu="\u002e";//Punkt
 
		@SuppressWarnings("unused")
		File file = new File(datei);
		BufferedWriter fw = new BufferedWriter(new FileWriter(datei));
		
		fw.write("<"+fz+"wpl version=\"1"+pu+"0\""+fz+">");	fw.newLine();
		fw.write("<smil>");									fw.newLine();
		fw.write("<head>");									fw.newLine();
		fw.write("<title>"+datei+"</title>");				fw.newLine();
		fw.write("</head>");								fw.newLine();
		fw.write("<body>");									fw.newLine();
		fw.write("<seq>");									
		
			int anzahl=global.dateilisteMitte.size();//Anzahl Dateien in der Playlist
			for(int i=0;i<anzahl;i++){
			//hier Dateien einf�gen
			String name=global.dateilisteMitte.get(i);
			String tmp=name.replace("&","&amp;");//Sonderzeichenschreibweise der Playlist des WMP
			fw.newLine();
			fw.write("<media src=\""+tmp+"\"/>");
			}
		fw.newLine();
		fw.write("</seq>\n");								fw.newLine();
		fw.write("</body>\n");								fw.newLine();
		fw.write("</smil>\n");								fw.newLine();
	

		fw.flush();
		fw.close();
	}
	catch( IOException e ){
		e.printStackTrace();
	}	
}



@SuppressWarnings("unchecked")
public void openGespeicherteSuche()
{
 	
		String datei=OpenDialogSuche();//R�ckgabewert vom Dialog

		if(!(datei==null))	//falls keine Datei �bergeben wurde nichts tun
		{
			try {
			FileInputStream f_out = new FileInputStream(datei);
	 		 ObjectInputStream obj_out = new ObjectInputStream (f_out);
	 		
	 	        	try {
	 					AlleDaten.dateiliste= (java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}
	 				try {
	 					AlleDaten.dateilisteshort=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}	
	 				try {
	 					AlleDaten.dateilisteTitel=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e1) {
	 					e1.printStackTrace();
	 				}
	 				try {
	 					AlleDaten.dateilisteAlbum=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}	
	 				try {
	 					AlleDaten.dateilisteInterpret=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}
	 				try {
	 					AlleDaten.dateilisteJahr=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}	
	 				try {
	 					AlleDaten.dateilisteBitrate=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}
	 				try {
	 					AlleDaten.dateilisteFrequenz=(java.util.ArrayList<String>) obj_out.readObject();
	 				} catch (ClassNotFoundException e) {
	 					e.printStackTrace();
	 				}	

	 		 			global.baum.ListeF�llen();//Baumansicht neu zeichnen
	 		 			global.tabelle.showInterpret("Alle","AlleDatenAnzeigen");//Tabelle neu zeichnen Alles anzigen
	 	        
	 	     } 
	 		 catch (IOException e) {
	 	         e.printStackTrace();
	 	     }
		}	 	
}	


//Dialogfelder �ffnen
	 public String OpenDialogSuche()
	 {
		 	FileDialog dialog = new FileDialog(global.getShell(), SWT.DIALOG_TRIM);
		 	dialog.setText("Suche �ffnen...");
		 	dialog.setFilterPath("C:/");
		 	dialog.setFilterExtensions(new String[] { "*.src"});
		 	String datei = dialog.open();
		 	
		 	return datei;
	 }
	 
	 public String SaveDialogSuche()
	 {
		 	FileDialog dialog = new FileDialog(global.getShell(), SWT.SAVE);
		 	dialog.setFilterNames(new String[] { "Gespeicherte Suchergebnisse" });
		 	dialog.setText("Suche speichern...");
		 	dialog.setFilterExtensions(new String[] { "*.src"});
		 	
		     dialog.setFilterPath("c:\\");
		 	dialog.setFileName("test.src");
		 	String datei=dialog.open();
		 	
		 	
		 return datei;
	 }
	 

	 public String OpenDialogPlaylist()
	 {
		 	FileDialog dialog = new FileDialog(global.getShell(), SWT.DIALOG_TRIM);
		 	dialog.setText("Playlist �ffnen...");
		 	dialog.setFilterPath("C:/");
		 	dialog.setFilterExtensions(new String[] { "*.pls"});
		 	String datei = dialog.open();
		 	
		 	return datei;
	 }
	 
	 public String SaveDialogPlaylist()
	 {
		 	FileDialog dialog = new FileDialog(global.getShell(), SWT.SAVE);
		 	dialog.setFilterNames(new String[] { "Playlists" });
		 	dialog.setText("Playlist speichern...");
		 	dialog.setFilterExtensions(new String[] { "*.pls"});
		 	
		     dialog.setFilterPath("c:\\");
		 	dialog.setFileName("test.pls");
		 	String datei=dialog.open();
		 	
		 	
		 return datei;
	 }
	 

	 public String SaveDialogExport()
	 {
		 	FileDialog dialog = new FileDialog(global.getShell(), SWT.SAVE);
		 	dialog.setFilterNames(new String[] { "Windows Media Playler Playlists" });
		 	dialog.setText("Windows Media Playler Playlist speichern...");
		 	dialog.setFilterExtensions(new String[] { "*.wpl"});
		 	
		     dialog.setFilterPath("c:\\");
		 	dialog.setFileName("test.wpl");
		 	String datei=dialog.open();
		 	
		 	
		 return datei;
	 }

	 public void saveGespeicherteSuche()
	 {
	 	 	String datei=SaveDialogSuche();//R�ckgabewert vom Dialog
	 	
	 	 	if(!(datei==null))	//falls keine Datei �bergeben wurde nichts tun
	 	 	{
	 	 		try {
	 	 			FileOutputStream f_out = new FileOutputStream(datei);
	 	 			ObjectOutputStream obj_out = new ObjectOutputStream (f_out);
	 	
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateiliste);
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteshort);	
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteTitel);
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteAlbum);	
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteInterpret);
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteJahr);	
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteBitrate);
	 	 			obj_out.writeObject((java.util.ArrayList<String>)AlleDaten.dateilisteFrequenz);	
	 	 				
	 	 			obj_out.flush();
	 	 			f_out.close();
	      } 
	 	 catch (IOException e) {
	          e.printStackTrace();
	      }   
	 } 	
}
	
	 
//f�r den Playbutton in der Toolbar
public void PlaylistAbspielen()
{
		datenSchreibenPlaylist("temp.wpl"); //tempor�re Playlist erstellen
		//Playlist an Mediaplayer �bergeben und Abspielen
		String vereichnis=System.getProperty("user.dir")+"\\temp.wpl";//Im verzeichnis aus dem das Programm gestartet wurde benutzen
		ProcessBuilder builder = new ProcessBuilder( "C:\\Program Files\\Windows Media Player\\wmplayer.exe",vereichnis);	
		@SuppressWarnings("unused")
		Process p = null;
		try {
			p = builder.start();
		} catch (IOException e1) {
			e1.printStackTrace();
		}}
}