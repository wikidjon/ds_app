package a02_text2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a25_clipboard {
	
public static void main(String[] args) {
	Display display = new Display();
	final Clipboard cb = new Clipboard(display);
	Shell shell = new Shell(display);
	shell.setLayout(new FillLayout());
	
	final Text text = new Text(shell, SWT.BORDER | SWT.MULTI | SWT.WRAP);
	
	Menu menu = new Menu(shell, SWT.POP_UP);
	final MenuItem copyItem = new MenuItem(menu, SWT.PUSH);
	copyItem.setText("Kopieren");
	
	copyItem.addSelectionListener(new SelectionAdapter(){
		public void widgetSelected(SelectionEvent e) {
			String selection = text.getSelectionText();
			if (selection.length() == 0) return;
			Object[] data = new Object[]{selection};
			Transfer[] types = new Transfer[] {TextTransfer.getInstance()};
			cb.setContents(data, types);
		}
	});
	final MenuItem einfuegeItem = new MenuItem(menu, SWT.PUSH);
	einfuegeItem.setText ("Einfuegen");
	einfuegeItem.addSelectionListener(new SelectionAdapter(){
		public void widgetSelected(SelectionEvent e) {
			String string = (String)(cb.getContents(TextTransfer.getInstance()));
			if (string != null) text.insert(string);
		}
	});
	menu.addMenuListener(new MenuAdapter() {
		public void menuShown(MenuEvent e) {
			String selection = text.getSelectionText();
			copyItem.setEnabled(selection.length() > 0);
			TransferData[] available = cb.getAvailableTypes();
			boolean enabled = false;
			for (int i = 0; i < available.length; i++) {
				if (TextTransfer.getInstance().isSupportedType(available[i])) {
					enabled = true;
					break;
				}
			}
			einfuegeItem.setEnabled(enabled);
		}
	});
	text.setMenu (menu);

	shell.setSize(200, 200);
	shell.open();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch())
			display.sleep();
	}
	cb.dispose();
	display.dispose();
}
}
