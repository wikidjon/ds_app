package a02_text2.xxx;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a08_CaretPosition {

  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    Text text = new Text(shell, SWT.BORDER | SWT.V_SCROLL);
    text.setBounds(10, 10, 100, 100);
    text.append("Line \n");

    text.setSelection(30);
    System.out.println("selection=" + text.getSelection());
    System.out.println("caret position=" + text.getCaretPosition());
    System.out.println("caret location=" + text.getCaretLocation());

    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}