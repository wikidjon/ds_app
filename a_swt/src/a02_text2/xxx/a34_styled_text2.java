package a02_text2.xxx;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a34_styled_text2 {

	static String text = 	"Da die Gesamtheit der Prozessfunktionalit�ten nur in Anwender-Dynpros implementiert " +
							"ist, werden diese �ber das Batch - Input - Verfahren f�r den externen Zugriff verf�gbar " +
							"gemacht. Dieses Verfahren wird im Abschnitt 2.1.5 ausf�hrlich beschrieben. Dabei bleibt " +
							"es f�r den Gutachter unklar, wie �Nutzereingaben ... automatisch erfolgen� k�nnen (Seite 15). " +
							"Offenbar werden diese Eingaben dann in einer Tabelle gespeichert, die im Folgenden beschrieben wird. " +
							"Auch handelt es sich wohl bei den FNAM und FVAL um Zeilen und nicht um Spalten (S. 16 oben)." ;
	static Image oldImage;
	
	public static void main(String [] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		
		final StyledText styledText = new StyledText(shell, SWT.WRAP | SWT.BORDER);
		styledText.setText(text);
		
		FontData data = display.getSystemFont().getFontData()[0];
		Font font = new Font(display, data.getName(), 16, SWT.BOLD);
		styledText.setFont(font);
		styledText.setForeground(display.getSystemColor (SWT.COLOR_BLUE));
		
		styledText.addListener (SWT.Resize, new Listener () {
			public void handleEvent (Event event) {
				Rectangle rect = styledText.getClientArea ();
				Image newImage = new Image (display, 1, Math.max (1, rect.height));
				GC gc = new GC (newImage);
				gc.setForeground (display.getSystemColor (SWT.COLOR_WHITE));
				gc.setBackground (display.getSystemColor (SWT.COLOR_YELLOW));
				gc.fillGradientRectangle (rect.x, rect.y, 1, rect.height, true);
				gc.dispose ();
//				styledText.setBackgroundImage (newImage);
				if (oldImage != null) oldImage.dispose ();
				oldImage = newImage;
			}
		});	
		shell.setSize(700, 400);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		if (oldImage != null) oldImage.dispose ();
		font.dispose();
		display.dispose();
	}
}
