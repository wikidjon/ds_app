package a02_text2.xxx;

import org.eclipse.swt.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class a40_ChooseFont {
  private Font font;
  private Color color;

  public void run() {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setText("Font Auswahl");
    createContents(shell);
    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }

    if (font != null) font.dispose();
    if (color != null) color.dispose();

    display.dispose();
  }

  private void createContents(final Shell shell) {
    shell.setLayout(new GridLayout(2, false));

    final Label fontLabel = new Label(shell, SWT.NONE);
    fontLabel.setText("ausgewählter font: ");

    Button button = new Button(shell, SWT.PUSH);
    button.setText("Font...");
    button.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent event) {
    
        FontDialog dlg = new FontDialog(shell);

   
        if (font != null) dlg.setFontList(fontLabel.getFont().getFontData());
        if (color != null) dlg.setRGB(color.getRGB());

        if (dlg.open() != null) {
         
          if (font != null) font.dispose();
          if (color != null) color.dispose();

         
          font = new Font(shell.getDisplay(), dlg.getFontList());
          fontLabel.setFont(font);

         
          color = new Color(shell.getDisplay(), dlg.getRGB());
          fontLabel.setForeground(color);

        }
      }
    });
  }


  public static void main(String[] args) {
    new a40_ChooseFont().run();
  }
}
