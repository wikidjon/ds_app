package a02_text2.xxx;
/***********************************************
SWT offers the Text class for text-entry fields. 
Style			Description
SWT.MULTI		Creates a multiple-line text field.
SWT.SINGLE		Creates a single-line text field. This is the default. You may specifiy only one of SWT.MULTI or SWT.SINGLE.
SWT.READ_ONLY	Creates a text field with uneditable contents.
SWT.WRAP		With multiple-line text fields, causes text to wrap.
SWT.BORDER		Draws a border around the text field. Note that this style isn't set by default, and your text fields will look funny without it.
SWT.CENTER		Centers the text in this text field.
SWT.LEFT		Left-aligns the text in this text field. This is the default.
SWT.RIGHT		Right-aligns the text in this text field. You may specify only one of SWT.CENTER, SWT.LEFT, or SWT.RIGHT.
SWT.PASSWORD	Creates a text field suitable for password entry-it doesn't display the actual characters the user types, but rather it displays asterisks.
SWT.H_SCROLL	Creates a horizontal scrollbar to scroll this text field.
SWT.V_SCROLL	Creates a vertical scrollbar to scroll this text field.




SWT.SINGLE | SWT.LEFT is set by default

*********************************/