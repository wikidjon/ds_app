package a02_text2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a24_ein_button {

	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);

		final Label label = new Label (shell, SWT.NONE);
		label.setText ("Ihr Name:");

		final Text text = new Text (shell, SWT.BORDER);		// Achtung final!!!
		// keine Ref. aus innerer Klasse mgl.-> siehe ok Button
		text.setLayoutData (new RowData (100, SWT.DEFAULT));

		final Button ok = new Button (shell, SWT.PUSH);
		ok.setText ("OK");

		ok.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				System.out.println("OK "+ text.getSelectionText());
			}
		});

		final Button cancel = new Button (shell, SWT.PUSH);
		cancel.setText ("Abbruch");

		cancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				System.out.println("Abbruch");
			}
		});

		shell.setDefaultButton (cancel);
		shell.setLayout (new RowLayout ());
		shell.pack ();
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
