package a02_text2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a26_printer2 {
	public static void main(String[] args) {
		new a26_printer2().open();
	}
	Display display;
	Shell shell;
	Text text;
	Font font;

	Color foregroundColor, backgroundColor;
	Printer printer;
	GC gc;
	FontData[] printerFontData;

	RGB printerForeground, printerBackground;
	int lineHeight = 0;
	int tabWidth = 0;
	int leftMargin, rightMargin, topMargin, bottomMargin;
	int x, y;
	int index, end;
	String textToPrint;
	String tabs;

	StringBuffer wordBuffer;

	void menuBackgroundColor() {
		ColorDialog colorDialog = new ColorDialog(this.shell);
		colorDialog.setRGB(this.text.getBackground().getRGB());
		RGB rgb = colorDialog.open();
		if (rgb != null) {
			if (this.backgroundColor != null) {
				this.backgroundColor.dispose();
			}
			this.backgroundColor = new Color(this.display, rgb);
			this.text.setBackground(this.backgroundColor);
		}
	}

	void menuFont() {
		FontDialog fontDialog = new FontDialog(this.shell);
		fontDialog.setFontList(this.text.getFont().getFontData());
		FontData fontData = fontDialog.open();
		if (fontData != null) {
			if (this.font != null) {
				this.font.dispose();
			}
			this.font = new Font(this.display, fontData);
			this.text.setFont(this.font);
		}
	}

	void menuForegroundColor() {
		ColorDialog colorDialog = new ColorDialog(this.shell);
		colorDialog.setRGB(this.text.getForeground().getRGB());
		RGB rgb = colorDialog.open();
		if (rgb != null) {
			if (this.foregroundColor != null) {
				this.foregroundColor.dispose();
			}
			this.foregroundColor = new Color(this.display, rgb);
			this.text.setForeground(this.foregroundColor);
		}
	}

	void menuOpen() {
		final String textString;
		FileDialog dialog = new FileDialog(this.shell, SWT.OPEN);
		dialog.setFilterExtensions(new String[] { "*.java", "*.*" });
		String name = dialog.open();
		if ((name == null) || (name.length() == 0))
			return;

		try {
			File file = new File(name);
			FileInputStream stream = new FileInputStream(file.getPath());
			try {
				Reader in = new BufferedReader(new InputStreamReader(stream));
				char[] readBuffer = new char[2048];
				StringBuffer buffer = new StringBuffer((int) file.length());
				int n;
				while ((n = in.read(readBuffer)) > 0) {
					buffer.append(readBuffer, 0, n);
				}
				textString = buffer.toString();
				stream.close();
			} catch (IOException e) {
				MessageBox box = new MessageBox(this.shell, SWT.ICON_ERROR);
				box.setMessage("Error reading file:\n" + name);
				box.open();
				return;
			}
		} catch (FileNotFoundException e) {
			MessageBox box = new MessageBox(this.shell, SWT.ICON_ERROR);
			box.setMessage("File not found:\n" + name);
			box.open();
			return;
		}
		this.text.setText(textString);
	}

	void menuPrint() {
		PrintDialog dialog = new PrintDialog(this.shell, SWT.NONE);
		PrinterData data = dialog.open();
		if (data == null)
			return;
		if (data.printToFile) {
			data.fileName = "print.out"; // you probably want to ask the user
			// for a filename
		}

		/*
		 * Get the text to print from the Text widget (you could get it from
		 * anywhere, i.e. your java model)
		 */
		this.textToPrint = this.text.getText();

		/* Get the font & foreground & background data. */
		this.printerFontData = this.text.getFont().getFontData();
		this.printerForeground = this.text.getForeground().getRGB();
		this.printerBackground = this.text.getBackground().getRGB();

		/*
		 * Do the printing in a background thread so that spooling does not
		 * freeze the UI.
		 */
		this.printer = new Printer(data);
		Thread printingThread = new Thread("Printing") {
			@Override
			public void run() {
				print(a26_printer2.this.printer);
				a26_printer2.this.printer.dispose();
			}
		};
		printingThread.start();
	}

	void newline() {
		this.x = this.leftMargin;
		this.y += this.lineHeight;
		if (this.y + this.lineHeight > this.bottomMargin) {
			this.printer.endPage();
			if (this.index + 1 < this.end) {
				this.y = this.topMargin;
				this.printer.startPage();
			}
		}
	}

	void open() {
		this.display = new Display();
		this.shell = new Shell(this.display);
		this.shell.setLayout(new FillLayout());
		this.shell.setText("Print Text");
		this.text = new Text(this.shell, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL
				| SWT.H_SCROLL);

		Menu menuBar = new Menu(this.shell, SWT.BAR);
		this.shell.setMenuBar(menuBar);
		MenuItem item = new MenuItem(menuBar, SWT.CASCADE);
		item.setText("&File");
		Menu fileMenu = new Menu(this.shell, SWT.DROP_DOWN);
		item.setMenu(fileMenu);
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("&Open...");
		item.setAccelerator(SWT.CTRL + 'O');
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				menuOpen();
			}
		});
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("Font...");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				menuFont();
			}
		});
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("Foreground Color...");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				menuForegroundColor();
			}
		});
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("Background Color...");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				menuBackgroundColor();
			}
		});
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("&Print...");
		item.setAccelerator(SWT.CTRL + 'P');
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				menuPrint();
			}
		});
		new MenuItem(fileMenu, SWT.SEPARATOR);
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("E&xit");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				System.exit(0);
			}
		});

		this.shell.open();
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				this.display.sleep();
			}
		}
		if (this.font != null) {
			this.font.dispose();
		}
		if (this.foregroundColor != null) {
			this.foregroundColor.dispose();
		}
		if (this.backgroundColor != null) {
			this.backgroundColor.dispose();
		}
		this.display.dispose();
	}

	void print(Printer printer) {
		if (printer.startJob("Text")) { // the string is the job name - shows up
			// in the printer's job list
			Rectangle clientArea = printer.getClientArea();
			Rectangle trim = printer.computeTrim(0, 0, 0, 0);
			Point dpi = printer.getDPI();
			this.leftMargin = dpi.x + trim.x; // one inch from left side of paper
			this.rightMargin = clientArea.width - dpi.x + trim.x + trim.width; // one
			// inch
			// from
			// right
			// side
			// of
			// paper
			this.topMargin = dpi.y + trim.y; // one inch from top edge of paper
			this.bottomMargin = clientArea.height - dpi.y + trim.y + trim.height; // one
			// inch
			// from
			// bottom
			// edge
			// of
			// paper

			/* Create a buffer for computing tab width. */
			int tabSize = 4; // is tab width a user setting in your UI?
			StringBuffer tabBuffer = new StringBuffer(tabSize);
			for (int i = 0; i < tabSize; i++) {
				tabBuffer.append(' ');
			}
			this.tabs = tabBuffer.toString();

			/*
			 * Create printer GC, and create and set the printer font &
			 * foreground color.
			 */
			this.gc = new GC(printer);
			Font printerFont = new Font(printer, this.printerFontData);
			Color printerForegroundColor = new Color(printer, this.printerForeground);
			Color printerBackgroundColor = new Color(printer, this.printerBackground);

			this.gc.setFont(printerFont);
			this.gc.setForeground(printerForegroundColor);
			this.gc.setBackground(printerBackgroundColor);
			this.tabWidth = this.gc.stringExtent(this.tabs).x;
			this.lineHeight = this.gc.getFontMetrics().getHeight();

			/* Print text to current gc using word wrap */
			printText();
			printer.endJob();

			/* Cleanup graphics resources used in printing */
			printerFont.dispose();
			printerForegroundColor.dispose();
			printerBackgroundColor.dispose();
			this.gc.dispose();
		}
	}

	void printText() {
		this.printer.startPage();
		this.wordBuffer = new StringBuffer();
		this.x = this.leftMargin;
		this.y = this.topMargin;
		this.index = 0;
		this.end = this.textToPrint.length();
		while (this.index < this.end) {
			char c = this.textToPrint.charAt(this.index);
			this.index++;
			if (c != 0) {
				if (c == 0x0a || c == 0x0d) {
					if (c == 0x0d && this.index < this.end
							&& this.textToPrint.charAt(this.index) == 0x0a) {
						this.index++; // if this is cr-lf, skip the lf
					}
					printWordBuffer();
					newline();
				} else {
					if (c != '\t') {
						this.wordBuffer.append(c);
					}
					if (Character.isWhitespace(c)) {
						printWordBuffer();
						if (c == '\t') {
							this.x += this.tabWidth;
						}
					}
				}
			}
		}
		if (this.y + this.lineHeight <= this.bottomMargin) {
			this.printer.endPage();
		}
	}

	void printWordBuffer() {
		if (this.wordBuffer.length() > 0) {
			String word = this.wordBuffer.toString();
			int wordWidth = this.gc.stringExtent(word).x;
			if (this.x + wordWidth > this.rightMargin) {
				/* word doesn't fit on current line, so wrap */
				newline();
			}
			this.gc.drawString(word, this.x, this.y, false);
			this.x += wordWidth;
			this.wordBuffer = new StringBuffer();
		}
	}
}
