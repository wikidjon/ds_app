package a02_text2;


import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/********************************************
 * erkennen des scrollens
 *******************************************/

public class a27_scroll {
public static void main(String[] args) {
	Display display = new Display ();
	Shell shell = new Shell (display);
	shell.setLayout (new FillLayout ());
	
	final Text text = new Text (shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
	for (int i=0; i<32; i++) {
		text.append (i + " - Das ist eine Textzeile in einem Widget - " + i + "\n");
	}
	text.setSelection (0);
	
	Listener listener = new Listener () {
		int lastIndex = text.getTopIndex ();
		public void handleEvent (Event e) {
			int index = text.getTopIndex ();
			if (index != lastIndex) {
				lastIndex = index;
				System.out.println ("gescrolled, topIndex=" + index);
			}
		}
	};
	
	/* benutzerdefiniertes scrollen */
	text.addListener (SWT.MouseDown, listener);
	text.addListener (SWT.MouseMove, listener);
	text.addListener (SWT.MouseUp, listener);
	text.addListener (SWT.KeyDown, listener);
	text.addListener (SWT.KeyUp, listener);
	text.addListener (SWT.Resize, listener);
	
	ScrollBar hBar = text.getHorizontalBar();
	if (hBar != null) {
		hBar.addListener (SWT.Selection, listener);
	}
	ScrollBar vBar = text.getVerticalBar();
	if (vBar != null) {
		vBar.addListener (SWT.Selection, listener);
	}
	
	shell.pack ();
	
	Point size = shell.computeSize (SWT.DEFAULT, SWT.DEFAULT);
	shell.setSize (size. x - 32, size.y / 2);
	
	shell.open ();
	while (!shell.isDisposed ()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
}