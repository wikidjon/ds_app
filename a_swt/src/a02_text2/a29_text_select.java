package a02_text2;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a29_text_select {

public static void main (String [] args) {
	Display display = new Display ();
	Shell shell = new Shell (display);
	
	Text text = new Text (shell, 0);
	text.setText ("ASDF");
	text.setSize (64, 32);
	text.selectAll ();
	
	shell.pack ();
	shell.open ();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
}
} 
