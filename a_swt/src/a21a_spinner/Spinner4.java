package a21a_spinner;

/*
 * Spinner example snippet: create and initialize a spinner widget
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;

public class Spinner4 {

  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    Spinner spinner = new Spinner(shell, SWT.BORDER);
    spinner.setMinimum(0);
    spinner.setMaximum(1000);
    spinner.setSelection(500);
    spinner.setIncrement(1);
    spinner.setPageIncrement(100);
    spinner.pack();
    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    display.dispose();
  }
}
