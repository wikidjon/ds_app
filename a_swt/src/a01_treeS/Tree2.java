package a01_treeS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class Tree2 {

	public static void main(String[] args) {
		final Display d = new Display();
		Shell s = new Shell(d);
		s.setSize(250,200);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("Tree Beispiel 2");
		s.setLayout(new FillLayout(SWT.HORIZONTAL));   //VERTICAL

		final Tree t = new Tree(s, SWT.SINGLE | SWT.BORDER);

		final TreeItem kind1 = new TreeItem(t, SWT.NONE, 0);
		kind1.setText("1");
		kind1.setImage(new Image(d, "src/icons/folder.gif"));

		final TreeItem kind2 = new TreeItem(t, SWT.NONE, 1);
		kind2.setText("2");
		kind2.setImage(new Image(d, "src/icons/cut.gif"));

		final TreeItem kind2a = new TreeItem(kind2, SWT.NONE, 0);
		kind2a.setText("2A");
		kind2a.setImage(new Image(d, "src/icons/folder.gif"));

		kind2a.addListener (SWT.Selection, new Listener () {
			@Override
			public void handleEvent (Event e) {
				System.out.println("Teste");
			}

		});
		final TreeItem kind2b = new TreeItem(kind2, SWT.NONE, 1);
		kind2b.setText("2B");

		final TreeItem kind3 = new TreeItem(t, SWT.NONE, 2);
		kind3.setText("3");
		kind3.setImage(new Image(d, "src/icons/cut.gif"));

		//rechte Seite
		final List l = new List(s, SWT.BORDER | SWT.SINGLE);
		l.setBackground(d.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));

		t.addTreeListener(new TreeListener() {
			@Override
			public void treeCollapsed(TreeEvent e) {
				TreeItem ti = (TreeItem)e.item;
				ti.setImage(new Image(d, "src/icons/folder.gif"));
			}

			@Override
			public void treeExpanded(TreeEvent e) {
				TreeItem ti = (TreeItem)e.item;
				ti.setImage(new Image(d, "src/icons/cut.gif"));
			}
		});

		t.addSelectionListener(new SelectionAdapter() {
			private void populateList(String type){
				if(type.equals("1"))
				{
					l.removeAll();
					l.add("Datei 1");
					l.add("Datei 2");
				}
				if(type.equals("2"))
				{
					l.removeAll();
				}
				if(type.equals("2A"))
				{
					l.removeAll();
					l.add("Datei 3");
					l.add("Datei 4");

				}
				if(type.equals("2B"))
				{
					l.removeAll();
					l.add("Datei 5");
					l.add("Datei 6");
				}
				if(type.equals("3"))
				{
					l.removeAll();
					l.add("Datei 7");
					l.add("Datei 8");
				}

			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeItem ti = (TreeItem)e.item;
				populateList(ti.getText());
			}
		});
		s.open();
		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
