package calendar;

/**
 * Kalenderdialog als swt dialog.
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class KalenderDialog extends Dialog implements MouseListener {

	public static void main(final String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setText("");

		final FillLayout fl = new FillLayout();
		shell.setLayout(fl);

		final Button oeffnen = new Button(shell, SWT.PUSH);
		oeffnen.setText("oeffnen");
		oeffnen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				final KalenderDialog kd = new KalenderDialog(shell);
				System.out.println(kd.open());
				//kd.open();  //liefert ausgew�hltes Datum zur�ck
			}
		});
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private Display display = null;

	private Date aktuellesDatum = null;
	private String ausgewaehltesDatum = null;
	private Shell shell = null;
	private GridLayout gridLayout = null;

	private GridData gridData = null;
	private CLabel Sonntag = null;
	private CLabel Montag = null;
	private CLabel Dienstag = null;
	private CLabel Mittwoch = null;
	private CLabel Donnerstag = null;
	private CLabel Freitag = null;

	private CLabel Samstag = null;
	private Button vorherigesJahr = null;
	private Button naechstesJahr = null;
	private Button vorherigerMonat = null;
	private Button naechsterMonat = null;

	private CLabel aktuellLabel = null;

	private final CLabel[] tage = new CLabel[42];

	public KalenderDialog(final Shell parent) {
		this(parent, 0);
	}

	public KalenderDialog(final Shell parent, final int style) {
		super(parent, style);
	}

	private int getLetzterTagMonat(final int Jahr, final int Monat) {
		if (Monat == 1 || Monat == 3 || Monat == 5 || Monat == 7 || Monat == 8 || Monat == 10 || Monat == 12)
			return 31;
		if (Monat == 4 || Monat == 6 || Monat == 9 || Monat == 11)
			return 30;
		if (Monat == 2) {
			if (istSchaltJahr(Jahr))
				return 29;
			else
				return 28;
		}
		return 0;
	}

	public boolean istSchaltJahr(final int Jahr) {
		return (Jahr % 4 == 0 && Jahr % 100 != 0) || (Jahr % 400 == 0);
	}

	@Override
	public void mouseDoubleClick(final MouseEvent e) {
		final CLabel day = (CLabel) e.getSource();
		if (!day.getText().equals("")) {
			this.ausgewaehltesDatum = this.aktuellLabel.getText() + "-" + day.getText();
			this.shell.close();
		}
	}

	@Override
	public void mouseDown(final MouseEvent e) {
	}

	@Override
	public void mouseUp(final MouseEvent e) {
	}

	private void moveTo(final int type, final int value) {
		final Calendar aktuell = Calendar.getInstance(); 		// aktuelles Kalenderobject
		aktuell.setTime(this.aktuellesDatum); 					// akturelles Datum
		aktuell.add(type, value); 								// add to spec time.
		this.aktuellesDatum = new Date(aktuell.getTimeInMillis()); // resultat
		final SimpleDateFormat formatter = new SimpleDateFormat("mm/yyyy");// format
		this.aktuellLabel.setText(formatter.format(this.aktuellesDatum)); // setzen des labels
		setDayForDisplay(aktuell);
	}

	public void naechsterMonat() {
		moveTo(Calendar.MONTH, 1);
	}

	public void naechstesJahr() {
		moveTo(Calendar.YEAR, 1);
	}

	public Object open() {
		final Shell parent = getParent();
		this.display = Display.getDefault();
		this.shell = new Shell(parent, SWT.TITLE | SWT.PRIMARY_MODAL);
		this.shell.setText("Kalender v. 0.01");
		this.shell.setSize(230, 220);

		this.gridLayout = new GridLayout();
		this.gridLayout.numColumns = 7;
		this.shell.setLayout(this.gridLayout);

		this.gridData = new GridData(GridData.FILL_HORIZONTAL);
		this.vorherigesJahr = new Button(this.shell, SWT.PUSH | SWT.FLAT);
		this.vorherigesJahr.setText("<");
		this.vorherigesJahr.setLayoutData(this.gridData);
		this.vorherigesJahr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				vorherigesJahr();
			}
		});

		this.gridData = new GridData(GridData.FILL_HORIZONTAL);
		this.vorherigerMonat = new Button(this.shell, SWT.PUSH | SWT.FLAT);
		this.vorherigerMonat.setText("<<");
		this.vorherigerMonat.setLayoutData(this.gridData);
		this.vorherigerMonat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				vorherigerMonat();
			}
		});

		this.aktuellLabel = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL);
		this.gridData.horizontalSpan = 3;
		this.aktuellLabel.setLayoutData(this.gridData);
		final SimpleDateFormat formatter = new SimpleDateFormat("mm/yyyy");   //?????
		this.aktuellLabel.setText(formatter.format(new Date()));

		this.gridData = new GridData(GridData.FILL_HORIZONTAL);
		this.naechsterMonat = new Button(this.shell, SWT.PUSH | SWT.FLAT);
		this.naechsterMonat.setText(">>");
		this.naechsterMonat.setLayoutData(this.gridData);
		this.naechsterMonat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				naechsterMonat();
			}
		});

		this.gridData = new GridData(GridData.FILL_HORIZONTAL);
		this.naechstesJahr = new Button(this.shell, SWT.PUSH | SWT.FLAT);
		this.naechstesJahr.setText(">");
		this.naechstesJahr.setLayoutData(this.gridData);
		this.naechstesJahr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				naechstesJahr();
			}
		});

		this.Sonntag = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Sonntag.setLayoutData(this.gridData);
		this.Sonntag.setText("So");

		this.Montag = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Montag.setLayoutData(this.gridData);
		this.Montag.setText("Mo");

		this.Dienstag = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Dienstag.setLayoutData(this.gridData);
		this.Dienstag.setText("Di");

		this.Mittwoch = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Mittwoch.setLayoutData(this.gridData);
		this.Mittwoch.setText("Mi");

		this.Donnerstag = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Donnerstag.setLayoutData(this.gridData);
		this.Donnerstag.setText("Do");

		this.Freitag = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Freitag.setLayoutData(this.gridData);
		this.Freitag.setText("Fr");

		this.Samstag = new CLabel(this.shell, SWT.CENTER | SWT.SHADOW_OUT);
		this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		this.gridData.widthHint = 20;
		this.gridData.heightHint = 20;
		this.Samstag.setLayoutData(this.gridData);
		this.Samstag.setText("Sa");

		for (int i = 0; i < 42; i++) {
			this.tage[i] = new CLabel(this.shell, SWT.FLAT | SWT.CENTER);
			this.gridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
			this.tage[i].setLayoutData(this.gridData);
			this.tage[i].setBackground(this.display.getSystemColor(SWT.COLOR_WHITE));
			this.tage[i].addMouseListener(this);
			this.tage[i].setToolTipText("Doppelklick gibt das aktuelle Datum.");
		}

		final Calendar aktuell = Calendar.getInstance(); //
		this.aktuellesDatum = new Date(aktuell.getTimeInMillis());
		setDayForDisplay(aktuell);

		this.shell.open();
		final Display display = parent.getDisplay();
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return this.ausgewaehltesDatum;
	}

	private void setDayForDisplay(final Calendar aktuell) {
		final int currentDay = aktuell.get(Calendar.DATE);
		aktuell.add(Calendar.DAY_OF_MONTH, -(aktuell.get(Calendar.DATE) - 1)); //
		final int startIndex = aktuell.get(Calendar.DAY_OF_WEEK) - 1; //
		final int Jahr = aktuell.get(Calendar.YEAR); //
		final int Monat = aktuell.get(Calendar.MONTH) + 1; //
		final int lastDay = this.getLetzterTagMonat(Jahr, Monat); //
		final int endIndex = startIndex + lastDay - 1; //
		int startday = 1;
		for (int i = 0; i < 42; i++) {
			final Color temp = this.tage[i].getBackground();
			if (temp.equals(this.display.getSystemColor(SWT.COLOR_BLUE))) {

				this.tage[i].setBackground(this.display.getSystemColor(SWT.COLOR_WHITE));
			}
		}
		for (int i = 0; i < 42; i++) {
			if (i >= startIndex && i <= endIndex) {
				this.tage[i].setText("" + startday);
				if (startday == currentDay) {

					this.tage[i].setBackground(this.display
							.getSystemColor(SWT.COLOR_BLUE)); //
				}
				startday++;
			} else {
				this.tage[i].setText("");
			}
		}

	}

	public void vorherigerMonat() {
		moveTo(Calendar.MONTH, -1);
	}

	public void vorherigesJahr() {
		moveTo(Calendar.YEAR, -1);
	}
}
