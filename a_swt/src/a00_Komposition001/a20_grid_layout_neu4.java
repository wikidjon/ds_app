package a00_Komposition001;

/*************************************************************************
 * 3 Zeilen (grid), wobei die obere und untere nicht gr��er wird, sondern
 * nur die mittlere Zeile,
 * die Mittlere Zeile ist nochmals durch ein grid mit 3 Spalten untersetzt
 * unterschiedliche Gr��e / Wachstum der mittleren Komponenten
 ************************************************************************/

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class a20_grid_layout_neu4 {

	static Label label1;

	public static void main(String[] args) {
		Display display = new Display();

		Shell shell = new Shell(display);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;

		// oben und unten sollen nur breiter werden
		GridData gridData1 = new GridData();
		gridData1.horizontalAlignment = GridData.FILL;
		gridData1.heightHint = 30; // !!!! H�he der Iconleiste

		GridData gridData1a = new GridData();
		gridData1a.horizontalAlignment = GridData.FILL;
		gridData1a.heightHint = 15; // !!!! H�he der Statuszeile

		GridData gridData2 = new GridData();
		gridData2.grabExcessHorizontalSpace = true;
		gridData2.grabExcessVerticalSpace = true;
		gridData2.horizontalAlignment = GridData.FILL;
		gridData2.verticalAlignment = GridData.FILL;

		GridData gridData3 = new GridData();				// u.a. f�r SashForm
		gridData3.grabExcessHorizontalSpace = true;
		gridData3.grabExcessVerticalSpace = true;
		gridData3.horizontalAlignment = GridData.FILL;
		gridData3.verticalAlignment = GridData.FILL;
		gridData3.minimumWidth = 40;
		//gridData3.horizontalIndent = 40; // horuzontale Einr�ckung

		// gridlayout fuer Mitte
		GridLayout gridLayout2 = new GridLayout();
		gridLayout2.numColumns = 3;
		//gridLayout2.verticalSpacing=0;

		// 1. composite - oben f�r  Men� ************************************************
		// ******************************************************************************
		final Composite comp = new Composite(shell, SWT.NONE);
		//comp.setBackground(new Color(display, 0, 255, 255));						// ///auskommentieren !!
		comp.setLayoutData(gridData1);

		a20_menue2 men = new a20_menue2();
		men.menue2(shell, display, comp);

		final CoolBar bar = new CoolBar(comp, SWT.NONE);
		bar.setSize(780, 70);
		bar.setLocation(0, 0);
		// images f�r coolbar
		final Image saveIcon = new Image(display, "src/icons/folder.gif");
		final Image openIcon = new Image(display, "src/icons/open1.gif");
		final Image cutIcon = new Image(display, "src/icons/hotel.gif");
		final Image copyIcon = new Image(display, "src/icons/cancel.gif");
		final Image pasteIcon = new Image(display, "src/icons/cut.gif");

		// oeffnenbutton
		final CoolItem openCoolItem = new CoolItem(bar, SWT.NONE);
		final Button openBtn = new Button(bar, SWT.PUSH);
		openBtn.setImage(openIcon);
		openBtn.pack();
		Point size = openBtn.getSize();
		openCoolItem.setControl(openBtn);
		openCoolItem.setSize(openCoolItem.computeSize(size.x, size.y));

		// savebutton
		final CoolItem saveCoolItem = new CoolItem(bar, SWT.NONE);
		final Button saveBtn = new Button(bar, SWT.FLAT);
		saveBtn.setImage(saveIcon);
		saveBtn.setText("Speichern");
		saveBtn.pack();
		size = saveBtn.getSize();
		saveCoolItem.setControl(saveBtn);
		saveCoolItem.setSize(saveCoolItem.computeSize(size.x, size.y));

		// cutbutton
		final CoolItem cutCoolItem = new CoolItem(bar, SWT.PUSH);
		final Button cutBtn = new Button(bar, SWT.PUSH);
		cutBtn.setImage(cutIcon);
		cutBtn.pack();
		size = cutBtn.getSize();
		cutCoolItem.setControl(cutBtn);
		cutCoolItem.setSize(cutCoolItem.computeSize(size.x, size.y));

		// copybutton
		final CoolItem copyCoolItem = new CoolItem(bar, SWT.PUSH);
		final Button copyBtn = new Button(bar, SWT.PUSH);
		copyBtn.setImage(copyIcon);
		copyBtn.pack();
		size = copyBtn.getSize();
		copyCoolItem.setControl(copyBtn);
		copyCoolItem.setSize(copyCoolItem.computeSize(size.x, size.y));

		// paste button
		final CoolItem pasteCoolItem = new CoolItem(bar, SWT.PUSH);
		final Button pasteBtn = new Button(bar, SWT.PUSH);
		pasteBtn.setImage(pasteIcon);
		pasteBtn.pack();
		size = pasteBtn.getSize();
		pasteCoolItem.setControl(pasteBtn);
		pasteCoolItem.setSize(pasteCoolItem.computeSize(size.x, size.y));
		pasteCoolItem.setMinimumSize(size);

		// 2. composite - Mitte - wird weiter unterteilt
		// *******************************
		// ******************************************************************************
		final Composite comp1 = new Composite(shell, SWT.NONE);
		comp1.setBackground(new Color(display, 255, 255, 255));
		comp1.setLayoutData(gridData);
		//comp1.setBackground(new Color(display, 0, 0, 0));
		// ************************************************
		SashForm sForm = new SashForm(comp1, SWT.HORIZONTAL);
		sForm.setLayoutData(gridData3);
		// ************************************************

		// mittlerer Bereich *****************************
		comp1.setLayout(gridLayout2);

		final Composite comp11 = new Composite(sForm, SWT.BORDER);
		//		comp11.setBackground(new Color(display, 255, 0, 0));
		comp11.setLayoutData(gridData3);

		//******************************
		final Tree tree = new Tree(comp11, SWT.NONE);
		// c.setBackground(new Color(display, 231, 133, 31));
		//		com.setBackground(new Color(d, 231, 133, 31));
		tree.setSize(100, 250); //150, 250
		tree.setLocation(5,5);

		TreeItem ebene1 = new TreeItem(tree, SWT.NONE);
		ebene1.setText("Oma");
		TreeItem ebene1a = new TreeItem(tree, SWT.NONE);
		ebene1a.setText("Opa");
		TreeItem ebene1b = new TreeItem(tree, SWT.NONE);
		ebene1b.setText("Weihnachtsmann");
		TreeItem ebene1c = new TreeItem(tree, SWT.NONE);     //ebene1
		ebene1c.setText("Hausfreund");
		TreeItem ebene1d = new TreeItem(tree, SWT.NONE);     //ebene1
		ebene1d.setText("AAAAAA");
		//zweite Ebene
		TreeItem ebene12 = new TreeItem(ebene1, SWT.NONE,0);
		ebene12.setText("Tochter");
		TreeItem ebene120 = new TreeItem(ebene1, SWT.NONE, 1);
		ebene120.setText("Sohn");
		TreeItem ebene121 = new TreeItem(ebene1b, SWT.NONE, 0);
		ebene121.setText("Weihnachtsmaennchen");
		TreeItem ebene122 = new TreeItem(ebene1a, SWT.NONE, 0);
		ebene122.setText("(Freundin!!!)");

		TreeItem ebene130 = new TreeItem(ebene12, SWT.NONE);
		ebene130.setText("Enkelsohn");

		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeItem[] t = tree.getSelection();
				System.out.print("Auswahl: ");
				for(int i = 0; i < t.length; i++) {
					System.out.print(t[i].getText() + ", ");
					//a_MenuShell_composite.setLabelText(t[i].getText());
				}
				System.out.println();
			}
		});

		tree.addTreeListener(new TreeListener() {
			@Override
			public void treeCollapsed(TreeEvent e) {
				System.out.println("Tree collapsed.");
			}
			@Override
			public void treeExpanded(TreeEvent e) {
				System.out.println("Tree expanded.");
			}
		});

		//***************************



		final Composite comp12 = new Composite(sForm,SWT.BORDER);
		//		comp12.setBackground(new Color(display, 255, 255,255));
		//		this.bildText = new Text(shell, SWT.SINGLE | SWT.BORDER);
		comp12.setLayoutData(gridData);
		Text t = new Text(comp12,  SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		t.setBounds(display.getClientArea ());
		t.setLayoutData(new GridData(GridData.FILL_BOTH));

		t.setEditable(true);
		t.setText("Hallo");
		//t.pack();


		final Composite comp13 = new Composite(sForm, SWT.BORDER);
		comp13.setBackground(new Color(display, 0, 0, 255));
		comp13.setLayoutData(gridData3);
		// Ende mittlerer Bereich ************************

		// 3. composite - unten
		// *******************************************************
		// ******************************************************************************

		final Composite comp2 = new Composite(shell, SWT.BORDER);
		// comp2.setBackground(new Color(display, 255, 255, 0));
		comp2.setLayoutData(gridData1a);

		Label label1 = new Label(comp2, SWT.NONE);
		label1.setText("Hallo");
		label1.pack();

		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
	}

	static void setLabelText(String object) {
		label1.setText(object);
	}
	/***********************************
	protected Label createLabel(Composite c, String text) {
		return createLabel(c, text, null);
	}


	protected Label createLabel(Composite c, String text, Image icon) {
		return createLabel(c, text, icon, SWT.LEFT);
	}
	 ************************************/
}
