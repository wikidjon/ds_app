package a00_Komposition001;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class a20_menue2{

	private static Image createImage(String name) {
		return new Image(
				Display.getCurrent(),"src/icons/"+ name + ".gif");
		//			a04_Menue_toolbar.class.getResourceAsStream("../icons/" + name + ".gif"));
	}
	@SuppressWarnings("unused")
	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	@SuppressWarnings("unused")
	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	public void menue2(Shell s, Display de, Composite c){			//Display von d auf de ge�ndert
		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;

		SelectionListener l = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
			}
		};

		//		FillLayout fillLayout = new FillLayout (SWT.HORIZONTAL);
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);

		//	Label sep1 = new Label(s, SWT.SEPARATOR| SWT.HORIZONTAL | SWT.SHADOW_IN );
		//	sep1.setBounds(30,60,100,20);


		menu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);
		;
	}
}