package a11_table_old;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
//import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.*;

public class TableShellExample3 {
    
    Display d;
    Shell s;
    
	public static void main(String[] args) {
	    new TableShellExample3();
		}
    
    TableShellExample3()    {
        d = new Display();
        s = new Shell(d);
        
        s.setSize(250,200);
        s.setImage(new Image(d, "src/icons/cut.gif"));
        s.setText("Tabellen Shell Beispiel");
        GridLayout gl = new GridLayout();
        gl.numColumns = 4;
        s.setLayout(gl);
        
        final Table t = new Table(s, SWT.BORDER | SWT.CHECK | 
            SWT.MULTI | SWT.FULL_SELECTION);
        final GridData gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 4;
        t.setLayoutData(gd);
        t.setHeaderVisible(true);
        final TableColumn tc1 = new TableColumn(t, SWT.LEFT);
        final TableColumn tc2 = new TableColumn(t, SWT.CENTER);
        final TableColumn tc3 = new TableColumn(t, SWT.CENTER);
        tc1.setText("Vorname");
        tc2.setText("Nachname");
        tc3.setText("Addresse");
        tc1.setWidth(70);
        tc2.setWidth(70);
        tc3.setWidth(80);
        final TableItem item1 = new TableItem(t,SWT.NONE);
        item1.setText(new String[] {"Eric","Clapton","Halle"});
        final TableItem item2 = new TableItem(t,SWT.NONE);
        item2.setText(new String[] {"Ennio","Morricone","Zeitz"});
        final TableItem item3 = new TableItem(t,SWT.NONE);
        item3.setText(new String[] {"Charles","Bronson","Potsdam"});
        
        final Text find = new Text(s, SWT.SINGLE | SWT.BORDER);
        final Text replace = new Text(s, SWT.SINGLE | SWT.BORDER);
        final Button replaceButton = new Button(s, SWT.BORDER | SWT.PUSH);
        replaceButton.setText("Ersetzen");
        replaceButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                TableItem[] tia = t.getItems();
                
                for(int i = 0; i<tia.length;i++)
                {
                    if (tia[i].getText(2).equals(find.getText()))
                    {
                        tia[i].setText(2, replace.getText());
                    }
                    
                }
            }
        });
               
        s.open();
        while(!s.isDisposed()){
            if(!d.readAndDispatch())
                d.sleep();
        }
        d.dispose();
    }
}
