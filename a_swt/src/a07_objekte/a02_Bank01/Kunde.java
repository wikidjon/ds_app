package a07_objekte.a02_Bank01;

public class Kunde {
    // hier werden die Variablen deklariert und mit Zugriffseigenschaften ausgestattet.
    private String inhaber;
    private int kontonummer = 0;
    private int kontostand = 0;
    
    // der KONSTRUKTOR	-	 Wichtig: er hat NIEMALS einen R�ckgabewert !!!
    public Kunde() {
        inhaber = "leer";
        kontonummer = 0;
        kontostand = 0;
    }
    
    // So wird ein Objekt gleich mit  Information gef�llt.
    public Kunde(String name, int geld, int knummer) {
        inhaber = name;
        kontostand = geld;
        kontonummer = knummer;
    }
    
    public void einzahlen(int betrag) {
        kontostand = kontostand + betrag;
    }
    
    public void auszahlen(int betrag) {
        kontostand = kontostand - betrag;
    }
    
    public int geldstand() {
        return kontostand;
    }
    
    public String gebeName() {
        return inhaber;
    }
    
    public int gebeKontonummer() {
        return kontonummer;
    }
}
