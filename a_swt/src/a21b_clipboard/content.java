package a21b_clipboard;

/*
 * Clipboard example snippet: enable/disable menu depending on clipboard content availability
 *
 * For a list of all SWT example snippets see
 * http://dev.eclipse.org/viewcvs/index.cgi/%7Echeckout%7E/platform-swt-home/dev.html#snippets
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class content {

  public static void main(String[] args) {
    Display display = new Display();
    final Clipboard cb = new Clipboard(display);
    Shell shell = new Shell(display);
    shell.setLayout(new FillLayout());
    final Text text = new Text(shell, SWT.BORDER | SWT.MULTI | SWT.WRAP);
    Menu menu = new Menu(shell, SWT.POP_UP);
    final MenuItem copyItem = new MenuItem(menu, SWT.PUSH);
    copyItem.setText("Copy");
    copyItem.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent e) {
        String selection = text.getSelectionText();
        if (selection.length() == 0)
          return;
        Object[] data = new Object[] { selection };
        Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
        cb.setContents(data, types);
      }
    });
    final MenuItem pasteItem = new MenuItem(menu, SWT.PUSH);
    pasteItem.setText("Paste");
    pasteItem.addSelectionListener(new SelectionAdapter() {
      public void widgetSelected(SelectionEvent e) {
        String string = (String) (cb.getContents(TextTransfer
            .getInstance()));
        if (string != null)
          text.insert(string);
      }
    });
    menu.addMenuListener(new MenuAdapter() {
      public void menuShown(MenuEvent e) {
        // is copy valid?
        String selection = text.getSelectionText();
        copyItem.setEnabled(selection.length() > 0);
        // is paste valid?
        TransferData[] available = cb.getAvailableTypes();
        boolean enabled = false;
        for (int i = 0; i < available.length; i++) {
          if (TextTransfer.getInstance()
              .isSupportedType(available[i])) {
            enabled = true;
            break;
          }
        }
        pasteItem.setEnabled(enabled);
      }
    });
    text.setMenu(menu);

    shell.setSize(200, 200);
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch())
        display.sleep();
    }
    cb.dispose();
    display.dispose();
  }
}

