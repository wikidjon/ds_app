package a99_xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public class a23_DomNodes
{
	public static void main (String args[]) throws Exception

	{
		/*
		if( args.length != 2 )
		{
			System.err.println( "Aufruf:   java a23_DomNodes <XmlFile> <TagName>" );
			System.err.println( "Beispiel: java a23_DomNodes MyXmlFile.xml Button" );
			System.exit( 1 );
		}
		 */
		try {
			String filename = "C:/a_xml/student.xml";
			// ******* Parsen der XML Datei *********************
			DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
			DocumentBuilder        builder  = factory.newDocumentBuilder();
			Document               document = builder.parse( new File( filename ) );

			// ****** Liste der Knoten des gewuenschten Elementtags *****
			NodeList ndList = document.getElementsByTagName( filename );
			printNodesFromList( ndList );  // printNodesFromList see below

		} catch( SAXParseException spe ) {
			System.out.println( "\n*** Parserfehler, Zeile: " + spe.getLineNumber()
					+ ", uri "  + spe.getSystemId() );
			System.out.println( "   " + spe.getMessage() );
			Exception e = ( spe.getException() != null ) ? spe.getException() : spe;
			e.printStackTrace();
		} catch( SAXException sxe ) {
			Exception e = ( sxe.getException() != null ) ? sxe.getException() : sxe;
			e.printStackTrace();
		} catch( ParserConfigurationException pce ) {
			pce.printStackTrace();
		} catch( IOException ioe ) {
			ioe.printStackTrace();
		}
	}

	// ****** Zusaetzliche Methoden *******

	public static void printNodeInfos( String sNodeName, Node node )
	{
		System.out.println(  "\n---------------------- " + sNodeName );
		if( null != node )
		{
			printObjIfVisible(   "getNodeType()        = ", "" + node.getNodeType() );
			printObjIfVisible(   "getNodeName()        = ", node.getNodeName() );
			printObjIfVisible(   "getLocalName()       = ", node.getLocalName() );
			printObjIfVisible(   "getNodeValue()       = ", node.getNodeValue() );
			if( node.hasAttributes() ) {
				printObjIfVisible( "getAttributes()      = ", node.getAttributes() );
			}
			if( node.hasChildNodes() ) {
				printObjIfVisible( "getChildNodes()      = ", node.getChildNodes() );
				printObjIfVisible( "getFirstChild()      = ", node.getFirstChild() );
			}
			printObjIfVisible(   "getPreviousSibling() = ", node.getPreviousSibling() );
			printObjIfVisible(   "getNextSibling()     = ", node.getNextSibling() );
		}
		System.out.println(    "----------------------\n" );
	}

	public static void printNodesFromList( NodeList ndList )
	{
		for( int i=0; i<ndList.getLength(); i++ ) {
			printNodeInfos( "ndList.item("+i+")", ndList.item(i) );
		}
	}

	private static void printObjIfVisible( String sValName, Object obj )
	{
		if( null == obj )  return;
		String s = obj.toString();
		if( null != s && 0 < s.trim().length() && !s.trim().equals( "\n" ) ) {
			System.out.println( sValName + s );
		}
	}
}