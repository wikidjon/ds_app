package csv;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


public class CSVReader {
	
	public static void main(String[] args) {
	
//--------------------- Erzeugen der Vektoren-----------------------------------------------------------------------------		
		Vector<String> Daten = new Vector<String> (0);  	// erzeugt einen Vector "Messstelle" vom Typ String 
		
		Vector<String> Messstelle = new Vector<String> (0);
		Vector<String> Zeitstempel = new Vector<String> (0);
		Vector<String> Messst = new Vector<String> (0);  // Vector mit den Messstellen ohne Dopplungen
		Vector<String> MessstelleZeit = new Vector<String>(0);  //Vector Zeiten zur Messstelle
		Vector<Integer> Zaehlwert=new Vector<Integer>(0);
		Vector<Integer> Zaehlwert1=new Vector<Integer>(0);
		Vector<String> Prioritaet=new Vector<String>(0);
		Vector<String> Prio=new Vector<String>(0);	
		
//-------------------------------------------------------------------------------------------------------------------------


//---------------------Variablendefinition----------------------------------------------------------------------------------		
		int z=0;
		int x;	
		int zeilenanzahl=0;
		
		int spaltenanzahl=0;
		
		int laengevector=0;
		int laengevectormessstelle=0;
		
//-------------------------------------------------------------------------------------------------------------------------		
		
		try {

			File CSVReader = new File("C:/Users/uschroet/workspace/a_swt/src/report_221210.csv");    
			FileReader reader = new FileReader(CSVReader);
			BufferedReader br = new BufferedReader(reader);
			String zeile;

			try {
				while ((zeile = br.readLine()) != null) {
					zeilenanzahl=zeilenanzahl+1;							//Bestimmung der Zeilenanzahl
					
				
					String[] ergebnis = zeile.split(";");
					spaltenanzahl=ergebnis.length;							//Bestimmung der L�nge des Arrays "ergebnis" --> Anzahl der Spalten
						
					
					for (int i = 0; i < spaltenanzahl; i++) {				
						
						Daten.addElement(ergebnis[i]);						//Elemente aus "ergebnis" in den Vektor "Daten" kopieren
											
					}
													
				} 
					
				laengevector=zeilenanzahl*spaltenanzahl;
				System.out.println(laengevector);
				
				for (int k=0; k<laengevector; ){			
					
					Messstelle.addElement(Daten.get(k));		 //Alle Messstellen vom Vektor "Daten" in den Vektor "Messstellen" kopieren
					k=k+spaltenanzahl;	

	
				}

				for (int k=1; k<laengevector; ){			
					Zeitstempel.addElement(Daten.get(k));		 //Alle Zeitstempel vom Vektor "Daten" in den Vektor "Zeitstempel" kopieren
					k=k+spaltenanzahl;	
					}
				
				for (int k=2; k<laengevector; ){			
					Prioritaet.addElement(Daten.get(k));		 //Alle Prioritaeten vom Vektor "Daten" in den Vektor "Prioritaet" kopieren
					k=k+spaltenanzahl;	
					}
					
//--------------------------------------------------------------
				
//---------- L�nge der Vektoren bestimmen-----------------------
				
				laengevectormessstelle=Messstelle.size();
				
//--------------------------------------------------------------

//----------- Vektor der Prioritaeten ohne Dopplungen -----------				
				
				Prio.addElement(Prioritaet.get(0));				//0. Element vom Vektor Prioritaet in Vektor Prio kopieren
				for (int m=1; m<Prioritaet.size() ;m++)		//Suche nach Dopplungen
				{
					x=0;
					for (int n=0; n<Prio.size();n++)
					{
						if (Prioritaet.elementAt(m).equals(Prio.elementAt(n)))   //Wenn Elemente aus Vektor Prioritaet noch nicht im Vektor 
						{														   //Prio vorhanden sind wird das Element in den Vektor Prio kopiert
							x=1;
						}
					}
					if (x==0)
					{
						Prio.addElement(Prioritaet.get(m));
					}
				}
				System.out.println("--------------------------------------");
				System.out.println("Folgende Prioritaeten traten auf: "+Prio);
				System.out.println(Prio.size()+" Prioritaeten traten auf.");
//--------------------------------------------------------------								
				
//----------- Vektor der Messstellen ohne Dopplungen -----------				
				
				Messst.addElement(Messstelle.get(0));				//0. Element vom Vektor Messstelle in Vektor Messst kopieren
				for (int m=1; m<laengevectormessstelle ;m++)		//Suche nach Dopplungen
				{
					x=0;
					for (int n=0; n<Messst.size();n++)
					{
						if (Messstelle.elementAt(m).equals(Messst.elementAt(n)))   //Wenn Elemente aus Vektor Messstelle noch nicht im Vektor 
						{														   //Messst vorhanden sind wird das Element in den Vektor Messst kopiert
							x=1;
						}
					}
					if (x==0)
					{
						Messst.addElement(Messstelle.get(m));
					}
				}
				System.out.println("--------------------------------------");
				System.out.println("Folgende Messstellen traten auf: "+Messst);
				System.out.println(Messst.size()+" Messstellen traten auf.");
//--------------------------------------------------------------				

//---------- Alle Vielfache der Prioritaeten auffinden ------------------------------------				
				
				
							
				System.out.println();
				for (int u=0; u<Prio.size();u++)
				{
					z=0;
					for (int l=0; l<Prioritaet.size();l++)
					{
						if (Prioritaet.elementAt(l).equals(Prio.elementAt(u)))
						{
							z=z+1;
							//MessstelleZeit.addElement(Zeitstempel.get(l));
						}
						
					}
					Zaehlwert1.addElement(z);
					
										
					System.out.println(Prio.elementAt(u) + " kommt " +z+ " mal.");
					
				}
				
				try
				{				
				PrintWriter writer = new PrintWriter( new BufferedWriter( new FileWriter ("C:/Users/uschroet/workspace/a_swt/src/report_auswertung_AnzahlPrioritaeten.csv" ) ) );
				for (int u=0; u<Prio.size();u++)
				{
				writer.println(Prio.get(u)+";"+Zaehlwert1.get(u));
				}	
				writer.close();
				}
				catch (IOException e) 
				{
					System.out.println("Fehler beim Erstellen der Datei");
				}		
		
//---------- Alle Vielfache der Messstellen auffinden ------------------------------------				
				
				
			
				
				System.out.println();
				for (int u=0; u<Messst.size();u++)
				{
					z=0;
					for (int l=0; l<laengevectormessstelle;l++)
					{
						if (Messstelle.elementAt(l).equals(Messst.elementAt(u)))
						{
							z=z+1;
							MessstelleZeit.addElement(Zeitstempel.get(l));
						}
						
					}
					Zaehlwert.addElement(z);
					
										
					System.out.println(Messst.elementAt(u) + " kommt " +z+ " mal.");
					
				}
				try
				{				
				PrintWriter writer = new PrintWriter( new BufferedWriter( new FileWriter ("C:/Users/uschroet/workspace/a_swt/src/report_auswertung_AnzahlMessstelle.csv" ) ) );
				for (int u=0; u<Messst.size();u++)
				{
				writer.println(Messst.get(u)+";"+Zaehlwert.get(u));
				}	
				writer.close();
				}
				catch (IOException e) 
				{
					System.out.println("Fehler beim Erstellen der Datei");
				}			
				
				
				System.out.println("--------------------------------------");				
				
				ArrayList<List<String>> list = new ArrayList<List<String>>();
				Long[] ek = new Long[list.size()];
				
				int k=0;
				int u=0;
				for (int v=0; v<Zaehlwert.size(); v++)
				{
					
					list.add(MessstelleZeit.subList(u,Zaehlwert.get(v)+k));
					//System.out.println("Messstelle "+ Messst.elementAt(v)+" kommt zu folgenden Zeiten: " +MessstelleZeit.subList(u,Zaehlwert.get(v)+k));
					
					k=Zaehlwert.get(v)+k;
					u=k;
					
				}
				
				try
				{				
				PrintWriter writer = new PrintWriter( new BufferedWriter( new FileWriter ("C:/Users/uschroet/workspace/a_swt/src/report_auswertung_221210.csv" ) ) );
				for ( int g=0; g<Messst.size();g++ )
				{
					writer.println(Messst.get(g) + list.get(g));
					
				}
				writer.close();
				}
				catch (IOException e) 
				{
					System.out.println("Fehler beim Erstellen der Datei");
				}
				
				
				
//-----------------------------------------------------------------------------------------------------------------------------------
	
				  
							
//--------------------------------------------------------------------------------------------------------

			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		
//----------------------- Schreiben in eine csv Datei ----------------------------------------------------		
		try
		{
			PrintWriter writer = new PrintWriter( new BufferedWriter( new FileWriter ("C:/Users/uschroet/workspace/a_swt/src/testreport_neu.csv" ) ) );
			for ( int q=0; q<MessstelleZeit.size();q++ )
			{
				writer.format(Messstelle.elementAt(q));				// schreibt Elemente vom Vektor Messstelle		
				writer.format(";");									// ; als Trennzeichen
				writer.format(Zeitstempel.elementAt(q));			// schreibt Elemente vom Vektor Zeitstempel
				writer.format(";");	
				writer.println();
			}
			writer.close();
		}
		catch (IOException e) 
		{
			System.out.println("Fehler beim Erstellen der Datei");
		}
		
//---------------------------------------------------------------------------------------------------------		 
	}
}
