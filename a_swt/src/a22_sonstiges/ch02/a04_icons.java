package a22_sonstiges.ch02;

// setzt icons mit verschiedenen Auflösungen

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a04_icons {
	public static void main(String[] args) {
		Display display = new Display();
		
		Image klein = new Image(display, 16, 16);
		GC gc = new GC(klein);
		gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
		gc.fillArc(0, 0, 16, 16, 45, 270);
		gc.dispose();
		
		Image gross = new Image(display, 64, 64);
		gc = new GC(gross);
		gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
		gc.fillArc(0, 0, 64, 64, 45, 270);
		gc.dispose();
		
		/* Provide different resolutions for icons to get
		 * high quality rendering wherever the OS needs 
		 * gross icons. For example, the ALT+TAB window 
		 * on certain systems uses a grossr icon.
		 */
		Shell shell = new Shell(display);
		shell.setText("kleine und grosse icons");
		shell.setImages(new Image[] {klein, gross});

		/* No gross icon: the OS will scale up the
		 * klein icon when it needs a gross one.
		 */
		Shell shell2 = new Shell(display);
		shell2.setText("kleines icon");
		shell2.setImage(klein);
		
		shell.open();
		shell2.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		klein.dispose();
		gross.dispose();
		display.dispose();
	}
}
