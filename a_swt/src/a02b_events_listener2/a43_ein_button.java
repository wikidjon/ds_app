package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class a43_ein_button {

	public static void main (String [] args) {
		final Display display = new Display ();
		Shell shell = new Shell (display);

		String s = "Test";

		Label label = new Label (shell, SWT.NONE);
		label.setText ("Geben Sie Ihren Namen ein:");

		final Text text = new Text (shell, SWT.BORDER);
		text.setLayoutData (new RowData (100, SWT.DEFAULT));
		text.setText(s);

		Button ok = new Button (shell, SWT.PUSH);
		ok.setText ("OK");
		ok.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("OK - "+ text.getText());
				String s = text.getText();
				System.out.println(s);
				Shell shell2 = new Shell (display);
				shell2.setLayout(new GridLayout(1, false));

				// Achtung ... gewöhnungsbedürftig !!!!
				new Label(shell2, SWT.BORDER).setText(s);

				shell2.open();
				shell2.pack();
			}
		});
		Button cancel = new Button (shell, SWT.PUSH);
		cancel.setText ("Abbruch");
		cancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Abbruch");
				display.dispose ();
			}
		});

		shell.setDefaultButton (cancel);
		shell.setLayout (new RowLayout ());
		shell.pack ();
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
