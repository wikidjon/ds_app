package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;

// Eventklasse
// Auffangen des Eventhandlings

public class a50_listener {

	public void Taste(final KeyEvent e) {

		// Holen der Ereignisquelle
		System.out.println("Ereignisquelle (e.getSource): " + e.getSource());
		System.out.println("e.getClass: " + e.getClass());
		if ((e.getSource()).toString().equals("Text {}"))
		{
			String string = "";
			switch (e.character) {
			case 0: string += " '\\0'"; break;
			case SWT.BS: string += " '\\b'"; break;
			case SWT.CR: string += " '\\r'"; break;
			case SWT.DEL: string += " DEL"; break;
			case SWT.ESC: string += " ESC"; break;
			case SWT.LF: string += " '\\n'"; break;
			default: string += " '" + e.character +"'";
			break;	}
			System.out.println ("Statemask: " + e.stateMask + " -  String: " + string.toString());}}

	public void TasteLos(final KeyEvent e) {
		if (e.stateMask == SWT.CONTROL && e.keyCode != SWT.CONTROL) {
			System.out.println("Taste losgelassen");
		}} };
