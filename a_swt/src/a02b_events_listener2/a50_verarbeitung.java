package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

// 	Tastaturereignisse auf Textfelder
// 	Verarbeitungs- / Logikklasse

//	final kann vor Variablen, Feldern, Methoden und Klassen stehen und erzwingt Unver�nderlichkeit.
//  Bei Variablen und Membervariablen ihre Konstanz, bei Methoden die Unm�glichkeit, sie in
//	abgeleiteten Klassen zu �berschreiben und schlie�lich bei Klassen, dass von ihnen keine weiteren
//	Ableitungen erfolgen k�nnen.
//  Einer finalen Variable wird einmalig ein Wert zugewiesen, der nachtr�glich nicht mehr ver�ndert
//	werden kann.
//	Eine finale Variable muss nicht in der Deklaration initialisiert werden.

public class a50_verarbeitung {

	public void setzeShell(final Shell s) {

		final a50_listener list = new a50_listener();    // final l�schen !!!!

		final Text text1 = new Text(s, SWT.BORDER);
		text1.setText("Bitte geben Sie Ihre Daten ein!");
		text1.setBounds(10, 10, 200, 20);
		text1.setTextLimit(30);

		// Eventhandler anmelden
		// Verarbeitung der Events separat in a50listener
		text1.addKeyListener(new KeyListener() {
			@Override
			// Mit @Override kann eine Methode gekennzeichnet werden, die die Methode ihrer Oberklasse
			// �berschreibt. Der Compiler stellt dann sicher, dass die Oberklasse diese Methode enth�lt
			// und gibt einen Fehler aus, wenn dies nicht der Fall ist.
			public void keyPressed(final KeyEvent e) {
				list.Taste(e);
				// ***********************************************
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				list.TasteLos(e);
				// ***********************************************
			}
		});

		final Text text2 = new Text(s, SWT.NONE);
		text2.setEchoChar('p');
		text2.setBounds(10, 50, 200, 20);
		text2.setText("Password");

		final Text text3 = new Text(s, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text3.setBounds(10, 90, 200, 100);
		text3.setEditable(false);
		text3.setText("Hier ist keine Eingabe m�glich.");

	}
}
