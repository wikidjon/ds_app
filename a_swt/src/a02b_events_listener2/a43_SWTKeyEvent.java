package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a43_SWTKeyEvent {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("SWT KeyEvent Beispiel");
		shell.setSize(250, 200);
		shell.setLayout(new FillLayout()); // Achtung !!!!

		new Label(shell, SWT.BORDER)
		.setText("Bitte Taste/Tastenkmbination dr�cken!");

		shell.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				String string = "";

				// Tastenkombination?
				if ((e.stateMask & SWT.ALT) != 0) {
					string += "ALT - keyCode = " + e.keyCode;
				}
				if ((e.stateMask & SWT.CTRL) != 0) {
					string += "CTRL - keyCode = " + e.keyCode;
				}
				if ((e.stateMask & SWT.SHIFT) != 0) {
					string += "SHIFT - keyCode = " + e.keyCode;
				}
				if (e.keyCode == SWT.BS) {
					string += "BACKSPACE - keyCode = " + e.keyCode;
				}
				if (e.keyCode == SWT.ESC) {
					string += "ESCAPE - keyCode = " + e.keyCode;
				}

				// Buchstaben ???
				if (e.keyCode >= 97 && e.keyCode <= 122) {
					string += " " + e.character + " - keyCode  = " + e.keyCode;
				}

				// Ziffern ???
				if (e.keyCode >= 48 && e.keyCode <= 57) {
					string += " " + e.character + " - keyCode = " + e.keyCode;
				}

				if (!string.equals("")) {
					System.out.println(string);
				}
			}
		});

		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}