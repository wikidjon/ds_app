package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

// Erkennen von Veränderungen an Textfeldern und
// pruefen der Zulässigkeit von Zeichen
// Verify- und ModifyListener

public class a46_text_mod {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(250,300);
		shell.open();

		Text text1 = new Text(shell, SWT.BORDER);
		text1.setText("Bitte geben Sie *Ihre Daten ein!");
		text1.setBounds(10,10,200,20);
		text1.setTextLimit(30);

		text1.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				System.out.println("Text modifiziert ....");
			}
		});
		text1.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				if (e.text.equals("*")) {
					System.out.println("* ist unzulässig");
					e.doit = false;									//doit KeyEvent
				}
			}
		});

		Text text2 = new Text(shell, SWT.NONE);
		text2.setEchoChar('p');
		text2.setBounds(10,50,200,20);
		text2.setText("Password");

		Text text3 = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text3.setBounds(10,90,200,100);
		text3.setEditable(false);
		text3.setText("Hier ist keine Eingabe möglich.");

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
		text1.dispose();
		text2.dispose();
		text3.dispose();
	}
}








