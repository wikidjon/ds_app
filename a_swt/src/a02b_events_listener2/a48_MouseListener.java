package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a48_MouseListener{

	public static void main(final String[] args) {

		final Display d = new Display();
		final Shell s = new Shell(d);

		s.setSize(250,200);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("MouseListener Beispiel");
		s.open();

		s.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(final MouseEvent e) {

			}
			@Override
			public void mouseDown(final MouseEvent e) {
				final Label l = new Label(s, SWT.FLAT);
				l.setText("Maustaste gedrueckt auf:" + e.x + " " + e.y);
				l.setBounds(e.x,e.y, 250,15);

			}
			@Override
			public void mouseUp(final MouseEvent e) {
				final Label l = new Label(s, SWT.FLAT);
				l.setText("Maustaste losgelassen auf:" + e.x + " " + e.y);
				l.setBounds(e.x,e.y, 250,15);
			}
		});

		while(!s.isDisposed()){
			if(!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}

}
