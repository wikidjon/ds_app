package a02b_events_listener2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

// Maus druecken/loslassen/Doppelklick und Mausposition
public class a41_mouse_event {

	public static void main(final String[] arguments) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setSize(300, 150);
		shell.open();

		final Button button1 = new Button(shell, SWT.PUSH);
		button1.setText("Drueck mich!");
		button1.setBounds(10,10,100,20);

		final Button button2 = new Button(shell, SWT.PUSH);
		button2.setText("Drueck mich auch!");
		button2.setBounds(140,10,100,20);

		//**********************************************************************
		//Adapter oder Listener: bei mehr als einer Methode --> Adapter
		//Adapter implementiert das Interface mit leeren Methoden, so dass die
		//nicht ben�tigten Methoden nicht implementiert werden m�ssen
		//Hier: Definition der Methoden ohne direkte Bindung an das Objekt
		final MouseAdapter mouseAdapter = new MouseAdapter() {
			@Override
			public void mouseDown(final MouseEvent e) {
				System.out.println("Widget: "+ e.widget +" gedrueckt");
				System.out.println("Maus gedrueckt auf (" + e.x + "," + e.y + ")");
			}
			@Override
			public void mouseUp(final MouseEvent e) {
				System.out.println("Maus losgelassen auf ("+ e.x + "," + e.y + ")");
			}

		};

		button1.addMouseListener(mouseAdapter);
		button2.addMouseListener(mouseAdapter);
		//***********************************************************************

		final Label label = new Label(shell, SWT.NONE);
		label.setText("Hier bitte double clicken");
		label.setBounds(15,60,200,20);
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(final MouseEvent e) {
				final Shell shell2 = new Shell(display);
				shell2.setSize(150,150);
				final Label label2 = new Label(shell2,SWT.NONE);
				label2.setText("Hallo Mausdruecker!!");
				label2.setBounds(10,50,150,40);
				shell2.open();
			}
		}
				);

		while(!shell.isDisposed()){
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}

		display.dispose();
		button1.dispose();
		button2.dispose();
	}
}
