package a02b_events_listener2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a47_KeyListener {

	public static void main(final String[] args) {

		final Display d = new Display();
		final Shell s = new Shell(d);

		s.setSize(250, 200);
		s.setImage(new Image(d, "src/icons/cut.gif"));
		s.setText("KeyListener Beispiel");
		s.setLayout(new RowLayout());

		final Combo c = new Combo(s, SWT.DROP_DOWN | SWT.BORDER);
		c.add("Beatles");
		c.add("Stones");
		c.add("Who");
		c.add("Cream");

		c.addKeyListener(new KeyListener() {
			String selectedItem = "";

			@Override
			public void keyPressed(final KeyEvent e) {
				//		if (c.getText().length() > 0)
				//			return;
				final String key = Character.toString(e.character);
				final String[] items = c.getItems();
				for (int i = 0; i < items.length; i++) {
					if (items[i].toLowerCase().startsWith(key.toLowerCase())) {
						c.select(i);
						this.selectedItem = items[i];
						return;
					}
				}
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				if (this.selectedItem.length() > 0) {
					c.setText(this.selectedItem);
				}
				System.out.println(this.selectedItem);
				this.selectedItem = "";
			}
		});
		s.open();
		while (!s.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}
		d.dispose();
	}
}
