package a01_grundlag;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a0_shell {

	public static void main(String[] args) {
		// Erzeugen eines neuen Display-Objekts f�r diese Applikation, das
		// danach �ber Display.getCurrent() zur Verf�gung steht
		Display display = new Display();
		
		// Erzeugen eines neuen Shell-Fensters mit unbestimmter Gr��e
		Shell shell = new Shell(display);

		// �ffnen des Fensters und verarbeiten aller Events bis das 
		// Fenster wieder geschlossen und somit die Applikation beendet wird
		shell.open();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
