package a01_grundlag;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class Beispiel1 extends Composite {

	Text nameFeld;
	Text adrFeld;
	Text telFeld;

	ArrayList<Text> felder = new ArrayList<Text>(); // alle Felder

	protected void loescheFelder() {
		for (Iterator<Text> i = felder.iterator(); i.hasNext();) {
			((Text) i.next()).setText("");
		}
	}

	public Beispiel1(Composite parent) {
		this(parent, SWT.NONE);
	}

	public Beispiel1(Composite parent, int style) {
		super(parent, style);
		createGui();
	}

	protected Text createLabelledText(Composite parent, String label) {
		return createLabelledText(parent, label, 20, null);
	}

	protected Text createLabelledText(Composite parent, String label, int limit, String tip) {
		Label l = new Label(parent, SWT.LEFT);
		l.setText(label);
		Text text = new Text(parent, SWT.SINGLE);
		if (limit > 0) {
			text.setTextLimit(limit);
		}
		if (tip != null) {
			text.setToolTipText(tip);
		}
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		felder.add(text);
		return text;
	}

	protected Button createButton(Composite parent, String label,
			SelectionListener l) {
		return createButton(parent, label, l);
	}

	protected Button createButton(Composite parent, String label, String tip,
			SelectionListener l) {
		Button b = new Button(parent, SWT.NONE);
		b.setText(label);
		if (tip != null) {
			b.setToolTipText(tip);
		}
		if (l != null) {
			b.addSelectionListener(l);
		}
		return b;
	}

	class MySelectionAdapter implements SelectionListener {
		public void widgetSelected(SelectionEvent e) {
			// Standard - hier soll nichts passieren
		}

		public void widgetDefaultSelected(SelectionEvent e) {
			widgetSelected(e);
		}
	};

	protected void createGui() {
		setLayout(new GridLayout(1, true));

		// Eingabemaske

		Group eingabeGroup = new Group(this, SWT.NONE);
		eingabeGroup.setText("Eingabedaten");
		// 2 Spalten, nicht gleich breit
		GridLayout entryLayout = new GridLayout(2, false);
		eingabeGroup.setLayout(entryLayout);
		eingabeGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		nameFeld = createLabelledText(eingabeGroup, "Name: ", 40, "Eingabe Name");
		adrFeld = createLabelledText(eingabeGroup, "Adresse: ", 40, "Eingabe Adresse");
		telFeld = createLabelledText(eingabeGroup, "Telefon: ", 20, "Eingabe Telefon");

		// Buttonbereich

		Composite buttons = new Composite(this, SWT.NONE);
		buttons.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		// alle Buttons haben gleiche Groesse
		FillLayout buttonLayout = new FillLayout();
		buttonLayout.marginHeight = 2;
		buttonLayout.marginWidth = 2;
		buttonLayout.spacing = 5;
		buttons.setLayout(buttonLayout);

		// OK Button gibt Werte aus
		@SuppressWarnings("unused")
		Button okButton = createButton(buttons, "&Ok", "Process input",
				new MySelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						System.out.println("Name:         "	+ nameFeld.getText());
						System.out.println("Adresse:      " + adrFeld.getText());
						System.out.println("Telefon:      " + telFeld.getText());
					}
				});

		// Loeschen Button loescht EIngabefelder
		@SuppressWarnings("unused")
		Button clearButton = createButton(buttons, "&Loeschen", "Loeschen Eingabe",
				new MySelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						loescheFelder();
						nameFeld.forceFocus();
					}
				});
	}

	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setText("Beispiel SWT Eingabemaske"); 

		shell.setLayout(new FillLayout());

		@SuppressWarnings("unused")
		Beispiel1 basic = new Beispiel1(shell);

		shell.pack(); 
		shell.open(); 

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) { 
				display.sleep(); 
			}
		}
		display.dispose(); 
	}
}
