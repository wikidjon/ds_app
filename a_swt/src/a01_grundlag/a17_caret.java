package a01_grundlag;
//package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Caret;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a17_caret {

	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);
		final Caret caret = new Caret (shell, SWT.NONE);   //SWT.BORDER
		caret.setBounds (10, 10, 2, 32);
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
