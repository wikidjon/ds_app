package a01_grundlag;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class a04_ShellResizeEvent {

	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display, SWT.SHELL_TRIM | SWT.H_SCROLL | SWT.V_SCROLL);

		shell.addListener (SWT.Resize,  new Listener () {
			@Override
			public void handleEvent (final Event e) {
				final Rectangle rect = shell.getClientArea ();
				System.out.println(rect);
				//System.out.println(rect.x + " - " + rect.y + " - " + rect.width + " -" +rect.height);   // .
			}
		});

		shell.open ();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}

}