package a01_grundlag;
/******************************************
 * SHADOW_IN, SHADOW_OUT, SHADOW_NONE: Styles zur Spezifikation der Schatteneffekte
 *******************************************/
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a07_LabelShadowInOut {
	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display, SWT.SHELL_TRIM);
		shell.setLayout(new FillLayout());

		Label label = new Label(shell, SWT.BORDER|SWT.SHADOW_OUT);
		label.setText("Das ist Text auf dem Label");

		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}