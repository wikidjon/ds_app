package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

// Fenster... mit label

public class a01_SWTHallo {
	public static void main(final String[] arguments) {
		// Erzeuge ein neues Display-Objekt f�r diese Applikation, das
		// danach �ber Display.getCurrent() zur Verf�gung steht
		final Display display = new Display();

		// Erzeuge ein neues Shell-Fenster mit 320x200 Pixeln Gr��e
		final Shell shell = new Shell(display);
		shell.setSize(320, 200);
		shell.setText("Test");

		// F�ge dem Fenster ein Label hinzu
		final Label label = new Label(shell, SWT.CENTER);	// NONE
		label.setText("Hallo Merseburg!");

		// Label soll den gesamten verf�gbaren Platz im Fenster ausf�llen
		// label.setBounds(shell.getClientArea());
		label.setBounds(15, 30, 400, 20);   // 		label.setBounds(0, 0, 400, 20);

		// �ffnen Fenster und verarbeiten aller Events bis das
		// Fenster wieder geschlossen und somit die Applikation beendet wird
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		// Gib alle angeforderten Betriebssystem-Ressourcen wieder frei
		display.dispose();
	}
}