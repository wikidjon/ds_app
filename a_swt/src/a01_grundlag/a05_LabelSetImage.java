package a01_grundlag;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class a05_LabelSetImage {
  public static void main(String[] args) {
    final Display display = new Display();
    final Shell shell = new Shell(display, SWT.SHELL_TRIM);
    shell.setLayout(new RowLayout());

//    Label label = new Label(shell, SWT.BORDER );
//    label.setText("Text als Label");
//    label.setLocation(200,200);

    Label label1 = new Label(shell, SWT.BORDER );
    label1.setImage(new Image(display,"src/icons/cut.gif"));
    shell.open();
    
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }

}