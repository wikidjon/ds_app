package a01_grundlag;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SWTAWTDemo {

	public static void main(String[] args) {
		final Display display = new Display();
		final Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		shell.setText("I am SWT");
		
		final Composite swing = new Composite(shell, SWT.EMBEDDED);
		
		Frame f = SWT_AWT.new_Frame(swing);
		Panel p = new Panel();//needed for Java 1.5
		f.add(p);
		JButton b = new JButton("I Swing");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				display.asyncExec(new Runnable() {
					public void run() {
						shell.close();
					}
				});
			}
		});
		p.add(b);

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}


