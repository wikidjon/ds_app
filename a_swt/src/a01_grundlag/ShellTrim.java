package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class ShellTrim {
	public static void main(String[] args) {
		new ShellTrim();
	}
	Display display = new Display();

	Shell shell = new Shell(this.display, SWT.SHELL_TRIM | SWT.TOOL);
	//Shell shell = new Shell(this.display, SWT.NO_TRIM);
	// Shell shell = new Shell(display, SWT.MAX);


	public ShellTrim() {
		this.shell.setText("Style: SHELL_TRIM | TOOL");

		this.shell.pack();
		this.shell.open();

		//	Shell shell2 = new Shell();
		//	shell2.pack();
		//	shell2.open();
		//textUser.forceFocus();

		// Set up the event loop.
		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				// If no more entries in event queue
				this.display.sleep();
			}
		}

		this.display.dispose();
	}

}
