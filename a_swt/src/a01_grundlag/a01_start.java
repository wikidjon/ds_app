package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class a01_start {

	public static void main(final String[] args) {

		final Display display = new Display();
		final int zaehler = 5; // Verzoegerung

		final Image image = new Image(display, "src/images/splash.jpg"); // Bildverzeichnis
		final Shell splash = new Shell(SWT.ON_TOP);

		final ProgressBar bar = new ProgressBar(splash, SWT.NONE);
		bar.setMaximum(zaehler);

		final Label label = new Label(splash, SWT.NONE);
		label.setImage(image);
		final FormLayout layout = new FormLayout();
		splash.setLayout(layout);

		splash.pack();
		//splash.setSize(300,200);
		final Rectangle splashRect = splash.getBounds();
		final Rectangle displayRect = display.getBounds();

		// Positionierung in der Mitte des BS
		/*
		final int x = (displayRect.width - splashRect.width) / 2;
		final int y = (displayRect.height - splashRect.height) / 2;
		splash.setLocation(x, y);
		 */
		splash.open();

		final Shell shell = new Shell(display);
		// shell.setLayout(new FillLayout());

		// Verzoegerung beim Slash
		for (int i = 0; i < 5; i++) {
			bar.setSelection(i + 1);
			try {
				Thread.sleep(1000);
			} catch (final Throwable e) {
			}
		}

		shell.open();
		splash.close();
		image.dispose();

		while (!shell.isDisposed())
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		display.dispose();
		label.dispose();
	}
}
