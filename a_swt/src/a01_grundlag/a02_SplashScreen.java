package a01_grundlag;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class a02_SplashScreen {

	public static void main(final String[] args) {
		final Display display = new Display();
		final int[] zaehler = new int[] { 5 };   							//Anzahl der Ladebalken
		final Image image = new Image(display, "src/images/splash.jpg");	//Bildverzeichnis
		final Shell splash = new Shell(SWT.ON_TOP);

		final ProgressBar bar = new ProgressBar(splash, SWT.NONE);
		bar.setMaximum(zaehler[0]);
		final Label label = new Label(splash, SWT.NONE);
		label.setImage(image);
		final FormLayout layout = new FormLayout();
		splash.setLayout(layout);

		// Positionierung der Progressbar
		final FormData progress = new FormData();
		progress.left = new FormAttachment(0, 5);
		progress.right = new FormAttachment(100, -5);
		progress.bottom = new FormAttachment(100, -5);
		bar.setLayoutData(progress);
		splash.pack();
		splash.setSize(150, 150);

		// Berechnung Mitte Bildschirm
		final Rectangle splashRecht = splash.getBounds();
		final Rectangle displayRect = display.getBounds();
		final int x = (displayRect.width - splashRecht.width) / 2;
		final int y = (displayRect.height - splashRecht.height) / 2;
		splash.setLocation(x, y);
		splash.open();

		//****???????????
		for (int i = 0; i < zaehler[0]; i++) {
			bar.setSelection(i + 1);
			try {
				Thread.sleep(1000);
			} catch (final Throwable e) {
			}
		}

		splash.close();
		image.dispose();

		final Shell shell = new Shell(display);
		shell.setSize(300, 300);
		shell.setMaximized(true);
		shell.open();

		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
