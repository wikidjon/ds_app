package a98_graphic_context;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class GCDemo {

	public static Display myDisplay;
	public static boolean internalCall = false;

	public static void main(String[] args) {
		internalCall = true;
		myDisplay = new Display();
		GCDemo gcd = new GCDemo();
		gcd.runDemo(myDisplay);
	}

	public void runDemo(Display display) {
		myDisplay = display;
		Shell shell = new Shell(display);
		shell.setSize(200, 240);
		shell.setText("GC Demo");

		Canvas canvas = new Canvas(shell, SWT.BORDER);

		canvas.setSize(150, 150);
		canvas.setLocation(20, 20);

		shell.open();
		
		//Graphic Kontext holen f�r das Canvas-Objekt
		GC gc = new GC(canvas);

		//Rechteckzeichnen von x = 3 bis 23, y = 5 bis 30 (inclusive)
		gc.drawRectangle(3, 5, 20, 25);

		//appears to do nothing, since background colour is still
		//the same as whatever we are drawing on
		//fills points x = 30 to 50, y = 5 to 30
		gc.fillRectangle(30, 5, 20, 25); 

		//get different colours from the system
		//Note we DON'T free (dispose) them, since they were allocated
		//by the system!
		Color blue = display.getSystemColor(SWT.COLOR_BLUE);
		Color red = display.getSystemColor(SWT.COLOR_RED);
		@SuppressWarnings("unused")
		Color green = display.getSystemColor(SWT.COLOR_GREEN);
		//set the foreground drawing colour
		gc.setForeground(blue);
		
		//draw an oval that fits within the specified rectangle
		//parameters are: x, y, width, height
		gc.drawOval(40, 40, 20, 10);

		//draw a line
		//parameters are x0, y0, x1, y1
		gc.drawLine(80, 20, 100, 80);
		
		//draw a polygon
		//int array represents X and Y coordinates, alternating
		gc.drawPolygon(new int[] {100, 100, 120, 120, 140, 100});

		//set the background colour.
		//gc.setBackground(green);
		gc.setBackground(red);

		//fill a rectangle red
		gc.fillRectangle(20, 100, 20, 20);

		//fill a red rectangle, then draw its outline blue
		gc.fillRectangle(50, 100, 20, 20);
		gc.drawRectangle(50, 100, 20, 20);

		//draw some text, blue on red
		gc.drawString("Text", 120, 20);

		//set a clipping region
		//and fill an oval that gets clipped
		gc.setClipping(40, 60, 40, 40);
		gc.fillOval(30, 50, 30, 25);

		//dispose of the GC, because it uses OS resources
		//that must be explicitly released
		gc.dispose();		

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}

		if (internalCall) display.dispose();

	}

}
