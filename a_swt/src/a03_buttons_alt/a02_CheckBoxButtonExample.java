package a03_buttons_alt;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class a02_CheckBoxButtonExample {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setLayout(new GridLayout(3, true));

    // Create three checkboxes
    new Button(shell, SWT.CHECK).setText("Checkbox 1");
    new Button(shell, SWT.CHECK).setText("Checkbox 2");
    new Button(shell, SWT.CHECK).setText("Checkbox 3");

    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}