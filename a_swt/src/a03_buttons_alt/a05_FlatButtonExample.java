package a03_buttons_alt;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

public class a05_FlatButtonExample {
  public static void main(String[] args) {
    Display display = new Display();
    Shell shell = new Shell(display);
    shell.setLayout(new GridLayout(3, true));

    // Create three flat buttons
    new Button(shell, SWT.FLAT).setText("Flat 1");
    new Button(shell, SWT.FLAT).setText("Flat 2");
    new Button(shell, SWT.FLAT).setText("Flat 3");


    shell.pack();
    shell.open();
    while (!shell.isDisposed()) {
      if (!display.readAndDispatch()) {
        display.sleep();
      }
    }
    display.dispose();
  }
}