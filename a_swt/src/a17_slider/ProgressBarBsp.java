package a17_slider;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

/**
 * This class simulates a long running operation
 */
class LongRunningOperation extends Thread {
	private Display display;
	private ProgressBar progressBar;

	public LongRunningOperation(Display display, ProgressBar progressBar) {
		this.display = display;
		this.progressBar = progressBar;
	}

	@Override
	public void run() {
		// Perform work here--this operation just sleeps
		for (int i = 0; i < 30; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// Do nothing
			}
			this.display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (LongRunningOperation.this.progressBar.isDisposed()) return;

					// Increment the progress bar
					LongRunningOperation.this.progressBar.setSelection(LongRunningOperation.this.progressBar.getSelection() + 1);
				}
			});
		}
	}
}

/**
 * This class demonstrates ProgressBar
 */
public class ProgressBarBsp {
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout());

		// Create a smooth progress bar
		ProgressBar pb1 = new ProgressBar(shell, SWT.HORIZONTAL | SWT.SMOOTH);
		pb1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		pb1.setMinimum(0);
		pb1.setMaximum(30);

		// Create an indeterminate progress bar
		//ProgressBarBsp pb2 = new ProgressBarBsp(shell, SWT.HORIZONTAL | SWT.INDETERMINATE);
		ProgressBarBsp pb2 = new ProgressBarBsp();
		//		pb2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// Start the first progress bar
		new LongRunningOperation(display, pb1).start();

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}

