package a17_slider;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

public class a17_LautstaerkeSlider {
	public static void main(String[] args) {
		new a17_LautstaerkeSlider();
	}
	Display display = new Display();

	Shell shell = new Shell(this.display);
	Slider slider;
	Text text;

	int wert;


	public a17_LautstaerkeSlider() {
		this.shell.setLayout(new GridLayout(1, true));

		Label label = new Label(this.shell, SWT.NULL);
		label.setText("Lautstärke:");

		this.slider = new Slider(this.shell, SWT.VERTICAL);
		this.slider.setBounds(0, 0, 40, 200);
		this.slider.setMaximum(20);
		this.slider.setMinimum(0);
		this.slider.setIncrement(1);
		this.slider.setPageIncrement(1);

		this.slider.setThumb(4);

		this.slider.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				a17_LautstaerkeSlider.this.wert = a17_LautstaerkeSlider.this.slider.getMaximum() - a17_LautstaerkeSlider.this.slider.getSelection() + a17_LautstaerkeSlider.this.slider.getMinimum() - a17_LautstaerkeSlider.this.slider.getThumb();
				a17_LautstaerkeSlider.this.text.setText("Wert: " + a17_LautstaerkeSlider.this.wert);
			}
		});

		this.text = new Text(this.shell, SWT.BORDER | SWT.SINGLE);
		this.text.setEditable(false);
		this.slider.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));
		this.text.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_CENTER));

		this.shell.pack();
		this.shell.open();

		while (!this.shell.isDisposed()) {
			if (!this.display.readAndDispatch()) {
				this.display.sleep();
			}
		}

		this.display.dispose();
	}

}
