package coolbar;

/*
 * CoolBar example snippet: create a cool bar
 *
 * For a list of all SWT example snippets see
 * http://dev.eclipse.org/viewcvs/index.cgi/%7Echeckout%7E/platform-swt-home/dev.html#snippets
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class CreateCB {

public static void main (String [] args) {
  Display display = new Display ();
  Shell shell = new Shell (display);
  CoolBar bar = new CoolBar (shell, SWT.BORDER);
  for (int i=0; i<2; i++) {
    CoolItem item = new CoolItem (bar, SWT.NONE);
    Button button = new Button (bar, SWT.PUSH);
    button.setText ("Button " + i);
    Point size = button.computeSize (SWT.DEFAULT, SWT.DEFAULT);
    item.setPreferredSize (item.computeSize (size.x, size.y));
    item.setControl (button);
  }
  bar.pack ();
  shell.open ();
  while (!shell.isDisposed ()) {
    if (!display.readAndDispatch ()) display.sleep ();
  }
  display.dispose ();
}
} 

