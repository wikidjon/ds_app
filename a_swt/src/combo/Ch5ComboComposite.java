package combo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

public class Ch5ComboComposite extends Composite {

  public Ch5ComboComposite(Composite parent) {
    super(parent, SWT.NONE);
    buildControls();
  }

  protected void buildControls() {
    setLayout(new RowLayout());

    int[] comboStyles = { SWT.SIMPLE, SWT.DROP_DOWN, SWT.READ_ONLY };

    for (int idxComboStyle = 0; idxComboStyle < comboStyles.length; ++idxComboStyle) {
      Combo combo = new Combo(this, comboStyles[idxComboStyle]);
      combo.add("Option #1");
      combo.add("Option #2");
      combo.add("Option #3");
    }
  }
}
