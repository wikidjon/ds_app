package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class a041_coolbar2 {

	public static void main (final String [] args) {
		final Display display = new Display ();
		final Shell shell = new Shell (display);
		final CoolBar bar = new CoolBar (shell, SWT.NONE); //.BORDER);
		for (int i=0; i<2; i++) {
			final CoolItem item = new CoolItem (bar, SWT.NONE);
			final Button button = new Button (bar, SWT.PUSH);
			button.setText ("Button " + i);
			final Point size = button.computeSize (SWT.DEFAULT, SWT.DEFAULT);
			item.setPreferredSize (item.computeSize (size.x, size.y));
			//item.setPreferredSize (item.computeSize (435, 65));
			item.setControl (button);
		}
		bar.pack ();
		shell.open ();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
}
