package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.ole.win32.OleClientSite;
import org.eclipse.swt.ole.win32.OleFrame;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

//import a13_layouts1.a_Table;

public class ba_Folder {

	public void folder(Shell s, Display d){
//		FillLayout fillLayout = new FillLayout ();
//		System.out.println ("Display Bounds=" + display.getBounds () + " Display ClientArea=" + display.getClientArea ());
//		s.setLayout(fillLayout);	//Platziert Menues nebeneinander
		
		TabFolder tabFolder1 = new TabFolder(s,SWT.NONE);
		tabFolder1.setBounds(d.getClientArea ());
	
		
		//Tabellen Tab *********************************************
		Composite buttonComp = new Composite(tabFolder1,SWT.NONE);
//		new a_Table().tabelle(buttonComp,d);
		TabItem item1 = new TabItem(tabFolder1,SWT.NONE);
		item1.setText("Tabellen");
		item1.setControl(buttonComp);

		//Text Tab **************************************************
		Composite labelComp = new Composite(tabFolder1,SWT.NONE);
	//	new a30_Notepad1().createWidgets();  //(labelComp,d);
//		labelComp.setLayout (fillLayout); //erweitert texteingabebereich
		
		final Font font = new Font(d, "Lucida Console", 10, SWT.NORMAL);
		Text text = new Text(labelComp, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		//text.setBounds(0,0,300,300);
		text.setBounds(d.getClientArea ());
		text.setFont(font);
//		text.setEditable(false);
		text.setText("Hier sind Eingaben m�glich.");
		
		TabItem item2 = new TabItem(tabFolder1,SWT.NONE);
		item2.setText("Text");
		item2.setControl(labelComp);

		// ???? Tab
		Composite testComp = new Composite(tabFolder1,SWT.NONE);
	//	testComp.setParent(new a10_Table().tabelle(s,d));
//*		testComp.setLayout (fillLayout);
		Label label3 = new Label(testComp,SWT.NONE);
		label3.setText("Hallo");
		
//		label3.setParent(new a10_Table().tabelle(s,d));
//			setLayoutData(new a10_Table().tabelle(s,d));
//		label3.setBounds(10,10,250,20);
//		new a10_Table().tabelle(s,d);
		
		TabItem item3 = new TabItem(tabFolder1,SWT.NONE);
		item3.setText("???");
		item3.setControl(testComp);
		
		//****************************************
		// Excel Tab
		Composite excelComp = new Composite(tabFolder1,SWT.NONE);
		Label label4 = new Label(excelComp,SWT.NONE);
		label4.setText("Excel");
//		excel_ole eo = new excel_ole();
//		eo.start(d);
		
//		shell.setText("Excel Example");
//		shell.setLayout(new FillLayout());
		try {
			OleFrame frame = new OleFrame(s, SWT.NONE);
			new OleClientSite(frame, SWT.NONE, "Excel.Sheet");
			//addFileMenu(frame);
		} catch (SWTError e) {
			System.out.println("Unable to open activeX control");
			return;
		}

		
		TabItem item4 = new TabItem(tabFolder1,SWT.NONE);
		item4.setText("Excel");
		item4.setControl(testComp);
		
		
		
		//****************************************
		// Word Tab
		Composite wordComp = new Composite(tabFolder1,SWT.NONE);
		TabItem item5 = new TabItem(tabFolder1,SWT.NONE);
		item5.setText("Word");
		item5.setControl(wordComp);

		//****************************************
		// ppt Tab
		@SuppressWarnings("unused")
		Composite pptComp = new Composite(tabFolder1,SWT.NONE);
		TabItem item6 = new TabItem(tabFolder1,SWT.NONE);
		item6.setText("Powerpoint");
		item6.setControl(testComp);

	}
	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());
		ba_Folder b = new ba_Folder();
		b.folder(shell, display );
		
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}