package a04_menue2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

/** Pulldown-Men�s...
 *  ohne Layoutmanager ... */

public class a04_Menue1{

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(300, 200);

		SelectionListener l = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
			}
		};

		Menu menubar;
		Menu menu;
		MenuItem item;

		menubar = new Menu(shell, SWT.BAR);		// 1. Schritt Menubar - SWT.BAR
		// shell ist eine Decoration - Instanzen der Klasse stellen das
		// Aussehen und Verhalten von Shells (und Dialogen) zur Verf�gung

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("Datei");

		//********************************
		menu = new Menu(menubar);		// Menue an Menubar haengen - Datei

		item.setMenu(menu);             // Untermenue anhaengen !!!  --> auskommentieren !!

		item = new MenuItem(menu, SWT.PUSH);	// Items anhaengen
		item.setText("�ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);


		//********************************
		menu = new Menu(menubar);  					//neues Menue an MB

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("Extras");
		item.setMenu(menu);

		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Textmodus");
		final MenuItem modusItem = item;				//Namen vergeben zur sp�teren Identifikation

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("Formatieren");
		final MenuItem formatItem = item;

		//Textmodus und Formatieren haengen an menu
		menu.addMenuListener(new MenuAdapter() {
			@Override
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());   //TRUE oder FALSE !!
			}
		});

		/**************************************************************
		 * Problem: Push an Cascadenmenue!!!
 		item = new MenuItem(menubar, SWT.PUSH);
		item.setText("Test");
		item.setMenu(menu);
		 **************************************************************/

		shell.setMenuBar(menubar);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}