package a04_menue2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * Eine Toolbar... erzeugt mit "create"-Methoden
 */
public class a04_Menue3 {

	private static Image createImage(String name) {
		return new Image(
				Display.getCurrent(),
				a04_Menue3.class.getResourceAsStream("/icons/" + name + ".gif"));
		//	final Image image = new Image(display, "src/icons/icon.gif");
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static void createToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	public static void main(String[] arguments) {
		Display display = new Display();
		final Shell shell = new Shell(display);

		// Toolbar statt Menue
		final ToolBar toolbar = new ToolBar(shell, SWT.FLAT);
		toolbar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		createToolItem(toolbar, "new");
		createToolItem(toolbar, "open");
		createToolItem(toolbar, "save");
		createSeparator(toolbar);
		createToolItem(toolbar, "undo");
		createSeparator(toolbar);
		createToolItem(toolbar, "cut");
		createToolItem(toolbar, "copy");
		createToolItem(toolbar, "paste");

		//Berechnen der Groesse fuer toolbar !!!!!
		shell.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				Rectangle r = shell.getClientArea();
				toolbar.setBounds(r.x, r.y, r.width, toolbar.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

}
