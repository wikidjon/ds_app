package a04_menue2;

// toolbar mit text

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

public class a04_toolbar_text {

public static void main (String [] args) {
	Shell shell = new Shell ();
	ToolBar bar = new ToolBar (shell, SWT.BORDER);
	for (int i=0; i<8; i++) {
		ToolItem item = new ToolItem (bar, SWT.PUSH);
		item.setText ("Eintrag " + i);
	}
	bar.pack ();
	shell.open ();
	Display display = shell.getDisplay ();
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch ()) display.sleep ();
	}
	display.dispose ();
} 
}
