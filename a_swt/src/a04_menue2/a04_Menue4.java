package a04_menue2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabFolderListener;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
public class a04_Menue4 {

	private static Image createImage(String name) {
		return new Image(
				Display.getCurrent(),
				a04_Menue4.class.getResourceAsStream("../icons/" + name + ".gif"));
	}

	public static void main(String[] arguments) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new FillLayout());

		CTabFolder folder = new CTabFolder(shell, SWT.NONE);

		//folder.setSelectionForeground(display.getSystemColor(SWT.COLOR_RED));

		CTabItem item;
		Text text;

		item = new CTabItem(folder, SWT.NONE);
		item.setText("Seite 1");
		item.setImage(createImage("open1"));
		text = new Text(folder, SWT.BORDER | SWT.MULTI);
		text.setText("Wir befinden uns auf Seite 1");
		item.setControl(text);

		item = new CTabItem(folder, SWT.NONE);
		item.setText("Zweite Seite");
		item.setImage(createImage("no"));
		text = new Text(folder, SWT.BORDER | SWT.MULTI);
		text.setText("Diesen Text lesen wir auf Seite zwei");
		item.setControl(text);

		folder.addCTabFolderListener(new CTabFolderListener() {
			@Override
			public void itemClosed(final CTabFolderEvent event) {
				event.item.dispose();
				System.out.println("Item geschlossen");
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}