package a04_menue2;
import org.eclipse.swt.SWT;
//import org.eclipse.swt.events.DisposeEvent;
//import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
//import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
//import org.eclipse.swt.layout.FillLayout;
//import org.eclipse.swt.layout.FormLayout;
//import org.eclipse.swt.layout.GridData;
//import org.eclipse.swt.layout.GridLayout;
//import org.eclipse.swt.widgets.Button;
//import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
//import org.eclipse.swt.widgets.TabFolder;
//import org.eclipse.swt.widgets.TabItem;
//import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class a99_menuea{

	public void menue(Shell s, Display d){

		Menu menubar = new Menu(s, SWT.BAR);
		Menu menu;
		MenuItem item;
		
		SelectionListener l = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println(e.widget.getData());
			}
		};

		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.PUSH);
		item.setText("�&ffnen...");
		item.setData("oeffnen");
		item.addSelectionListener(l);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Speichern\tStrg+S");
		item.setAccelerator(SWT.CTRL + 'S');
		item.setData("speichern");
		item.addSelectionListener(l);
	
		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Datei");
		item.setMenu(menu);

		//********************************
		menu = new Menu(menubar);
		item = new MenuItem(menu, SWT.CHECK);
		item.setText("Text&modus");
		final MenuItem modusItem = item;

		new MenuItem(menu, SWT.SEPARATOR);

		item = new MenuItem(menu, SWT.PUSH);
		item.setText("&Formatieren");
		final MenuItem formatItem = item;

		item = new MenuItem(menubar, SWT.CASCADE);
		item.setText("&Extras");
		item.setMenu(menu);

		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				formatItem.setEnabled(modusItem.getSelection());
			}
		});

		s.setMenuBar(menubar);
		
		final ToolBar toolbar = new ToolBar(s, SWT.FLAT);
		toolbar.setBackground(d.getSystemColor(SWT.COLOR_WHITE));

		neuToolItem(toolbar, "new");
		neuToolItem(toolbar, "open");
		neuToolItem(toolbar, "save");
		createSeparator(toolbar);
		neuToolItem(toolbar, "undo");
		createSeparator(toolbar);
		neuToolItem(toolbar, "cut");
		neuToolItem(toolbar, "copy" );
		neuToolItem(toolbar, "paste");
	}
	
	private static void neuToolItem(ToolBar toolbar, String name) {
		ToolItem item = new ToolItem(toolbar, SWT.PUSH);
		item.setImage(createImage(name));
	}

	private static void createSeparator(ToolBar toolbar) {
		new ToolItem(toolbar, SWT.SEPARATOR);
	}

	private static Image createImage(String name) {
		return new Image(
			Display.getCurrent(),
			a04_Menue3.class.getResourceAsStream("/images/" + name + ".gif"));
	}
	
}