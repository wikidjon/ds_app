package a04_menue2;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * Image als Fenster - Icon ...
 */
public class a04_Menue2 {
	public static void main(final String[] arguments) {
		final Display display = new Display();
		final Shell shell = new Shell(display);

		final Image image = new Image(display, "src/icons/icon.gif");

		// final Image image = new Image(Display.getCurrent(),
		// a04_Menue2.class.getResourceAsStream("/icons/icon.gif"));
		// private static Image createImage(String name) {
		// return new Image(Display.getCurrent(),
		// a04_Menue2.class.getResourceAsStream("/icons/" + name + ".gif"));
		// }

		// Icon im Fenster
		shell.setImage(image);
		shell.setSize(320, 200);
		shell.setBackground(display.getSystemColor(SWT.COLOR_WHITE));

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		image.dispose();
		display.dispose();
	}
}
