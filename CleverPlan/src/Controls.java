import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.layout.FillLayout;

public class Controls {
	private static Text text;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	
	public static void createCanvas(){
		
		
	}
	
	public static void main(String[] args) {
		Display display = Display.getDefault();
		Shell shell = new Shell();
		shell.setSize(956, 537);
		shell.setText("SWT Application");
		shell.setLayout(new GridLayout(3, false));
		
		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);
		
		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");
		
		Menu menu_1 = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_1);
		
		MenuItem help1 = new MenuItem(menu_1, SWT.PUSH);
		help1.setText("dwaw");
		
		Tree tree = new Tree(shell, SWT.BORDER);
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.heightHint = 427;
		tree.setLayoutData(gd_tree);
		
		
		
		Canvas canvas = new Canvas(shell, SWT.BORDER);
//		canvas.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 20;
		gridLayout.marginWidth = 20;
		canvas.setLayout(gridLayout);
		GridData gd_canvas = new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1);
		gd_canvas.heightHint = 447;
		gd_canvas.widthHint = 439;
		canvas.setLayoutData(gd_canvas);
		
		Canvas canvas_1 = new Canvas(canvas, SWT.NONE);
		canvas_1.setLayout(new GridLayout(1, false));
		canvas_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Button btnNewButton_1 = new Button(canvas_1, SWT.NONE);
		btnNewButton_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		btnNewButton_1.setText("New Button");
		
		
		text = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_text.heightHint = 428;
		gd_text.widthHint = 200;
		text.setLayoutData(gd_text);
		
		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblNewLabel.setText("New Label");
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);

		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
